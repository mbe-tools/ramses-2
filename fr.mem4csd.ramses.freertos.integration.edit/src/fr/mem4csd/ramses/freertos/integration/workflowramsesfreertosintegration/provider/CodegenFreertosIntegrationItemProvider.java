/**
 */
package fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.provider;


import fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration;
import fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationPackage;

import fr.mem4csd.ramses.freertos.workflowramsesfreertos.provider.CodegenFreertosItemProvider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CodegenFreertosIntegrationItemProvider extends CodegenFreertosItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodegenFreertosIntegrationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addMqttRuntimeDirPropertyDescriptor(object);
			addSocketsRuntimeDirPropertyDescriptor(object);
			addMqttRuntimeDirectoryURIPropertyDescriptor(object);
			addSocketsRuntimeDirectoryURIPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Mqtt Runtime Dir feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMqttRuntimeDirPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CodegenFreertosIntegration_mqttRuntimeDir_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CodegenFreertosIntegration_mqttRuntimeDir_feature", "_UI_CodegenFreertosIntegration_type"),
				 WorkflowramsesfreertosintegrationPackage.Literals.CODEGEN_FREERTOS_INTEGRATION__MQTT_RUNTIME_DIR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sockets Runtime Dir feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSocketsRuntimeDirPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CodegenFreertosIntegration_socketsRuntimeDir_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CodegenFreertosIntegration_socketsRuntimeDir_feature", "_UI_CodegenFreertosIntegration_type"),
				 WorkflowramsesfreertosintegrationPackage.Literals.CODEGEN_FREERTOS_INTEGRATION__SOCKETS_RUNTIME_DIR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Mqtt Runtime Directory URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMqttRuntimeDirectoryURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CodegenFreertosIntegration_mqttRuntimeDirectoryURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CodegenFreertosIntegration_mqttRuntimeDirectoryURI_feature", "_UI_CodegenFreertosIntegration_type"),
				 WorkflowramsesfreertosintegrationPackage.Literals.CODEGEN_FREERTOS_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sockets Runtime Directory URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSocketsRuntimeDirectoryURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CodegenFreertosIntegration_socketsRuntimeDirectoryURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CodegenFreertosIntegration_socketsRuntimeDirectoryURI_feature", "_UI_CodegenFreertosIntegration_type"),
				 WorkflowramsesfreertosintegrationPackage.Literals.CODEGEN_FREERTOS_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns CodegenFreertosIntegration.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/CodegenFreertosIntegration"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		return "Code Generation FreeRTOS Integration " + ((CodegenFreertosIntegration)object).getName();
//		String label = ((CodegenFreertosIntegration)object).getName();
//		return label == null || label.length() == 0 ?
//			getString("_UI_CodegenFreertosIntegration_type") :
//			getString("_UI_CodegenFreertosIntegration_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CodegenFreertosIntegration.class)) {
			case WorkflowramsesfreertosintegrationPackage.CODEGEN_FREERTOS_INTEGRATION__MQTT_RUNTIME_DIR:
			case WorkflowramsesfreertosintegrationPackage.CODEGEN_FREERTOS_INTEGRATION__SOCKETS_RUNTIME_DIR:
			case WorkflowramsesfreertosintegrationPackage.CODEGEN_FREERTOS_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI:
			case WorkflowramsesfreertosintegrationPackage.CODEGEN_FREERTOS_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return WorkflowramsesfreertosintegrationEditPlugin.INSTANCE;
	}

}
