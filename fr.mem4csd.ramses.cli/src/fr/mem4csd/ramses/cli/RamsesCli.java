package fr.mem4csd.ramses.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.osate.annexsupport.AnnexRegistry;
import org.osate.ba.AadlBaParserAction;
import org.osate.ba.AadlBaResolver;
import org.osate.ba.AadlBaUnParserAction;
import org.osate.ba.texteditor.AadlBaSemanticHighlighter;
import org.osate.ba.texteditor.AadlBaTextPositionResolver;

import de.mdelab.workflow.Workflow;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.mdelab.workflow.util.WorkflowLauncher;
import de.mdelab.workflow.util.WorkflowUtil;
import fr.mem4csd.ramses.core.util.RamsesStandaloneSetup;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationPackage;
import fr.mem4csd.ramses.freertos.workflowramsesfreertos.WorkflowramsesfreertosPackage;
import fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.WorkflowramseslinuxintegrationPackage;
import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;
import fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.WorkflowramsesnxtosekPackage;
import fr.mem4csd.ramses.pok.workflowramsespok.WorkflowramsespokPackage;
import fr.tpt.mem4csd.utils.osate.standalone.StandaloneAnnexRegistry.AnnexExtensionRegistration;

public class RamsesCli {

	private static final String MAIN_WORKFLOW_PATH = "platform:/plugin/fr.mem4csd.ramses.integration.all/ramses/integration/workflows/load_instanciate_validate_refine_and_generate_integration.workflow";

	public static void main(String[] args) {
		if(args.length != 1)
		{
			System.err.println("Wrong usage: one argument is expected, the path to the configuration file (.properties)");
		}
		
		String workingDirectory = System.getProperty("user.dir");
		System.out.println("INFO\t RAMSES-2 is running from (working directory): "+workingDirectory);
		
		
		// execute workflow
        URI workflowUri = URI.createURI(MAIN_WORKFLOW_PATH);
        
		Map<String, String> workflowPropertiesMap = new HashMap<String, String>();
        // StandAlone setup
        RamsesWorkflowKeys.setDefaultPropertyValues(workflowPropertiesMap);
		
        // TODO: move to BA plugin? --> BehaviorAnnexStandaloneSetup.initialize();
		List<AnnexExtensionRegistration> annexExtensions = new ArrayList<AnnexExtensionRegistration>();
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_PARSER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaParserAction.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_UNPARSER_EXT_ID, AadlBaUnParserAction.ANNEX_NAME, AadlBaUnParserAction.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_RESOLVER_EXT_ID, AadlBaResolver.ANNEX_NAME, AadlBaResolver.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_TEXTPOSITIONRESOLVER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaTextPositionResolver.class ));
		annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_HIGHLIGHTER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaSemanticHighlighter.class ));
		final ResourceSet resourceSet = new RamsesStandaloneSetup(annexExtensions).createResourceSet();
		
		final Resource workflowResource = resourceSet.getResource(workflowUri, true);

		final Workflow workflow = (Workflow) workflowResource.getContents().get(0);
		
		try (InputStream input = new FileInputStream(args[0])) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);
            
			final WorkflowLauncher launcher = new WorkflowLauncher(Collections.emptyList(), resourceSet, System.out);
            
            for(String key: prop.stringPropertyNames())
            	workflowPropertiesMap.put(key, prop.getProperty(key));
            	
            // TODO: need implementation for this properties resolution method
//            workflowPropertiesMap = WorflowUtils.resolveAllProperties(workflowPropertiesMap);
            
            // load ramses workflow components
            loadRamsesWorkflowComponents();
            
            // load AADL files
            String sourceFiles = workflowPropertiesMap.get(RamsesWorkflowKeys.SOURCE_AADL_FILE);
            if(sourceFiles==null)
            {
            	System.err.println("ERROR\t no source_aadl_file specified in configuration file " + args[0]);
            	return;
            }
            
            String sourceFileName = workflowPropertiesMap.get(RamsesWorkflowKeys.SOURCE_FILE_NAME);
            if(sourceFileName==null)
            {
            	System.err.println("ERROR\t AADL source_file_name is missing in configuration file " + args[0]);
            	return;
            }
            
            workflowPropertiesMap.remove(RamsesWorkflowKeys.SOURCE_AADL_FILE);
            
            File propertiesFile = new File(args[0]);
            URI propertiesFileURI = URI.createFileURI(propertiesFile.getAbsolutePath());
            
            
            boolean foundMainSourceFile = false;
            String[] sourceFileArray = sourceFiles.split(",");
            for(String sourceFile: sourceFileArray)
            {
            	// get properties file URI
            	URI sourceFileURI = URI.createFileURI(sourceFile);
            	propertiesFileURI = resourceSet.getURIConverter().normalize(propertiesFileURI);
            	if(!sourceFileURI.hasAbsolutePath())
            	{
            		sourceFileURI = WorkflowUtil.getResolvedURI( sourceFileURI, propertiesFileURI );
            	}
            	else
            		sourceFileURI = URI.createFileURI(sourceFile);
            	
            	String lastSegment = sourceFileURI.toFileString();
            	if(sourceFileURI.segmentCount()>1)
            		lastSegment = sourceFileURI.trimFileExtension().segment(sourceFileURI.segmentCount()-1);
            	
            	System.out.println("INFO\t loading AADL file: "+ sourceFile);
        		
            	foundMainSourceFile = !foundMainSourceFile && (lastSegment.equals(sourceFileName));
            	if(foundMainSourceFile)
            		workflowPropertiesMap.put(RamsesWorkflowKeys.SOURCE_AADL_FILE, sourceFileURI.toString());
            	else
            		resourceSet.getResource(sourceFileURI, true);
            }
            
            String outputDir = workflowPropertiesMap.get(RamsesWorkflowKeys.OUTPUT_DIR);
            URI outputDirURI = URI.createURI(outputDir);
			
        	if(!outputDirURI.hasAbsolutePath())
        	{
        		outputDirURI = WorkflowUtil.getResolvedURI( outputDirURI, propertiesFileURI );
        		workflowPropertiesMap.put(RamsesWorkflowKeys.OUTPUT_DIR, outputDirURI.toString());
        	}
        	
        	String targetInstallDir = workflowPropertiesMap.get(RamsesWorkflowKeys.TARGET_INSTALL_DIR);
        	if(targetInstallDir == null || targetInstallDir.isEmpty())
        	{
        		String defaultPokPath = System.getenv("HOME")+"/ramses-resources/pok";
        		String defaultNxtOSEKPath = System.getenv("HOME")+"/ramses-resources/nxtOSEK";
        		String defaultFreeRTOS = System.getenv("HOME")+"/ramses-resources/freertos";

            	workflowPropertiesMap.put(RamsesWorkflowKeys.TARGET_INSTALL_DIR, "POK="+defaultPokPath+","+"nxtOSEK="+defaultNxtOSEKPath+","+"FreeRTOS="+defaultFreeRTOS);
        	}
            
            try {
    			
            	if ( workflowResource.getContents().isEmpty() ) {
            		throw new WorkflowExecutionException( "Resource '" + workflowResource.getURI() + "' is empty." );
            	}
            	
            	launcher.execute(workflow, workflowPropertiesMap, new NullProgressMonitor());

//            	workflow.execute( new NullProgressMonitor(), System.out, workflowPropertiesMap, null, resourceSet );
            } catch (WorkflowExecutionException e) {
            	// TODO Auto-generated catch block
            	e.printStackTrace();
            }
    		
            
        } catch (IOException ex) {
            System.err.println("ERROR\t "+args[0]+" not found");
        }
		
	}

	private static void loadRamsesWorkflowComponents() {
		RamsesStandaloneSetup.initialize();
		// TODO move the targets code in IntegrationStandaloneSetup.initialize();
		// targets
		WorkflowramsesnxtosekPackage.eINSTANCE.eClass();
		WorkflowramseslinuxPackage.eINSTANCE.eClass();
		WorkflowramsespokPackage.eINSTANCE.eClass();
		WorkflowramseslinuxintegrationPackage.eINSTANCE.eClass();
		WorkflowramsesfreertosPackage.eINSTANCE.eClass();
		WorkflowramsesfreertosintegrationPackage.eINSTANCE.eClass();
	}

}
