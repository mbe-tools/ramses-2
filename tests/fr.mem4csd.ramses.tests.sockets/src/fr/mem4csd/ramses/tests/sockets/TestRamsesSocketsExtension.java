/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.sockets;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.junit.BeforeClass;
import org.junit.Test;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;
import fr.mem4csd.ramses.tests.core.AbstractRamsesPluginTest;
import fr.mem4csd.ramses.tests.core.utils.ReportComparator;

public class TestRamsesSocketsExtension extends AbstractRamsesPluginTest {
	
	private static final String SYSTEM_IMPL_NAME = "root.posix";
	private static final String SOURCES_PROJECT_NAME = "fr.mem4csd.ramses.tests.sockets.sources";
	
	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		cleanResources( SOURCES_PROJECT_NAME );

		if ( !Platform.isRunning() ) {
			WorkflowramseslinuxPackage.eINSTANCE.eClass();
		}
	}
//	
//	@BeforeClass
//	public static void cleanResources()
//	throws CoreException, InterruptedException, IOException {
//		if (Platform.isRunning()) {
//			final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject( PROJECT_NAME );
//			final IFolder folder = project.getFolder( MODEL_DIR );
//			final IFolder folderoutput = folder.getFolder( OUTPUT_DIR );
//			folderoutput.delete(true, new NullProgressMonitor());
//			folderoutput.create(true, true, new NullProgressMonitor());
//			project.build(IncrementalProjectBuilder.CLEAN_BUILD, new NullProgressMonitor());
//
//			// Building is performed in another thread so we need to ensure it jas 
//			Thread.sleep( 8000 );
//		}
//		else {
//			File outputDir = new File( "../" + PROJECT_NAME + "/" + MODEL_DIR + "/" + OUTPUT_DIR );
//			if(outputDir.exists())
//				FileUtils.cleanDirectory( outputDir );
//			WorkflowramsesmodesPackage.eINSTANCE.eClass();
//			WorkflowramsesposixPackage.eINSTANCE.eClass();
//			WorkflowramsessocketsPackage.eINSTANCE.eClass();
//		}
//	}

	
	@Test
	public void testLoadRefinement() 
	throws WorkflowExecutionException, IOException {
		final WorkflowExecutionContext workflowContext = executeWithContext("load_transformation_resources_sockets", createWorkflowProperties());
		
		assertFalse(workflowContext.getModelSlots().isEmpty());
	}
		
	@Test
	public void testRefinementSocketsPing() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String srcAadlFile;
		final String instanceModelFile;
		final String refinedAadlFile;
		final String refinedAadlFileComp;
		final String refinedAadlFileRef;

		final String refinedTraceFile;
		final String traceFileComp;
		final String traceFileRef;
		
		final URI instModelUri = computeInstanceModelUri( AADL_PING_URI, SYSTEM_IMPL_NAME );

		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_PING_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_PING_REFINED_URI, true ).toString();
			refinedAadlFileComp = refinedAadlFile;
			refinedAadlFileRef = URI.createPlatformResourceURI( AADL_PING_REFINED_URI_REF, true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_PING_REFINED_URI, true ).toString();
			traceFileComp = refinedTraceFile;
			traceFileRef = URI.createPlatformResourceURI( TRACE_PING_REFINED_URI_REF, true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_PING_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_PING_REFINED_URI;
			refinedAadlFileComp = TEST_SOURCES_ABS_LOCATION + AADL_PING_REFINED_URI;
			refinedAadlFileRef = TEST_SOURCES_ABS_LOCATION + AADL_PING_REFINED_URI_REF;

			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_PING_REFINED_URI;
			traceFileComp = TEST_SOURCES_ABS_LOCATION + TRACE_PING_REFINED_URI;
			traceFileRef = TEST_SOURCES_ABS_LOCATION + TRACE_PING_REFINED_URI_REF;
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );

		
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_PING_FILE_NAME );
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.sockets/ramses/sockets/workflows/default_sockets.properties");
		
		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_refine_core", props );		
		assertFalse(workflowContext.getModelSlots().isEmpty());
		
		final ReportComparator cmpAadl = new ReportComparator(refinedAadlFileComp, refinedAadlFileRef, getResourceSet());
		assertTrue( cmpAadl.checkResults() );

		final ReportComparator cmpTrace = new ReportComparator(traceFileComp, traceFileRef, getResourceSet());
		assertTrue( cmpTrace.checkResults() );
	}
	
	@Override
	protected String getSourcesProjectName() {
		return SOURCES_PROJECT_NAME;
	}

	@Override
	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.ramses.sockets";
	}
	
	@Override
	protected String[] getWorkflowDir() {
		String[] res = {"ramses", "sockets", "workflows"};
		return res;
	}
}
