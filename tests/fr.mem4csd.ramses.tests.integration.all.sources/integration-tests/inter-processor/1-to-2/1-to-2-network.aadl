--
-- AADL-RAMSES
-- 
-- Copyright ¬© 2012 TELECOM ParisTech and CNRS
-- 
-- TELECOM ParisTech/LTCI
-- 
-- Authors: see AUTHORS
-- 
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program.  If not, see 
-- http://www.eclipse.org/org/documents/epl-v10.php
--

package test_1_to_2_network
public
with Data_Model, ARINC653, common_pkg, RAMSES_buses, RAMSES_properties;
with POK;

  system root
  end root;

  system implementation root.impl
  subcomponents
    the_cpu1: processor common_pkg::connected_cpu_4p.with_system_partition;
    the_proc1: process proc1.impl;
    the_cpu2: processor common_pkg::connected_cpu.with_system_partition;
    the_proc2: process proc2.impl;
    the_proc3: process proc2.impl;
    the_cpu3: processor common_pkg::connected_cpu.with_system_partition;
    the_mem1: memory common_pkg::mem.two_partitions;
    the_mem2: memory common_pkg::mem.two_partitions;
    the_mem3: memory common_pkg::mem.two_partitions;
    the_mem4: memory common_pkg::mem.two_partitions;
    the_mem5: memory common_pkg::mem.two_partitions;
    the_bus1: bus ;
    the_bus2: bus ;
    the_bus3: bus ;
    the_bus4: bus ;
  connections
    bus_to_cpu1_1: bus access the_cpu1.bus_access -> the_bus1;
    bus_to_cpu1_2: bus access the_cpu1.bus_access2 -> the_bus2;
    bus_to_cpu1_3: bus access the_cpu1.bus_access3 -> the_bus3;
    bus_to_cpu1_4: bus access the_cpu1.bus_access4 -> the_bus4;
    bus_to_cpu2: bus access the_cpu2.bus_access -> the_bus1;
    bus_to_cpu3: bus access the_cpu3.bus_access -> the_bus2;
    proc_cnx1: port the_proc1.p_out -> the_proc2.p_in;
    proc_cnx2: port the_proc1.p_out -> the_proc3.p_in;
  properties
  	Actual_Processor_Binding => (reference (the_cpu1.the_part)) applies to the_proc1;
    Actual_Processor_Binding => (reference (the_cpu2.the_part)) applies to the_proc2;
    Actual_Processor_Binding => (reference (the_cpu3.the_part)) applies to the_proc3;
    Actual_Memory_Binding => (reference (the_mem1.part1_mem)) applies to the_proc1;
    Actual_Memory_Binding => (reference (the_mem2.part1_mem)) applies to the_proc2;
    Actual_Memory_Binding => (reference (the_mem3.part1_mem)) applies to the_proc3;
    Actual_Connection_Binding => (reference(the_bus1)) applies to proc_cnx1;
    Actual_Connection_Binding => (reference(the_bus2)) applies to proc_cnx2;
  end root.impl;

  system implementation root.linux extends root.impl
  	subcomponents
  		the_bus1: refined to bus RAMSES_buses::tcp_socket_bus;
    	the_bus2: refined to bus RAMSES_buses::tcp_socket_bus;
    	the_bus3: refined to bus RAMSES_buses::tcp_socket_bus;
    	the_bus4: refined to bus RAMSES_buses::tcp_socket_bus;
  		
  	properties
  		RAMSES_properties::Communication_Address => "127.0.0.1" applies to 	the_cpu1.bus_access, 
  																			the_cpu1.bus_access2,
  																			the_cpu2.bus_access,
  																			the_cpu3.bus_access;
    	RAMSES_properties::Communication_Port => "1238" applies to the_cpu1.bus_access;
    	RAMSES_properties::Communication_Port => "1236" applies to the_cpu1.bus_access2;
    	RAMSES_properties::Communication_Port => "1239" applies to the_cpu2.bus_access;
    	RAMSES_properties::Communication_Port => "1237" applies to the_cpu3.bus_access;
    	RAMSES_properties::Communication_Protocol => "SOCKETS_TCP" applies to the_bus1;
    	RAMSES_properties::Communication_Protocol => "SOCKETS_TCP" applies to the_bus2;
    	Scheduling_Protocol => (RMS) applies to the_cpu1, the_cpu2, the_cpu3;
    	RAMSES_Properties::Target => "linux" applies to the_cpu1, the_cpu2, the_cpu3;
  end root.linux;

  system implementation root.pok extends root.impl
  	properties
  		POK::Address=>"00:1F:C6:BF:74:06" applies to the_cpu1;
  		POK::Address=>"00:1E:A5:32:DE:32" applies to the_cpu2;
  		POK::Address=>"00:1E:A5:32:EF:32" applies to the_cpu3;
  		RAMSES_Properties::Target => "pok" applies to the_cpu1, the_cpu2, the_cpu3;
  end root.pok;


  process proc1
  features
    p_out: out event data port common_pkg::Integer {	Queue_Size => 5; 
    							          				Queue_Processing_Protocol => FIFO;};
  end proc1;

  process implementation proc1.impl
  subcomponents
    the_sender: thread common_pkg::eventdata_sender.impl;
    shutdown: thread common_pkg::shutdown.impl {
    								Dispatch_Protocol => Periodic;
    								Period => 100 ms;
    								Priority => 20;
    								Dispatch_Offset=> 20000 ms;};
  connections
    cnx: port the_sender.p_out -> p_out;
  end proc1.impl;

  process proc2
  features
    p_in: in event data port common_pkg::Integer{ Queue_Size => 10; 
    							    				Queue_Processing_Protocol => FIFO;};
  end proc2;

  process implementation proc2.impl
  subcomponents
    the_receiver: thread common_pkg::eventdata_receiver.impl;
    shutdown: thread common_pkg::shutdown.impl {
    								Dispatch_Protocol => Periodic;
    								Period => 100 ms;
    								Priority => 20;
    								Dispatch_Offset=> 20000 ms;};
  connections
    cnx: port p_in -> the_receiver.p_in;
  end proc2.impl;

end test_1_to_2_network;