--
-- AADL-RAMSES
-- 
-- Copyright ¬© 2012 TELECOM ParisTech and CNRS
-- 
-- TELECOM ParisTech/LTCI
-- 
-- Authors: see AUTHORS
-- 
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program.  If not, see 
-- http://www.eclipse.org/org/documents/epl-v10.php
--

package ping_network_mqtt
public
with Data_Model, ARINC653, common_pkg, RAMSES_buses, RAMSES_properties;

  virtual bus mqtt_ps
  	features
  		p: in out event data port common_pkg::Integer;
  end mqtt_ps;
  
  
  system root
  end root;

  system implementation root.impl
  subcomponents
    the_cpu1: processor common_pkg::connected_cpu.with_system_partition;
    the_proc1: process proc1.impl;
    the_cpu2: processor common_pkg::connected_cpu.with_system_partition;
    the_proc2: process proc2.impl;
    the_mem1: memory common_pkg::mem.two_partitions;
    the_mem2: memory common_pkg::mem.two_partitions;
  properties
  	Actual_Processor_Binding => (reference (the_cpu1.the_part)) applies to the_proc1;
    Actual_Processor_Binding => (reference (the_cpu2.the_part)) applies to the_proc2;
    Actual_Memory_Binding => (reference (the_mem1.part1_mem)) applies to the_proc1;
    Actual_Memory_Binding => (reference (the_mem2.part1_mem)) applies to the_proc2;
  end root.impl;

  system implementation root.linux extends root.impl
  	subcomponents
  		the_net: virtual bus mqtt_ps;
  	connections
    	proc1_cnx: port the_proc1.p_out -> the_net.p;
    	proc2_cnx: port the_net.p -> the_proc2.p_in;
  	properties
  		RAMSES_properties::Communication_Protocol => "MQTT" applies to the_net;
    	Scheduling_Protocol => (RMS) applies to the_cpu1, the_cpu2;
    	RAMSES_Properties::Target => "linux" applies to the_cpu1, the_cpu2;
  end root.linux;

  process proc1
  features
    p_out: out event data port common_pkg::Integer {	Queue_Size => 5; 
    							          				Queue_Processing_Protocol => FIFO;};
  end proc1;

  process implementation proc1.impl
  subcomponents
    the_sender: thread common_pkg::eventdata_sender.impl;
    shutdown: thread common_pkg::shutdown.impl {
    								Dispatch_Protocol => Periodic;
    								Period => 100 ms;
    								Priority => 10;
    								Dispatch_Offset=> 20000 ms;};
  connections
    cnx: port the_sender.p_out -> p_out;
  end proc1.impl;

  process proc2
  features
    p_in: in event data port common_pkg::Integer{ Queue_Size => 10; 
    							    				Queue_Processing_Protocol => FIFO;};
  end proc2;

  process implementation proc2.impl
  subcomponents
    the_receiver: thread common_pkg::eventdata_receiver.impl;
    shutdown: thread common_pkg::shutdown.impl {
    								Dispatch_Protocol => Periodic;
    								Period => 100 ms;
    								Priority => 10;
    								Dispatch_Offset=> 20000 ms;};
  connections
    cnx: port p_in -> the_receiver.p_in;
  end proc2.impl;

end ping_network_mqtt;