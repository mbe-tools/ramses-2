/**
 * AADL-RAMSES
 * 
 * Copyright ¬© 2012 TELECOM ParisTech and CNRS
 * 
 * TELECOM ParisTech/LTCI
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.  If not, see 
 * http://www.eclipse.org/org/documents/epl-v10.php
 */

#ifdef USE_POSIX
#include <stdio.h>
#include <stdlib.h>
#endif

#ifdef USE_FREERTOS
#include "FreeRTOS.h"
#endif

#ifdef USE_POK
#include <libc/stdlib.h>
#endif

#if !defined USE_POSIX && !defined USE_POK
#include "ecrobot_interface.h"
#endif

#include "user_code.h"


int i=0;
int init_counter=0; // prevent different execution trace orders during first hyperperiod
void s1()
{
  struct timespec current_time;
  #if defined USE_LINUX
  clock_gettime(CLOCK_MONOTONIC, &current_time);
  uint64_t time = (current_time.tv_sec*1000000)+current_time.tv_nsec/1000;
  #endif
  #ifdef USE_FREERTOS
  unsigned long time = xTaskGetTickCount()* 1000000 / configTICK_RATE_HZ;
  #endif
  
  uint64_t limit = time + 25*1000;
  while(time < limit){
    #if defined USE_LINUX
    clock_gettime(CLOCK_REALTIME, &current_time);
    time = (current_time.tv_sec*1000000)+current_time.tv_nsec/1000;
    #endif
    #ifdef USE_FREERTOS
    time = xTaskGetTickCount()* 1000000 / configTICK_RATE_HZ;
    #endif
  }

  mode_id_t mode;
  Current_System_Mode(&mode);
  if(init_counter<3)
    init_counter++;
  else
    printf("Executing s1 in mode %d\n", mode);
}

void s2()
{
  struct timespec current_time;
  #if defined USE_LINUX
  clock_gettime(CLOCK_REALTIME, &current_time);
  unsigned long time = (current_time.tv_sec*1000000)+current_time.tv_nsec/1000;
  #endif
  #ifdef USE_FREERTOS
  unsigned long time = xTaskGetTickCount()* 1000000 / configTICK_RATE_HZ;
  #endif

  unsigned long limit = time + 500*1000;
  while(time < limit){
    #if defined USE_LINUX
    clock_gettime(CLOCK_REALTIME, &current_time);
    time = (current_time.tv_sec*1000000)+current_time.tv_nsec/1000;
    #endif
    #ifdef USE_FREERTOS
    time = xTaskGetTickCount()* 1000000 / configTICK_RATE_HZ;
    #endif
  }

  mode_id_t mode;
  Current_System_Mode(&mode);
  if(init_counter<3)
    init_counter++;
  else
    printf("Executing s2 in mode %d\n", mode);
}

int counter;

void s3()
{
  struct timespec current_time;
  #if defined USE_LINUX
  clock_gettime(CLOCK_REALTIME, &current_time);
  unsigned long time = (current_time.tv_sec*1000000)+current_time.tv_nsec/1000;
  #endif
  #ifdef USE_FREERTOS
  unsigned long time = xTaskGetTickCount()* 1000000 / configTICK_RATE_HZ;
  #endif

  unsigned long limit = 0;
  mode_id_t mode;
  Current_System_Mode(&mode);
  i = (i+1)%3; // provoke a tfe every 3 iterations 
  if(i==0)
    limit = time + 2250*1000;
  else
    limit = time + 250*1000;
  while(time < limit){
    #if defined USE_LINUX
    clock_gettime(CLOCK_REALTIME, &current_time);
    time = (current_time.tv_sec*1000000)+current_time.tv_nsec/1000;
    #endif
    #ifdef USE_FREERTOS
    time = xTaskGetTickCount()* 1000000 / configTICK_RATE_HZ;
    #endif
  }
  Current_System_Mode(&mode);
  if(init_counter<3)
    init_counter++;
  else
    printf("Executing s3 in mode %d\n", mode);
  counter++;
  if(counter>9)
    exit(0);
}
