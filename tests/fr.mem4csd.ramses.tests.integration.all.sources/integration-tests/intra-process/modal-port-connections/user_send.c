/**
 * AADL-RAMSES
 * 
 * Copyright ¬© 2012 TELECOM ParisTech and CNRS
 * 
 * TELECOM ParisTech/LTCI
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.  If not, see 
 * http://www.eclipse.org/org/documents/epl-v10.php
 */

#ifdef USE_POSIX
#include <stdio.h>
#include <stdlib.h>
#endif

#ifdef USE_POK
#include <libc/stdlib.h>
#endif

#if !defined USE_POSIX && !defined USE_POK
#include "ecrobot_interface.h"
#endif

#include "user_send.h"
#include "aadl_runtime_services.h"

int event = 1;
int value = 1;
int counter = 0;
mode_id_t previous_mode = 1;

void check_event()
{
	if(event>5)
	{
#ifdef USE_POSIX
		exit(0);
#endif
#ifdef USE_POK
		STOP_SELF();
#endif
	}
}

void send_num(__send_num_context * ctx)
{
  check_event();
  mode_id_t current_mode = 3;
  Current_System_Mode(&current_mode);

  if (current_mode != previous_mode)
  {
    previous_mode = current_mode;
    value = (value % 100) + (current_mode + 1) * 100;
  }

// Next code section is introduced to prevent non-deterministic execution traces
#if defined(USE_LINUX)
  struct timespec current_time;
  clock_gettime(CLOCK_MONOTONIC, &current_time);
  unsigned long time = (current_time.tv_sec*1000000)+current_time.tv_nsec/1000;
  unsigned long limit = time + 500*1000;
  while(time < limit){
    clock_gettime(CLOCK_MONOTONIC, &current_time);
    time = (current_time.tv_sec*1000000)+current_time.tv_nsec/1000;
  }
#endif

  int test = Put_Value(ctx->result, &value);
#if defined(USE_POSIX) || defined(USE_POK)
  if(test == 0 && counter>0) {
    printf("sent %d\n", value);
  }
#else
  ecrobot_debug1(0, 0, mes);
#endif
  if(counter==0)
    counter++;
  value++;
}

void send(__send_context* ctx)
{
  check_event();
  if(event%1==0)
  {
	  Put_Value(ctx->result, NULL);
#if defined(USE_POSIX) || defined(USE_POK)
	  printf("sent event to change mode, %d\n", event);
#endif
  }
  event++;
}
