/**
 * AADL-RAMSES
 * 
 * Copyright ¬© 2012 TELECOM ParisTech and CNRS
 * 
 * TELECOM ParisTech/LTCI
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.  If not, see 
 * http://www.eclipse.org/org/documents/epl-v10.php
 */

#ifdef USE_POSIX
#include <stdio.h>
#include <stdlib.h>
#endif

#ifdef USE_POK
#include <libc/stdlib.h>
#endif

#if !defined USE_POSIX && !defined USE_POK
#include "ecrobot_interface.h"
#endif

#include "user_send.h"

unsigned char counter = 0;

void send(__send_context* ctx)
{
  Put_Value(ctx->result, NULL);
  counter++;
#if defined(USE_POSIX) || defined(USE_POK)
  printf("sent mode change request\n");
#ifdef USE_POSIX
  if(counter>9)
    exit(0);
#endif
#endif
}
