--
-- AADL-RAMSES
-- 
-- Copyright ¬© 2012 TELECOM ParisTech and CNRS
-- 
-- TELECOM ParisTech/LTCI
-- 
-- Authors: see AUTHORS
-- 
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program.  If not, see 
-- http://www.eclipse.org/org/documents/epl-v10.php
--

package common_pkg
public
with Data_Model,ARINC653,POK,RAMSES_processors,RAMSES_buses;

  --------------------------------------------------
  --- PROCESSOR COMPONENTS
  --------------------------------------------------
  
  processor cpu extends RAMSES_processors::pok_x86_qemu_processor
  properties
    ARINC653::HM_Error_ID_Levels => (
    	[ErrorIdentifier => 1; Description => "Numeric error"; ErrorLevel=>Module_Level;ErrorCode=>Numeric_Error;],
    	[ErrorIdentifier => 2; Description => "Illegal request"; ErrorLevel=>Module_Level;ErrorCode=>Illegal_Request;],
    	[ErrorIdentifier => 3; Description => "Numeric error"; ErrorLevel=>Partition_Level;ErrorCode=>Numeric_Error;],
    	[ErrorIdentifier => 4; Description => "Illegal request"; ErrorLevel=>Partition_Level;ErrorCode=>Illegal_Request;],
    	[ErrorIdentifier => 5; Description => "Numeric error"; ErrorLevel=>Process_Level;ErrorCode=>Numeric_Error;],
    	[ErrorIdentifier => 6; Description => "Illegal request"; ErrorLevel=>Process_Level;ErrorCode=>Illegal_Request;]
    );
    ARINC653::HM_Error_ID_Actions => (
    	[ErrorIdentifier => 1; Description => "Restart partition"; Action => "Stop";],
    	[ErrorIdentifier => 2; Description => "Warm partition restart"; Action => "Reset";]
    );
  end cpu;

  processor implementation cpu.single_partition
  subcomponents
    the_part:  virtual processor part;
  properties
    ARINC653::Module_Major_Frame => 2000 ms;
    ARINC653::Module_Schedule => (
       	[Partition => reference(the_part); Duration => 2000ms; Periodic_Processing_Start => true;]
    );
  end cpu.single_partition;

  processor implementation cpu.two_partitions
  subcomponents
    the_part1:  virtual processor part;
    the_part2:  virtual processor part;
  properties
    ARINC653::Module_Major_Frame => 4000 ms;
    ARINC653::Module_Schedule => (
       	[Partition => reference(the_part1); Duration => 2000ms; Periodic_Processing_Start => true;],
       	[Partition => reference(the_part1); Duration => 1000ms; Periodic_Processing_Start => true;],
       	[Partition => reference(the_part2); Duration => 1000ms; Periodic_Processing_Start => true;]
    );
    --POK::Ports_Flush_Time => Partition_Slot_Switch;
  end cpu.two_partitions;

  processor connected_cpu extends cpu
  features
    bus_access: requires bus access ;
  end connected_cpu;

  processor connected_cpu_2p extends connected_cpu
  features
    bus_access2: requires bus access ;
  end connected_cpu_2p;

  processor connected_cpu_4p extends connected_cpu_2p
  features
    bus_access3: requires bus access ;
    bus_access4: requires bus access ;
  end connected_cpu_4p;

  processor implementation connected_cpu.with_system_partition
  subcomponents
    the_part:  virtual processor part;
    system_part:  virtual processor part;
  properties
    ARINC653::System_Partition => true applies to system_part;
    ARINC653::Module_Major_Frame => 2000 ms;
    Scheduling_Protocol => (ARINC653);
    ARINC653::Module_Schedule => (
       	[Partition => reference(the_part); Duration => 1000ms; Periodic_Processing_Start => true;],
       	[Partition => reference(system_part); Duration => 1000ms; Periodic_Processing_Start => true;]
    );
    --POK::Ports_Flush_Time => Partition_Slot_Switch;
  end connected_cpu.with_system_partition;
  
  processor implementation connected_cpu_2p.with_system_partition extends connected_cpu.with_system_partition
  end connected_cpu_2p.with_system_partition;
  
  processor implementation connected_cpu_4p.with_system_partition extends connected_cpu_2p.with_system_partition
  end connected_cpu_4p.with_system_partition;
  
  processor implementation connected_cpu_2p.two_partitions
  subcomponents
    the_part1:  virtual processor part;
    the_part2:  virtual processor part;
  properties
    ARINC653::Module_Major_Frame => 2000 ms;
    ARINC653::Module_Schedule => (
       	[Partition => reference(the_part1); Duration => 1000ms; Periodic_Processing_Start => true;],
       	[Partition => reference(the_part2); Duration => 1000ms; Periodic_Processing_Start => true;]
    );
  end connected_cpu_2p.two_partitions;
  
  processor implementation connected_cpu.one_partition
  subcomponents
    the_part:  virtual processor part;
  properties
    ARINC653::Module_Major_Frame => 1000 ms;
    ARINC653::Module_Schedule => (
       	[Partition => reference(the_part); Duration => 1000ms; Periodic_Processing_Start => true;]
    );
  end connected_cpu.one_partition;
  
  processor implementation connected_cpu.two_partitions
  subcomponents
    the_part1:  virtual processor part;
    the_part2:  virtual processor part;
  properties
    ARINC653::Module_Major_Frame => 2000 ms;
    ARINC653::Module_Schedule => (
       	[Partition => reference(the_part1); Duration => 1000ms; Periodic_Processing_Start => true;],
       	[Partition => reference(the_part2); Duration => 1000ms; Periodic_Processing_Start => true;]
    );
  end connected_cpu.two_partitions;
  
  processor implementation connected_cpu_2p.one_partition  extends connected_cpu.with_system_partition
  end connected_cpu_2p.one_partition;
    
  --------------------------------------------------
  --- VIRTUAL PROCESSOR COMPONENTS
  --------------------------------------------------

  virtual processor part
  properties
    ARINC653::Partition_Identifier => 1;
    POK::Additional_Features => (libc_stdio, console);
    Scheduling_Protocol => (Round_Robin_Protocol);
    Period => 1000 Ms;
    ARINC653::HM_Error_ID_Actions => (
    	[ErrorIdentifier => 3; Description => "Restart process"; Action => "Cold_Restart";],
    	[ErrorIdentifier => 4; Description => "Warm partition restart"; Action => "Warm_Restart";]
    );
  end part;
  

  --------------------------------------------------
  --- MEMORY COMPONENTS
  --------------------------------------------------

  memory mem
  properties
    Memory_Size => 300000 bytes;
  end mem;

  memory implementation mem.impl
  properties
    Memory_Protocol => read_write;
    
    -- Issue #35 
    ARINC653::Memory_Kind => (Memory_Data);
--    ARINC653::Memory_Type => (Data_Memory);
  end mem.impl;

  memory implementation mem.two_partitions
  subcomponents
    part1_mem: memory part_mem;
    part2_mem: memory part_mem;
  end mem.two_partitions;
  

  memory part_mem
  properties
    Memory_Size => 250000 bytes;
    Memory_Protocol => read_write;

    -- Issue #35 
    ARINC653::Memory_Kind => (Memory_Data);
--    ARINC653::Memory_Type => (Data_Memory);
  end part_mem;

  --------------------------------------------------
  --- THREAD GROUPS
  --------------------------------------------------

  thread group readerwriter
  end readerwriter;

  thread group implementation readerwriter.impl
  subcomponents
    the_sender: thread data_sender.impl;
    the_receiver: thread data_receiver.impl;
  connections
    cnx: port the_sender.p_out -> the_receiver.p_in;
  end readerwriter.impl;

  --------------------------------------------------
  --- THREADS
  --------------------------------------------------

  thread data_sender
  features
    p_out: out data port Integer;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 2000 Ms;
    Priority => 5;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
    ARINC653::HM_Error_ID_Actions => (
    	[ErrorIdentifier => 5; Description => "Restart process"; Action => "Process_Restart";],
    	[ErrorIdentifier => 6; Description => "Warm partition restart"; Action => "Warm_Restart";]
    );
  end data_sender;

  thread implementation data_sender.impl
  calls
    call : { c : subprogram sender_spg;};
  connections
    cnx: parameter c.result -> p_out;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end data_sender.impl;

  thread data_sender_w_fg
  features
    p_fg: feature group inverse of fg;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 2000 Ms;
    Priority => 5;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
    ARINC653::HM_Error_ID_Actions => (
    	[ErrorIdentifier => 5; Description => "Restart process"; Action => "Process_Restart";],
    	[ErrorIdentifier => 6; Description => "Warm partition restart"; Action => "Warm_Restart";]
    );
  end data_sender_w_fg;

  thread implementation data_sender_w_fg.impl
  calls
    call : { c : subprogram sender_spg;};
  connections
    cnx: parameter c.result -> p_fg.p_in;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end data_sender_w_fg.impl;

  thread th1_w_fg2
  features
    th_fg2: feature group fg2;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 2000 Ms;
    Priority => 5;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
    ARINC653::HM_Error_ID_Actions => (
    	[ErrorIdentifier => 5; Description => "Restart process"; Action => "Process_Restart";],
    	[ErrorIdentifier => 6; Description => "Warm partition restart"; Action => "Warm_Restart";]
    );
  end th1_w_fg2;

  thread implementation th1_w_fg2.impl
  calls
    call : { c : subprogram fg2_spg;};
  connections
    cnx: parameter c.p1 -> th_fg2.p1;
    cnx2: parameter c.p2 -> th_fg2.p2;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end th1_w_fg2.impl;

  thread th2_w_fg2
  features
    th_fg2: feature group inverse of fg2;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 2000 Ms;
    Priority => 5;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
    ARINC653::HM_Error_ID_Actions => (
    	[ErrorIdentifier => 5; Description => "Restart process"; Action => "Process_Restart";],
    	[ErrorIdentifier => 6; Description => "Warm partition restart"; Action => "Warm_Restart";]
    );
  end th2_w_fg2;

  thread implementation th2_w_fg2.impl
  calls
    call : { c : subprogram fg2_spg;};
  connections
    cnx: parameter c.p2 -> th_fg2.p1;
    cnx2: parameter c.p1 -> th_fg2.p2;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end th2_w_fg2.impl;


  thread data_receiver
  features
    p_in: in data port Integer;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 1000 Ms;
    Priority => 10;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
    ARINC653::HM_Error_ID_Actions => (
    	[ErrorIdentifier => 3; Description => "Restart process"; Action => "Process_Restart";],
    	[ErrorIdentifier => 4; Description => "Warm partition restart"; Action => "Warm_Restart";]
    );
  end data_receiver;

  thread data_receiver_w_fg
  features
    p_fg: feature group fg;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 1000 Ms;
    Priority => 10;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
    ARINC653::HM_Error_ID_Actions => (
    	[ErrorIdentifier => 3; Description => "Restart process"; Action => "Process_Restart";],
    	[ErrorIdentifier => 4; Description => "Warm partition restart"; Action => "Warm_Restart";]
    );
  end data_receiver_w_fg;

  thread implementation data_receiver.impl
  calls
    call : { c : subprogram receiver_spg;};
  connections
    cnx: parameter p_in -> c.input;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end data_receiver.impl;

  thread implementation data_receiver_w_fg.impl
  calls
    call2 : { c : subprogram receiver_spg;};
  connections
    cnx2: parameter p_fg.p_in -> c.input;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call2);
  end data_receiver_w_fg.impl;

  thread eventdata_sender
  features
    p_out: out event data port Integer {Output_Time => ([Time => Deadline; Offset => 0 ns .. 0 ns;]);};
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 2000 Ms;
    Priority => 5;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
  end eventdata_sender;

  thread implementation eventdata_sender.impl
  calls
    call : { c : subprogram sender_spg;};
  connections
    cnx: parameter c.result -> p_out;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end eventdata_sender.impl;

  thread eventdata_receiver
  features
    p_in: in event data port Integer;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 1000 Ms;
    Priority => 10;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
  end eventdata_receiver;

  thread implementation eventdata_receiver.impl
  calls
    call : { c : subprogram receiver_spg;};
  connections
    cnx: parameter p_in -> c.input;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end eventdata_receiver.impl;

  thread eventdata_transmitter
  features
    p_in: in event data port Integer;
    p_out: out event data port Integer;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 1000 Ms;
    Priority => 10;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
  end eventdata_transmitter;
  
  thread implementation eventdata_transmitter.impl
  calls
    call : { c : subprogram transmitter_spg;};
  connections
    cnx: parameter p_in -> c.input;
    cnx2: parameter c.output -> p_out;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end eventdata_transmitter.impl;
  
  thread event_sender
  features
    p_out: out event port;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 1000 Ms;
    Priority => 5;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
  end event_sender;

  thread implementation event_sender.impl
  calls
    call : { c : subprogram event_sender_spg;};
  connections
  	cnx: port c.e -> p_out;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end event_sender.impl;

  thread event_receiver
  features
    p_in: in event port;
  properties
    Dispatch_Protocol => Timed;
    Period => 200 Ms;
    Priority => 10;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
  end event_receiver;

  thread implementation event_receiver.impl
  calls
    call : { c : subprogram event_receiver_spg;};
    call_nothing : { c2 : subprogram nothing_received; };
  properties
    Compute_Entrypoint_Call_Sequence => reference (call) applies to p_in;
    Compute_Entrypoint_Call_Sequence => reference (call_nothing);
  end event_receiver.impl;
  
  thread data_writer
   	features
   		da_w: requires data access Integer;
   	properties
   		Dispatch_Protocol => Periodic;
   		Period => 1000ms;
   		Priority => 3;
   end data_writer;
   
   thread implementation data_writer.impl
   	calls
    call : { c : subprogram sender_spg;};
  connections
    cnx: parameter c.result -> da_w;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
   end data_writer.impl;
   
   thread data_reader
   	features
   		da_r: requires data access Integer;
   	properties
   		Dispatch_Protocol => Periodic;
   		Period => 500 ms;
   		Priority => 4;
   end data_reader;
   
   thread implementation data_reader.impl
   	calls
    call : { c : subprogram receiver_spg;};
  connections
    cnx: parameter da_r -> c.input;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
   end data_reader.impl;
     
  thread periodic
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 500 Ms;
    Priority => 20;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
  end periodic;
  
  thread implementation periodic.impl
  calls
    call : { c : subprogram periodic_spg;};
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end periodic.impl;

  thread sporadic_receiver
  features
    p_in: in event data port Integer;
  properties
    Dispatch_Protocol => Sporadic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 1000 Ms;
    Priority => 15;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
  end sporadic_receiver;

  thread implementation sporadic_receiver.impl
  calls
    call : { c : subprogram receiver_spg;};
  connections
    cnx: parameter p_in -> c.input;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end sporadic_receiver.impl;
  
  thread hybrid_receiver
  features
    p_in: in event data port Integer;
  properties
    Dispatch_Protocol => Hybrid;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 1000 Ms;
    Priority => 15;
    Data_Size => 4000 bytes;
    Stack_Size => 4000 bytes;
    Code_Size => 40 bytes;
  end hybrid_receiver;
  
  thread implementation hybrid_receiver.impl
  calls
    call : { c : subprogram receiver_spg;};
    call_nothing : { c2 : subprogram nothing_received; };
  connections
    cnx: parameter p_in -> c.input;
  properties
  	Compute_Entrypoint_Call_Sequence => reference (call) applies to p_in;
    Compute_Entrypoint_Call_Sequence => reference (call_nothing);
  end hybrid_receiver.impl;
  
  thread data_receiver2p
  features
    p_in1: in data port Integer;
    p_in2: in data port Integer;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 1000 Ms;
    Priority => 10;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
    ARINC653::HM_Error_ID_Actions => (
    	[ErrorIdentifier => 3; Description => "Restart process"; Action => "Process_Restart";],
    	[ErrorIdentifier => 4; Description => "Warm partition restart"; Action => "Warm_Restart";]
    );
  end data_receiver2p;

  thread implementation data_receiver2p.impl
  calls
    call : { c : subprogram receiver_spg2p;};
  connections
    cnx1: parameter p_in1 -> c.input1;
    cnx2: parameter p_in2 -> c.input2;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end data_receiver2p.impl;
  
  thread shutdown
  end shutdown;
  
  thread implementation shutdown.impl
  calls
    call : { c : subprogram shutdown_spg;};
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end shutdown.impl;
  
  --------------------------------------------------
  --- SUBPROGRAMS
  --------------------------------------------------

  subprogram periodic_spg
  properties
    source_name => "periodic";
    source_language => (C);
    source_text => ("user_code.h","user_code.c");
  end periodic_spg;
  

  subprogram receiver_spg
  features
    input : in parameter Integer;
  properties
    source_name => "user_receive";
    source_language => (C);
    source_text => ("user_code.h","user_code.c");
  end receiver_spg;

  subprogram receiver_spg2p
  features
    input1 : in parameter Integer;
    input2 : in parameter Integer;
  properties
    source_name => "user_receive2p";
    source_language => (C);
    source_text => ("user_code.h","user_code.c");
  end receiver_spg2p;

  subprogram sender_spg
  features
    result : out parameter Integer;
  properties
    source_name => "user_send";
    source_language => (C);
    source_text => ("user_code.h","user_code.c");
  end sender_spg;

  subprogram transmitter_spg
  features
    input : in parameter Integer;
    output: out parameter Integer;
  properties
    source_name => "user_transmit";
    source_language => (C);
    source_text => ("user_code.h","user_code.c");
  end transmitter_spg;


  subprogram event_receiver_spg
  properties
    source_name => "event_received";
    source_language => (C);
    source_text => ("event_code.h","event_code.c");
  end event_receiver_spg;

  subprogram event_sender_spg
  features
    e: out event port;
  properties
    source_name => "send_event";
    source_language => (C);
    source_text => ("event_code.h","event_code.c");
  end event_sender_spg;

  subprogram nothing_received
  properties
    source_name => "nothing_received";
    source_language => (C);
    source_text => ("nothing_received.h","nothing_received.c");
  end nothing_received;

  subprogram fg2_spg
  features
  	p1: in parameter Integer;
  	p2: out parameter Integer;
  properties
  	source_name => "feature_group2_spg";
    source_language => (C);
    source_text => ("user_code.h","user_code.c");
  end fg2_spg;

  subprogram shutdown_spg
  properties
  	source_name => "target_shutdown";
    source_language => (C);
    source_text => ("user_code.h","user_code.c");
  end shutdown_spg;

  --------------------------------------------------
  --- DATA TYPES
  --------------------------------------------------
  
  data Integer
  properties
    Data_Model::Data_Representation => integer;
    Data_Size => 4 Bytes;
    RAMSES_Properties::Communication_Topic => "interesting_info";
  end Integer;

  --------------------------------------------------
  --- FEATURE GROUPS
  --------------------------------------------------

  feature group fg
  	features
  		p_in: in data port Integer;
  end fg;

  feature group fg2
  	features
  		p1: in data port Integer;
  		p2: out data port Integer;
  end fg2;

end common_pkg;
