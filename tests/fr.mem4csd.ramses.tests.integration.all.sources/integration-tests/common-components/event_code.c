/**
 * AADL-RAMSES
 * 
 * Copyright ¬© 2012 TELECOM ParisTech and CNRS
 * 
 * TELECOM ParisTech/LTCI
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.  If not, see 
 * http://www.eclipse.org/org/documents/epl-v10.php
 */

#include "event_code.h"
#include "aadl_runtime_services.h"
#ifdef USE_POK
#include <libc/stdio.h>
#include <arinc653/process.h>
#endif

#ifdef USE_POSIX
#include <stdio.h>
#endif

#ifdef USE_OSEK
#include "ecrobot_interface.h"
#endif

unsigned char counter=0;

void event_received()
{
  counter++;
  if(counter<10 && counter>1)
  {
    printf("received event: %d\n", counter);
  }
#ifdef USE_POSIX
  else if(counter>9)
	  exit(0);
#endif
#ifdef USE_POK
  else if(counter>9)
	  STOP_SELF();
#endif
}

void send_event (__send_event_context * context)
{
  if(counter<10 && counter>1)
    printf("send event\n");
  Put_Value(context->e, NULL);
  Send_Output (context->e);

#ifdef USE_POSIX
  if(counter>9)
	  exit(0);
#endif
#ifdef USE_POK
  if(counter>9)
	  STOP_SELF();
#endif
}
