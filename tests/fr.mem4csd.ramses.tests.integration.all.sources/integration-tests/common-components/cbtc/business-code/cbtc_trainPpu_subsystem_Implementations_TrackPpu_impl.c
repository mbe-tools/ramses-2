/*---------- Includes  ----------------------------------------------*/
#include "cbtc_trainPpu_subsystem_Implementations_TrackPpu_impl.h"


int railway_cycle_nb = 0;
const int max_railway_cycle_nb = 30;

////////////////////////////////////////////////////////////////////////////////////////////
//Operation : TrackPpu_cdvAcquisition


/*@
  @ requires \separated(trainDataIn,cdvOut);
  @ requires \valid(trainDataIn);
  @ requires \valid(cdvOut);

  @ assigns (*cdvOut);

@*/



void  TrackPpu_cdvAcquisition(TrainData* trainDataIn,CdvData* cdvOut){
// Start of user code TrackPpu_cdvAcquisition
  if(railway_cycle_nb<max_railway_cycle_nb)
    printf("\n-------------- Cycle %d ------------------\n", railway_cycle_nb);
  railway_cycle_nb++;

  cdvOut->ident = 5;
  cdvOut->train_position = trainDataIn->distance_m;

// End of user code

}



