--
-- RAMSES 2.0
-- 
-- Copyright © 2012-2015 TELECOM Paris and CNRS
-- Copyright © 2016-2020 TELECOM Paris
-- 
-- TELECOM Paris
-- 
-- Authors: see AUTHORS
--
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program. 
-- If not, see https://www.eclipse.org/legal/epl-2.0/
--

package test_ping_network
public
with Data_Model, RAMSES_properties;

  system root
  end root;

  system implementation root.impl
  subcomponents
    the_cpu1: processor connected_cpu;
    the_proc1_socket: process proc1.impl;
    the_cpu2: processor connected_cpu;
    the_proc2_socket: process proc2.impl;
    the_proc1_mqtt: process proc1.impl;
    the_proc2_mqtt: process proc2.impl;
    
    the_mem1: memory mem;
    the_mem2: memory mem;
    the_bus_socket: bus sock_bus;
    the_bus_mqtt: bus mqtt_bus;
  connections
    bus_to_cpu1: bus access the_cpu1.bus_access -> the_bus_socket;
    bus_to_cpu2: bus access the_cpu2.bus_access -> the_bus_socket;
    proc_sock_cnx: port the_proc1_socket.p_out -> the_proc2_socket.p_in;
    proc_mqtt_cnx1: port the_proc1_mqtt.p_out -> the_bus_mqtt.p_in;
    proc_mqtt_cnx2: port the_bus_mqtt.p_out -> the_proc2_mqtt.p_in;
    
  properties
  	Actual_Processor_Binding => (reference (the_cpu1)) applies to the_proc1_socket,the_proc1_mqtt;
    Actual_Processor_Binding => (reference (the_cpu2)) applies to the_proc2_socket,the_proc2_mqtt;
    Actual_Memory_Binding => (reference (the_mem1)) applies to the_proc1_socket,the_proc1_mqtt;
    Actual_Memory_Binding => (reference (the_mem2)) applies to the_proc2_socket,the_proc2_mqtt;
    Actual_Connection_Binding => (reference(the_bus_socket)) applies to proc_sock_cnx;
    RAMSES_properties::Communication_Address => "127.0.0.1" applies to the_cpu1.bus_access;
   	RAMSES_properties::Communication_Port => "1235" applies to the_cpu1.bus_access;
   	RAMSES_properties::Communication_Address => "127.0.0.1" applies to the_cpu2.bus_access;
   	RAMSES_properties::Communication_Port => "1234" applies to the_cpu2.bus_access;
   	RAMSES_properties::Communication_Protocol => "SOCKETS_TCP" applies to the_bus_socket;
   	RAMSES_properties::Communication_Protocol => "MQTT" applies to the_bus_mqtt;
   	Scheduling_Protocol => (RMS) applies to the_cpu1, the_cpu2;
  end root.impl;

  process proc1
  features
    p_out: out event data port Integer {	Queue_Size => 5; 
    							          				Queue_Processing_Protocol => FIFO;};
  end proc1;

  process implementation proc1.impl
  subcomponents
    the_sender: thread eventdata_sender.impl;
  connections
    cnx: port the_sender.p_out -> p_out;
  end proc1.impl;

  process proc2
  features
    p_in: in event data port Integer{ Queue_Size => 10; 
    							    				Queue_Processing_Protocol => FIFO;};
  end proc2;

  process implementation proc2.impl
  subcomponents
    the_receiver: thread eventdata_receiver.impl;
  connections
    cnx: port p_in -> the_receiver.p_in;
  end proc2.impl;

  processor connected_cpu
  features
    bus_access: requires bus access sock_bus;
  end connected_cpu;

  bus sock_bus
  end sock_bus;
  
  bus mqtt_bus
  features
    p_in: in event data port Integer;
    p_out: out event data port Integer;
  end mqtt_bus;
  
  memory mem
  properties
    Memory_Size => 300000 bytes;
  end mem;
  
  data Integer
  properties
    Data_Model::Data_Representation => integer;
    Data_Size => 4 Bytes;
    RAMSES_Properties::Communication_Topic => "the_top";
  end Integer;
  
    thread eventdata_sender
  features
    p_out: out event data port Integer {Output_Time => ([Time => Deadline; Offset => 0 ns .. 0 ns;]);};
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 2000 Ms;
    Priority => 5;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
  end eventdata_sender;

  thread implementation eventdata_sender.impl
  calls
    call : { c : subprogram sender_spg;};
  connections
    cnx: parameter c.result -> p_out;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end eventdata_sender.impl;

  thread eventdata_receiver
  features
    p_in: in event data port Integer;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 1000 Ms;
    Priority => 10;
    Data_Size => 40000 bytes;
    Stack_Size => 40000 bytes;
    Code_Size => 40 bytes;
  end eventdata_receiver;

  thread implementation eventdata_receiver.impl
  calls
    call : { c : subprogram receiver_spg;};
  connections
    cnx: parameter p_in -> c.input;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end eventdata_receiver.impl;
  
  subprogram receiver_spg
  features
    input : in parameter Integer;
  properties
    source_name => "user_receive";
    source_language => (C);
    source_text => ("user_code.h","user_code.c");
  end receiver_spg;

  subprogram sender_spg
  features
    result : out parameter Integer;
  properties
    source_name => "user_send";
    source_language => (C);
    source_text => ("user_code.h","user_code.c");
  end sender_spg;
end test_ping_network;