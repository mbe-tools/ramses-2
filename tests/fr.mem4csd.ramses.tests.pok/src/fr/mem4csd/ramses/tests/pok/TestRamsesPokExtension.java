/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.pok;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.mem4csd.ramses.pok.workflowramsespok.WorkflowramsespokPackage;
import fr.mem4csd.ramses.tests.core.utils.ReportComparator;

public class TestRamsesPokExtension extends AbstractRamsesPokExtensionTest {

	private static final String SYSTEM_IMPL_NAME = "main.pok";
	private static final String SOURCES_PROJECT_NAME = "fr.mem4csd.ramses.tests.pok.sources";
	
	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		cleanResources( SOURCES_PROJECT_NAME );

		if (!Platform.isRunning()) {
			WorkflowramsespokPackage.eINSTANCE.eClass();
		}
	}
		
	@Test
	public void testLoadTransformationResources() 
	throws WorkflowExecutionException, IOException {
		final WorkflowExecutionContext workflowContext = executeWithContext("load_transformation_resources_pok", createWorkflowProperties());
		
		assertFalse(workflowContext.getModelSlots().isEmpty());
	}
	
	@Test
	public void testValidateSampledCommunications() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();
		
		final String aadlFile;
		final String reportFile;
		final String reportFileRef;
		final String instanceModelFile;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );

		if ( Platform.isRunning() ) {
			aadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			reportFile = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_URI, true ).toString();
			reportFileRef = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_URI_REF, true ).toString();
		}
		else {
			aadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_URI;
			reportFileRef = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_URI_REF;
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, aadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_FILE_NAME);
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.pok/ramses/pok/workflows/default_pok.properties");

		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_validate_core", props );		
		
		assertFalse(workflowContext.getModelSlots().isEmpty());
		
		ReportComparator cmp = new ReportComparator( reportFileRef, reportFile, getResourceSet());
		boolean results = cmp.checkResults();

		assertTrue(results);
	}
	
	@Test
	public void testValidateSampledCommunicationsInvalid() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();
		
		final String aadlFile;
		final String instanceModelFile;
		final String reportFile;
		final String reportFileRef;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );

		if ( Platform.isRunning() ) {
			aadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_INVALID_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			reportFile = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_INVALID_URI, true ).toString();
			reportFileRef = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_INVALID_URI_REF, true ).toString();
		}
		else {
			aadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_INVALID_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI;
			reportFileRef = TEST_SOURCES_ABS_LOCATION+ REPORT_SAMPLED_COM_INVALID_URI_REF;
		}
		
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, aadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_INVALID_FILE_NAME);

		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.pok/ramses/pok/workflows/default_pok.properties");

		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		try {
			final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_validate_core", props );		
			assertFalse(workflowContext.getModelSlots().isEmpty());
		}
		catch(WorkflowExecutionException ex)
		{
			// nothing to do
		}
		
		final ReportComparator cmp = new ReportComparator( reportFileRef, reportFile, getResourceSet());
		boolean results = cmp.checkResults();
		assertTrue(results);
	}
	
	@Test
	public void testRefinementSampledCommunications() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String srcAadlFile;
		final String instanceModelFile;
		final String refinedAadlFile;
		final String refinedAadlFileRef;

		final String traceFile;
		final String traceFileRef;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );

		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI, true ).toString();
			refinedAadlFileRef = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI_REF, true ).toString();
			traceFile = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_URI, true ).toString();
			traceFileRef = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_URI_REF, true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;
			refinedAadlFileRef = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI_REF;
			traceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI;
			traceFileRef = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI_REF;
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, traceFile );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_FILE_NAME);
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.pok/ramses/pok/workflows/default_pok.properties");
		
		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_refine_core", props );		
		assertFalse(workflowContext.getModelSlots().isEmpty());

		final ReportComparator cmpTrace = new ReportComparator( traceFileRef, traceFile, getResourceSet() );
		assertTrue( cmpTrace.checkResults() );
		
		final ReportComparator cmpAadl = new ReportComparator( refinedAadlFileRef, refinedAadlFile, getResourceSet() );
		assertTrue( cmpAadl.checkResults() );
	}
	
	@Test
	public void testCodegenExecution()
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String refinedAadlFile;
		final String instanceModelFile;
		final String srcAadlFile;
		final String refinedTraceFile;
		final String outputDir;
		final String includeDir;
		final String installDir;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		
		if (Platform.isRunning()) {
			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_IN_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_IN_URI, true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
			installDir = URI.createPlatformResourceURI( POK_INSTALL_DIR_URI.toString(), true ).toString();
		}
		else {
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_IN_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_IN_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			includeDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.toString();
			installDir = POK_INSTALL_DIR_URI.toString();
		}

		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.TARGET_INSTALL_DIR, installDir );

		final WorkflowExecutionContext workflowContext = executeWithContext("load_and_generate_pok", props );		
		
		final String codeGenDir;
		
		if (Platform.isRunning()) {
			codeGenDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			codeGenDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}
		
		URI mainFileURI = null;
		if (Platform.isRunning()) {
			mainFileURI = CommonPlugin.resolve( URI.createFileURI( codeGenDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("main.c") );
		} else {
			mainFileURI = URI.createURI( codeGenDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("main.c");
		}
		File mainFile = new File( mainFileURI.toFileString() );
		
		assertTrue(mainFile.exists());
		
		assertFalse(workflowContext.getModelSlots().isEmpty());
	}
	
	@Test
	public void testCodegenCompilation()
			throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String workingDir;
		final String installDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
			installDir = URI.createPlatformResourceURI( POK_INSTALL_DIR_URI.toString(), true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
			installDir = POK_INSTALL_DIR_URI.toString();
		}
		
		props.put( RamsesWorkflowKeys.WORKING_DIR, workingDir );
		props.put( RamsesWorkflowKeys.TARGET_INSTALL_DIR, installDir );

		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "compile_codegen_core", props );

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc.elf") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc.elf");
		}
		
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
	}
	
	@Test
	public void testPokExecution() 
			throws WorkflowExecutionException, IOException, InterruptedException, ExecutionException, TimeoutException, CoreException {
		
		cleanResources(); // if same output dir as a previous test, libpok is going to be reused.
		
		final Map<String, String> props = createWorkflowProperties();
		
		final String srcAadlFile;		
//		final String refinedAadlFile;
//		final String refinedTraceFile;
		final String includeDir;
		final String outputDir;
		final String instanceModelFile;
//		final String reportFile;
		final String installDir;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		
		if (Platform.isRunning()) {
//			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_IN_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
//			refinedTraceFile = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_IN_URI, true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
//			reportFile = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_INVALID_URI, true ).toString();
			installDir = URI.createPlatformResourceURI( POK_INSTALL_DIR_URI.toString(), true ).toString();
		} else {
//			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_IN_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
//			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_IN_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			includeDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.toString();
//			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI;
			installDir = POK_INSTALL_DIR_URI.toString();
		}
		
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
//		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
//		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
//		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile );
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.TARGET_INSTALL_DIR, installDir );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_FILE_NAME);
		
		
		final WorkflowExecutionContext workflowContext = executeWithContext( "load_instanciate_validate_refine_and_generate_pok", props );

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI, true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc.elf") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc.elf");
		}
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
		
		final String traceFileRef;
		final String traceFileName = "the_cpu.exec_trace";
		
		if (Platform.isRunning()) {
			URI traceFileRefURI = URI.createPlatformResourceURI(BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( traceFileName ).toString(), true);
			traceFileRef = CommonPlugin.resolve( traceFileRefURI).toFileString();
		} else {
			traceFileRef = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment(OUTPUT_DIR_REF).appendSegment(traceFileName).toString();
		}
		
		testGeneratedCodeExecution(URI.createURI( TEST_SOURCES_ABS_LOCATION + CODEGEN_URI ).toFileString());
		
		// sleep long enough to let generated code execute for at least 10 sec.
		Thread.sleep(40*1000);
		
		URI theCpuTraceURI = null;		
		if (Platform.isRunning()) {
			theCpuTraceURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment(traceFileName));
		} else {
			theCpuTraceURI = URI.createURI( TEST_SOURCES_ABS_LOCATION + CODEGEN_URI )
					.appendSegment("the_cpu")
					.appendSegment(traceFileName);
		}
		
		File execTrace1 = new File(theCpuTraceURI.toFileString());
		File execTrace2 = new File(URI.createURI(traceFileRef).toFileString());
		
		assertTrue(execTrace1.exists());
		assertTrue(execTrace2.exists());
		
		boolean sameExecutionTrace = FileUtils.contentEquals(execTrace1, execTrace2);
		
		List<String> commandLine = new ArrayList<String>();
		commandLine.add("sudo");
		commandLine.add("killall");
		commandLine.add("qemu-system-i386");
		int ret = executeProcess(commandLine);
		if(ret!=0)
		{
			System.out.println("Could not killall processes qemu-system-i386; do it manually !!!");
			System.out.println("To fix this, make sure you are a sudoer and executing killall without entering your password is allowed");
		}
		assertTrue(sameExecutionTrace);
	}
	
	@Override
	protected String getSourcesProjectName() {
		return SOURCES_PROJECT_NAME;
	}

	@Override
	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.ramses.pok";
	}

	@Override
	protected String[] getWorkflowDir() {
		String[] res = {"ramses", "pok", "workflows"};
		return res;
	}
	
	@Override
	protected String getTargetName()
	{
		return "pok";
	}
}
