/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.osek;

import static org.junit.Assert.assertFalse;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.WorkflowramsesnxtosekPackage;
import fr.mem4csd.ramses.tests.core.AbstractRamsesPluginTest;

public class TestRamsesOsekExtension extends AbstractRamsesPluginTest {
	
	private static final String SOURCES_PROJECT_NAME = "fr.mem4csd.ramses.tests.osek.sources";
	
	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		cleanResources( SOURCES_PROJECT_NAME );

		if (!Platform.isRunning()) {
			WorkflowramsesnxtosekPackage.eINSTANCE.eClass();
		}
	}
		
	@Test
	public void testLoadAadlResources() 
	throws WorkflowExecutionException, IOException {
		final WorkflowExecutionContext workflowContext = executeWithContext("load_aadl_resources_osek", createWorkflowProperties());
		
		assertFalse(workflowContext.getModelSlots().isEmpty());
	}
	
	@Test
	public void testLoadTransformationResources() 
	throws WorkflowExecutionException, IOException {
		final WorkflowExecutionContext workflowContext = executeWithContext("load_transformation_resources_osek", createWorkflowProperties());
		
		assertFalse(workflowContext.getModelSlots().isEmpty());
	}

	@Override
	protected String getSourcesProjectName() {
		return SOURCES_PROJECT_NAME;
	}
	
	@Override
	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.ramses.osek" ;
	}

	@Override
	protected String[] getWorkflowDir() {
		String[] res = {"ramses", "osek", "workflows"} ;
		return res;
	}
}
