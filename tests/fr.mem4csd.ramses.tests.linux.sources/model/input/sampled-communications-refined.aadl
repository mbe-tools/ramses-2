package refined_model_main_posix_Instance_Linux
public
	with Base_Types;
	with AADL_Runtime;
	with sampled_communications;
	with Data_Model;
	with RAMSES_properties;
	with Memory_Properties;
	with Programming_Properties;
	with Thread_Properties;
	with Timing_Properties;
	with Deployment_Properties;
	with Code_Generation_Properties;

	system implementation refined_model.impl
		subcomponents
			the_proc: process the_proc.impl;
			the_cpu: processor sampled_communications::cpu.impl {RAMSES_properties::Target => "linux";
				Scheduling_Protocol => (RMS);};
			the_mem: memory sampled_communications::mem.impl {Memory_Size => 200000 Bytes;};
		properties
			Actual_Processor_Binding => (reference (the_cpu)) applies to the_proc;
			Actual_Memory_Binding => (reference (the_mem)) applies to the_proc;
			Actual_Processor_Binding => (reference (the_cpu)) applies to the_proc.the_sender;
			Actual_Memory_Binding => (reference (the_mem)) applies to the_proc.the_sender;
			Actual_Processor_Binding => (reference (the_cpu)) applies to the_proc.the_receiver;
			Actual_Memory_Binding => (reference (the_mem)) applies to the_proc.the_receiver;
	end refined_model.impl;

	system refined_model
	end refined_model;

	process the_proc
	end the_proc;

	process implementation the_proc.impl
		subcomponents
			the_sender: thread the_proc_the_sender.impl {Dispatch_Protocol => Periodic;
				Compute_Execution_Time => 0ms .. 1ms; Period => 2000ms; Priority => 5; Data_Size => 40000 Bytes;
				Stack_Size => 512 Bytes; Code_Size => 40 Bytes;
				Compute_Entrypoint_Call_Sequence => reference (main_call);};
			the_receiver: thread the_proc_the_receiver.impl {Dispatch_Protocol => Periodic;
				Compute_Execution_Time => 0ms .. 1ms; Period => 1000ms; Priority => 10; Data_Size => 40000 Bytes;
				Stack_Size => 512 Bytes; Code_Size => 40 Bytes;
				Compute_Entrypoint_Call_Sequence => reference (main_call);};
			the_sender_context: data sampled_communications_sampled_communications_public_sender_context_t {
				Data_Model::Initial_Value => ("{&CUTsender_p_port_ref, &the_sender_config}");};
			the_receiver_context: data sampled_communications_sampled_communications_public_receiver_context_t {
				Data_Model::Initial_Value => ("{&CUTreceiver_p_port_ref, &the_receiver_config}");};
			CUTsender_p_port_ref: data AADL_Runtime::PortReferenceType {Data_Model::Initial_Value => (
					"{DATA, OUT, THREAD, &the_sender_config,&CUTsender_p_output_port, sizeof(sampled_communications__Integer)}");};
			CUTsender_p_output_port: data AADL_Runtime::ThreadOutputPortType {Data_Model::Initial_Value => (
					"{&CUTsender_p_port_variable, 0, 1, 1,CUTsender_p_dest_index_by_mode,CUTsender_p_cnxs_reference}");};
			CUTsender_p_cnxs_reference: data the_proc_the_sender_p_src_port_reference_array {
				Data_Model::Initial_Value => ("{&intraprocess_cnx_CUTsender_p_TO_CUTreceiver_p}");};
			CUTsender_p_port_variable: data sampled_communications::Integer;
			CUTsender_p_dest_index_by_mode: data the_proc_the_sender_p_src_port_reference_by_mode_array {
				Data_Model::Initial_Value => ("{1,1}");};
			intraprocess_cnx_CUTsender_p_TO_CUTreceiver_p: data AADL_Runtime::PortConnectionType {
				Data_Model::Initial_Value => (
					"{&CUTsender_p_port_ref,&CUTreceiver_p_port_ref,&intraprocess_cnx_protocol_CUTsender_p_TO_CUTreceiver_p}");};
			intraprocess_cnx_protocol_CUTsender_p_TO_CUTreceiver_p: data AADL_Runtime::ConnectionProtocolType {
				Data_Model::Initial_Value => ("{SAMPLED,NULL}");};
			CUTreceiver_p_port_ref: data AADL_Runtime::PortReferenceType {Data_Model::Initial_Value => (
					"{DATA, IN, THREAD,&the_receiver_config,&CUTreceiver_p_in_port, sizeof(sampled_communications__Integer)}");};
			CUTreceiver_p_src_index_by_mode: data the_proc_the_receiver_p_src_index_by_mode_t {
				Data_Model::Initial_Value => ("{0}");};
			CUTreceiver_p_data_rw_per_mode: data the_proc_the_receiver_p_data_rw_per_mode_t {
				Data_Model::Initial_Value => (
					"{{&CUTsender_p_CUTreceiver_p_value_read, &CUTsender_p_CUTreceiver_p_value_write}}");};
			CUTreceiver_p_in_port: data AADL_Runtime::ThreadInputPortType {Data_Model::Initial_Value => (
					"{&the_receiver_globalQueue,&CUTreceiver_p_port_ref,&CUTreceiver_p_data_port,1,CUTreceiver_p_cnxs_reference,0,0}");};
			CUTreceiver_p_cnxs_reference: data the_proc_the_receiver_p_src_port_reference_array {
				Data_Model::Initial_Value => ("{&intraprocess_cnx_CUTsender_p_TO_CUTreceiver_p}");};
			CUTreceiver_p_data_port: data AADL_Runtime::DataPortType {Data_Model::Initial_Value => (
					"{1, CUTreceiver_p_src_index_by_mode, CUTreceiver_p_data_rw_per_mode}");};
			CUTsender_p_CUTreceiver_p_value_read: data sampled_communications::Integer;
			CUTsender_p_CUTreceiver_p_value_write: data sampled_communications::Integer;
			the_sender_config: data AADL_Runtime::ThreadConfig {Data_Model::Initial_Value => (
					"{PERIODIC, NULL, &the_sender_config_periodic, 2.0E9, 0,5, 2.0E9, 1,{0},&the_sender_tid, &the_sender_mode_event, 1, the_sender_output_ports_reference,0, NULL,NULL,0,NULL,0,0, NULL, NULL,NULL,NULL,NULL,0,0,&the_sender_watchdog}");};
			the_sender_tid: data AADL_Runtime::TargetThreadType;
			the_sender_mode_event: data AADL_Runtime::TargetWaitModeType;
			the_sender_config_periodic: data AADL_Runtime::PeriodicThreadConfig;
			the_sender_watchdog: data AADL_Runtime::TargetWatchDogType;
			the_sender_output_ports_reference: data the_proc_the_sender_output_port_reference_array_t {
				Data_Model::Initial_Value => ("{&CUTsender_p_port_ref}");};
			the_receiver_config: data AADL_Runtime::ThreadConfig {Data_Model::Initial_Value => (
					"{PERIODIC, &the_receiver_globalQueue, &the_receiver_config_periodic, 1.0E9, 0,10, 1.0E9, 1,{0},&the_receiver_tid, &the_receiver_mode_event, 0, NULL,1, the_receiver_input_ports_reference,NULL,0,NULL,0,0, NULL, NULL,NULL,NULL,NULL,0,0,&the_receiver_watchdog}");};
			the_receiver_tid: data AADL_Runtime::TargetThreadType;
			the_receiver_mode_event: data AADL_Runtime::TargetWaitModeType;
			the_receiver_config_periodic: data AADL_Runtime::PeriodicThreadConfig;
			the_receiver_globalQueue_evt: data AADL_Runtime::TargetWaitMessageType;
			the_receiver_watchdog: data AADL_Runtime::TargetWatchDogType;
			the_receiver_input_ports_reference: data the_proc_the_receiver_input_ports_array_t {
				Data_Model::Initial_Value => ("{&CUTreceiver_p_port_ref}");};
			the_receiver_globalQueue: data AADL_Runtime::PortListType {Data_Model::Initial_Value => (
					"{&the_receiver_config,0, 0, 0, 1, the_receiver_input_port_reference_array,&the_receiver_globalQueue_evt}");};
			the_receiver_input_port_reference_array: data the_proc_the_receiver_input_port_reference_array_t {
				Data_Model::Initial_Value => ("{&CUTreceiver_p_port_ref}");};
			this_process: data AADL_Runtime::ProcessConfig {Data_Model::Initial_Value => (
					"{{&the_sender_config,&the_receiver_config},1,{0},2000000000}");};
			thread_number_in_all_modes: data Base_Types::Unsigned_16 {Data_Model::Initial_Value => ("2");};
			thread_configs_in_all_modes: data the_proc_thread_configs_all_mode_t {Data_Model::Initial_Value => (
					"{&the_sender_config,&the_receiver_config}");};
			thread_number_per_mode: data the_proc_nb_threads_per_mode_t {Data_Model::Initial_Value => ("{}");};
			thread_configs_per_mode: data the_proc_thread_config_array_per_mode_t {Data_Model::Initial_Value => (
					"{}");};
			port_to_process: data the_proc_ports_to_process_t {Data_Model::Initial_Value => ("{}");};
			process_to_node: data the_proc_processes_to_nodes_t {Data_Model::Initial_Value => ("{0}");};
			nb_dst_per_port: data the_proc_nb_dst_array_t {Data_Model::Initial_Value => ("{}");};
			global_to_local: data the_proc_global_to_local_t;
		connections
			the_sender_to_ThreadConfiguration: data access the_sender.the_sender_context -> the_sender_context;
			the_receiver_to_ThreadConfiguration: data access the_receiver.the_receiver_context -> the_receiver_context;
	end the_proc.impl;

	data the_proc_thread_configs_all_mode_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (AADL_Runtime::ThreadConfigAddr));
			Data_Model::Dimension => (2);
	end the_proc_thread_configs_all_mode_t;

	data the_proc_thread_config_array_per_mode_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (AADL_Runtime::ThreadConfigAddrArray));
			Data_Model::Dimension => (0);
	end the_proc_thread_config_array_per_mode_t;

	data the_proc_nb_threads_per_mode_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (Base_Types::Unsigned_16));
			Data_Model::Dimension => (0);
	end the_proc_nb_threads_per_mode_t;

	data the_proc_nb_dst_array_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (Base_Types::Integer_16));
			Data_Model::Dimension => (0);
	end the_proc_nb_dst_array_t;

	data the_proc_ports_to_process_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (Base_Types::Integer_16));
			Data_Model::Dimension => (0);
	end the_proc_ports_to_process_t;

	data the_proc_processes_to_nodes_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (Base_Types::Integer_16));
			Data_Model::Dimension => (1);
	end the_proc_processes_to_nodes_t;

	data the_proc_the_sender_p_src_port_reference_array
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (AADL_Runtime::PortConnectionAddr));
			Data_Model::Dimension => (1);
	end the_proc_the_sender_p_src_port_reference_array;

	data the_proc_the_sender_p_src_port_reference_by_mode_array
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (Base_Types::Integer_16));
			Data_Model::Dimension => (2);
	end the_proc_the_sender_p_src_port_reference_by_mode_array;

	data the_proc_the_receiver_p_src_index_by_mode_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (Base_Types::Integer_16));
			Data_Model::Dimension => (1);
	end the_proc_the_receiver_p_src_index_by_mode_t;

	data the_proc_the_receiver_p_data_rw_per_mode_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (AADL_Runtime::DoubleBufferType));
			Data_Model::Dimension => (1);
	end the_proc_the_receiver_p_data_rw_per_mode_t;

	data the_proc_the_receiver_p_src_port_reference_array
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (AADL_Runtime::PortConnectionAddr));
			Data_Model::Dimension => (1);
	end the_proc_the_receiver_p_src_port_reference_array;

	subprogram the_proc_the_receiver_entrypoint
		features
			the_receiver_context: requires data access sampled_communications_sampled_communications_public_receiver_context_t;
	end the_proc_the_receiver_entrypoint;

	subprogram implementation the_proc_the_receiver_entrypoint.impl
		subcomponents
			the_receiver_runtime_call_ret: data AADL_Runtime::RuntimeReturnCode {Data_Model::Initial_Value => ("0");};
			p_localVariable: data sampled_communications::Integer;
			runtime_call_error_detected: data Base_Types::Boolean {Data_Model::Initial_Value => ("0");};
		annex behavior_specification {**
          variables
            whichPortActivated : refined_model_main_posix_Instance_Linux::the_proc_the_receiver_behaviorIdentifier_enum;
          states
            BA_entrypoint_init_state : initial state;
            BA_entrypoint_wait_dispatch_state : state;
            BA_entrypoint_exec_state : state;
            BA_entrypoint_exec_prologue_state : state;
            BA_entrypoint_final_state : final state;
          transitions
            call [0] : BA_entrypoint_exec_state -[whichPortActivated = the_proc_the_receiver_behaviorIdentifier_enum#Enumerators.the_receiver_behaviorIdentifier_enum_default_behavior]-> BA_entrypoint_wait_dispatch_state {
              AADL_Runtime::GetValue ! (the_receiver_context.p, p_localVariable, the_receiver_runtime_call_ret);
              runtime_call_error_detected := runtime_call_error_detected or the_receiver_runtime_call_ret != 0;
              if (runtime_call_error_detected = 0)
                sampled_communications::receiver_spg ! (p_localVariable)
              end if
            };
            BA_entrypoint_exec_state -[otherwise]-> BA_entrypoint_wait_dispatch_state;
            which_behavior_default_mode : BA_entrypoint_init_state -[]-> BA_entrypoint_exec_prologue_state {
              whichPortActivated := the_proc_the_receiver_behaviorIdentifier_enum#Enumerators.the_receiver_behaviorIdentifier_enum_default_behavior;
              AADL_Runtime::AwaitMode ! (the_receiver_context.config);
              runtime_call_error_detected := 0
            };
            dispatch_transition : BA_entrypoint_wait_dispatch_state -[]-> BA_entrypoint_exec_prologue_state {
              if (the_receiver_runtime_call_ret != 0)
                AADL_Runtime::ErrorHandler ! (the_receiver_context.config, the_receiver_runtime_call_ret)
              end if;
              AADL_Runtime::AwaitDispatch ! (the_receiver_context.config);
              runtime_call_error_detected := 0
            };
            prologue_transition : BA_entrypoint_exec_prologue_state -[]-> BA_entrypoint_exec_state {
              AADL_Runtime::ReceiveInputThread ! (the_receiver_context.config, the_receiver_runtime_call_ret)
            };
        **};
	end the_proc_the_receiver_entrypoint.impl;

	thread implementation the_proc_the_receiver.impl
		calls
			main_call: {
				call_entrypoint: subprogram the_proc_the_receiver_entrypoint.impl;
			};
		connections
			the_receiver_to_ThreadConfiguration: data access call_entrypoint.the_receiver_context -> the_receiver_context;
	end the_proc_the_receiver.impl;

	thread the_proc_the_receiver
		features
			the_receiver_context: requires data access sampled_communications_sampled_communications_public_receiver_context_t;
	end the_proc_the_receiver;

	data the_proc_the_receiver_input_ports_array_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (AADL_Runtime::PortReferenceAddr));
			Data_Model::Dimension => (1);
	end the_proc_the_receiver_input_ports_array_t;

	data the_proc_the_receiver_input_port_reference_array_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (AADL_Runtime::PortReferenceAddr));
			Data_Model::Dimension => (1);
	end the_proc_the_receiver_input_port_reference_array_t;

	subprogram the_proc_the_sender_entrypoint
		features
			the_sender_context: requires data access sampled_communications_sampled_communications_public_sender_context_t;
	end the_proc_the_sender_entrypoint;

	subprogram implementation the_proc_the_sender_entrypoint.impl
		subcomponents
			the_sender_runtime_call_ret: data AADL_Runtime::RuntimeReturnCode {Data_Model::Initial_Value => ("0");};
			sampled_communications_sender_spg_result_localVariable: data sampled_communications::Integer;
			runtime_call_error_detected: data Base_Types::Boolean {Data_Model::Initial_Value => ("0");};
		annex behavior_specification {**
          variables
            whichPortActivated : refined_model_main_posix_Instance_Linux::the_proc_the_sender_behaviorIdentifier_enum;
          states
            BA_entrypoint_init_state : initial state;
            BA_entrypoint_wait_dispatch_state : state;
            BA_entrypoint_exec_state : state;
            BA_entrypoint_exec_prologue_state : state;
            BA_entrypoint_final_state : final state;
          transitions
            call [0] : BA_entrypoint_exec_state -[whichPortActivated = the_proc_the_sender_behaviorIdentifier_enum#Enumerators.the_sender_behaviorIdentifier_enum_default_behavior]-> BA_entrypoint_wait_dispatch_state {
              sampled_communications::sender_spg ! (sampled_communications_sender_spg_result_localVariable);
              AADL_Runtime::PutValue ! (the_sender_context.p, sampled_communications_sender_spg_result_localVariable, the_sender_runtime_call_ret)
            };
            BA_entrypoint_exec_state -[otherwise]-> BA_entrypoint_wait_dispatch_state;
            which_behavior_default_mode : BA_entrypoint_init_state -[]-> BA_entrypoint_exec_prologue_state {
              whichPortActivated := the_proc_the_sender_behaviorIdentifier_enum#Enumerators.the_sender_behaviorIdentifier_enum_default_behavior;
              AADL_Runtime::AwaitMode ! (the_sender_context.config)
            };
            dispatch_transition : BA_entrypoint_wait_dispatch_state -[]-> BA_entrypoint_exec_prologue_state {
              AADL_Runtime::SendOutputThread ! (the_sender_context.config, the_sender_runtime_call_ret);
              if (the_sender_runtime_call_ret != 0)
                AADL_Runtime::ErrorHandler ! (the_sender_context.config, the_sender_runtime_call_ret)
              end if;
              AADL_Runtime::AwaitDispatch ! (the_sender_context.config)
            };
            prologue_transition : BA_entrypoint_exec_prologue_state -[]-> BA_entrypoint_exec_state;
        **};
	end the_proc_the_sender_entrypoint.impl;

	thread implementation the_proc_the_sender.impl
		calls
			main_call: {
				call_entrypoint: subprogram the_proc_the_sender_entrypoint.impl;
			};
		connections
			the_sender_to_ThreadConfiguration: data access call_entrypoint.the_sender_context -> the_sender_context;
	end the_proc_the_sender.impl;

	thread the_proc_the_sender
		features
			the_sender_context: requires data access sampled_communications_sampled_communications_public_sender_context_t;
	end the_proc_the_sender;

	data the_proc_the_sender_output_port_reference_array_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (AADL_Runtime::PortReferenceAddr));
			Data_Model::Dimension => (1);
	end the_proc_the_sender_output_port_reference_array_t;

	data the_proc_global_to_local_t
		properties
			Data_Model::Data_Representation => Array;
			Data_Model::Base_Type => (classifier (AADL_Runtime::InputProcessorPortAddr));
			Data_Model::Dimension => (1);
	end the_proc_global_to_local_t;

	data sampled_communications_sampled_communications_public_receiver_context_t
		properties
			Data_Model::Data_Representation => Struct;
			Data_Model::Element_Names => ("p", "config");
			Data_Model::Base_Type => (classifier (AADL_Runtime::PortReferenceAddr),
				classifier (AADL_Runtime::ThreadConfigAddr));
			Source_Name => "__sampled_communications_separ_receiver_context";
	end sampled_communications_sampled_communications_public_receiver_context_t;

	data the_proc_the_receiver_behaviorIdentifier_enum
		properties
			Data_Model::Data_Representation => Enum;
			Data_Model::Enumerators => ("the_receiver_behaviorIdentifier_enum_default_behavior",
				"the_receiver_behaviorIdentifier_enum_p");
			Data_Model::Initial_Value => ("the_receiver_behaviorIdentifier_enum_default_behavior");
	end the_proc_the_receiver_behaviorIdentifier_enum;

	data sampled_communications_sampled_communications_public_sender_context_t
		properties
			Data_Model::Data_Representation => Struct;
			Data_Model::Element_Names => ("p", "config");
			Data_Model::Base_Type => (classifier (AADL_Runtime::PortReferenceAddr),
				classifier (AADL_Runtime::ThreadConfigAddr));
			Source_Name => "__sampled_communications_separ_sender_context";
	end sampled_communications_sampled_communications_public_sender_context_t;

	data the_proc_the_sender_behaviorIdentifier_enum
		properties
			Data_Model::Data_Representation => Enum;
			Data_Model::Enumerators => ("the_sender_behaviorIdentifier_enum_default_behavior");
			Data_Model::Initial_Value => ("the_sender_behaviorIdentifier_enum_default_behavior");
	end the_proc_the_sender_behaviorIdentifier_enum;
end refined_model_main_posix_Instance_Linux;