/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifdef USE_POSIX
#include <stdio.h>
#include <stdlib.h>
#endif

#ifdef USE_POK
#include <libc/stdlib.h>
#endif

#if !defined USE_POSIX && !defined USE_POK
#include "ecrobot_interface.h"
#endif

#include "user_send.h"
#include "aadl_runtime_services.h"

int event = 1;
int value = 1;
mode_id_t previous_mode = 1;

void check_event()
{
	if(event>5)
	{
#ifdef USE_POSIX
		exit(0);
#endif
#ifdef USE_POK
		STOP_SELF();
#endif
	}
}

void send_num(__send_num_context * ctx)
{
  check_event();
  mode_id_t current_mode = 3;
  Current_System_Mode(&current_mode);

  if (current_mode != previous_mode)
  {
    previous_mode = current_mode;
    value = (value % 100) + (current_mode + 1) * 100;
  }

  int test = Put_Value(ctx->result, &value);
#if defined(USE_POSIX) || defined(USE_POK)
  if(test == 0) {
    printf("sent %d\n", value);
  }
#else
  ecrobot_debug1(0, 0, mes);
#endif
  value++;
}

void send(__send_context* ctx)
{
  check_event();
  if(event%1==0)
  {
	  Put_Value(ctx->result, NULL);
#if defined(USE_POSIX) || defined(USE_POK)
	  printf("sent event to change mode, %d\n", event);
#endif
  }
  event++;
}
