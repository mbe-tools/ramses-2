/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifdef USE_POSIX
#include <stdio.h>
#include <stdlib.h>
#endif

#ifdef USE_POK
#include <libc/stdlib.h>
#endif

#ifdef USE_OSEK
#include "ecrobot_interface.h"
#endif

#include "user_receive.h"

void receive(int input_data)
{
  mode_id_t current_mode = 3;
  Current_System_Mode(&current_mode);
#if defined(USE_POSIX) || defined(USE_POK)
  printf("received value %d in mode %u\n", input_data, current_mode);
#else
  ecrobot_debug1(input_data, 0, 0);
#endif
}
