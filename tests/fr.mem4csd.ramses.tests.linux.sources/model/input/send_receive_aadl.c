/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "send_receive_aadl.h"
#include "aadl_runtime_services.h"

#include <stdlib.h>

uint8_t counter = 1;

void periodic_send(__periodic_send_context *  context)
{
  for(int i=0; i<counter; i++)
  {
     Put_Value(context->p, &counter);
     Send_Output(context->p);
     if(counter>0 && counter<9)
       printf("sent value %d\n", counter);
  }
  counter++;
  if(counter>9)
    exit(0);
}

void periodic_receive(__periodic_receive_context *  context)
{
  uint8_t rec;
  uint16_t count = 0;
  Get_Count(context->p, &count);
  while(count)
  {
    Get_Value(context->p, &rec);
    Next_Value(context->p);
    Get_Count(context->p, &count);
    if(rec>0 && rec<9)
      printf("received value %d, remaining messages count %d\n", rec, count);
  }
}

