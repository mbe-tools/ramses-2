/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.nxtosek;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mdelab.workflow.Workflow;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.WorkflowramsesnxtosekPackage;
import fr.mem4csd.ramses.tests.core.AbstractRamsesPluginTest;
import fr.mem4csd.ramses.tests.core.utils.ReportComparator;

public class TestRamsesNxtOsekExtension extends AbstractRamsesPluginTest {
	
	private static final URI NXTOSEK_RUNTIME_DIR_URI = URI.createURI( System.getenv("HOME")+"/ramses-resources/nxtOSEK", true );
	private static final String SYSTEM_IMPL_NAME = "main.osek";
	private static final String SOURCES_PROJECT_NAME = "fr.mem4csd.ramses.tests.nxtosek.sources";
	
	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		cleanResources( SOURCES_PROJECT_NAME );

		if (!Platform.isRunning()) {
			WorkflowramsesnxtosekPackage.eINSTANCE.eClass();
		}
	}
	
	@Test
	public void testValidateSampledCommunications() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();
		props.put( "system_implementation_name", SYSTEM_IMPL_NAME );
		props.put( "source_name", "SAMPLED-COMMUNICATIONS" );
		
		final String aadlFile;
		final String reportFile;
		final String reportFileComp;
		final String reportFileRef;
		final String instanceModelFile;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		final String outputDir;
		
		if ( Platform.isRunning() ) {
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			aadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			reportFile = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_URI, true ).toString();
			reportFileComp = reportFile;
			reportFileRef = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_URI_REF, true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
		}
		else {
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			aadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_URI;
			reportFileComp = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_URI;
			reportFileRef = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_URI_REF;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
		}

		props.put("source_aadl_file", aadlFile);
		props.put("validation_report_file", reportFile);
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_FILE_NAME);
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.nxtosek/ramses/nxtosek/workflows/default_nxtosek.properties");

		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_validate_core", props );
		assertFalse(workflowContext.getModelSlots().isEmpty());

		ReportComparator cmp = new ReportComparator(reportFileComp, reportFileRef, getResourceSet());
		boolean results = cmp.checkResults();

		assertTrue(results);
	}
	
	@Test
	public void testValidateSampledCommunicationsInvalid() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();
		props.put( "system_implementation_name", SYSTEM_IMPL_NAME );
		props.put( "source_name", "SAMPLED-COMMUNICATIONS-INVALID" );
		
		final String aadlFile;
		final String reportFile;
		final String reportFileComp;
		final String reportFileRef;
		final String instanceModelFile;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		final String outputDir;
		
		if ( Platform.isRunning() ) {
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			aadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_INVALID_URI.toString(), true ).toString();
			reportFile = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_INVALID_URI, true ).toString();
			reportFileComp = reportFile;
			reportFileRef = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_INVALID_URI_REF, true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
		}
		else {
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			aadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_INVALID_URI;
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI;
			reportFileComp = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI;
			reportFileRef = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI_REF;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
		}

		props.put("source_aadl_file", aadlFile);
		props.put("validation_report_file", reportFile);

		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_INVALID_FILE_NAME);
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.nxtosek/ramses/nxtosek/workflows/default_nxtosek.properties");

		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		try
		{
			final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_validate_core", props );
			assertFalse(workflowContext.getModelSlots().isEmpty());
		}
		catch (WorkflowExecutionException ex)
		{
			// do nothing
		}

		final ReportComparator cmp = new ReportComparator(reportFileComp, reportFileRef, getResourceSet());
		boolean results = cmp.checkResults();
		assertTrue(results);
	}
	
	@Test
	public void testNxtOSEKSampledCommunicationsExecution() 
			throws WorkflowExecutionException, IOException, InterruptedException, ExecutionException, TimeoutException {
		
		
		executeNxtOSEKTest(AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME, "the_cpu", "the_proc");
	}
	
	@Test
	public void testNxtOSEKSporadicExecution() 
			throws WorkflowExecutionException, IOException, InterruptedException, ExecutionException, TimeoutException {
		
		
		executeNxtOSEKTest(AADL_SPRORADIC_URI, "root.impl", "the_cpu", "the_proc");
	}
	
	private void executeNxtOSEKTest(URI AADL_SAMPLED_COM_URI, String SYSTEM_IMPL_NAME, String processorName, String processName) throws WorkflowExecutionException, IOException
	{
		final Map<String, String> props = createWorkflowProperties();
		
		final String srcAadlFile;
		final String includeDir;
		final String outputDir;
		final String instanceModelFile;
//		final String reportFile;
//		final String refinedAadlFile;
		final String runtimeDir;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		
		if (Platform.isRunning()) {
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
//			reportFile = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_INVALID_URI, true ).toString();
//			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI, true ).toString();
			runtimeDir = URI.createPlatformResourceURI( NXTOSEK_RUNTIME_DIR_URI.toString(), true ).toString();
			
		} else {
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			includeDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.toString();
//			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI;
//			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;
			runtimeDir = NXTOSEK_RUNTIME_DIR_URI.toString();			
		}
		
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );

//		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );

		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_SAMPLED_COM_FILE_NAME );
//		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile);
		props.put( RamsesWorkflowKeys.TARGET_INSTALL_DIR, runtimeDir );

		final WorkflowExecutionContext workflowContext = executeWithContext( "load_instanciate_validate_refine_and_generate_nxtosek", props );

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment(processorName)
					.appendSegment(processName)
					.appendSegment(processName+"_OSEK.rxe") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment(processorName)
					.appendSegment(processName)
					.appendSegment(processName+"_OSEK.rxe");
		}
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
	}
	
	@Test
	public void testNxtOSEKRefinementSampledCommunications() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();
		
		final String srcAadlFile;
		final String instanceModelFile;
		final String refinedAadlFile;
		final String refinedAadlFileComp;
		final String refinedAadlFileRef;

		final String refinedTraceFile;
		final String traceFileComp;
		final String traceFileRef;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );

		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI, true ).toString();
			refinedAadlFileComp = refinedAadlFile;
			refinedAadlFileRef = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI_REF, true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_URI, true ).toString();
			traceFileComp = refinedTraceFile;
			traceFileRef = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_URI_REF, true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;
			refinedAadlFileComp = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;
			refinedAadlFileRef = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI_REF;

			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI;
			traceFileComp = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI;
			traceFileRef = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI_REF;
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_FILE_NAME);
		
		
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.nxtosek/ramses/nxtosek/workflows/default_nxtosek.properties");
		
		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_refine_core", props );		
		assertFalse(workflowContext.getModelSlots().isEmpty());

		
		final ReportComparator cmpAadl = new ReportComparator(refinedAadlFileComp, refinedAadlFileRef, getResourceSet());
		assertTrue( cmpAadl.checkResults() );

		final ReportComparator cmpTrace = new ReportComparator(traceFileComp, traceFileRef, getResourceSet());
		assertTrue( cmpTrace.checkResults() );
	}
	
	@Test
	public void testNxtOSEKCodegenExecution()
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();
		
		WorkflowramsesnxtosekPackage.eINSTANCE.eClass();
		
		final String refinedAadlFile;
		final String instanceModelFile;
		final String srcAadlFile;
		final String refinedTraceFile;
		final String outputDir;
		final String runtimeDir;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		
		if (Platform.isRunning()) {
			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_IN_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_IN_URI, true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			runtimeDir = URI.createPlatformResourceURI( NXTOSEK_RUNTIME_DIR_URI.toString(), true ).toString();
		}
		else {
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_IN_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_IN_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			runtimeDir = NXTOSEK_RUNTIME_DIR_URI.toString();
		}

		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, null );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile);
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile);
		
		props.put( RamsesWorkflowKeys.TARGET_INSTALL_DIR, runtimeDir );
		
		final WorkflowExecutionContext workflowContext = executeWithContext("load_and_generate_nxtosek", props );		
		
		final String codeGenDir;
		
		if (Platform.isRunning()) {
			codeGenDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			codeGenDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}
		
		URI mainFileURI = null;
		if (Platform.isRunning()) {
			mainFileURI = CommonPlugin.resolve( URI.createFileURI( codeGenDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("main.c") );
		} else {
			mainFileURI = URI.createURI( codeGenDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("main.c");
		}
		File mainFile = new File( mainFileURI.toFileString() );
		
		assertTrue(mainFile.exists());
		
		assertFalse(workflowContext.getModelSlots().isEmpty());
	}

	@Override
	protected String getSourcesProjectName() {
		return SOURCES_PROJECT_NAME;
	}
	
	@Override
	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.ramses.nxtosek" ;
	}

	@Override
	protected String[] getWorkflowDir() {
		String[] res = {"ramses", "nxtosek", "workflows"} ;
		return res;
	}
	
	@Override
	protected String getTargetName()
	{
		return "nxtosek";
	}
}
