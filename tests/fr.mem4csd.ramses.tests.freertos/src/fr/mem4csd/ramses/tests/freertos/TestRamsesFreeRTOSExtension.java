package fr.mem4csd.ramses.tests.freertos;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.mem4csd.ramses.freertos.workflowramsesfreertos.WorkflowramsesfreertosPackage;
import fr.mem4csd.ramses.tests.core.utils.ReportComparator;

public class TestRamsesFreeRTOSExtension extends AbstractRamsesFreeRTOSExtensionTest{
	
	private static final String SYSTEM_IMPL_NAME = "main.freertos";
	private static final String SOURCES_PROJECT_NAME = "fr.mem4csd.ramses.tests.freertos.sources";
	
	@Override
	protected boolean executeAsSudo()
	{
		return true;
	}
	
	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		cleanResources( SOURCES_PROJECT_NAME );

		if (!Platform.isRunning()) {
			WorkflowramsesfreertosPackage.eINSTANCE.eClass();
		}
	}
		
	@Test
	public void testLoadTransformationResources() 
	throws WorkflowExecutionException, IOException {
		final WorkflowExecutionContext workflowContext = executeWithContext("load_transformation_resources_freertos", createWorkflowProperties());
		
		assertFalse(workflowContext.getModelSlots().isEmpty());
	}
	
	@Test
	public void testValidateSampledCommunications() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();
		
		final String aadlFile;
		final String reportFile;
		final String reportFileRef;
		final String instanceModelFile;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );

		if ( Platform.isRunning() ) {
			aadlFile = null;
			instanceModelFile = null;
			reportFile = null;
			reportFileRef = null;
		}
		else {
			aadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_URI;
			reportFileRef = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_URI_REF;
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, aadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_FILE_NAME);
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.freertos/ramses/freertos/workflows/default_freertos.properties");

		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_validate_core", props );		
		
		assertFalse(workflowContext.getModelSlots().isEmpty());
		
		ReportComparator cmp = new ReportComparator( reportFileRef, reportFile, getResourceSet());
		boolean results = cmp.checkResults();

		assertTrue(results);
	}
	
//	@Test
//	public void testValidateSampledCommunicationsInvalid() 
//	throws WorkflowExecutionException, IOException {
//		final Map<String, String> props = createWorkflowProperties();
//		
//		final String aadlFile;
//		final String instanceModelFile;
//		final String reportFile;
//		final String reportFileRef;
//		
//		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
//
//		if ( Platform.isRunning() ) {
//			aadlFile = null;
//			instanceModelFile = null;
//			reportFile = null;
//			reportFileRef = null;
//		}
//		else {
//			aadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_INVALID_URI;
//			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
//			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI;
//			reportFileRef = TEST_SOURCES_ABS_LOCATION+ REPORT_SAMPLED_COM_INVALID_URI_REF;
//		}
//		
//		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
//		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, aadlFile );
//		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
//		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile );
//		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_INVALID_FILE_NAME);
//
//		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.freertos/ramses/freertos/workflows/default_freertos.properties");
//
//		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
//		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_validate_core", props );		
//				
//		assertFalse(workflowContext.getModelSlots().isEmpty());
//		
//		final ReportComparator cmp = new ReportComparator( reportFileRef, reportFile, getResourceSet());
//		boolean results = cmp.checkResults();
//		assertTrue(results);
//	}
//	
	@Test
	public void testRefinementSampledCommunications() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String srcAadlFile;
		final String instanceModelFile;
		final String refinedAadlFile;
		final String refinedAadlFileRef;

		final String traceFile;
		final String traceFileRef;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );

		if (Platform.isRunning()) {
			srcAadlFile = null;
			instanceModelFile = null;
			refinedAadlFile = null;
			refinedAadlFileRef = null;
			traceFile = null;
			traceFileRef = null;
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;
			refinedAadlFileRef = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI_REF;
			traceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI;
			traceFileRef = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI_REF;
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, traceFile );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_FILE_NAME);
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.freertos/ramses/freertos/workflows/default_freertos.properties");
		
		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_refine_core", props );		
		assertFalse(workflowContext.getModelSlots().isEmpty());

		final ReportComparator cmpTrace = new ReportComparator( traceFileRef, traceFile, getResourceSet() );
		assertTrue( cmpTrace.checkResults() );
		
		final ReportComparator cmpAadl = new ReportComparator( refinedAadlFileRef, refinedAadlFile, getResourceSet() );
		assertTrue( cmpAadl.checkResults() );
	}
	
	@Test
	public void testCodegenExecution()
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String refinedAadlFile;
		final String instanceModelFile;
		final String srcAadlFile;
		final String refinedTraceFile;
		final String outputDir;
		final String includeDir;
		final String installDir;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		
		if (Platform.isRunning()) {
			refinedAadlFile = null;
			instanceModelFile = null;
			srcAadlFile = null;
			refinedTraceFile = null;
			outputDir = null;
			includeDir = null;
			installDir = URI.createPlatformResourceURI( FREERTOS_INSTALL_DIR_URI.toString(), true ).toString();
		}
		else {
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_IN_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_IN_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			includeDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.toString();
			installDir = FREERTOS_INSTALL_DIR_URI.toString();
		}

		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.TARGET_INSTALL_DIR, installDir );

		final WorkflowExecutionContext workflowContext = executeWithContext("load_and_generate_freertos", props );		
		
		final String codeGenDir;
		
		if (Platform.isRunning()) {
			codeGenDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			codeGenDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}
		
		URI mainFileURI = null;
		if (Platform.isRunning()) {
			mainFileURI = CommonPlugin.resolve( URI.createFileURI( codeGenDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("main.c") );
		} else {
			mainFileURI = URI.createURI( codeGenDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("main.c");
		}
		File mainFile = new File( mainFileURI.toFileString() );
		
		assertTrue(mainFile.exists());
		
		assertFalse(workflowContext.getModelSlots().isEmpty());
	}
	
	@Test
	public void testCodegenCompilation()
			throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String workingDir;
		final String installDir;
		
		if (Platform.isRunning()) {
			workingDir = null;
			installDir = null;
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
			installDir = FREERTOS_INSTALL_DIR_URI.toString();
		}
		
		props.put( RamsesWorkflowKeys.WORKING_DIR, workingDir );
		props.put( RamsesWorkflowKeys.TARGET_INSTALL_DIR, installDir );

		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "compile_codegen_core", props );

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = null;
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc");
		}
		
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
	}
	
	@Test
	public void testFreeRTOSExecution() 
			throws WorkflowExecutionException, IOException, InterruptedException, ExecutionException, TimeoutException {
		
		final Map<String, String> props = createWorkflowProperties();
		
		final String srcAadlFile;		
//		final String refinedAadlFile;
//		final String refinedTraceFile;
		final String includeDir;
		final String outputDir;
		final String instanceModelFile;
//		final String reportFile;
		final String installDir;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		
		if (Platform.isRunning()) {
//			refinedAadlFile = null;
			instanceModelFile = null;
			srcAadlFile = null;
//			refinedTraceFile = null;
			outputDir = null;
			includeDir = null;
//			reportFile = null;
			installDir = null;
		} else {
//			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_IN_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
//			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_IN_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			includeDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.toString();
//			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI;
			installDir = FREERTOS_INSTALL_DIR_URI.toString();
		}
		
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
//		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
//		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
//		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile );
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.TARGET_INSTALL_DIR, installDir );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_FILE_NAME);
		
		
		final WorkflowExecutionContext workflowContext = executeWithContext( "load_instanciate_validate_refine_and_generate_freertos", props );

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = null;
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = null;
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc");
		}
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
		
		final String traceFileRef;
		final String traceFileName = "the_cpu.exec_trace";
		
		if (Platform.isRunning()) {
			URI traceFileRefURI = null;
			traceFileRef = null;
		} else {
			traceFileRef = URI.createURI(TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI).appendSegment(OUTPUT_DIR_REF).appendSegment(traceFileName).toFileString();
		}
		
		testGeneratedCodeExecution(URI.createURI( TEST_SOURCES_ABS_LOCATION + CODEGEN_URI ).toFileString());
		
		// sleep long enough to let generated code execute for at least 10 sec.
		Thread.sleep(25*1000);
		
		URI theCpuTraceURI = null;		
		if (Platform.isRunning()) {
			theCpuTraceURI = null;
		} else {
			theCpuTraceURI = URI.createURI( TEST_SOURCES_ABS_LOCATION + CODEGEN_URI )
					.appendSegment("the_cpu")
					.appendSegment(traceFileName);
		}
		
		File execTrace1 = new File(theCpuTraceURI.toFileString());
		File execTrace2 = new File(traceFileRef);
		
		assertTrue(execTrace1.exists());
		
		boolean sameExecutionTrace = FileUtils.contentEquals(execTrace1, execTrace2);
		
		assertTrue(sameExecutionTrace);
	}
	
	@Override
	protected String getSourcesProjectName() {
		return SOURCES_PROJECT_NAME;
	}

	@Override
	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.ramses.freertos";
	}

	@Override
	protected String[] getWorkflowDir() {
		String[] res = {"ramses", "freertos", "workflows"};
		return res;
	}

	@Override
	protected String getTargetName() {
		return "freertos";
	}
	
}
