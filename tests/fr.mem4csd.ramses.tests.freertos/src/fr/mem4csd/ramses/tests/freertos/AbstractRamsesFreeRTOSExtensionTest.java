package fr.mem4csd.ramses.tests.freertos;

import org.eclipse.emf.common.util.URI;

import fr.mem4csd.ramses.tests.core.AbstractRamsesPluginTest;

public abstract class AbstractRamsesFreeRTOSExtensionTest extends AbstractRamsesPluginTest{
	
	protected static final URI FREERTOS_INSTALL_DIR_URI = URI.createURI( System.getenv("HOME")+"/ramses-resources/freertos", true );

}
