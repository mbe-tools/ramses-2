/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.linux;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;
import fr.mem4csd.ramses.tests.core.AbstractRamsesPluginTest;
import fr.mem4csd.ramses.tests.core.utils.ReportComparator;

public class TestRamsesLinuxExtension extends AbstractRamsesPluginTest {
	
	private static final String SYSTEM_IMPL_NAME = "main.posix";
	private static final String SOURCES_PROJECT_NAME = "fr.mem4csd.ramses.tests.linux.sources";
	
	@Override
	protected boolean executeAsSudo()
	{
		return true;
	}
	
	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		cleanResources( SOURCES_PROJECT_NAME );

		if ( !Platform.isRunning() ) {
			WorkflowramseslinuxPackage.eINSTANCE.eClass();
		}
	}
	
	private void configRefinementSampledCommunications(Map<String, String> props) {
		final String srcAadlFile;
		final String instanceModelFile;
		final String refinedAadlFile;
		

		final String refinedTraceFile;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );

		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI, true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_URI, true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;
			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI;
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );

		
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_SAMPLED_COM_FILE_NAME );
	}
	
	@Test
	public void testValidateSampledCommunications() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();
		
		final String instanceModelFile;
		final String srcAadlFile;
		final String reportFile;
	//	final String reportFileComp;
		final String reportFileRef;
		final String outputDir;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );

		if ( Platform.isRunning() ) {
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			reportFile = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_URI, true ).toString();
			//reportFileComp = reportFile;
			reportFileRef = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_URI_REF, true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
		}
		else {
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_URI;
			//reportFileComp = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_URI;
			reportFileRef = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_URI_REF;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );

		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile);
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_FILE_NAME);
		
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.linux/ramses/linux/workflows/default_linux.properties");

		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_validate_core", props );
		assertFalse(workflowContext.getModelSlots().isEmpty());
		
		ReportComparator cmp = new ReportComparator( reportFileRef, reportFile, getResourceSet());
		boolean results = cmp.checkResults();

		assertTrue(results);
	}
	
	@Test
	public void testValidateSampledCommunicationsInvalid() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();
		
		final String instanceModelFile;
		final String aadlFile;
		final String reportFile;
		//final String reportFileComp;
		final String reportFileRef;
		final String outputDir;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );

		if ( Platform.isRunning() ) {
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			aadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_INVALID_URI.toString(), true ).toString();
			reportFile = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_INVALID_URI, true ).toString();
			//reportFileComp = reportFile;
			reportFileRef = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_INVALID_URI_REF, true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
		}
		else {
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			aadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_INVALID_URI;
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI;
			reportFileRef = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI_REF;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, aadlFile );
		
		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile);
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_INVALID_FILE_NAME);
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.linux/ramses/linux/workflows/default_linux.properties");

		String[] coreWorkflowDir = {"ramses", "core", "workflows"};

		try {
			/*final WorkflowExecutionContext workflowContext = */executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_validate_core", props );
		}
		catch ( final WorkflowExecutionException ex ) {
			//assertFalse(workflowContext.getModelSlots().isEmpty());
			
			final ReportComparator cmp = new ReportComparator( reportFileRef, reportFile, getResourceSet() );
			boolean results = cmp.checkResults();
			assertTrue(results);
		}
	}
	
	
	@Test
	public void testRefinementSampledCommunications() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();
		configRefinementSampledCommunications(props);
		
		final String refinedAadlFile;
		final String traceFile;
		
		final String refinedAadlFileRef;
		final String traceFileRef;
		
		if (Platform.isRunning()) {
			refinedAadlFileRef = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI_REF, true ).toString();
			traceFileRef = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_URI_REF, true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI, true ).toString();
			traceFile = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_URI, true ).toString();
		}
		else {
			refinedAadlFileRef = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI_REF;
			traceFileRef = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI_REF;
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;
			traceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI;
		}
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_FILE_NAME);
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.linux/ramses/linux/workflows/default_linux.properties");

		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_refine_core", props );
		assertFalse(workflowContext.getModelSlots().isEmpty());
		
		final ReportComparator cmpAadl = new ReportComparator( refinedAadlFileRef, refinedAadlFile, getResourceSet());
		assertTrue( cmpAadl.checkResults() );

		final ReportComparator cmpTrace = new ReportComparator( traceFileRef, traceFile, getResourceSet());
		assertTrue( cmpTrace.checkResults() );
	}
	
	@Test
	public void testRefinementSampledCommunicationsExposeRuntimeSharedResources() 
	throws WorkflowExecutionException, IOException {
		Map<String, String> props = createWorkflowProperties();
		configRefinementSampledCommunications(props);
		
		props.put(RamsesWorkflowKeys.EXPOSE_RUNTIME_SHARED_RESOURCES, "true");
		
		final String refinedAadlFileComp;
		final String traceFileComp;
		
		final String refinedAadlFileRef;
		final String traceFileRef;
		
		if (Platform.isRunning()) {
			refinedAadlFileRef = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_SHARED_RESOURCES_URI_REF, true ).toString();
			traceFileRef = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_SHARED_RESOURCES_URI_REF, true ).toString();
			refinedAadlFileComp = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI, true ).toString();
			traceFileComp = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_URI, true ).toString();
		}
		else {
			refinedAadlFileRef = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_SHARED_RESOURCES_URI_REF;
			traceFileRef = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_SHARED_RESOURCES_URI_REF;
			refinedAadlFileComp = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;
			traceFileComp = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI;
		}
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME,  AADL_SAMPLED_COM_FILE_NAME);
		
		props.put( "target_properties", "platform:/plugin/fr.mem4csd.ramses.linux/ramses/linux/workflows/default_linux.properties");

		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "load_instanciate_and_refine_core", props );
		assertFalse(workflowContext.getModelSlots().isEmpty());
		
		final ReportComparator cmpAadl = new ReportComparator(refinedAadlFileComp, refinedAadlFileRef, getResourceSet());
		assertTrue( cmpAadl.checkResults() );

		final ReportComparator cmpTrace = new ReportComparator(traceFileComp, traceFileRef, getResourceSet());
		assertTrue( cmpTrace.checkResults() );
	}
	
	@Test
	public void testCodegenExecution()
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();
		
		WorkflowramseslinuxPackage.eINSTANCE.eClass();
		
		final String refinedAadlFile;
		final String instanceModelFile;
		final String srcAadlFile;
		final String refinedTraceFile;
		final String outputDir;
		final String includeDir;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		
		if (Platform.isRunning()) {
			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_IN_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_IN_URI, true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
		}
		else {
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_IN_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_IN_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			includeDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.toString();
		}

		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile);
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile);
		
		final WorkflowExecutionContext workflowContext = executeWithContext("load_and_generate_linux", props );		
		
		final String codeGenDir;
		
		if (Platform.isRunning()) {
			codeGenDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			codeGenDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}
		
		URI mainFileURI = null;
		if (Platform.isRunning()) {
			mainFileURI = CommonPlugin.resolve( URI.createFileURI( codeGenDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("main.c") );
		} else {
			mainFileURI = URI.createURI( codeGenDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("main.c");
		}
		File mainFile = new File( mainFileURI.toFileString() );
		
		assertTrue(mainFile.exists());
		
		assertFalse(workflowContext.getModelSlots().isEmpty());
	}
	
	@Test
	public void testCodegenCompilation()
			throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}
		props.put( RamsesWorkflowKeys.WORKING_DIR, workingDir );
		
		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		/*final WorkflowExecutionContext workflowContext =*/ executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "compile_codegen_core", props );

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc");
		}
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
	}
	
	private void executeTest(URI aadlModelURI, String fileName, String systemName, String processorName, String processName, Map<String, String> props) throws WorkflowExecutionException, IOException
	{
		final String srcAadlFile;
		final String includeDir;
		final String outputDir;
		final String instanceModelFile;
//		final String reportFile;
//		final String refinedAadlFile;
		
		final URI instModelUri = computeInstanceModelUri( aadlModelURI, systemName );
		
		if (Platform.isRunning()) {
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
//			reportFile = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_INVALID_URI, true ).toString();
//			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI, true ).toString();
			
		} else {
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + aadlModelURI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
//			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI;
//			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;

			// Code gen directories
			final String absLoc = URI.createURI( TEST_SOURCES_ABS_LOCATION ).toFileString();
			includeDir = absLoc + BASE_TEST_MODEL_DIR_URI.toString();
		}
		
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, systemName );

//		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );

		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, fileName );
//		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile);
		
		
		/*final WorkflowExecutionContext workflowContext =*/ executeWithContext( "load_instanciate_validate_refine_and_generate_linux", props );

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment(processorName)
					.appendSegment(processName)
					.appendSegment(processName) );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment(processorName)
					.appendSegment(processName)
					.appendSegment(processName);//				.resolve( workflowContext.getWorkflowFileURI() );
		}
		
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
		
		final String traceFileRef;
		final String traceFileName = processorName+".exec_trace";
		
		if (Platform.isRunning()) {
			URI traceFileRefURI = URI.createPlatformResourceURI(BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( traceFileName ).toString(), true);
			traceFileRef = CommonPlugin.resolve( traceFileRefURI).toFileString();
		} else {
			traceFileRef = "../"+BASE_TEST_MODEL_DIR_URI.appendSegment(OUTPUT_DIR_REF).appendSegment(traceFileName).toString();
		}
		
		testGeneratedCodeExecution(URI.createFileURI( "../"+CODEGEN_URI ).toFileString(), 20, TimeUnit.SECONDS);
				
		URI theCpuTraceURI = null;		
		if (Platform.isRunning()) {
			theCpuTraceURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment(processorName)
					.appendSegment(traceFileName));
		} else {
			theCpuTraceURI = URI.createFileURI( "../"+CODEGEN_URI )
					.appendSegment(processorName)
					.appendSegment(traceFileName);
		}
		
		File execTrace1 = new File(theCpuTraceURI.toFileString());
		File execTrace2 = new File(traceFileRef);
		
		assertTrue(execTrace1.exists());
		assertTrue(execTrace2.exists());
		
		boolean sameExecutionTrace = FileUtils.contentEquals(execTrace1, execTrace2);
		assertTrue(sameExecutionTrace);		
	}

	@Test
	public void testLinuxExecution() 
			throws WorkflowExecutionException, IOException, InterruptedException, ExecutionException, TimeoutException {
		
		final Map<String, String> props = createWorkflowProperties();
		executeTest(AADL_SAMPLED_COM_URI, AADL_SAMPLED_COM_FILE_NAME, SYSTEM_IMPL_NAME, "the_cpu", "the_proc", props);

	}
	
	@Test
	public void testLinuxExecutionExposeSharedResources() 
			throws WorkflowExecutionException, IOException, InterruptedException, ExecutionException, TimeoutException {
		
		final Map<String, String> props = createWorkflowProperties();
		props.put(RamsesWorkflowKeys.EXPOSE_RUNTIME_SHARED_RESOURCES, "true");
		executeTest(AADL_SAMPLED_COM_URI, AADL_SAMPLED_COM_FILE_NAME, SYSTEM_IMPL_NAME, "the_cpu", "the_proc", props);

	}
	
	@Test
	public void testLinuxExecutionComputeEntrypointSourceText() 
			throws WorkflowExecutionException, IOException, InterruptedException, ExecutionException, TimeoutException {
		
		final Map<String, String> props = createWorkflowProperties();
		executeTest(AADL_SAMPLED_COM_COMPUTE_ENTRYPOINT_SOURCE_TEXT_URI, AADL_SAMPLED_COM_COMPUTE_ENTRYPOINT_SOURCE_TEXT_FILE_NAME, SYSTEM_IMPL_NAME, "the_cpu2", "the_proc", props);

	}
	
	@Test
	public void testLinuxExecutionWithModes() 
	throws WorkflowExecutionException, IOException {
		
		// EB: works if launched from GUI; fails otherwise, because of an error in local-communications
		// transformation. See mapSpgPropertyAssociationList in AADLICopyHelper to comment the failing
		// part.
		
		final Map<String, String> props = createWorkflowProperties();

		final String srcAadlFile;
		final String instanceModelFile;
		final String refinedAadlFile;
		final String reportFile;
		final String outputDir;
		
		String rootSystemName = "root.impl";
		
		final URI instModelUri = computeInstanceModelUri( AADL_MODES_PORTS_CONNECTION_URI,  rootSystemName);
		
		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_MODES_PORTS_CONNECTION_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_MODES_PORTS_CONNECTION_REFINED_URI, true ).toString();
			reportFile = URI.createPlatformResourceURI( REPORT_MODES_PORTS_CONNECTION_INVALID_URI, true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_MODES_PORTS_CONNECTION_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_MODES_PORTS_CONNECTION_REFINED_URI;
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_MODES_PORTS_CONNECTION_INVALID_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, rootSystemName );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );

		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_MODES_PORTS_CONNECTION_FILE_NAME );
		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		
		final WorkflowExecutionContext workflowContext = executeWithContext( "load_instanciate_validate_refine_and_generate_linux", props );

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc_modes")
					.appendSegment("the_proc_modes") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc_modes")
					.appendSegment("the_proc_modes");
		}
		String theProcPath = theProcURI.toFileString() ;
		File theProcExecutable = new File( theProcPath );
		
		assertTrue(theProcExecutable.exists());
	}
	
	@Override
	protected String getSourcesProjectName() {
		return SOURCES_PROJECT_NAME;
	}

	@Override
	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.ramses.linux";
	}
	
	@Override
	protected String[] getWorkflowDir() {
		String[] res = {"ramses", "linux", "workflows"};
		return res;
	}
	
	@Override
	protected String getTargetName()
	{
		return "linux";
	}
}
