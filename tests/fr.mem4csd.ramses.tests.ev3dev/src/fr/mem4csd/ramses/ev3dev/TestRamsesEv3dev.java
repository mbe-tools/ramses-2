package fr.mem4csd.ramses.ev3dev;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.Workflowramsesev3devPackage;
import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;
import fr.mem4csd.ramses.tests.core.AbstractRamsesPluginTest;

public class TestRamsesEv3dev extends AbstractRamsesPluginTest {
	private static final String SYSTEM_IMPL_NAME = "main.posix";
	private static final String SOURCES_PROJECT_NAME = "fr.mem4csd.ramses.tests.ev3dev.sources";

	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		cleanResources( SOURCES_PROJECT_NAME );

		if ( !Platform.isRunning() ) {
			Workflowramsesev3devPackage.eINSTANCE.eClass();
			WorkflowramseslinuxPackage.eINSTANCE.eClass();
		}
	}
	
	private void configRefinementSampledCommunications(Map<String, String> props) {
		final String srcAadlFile;
		final String instanceModelFile;
		final String refinedAadlFile;

		final String refinedTraceFile;
	
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );

		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI, true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_URI, true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;
			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_URI;
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );


		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );

		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_SAMPLED_COM_FILE_NAME );
	}
	
	
		
	
	@Test
	public void testCodegenExecution()
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String refinedAadlFile;
		final String instanceModelFile;
		final String srcAadlFile;
		final String refinedTraceFile;
		final String outputDir;
		final String includeDir;

		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );

		if (Platform.isRunning()) {
			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_IN_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_SAMPLED_COM_REFINED_IN_URI, true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
		}
		else {
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_IN_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_SAMPLED_COM_REFINED_IN_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			includeDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.toString();
		}

		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile);
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile);
		
		final WorkflowExecutionContext workflowContext = executeWithContext("load_and_generate_ev3dev", props );		

		assertFalse(workflowContext.getModelSlots().isEmpty());
	}

	@Test
	public void testCodegenCompilation()
			throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String workingDir;

		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}
		props.put( RamsesWorkflowKeys.WORKING_DIR, workingDir );
		
		String outputDir = URI.createURI(TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR )).toFileString();
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( "docker_mount_dir", System.getProperty("user.home"));
		
		String[] coreWorkflowDir = {"ramses", "core", "workflows"};
		final WorkflowExecutionContext workflowContext = executeWithContext("fr.mem4csd.ramses.core", coreWorkflowDir, "compile_codegen_core", props );

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc");
		}
		File theProcExecutable = new File( theProcURI.toFileString() );

		assertTrue(theProcExecutable.exists());
	}

	@Test
	public void testEv3devExecution() 
			throws WorkflowExecutionException, IOException, InterruptedException, ExecutionException, TimeoutException {
		
		
		final Map<String, String> props = createWorkflowProperties();
		
		final String srcAadlFile;
		final String includeDir;
		final String outputDir;
		final String instanceModelFile;
//		final String reportFile;
//		final String refinedAadlFile;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		
		if (Platform.isRunning()) {
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
//			reportFile = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_INVALID_URI, true ).toString();
//			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI, true ).toString();
			
		} else {
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			outputDir = URI.createURI(TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString()).toString();
//			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI;
//			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;

			// Code gen directories
			final String absLoc = URI.createURI( TEST_SOURCES_ABS_LOCATION ).toFileString();
			includeDir = absLoc + BASE_TEST_MODEL_DIR_URI.toString();
		}
		
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, SYSTEM_IMPL_NAME );

//		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );

		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_SAMPLED_COM_FILE_NAME );
//		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile);
		
		
		final WorkflowExecutionContext workflowContext = executeWithContext( "load_instanciate_validate_refine_and_generate_ev3dev", props );

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}
		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc");//				.resolve( workflowContext.getWorkflowFileURI() );
		}
		
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
	}
	
	@Test
	public void testEv3DevExecutionWithModes() 
	throws WorkflowExecutionException, IOException {
		
		// EB: works if launched from GUI; fails otherwise, because of an error in local-communications
		// transformation. See mapSpgPropertyAssociationList in AADLICopyHelper to comment the failing
		// part.
		
		final Map<String, String> props = createWorkflowProperties();

		final String srcAadlFile;
		final String instanceModelFile;
		final String refinedAadlFile;
		final String reportFile;
		final String outputDir;
		
		String rootSystemName = "root.impl";
		
		final URI instModelUri = computeInstanceModelUri( AADL_MODES_PORTS_CONNECTION_URI,  rootSystemName);
		
		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_MODES_PORTS_CONNECTION_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_MODES_PORTS_CONNECTION_REFINED_URI, true ).toString();
			reportFile = URI.createPlatformResourceURI( REPORT_MODES_PORTS_CONNECTION_INVALID_URI, true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_MODES_PORTS_CONNECTION_URI;
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_MODES_PORTS_CONNECTION_REFINED_URI;
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_MODES_PORTS_CONNECTION_INVALID_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toFileString();
			
		}

		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, rootSystemName );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );

		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_MODES_PORTS_CONNECTION_FILE_NAME );
		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		
		final WorkflowExecutionContext workflowContext = executeWithContext( "load_instanciate_validate_refine_and_generate_ev3dev", props );

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc_modes")
					.appendSegment("the_proc_modes") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc_modes")
					.appendSegment("the_proc_modes")
					.resolve( workflowContext.getWorkflowFileURI() );
		}
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
	}
	
	@Override
	protected String getSourcesProjectName() {
		return SOURCES_PROJECT_NAME;
	}

	@Override
	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.ramses.ev3dev";
	}
	
	@Override
	protected String[] getWorkflowDir() {
		String[] res = {"ramses", "ev3dev", "workflows"};
		return res;
	}
	
	@Override
	protected String getTargetName()
	{
		return "ev3dev";
	}
}
