/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "user_code.h"

#ifdef USE_POK
#include <libc/stdio.h>
#endif

#ifdef USE_POSIX
#include <stdio.h>
#include <stdlib.h>
#endif

#ifdef USE_OSEK
#include "ecrobot_interface.h"
#endif

int counter=0;
void user_receive(unsigned char d)
{

  if(d<10 && d>1 && counter<45)
  {
#if defined USE_POSIX || defined USE_POK
    printf("received value: %d\n", d);
#else
    ecrobot_debug1(d, 0, 0);
#endif
  }
  counter++;
#ifdef USE_POSIX
  if(d>=9 || counter>45)
    exit(0);
#endif
#ifdef USE_POK
  if(counter>45)
	  STOP_SELF();
#endif
}

unsigned char value=0;

void user_send(unsigned char * d)
{
  *d = value;
  value++;
  if(*d<10 && *d>1)
  {
#if defined USE_POSIX || defined USE_POK
    printf("send value: %d\n", *d);
#else
    ecrobot_debug1(0, 0, d);
#endif
  }
#ifdef USE_POK
  else if(*d>15)
    	STOP_SELF();
#endif
#ifdef USE_POSIX
  else if(*d>15)
    	exit(0);
#endif
}

void user_transmit(unsigned char d_in, unsigned char * d_out)
{
#if defined USE_POSIX || defined USE_POK
  if(d_in<10 && d_in>1)
    printf("Transmit %d\n", d_in);
#endif
  *d_out = d_in;
}

void periodic()
{
  if(value<10 && value>1) {
#if defined USE_POSIX || defined USE_POK
    printf("periodic activation\n");
#else
    ecrobot_debug1(0, 0, d);
#endif
  }
#ifdef USE_POK
  //  else if(value>9)
    //  	  STOP_SELF();
#endif
}

void feature_group2_spg(int p1, int * p2)
{
	*p2 = p1+1;
#if defined USE_POSIX || defined USE_POK
	printf("received value %d\n", p1);
#endif
	counter++;
#ifdef USE_POSIX
  if(counter>9)
	  exit(0);
#endif
#ifdef USE_POK
  if(counter>9)
	  STOP_SELF();
#endif
}

void user_receive2p(unsigned char d1, unsigned char d2)
{
  if(d1<10 && d1>1 && d2<10 && d2 > 1 && counter<45)
  {
#if defined USE_POSIX || defined USE_POK
    printf("received values: %d %d\n", d1, d2);
#else
    ecrobot_debug1(d1, d2, 0);
#endif
  }
  counter++;
#ifdef USE_POSIX
  if(d1>=9 || counter>45)
    exit(0);
#endif
#ifdef USE_POK
  if(counter>45)
	  STOP_SELF();
#endif
}

void receive(unsigned char d)
{

  if(d<10 && d>1 && counter<45)
  {
#if defined USE_POSIX || defined USE_POK
    printf("received value: %d\n", d);
#else
    ecrobot_debug1(d, 0, 0);
#endif
  }
  counter++;
#ifdef USE_POSIX
  if(d>=9 || counter>45)
    exit(0);
#endif
#ifdef USE_POK
  if(counter>45)
	  STOP_SELF();
#endif
}

void send(unsigned char * d)
{
  *d = value;
  value++;
  if(*d<10 && *d>1)
  {
#if defined USE_POSIX || defined USE_POK
    printf("send value: %d\n", *d);
#if defined USE_POSIX
	fflush(stdout);
#endif
#else
    ecrobot_debug1(0, 0, d);
#endif
  }
#ifdef USE_POK
  else if(*d>15)
    	STOP_SELF();
#endif
#ifdef USE_POSIX
  else if(*d>15)
    	exit(0);
#endif
}

