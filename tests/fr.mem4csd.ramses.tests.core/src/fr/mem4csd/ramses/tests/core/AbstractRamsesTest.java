package fr.mem4csd.ramses.tests.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.junit.After;
import org.osate.aadl2.modelsupport.FileNameConstants;

import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;
import fr.mem4csd.ramses.core.validation_report.Validation_reportPackage;

public abstract class AbstractRamsesTest {

	protected final String VALIDATION_REPORT_EXT = Validation_reportPackage.eNAME;//"validation_report";
	protected final String TRACE_EXT = Arch_tracePackage.eNAME;//"arch_trace";
		
	protected static final String MODEL_DIR = "model";
	
	protected static final String INPUT_DIR = "input";
	protected static final String OUTPUT_DIR = "output";
	protected static final String OUTPUT_DIR_REF = "output_ref";
	protected static final String CODEGEN_DIR = "generated-code";

	protected final String EXPOSE_SHARED_RESOURCES_SUFFIX = "-with-shared-resources";

	protected final String AADL_SAMPLED_COM_FILE_NAME = "sampled-communications";
	protected final String AADL_SAMPLED_COM_COMPUTE_ENTRYPOINT_SOURCE_TEXT_FILE_NAME = "sampled-communications-compute-entrypoint-source-text";
	protected final String AADL_MODES_PORTS_CONNECTION_FILE_NAME = "modes_port_connection";
	protected final String AADL_MULTI_TARGET_FILE_NAME = "multi_target";
	protected final String AADL_MULTI_PROTOCOL_FILE_NAME = "multi_protocol";
	protected final String AADL_PING_FILE_NAME = "ping";
	protected final String AADL_SPORADIC_FILE_NAME = "sporadic";
	
	protected final String AADL_BA_DISPATCH_CONDITIONS_FILE_NAME = "ba_dispatch_conditions";
	protected final String AADL_BA_PORTS_ACTIONS_FILE_NAME = "ba_ports_actions";

	
	protected final String AADL_SAMPLED_COM_INVALID_FILE_NAME = AADL_SAMPLED_COM_FILE_NAME + "-invalid";
	protected final String AADL_SAMPLED_COM_REFINED_FILE_NAME = AADL_SAMPLED_COM_FILE_NAME + "-refined";
	
	protected final String AADL_MODES_PORTS_CONNECTION_INVALID_FILE_NAME = AADL_MODES_PORTS_CONNECTION_FILE_NAME + "-invalid";
	protected final String AADL_MODES_PORTS_CONNECTION_REFINED_FILE_NAME = AADL_MODES_PORTS_CONNECTION_FILE_NAME+ "-refined";
	
	protected final String AADL_PING_INVALID_FILE_NAME = AADL_PING_FILE_NAME + "-invalid";
	protected final String AADL_PING_REFINED_FILE_NAME = AADL_PING_FILE_NAME+ "-refined";
	
	protected final String AADL_MULTI_TARGET_REFINED_FILE_NAME = AADL_MULTI_TARGET_FILE_NAME+"-refined";
	protected final String AADL_MULTI_PROTOCOL_REFINED_FILE_NAME = AADL_MULTI_PROTOCOL_FILE_NAME+"-refined";
	
	protected final String AADL_BA_PORTS_ACTIONS_REFINED_FILE_NAME = AADL_BA_PORTS_ACTIONS_FILE_NAME+"-refined";
	protected final String AADL_BA_DISPATCH_CONDITIONS_REFINED_FILE_NAME = AADL_BA_DISPATCH_CONDITIONS_FILE_NAME+"-refined";

	
	protected final URI BASE_TEST_MODEL_DIR_URI = URI.createURI( getSourcesProjectName(), true ).appendSegment( MODEL_DIR );

	protected final String AADL_SAMPLED_COM_FILE_PATH = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_SAMPLED_COM_FILE_NAME ).toString();
	protected final String AADL_PING_FILE_PATH = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_PING_FILE_NAME ).toString();
	protected final String AADL_MODES_PORTS_CONNECTION_FILE_PATH = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_MODES_PORTS_CONNECTION_FILE_NAME ).toString();
			
	protected final URI AADL_SAMPLED_COM_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_SAMPLED_COM_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	protected final URI AADL_SAMPLED_COM_COMPUTE_ENTRYPOINT_SOURCE_TEXT_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_SAMPLED_COM_COMPUTE_ENTRYPOINT_SOURCE_TEXT_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	protected final URI AADL_MODES_PORTS_CONNECTION_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_MODES_PORTS_CONNECTION_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	protected final URI AADL_PING_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_PING_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	protected final URI AADL_SPRORADIC_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_SPORADIC_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	
	protected final String AADL_SAMPLED_COM_INVALID_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_SAMPLED_COM_INVALID_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT).toString();
	
	protected final String AADL_SAMPLED_COM_REFINED_IN_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_SAMPLED_COM_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT).toString();
	protected final String TRACE_SAMPLED_COM_REFINED_IN_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_SAMPLED_COM_REFINED_FILE_NAME ).appendFileExtension( TRACE_EXT ).toString();

	protected final String REPORT_SAMPLED_COM_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_SAMPLED_COM_FILE_NAME ).appendFileExtension( VALIDATION_REPORT_EXT ).toString();
	protected final String REPORT_SAMPLED_COM_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_SAMPLED_COM_FILE_NAME ).appendFileExtension( VALIDATION_REPORT_EXT ).toString();
	
	protected final String REPORT_PING_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_PING_FILE_NAME ).appendFileExtension( VALIDATION_REPORT_EXT ).toString();
	
	protected final String REPORT_SAMPLED_COM_INVALID_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_SAMPLED_COM_INVALID_FILE_NAME ).appendFileExtension( VALIDATION_REPORT_EXT ).toString();
	protected final String REPORT_SAMPLED_COM_INVALID_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_SAMPLED_COM_INVALID_FILE_NAME ).appendFileExtension( VALIDATION_REPORT_EXT ).toString();

	protected final String REPORT_PING_INVALID_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_PING_INVALID_FILE_NAME ).appendFileExtension( VALIDATION_REPORT_EXT ).toString();
	protected final String REPORT_MODES_PORTS_CONNECTION_INVALID_URI= BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_MODES_PORTS_CONNECTION_INVALID_FILE_NAME ).appendFileExtension( VALIDATION_REPORT_EXT ).toString();
	
	protected final String AADL_SAMPLED_COM_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_SAMPLED_COM_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT).toString();
	protected final String AADL_SAMPLED_COM_REFINED_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_SAMPLED_COM_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT).toString();
	protected final String AADL_SAMPLED_COM_REFINED_SHARED_RESOURCES_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_SAMPLED_COM_REFINED_FILE_NAME+EXPOSE_SHARED_RESOURCES_SUFFIX ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT ).toString();
	protected final String AADL_MODES_PORTS_CONNECTION_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_MODES_PORTS_CONNECTION_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT).toString();
	protected final String AADL_MODES_PORTS_CONNECTION_REFINED_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_MODES_PORTS_CONNECTION_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT).toString();

	protected final String AADL_PING_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_PING_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT).toString();
	protected final String AADL_PING_REFINED_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_PING_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT).toString();
	
	protected final URI AADL_MULTI_TARGET_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_MULTI_TARGET_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	protected final String AADL_MULTI_TARGET_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_MULTI_TARGET_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT).toString();
	protected final String TRACE_MULTI_TARGET_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_MULTI_TARGET_REFINED_FILE_NAME ).appendFileExtension(TRACE_EXT).toString();
	protected final String REPORT_MULTI_TARGET_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_MULTI_TARGET_REFINED_FILE_NAME ).appendFileExtension( VALIDATION_REPORT_EXT ).toString();
	
	protected final URI AADL_MULTI_PROTOCOL_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	protected final String AADL_MULTI_PROTOCOL_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_MULTI_PROTOCOL_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT).toString();
	protected final String TRACE_MULTI_PROTOCOL_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_MULTI_PROTOCOL_REFINED_FILE_NAME ).appendFileExtension(TRACE_EXT).toString();
	protected final String REPORT_MULTI_PROTOCOL_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_MULTI_PROTOCOL_REFINED_FILE_NAME ).appendFileExtension( VALIDATION_REPORT_EXT ).toString();
	
	
	protected final String TRACE_SAMPLED_COM_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_SAMPLED_COM_REFINED_FILE_NAME ).appendFileExtension( TRACE_EXT ).toString();
	protected final String TRACE_SAMPLED_COM_REFINED_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_SAMPLED_COM_REFINED_FILE_NAME ).appendFileExtension( TRACE_EXT ).toString();
	protected final String TRACE_SAMPLED_COM_REFINED_SHARED_RESOURCES_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_SAMPLED_COM_REFINED_FILE_NAME+EXPOSE_SHARED_RESOURCES_SUFFIX ).appendFileExtension( TRACE_EXT ).toString();
	
	protected final String TRACE_MODES_PORTS_CONNECTION_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_MODES_PORTS_CONNECTION_REFINED_FILE_NAME ).appendFileExtension( TRACE_EXT ).toString();
	protected final String TRACE_MODES_PORTS_CONNECTION_REFINED_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_MODES_PORTS_CONNECTION_REFINED_FILE_NAME ).appendFileExtension( TRACE_EXT ).toString();
	
	protected final String TRACE_PING_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_PING_REFINED_FILE_NAME ).appendFileExtension( TRACE_EXT ).toString();
	protected final String TRACE_PING_REFINED_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_PING_REFINED_FILE_NAME ).appendFileExtension( TRACE_EXT ).toString();
	
	protected final URI AADL_BA_PORTS_ACTIONS_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_BA_PORTS_ACTIONS_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	protected final URI AADL_BA_PORTS_ACTIONS_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_BA_PORTS_ACTIONS_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	protected final URI TRACE_BA_PORTS_ACTIONS_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_BA_PORTS_ACTIONS_REFINED_FILE_NAME ).appendFileExtension( TRACE_EXT );
	protected final URI AADL_BA_PORTS_ACTIONS_REFINED_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_BA_PORTS_ACTIONS_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	
	protected final URI AADL_BA_DISPATCH_CONDITIONS_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).appendSegment( AADL_BA_DISPATCH_CONDITIONS_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	protected final URI AADL_BA_DISPATCH_CONDITIONS_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_BA_DISPATCH_CONDITIONS_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	protected final URI TRACE_BA_DISPATCH_CONDITIONS_REFINED_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( AADL_BA_DISPATCH_CONDITIONS_REFINED_FILE_NAME ).appendFileExtension( TRACE_EXT );
	protected final URI AADL_BA_DISPATCH_CONDITIONS_REFINED_URI_REF = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment( AADL_BA_DISPATCH_CONDITIONS_REFINED_FILE_NAME ).appendFileExtension(FileNameConstants.SOURCE_FILE_EXT);
	
	protected boolean executeAsSudo()
	{
		return false;
	}
	
	abstract protected String getSourcesProjectName();

	protected static void cleanResources( final String projectName )
			throws CoreException, InterruptedException, IOException {
		if ( Platform.isRunning() ) {
			final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject( projectName );
			final IFolder folder = project.getFolder( MODEL_DIR );
			final IFolder folderoutput = folder.getFolder( OUTPUT_DIR );
			folderoutput.delete( true, new NullProgressMonitor() );
			folderoutput.create( true, true, new NullProgressMonitor() );
			project.build( IncrementalProjectBuilder.CLEAN_BUILD, new NullProgressMonitor() );

			// Building is performed in another thread so we need to ensure it has finished
			Thread.sleep( 8000 );
		}
		else {
			final File outputDir = new File( "../" + projectName + "/" + MODEL_DIR + "/" + OUTPUT_DIR );

			if ( outputDir.exists() ) {
				FileUtils.cleanDirectory( outputDir );
			}
		}
	}

	protected List<String> buildCommandLineExecuteGeneratedCode(final String generatedCodePath) {
		List<String> res = new ArrayList<String>();
		if( executeAsSudo() )
			res.add("sudo");
		res.add("make");
		res.add("-s");
		res.add("-C");
		res.add(generatedCodePath);
		res.add("test");
		return res;
	}
	
	protected int executeProcess(final List<String> commandLine) 
			throws IOException, InterruptedException, ExecutionException, TimeoutException {
		return executeProcess(commandLine, 0, null);
	}
	
	protected int executeProcess(final List<String> commandLine, int timeout, TimeUnit unit) 
			throws IOException, InterruptedException, ExecutionException, TimeoutException {
		ProcessBuilder pb = new ProcessBuilder(commandLine);
		
		final Process p = pb.start();
		
		// Launch stdout print in a separate thread
		new Thread(new Runnable() {
		    public void run() {
		     BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		     String line = null; 

		     try {
		        while ((line = input.readLine()) != null)
		            System.out.println(line);
		     } catch (IOException e) {
		            e.printStackTrace();
		     }
		    }
		}).start();

		if(timeout>0)
		{
			boolean ret = p.waitFor(timeout, unit);
			if(!ret)
			{
				p.destroy();
				return -1;
			}
		}
		else
			return p.waitFor();
		return 0;
	}
	
	protected void testGeneratedCodeExecution(final String generatedCodePath, int timeout, TimeUnit unit) 
	{
		try {
			executeProcess( buildCommandLineExecuteGeneratedCode(generatedCodePath), timeout, unit);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void testGeneratedCodeExecution(final String generatedCodePath) 
			throws IOException, InterruptedException, ExecutionException, TimeoutException
	{
		executeProcess( buildCommandLineExecuteGeneratedCode(generatedCodePath));
	}
	
	protected List<String> getProgramNameToKill()
	{
		return Collections.emptyList();
	}
	
	@After
	public void cleanProcesses()
	{
		try {
			
			for(String progName:getProgramNameToKill())
			{
				Process p = Runtime.getRuntime().exec("pgrep "+progName);
		        BufferedReader input =
		            new BufferedReader
		              (new InputStreamReader(p.getInputStream()));
		        String line;
		        line = input.readLine();
		        
		        if (line != null) {
		        	Runtime.getRuntime().exec("pkill "+progName).waitFor();
		        }
		        input.close();
		        
			}
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}