/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.core;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Map.Entry;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mdelab.workflow.Workflow;
import de.mdelab.workflow.WorkflowProperty;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelFactory;

public class TestRamsesWorkflowConfiguration {

	protected static final String PROJECT_NAME = "fr.mem4csd.ramses.tests.sources";
	protected static final String MODEL_DIR = "model";
	protected static final String WORKFLOW_DIR = "workflows";
	protected static final String CONFIG_DIR = "ramses_config";
	protected static final String TEST_WORKFLOW = URI.createURI( PROJECT_NAME )
			.appendSegment( MODEL_DIR )
			.appendSegment( WORKFLOW_DIR )
			.appendSegment( "test" )
			.appendFileExtension( "workflow" )
			.toString();
	protected static final String CONFIG_OUTPUT = URI.createURI( PROJECT_NAME )
			.appendSegment( CONFIG_DIR )
			.appendSegment( "test_out" )
			.appendFileExtension( "properties" )
			.toString();
	protected static final String CONFIG_INPUT = URI.createURI( PROJECT_NAME )
			.appendSegment( CONFIG_DIR )
			.appendSegment( "test_in" )
			.appendFileExtension( "properties" )
			.toString();
	
	protected static final String RELATIVE_RESOLUTION = "../../";
	
	private final ResourceSet resourceSet;
	
	public TestRamsesWorkflowConfiguration() {
		resourceSet = new ResourceSetImpl();
	}

	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException {
		if (Platform.isRunning()) {
			final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject( PROJECT_NAME );
			final IFile fileoutput = project.getFolder( CONFIG_DIR ).getFile( "test_out.properties" );
			fileoutput.delete(true, new NullProgressMonitor());
		}
		else {
			deleteFile( new File( "../" + PROJECT_NAME + "/" + MODEL_DIR + "/" + WORKFLOW_DIR ), true );
		}
	}
	
	@Test
	public void testConfigCreate() throws WorkflowExecutionException, IOException {
		
		RamsesWorkflowConfiguration config = RamsesviewmodelFactory.eINSTANCE.createRamsesWorkflowConfiguration();
		
		final URI workflowUri = Platform.isRunning() ? URI.createPlatformResourceURI( TEST_WORKFLOW, true ) : URI.createFileURI( RELATIVE_RESOLUTION + TEST_WORKFLOW );
		
		final Resource workflowResource = resourceSet.getResource(workflowUri, true);

		if ( workflowResource.getContents().isEmpty() ) {
			throw new WorkflowExecutionException( "Resource '" + workflowResource.getURI() + "' is empty." );
		}
		
		final Workflow workflow = (Workflow) workflowResource.getContents().get(0);
		
		config.create("TEST_CONFIG", workflow);

		for ( final WorkflowProperty prop : workflow.getProperties() ) {
			assertTrue(config.getProperties().containsKey( prop.getName() ));
			assertTrue(config.getProperties().get( prop.getName() ) == prop.getDefaultValue() || config.getProperties().get( prop.getName() ).equals( prop.getDefaultValue() ));
		}
		
		final URI configURI = Platform.isRunning() ? URI.createPlatformResourceURI( CONFIG_OUTPUT, true ) : URI.createFileURI( RELATIVE_RESOLUTION + CONFIG_OUTPUT );
		
		config.store(configURI);
	}

	@Test
	public void testConfigLoad() throws WorkflowExecutionException, IOException {
		
		RamsesWorkflowConfiguration config = RamsesviewmodelFactory.eINSTANCE.createRamsesWorkflowConfiguration();
		
		final URI workflowUri = Platform.isRunning() ? URI.createPlatformResourceURI( TEST_WORKFLOW, true ) : URI.createFileURI( RELATIVE_RESOLUTION + TEST_WORKFLOW );
		
		final Resource workflowResource = resourceSet.getResource(workflowUri, true);

		if ( workflowResource.getContents().isEmpty() ) {
			throw new WorkflowExecutionException( "Resource '" + workflowResource.getURI() + "' is empty." );
		}
		
		final Workflow workflow = (Workflow) workflowResource.getContents().get(0);
		
		final URI configURI = Platform.isRunning() ? URI.createPlatformResourceURI( CONFIG_INPUT, true ) : URI.createFileURI( RELATIVE_RESOLUTION + CONFIG_OUTPUT );
		
		config.load(configURI);

		for ( final WorkflowProperty prop : workflow.getProperties() ) {
			assertTrue(config.getProperties().containsKey( prop.getName() ));
			assertTrue(config.getProperties().get( prop.getName() ) == prop.getDefaultValue() || config.getProperties().get( prop.getName() ).equals( prop.getDefaultValue() ));
		}
	}
	
	@Test
	public void testConfigStore() throws IOException, WorkflowExecutionException {
		
		RamsesWorkflowConfiguration config = RamsesviewmodelFactory.eINSTANCE.createRamsesWorkflowConfiguration();
		
		final URI workflowUri = Platform.isRunning() ? URI.createPlatformResourceURI( TEST_WORKFLOW, true ) : URI.createFileURI( RELATIVE_RESOLUTION + TEST_WORKFLOW );
		
		final Resource workflowResource = resourceSet.getResource(workflowUri, true);

		if ( workflowResource.getContents().isEmpty() ) {
			throw new WorkflowExecutionException( "Resource '" + workflowResource.getURI() + "' is empty." );
		}
		
		final Workflow workflow = (Workflow) workflowResource.getContents().get(0);
		
		config.create("TEST_CONFIG", workflow);
		
		final URI configURI = Platform.isRunning() ? URI.createPlatformResourceURI( CONFIG_OUTPUT, true ) : URI.createFileURI( RELATIVE_RESOLUTION + CONFIG_OUTPUT );
		
		config.store(configURI);
		
		RamsesWorkflowConfiguration outputConfig = RamsesviewmodelFactory.eINSTANCE.createRamsesWorkflowConfiguration();
		outputConfig.load(configURI);
		
		assertTrue(config.getName().equals(outputConfig.getName()));
		
		assertTrue(config.getWorkflowURI().equals(outputConfig.getWorkflowURI()));

		for ( final Entry<String, String> prop : config.getProperties() ) {
			assertTrue( outputConfig.getProperties().containsKey( prop.getKey() ));
			assertTrue( outputConfig.getProperties().get(prop.getKey()) == prop.getValue() ||
					outputConfig.getProperties().get(prop.getKey()).equals( prop.getValue() ));
		}
		
	}
	
	private static void deleteFile( final File file,
									final boolean deleteContentOnly ) {
		if ( file.isDirectory() ) {
			String[] fileNames = file.list();
			
			if ( fileNames == null ) {
				return;
			}
			
			for ( String fileName : fileNames ) {
				File subFile = new File( file.getPath() + File.separatorChar + fileName );
				
				// A directory that contains files will not be deleted
				deleteFile( subFile, false );
				//subFile.delete();        
			}
		}

		if ( !deleteContentOnly ) {
			file.delete();
		}
	}
}
