/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.core.utils;

import java.io.IOException;
import java.io.PrintStream;

import org.antlr.v4.runtime.misc.Pair;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import fr.tpt.mem4csd.utils.compare.IComparisonReport;
import fr.tpt.mem4csd.utils.compare.aadl.DefaultComparatorAADL;
import fr.tpt.mem4csd.utils.compare.emf.IComparatorEMF;

/**
 * 
 * <!-- begin-user-doc -->
 * This class is used to compare the output of 2 reports
 * <!-- end-user-doc -->
 */
public class ReportComparator {
	protected final ResourceSet resourceSet;
	protected final Pair<Resource, Resource> files;
	protected final IComparatorEMF comparator;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param path of the first report. It supports eclipse platform urls.
	 */
	public ReportComparator(final String filePathLeft, final String filePathRight) {
		this( filePathLeft, filePathRight, new ResourceSetImpl() );
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param path of the first report. It supports eclipse platform urls.
	 */
	public ReportComparator(	final String filePathReference, 
								final String filePathChanged,
								final ResourceSet resourceSet ) {
		this.resourceSet = resourceSet;
		final Resource fileReference = createResource(filePathReference);
		final Resource fileChanged = createResource(filePathChanged);
		
		// We need to resolve references in order to avoid differences on different href
		// encodings leading to the same objects
		EcoreUtil.resolveAll( fileReference );
		EcoreUtil.resolveAll( fileChanged );

		files = new Pair<Resource, Resource>(fileReference, fileChanged);
		comparator = new DefaultComparatorAADL(resourceSet);
	}
	
	protected Resource createResource(String resourcePath) {
		final URI uri = /*CommonPlugin.resolve(*/URI.createURI(resourcePath);
		final Resource toGenerate = resourceSet.getResource(uri, true);
		return toGenerate;
	}
	
	protected void toPrint(	final PrintStream ostream,
							final IComparisonReport report ) {
		ostream.println("---ReportComparator---");
		ostream.println("Reports were not similar, expected: ");
		report.print(ostream);
//		if (files.b.getContents().isEmpty()) {
//			ostream.println("Empty");
//		} else {
//			ostream.println(files.b.getContents().get(0));
//		}
//		ostream.println("But instead, got: ");
//		if (files.a.getContents().isEmpty()) {
//			ostream.println("Empty");
//		} else {
//			ostream.println(files.a.getContents().get(0));
//		}
		
		ostream.println("---<<<<<<<-->>>>>>>---");
	}
	
	public boolean checkResults()
	throws IOException {
		return checkResults(System.err);
	}
	
	public boolean checkResults(final PrintStream ostream) 
	throws IOException {
		IComparisonReport report = comparator.compare(files.a, files.b);
		
		if (report.containsDiff()) {
			toPrint( ostream, report);
			//report.print(ostream);
			return false;
		} //else {
			
		return true;
	}
}
