/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.core;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.mem4csd.ramses.tests.core.utils.ReportComparator;

public class TestCoreWorkflows extends AbstractRamsesPluginTest {
	
	private static final String SOURCES_PROJECT_NAME = "fr.mem4csd.ramses.tests.core.sources";
//	private static final URI NXTOSEK_RUNTIME_DIR_URI = URI.createURI( System.getenv("HOME")+"/ramses-resources/nxtOSEK", true );
	protected static final URI POK_RUNTIME_DIR_URI = URI.createURI( System.getenv("HOME")+"/ramses-resources/pok", true );
	protected final String POSIX_CODEGEN_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( "posix" ).appendSegment( CODEGEN_DIR ).toString();
	protected final String POK_CODEGEN_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( "pok" ).appendSegment( CODEGEN_DIR ).toString();
	
	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		cleanResources( SOURCES_PROJECT_NAME );
	}

	@Test
	public void testLoadAadlContributionsCoreWorkflow()
	throws WorkflowExecutionException, IOException {
		final WorkflowExecutionContext context = executeWithContext( "load_aadl_resources_core", createWorkflowProperties() );
		
		assertFalse( context.getModelSlots().isEmpty() );
	}
	
	@Test
	public void testLoadRamsesAadlContributionsCoreWorkflow() 
	throws WorkflowExecutionException, IOException {
		final WorkflowExecutionContext context = executeWithContext("load_ramses_aadl_resources_core", createWorkflowProperties());
		
		assertFalse( context.getModelSlots().isEmpty() );
	}
	
	@Test
	public void testLoadValidationCoreWorkflow() 
	throws WorkflowExecutionException, IOException {
		final WorkflowExecutionContext context = executeWithContext("load_validation_core", createWorkflowProperties());
		
		assertFalse( context.getModelSlots().isEmpty() );
	}
	
	@Test
	public void testLoadRefinementCoreWorkflow() 
	throws WorkflowExecutionException, IOException {
		final WorkflowExecutionContext context = executeWithContext("load_refinement_core", createWorkflowProperties());
		
		assertFalse( context.getModelSlots().isEmpty() );
	}

	@Test
	public void testModesRefinementCoreWorkflow() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String srcAadlFile;
		final String instanceModelFile;
		final String refinedAadlFile;
		final String refinedTraceFile;
		final String refinedAadlFileRef;

		final String rootSystemName = "root.impl";
		
		final URI instModelUri = computeInstanceModelUri( AADL_MODES_PORTS_CONNECTION_URI, rootSystemName );

		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_MODES_PORTS_CONNECTION_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_MODES_PORTS_CONNECTION_REFINED_URI, true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_MODES_PORTS_CONNECTION_REFINED_URI, true ).toString();
			refinedAadlFileRef = URI.createPlatformResourceURI( AADL_MODES_PORTS_CONNECTION_REFINED_URI_REF, true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_MODES_PORTS_CONNECTION_URI.toString();
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri.toString();
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_MODES_PORTS_CONNECTION_REFINED_URI;
			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_MODES_PORTS_CONNECTION_REFINED_URI;
			refinedAadlFileRef = TEST_SOURCES_ABS_LOCATION + AADL_MODES_PORTS_CONNECTION_REFINED_URI_REF;
		}
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_MODES_PORTS_CONNECTION_FILE_NAME);
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, rootSystemName );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );

		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );
		
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );

		props.put( "refinement_workflow", "platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/refine_modes_core.workflow");
		props.put( "target_properties", "default.properties");
		
		final WorkflowExecutionContext workflowContext = executeWithContext( "load_instanciate_and_refine_core", props );
		
		assertFalse(workflowContext.getModelSlots().isEmpty());

		// Need to pass the resource set to compare cross references
		// Also reference model should be passes on the left side for proper changes message display 
		final ReportComparator cmpAadl = new ReportComparator( refinedAadlFileRef, refinedAadlFile, getResourceSet() );
		assertTrue( cmpAadl.checkResults() );
	}
	

	@Test
	public void testBaPortsActionsRefinementCoreWorkflow() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String srcAadlFile;
		final String instanceModelFile;
		final String refinedAadlFile;
		final String refinedTraceFile;
		//final String refinedAadlFileComp;
		final String refinedAadlFileRef;

		final String rootSystemName = "root.impl";
		
		final URI instModelUri = computeInstanceModelUri( AADL_BA_PORTS_ACTIONS_URI, rootSystemName );

		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_BA_PORTS_ACTIONS_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_BA_PORTS_ACTIONS_REFINED_URI.toString(), true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_BA_PORTS_ACTIONS_REFINED_URI.toString(), true ).toString();
			refinedAadlFileRef = URI.createPlatformResourceURI( AADL_BA_PORTS_ACTIONS_REFINED_URI_REF.toString(), true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_BA_PORTS_ACTIONS_URI.toString();
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri.toString();
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_BA_PORTS_ACTIONS_REFINED_URI;
			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_BA_PORTS_ACTIONS_REFINED_URI;
			refinedAadlFileRef = TEST_SOURCES_ABS_LOCATION + AADL_BA_PORTS_ACTIONS_REFINED_URI_REF;
		}
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_BA_PORTS_ACTIONS_FILE_NAME);
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, rootSystemName );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );

		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );
		
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );

		props.put( "refinement_workflow", "platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/refine_ba_core.workflow");
		props.put( "target_properties", "default.properties");
		
		final WorkflowExecutionContext workflowContext = executeWithContext( "load_instanciate_and_refine_core", props );
		
		assertFalse(workflowContext.getModelSlots().isEmpty());

		// Need to pass the resource set to compare cross references
		// Also reference model should be passes on the left side for proper changes message display 
		final ReportComparator cmpAadl = new ReportComparator( refinedAadlFileRef, refinedAadlFile, getResourceSet() );
		assertTrue( cmpAadl.checkResults() );
	}
	
	@Test
	public void testBADispatchConditionsRefinementCoreWorkflow() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String srcAadlFile;
		final String instanceModelFile;
		final String refinedAadlFile;
		final String refinedTraceFile;
		//final String refinedAadlFileComp;
		final String refinedAadlFileRef;

		final String rootSystemName = "root.impl";
		
		final URI instModelUri = computeInstanceModelUri( AADL_BA_DISPATCH_CONDITIONS_URI, rootSystemName );

		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_BA_DISPATCH_CONDITIONS_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_BA_DISPATCH_CONDITIONS_REFINED_URI.toString(), true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_BA_DISPATCH_CONDITIONS_REFINED_URI.toString(), true ).toString();
			refinedAadlFileRef = URI.createPlatformResourceURI( AADL_BA_DISPATCH_CONDITIONS_REFINED_URI_REF.toString(), true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_BA_DISPATCH_CONDITIONS_URI.toString();
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri.toString();
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_BA_DISPATCH_CONDITIONS_REFINED_URI;
			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_BA_DISPATCH_CONDITIONS_REFINED_URI;
			refinedAadlFileRef = TEST_SOURCES_ABS_LOCATION + AADL_BA_DISPATCH_CONDITIONS_REFINED_URI_REF;
		}
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_BA_DISPATCH_CONDITIONS_FILE_NAME);
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, rootSystemName );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );

		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );
		
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );

		props.put( "refinement_workflow", "platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/refine_ba_core.workflow");
		props.put( "target_properties", "default.properties");
		
		final WorkflowExecutionContext workflowContext = executeWithContext( "load_instanciate_and_refine_core", props );
		
		assertFalse(workflowContext.getModelSlots().isEmpty());

		// Need to pass the resource set to compare cross references
		// Also reference model should be passes on the left side for proper changes message display 
		final ReportComparator cmpAadl = new ReportComparator( refinedAadlFileRef, refinedAadlFile, getResourceSet() );
		assertTrue( cmpAadl.checkResults() );
	}
	
	@Override
	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.ramses.core";
	}

	@Override
	protected String getSourcesProjectName() {
		return SOURCES_PROJECT_NAME;
	}
}
