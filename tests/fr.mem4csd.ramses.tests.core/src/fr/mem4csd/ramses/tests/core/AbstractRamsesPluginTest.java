/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.osate.aadl2.modelsupport.FileNameConstants;
import org.osate.annexsupport.AnnexRegistry;
import org.osate.ba.AadlBaParserAction;
import org.osate.ba.AadlBaResolver;
import org.osate.ba.AadlBaUnParserAction;
import org.osate.ba.texteditor.AadlBaSemanticHighlighter;
import org.osate.ba.texteditor.AadlBaTextPositionResolver;

import de.mdelab.workflow.Workflow;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.WorkflowPackage;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.mdelab.workflow.util.WorkflowLauncher;
import fr.mem4csd.ramses.core.util.RamsesStandaloneSetup;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.tpt.mem4csd.utils.osate.standalone.StandaloneAnnexRegistry.AnnexExtensionRegistration;

public abstract class AbstractRamsesPluginTest extends AbstractRamsesTest {
	
	protected final String CODEGEN_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( CODEGEN_DIR ).appendSegment(getTargetName().toLowerCase()).toString();
	
	
	private final WorkflowLauncher launcher;
	
	protected final URI baseWorkflowDirUri;
	
	private final ResourceSet resourceSet;

	protected final Map<String, String> baseWorkflowProps;
	
	protected static final String TEST_SOURCES_ABS_LOCATION = URI.createFileURI( new File(".").getAbsolutePath() ).trimSegments( 2 ).toString() + "/";
	
	protected String[] getWorkflowDir() {
		String[] res = {"ramses", "core", "workflows"};
		return res;
	}
	
	protected String getTargetName()
	{
		return "";
	}

	protected AbstractRamsesPluginTest() {
		baseWorkflowDirUri = URI.createPlatformPluginURI( getWorkflowProjectDir(), true ).appendSegments( getWorkflowDir() );
		if ( Platform.isRunning() ) {
			
			resourceSet= new ResourceSetImpl();
		}
		else {
			String workflowDirPath = "";
			String[] workflowDir = getWorkflowDir();
			for(int i =0; i<workflowDir.length;i++)
			{
				workflowDirPath = workflowDirPath+"/"+workflowDir[i];
			}
			
			List<AnnexExtensionRegistration> annexExtensions = new ArrayList<AnnexExtensionRegistration>();
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_PARSER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaParserAction.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_UNPARSER_EXT_ID, AadlBaUnParserAction.ANNEX_NAME, AadlBaUnParserAction.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_RESOLVER_EXT_ID, AadlBaResolver.ANNEX_NAME, AadlBaResolver.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_TEXTPOSITIONRESOLVER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaTextPositionResolver.class ));
			annexExtensions.add(new AnnexExtensionRegistration(AnnexRegistry.ANNEX_HIGHLIGHTER_EXT_ID, AadlBaParserAction.ANNEX_NAME, AadlBaSemanticHighlighter.class ));
			resourceSet = new RamsesStandaloneSetup(annexExtensions).createResourceSet();
		}
		
		launcher = createWorkflowLauncher();
		
		baseWorkflowProps = createBaseWorkflowProperties();
	}
	
	protected Map<String, String> createBaseWorkflowProperties() {
		final Map<String, String> props = new HashMap<String, String>();
		
		RamsesWorkflowKeys.setDefaultPropertyValues(props);
		
		return props;
	}
	
	protected ResourceSet getResourceSet() {
		return resourceSet;
	}
	
	protected Map<String, String> createWorkflowProperties() {
		return new HashMap<String, String>(baseWorkflowProps);
	}
	
	protected abstract String getWorkflowProjectDir();
	
	protected WorkflowLauncher createWorkflowLauncher() {
		return new WorkflowLauncher(Collections.emptyList(), resourceSet, System.out);
	}

	protected Workflow executeWorkflow( final String workflowUriStr )
	throws WorkflowExecutionException, IOException {
		return executeWorkflow( workflowUriStr, Collections.emptyMap() );
	}

	protected Workflow executeWorkflow( final String workflowUriStr,
										final Map<String, String> propertyValues )
	throws WorkflowExecutionException, IOException {
		final URI workflowUri = baseWorkflowDirUri.appendSegment( workflowUriStr ).appendFileExtension( WorkflowPackage.eNS_PREFIX );
		
		final List<Workflow> executeWorkflows =  launcher.execute( Collections.singletonList( workflowUri ), propertyValues, new NullProgressMonitor() ); 
	
		return executeWorkflows.isEmpty() ? null : executeWorkflows.get( 0 );
	}
	
	protected WorkflowExecutionContext executeWithContext(final String workflowUriStr,
														  final Map<String, String> propertyValues )
	throws WorkflowExecutionException, IOException {
		final URI workflowUri = baseWorkflowDirUri.appendSegment( workflowUriStr ).appendFileExtension( WorkflowPackage.eNS_PREFIX );
		
		final Resource workflowResource = getResourceSet().getResource(workflowUri, true);

		if ( workflowResource.getContents().isEmpty() ) {
			throw new WorkflowExecutionException( "Resource '" + workflowResource.getURI() + "' is empty." );
		}
		
		final Workflow workflow = (Workflow) workflowResource.getContents().get(0);
		
		final WorkflowExecutionContext context = workflow.execute( new NullProgressMonitor(), System.out, propertyValues, null, getResourceSet() );
		
		return context;
	}
	
	protected WorkflowExecutionContext executeWithContext(String projectName, String[] workflowDir, String workflowName,
			Map<String, String> props) throws WorkflowExecutionException, IOException {
		
		final URI workflowDirUri;
		if ( Platform.isRunning() ) {
			workflowDirUri = URI.createPlatformPluginURI(projectName , true ).appendSegments( workflowDir );
		} else
		{
			String workflowDirPath = "";
			for(int i =0; i<workflowDir.length;i++)
			{
				workflowDirPath = workflowDirPath+"/"+workflowDir[i];
			}
			workflowDirUri = URI.createURI( "classpath:" + workflowDirPath );
		}
		
		final URI workflowUri = workflowDirUri.appendSegment( workflowName ).appendFileExtension( WorkflowPackage.eNS_PREFIX );
		
		final Resource workflowResource = getResourceSet().getResource(workflowUri, true);

		final Workflow workflow = (Workflow) workflowResource.getContents().get(0);
		
		return workflow.execute( new NullProgressMonitor(), System.out, props, null, getResourceSet() );
	}
	
	protected void handleException( final Throwable th ) {
		th.printStackTrace();
	}
	
	protected URI computeInstanceModelUri( 	final URI aadlFileUri,
											final String systImplName ) {
		final URI dir = aadlFileUri.trimFileExtension().trimSegments( 1 );
		final URI instanceURI = dir.appendSegment(FileNameConstants.AADL_INSTANCES_DIR).appendSegment(aadlFileUri.trimFileExtension().lastSegment() + "_"
				+ systImplName.substring( 0, systImplName.indexOf( '.' ) ) + "_" + systImplName.substring(systImplName.indexOf( '.' ) + 1 ) + FileNameConstants.INSTANCE_MODEL_POSTFIX);
		
		return instanceURI.appendFileExtension(FileNameConstants.INSTANCE_FILE_EXT);
	}
	
 }
