/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef __USER_CODE_H__
#define __USER_CODE_H__

#ifdef USE_POSIX
typedef unsigned char uint8_t;
#endif

void user_receive(unsigned char d);
void user_send(unsigned char* d);
void user_transmit(unsigned char d_in, unsigned char * d_out);
void event_received();
void nothing_received();
void periodic();
void feature_group2_spg(int p1, int * p2);
void my_receive(unsigned char d);
void my_send(unsigned char* d);

#endif
