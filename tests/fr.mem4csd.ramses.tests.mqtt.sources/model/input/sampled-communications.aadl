--
-- RAMSES 2.0
-- 
-- Copyright © 2012-2015 TELECOM Paris and CNRS
-- Copyright © 2016-2020 TELECOM Paris
-- 
-- TELECOM Paris
-- 
-- Authors: see AUTHORS
--
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program. 
-- If not, see https://www.eclipse.org/legal/epl-2.0/
--

package sampled_communications
public
with Data_Model,RAMSES_Properties;

  system main
  end main;

  system implementation main.impl
  subcomponents
    the_proc: process proc.impl;
    the_mem: memory mem.impl;
  properties
    actual_memory_binding => (reference (the_mem)) applies to the_proc;
  end main.impl;

  system implementation main.posix extends main.impl
  	subcomponents
    	the_cpu: processor cpu.impl {RAMSES_Properties::Target=>"linux";};
  	properties
  		actual_processor_binding => (reference (the_cpu)) applies to the_proc;
  end main.posix;
  
  processor cpu
  end cpu;

  processor implementation cpu.impl
  properties
      Scheduling_Protocol => (RMS) ;      
  end cpu.impl;

  process proc
  end proc;

  process implementation proc.impl
  subcomponents
    the_sender: thread sender.impl;
    the_receiver: thread receiver.impl;
  connections
    cnx: port the_sender.p -> the_receiver.p;
  end proc.impl;

  memory mem
  end mem;

  memory implementation mem.impl
  properties
    Memory_Size => 200000 Bytes;
  end mem.impl;

  thread sender
  features
    p: out data port Integer;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 2000 Ms;
    Priority => 5;
    Data_Size => 40000 bytes;
    Stack_Size => 512 bytes;
    Code_Size => 40 bytes;
  end sender;

  thread implementation sender.impl
  calls
    call : { c : subprogram sender_spg;};
  connections
    cnx: parameter c.result -> p;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end sender.impl;

  subprogram sender_spg
  features
    result : out parameter Integer;
  properties
    source_name => "my_send";
    source_language => (C);
    source_text => ("user_code.h","user_code.c");
  end sender_spg;

  thread receiver
  features
    p: in data port Integer;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 1000 Ms;
    Priority => 10;
    Data_Size => 40000 bytes;
    Stack_Size => 512 bytes;
    Code_Size => 40 bytes;
  end receiver;

  thread implementation receiver.impl
  calls
    call : { c : subprogram receiver_spg;};
  connections
    cnx: parameter p -> c.input;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end receiver.impl;

  subprogram receiver_spg
  features
    input : in parameter Integer;
  properties
    source_name => "my_receive";
    source_language => (C);
    source_text => ("user_code.h","user_code.c");
  end receiver_spg;

  data Integer
  properties
    Data_Model::Data_Representation => integer;
  end Integer;

end sampled_communications;