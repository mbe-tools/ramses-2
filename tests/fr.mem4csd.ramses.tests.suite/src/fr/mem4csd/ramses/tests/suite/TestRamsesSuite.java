/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import fr.mem4csd.ramses.ev3dev.TestRamsesEv3dev;
import fr.mem4csd.ramses.tests.core.TestCoreWorkflows;
import fr.mem4csd.ramses.tests.freertos.TestRamsesFreeRTOSExtension;
import fr.mem4csd.ramses.tests.integration.linux.TestRamsesLinuxIntegrationExtension;
import fr.mem4csd.ramses.tests.linux.TestRamsesLinuxExtension;
import fr.mem4csd.ramses.tests.mqtt.TestRamsesMqttExtension;
import fr.mem4csd.ramses.tests.nxtosek.TestRamsesNxtOsekExtension;
import fr.mem4csd.ramses.tests.osek.TestRamsesOsekExtension;
import fr.mem4csd.ramses.tests.pok.TestRamsesPokExtension;
import fr.mem4csd.ramses.tests.posix.TestRamsesPosixExtension;
import fr.mem4csd.ramses.tests.sockets.TestRamsesSocketsExtension;
import fr.mem4csd.ramses.tests.workflows.integration.TestIntegrationWorkflows;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	TestCoreWorkflows.class,
	TestRamsesOsekExtension.class,
	TestRamsesNxtOsekExtension.class,
	TestRamsesPokExtension.class,
	TestRamsesLinuxExtension.class,
	TestRamsesPosixExtension.class,
	TestRamsesSocketsExtension.class,
	TestRamsesMqttExtension.class,
	TestRamsesLinuxIntegrationExtension.class,
	TestRamsesFreeRTOSExtension.class,
	TestRamsesEv3dev.class,
	TestIntegrationWorkflows.class,
	TestIntegrationAllCli.class
})

public class TestRamsesSuite {

}
