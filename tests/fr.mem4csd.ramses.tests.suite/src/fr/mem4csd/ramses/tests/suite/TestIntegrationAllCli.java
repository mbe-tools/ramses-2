/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import fr.mem4csd.ramses.tests.workflows.integration.TestIntegrationInterProcessPokCli;
import fr.mem4csd.ramses.tests.workflows.integration.TestIntegrationInterProcessorFreeRTOSCli;
import fr.mem4csd.ramses.tests.workflows.integration.TestIntegrationInterProcessorLinuxCli;
import fr.mem4csd.ramses.tests.workflows.integration.TestIntegrationIntraProcessFreeRTOSCli;
import fr.mem4csd.ramses.tests.workflows.integration.TestIntegrationIntraProcessLinuxCli;
import fr.mem4csd.ramses.tests.workflows.integration.TestIntegrationIntraProcessPokCli;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	TestIntegrationIntraProcessLinuxCli.class,
	TestIntegrationInterProcessorLinuxCli.class,
	TestIntegrationIntraProcessPokCli.class,
	TestIntegrationInterProcessPokCli.class,
	TestIntegrationIntraProcessFreeRTOSCli.class,
	TestIntegrationInterProcessorFreeRTOSCli.class
})


public class TestIntegrationAllCli {

}
