  /**
 * AADL-RAMSES
 * 
 * Copyright ¬© 2012 TELECOM ParisTech and CNRS
 * 
 * TELECOM ParisTech/LTCI
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.  If not, see 
 * http://www.eclipse.org/org/documents/epl-v10.php
 */

#include "user_code.h"

#ifdef USE_POK
#include <libc/stdio.h>
#endif

#if defined USE_POSIX || defined USE_FREERTOS
#include <stdio.h>
#include <stdlib.h>
#endif

#ifdef USE_OSEK
#include "ecrobot_interface.h"
#endif

int counter=0;
void receive(unsigned char d)
{

  if(d<10 && d>1 && counter<45)
  {
#if defined USE_POSIX || defined USE_POK || defined USE_FREERTOS
    printf("received value: %d\n", d);
#elif defined USE_OSEK
    ecrobot_debug1(d, 0, 0);
#endif
  }
  counter++;
#if defined USE_POSIX || defined USE_FREERTOS
  if(d>=9 || counter>45)
    exit(0);
#endif
#ifdef USE_POK
  if(counter>45)
	  STOP_SELF();
#endif
}

unsigned char value=0;

void send(unsigned char * d)
{
  *d = value;
  value++;
  if(*d<10 && *d>1)
  {
#if defined USE_POSIX || defined USE_POK || defined USE_FREERTOS
    printf("send value: %d\n", *d);
#if defined USE_POSIX || defined USE_FREERTOS
	fflush(stdout);
#endif    
#elif defined USE_OSEK
    ecrobot_debug1(0, 0, d);
#endif
  }
#ifdef USE_POK
  else if(*d>15)
    	STOP_SELF();
#endif
#if defined USE_POSIX || defined USE_FREERTOS
  else if(*d>15)
    	exit(0);
#endif
}
