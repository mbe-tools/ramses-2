package fr.mem4csd.ramses.tests.workflows.integration;

import java.io.IOException;

import org.junit.Test;

import de.mdelab.workflow.impl.WorkflowExecutionException;

public abstract class TestIntegrationInterProcessCli extends TestIntegrationCli {

	private static final String INTERPROCESS_DIR = "inter-process";
	
	@Override
	protected String getRootDir()
	{
		return INTERPROCESS_DIR;
	}
	
	@Test
	public void testInterProcessCbtc() 
	throws WorkflowExecutionException, IOException {
		runTest("cbtc", "root_inst", 40);
	}
	
	@Test
	public void testInterProcessFeatureGroup() 
	throws WorkflowExecutionException, IOException {
		runTest("featuregroup", "the_cpu");
	}
	
	@Test
	public void testInterProcessPeriodicDataPorts() 
	throws WorkflowExecutionException, IOException {
		runTest("periodic-data-ports", "the_cpu");
	}

	@Test
	public void testInterProcessPeriodicDataPortsMultiCore() 
	throws WorkflowExecutionException, IOException {
		runTest("periodic-data-ports-multicore", "the_cpu");
	}
	
	@Test
	public void testInterProcessPeriodicDataPortsFlushMaf() 
	throws WorkflowExecutionException, IOException {
		runTest("periodic-data-ports-flush-maf", "the_cpu");
	}

	@Test
	public void testInterProcessPeriodicDataPortsFlushMif() 
	throws WorkflowExecutionException, IOException {
		runTest("periodic-data-ports-flush-mif", "the_cpu");
	}
	
	@Test
	public void testInterProcessPeriodicDataPortsFlushWindow() 
	throws WorkflowExecutionException, IOException {
		runTest("periodic-data-ports-flush-window", "the_cpu");
	}
	
	@Test
	public void testInterProcessPeriodicEventDataPorts() 
	throws WorkflowExecutionException, IOException {
		runTest("periodic-eventdata-ports", "platform_the_cpu", 30);
	}
	
	@Test
	public void testInterProcessPeriodicEventDataPorts1To2() 
	throws WorkflowExecutionException, IOException {
		runTest("periodic-eventdata-ports-1-to-2", "platform_the_cpu", 30);
	}
	
	@Test
	public void testInterProcessPingPong() 
	throws WorkflowExecutionException, IOException {
		runTest("pingpong", "node_1_inst");
	}
	
	
}
