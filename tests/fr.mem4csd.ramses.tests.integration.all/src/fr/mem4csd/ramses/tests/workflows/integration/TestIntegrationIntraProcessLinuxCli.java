package fr.mem4csd.ramses.tests.workflows.integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.mdelab.workflow.impl.WorkflowExecutionException;

public class TestIntegrationIntraProcessLinuxCli extends TestIntegrationIntraProcessCli {

	private static final String LINUX_TARGET_NAME = "linux";
	
	@Override
	protected boolean executeAsSudo()
	{
		return true;
	}
	
	@Override
	protected String getTargetName()
	{
		return LINUX_TARGET_NAME;
	}
	
	@Test
	public void testIntraProcessModalPortConnections() 
	throws WorkflowExecutionException, IOException {
		runTest("modal-port-connections", "the_cpu", 50);
	}
	
	@Test
	public void testIntraProcessModalPortToDataConnections() 
	throws WorkflowExecutionException, IOException {
		runTest("modal-port-to-data-connections", "the_cpu", 30);
	}
	
	@Test
	public void testIntraProcessSharedResource() 
	throws WorkflowExecutionException, IOException {
		runTest("shared-resource", "the_cpu");
	}
	
	@Test
	public void testIntraProcessThreadGroup() 
	throws WorkflowExecutionException, IOException {
		runTest("threadgroup", "the_cpu");
	}
	
	@Test
	public void testIntraProcessTimeTriggeredMxC() 
	throws WorkflowExecutionException, IOException {
		runTest("time-triggered-mxc", "cpu", 40);
	}
	
}
