/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.workflows.integration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.junit.BeforeClass;
import org.junit.Test;
import org.osate.workspace.WorkspacePlugin;

import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.WorkflowramsesnxtosekPackage;
import fr.mem4csd.ramses.pok.workflowramsespok.WorkflowramsespokPackage;
import fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.WorkflowramseslinuxintegrationPackage;
import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;
import fr.mem4csd.ramses.tests.core.AbstractRamsesPluginTest;
import fr.mem4csd.ramses.tests.core.utils.ReportComparator;

public class TestIntegrationWorkflows extends AbstractRamsesPluginTest {
	
	static final String SOURCES_PROJECT_NAME = "fr.mem4csd.ramses.tests.integration.all.sources";
	private static final URI NXTOSEK_RUNTIME_DIR_URI = URI.createURI( System.getenv("HOME")+"/ramses-resources/nxtOSEK", true );
	protected static final URI POK_RUNTIME_DIR_URI = URI.createURI( System.getenv("HOME")+"/ramses-resources/pok", true );
	protected final String LINUX_CODEGEN_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( CODEGEN_DIR ).appendSegment( "linux" ).toString();
	protected final String POK_CODEGEN_URI = BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment( CODEGEN_DIR ).appendSegment( "pok" ).toString();
	
	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		cleanResources( SOURCES_PROJECT_NAME );
		
		if ( !Platform.isRunning() ) {
			WorkflowramseslinuxPackage.eINSTANCE.eClass();
			WorkflowramseslinuxintegrationPackage.eINSTANCE.eClass();
			WorkflowramsespokPackage.eINSTANCE.eClass();
			WorkflowramsesnxtosekPackage.eINSTANCE.eClass();
		}
	}
	
	@Test
	public void testAllTargetsExecutionCoreWorkflow() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String srcAadlFile;
		final String instanceModelFile;
		
		final String rootSystemName = "main.impl";
		
		final URI instModelUri = computeInstanceModelUri( AADL_MULTI_TARGET_URI, rootSystemName );
		
		final String pokRuntimeDir, osekRuntimeDir;
		final String includeDir;
		final String reportFile;
		final String outputDir;
		
		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_MULTI_TARGET_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
			reportFile = URI.createPlatformResourceURI( REPORT_MULTI_TARGET_URI, true ).toString();
			pokRuntimeDir = URI.createPlatformResourceURI( POK_RUNTIME_DIR_URI.toString(), true ).toString();
			osekRuntimeDir = URI.createPlatformResourceURI( NXTOSEK_RUNTIME_DIR_URI.toString(), true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_MULTI_TARGET_URI.toString();
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri.toString();
			includeDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString();
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_MULTI_TARGET_URI;
			pokRuntimeDir = POK_RUNTIME_DIR_URI.toString();
			osekRuntimeDir = NXTOSEK_RUNTIME_DIR_URI.toString();
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
		}
		
		props.put(RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_MULTI_TARGET_FILE_NAME);
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, rootSystemName );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		
		props.put( RamsesWorkflowKeys.TARGET_INSTALL_DIR, "POK="+pokRuntimeDir+","+"nxtOSEK="+osekRuntimeDir);
		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		
		executeWithContext( "load_instanciate_validate_refine_and_generate_integration", props );
		
		String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( LINUX_CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + LINUX_CODEGEN_URI;
		}

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu_posix")
					.appendSegment("the_proc_posix")
					.appendSegment("the_proc_posix") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu_posix")
					.appendSegment("the_proc_posix")
					.appendSegment("the_proc_posix");//				.resolve( workflowContext.getWorkflowFileURI() );
		}
		
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( POK_CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + POK_CODEGEN_URI;
		}
		
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu_pok")
					.appendSegment("the_proc_pok")
					.appendSegment("the_proc_pok") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu_pok")
					.appendSegment("the_proc_pok")
					.appendSegment("the_proc_pok.elf");//				.resolve( workflowContext.getWorkflowFileURI() );
		}
		
		theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
		
	}
	
	@Test
	public void testAllProtocolsRefinementCoreWorkflow() 
	throws WorkflowExecutionException, IOException {
		final Map<String, String> props = createWorkflowProperties();

		final String srcAadlFile;
		final String instanceModelFile;
	
		final String rootSystemName = "root.impl";
		
		final URI instModelUri = computeInstanceModelUri( AADL_MULTI_PROTOCOL_URI, rootSystemName );
		
		final String pokRuntimeDir, osekRuntimeDir;
		final String includeDir;
		final String reportFile;
		final String outputDir, outputDirRef;
		
		final String refinedTraceFile, refinedAadlFile;
		
		if (Platform.isRunning()) {
			srcAadlFile = URI.createPlatformResourceURI( AADL_MULTI_PROTOCOL_URI.toString(), true ).toString();
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
			reportFile = URI.createPlatformResourceURI( REPORT_MULTI_PROTOCOL_URI, true ).toString();
			pokRuntimeDir = URI.createPlatformResourceURI( POK_RUNTIME_DIR_URI.toString(), true ).toString();
			osekRuntimeDir = URI.createPlatformResourceURI( NXTOSEK_RUNTIME_DIR_URI.toString(), true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			outputDirRef = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).toString(), true ).toString();
			refinedAadlFile = URI.createPlatformResourceURI( AADL_MULTI_PROTOCOL_REFINED_URI, true ).toString();
			refinedTraceFile = URI.createPlatformResourceURI( TRACE_MULTI_PROTOCOL_REFINED_URI, true ).toString();
		}
		else {
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_MULTI_PROTOCOL_URI.toString();
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri.toString();
			includeDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString();
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_MULTI_PROTOCOL_URI;
			pokRuntimeDir = POK_RUNTIME_DIR_URI.toString();
			osekRuntimeDir = NXTOSEK_RUNTIME_DIR_URI.toString();
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			outputDirRef = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).toString();
			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_MULTI_PROTOCOL_REFINED_URI;
			refinedTraceFile = TEST_SOURCES_ABS_LOCATION + TRACE_MULTI_PROTOCOL_REFINED_URI;
			
		}
		
		props.put(RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_MULTI_PROTOCOL_FILE_NAME);
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, rootSystemName );
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		
		props.put( RamsesWorkflowKeys.TARGET_INSTALL_DIR, "POK="+pokRuntimeDir+","+"OSEK="+osekRuntimeDir);
		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		
		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );
		props.put( RamsesWorkflowKeys.REFINED_TRACE_FILE, refinedTraceFile );
		
		executeWithContext( "load_instanciate_and_refine_all_protocols", props );
		
		String refinedAadlFileRef,refinedTraceFileRef,refinedAadlFileCmp, refinedTraceFileCmp;
		if (Platform.isRunning()) {
			refinedAadlFileCmp = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment("refined-models").appendSegment("remote-communications").appendSegment("mqtt").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME +"-remote-communications" ).appendFileExtension(WorkspacePlugin.SOURCE_FILE_EXT).toString(), true ).toString();
			refinedTraceFileCmp = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment("refined-models").appendSegment("remote-communications").appendSegment("mqtt").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME +"-remote-communications" ).appendFileExtension(TRACE_EXT).toString(), true ).toString();
			refinedAadlFileRef = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment("mqtt").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME ).appendFileExtension(WorkspacePlugin.SOURCE_FILE_EXT).toString(), true ).toString();
			refinedTraceFileRef = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment("mqtt").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME ).appendFileExtension(TRACE_EXT).toString(), true ).toString();
		} else {
			refinedAadlFileCmp = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment("refined-models").appendSegment("remote-communications").appendSegment("mqtt").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME +"-remote-communications").appendFileExtension(WorkspacePlugin.SOURCE_FILE_EXT).toString();
			refinedTraceFileCmp = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment("refined-models").appendSegment("remote-communications").appendSegment("mqtt").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME +"-remote-communications").appendFileExtension(TRACE_EXT).toString();
			refinedAadlFileRef = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment("mqtt").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME ).appendFileExtension(WorkspacePlugin.SOURCE_FILE_EXT).toString();
			refinedTraceFileRef = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment("mqtt").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME ).appendFileExtension(TRACE_EXT).toString();
		}

		final ReportComparator cmpAadlMqtt = new ReportComparator( refinedAadlFileRef, refinedAadlFileCmp, getResourceSet() );
		assertTrue( cmpAadlMqtt.checkResults() );
		
		final ReportComparator cmpTraceMqtt = new ReportComparator( refinedTraceFileRef, refinedTraceFileCmp, getResourceSet() );
		assertTrue( cmpTraceMqtt.checkResults() );
		
		if (Platform.isRunning()) {
			refinedAadlFileCmp = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment("refined-models").appendSegment("remote-communications").appendSegment("sockets").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME +"-remote-communications" ).appendFileExtension(WorkspacePlugin.SOURCE_FILE_EXT).toString(), true ).toString();
			refinedTraceFileCmp = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment("refined-models").appendSegment("remote-communications").appendSegment("sockets").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME +"-remote-communications" ).appendFileExtension(TRACE_EXT).toString(), true ).toString();
			refinedAadlFileRef = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment("sockets").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME ).appendFileExtension(WorkspacePlugin.SOURCE_FILE_EXT).toString(), true ).toString();
			refinedTraceFileRef = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment("sockets").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME ).appendFileExtension(TRACE_EXT).toString(), true ).toString();
		} else {
			refinedAadlFileCmp = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment("refined-models").appendSegment("remote-communications").appendSegment("sockets").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME +"-remote-communications" ).appendFileExtension(WorkspacePlugin.SOURCE_FILE_EXT).toString();
			refinedTraceFileCmp = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).appendSegment("refined-models").appendSegment("remote-communications").appendSegment("sockets").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME +"-remote-communications" ).appendFileExtension(TRACE_EXT).toString();
			refinedAadlFileRef = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment("sockets").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME ).appendFileExtension(WorkspacePlugin.SOURCE_FILE_EXT).toString();
			refinedTraceFileRef = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegment("sockets").appendSegment( AADL_MULTI_PROTOCOL_FILE_NAME ).appendFileExtension(TRACE_EXT).toString();
		}
		
		final ReportComparator cmpAadlSockets = new ReportComparator( refinedAadlFileRef, refinedAadlFileCmp, getResourceSet() );
		assertTrue( cmpAadlSockets.checkResults() );
		
		final ReportComparator cmpTraceSockets = new ReportComparator( refinedTraceFileRef, refinedTraceFileCmp, getResourceSet() );
		assertTrue( cmpTraceSockets.checkResults() );
	}
	
	@Override
	protected String[] getWorkflowDir() {
		String[] res = {"ramses", "integration", "workflows"};
		return res;
	}
	
	@Override
	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.ramses.integration.all";
	}

	@Override
	protected String getSourcesProjectName() {
		return SOURCES_PROJECT_NAME;
	}

}
