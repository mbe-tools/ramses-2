package fr.mem4csd.ramses.tests.workflows.integration;

import java.io.IOException;

import org.junit.Test;

import de.mdelab.workflow.impl.WorkflowExecutionException;

public abstract class TestIntegrationIntraProcessCli extends TestIntegrationCli {

	private static final String INTRAPROCESS_DIR = "intra-process";
	
	@Override
	protected String getRootDir()
	{
		return INTRAPROCESS_DIR;
	}
	
	@Test
	public void testIntraProcessFeatureGroup() 
	throws WorkflowExecutionException, IOException {
		runTest("featuregroup", "the_cpu");
	}
	
	@Test
	public void testIntraProcessPeriodicDataPorts() 
	throws WorkflowExecutionException, IOException {
		runTest("periodic-data-ports", "the_cpu");
	}

	@Test
	public void testIntraProcessPeriodicEventDataPorts() 
	throws WorkflowExecutionException, IOException {
		runTest("periodic-eventdata-ports", "the_cpu", 30);
	}
	
	@Test
	public void testIntraProcessPeriodicDelayedEventDataPorts() 
	throws WorkflowExecutionException, IOException {
		runTest("periodic-delayed-eventdata-ports", "the_cpu", 30);
	}
	
	@Test
	public void testIntraProcessPingPong() 
	throws WorkflowExecutionException, IOException {
		runTest("ping-pong", "node_1_inst");
	}
		
	@Test
	public void testIntraProcessSporadic() 
	throws WorkflowExecutionException, IOException {
		runTest("sporadic", "the_cpu");
	}
	
	@Test
	public void testIntraProcessHybrid() 
	throws WorkflowExecutionException, IOException {
		runTest("hybrid", "the_cpu");
	}
	
	@Test
	public void testIntraProcessTimedEventPorts() 
	throws WorkflowExecutionException, IOException {
		runTest("timed-eventports", "the_cpu");
	}
	
	@Test
	public void testIntraProcessCbtc() 
	throws WorkflowExecutionException, IOException {
		runTest("cbtc", "root_inst", 40);
	}
	
}
