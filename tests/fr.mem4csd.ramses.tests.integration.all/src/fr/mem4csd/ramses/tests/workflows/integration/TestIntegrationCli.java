/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.tests.workflows.integration;

import static org.junit.Assert.assertTrue;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.junit.Before;

import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.cli.RamsesCli;
import fr.mem4csd.ramses.tests.core.AbstractRamsesTest;

public abstract class TestIntegrationCli extends AbstractRamsesTest {
	
	
	private static final String OUTPUT_DIR = "output";
	private static final String OUTPUT_DIR_REF = "output_ref";
	private static final String CODEGEN_DIR = "generated-code";
	private static final String INTEGRATION_TEST_DIR = "integration-tests";
	private static final String EXEC_TRACE_EXT = "exec_trace";	
	
	protected static final URI BASE_TEST_MODEL_DIR_URI = URI.createURI( TestIntegrationWorkflows.SOURCES_PROJECT_NAME, true ).appendSegment( INTEGRATION_TEST_DIR );

	@Before
	public void cleanResources()
	throws CoreException, InterruptedException, IOException {
		final File outputDir = new File( "../" + BASE_TEST_MODEL_DIR_URI + "/" + OUTPUT_DIR );
		
		if ( outputDir.exists() ) {
			FileUtils.cleanDirectory( outputDir );
		}
	}
	
	
	protected void cleanResources(String[] outputDirParents)
	throws CoreException, InterruptedException, IOException {
		URI uri = BASE_TEST_MODEL_DIR_URI;
		for(int i=0;i<outputDirParents.length;i++)
			uri = uri.appendSegment(outputDirParents[i]);
		
		final File outputDir = new File( "../" + uri.appendSegment( OUTPUT_DIR ) );
		
		if ( outputDir.exists() ) {
			FileUtils.cleanDirectory( outputDir );
		}
	}
	
	
	protected void runTest(String fileName, String processorName)
			throws WorkflowExecutionException, IOException
	{
		runTest(fileName, processorName, 20);
	}
	
	protected abstract String getRootDir();
	
	protected abstract String getTargetName();
	
	protected int getSleepTime()
	{
		return 0;
	}
		
	protected void runTest(String fileName, String processorName, int timeout)
			throws WorkflowExecutionException, IOException
	{
		runTest(fileName, processorName, timeout, 0);
	}
	
	protected void runTest(String fileName, String processorName, int timeout, int linesFromEnd)
			throws WorkflowExecutionException, IOException
	{
		String[] processorNameArray = {processorName};
		runTest(fileName, processorNameArray, timeout, linesFromEnd);
	}
	
	protected void runTest(String fileName, String[] processorNameArray, int timeout)
			throws WorkflowExecutionException, IOException
	{
		runTest(fileName, processorNameArray, timeout, 0);
	}
	
	// linesFromEnd is used to compare only the last line because the execution trace may start
	// with system/emulator/network initialization info that is not in RAMSES runtime or generated code
	protected void runTest(String fileName, String[] processorNameArray, int timeout, int linesFromEnd)
			throws WorkflowExecutionException, IOException
	{
		String[] currentTestDir = {getRootDir(),fileName};
		
		try {
			cleanResources(currentTestDir);
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String[] args = {"../"+getSourcesProjectName()+"/"+INTEGRATION_TEST_DIR+"/"+currentTestDir[0]+"/"+currentTestDir[1]+"/"+"config-"+getTargetName()+".properties"};
		
		RamsesCli.main(args);
		
		URI codeGendURI = BASE_TEST_MODEL_DIR_URI.appendSegment(currentTestDir[0]).appendSegment(currentTestDir[1]).appendSegment( OUTPUT_DIR ).appendSegment(CODEGEN_DIR).appendSegment(getTargetName());

		for(String processorName: processorNameArray)
		{
			String execTraceFile = URI.createURI("../"+codeGendURI.appendSegment( processorName ).appendSegment( processorName ).appendFileExtension(EXEC_TRACE_EXT).toString(), true ).toString();
			String execTraceFileRef = URI.createURI("../"+BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR_REF ).appendSegments( currentTestDir ).appendSegment( getTargetName() ).appendSegment( processorName ).appendFileExtension(EXEC_TRACE_EXT).toString(), true ).toString();

			testGeneratedCodeExecution("../"+codeGendURI.toString(), timeout, TimeUnit.SECONDS);
			try {
				Thread.sleep(1000);

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			File execTrace = new File(execTraceFile);
			boolean exists = execTrace.exists();
			assertTrue(exists);

			File execTraceRef = new File(execTraceFileRef);
			exists = execTraceRef.exists();
			assertTrue(exists);

			try {
				Thread.sleep(getSleepTime()*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			boolean sameTraces = FileUtils.contentEquals(execTrace, execTraceRef);
			System.out.println("Ref file path: "+execTraceRef.getAbsolutePath());
			System.out.println("Act file path: "+execTrace.getAbsolutePath());
			
			//long length = Math.max(execTrace.length(), execTraceRef.length());
			
			if(linesFromEnd<0)
				linesFromEnd=-linesFromEnd;
			
			if(sameTraces)
				assertTrue(true);
			else
			{
				long totalExecTrace = getLinesNumber(execTrace);
				
				String cmd = "diff <( tail -n +"+String.valueOf(totalExecTrace-linesFromEnd+1)+ " "+ execTrace.getAbsolutePath()+ ") <( tail -n +0 "+ execTraceRef.getAbsolutePath() + ")";
				
				String[] command = new String[]{"bash", "-c", cmd};
				
				// Without bash -c (does not work)
				// String[] command = new String[]{"diff", "<(", "tail", "-n", "+"+String.valueOf(totalExecTrace-linesFromEnd+1), execTrace.getAbsolutePath(), ")", "<(","tail", "-n", "+0" , execTraceRef.getAbsolutePath(), ")"};

				// Without excusion of first lines:
				// String[] command = new String[]{"diff", execTraceRef.getAbsolutePath(), execTraceRef.getAbsolutePath()};
				
				Process p = Runtime.getRuntime().exec(command);
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String line;
				int added=0, removed=0;
				while ((line = input.readLine()) != null) {
					if(line.startsWith(">"))
					{
						added++;
					}
					if(line.startsWith("<"))
						removed++;
				}
				float maxDiff = removed;
				if(added>removed)
					maxDiff = added;

				input.close();

				float ratio = maxDiff/(totalExecTrace-linesFromEnd); // linesFromEnd >= 0
				assertTrue(ratio<0.2F);
			}
		}

	}
	
	private long getLinesNumber(File f) throws IOException
	{
		long total;
		InputStream is = new BufferedInputStream(new FileInputStream(f.getAbsolutePath()));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			while ((readChars = is.read(c)) != -1) {
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			total = count;
		} finally {
			is.close();
		}
		return total;
	}
	
	@Override
	protected String getSourcesProjectName() {
		return TestIntegrationWorkflows.SOURCES_PROJECT_NAME;
	}
	
}
