package fr.mem4csd.ramses.tests.workflows.integration;

import java.util.ArrayList;
import java.util.List;

public class TestIntegrationInterProcessPokCli extends TestIntegrationInterProcessCli {
	
	private static final String POK_TARGET_NAME = "pok";
	
	@Override
	protected int getSleepTime()
	{
		return 60;
	}
	
	@Override
	protected String getTargetName()
	{
		return POK_TARGET_NAME;
	}
	
	@Override
	protected List<String> getProgramNameToKill()
	{
		List<String> progNameList = new ArrayList<String>();
		progNameList.add("qemu");
		return progNameList;
	}
	
}
