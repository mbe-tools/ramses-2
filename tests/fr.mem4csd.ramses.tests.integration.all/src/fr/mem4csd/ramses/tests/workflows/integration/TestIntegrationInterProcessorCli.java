package fr.mem4csd.ramses.tests.workflows.integration;

import java.io.IOException;

import org.junit.Test;

import de.mdelab.workflow.impl.WorkflowExecutionException;

public abstract class TestIntegrationInterProcessorCli extends TestIntegrationCli {

	@Override
	protected String getRootDir() {
		return "inter-processor";
	}

	@Test
	public void testInterProcessorPingSockets() 
	throws WorkflowExecutionException, IOException {
		runTest("ping", "the_cpu2", 30, 7);
	}
	
	@Test
	public void testInterProcessorPingMqtt() 
	throws WorkflowExecutionException, IOException {
		runTest("ping-mqtt", "the_cpu2", 30, 7);
	}
	
	@Test
	public void testInterProcessorPingPongSockets() 
	throws WorkflowExecutionException, IOException {
		runTest("pingpong", "the_cpu1", 30, 7);
	}
	
	@Test
	public void testInterProcessor1To2Sockets() 
	throws WorkflowExecutionException, IOException {
		String[] processorNamesArray = {"the_cpu2", "the_cpu3"};
		runTest("1-to-2", processorNamesArray, 30, 7);
	}

}
