package fr.mem4csd.ramses.tests.workflows.integration;

public class TestIntegrationInterProcessorFreeRTOSCli extends TestIntegrationInterProcessorCli {

	
	private static final String FREERTOS_TARGET_NAME = "freertos";
	
	@Override
	protected boolean executeAsSudo()
	{
		return true;
	}
	
	@Override
	protected String getTargetName()
	{
		return FREERTOS_TARGET_NAME;
	}
}
