package fr.mem4csd.ramses.tests.workflows.integration;

public class TestIntegrationInterProcessorLinuxCli extends TestIntegrationInterProcessorCli {

	private static final String LINUX_TARGET_NAME = "linux";
	
	@Override
	protected boolean executeAsSudo()
	{
		return true;
	}
	
	@Override
	protected String getTargetName()
	{
		return LINUX_TARGET_NAME;
	}
	
}
