/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */
package fr.mem4csd.ramses.tests.integration.linux;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.junit.BeforeClass;
import org.junit.Test;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;
import fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.WorkflowramseslinuxintegrationPackage;
import fr.mem4csd.ramses.tests.core.AbstractRamsesPluginTest;

public class TestRamsesLinuxIntegrationExtension extends AbstractRamsesPluginTest {
	
	private static final String SYSTEM_IMPL_NAME = "root.posix";
	private static final String SOURCES_PROJECT_NAME = "fr.mem4csd.ramses.tests.linux.integration.sources";
	
	protected boolean executeAsSudo()
	{
		return true;
	}
	
	@BeforeClass
	public static void cleanResources()
	throws CoreException, InterruptedException, IOException {
		cleanResources( SOURCES_PROJECT_NAME );

		if ( !Platform.isRunning() ) {
			WorkflowramseslinuxPackage.eINSTANCE.eClass();
			WorkflowramseslinuxintegrationPackage.eINSTANCE.eClass();
		}
	}
	
	@Test
	public void testSocketPosixExecutionLinux() 
			throws WorkflowExecutionException, IOException, CoreException, InterruptedException {
		
		cleanResources();
		
		final Map<String, String> props = createWorkflowProperties();
		
		final String srcAadlFile;
		final String includeDir;
		final String outputDir;
		final String instanceModelFile;
		final String reportFile;

		final URI instModelUri = computeInstanceModelUri( AADL_PING_URI, "root.sockets" );
		
		if (Platform.isRunning()) {
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_PING_URI.toString(), true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
			reportFile = URI.createPlatformResourceURI( REPORT_PING_INVALID_URI, true ).toString();
		} else {
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_PING_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			includeDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.toString();
			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_PING_INVALID_URI;
		}
		
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, "root.sockets" );
		
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_PING_FILE_NAME );
		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile);
				
		/*final WorkflowExecutionContext workflowContext =*/ executeWithContext( "load_instanciate_validate_refine_and_generate_integration_linux", props );

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}

		URI theProc1URI = null;
		if (Platform.isRunning()) {
			theProc1URI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu1")
					.appendSegment("the_proc1")
					.appendSegment("the_proc1") );
		} else {
			theProc1URI = URI.createURI( workingDir )
					.appendSegment("the_cpu1")
					.appendSegment("the_proc1")
					.appendSegment("the_proc1");
		}
		File theProc1Executable = new File( theProc1URI.toFileString() );
		
		assertTrue(theProc1Executable.exists());
		
		URI theProc2URI = null;
		if (Platform.isRunning()) {
			theProc2URI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu2")
					.appendSegment("the_proc2")
					.appendSegment("the_proc2") );
		} else {
			theProc2URI = URI.createURI( workingDir )
					.appendSegment("the_cpu2")
					.appendSegment("the_proc2")
					.appendSegment("the_proc2");
		}
		
		File theProc2Executable = new File( theProc2URI.toFileString() );
		
		assertTrue(theProc2Executable.exists());
	}
	
	@Test
	public void testNoSocketPosixExecutionLinux() 
			throws WorkflowExecutionException, IOException {
		
		
		final Map<String, String> props = createWorkflowProperties();
		
		final String srcAadlFile;
		final String includeDir;
		final String outputDir;
		final String instanceModelFile;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		
		if (Platform.isRunning()) {
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
			
		} else {
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();

			// Code gen directories
			final String absLoc = URI.createURI( TEST_SOURCES_ABS_LOCATION ).toFileString();
			includeDir = absLoc + BASE_TEST_MODEL_DIR_URI.toString();
		}
		
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, "main.posix" );


		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_SAMPLED_COM_FILE_NAME );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile);
		
		final WorkflowExecutionContext workflowContext = executeWithContext( "load_instanciate_validate_refine_and_generate_integration_linux", props );

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc");//				.resolve( workflowContext.getWorkflowFileURI() );
		}
		
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
		
	}
	
	@Test
	public void testMqttLinuxExecutionPing() 
			throws WorkflowExecutionException, IOException {
		
		final Map<String, String> props = createWorkflowProperties();
		
		final String srcAadlFile;
		final String includeDir;
		final String outputDir;
		final String instanceModelFile;
//		final String reportFile;

		final URI instModelUri = computeInstanceModelUri( AADL_PING_URI, "root.mqtt" );
		
		if (Platform.isRunning()) {
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_PING_URI.toString(), true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
//			reportFile = URI.createPlatformResourceURI( REPORT_PING_INVALID_URI, true ).toString();
		} else {
			
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_PING_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
			includeDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.toString();
//			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_PING_INVALID_URI;
			
		}
		
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, "root.mqtt" );
		
		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_PING_FILE_NAME );
//		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile);
		
		/*final WorkflowExecutionContext workflowContext =*/ executeWithContext( "load_instanciate_validate_refine_and_generate_integration_linux", props );

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}

		URI theProc1URI = null;
		if (Platform.isRunning()) {
			theProc1URI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu1")
					.appendSegment("the_proc1")
					.appendSegment("the_proc1") );
		} else {
			theProc1URI = URI.createURI( workingDir )
					.appendSegment("the_cpu1")
					.appendSegment("the_proc1")
					.appendSegment("the_proc1");
		}
		File theProc1Executable = new File( theProc1URI.toFileString() );
		
		assertTrue(theProc1Executable.exists());
		
		URI theProc2URI = null;
		if (Platform.isRunning()) {
			theProc2URI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu2")
					.appendSegment("the_proc2")
					.appendSegment("the_proc2") );
		} else {
			theProc2URI = URI.createURI( workingDir )
					.appendSegment("the_cpu2")
					.appendSegment("the_proc2")
					.appendSegment("the_proc2");
		}
		File theProc2Executable = new File( theProc2URI.toFileString() );
		
		assertTrue(theProc2Executable.exists());

		
	}
	
	@Test
	public void testNoMqttPosixExecutionLinux() 
			throws WorkflowExecutionException, IOException {
		
		
		final Map<String, String> props = createWorkflowProperties();
		
		final String srcAadlFile;
		final String includeDir;
		final String outputDir;
		final String instanceModelFile;
//		final String reportFile;
//		final String refinedAadlFile;
		
		final URI instModelUri = computeInstanceModelUri( AADL_SAMPLED_COM_URI, SYSTEM_IMPL_NAME );
		
		if (Platform.isRunning()) {
			instanceModelFile = URI.createPlatformResourceURI( instModelUri.toString(), true ).toString();
			srcAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_URI.toString(), true ).toString();
			outputDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString(), true ).toString();
			includeDir = URI.createPlatformResourceURI( BASE_TEST_MODEL_DIR_URI.appendSegment( INPUT_DIR ).toString(), true ).toString();
//			reportFile = URI.createPlatformResourceURI( REPORT_SAMPLED_COM_INVALID_URI, true ).toString();
//			refinedAadlFile = URI.createPlatformResourceURI( AADL_SAMPLED_COM_REFINED_URI, true ).toString();
			
		} else {
			instanceModelFile = TEST_SOURCES_ABS_LOCATION + instModelUri;
			srcAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_URI;
			outputDir = TEST_SOURCES_ABS_LOCATION + BASE_TEST_MODEL_DIR_URI.appendSegment( OUTPUT_DIR ).toString();
//			reportFile = TEST_SOURCES_ABS_LOCATION + REPORT_SAMPLED_COM_INVALID_URI;
//			refinedAadlFile = TEST_SOURCES_ABS_LOCATION + AADL_SAMPLED_COM_REFINED_URI;

			// Code gen directories
			final String absLoc = URI.createURI( TEST_SOURCES_ABS_LOCATION ).toFileString();
			includeDir = absLoc + BASE_TEST_MODEL_DIR_URI.toString();
		}
		
		props.put( RamsesWorkflowKeys.SOURCE_AADL_FILE, srcAadlFile );
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile );
		props.put( RamsesWorkflowKeys.INCLUDE_DIR, includeDir );
		props.put( RamsesWorkflowKeys.OUTPUT_DIR, outputDir );
		props.put( RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, "main.posix" );
		
//		props.put( RamsesWorkflowKeys.REFINED_AADL_FILE, refinedAadlFile );

		props.put( RamsesWorkflowKeys.SOURCE_FILE_NAME, AADL_SAMPLED_COM_FILE_NAME );
//		props.put( RamsesWorkflowKeys.VALIDATION_REPORT_FILE, reportFile);
		props.put( RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceModelFile);
		
		
		final WorkflowExecutionContext workflowContext = executeWithContext( "load_instanciate_validate_refine_and_generate_integration_linux", props );

		final String workingDir;
		
		if (Platform.isRunning()) {
			workingDir = URI.createPlatformResourceURI( CODEGEN_URI , true ).toString();
		} else {
			workingDir = TEST_SOURCES_ABS_LOCATION + CODEGEN_URI;
		}

		URI theProcURI = null;
		if (Platform.isRunning()) {
			theProcURI = CommonPlugin.resolve( URI.createFileURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc") );
		} else {
			theProcURI = URI.createURI( workingDir )
					.appendSegment("the_cpu")
					.appendSegment("the_proc")
					.appendSegment("the_proc");//				.resolve( workflowContext.getWorkflowFileURI() );
		}
		
		File theProcExecutable = new File( theProcURI.toFileString() );
		
		assertTrue(theProcExecutable.exists());
		
	}
	
	@Override
	protected String getSourcesProjectName() {
		return SOURCES_PROJECT_NAME;
	}

	@Override
	protected String getWorkflowProjectDir() {
		return "fr.mem4csd.ramses.linux.integration";
	}
	
	@Override
	protected String[] getWorkflowDir() {
		String[] res = {"ramses", "linux", "integration_workflows"};
		return res;
	}
	
	@Override
	protected String getTargetName()
	{
		return "linux";
	}
}
