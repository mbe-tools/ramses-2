/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.integration.all;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.ProcessSubcomponent;
import org.osate.aadl2.Subcomponent;

import fr.mem4csd.ramses.core.codegen.AbstractMakefileUnparser;
import fr.mem4csd.ramses.core.codegen.GenerationException;

public class IntegrationMakefileUnparser extends AbstractMakefileUnparser{

	public static final String INTERGRATION_TARGET_ID = "All protocols and targets";

	@Override
	public String getTargetName() {
		return "All";
	}

	@Override
	public String getTargetShortDescription() {
		return "(all available protocols and targets)";
	}

	@Override
	public boolean validateTargetPath(URI runtimePath) {
		return true;
	}


	
	@Override
	public void generateProcessorBuild(Subcomponent processor, URI processorDir, IProgressMonitor monitor) throws GenerationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void generateProcessBuild(ProcessSubcomponent process, URI processDir, IProgressMonitor monitor)
			throws GenerationException {
		// TODO Auto-generated method stub
		
	}
	
}