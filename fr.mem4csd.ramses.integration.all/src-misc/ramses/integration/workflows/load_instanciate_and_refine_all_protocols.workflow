<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_he6BYPM4EeqsDOGYuOmxLA" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_WjHWUAk-EeuixIGZFNyO-g" name="workflowDelegation" workflowURI="load_all_transformation_resources.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_b86FgAk-EeuixIGZFNyO-g" name="workflowDelegation" workflowURI="load_all_aadl_resources.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_tZy9sAk_EeuixIGZFNyO-g" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/instanciate.workflow">
    <propertyValues xmi:id="_vN0gEAk_EeuixIGZFNyO-g" name="instance_model_file" defaultValue="${instance_model_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_yfz1QAk_EeuixIGZFNyO-g" name="source_aadl_file" defaultValue="${source_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_0T1-sAk_EeuixIGZFNyO-g" name="system_implementation_name" defaultValue="${system_implementation_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_F1vW4BCNEeuDWeS4dZ-6Dg" name="workflowDelegation" workflowURI="refine_all_protocols.workflow">
    <propertyValues xmi:id="_e5g9YBCNEeuDWeS4dZ-6Dg" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_hSDiEBCNEeuDWeS4dZ-6Dg" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_jJHxYBCNEeuDWeS4dZ-6Dg" name="include_dir" defaultValue="${include_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_k_onEBCNEeuDWeS4dZ-6Dg" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_RAT4AKDmEeqR3ds1UXwt-g" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_WpUu8KDmEeqR3ds1UXwt-g" name="instance_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_jAEpMKDmEeqR3ds1UXwt-g" name="refined_trace_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_mcjXIKDmEeqR3ds1UXwt-g" name="validation_report_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_2FwDgKE8EeqgtNEZpZXkFQ" name="refined_aadl_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_YZkB4KEEEeqR3ds1UXwt-g" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_a1l4kLDgEeqLE8HsirCK_w" name="source_file_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_5VZQMPNKEeqsDOGYuOmxLA" fileURI="default_integration.properties"/>
</workflow:Workflow>
