<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_hSY4kPHpEeqsDOGYuOmxLA" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_BMW58AjXEeuixIGZFNyO-g" name="workflowDelegation" workflowURI="${integration_workflows_path}load_all_aadl_resources.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_isCqQAkuEeuixIGZFNyO-g" name="workflowDelegation" workflowURI="${integration_workflows_path}load_all_transformation_resources.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_WYti8AjXEeuixIGZFNyO-g" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/instanciate.workflow">
    <propertyValues xmi:id="__89SIAjXEeuixIGZFNyO-g" name="source_aadl_file" defaultValue="${source_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_CjpnkAjYEeuixIGZFNyO-g" name="system_implementation_name" defaultValue="${system_implementation_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_nlE1wAkuEeuixIGZFNyO-g" name="workflowDelegation" workflowURI="${integration_workflows_path}validate_refine_and_generate_integration.workflow">
    <propertyValues xmi:id="_GcLDwAlFEeuSHr8HPIOy3g" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_5vr4IAlGEeuSHr8HPIOy3g" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_23TD0AlIEeuSHr8HPIOy3g" name="include_dir" defaultValue="${include_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_-cZ0AAlIEeuSHr8HPIOy3g" name="target_install_dir" defaultValue="${target_install_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_SYhHsAlKEeuixIGZFNyO-g" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_Wro5oF4rEeqYp8ezKVdS_w" name="include_dir" description="${description_include_dir}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_auww0F4rEeqYp8ezKVdS_w" name="source_aadl_file" defaultValue="${source_aadl_file}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_4lCdAF4rEeqYp8ezKVdS_w" name="system_implementation_name" defaultValue="${system_implementation_name}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_RvqH0F4rEeqYp8ezKVdS_w" name="output_dir" defaultValue="${output_dir}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_celxIPKTEeqsDOGYuOmxLA" name="runtime_scheme" defaultValue="${runtime_scheme}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_wmXVsPKpEeqsDOGYuOmxLA" name="target_install_dir" defaultValue="${target_install_dir}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_8CzoMNpdEeq9fI4NRnUBRA" fileURI="default_integration.properties"/>
  <propertiesFiles xmi:id="_FJzm8AjXEeuixIGZFNyO-g" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties"/>
</workflow:Workflow>
