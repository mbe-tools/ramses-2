<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_BXDDIAKZEeuXP8YXOvMHxQ" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_rcx-EAMCEeuXP8YXOvMHxQ" name="workflowDelegation" workflowURI="${scheme}${ramses_linux_plugin}ramses/linux/workflows/load_transformation_resources_linux.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_FY7-EAMDEeuXP8YXOvMHxQ" name="workflowDelegation" workflowURI="${scheme}${ramses_mqtt_plugin}ramses/mqtt/workflows/load_transformation_resources_mqtt.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_4hM2oAMCEeuXP8YXOvMHxQ" name="workflowDelegation" workflowURI="${scheme}${ramses_nxtosek_plugin}ramses/nxtosek/workflows/load_transformation_resources_nxtosek.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_8X1J8AMCEeuXP8YXOvMHxQ" name="workflowDelegation" workflowURI="${scheme}${ramses_pok_plugin}ramses/pok/workflows/load_transformation_resources_pok.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_IRb1MAMDEeuXP8YXOvMHxQ" name="workflowDelegation" workflowURI="${scheme}${ramses_sockets_plugin}ramses/sockets/workflows/load_transformation_resources_sockets.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_QWfC4EjiEeujCYcfh2N14w" name="workflowDelegation" workflowURI="${scheme}${ramses_freertos_plugin}ramses/freertos/workflows/load_transformation_resources_freertos.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_LVE6MFQ1EeugtZQkCVSjcg" name="workflowDelegation" workflowURI="${scheme}${ramses_ev3dev_plugin}ramses/ev3dev/workflows/load_transformation_resources_ev3dev.workflow"/>
  <propertiesFiles xmi:id="_sB-V0AMCEeuXP8YXOvMHxQ" fileURI="default_integration.properties"/>
</workflow:Workflow>
