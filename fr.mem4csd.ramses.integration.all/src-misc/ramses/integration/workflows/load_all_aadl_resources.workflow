<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_BXDDIAKZEeuXP8YXOvMHxQ" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_CdNMwAMDEeuXP8YXOvMHxQ" name="workflowDelegation" workflowURI="${scheme}${ramses_osek_plugin}ramses/osek/workflows/load_aadl_resources_osek.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_8X1J8AMCEeuXP8YXOvMHxQ" name="workflowDelegation" workflowURI="${scheme}${ramses_pok_plugin}ramses/pok/workflows/load_aadl_resources_pok.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_DHPaYEjiEeujCYcfh2N14w" name="workflowDelegation" workflowURI="${scheme}${ramses_freertos_plugin}ramses/freertos/workflows/load_aadl_resources_freertos.workflow"/>
  <propertiesFiles xmi:id="_sB-V0AMCEeuXP8YXOvMHxQ" fileURI="default_integration.properties"/>
</workflow:Workflow>
