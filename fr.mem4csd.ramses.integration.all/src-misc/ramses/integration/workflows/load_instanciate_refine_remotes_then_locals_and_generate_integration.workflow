<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowramses="https://mem4csd.telecom-paris.fr/ramses/workflowramses" xmi:id="_he6BYPM4EeqsDOGYuOmxLA" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_IdpnUAJ2EeuQd8rPO2uDsw" name="workflowDelegation" workflowURI="{integration_workfows_path}load_all_aadl_resources.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_Vw_FoAJ2EeuQd8rPO2uDsw" name="workflowDelegation" workflowURI="{integration_workfows_path}load_all_transformation_resources.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_tuuVgAKUEeuXP8YXOvMHxQ" name="workflowDelegation" workflowURI="${aadl_model_instanciation_workflow}">
    <propertyValues xmi:id="_exCb8AKZEeuXP8YXOvMHxQ" name="system_implementation_name" defaultValue="${system_implementation_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_pe_MYAKZEeuXP8YXOvMHxQ" name="source_aadl_file" defaultValue="${source_dir_uri}/${source_file_name}.aadl">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_9vIVwAkxEeuixIGZFNyO-g" name="workflowDelegation" workflowURI="validate_integration.workflow"/>
  <components xsi:type="workflowramses:ConditionEvaluationProtocol" xmi:id="_ZPPKoAJ1EeuQd8rPO2uDsw" name="conditionEvaluationProtocol" instanceModelSlot="sourceAadlInstance" resultModelSlot="hasMQTT" protocols="MQTT"/>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_ZdpkoAJ2EeuQd8rPO2uDsw" name="hasMQTT" conditionSlot="hasMQTT">
    <onTrue xmi:id="_a9auQAJ2EeuQd8rPO2uDsw" name="onTrue (has MQTT communications)">
      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_DTo1QPM7EeqsDOGYuOmxLA" name="workflowDelegation" workflowURI="${scheme}${ramses_mqtt_plugin}ramses/mqtt/workflows/refine_mqtt.workflow">
        <propertyValues xmi:id="_eDYKEKEEEeqR3ds1UXwt-g" name="output_dir" defaultValue="${output_dir}/mqtt">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_VCf9QKD1EeqDXb0OjUP0yw" name="refined_trace_file" defaultValue="${output_dir}/${source_file_name}.arch_trace">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_NE2cgKE9EeqxdbEz6rHidg" name="refined_aadl_file" defaultValue="${output_dir}/${source_file_name}.aadl">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
      </components>
    </onTrue>
  </components>
  <components xsi:type="workflowramses:ConditionEvaluationProtocol" xmi:id="_bQepoAJ1EeuQd8rPO2uDsw" name="conditionEvaluationProtocol" instanceModelSlot="sourceAadlInstance" resultModelSlot="hasMQTT"/>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_B0fw8AKVEeuXP8YXOvMHxQ" name="hasSockets" conditionSlot="hasSockets">
    <onTrue xmi:id="_CfAGgAKVEeuXP8YXOvMHxQ" name="onTrue (has Sockets communications)">
      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_GPCPYPM7EeqsDOGYuOmxLA" name="workflowDelegation" workflowURI="${scheme}${ramses_socketsplugin}ramses/sockets/workflows/refine_sockets.workflow">
        <propertyValues xmi:id="_tZwJS_aaEeqzYKJCypr2Vw" name="output_dir" defaultValue="${output_dir}/sockets">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_tZwJSvaaEeqzYKJCypr2Vw" name="refined_trace_file" defaultValue="${output_dir}/${source_file_name}.arch_trace">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_tZwJTPaaEeqzYKJCypr2Vw" name="refined_aadl_file" defaultValue="${output_dir}/${source_file_name}.aadl">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
      </components>
    </onTrue>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_fiTrMAJ1EeuQd8rPO2uDsw" name="workflowDelegation" workflowURI="{integration_workfows_path}refine_and_generate_integration.workflow"/>
  <properties xmi:id="_Lt5lYAKdEeuXP8YXOvMHxQ" name="source_dir_uri">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_a1l4kLDgEeqLE8HsirCK_w" name="source_file_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_RAT4AKDmEeqR3ds1UXwt-g" name="system_implementation_name" defaultValue="${system_implementation_name}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_YZkB4KEEEeqR3ds1UXwt-g" name="output_dir" defaultValue="${output_dir}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_5VZQMPNKEeqsDOGYuOmxLA" fileURI="default_integration.properties"/>
</workflow:Workflow>
