<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_he6BYPM4EeqsDOGYuOmxLA" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_F1vW4BCNEeuDWeS4dZ-6Dg" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/templates/refine_remotes.workflow">
    <propertyValues xmi:id="_aDUqgBCNEeuDWeS4dZ-6Dg" name="has_remote_connections_cond" defaultValue="hasMqttCom">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_c6DAABCNEeuDWeS4dZ-6Dg" name="remotes_refinement_workflow" defaultValue="${scheme}${ramses_mqtt_plugin}ramses/mqtt/workflows/refine_mqtt.workflow">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_e5g9YBCNEeuDWeS4dZ-6Dg" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_hSDiEBCNEeuDWeS4dZ-6Dg" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_jJHxYBCNEeuDWeS4dZ-6Dg" name="include_dir" defaultValue="${include_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_k_onEBCNEeuDWeS4dZ-6Dg" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_ms4MABCNEeuDWeS4dZ-6Dg" name="protocols" defaultValue="MQTT">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_KB7ggGZuEeuSitgq2seg7g" name="protocol" defaultValue="mqtt">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_DfjGIBCOEeuDWeS4dZ-6Dg" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/templates/refine_remotes.workflow">
    <propertyValues xmi:id="_DfjGIRCOEeuDWeS4dZ-6Dg" name="has_remote_connections_cond" defaultValue="hasSocketCom">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_DfjGIhCOEeuDWeS4dZ-6Dg" name="remotes_refinement_workflow" defaultValue="${scheme}${ramses_sockets_plugin}ramses/sockets/workflows/refine_sockets.workflow">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_DfjGIxCOEeuDWeS4dZ-6Dg" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_DfjGJBCOEeuDWeS4dZ-6Dg" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_DfjGJRCOEeuDWeS4dZ-6Dg" name="include_dir" defaultValue="${include_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_DfjGJhCOEeuDWeS4dZ-6Dg" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_DfjGJxCOEeuDWeS4dZ-6Dg" name="protocols" defaultValue="SOCKETS_TCP,SOCKETS_UDP">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_QYs1UGZuEeuSitgq2seg7g" name="protocol" defaultValue="sockets">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_YZkB4KEEEeqR3ds1UXwt-g" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_a1l4kLDgEeqLE8HsirCK_w" name="source_file_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_5VZQMPNKEeqsDOGYuOmxLA" fileURI="default_integration.properties"/>
</workflow:Workflow>
