<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_hSY4kPHpEeqsDOGYuOmxLA" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_4d4TABCOEeuDWeS4dZ-6Dg" name="workflowDelegation" workflowURI="${validation_workflow_linux}">
    <propertyValues xmi:id="_j2cPcBIDEeuLp_MRejpHRQ" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_mYFFwBIDEeuLp_MRejpHRQ" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_Q9ZV4BCPEeuDWeS4dZ-6Dg" name="workflowDelegation" workflowURI="${validation_workflow_pok}">
    <propertyValues xmi:id="_pyhMUBIDEeuLp_MRejpHRQ" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_pyhMURIDEeuLp_MRejpHRQ" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_VKEKoBCPEeuDWeS4dZ-6Dg" name="workflowDelegation" workflowURI="${validation_workflow_nxtosek}">
    <propertyValues xmi:id="_p_U5QBIDEeuLp_MRejpHRQ" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_p_U5QRIDEeuLp_MRejpHRQ" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_LVZuoEjiEeujCYcfh2N14w" name="workflowDelegation" workflowURI="${validation_workflow_freertos}">
    <propertyValues xmi:id="_p_U5QBIDEeuLs_MRejpHRQ" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_p_U5QRIDEeuLq_MRejpHRQ" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_bYMYgFQ1EeugtZQkCVSjcg" name="workflowDelegation" workflowURI="${validation_workflow_ev3dev}">
    <propertyValues xmi:id="_bYMYgVQ1EeugtZQkCVSjcg" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_bYMYglQ1EeugtZQkCVSjcg" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_gcJ7oEXREeuCFLU3z5s2hw" name="error Linux" conditionSlot="violateslinuxConstraints">
    <onTrue xmi:id="_ofXkIEXREeuCFLU3z5s2hw" name="Model has errors, stop workflow "/>
    <onFalse xmi:id="_pGcyQEXREeuCFLU3z5s2hw" name="Validation for Linux OK, continue">
      <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_xEXEAEXREeuCFLU3z5s2hw" name="error POK" conditionSlot="violatespokConstraints">
        <onTrue xmi:id="_0GEhgEXREeuCFLU3z5s2hw" name="Model has errors, stop workflow "/>
        <onFalse xmi:id="_1zRqMEXREeuCFLU3z5s2hw" name="Validation for POK OK, continue">
          <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_8js1IEXREeuCFLU3z5s2hw" name="conditionalExecution" conditionSlot="violatesnxtosekConstraints">
            <onTrue xmi:id="_EzcdwEXSEeuCFLU3z5s2hw" name="Model has errors, stop workflow "/>
            <onFalse xmi:id="_GRESUEXSEeuCFLU3z5s2hw" name="Validation for NxtOSEK OK, continue">
              <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_X98J4E8sEeun3PjrBVIK7Q" name="conditionalExecution" conditionSlot="violatesfreertosConstraints">
                <onTrue xmi:id="_cs7BgE8sEeun3PjrBVIK7Q" name="Model has erros, stop workflow"/>
                <onFalse xmi:id="_jPSiUE8sEeun3PjrBVIK7Q" name="Validation for FreeRTOS OK, continue">
                  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_hd2IIFQ1EeugtZQkCVSjcg" name="conditionalExecution" conditionSlot="violatesev3devConstraints">
                    <onTrue xmi:id="_klhDsFQ1EeugtZQkCVSjcg" name="Model has erros, stop workflow"/>
                    <onFalse xmi:id="_l_uX4FQ1EeugtZQkCVSjcg" name="Validation for EV3DEV OK, continue">
                      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_Emx-gBCPEeuDWeS4dZ-6Dg" name="workflowDelegation" workflowURI="refine_all_protocols.workflow">
                        <propertyValues xmi:id="_tdTUoBIGEeuujpa9mRWNvA" name="source_file_name" defaultValue="${source_file_name}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_tdTUoRIGEeuujpa9mRWNvA" name="runtime_scheme" defaultValue="${runtime_scheme}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_tdTUohIGEeuujpa9mRWNvA" name="output_dir" defaultValue="${output_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_JJRAMBIZEeuLp_MRejpHRQ" name="include_dir" defaultValue="${include_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                      </components>
                      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_tthjkPHpEeqsDOGYuOmxLA" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/templates/check_refine_and_generate.workflow">
                        <propertyValues xmi:id="_v_QXUAkvEeuixIGZFNyO-g" name="target" defaultValue="linux">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_DLJYQKfnEeqjWv6l-exT6w" name="source_file_name" defaultValue="${source_file_name}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_BFrRMOe8Eeqy-t86Uy9Gdw" name="runtime_scheme" defaultValue="${runtime_scheme}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_0wxaQPKhEeqsDOGYuOmxLA" name="output_dir" defaultValue="${output_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_g3asYPKwEeqsDOGYuOmxLA" name="include_dir" defaultValue="${include_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_BEqZEAkwEeuixIGZFNyO-g" name="target_install_dir" defaultValue="${target_install_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_vD4nUAlHEeuixIGZFNyO-g" name="code_generation_workflow" defaultValue="${code_generation_workflow_linux}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_RIrF4AlJEeuixIGZFNyO-g" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file_linux}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                      </components>
                      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_vvRY4PHpEeqsDOGYuOmxLA" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/templates/check_refine_and_generate.workflow">
                        <propertyValues xmi:id="_C6stoAkwEeuixIGZFNyO-g" name="target" defaultValue="pok">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_gHsl0PKTEeqsDOGYuOmxLA" name="runtime_scheme" defaultValue="${runtime_scheme}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_5073sPKhEeqsDOGYuOmxLA" name="output_dir" defaultValue="${output_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_qxEWQPKpEeqsDOGYuOmxLA" name="target_install_dir" defaultValue="${target_install_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_jtJjYPKwEeqsDOGYuOmxLA" name="include_dir" defaultValue="${include_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_iZ3F8P15Eeq3Gs5l9Sfxsw" name="source_file_name" defaultValue="${source_file_name}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_ZgI8kglIEeuixIGZFNyO-g" name="code_generation_workflow" defaultValue="${code_generation_workflow_pok}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_TuhGwAlJEeuixIGZFNyO-g" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file_pok}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                      </components>
                      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_v-BmkPHpEeqsDOGYuOmxLA" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/templates/check_refine_and_generate.workflow">
                        <propertyValues xmi:id="_K9sTsAkwEeuixIGZFNyO-g" name="target" defaultValue="nxtosek">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_DLJYQKfnEeqjWv6l-exT6w" name="source_file_name" defaultValue="${source_file_name}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_YDXRwOe4Eeqy-t86Uy9Gdw" name="runtime_scheme" defaultValue="${runtime_scheme}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_2GCAwNDmEeqtbeq_dJTm6w" name="target_install_dir" defaultValue="${target_install_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_6DvIsPKhEeqsDOGYuOmxLA" name="output_dir" defaultValue="${output_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_kVv0cPKwEeqsDOGYuOmxLA" name="include_dir" defaultValue="${include_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_dCELMglIEeuixIGZFNyO-g" name="code_generation_workflow" defaultValue="${code_generation_workflow_nxtosek}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_VIejUAlJEeuixIGZFNyO-g" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file_nxtosek}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                      </components>
                      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_NqKXgE8tEeun3PjrBVIK7Q" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/templates/check_refine_and_generate.workflow">
                        <propertyValues xmi:id="_Par58E8tEeun3PjrBVIK7Q" name="target" defaultValue="freertos">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_oONd8E8tEeuvL8nlFGqg6A" name="source_file_name" defaultValue="${source_file_name}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_tSwV4E8tEeuvL8nlFGqg6A" name="runtime_scheme" defaultValue="${runtime_scheme}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_E9nBoE8uEeuvL8nlFGqg6A" name="output_dir" defaultValue="${output_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_KhSFkE8uEeuvL8nlFGqg6A" name="include_dir" defaultValue="${include_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_P5rtoE8uEeuvL8nlFGqg6A" name="target_install_dir" defaultValue="${target_install_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_vn8PIE8uEeudu4VOe4VPoA" name="code_generation_workflow" defaultValue="${code_generation_workflow_freertos}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_BBvMgE8vEeuRva-782zuEg" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file_freertos}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                      </components>
                      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_2K0GcFQ1EeugtZQkCVSjcg" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/templates/check_refine_and_generate.workflow">
                        <propertyValues xmi:id="_2K0GcVQ1EeugtZQkCVSjcg" name="target" defaultValue="ev3dev">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_2K0GclQ1EeugtZQkCVSjcg" name="source_file_name" defaultValue="${source_file_name}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_2K0Gc1Q1EeugtZQkCVSjcg" name="runtime_scheme" defaultValue="${runtime_scheme}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_2K0GdFQ1EeugtZQkCVSjcg" name="output_dir" defaultValue="${output_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_2K0GdVQ1EeugtZQkCVSjcg" name="include_dir" defaultValue="${include_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_2K0GdlQ1EeugtZQkCVSjcg" name="target_install_dir" defaultValue="${target_install_dir}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_2K0Gd1Q1EeugtZQkCVSjcg" name="code_generation_workflow" defaultValue="${code_generation_workflow_ev3dev}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                        <propertyValues xmi:id="_2K0GeFQ1EeugtZQkCVSjcg" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file_ev3dev}">
                          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                        </propertyValues>
                      </components>
                    </onFalse>
                  </components>
                </onFalse>
              </components>
            </onFalse>
          </components>
        </onFalse>
      </components>
    </onFalse>
  </components>
  <properties xmi:id="_UZ5ZoKFMEeqN5O1Mj1GQIw" name="refined_models_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Wro5oF4rEeqYp8ezKVdS_w" name="include_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_RvqH0F4rEeqYp8ezKVdS_w" name="output_dir" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_mL6J0PJ5EeqsDOGYuOmxLA" name="ramses_posix_plugin">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_oDmEMPJ5EeqsDOGYuOmxLA" name="ramses_pok_plugin">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_oTKKMPJ5EeqsDOGYuOmxLA" name="ramses_osek_plugin">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_celxIPKTEeqsDOGYuOmxLA" name="runtime_scheme">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_wmXVsPKpEeqsDOGYuOmxLA" name="target_install_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_8CzoMNpdEeq9fI4NRnUBRA" fileURI="default_integration.properties"/>
</workflow:Workflow>
