<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_bseAcCKSEeu4RuUICTpSnA" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_gJsZsCKSEeu4RuUICTpSnA" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/templates/load_validate_refine_and_generate.workflow">
    <propertyValues xmi:id="_oyAigATGEeuI-KRpR8uPjw" name="target_install_dir" defaultValue="${target_install_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_12YCoATEEeuI-KRpR8uPjw" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_8EbgUATEEeuI-KRpR8uPjw" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_Di_joATFEeuI-KRpR8uPjw" name="refined_trace_file" defaultValue="${refined_trace_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_gXh_oAS9EeuI-KRpR8uPjw" name="target" defaultValue="${target}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_5jfZIAS6EeuI-KRpR8uPjw" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_AnvfAAS7EeuI-KRpR8uPjw" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_bZN_oF7GEeqTbYoc6DiMAg" name="validation_report_file" defaultValue="${validation_report_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_KsiTMOe7Eeqy-t86Uy9Gdw" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_IULv4P8hEeqjUK5cPU4ZFw" name="load_aadl_resources_workflow" defaultValue="${load_aadl_resources_workflow}"/>
    <propertyValues xmi:id="_RgoRUP8hEeqjUK5cPU4ZFw" name="load_transformation_resources_workflow" defaultValue="${load_transformation_resources_workflow}"/>
    <propertyValues xmi:id="_zOUJYP8gEeqjUK5cPU4ZFw" name="validation_workflow" defaultValue="${validation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_407MkP8gEeqjUK5cPU4ZFw" name="refinement_workflow" defaultValue="${refinement_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_A4CHYP8hEeqjUK5cPU4ZFw" name="code_generation_workflow" defaultValue="${code_generation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_N76b8AZJEeuDOaqjzi9xMA" name="expose_runtime_shared_resources" defaultValue="${expose_runtime_shared_resources}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_Ik0gwAcgEeuBsNtPqQbVlg" name="instance_model_file" defaultValue="${instance_model_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_hYa2oCKSEeu4RuUICTpSnA" name="target_install_dir" defaultValue="${target_install_dir}"/>
  <properties xmi:id="_h0UvgCKSEeu4RuUICTpSnA" name="output_dir" defaultValue="${output_dir}"/>
  <properties xmi:id="_iOkbgCKSEeu4RuUICTpSnA" name="include_dir" description="${description_include_dir}"/>
  <properties xmi:id="_ishi0CKSEeu4RuUICTpSnA" name="source_aadl_file" defaultValue="${source_aadl_file}"/>
  <properties xmi:id="_j_HMECKSEeu4RuUICTpSnA" name="system_implementation_name" defaultValue="${system_implementation_name}"/>
  <properties xmi:id="_kd2WgCKSEeu4RuUICTpSnA" name="instance_model_file" defaultValue="${instance_model_file}"/>
  <properties xmi:id="_k_lxoCKSEeu4RuUICTpSnA" name="refined_aadl_file" defaultValue="${refined_aadl_file}"/>
  <properties xmi:id="_lhryECKSEeu4RuUICTpSnA" name="refined_trace_file" defaultValue="${refined_trace_file}"/>
  <properties xmi:id="_mZ-E8CKSEeu4RuUICTpSnA" name="validation_report_file" defaultValue="${validation_report_file}"/>
  <propertiesFiles xmi:id="_ga63ICKTEeu4RuUICTpSnA" fileURI="default_freertos.properties"/>
  <propertiesFiles xmi:id="_mQ2vwCKTEeu4RuUICTpSnA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
