<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowramsesfreertos="https://mem4csd.telecom-paris.fr/ramses/workflowramsesfreertos" xmi:id="_XnpPMCKJEeu4RuUICTpSnA" name="workflow">
  <components xsi:type="workflow.components:ModelReader" xmi:id="_YoKRACKJEeu4RuUICTpSnA" name="${NameRefinedResourceReader}" modelSlot="refinedAadlModel${target}" modelURI="${refined_aadl_file}" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_h9AggCKJEeu4RuUICTpSnA" name="${NameRefinementTraceReader}" modelSlot="refinedTraceModel${target}" modelURI="${refined_trace_file}" modelElementIndex="0"/>
  <components xsi:type="workflowramsesfreertos:CodegenFreertos" xmi:id="_7fUG8CNWEeuVo6SlWEf8qQ" name="${NameCodeGeneration}" aadlModelSlot="refinedAadlModel${target}" traceModelSlot="refinedTraceModel${target}" outputDirectory="${output_dir}" targetInstallDir="${target_install_dir}" includeDir="${include_dir}" coreRuntimeDir="${core_runtime_dir}" targetRuntimeDir="${target_runtime_dir}"/>
  <properties xmi:id="_yUcB8CKJEeu4RuUICTpSnA" name="include_dir"/>
  <properties xmi:id="_1pzbQCKJEeu4RuUICTpSnA" name="output_dir"/>
  <properties xmi:id="_2PNOYCKJEeu4RuUICTpSnA" name="refined_aadl_file"/>
  <properties xmi:id="_2wnSUCKJEeu4RuUICTpSnA" name="refined_trace_file"/>
  <properties xmi:id="_3V8M8CKJEeu4RuUICTpSnA" name="target_install_dir"/>
  <propertiesFiles xmi:id="_amMOgCKKEeu4RuUICTpSnA" fileURI="default_freertos.properties"/>
  <propertiesFiles xmi:id="_dUjSMCKKEeu4RuUICTpSnA" fileURI="platform:/plugin/fr.mem4csd.ramses.freertos/ramses/freertos/workflows/default_freertos.properties" resolveURI="false"/>
  <propertiesFiles xmi:id="_iCg2ICKKEeu4RuUICTpSnA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
