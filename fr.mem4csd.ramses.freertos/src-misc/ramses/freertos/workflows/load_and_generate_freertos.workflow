<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowramsesfreertos="https://mem4csd.telecom-paris.fr/ramses/workflowramsesfreertos" xmi:id="_OOOhYCKMEeu4RuUICTpSnA" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_TJC1kCKMEeu4RuUICTpSnA" name="workflowDelegation" workflowURI="${load_aadl_resources_workflow}"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_aP7YsCKMEeu4RuUICTpSnA" name="${NameInputResourcesReader}" modelSlot="sourceAadlModel" modelURI="${source_aadl_file}" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_hwSy0CKMEeu4RuUICTpSnA" name="${NameInputResourcesReader}" modelSlot="sourceAadlInstance" modelURI="${instance_model_file}" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_sOt5gCKMEeu4RuUICTpSnA" name="${NameRefinedResourceReader}" modelSlot="refinedAadlModelFreeRTOS" modelURI="${refined_aadl_file}" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_ypeWcCKMEeu4RuUICTpSnA" name="${NameRefinementTraceReader}" modelSlot="refinedTraceModelFreeRTOS" modelURI="${refined_trace_file}" modelElementIndex="0"/>
  <components xsi:type="workflowramsesfreertos:CodegenFreertos" xmi:id="_7fUG8CNWEeuVo6SlWEf8qQ" name="${NameCodeGeneration}" aadlModelSlot="refinedAadlModelFreeRTOS" traceModelSlot="refinedTraceModelFreeRTOS" outputDirectory="${output_dir}" targetInstallDir="${target_install_dir}" includeDir="${include_dir}" coreRuntimeDir="${core_runtime_dir}" targetRuntimeDir="${target_runtime_dir}"/>
  <properties xmi:id="_omgnQCKNEeu4RuUICTpSnA" name="source_aadl_file"/>
  <properties xmi:id="_pJ5oMCKNEeu4RuUICTpSnA" name="refined_aadl_file"/>
  <properties xmi:id="_pzySsCKNEeu4RuUICTpSnA" name="refined_trace_file"/>
  <properties xmi:id="_qZu4YCKNEeu4RuUICTpSnA" name="instance_model_file"/>
  <properties xmi:id="_rAhLkCKNEeu4RuUICTpSnA" name="include_dir"/>
  <properties xmi:id="_rls8QCKNEeu4RuUICTpSnA" name="output_dir"/>
  <properties xmi:id="_sYJF0CKNEeu4RuUICTpSnA" name="target_install_dir"/>
  <propertiesFiles xmi:id="_dX49QCKOEeu4RuUICTpSnA" fileURI="default_freertos.properties"/>
  <propertiesFiles xmi:id="_gt0-UCKOEeu4RuUICTpSnA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
