<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_1AkpUCKPEeu4RuUICTpSnA" name="workflow">
  <components xsi:type="workflow.components:ModelReader" xmi:id="_HtyWYCKQEeu4RuUICTpSnA" name="${NameLoadRefinementTransformationModules}" modelSlot="Refinement${target}" modelURI="${ramses_freertos_transformation_refinement_path}FreeRTOSTarget.emftvm" modelElementIndex="0"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_xu56UC5uEeuiYPByRdHPMg" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/load_transformation_resources_core.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_zQvKoFAGEeu_g8Ypa0KQeg" name="${NameLoadValidationTransformationModules}" modelSlot="FreeRTOSValidation" modelURI="${ramses_freertos_transformation_refinement_path}FreeRTOSValidation.emftvm" modelElementIndex="0"/>
  <properties xmi:id="_KDel8CKREeu4RuUICTpSnA" name="ramses_freertos_transformation_path" defaultValue="${scheme}${ramses_freertos_plugin}ramses/freertos/transformations/atl/"/>
  <properties xmi:id="_tUA7YCKREeu4RuUICTpSnA" name="ramses_freertos_transformation_refinement_path" defaultValue="${ramses_freertos_transformation_path}refinement/"/>
  <propertiesFiles xmi:id="_4e-_ACKREeu4RuUICTpSnA" fileURI="default_freertos.properties"/>
  <propertiesFiles xmi:id="_64InsCKREeu4RuUICTpSnA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
