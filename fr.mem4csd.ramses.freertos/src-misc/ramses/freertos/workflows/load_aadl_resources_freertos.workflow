<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_uov94CKKEeu4RuUICTpSnA" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_yxYnMCKKEeu4RuUICTpSnA" name="load_refinement_freertos" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/load_aadl_resources_core.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_DIjyICTJEeuxZtciNsDo3w" name="load_refinement_freertos" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/load_ramses_aadl_resources_core.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_6SY6gCKKEeu4RuUICTpSnA" name="${NameRamsesAadlResourcesReader}" modelSlot="freertos_properties" modelURI="${scheme}${ramses_freertos_plugin}ramses/freertos/propertysets/freertos_properties.aadl" resolveURI="false" modelElementIndex="0"/>
  <propertiesFiles xmi:id="_DMqG0CKLEeu4RuUICTpSnA" fileURI="default_freertos.properties"/>
  <propertiesFiles xmi:id="_SCy0oCKLEeu4RuUICTpSnA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties"/>
</workflow:Workflow>
