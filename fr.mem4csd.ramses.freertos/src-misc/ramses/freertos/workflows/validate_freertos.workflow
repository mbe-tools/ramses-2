<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowemftvm="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowemftvm" xmlns:workflowramses="https://mem4csd.telecom-paris.fr/ramses/workflowramses" xmi:id="_utIloNj9EeiIc5Eb0h0CDw" name="workflow" description="Operations of validation for FreeRTOS">
  <components xsi:type="workflowramses:ClearValidationErrors" xmi:id="_SlmqcNDAEeqjHe-01HNqJA" name="clearValidationErrors" validationReportModelURI="${validation_report_file}"/>
  <components xsi:type="workflowemftvm:EmftVmTransformer" xmi:id="_6a4ZoNkSEeiIc5Eb0h0CDw" name="${NameValidateInputModel} for ${target}" registerDependencyModels="true" discardExtraRootElements="true">
    <rulesModelSlot>IOHelpers</rulesModelSlot>
    <rulesModelSlot>AADLCopyHelpers</rulesModelSlot>
    <rulesModelSlot>AADLICopyHelpers</rulesModelSlot>
    <rulesModelSlot>Services</rulesModelSlot>
    <rulesModelSlot>PropertiesTools</rulesModelSlot>
    <rulesModelSlot>Common</rulesModelSlot>
    <rulesModelSlot>DataInstances</rulesModelSlot>
    <rulesModelSlot>ProcessInstances</rulesModelSlot>
    <rulesModelSlot>ConnectionInstances</rulesModelSlot>
    <rulesModelSlot>ProcessorInstances</rulesModelSlot>
    <rulesModelSlot>VirtualProcessorInstances</rulesModelSlot>
    <rulesModelSlot>VirtualBusInstances</rulesModelSlot>
    <rulesModelSlot>FeatureInstances</rulesModelSlot>
    <rulesModelSlot>Subprograms</rulesModelSlot>
    <rulesModelSlot>Refinement${target}</rulesModelSlot>
    <rulesModelSlot>FreeRTOSValidation</rulesModelSlot>
    <inputModels xmi:id="_6a4ZodkSEeiIc5Eb0h0CDw" modelName="IN" modelSlot="sourceAadlInstance" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_SMJGEOT2Eem6pbBi9x9dBw" modelName="TOOLS" modelSlot="atlHelper" metamodelName="ATLHELPER" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/helpers"/>
    <inputModels xmi:id="_faGDIOW8EemCAs4cSq20jw" modelName="BASE_TYPES" modelSlot="Base_Types" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba"/>
    <inputModels xmi:id="_faGDJuW8EemCAs4cSq20jw" modelName="AADL_RUNTIME" modelSlot="aadl_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDKuW8EemCAs4cSq20jw" modelName="RAMSES_PROCESSORS" modelSlot="RAMSES_processors" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDK-W8EemCAs4cSq20jw" modelName="RAMSES_BUSES" modelSlot="RAMSES_buses" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDLOW8EemCAs4cSq20jw" modelName="AADL_PROJECT" modelSlot="AADL_Project" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDLeW8EemCAs4cSq20jw" modelName="COMMUNICATION_PROPERTIES" modelSlot="Communication_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDLuW8EemCAs4cSq20jw" modelName="DATA_MODEL" modelSlot="Data_Model" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDL-W8EemCAs4cSq20jw" modelName="DEPLOYMENT_PROPERTIES" modelSlot="Deployment_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDMOW8EemCAs4cSq20jw" modelName="MEMORY_PROPERTIES" modelSlot="Memory_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDMeW8EemCAs4cSq20jw" modelName="MODELING_PROPERTIES" modelSlot="Modeling_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDMuW8EemCAs4cSq20jw" modelName="PROGRAMMING_PROPERTIES" modelSlot="Programming_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDM-W8EemCAs4cSq20jw" modelName="THREAD_PROPERTIES" modelSlot="Thread_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDNOW8EemCAs4cSq20jw" modelName="TIMING_PROPERTIES" modelSlot="Timing_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDNeW8EemCAs4cSq20jw" modelName="ARINC653" modelSlot="ARINC653" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDNuW8EemCAs4cSq20jw" modelName="CODE_GENERATION_PROPERTIES" modelSlot="Code_Generation_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_faGDOeW8EemCAs4cSq20jw" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <outputModels xmi:id="_VJyf0OafEemFCv6p94An5g" modelName="OUT" modelSlot="validation_report" metamodelName="CV" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/validation_report"/>
  </components>
  <components xsi:type="workflow.components:ModelWriter" xmi:id="_JxKyEKpJEeaQaI5SJTCSUw" name="write_validation_report" modelSlot="validation_report" modelURI="${validation_report_file}" cloneModel="false" resolveURI="false"/>
  <components xsi:type="workflowramses:ReportValidationErrors" xmi:id="_XmULQNDAEeqjHe-01HNqJA" name="reportValidationErrors" validationReportModelSlot="validation_report" hasErrorModelSlot="violates${target}Constraints"/>
  <properties xmi:id="_stxcMF68EeqTbYoc6DiMAg" name="validation_report_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_yAH78AZHEeuDOaqjzi9xMA" name="target">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_SQzgMEmqEeqLJuiYUp38jw" fileURI="default_freertos.properties"/>
  <propertiesFiles xmi:id="_dRk6MNpeEeq9fI4NRnUBRA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
