/**
 */
package fr.mem4csd.ramses.freertos.workflowramsesfreertos;

import fr.mem4csd.ramses.linux.workflowramseslinux.CodegenLinux;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Codegen Freertos</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.mem4csd.ramses.freertos.workflowramsesfreertos.WorkflowramsesfreertosPackage#getCodegenFreertos()
 * @model
 * @generated
 */
public interface CodegenFreertos extends CodegenLinux {
} // CodegenFreertos
