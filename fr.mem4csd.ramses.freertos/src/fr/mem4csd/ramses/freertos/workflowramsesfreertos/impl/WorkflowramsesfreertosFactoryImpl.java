/**
 */
package fr.mem4csd.ramses.freertos.workflowramsesfreertos.impl;

import fr.mem4csd.ramses.freertos.workflowramsesfreertos.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowramsesfreertosFactoryImpl extends EFactoryImpl implements WorkflowramsesfreertosFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WorkflowramsesfreertosFactory init() {
		try {
			WorkflowramsesfreertosFactory theWorkflowramsesfreertosFactory = (WorkflowramsesfreertosFactory)EPackage.Registry.INSTANCE.getEFactory(WorkflowramsesfreertosPackage.eNS_URI);
			if (theWorkflowramsesfreertosFactory != null) {
				return theWorkflowramsesfreertosFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WorkflowramsesfreertosFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramsesfreertosFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WorkflowramsesfreertosPackage.CODEGEN_FREERTOS: return createCodegenFreertos();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodegenFreertos createCodegenFreertos() {
		CodegenFreertosImpl codegenFreertos = new CodegenFreertosImpl();
		return codegenFreertos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramsesfreertosPackage getWorkflowramsesfreertosPackage() {
		return (WorkflowramsesfreertosPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WorkflowramsesfreertosPackage getPackage() {
		return WorkflowramsesfreertosPackage.eINSTANCE;
	}

} //WorkflowramsesfreertosFactoryImpl
