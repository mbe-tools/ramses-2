/**
 */
package fr.mem4csd.ramses.freertos.workflowramsesfreertos.impl;

import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator;
import fr.mem4csd.ramses.freertos.codegen.c.AadlToFreeRTOSCUnparser;
import fr.mem4csd.ramses.freertos.codegen.makefile.AadlToFreeRTOSMakefileUnparser;
import fr.mem4csd.ramses.freertos.workflowramsesfreertos.CodegenFreertos;
import fr.mem4csd.ramses.freertos.workflowramsesfreertos.WorkflowramsesfreertosPackage;

import fr.mem4csd.ramses.linux.workflowramseslinux.impl.CodegenLinuxImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Codegen Freertos</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CodegenFreertosImpl extends CodegenLinuxImpl implements CodegenFreertos {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodegenFreertosImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesfreertosPackage.Literals.CODEGEN_FREERTOS;
	}

	/**
	 * 
	 * @generated not
	 */
	@Override
	public AadlToTargetConfigurationGenerator getAadlToTargetConfiguration() {
		if(aadlToTargetConfiguration==null)
			setAadlToTargetConfiguration(new AadlToFreeRTOSCUnparser());
		return aadlToTargetConfiguration;
	}
	
	/**
	 * 
	 * @generated not
	 */
	@Override
	public AadlToTargetBuildGenerator getAadlToTargetBuild() {
		
		if(aadlToTargetBuild==null) {
			AadlToFreeRTOSMakefileUnparser unparser = new AadlToFreeRTOSMakefileUnparser();
			setAadlToTargetBuild(unparser);
		}
		return aadlToTargetBuild;
	}
	
} //CodegenFreertosImpl
