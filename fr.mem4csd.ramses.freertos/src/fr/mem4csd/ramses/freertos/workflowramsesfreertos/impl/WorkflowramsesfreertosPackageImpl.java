/**
 */
package fr.mem4csd.ramses.freertos.workflowramsesfreertos.impl;

import de.mdelab.workflow.WorkflowPackage;

import de.mdelab.workflow.components.ComponentsPackage;

import de.mdelab.workflow.helpers.HelpersPackage;

import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;

import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import fr.mem4csd.ramses.freertos.workflowramsesfreertos.CodegenFreertos;
import fr.mem4csd.ramses.freertos.workflowramsesfreertos.WorkflowramsesfreertosFactory;
import fr.mem4csd.ramses.freertos.workflowramsesfreertos.WorkflowramsesfreertosPackage;

import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowramsesfreertosPackageImpl extends EPackageImpl implements WorkflowramsesfreertosPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codegenFreertosEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.ramses.freertos.workflowramsesfreertos.WorkflowramsesfreertosPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkflowramsesfreertosPackageImpl() {
		super(eNS_URI, WorkflowramsesfreertosFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WorkflowramsesfreertosPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkflowramsesfreertosPackage init() {
		if (isInited) return (WorkflowramsesfreertosPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowramsesfreertosPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorkflowramsesfreertosPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WorkflowramsesfreertosPackageImpl theWorkflowramsesfreertosPackage = registeredWorkflowramsesfreertosPackage instanceof WorkflowramsesfreertosPackageImpl ? (WorkflowramsesfreertosPackageImpl)registeredWorkflowramsesfreertosPackage : new WorkflowramsesfreertosPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Aadl2Package.eINSTANCE.eClass();
		Arch_tracePackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();
		HelpersPackage.eINSTANCE.eClass();
		ComponentsPackage.eINSTANCE.eClass();
		WorkflowramsesPackage.eINSTANCE.eClass();
		WorkflowramseslinuxPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkflowramsesfreertosPackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowramsesfreertosPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorkflowramsesfreertosPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkflowramsesfreertosPackage.eNS_URI, theWorkflowramsesfreertosPackage);
		return theWorkflowramsesfreertosPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodegenFreertos() {
		return codegenFreertosEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramsesfreertosFactory getWorkflowramsesfreertosFactory() {
		return (WorkflowramsesfreertosFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		codegenFreertosEClass = createEClass(CODEGEN_FREERTOS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		WorkflowramseslinuxPackage theWorkflowramseslinuxPackage = (WorkflowramseslinuxPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowramseslinuxPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		codegenFreertosEClass.getESuperTypes().add(theWorkflowramseslinuxPackage.getCodegenLinux());

		// Initialize classes, features, and operations; add parameters
		initEClass(codegenFreertosEClass, CodegenFreertos.class, "CodegenFreertos", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //WorkflowramsesfreertosPackageImpl
