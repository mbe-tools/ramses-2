/**
 */
package fr.mem4csd.ramses.freertos.workflowramsesfreertos.util;

import de.mdelab.workflow.NamedComponent;

import de.mdelab.workflow.components.WorkflowComponent;

import fr.mem4csd.ramses.core.workflowramses.Codegen;

import fr.mem4csd.ramses.freertos.workflowramsesfreertos.*;

import fr.mem4csd.ramses.linux.workflowramseslinux.CodegenLinux;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.freertos.workflowramsesfreertos.WorkflowramsesfreertosPackage
 * @generated
 */
public class WorkflowramsesfreertosAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static WorkflowramsesfreertosPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramsesfreertosAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = WorkflowramsesfreertosPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WorkflowramsesfreertosSwitch<Adapter> modelSwitch =
		new WorkflowramsesfreertosSwitch<Adapter>() {
			@Override
			public Adapter caseCodegenFreertos(CodegenFreertos object) {
				return createCodegenFreertosAdapter();
			}
			@Override
			public Adapter caseNamedComponent(NamedComponent object) {
				return createNamedComponentAdapter();
			}
			@Override
			public Adapter caseWorkflowComponent(WorkflowComponent object) {
				return createWorkflowComponentAdapter();
			}
			@Override
			public Adapter caseCodegen(Codegen object) {
				return createCodegenAdapter();
			}
			@Override
			public Adapter caseCodegenLinux(CodegenLinux object) {
				return createCodegenLinuxAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.freertos.workflowramsesfreertos.CodegenFreertos <em>Codegen Freertos</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.freertos.workflowramsesfreertos.CodegenFreertos
	 * @generated
	 */
	public Adapter createCodegenFreertosAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.workflow.NamedComponent <em>Named Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.workflow.NamedComponent
	 * @generated
	 */
	public Adapter createNamedComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.workflow.components.WorkflowComponent <em>Workflow Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.workflow.components.WorkflowComponent
	 * @generated
	 */
	public Adapter createWorkflowComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.Codegen <em>Codegen</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen
	 * @generated
	 */
	public Adapter createCodegenAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.linux.workflowramseslinux.CodegenLinux <em>Codegen Linux</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.linux.workflowramseslinux.CodegenLinux
	 * @generated
	 */
	public Adapter createCodegenLinuxAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //WorkflowramsesfreertosAdapterFactory
