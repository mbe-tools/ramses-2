/**
 */
package fr.mem4csd.ramses.freertos.workflowramsesfreertos;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.freertos.workflowramsesfreertos.WorkflowramsesfreertosPackage
 * @generated
 */
public interface WorkflowramsesfreertosFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowramsesfreertosFactory eINSTANCE = fr.mem4csd.ramses.freertos.workflowramsesfreertos.impl.WorkflowramsesfreertosFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Codegen Freertos</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Codegen Freertos</em>'.
	 * @generated
	 */
	CodegenFreertos createCodegenFreertos();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorkflowramsesfreertosPackage getWorkflowramsesfreertosPackage();

} //WorkflowramsesfreertosFactory
