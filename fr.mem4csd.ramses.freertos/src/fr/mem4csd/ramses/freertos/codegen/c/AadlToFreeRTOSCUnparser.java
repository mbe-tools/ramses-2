package fr.mem4csd.ramses.freertos.codegen.c;

import java.io.IOException ;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger ;
import org.eclipse.core.runtime.IProgressMonitor ;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.DataSubcomponent;
import org.osate.aadl2.ProcessImplementation ;
import org.osate.aadl2.ProcessSubcomponent ;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.SystemImplementation;
import org.osate.aadl2.ThreadSubcomponent;
import org.osate.aadl2.modelsupport.UnparseText ;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.codegen.AadlToXUnparser;
import fr.mem4csd.ramses.core.codegen.GenerationException;
import fr.mem4csd.ramses.core.codegen.ProcessProperties;
import fr.mem4csd.ramses.core.codegen.RoutingProperties;
import fr.mem4csd.ramses.core.codegen.c.AadlToCUnparser;
import fr.mem4csd.ramses.core.codegen.utils.FileUtils;
import fr.mem4csd.ramses.core.codegen.utils.GenerationUtilsC;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;
import fr.mem4csd.ramses.core.workflowramses.TargetProperties;
import fr.mem4csd.ramses.posix.codegen.c.AadlToPOSIXCUnparser;

public class AadlToFreeRTOSCUnparser extends AadlToXUnparser {
	
	private static Logger _LOGGER = Logger.getLogger(AadlToFreeRTOSCUnparser.class);
	protected UnparseText _configHCode;
	protected UnparseText _IPConfigHCode;
	
	@Override
	public void generateProcessorTargetConfiguration(Subcomponent processor, 
			URI processorDir,
			IProgressMonitor monitor)
					throws GenerationException
	{
		// nothing to do
		return;
	}
	
	@Override
	  public void generateProcessTargetConfiguration(ProcessSubcomponent process,
			  URI outputDir,
			  IProgressMonitor monitor)
					  throws GenerationException
	  {
		ProcessImplementation procImpl = (ProcessImplementation) process.getSubcomponentType();
		
	    ArchTraceSpec traces = getCurrentModelTransformationTrace();
		// Generate main.c, main.h and FreeRTOSConfig.h
		_mainCCode = new UnparseText();
		_mainHCode = new UnparseText();
		_configHCode = new UnparseText();
		if(GeneratorUtils.processUsesSOCKET(procImpl))
		{
			_IPConfigHCode = new UnparseText();
		}
		
		genMainImpl(process, traces) ;
	    
	    try
	    {
	      
	      FileUtils.saveFile(outputDir, "main.h",
	               _mainHCode.getParseOutput()) ;
	      
	      FileUtils.saveFile(outputDir, "main.c",
	               _mainCCode.getParseOutput()) ;
	      
	      FileUtils.saveFile(outputDir, "FreeRTOSConfig.h",
	               _configHCode.getParseOutput()) ;
	      
	      if(GeneratorUtils.processUsesSOCKET(procImpl))
	      {
	    	  FileUtils.saveFile(outputDir, "FreeRTOSIPConfig.h",
		               _IPConfigHCode.getParseOutput()) ;
	      }
	      
	    }
	    catch(IOException e)
	    {
	      String errMsg = "cannot save the generated files" ;
	      _LOGGER.fatal(errMsg, e) ;
	      throw new RuntimeException(errMsg, e) ;
	    }
	  }
	
	protected void genMainGlobalVariables(ProcessSubcomponent process)
	  {
		
		AadlToCUnparser.getAadlToCUnparser().genMainGlobalVariables(process, _mainCCode);
		_mainCCode.addOutputNewline("SemaphoreHandle_t init_barrier_mutex;");
		_mainCCode.addOutputNewline("SemaphoreHandle_t init_barrier_cond;");
		
		ProcessImplementation pi = (ProcessImplementation) process.getComponentImplementation();
		ProcessImplementation procImpl = (ProcessImplementation) process.getSubcomponentType();
		
	    EList<ThreadSubcomponent> subcomponents = pi.getOwnedThreadSubcomponents();
	    for(ThreadSubcomponent ts: subcomponents)
	    {
	      AadlToCUnparser.getAadlToCUnparser().genTaskGlobalVariables(ts, _mainCCode);
	    }
	    
	  }
	
	private void genMainImpl(ProcessSubcomponent process, ArchTraceSpec traces)
	  {
		  
		String guard = GenerationUtilsC.generateHeaderInclusionGuard("main.h");
		_mainHCode.addOutputNewline(guard) ;
		
		_mainHCode.addOutputNewline("#include \"gtypes.h\"");
		_mainHCode.addOutputNewline("#include \"globals.h\"");
		_mainHCode.addOutputNewline("#include \"aadl_dispatch.h\"");
		_mainHCode.addOutputNewline("#include \"bitset.h\"");
		_mainHCode.addOutputNewline("#include <limits.h>");
		_mainHCode.addOutputNewline("#include <stdio.h>");
		_mainHCode.addOutputNewline("#include \"FreeRTOS.h\"");
		_mainHCode.addOutputNewline("#include \"task.h\"");
		_mainHCode.addOutputNewline("#include \"semphr.h\"");
		
		ProcessImplementation procImpl = (ProcessImplementation) process.getSubcomponentType();
		
		if(GeneratorUtils.processUsesMQTT(procImpl))
			_mainHCode.addOutputNewline("#define USE_MQTT");
		if(GeneratorUtils.processUsesSOCKET(procImpl))
			_mainHCode.addOutputNewline("#define USE_SOCKET");
			
		
		_mainCCode.addOutputNewline("#include \"main.h\"");
		_mainCCode.addOutputNewline("#include \"activity.h\"") ;
		_mainHCode.addOutputNewline("#include \"subprograms.h\"");
		_mainCCode.addOutputNewline("#include \"globals.h\"") ;
		_mainCCode.addOutputNewline("#include \"aadl_ports_network.h\"") ;
		
		_mainCCode.addOutputNewline("#ifdef MQTTCLIENT_PLATFORM_HEADER");
		_mainCCode.addOutputNewline("#include \"aadl_ports_mqtt.h\"") ;
		
		if(GeneratorUtils.processUsesSOCKET(procImpl))
			_mainHCode.addOutputNewline("void stop_process();");
		
		_mainCCode.addOutputNewline("#endif");
		
	    genMainGlobalVariables(process);
	    
	    AadlToCUnparser.getAadlToCUnparser().genModeInit(process, _mainCCode);
	    _mainCCode.addOutputNewline("vTaskDelay(portMAX_DELAY);");
	    _mainCCode.decrementIndent();
	    _mainCCode.addOutputNewline("}");
	    _mainCCode.addOutputNewline("");
	    
	    AadlToCUnparser.getAadlToCUnparser().genStopProcessCode(process, _mainCCode);
	    
	    _mainCCode.addOutputNewline("const uint8_t ucMACAddress[ 6 ] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55 };");
	    
	    
	    _mainCCode.addOutputNewline("int main(int argc, char* argv[])");
	    _mainCCode.addOutputNewline("{");
	    _mainCCode.incrementIndent();
	    
	    _mainCCode.addOutputNewline("static const uint8_t ucIPAddress[ 4 ] = {127, 0, 0, 1};");
	    _mainCCode.addOutputNewline("static const uint8_t ucNetMask[ 4 ] = { 255, 255, 255, 255 };");
	    _mainCCode.addOutputNewline("static const uint8_t ucGatewayAddress[ 4 ] = { 127, 33, 31, 1 };");
	    _mainCCode.addOutputNewline("static const uint8_t ucDNSServerAddress[ 4 ] = { 208, 67, 222, 222 };");
	    

	    _mainCCode.addOutputNewline("FreeRTOS_IPInit( ucIPAddress, ucNetMask, ucGatewayAddress, ucDNSServerAddress, ucMACAddress );");

	    List<String> additionalHeaders = new ArrayList<String>();
	    genMainCCode(process, traces, additionalHeaders);
	    _mainHCode.addOutput(GenerationUtilsC.getAdditionalHeader(additionalHeaders));
	    
	    _mainCCode.decrementIndent();
	    _mainCCode.addOutputNewline("}");
	    
	    _mainCCode.addOutputNewline("");

	    StringBuilder sb = new StringBuilder(process.getQualifiedName());
	    ProcessProperties pp = new ProcessProperties(sb.substring(0, sb.lastIndexOf("::")+2)) ;
	     
	    ProcessImplementation processImpl = (ProcessImplementation) 
	            process.getComponentImplementation() ;
	    this.findCommunicationMechanism(processImpl, pp, traces);
	    
	    _mainHCode.addOutputNewline("#endif");
	    
	    genConfigHCode(process);
	    if(GeneratorUtils.processUsesSOCKET(procImpl))
	    {
	    	genIPConfigHCode(process);
	    }
	    
	  }
	
	protected void genMainCCode(ProcessSubcomponent process, ArchTraceSpec traces, List<String> additionalHeaders)
	  {
		_mainCCode.addOutputNewline("init_barrier_mutex = xSemaphoreCreateMutex();");
		_mainCCode.addOutputNewline("init_barrier_cond = xSemaphoreCreateCounting(1,0);");
		
	    ProcessImplementation pi = (ProcessImplementation) process.getComponentImplementation();  
	    ProcessImplementation procImpl = (ProcessImplementation) process.getSubcomponentType();
	    
	    for(DataSubcomponent ds: pi.getOwnedDataSubcomponents())
	    {
	      if(ds.getDataSubcomponentType().getName().equalsIgnoreCase("MQTTConfigType"))
	      {
	    	  _mainCCode.addOutputNewline("mqtt_init(&"+ds.getName()+
	    			  ",\"mqtt_ramses_model\","
	    			  + "\""+ process.getFullName().replaceAll(":", "_")+"\","+
	    			  "on_message_arrived_"+ds.getName()+");");
	      }
	    }
	    	
	    
	    EList<ThreadSubcomponent> subcomponents = pi.getOwnedThreadSubcomponents();
	    AadlToCUnparser.getAadlToCUnparser().genMainCCode(process, _mainCCode, traces, additionalHeaders);
	    
	    _mainCCode.addOutputNewline("TaskHandle_t xHandle = NULL;");
	    _mainCCode.addOutputNewline("xTaskCreate(mode_init, \"mode_init\", configMINIMAL_STACK_SIZE, NULL, configMAX_PRIORITIES - 3, &xHandle);"); // configMAX_PRIORITIES - 1 and configMAX_PRIORITIES - 2 are used for system tasks

	    Subcomponent target = (Subcomponent)
	    		AadlHelperImpl.getDeloymentProcessorSubcomponent(process);
	    String schedProtocol = GeneratorUtils.getSchedulingProtocol(target);
	    if(schedProtocol!=null && schedProtocol.equalsIgnoreCase("static"))
	    {
	    	long maxPrio = GeneratorUtils.getMaxPriority(target)-2;
	    	_mainCCode.addOutputNewline("void time_triggered_sched();");
	        _mainCCode.addOutputNewline("TaskHandle_t TT_sched_tid;");
	        _mainCCode.addOutputNewline("create_thread ("+maxPrio+", 0, (void*) time_triggered_sched, &TT_sched_tid, \"tt_sched\", NULL);");
	    }
	    
	    _mainCCode.addOutputNewline("vTaskStartScheduler();");
	        
	  }
	
	
	protected void genConfigHCode(ProcessSubcomponent process)
	{
		Long maxPrio = AadlHelperImpl.getMaxPriority(process);
		maxPrio = maxPrio + 1;
		
		_configHCode.addOutputNewline("#ifndef FREERTOS_CONFIG_H");
		_configHCode.addOutputNewline("#define FREERTOS_CONFIG_H");
		
		// Application specific definitions
		_configHCode.addOutputNewline("#define configUSE_PREEMPTION 1");
		_configHCode.addOutputNewline("#define configUSE_PORT_OPTIMISED_TASK_SELECTION 0");
		_configHCode.addOutputNewline("#define configUSE_IDLE_HOOK 0");
		_configHCode.addOutputNewline("#define configUSE_TICK_HOOK 0");
		_configHCode.addOutputNewline("#define configUSE_DAEMON_TASK_STARTUP_HOOK 0");
		_configHCode.addOutputNewline("#define configTICK_RATE_HZ ( 100 )");
		_configHCode.addOutputNewline("#define configTOTAL_HEAP_SIZE ( ( size_t ) ( 65 * 1024 ) )");
		_configHCode.addOutputNewline("#define configMINIMAL_STACK_SIZE ( ( unsigned short ) 70 )");
		_configHCode.addOutputNewline("#define configMAX_TASK_NAME_LEN ( 12 )");
		_configHCode.addOutputNewline("#define configUSE_16_BIT_TICKS 0");
		_configHCode.addOutputNewline("#define configUSE_TRACE_FACILITY	1");
		_configHCode.addOutputNewline("#define configIDLE_SHOULD_YIELD 1");
		_configHCode.addOutputNewline("#define configUSE_MUTEXES 1");
		_configHCode.addOutputNewline("#define configCHECK_FOR_STACK_OVERFLOW 0");
		_configHCode.addOutputNewline("#define configUSE_RECURSIVE_MUTEXES 1");
		_configHCode.addOutputNewline("#define configQUEUE_REGISTRY_SIZE 20");
		_configHCode.addOutputNewline("#define configUSE_APPLICATION_TASK_TAG 1");
		_configHCode.addOutputNewline("#define configUSE_COUNTING_SEMAPHORES 1");
		_configHCode.addOutputNewline("#define configUSE_ALTERNATIVE_API 0");
		_configHCode.addOutputNewline("#define configUSE_QUEUE_SETS	1");
		_configHCode.addOutputNewline("#define configUSE_TASK_NOTIFICATIONS 1");
		_configHCode.addOutputNewline("#define configSUPPORT_STATIC_ALLOCATION 0");
		
		// Software timer related configuration options
		_configHCode.addOutputNewline("#define configUSE_TIMERS	1");
		_configHCode.addOutputNewline("#define configTIMER_TASK_PRIORITY ( configMAX_PRIORITIES - 1 )");
		_configHCode.addOutputNewline("#define configTIMER_QUEUE_LENGTH	20");
		_configHCode.addOutputNewline("#define configTIMER_TASK_STACK_DEPTH	( configMINIMAL_STACK_SIZE * 2 )");
		_configHCode.addOutputNewline("#define configMAX_PRIORITIES	( " + maxPrio + " )");
		_configHCode.addOutputNewline("unsigned long ulGetRunTimeCounterValue( void );");
		_configHCode.addOutputNewline("void vConfigureTimerForRunTimeStats( void );");
		_configHCode.addOutputNewline("#define configGENERATE_RUN_TIME_STATS 1");
		
		// Co-routine related configuration options
		_configHCode.addOutputNewline("#define configUSE_CO_ROUTINES 0");
		_configHCode.addOutputNewline("#define configMAX_CO_ROUTINE_PRIORITIES ( 2 )");
		
		
		_configHCode.addOutputNewline("#define configUSE_STATS_FORMATTING_FUNCTIONS	0");
		_configHCode.addOutputNewline("#define configSTACK_DEPTH_TYPE uint32_t");
		
		// Included API function (1 to include, 0 to exclude)
		_configHCode.addOutputNewline("#define INCLUDE_vTaskPrioritySet	1");
		_configHCode.addOutputNewline("#define INCLUDE_uxTaskPriorityGet 1");
		_configHCode.addOutputNewline("#define INCLUDE_vTaskDelete 1");
		_configHCode.addOutputNewline("#define INCLUDE_vTaskCleanUpResources 0");
		_configHCode.addOutputNewline("#define INCLUDE_vTaskSuspend	1");
		_configHCode.addOutputNewline("#define INCLUDE_vTaskDelayUntil 1");
		_configHCode.addOutputNewline("#define INCLUDE_vTaskDelay 1");
		_configHCode.addOutputNewline("#define INCLUDE_uxTaskGetStackHighWaterMark 1");
		_configHCode.addOutputNewline("#define INCLUDE_uxTaskGetStackHighWaterMark2	1");
		_configHCode.addOutputNewline("#define INCLUDE_xTaskGetSchedulerState 1");
		_configHCode.addOutputNewline("#define INCLUDE_xTimerGetTimerDaemonTaskHandle 1");
		_configHCode.addOutputNewline("#define INCLUDE_xTaskGetIdleTaskHandle 1");
		_configHCode.addOutputNewline("#define INCLUDE_xTaskGetHandle 1");
		_configHCode.addOutputNewline("#define INCLUDE_eTaskGetState 1");
		_configHCode.addOutputNewline("#define INCLUDE_xSemaphoreGetMutexHolder	1");
		_configHCode.addOutputNewline("#define INCLUDE_xTimerPendFunctionCall 1");
		_configHCode.addOutputNewline("#define INCLUDE_xTaskAbortDelay 1");
		_configHCode.addOutputNewline("#define configINCLUDE_MESSAGE_BUFFER_AMP_DEMO	0");
		
		_configHCode.addOutputNewline("#if ( configINCLUDE_MESSAGE_BUFFER_AMP_DEMO == 1 )");
		_configHCode.addOutputNewline("extern void vGenerateCoreBInterrupt( void * xUpdatedMessageBuffer );");
		_configHCode.addOutputNewline("#define sbSEND_COMPLETED( pxStreamBuffer ) vGenerateCoreBInterrupt( pxStreamBuffer )");
		_configHCode.addOutputNewline("#endif /* configINCLUDE_MESSAGE_BUFFER_AMP_DEMO */");
		
		_configHCode.addOutputNewline("#define configUSE_MALLOC_FAILED_HOOK 0");
		
		// Networking definitions - Default configurations
		_configHCode.addOutputNewline("#define configMAC_ISR_SIMULATOR_PRIORITY	( configMAX_PRIORITIES - 1 )");
		_configHCode.addOutputNewline("#define ipconfigUSE_NETWORK_EVENT_HOOK 0");
		_configHCode.addOutputNewline("#define configNETWORK_INTERFACE_TO_USE 1L");
//		_configHCode.addOutputNewline("#define configECHO_SERVER_ADDR0	172");
//		_configHCode.addOutputNewline("#define configECHO_SERVER_ADDR1 19");
//		_configHCode.addOutputNewline("#define configECHO_SERVER_ADDR2 195");
//		_configHCode.addOutputNewline("#define configECHO_SERVER_ADDR3 36");
//		_configHCode.addOutputNewline("#define configMAC_ADDR0 0x00");
//		_configHCode.addOutputNewline("#define configMAC_ADDR1 0x11");
//		_configHCode.addOutputNewline("#define configMAC_ADDR2 0x22");
//		_configHCode.addOutputNewline("#define configMAC_ADDR3 0x33");
//		_configHCode.addOutputNewline("#define configMAC_ADDR4 0x44");
//		_configHCode.addOutputNewline("#define configMAC_ADDR5 0x41");
//		_configHCode.addOutputNewline("#define configIP_ADDR0 172");
//		_configHCode.addOutputNewline("#define configIP_ADDR1 19");
//		_configHCode.addOutputNewline("#define configIP_ADDR2 195");
//		_configHCode.addOutputNewline("#define configIP_ADDR3 37");
//		_configHCode.addOutputNewline("#define configGATEWAY_ADDR0	172");
//		_configHCode.addOutputNewline("#define configIP_ADDR1 19");
//		_configHCode.addOutputNewline("#define configIP_ADDR2 195");
//		_configHCode.addOutputNewline("#define configIP_ADDR3 37");
//		_configHCode.addOutputNewline("#define configGATEWAY_ADDR0	172");
//		_configHCode.addOutputNewline("#define configGATEWAY_ADDR1	19");
//		_configHCode.addOutputNewline("#define configGATEWAY_ADDR2	192");
//		_configHCode.addOutputNewline("#define configGATEWAY_ADDR3	1");
//		_configHCode.addOutputNewline("#define configDNS_SERVER_ADDR0 10");
//		_configHCode.addOutputNewline("#define configDNS_SERVER_ADDR1  4");
//		_configHCode.addOutputNewline("#define configDNS_SERVER_ADDR2 4");
//		_configHCode.addOutputNewline("#define configDNS_SERVER_ADDR3 10");
		_configHCode.addOutputNewline("#define configNET_MASK0 255");
		_configHCode.addOutputNewline("#define configNET_MASK1 255");
		_configHCode.addOutputNewline("#define configNET_MASK2 255");
		_configHCode.addOutputNewline("#define configNET_MASK3 255");
		_configHCode.addOutputNewline("#define configPRINT_PORT	( 15000 )");
		_configHCode.addOutputNewline("#endif /* FREERTOS_CONFIG_H */");
	}
	
	protected void genIPConfigHCode(ProcessSubcomponent process)
	{
		_IPConfigHCode.addOutputNewline("");
		_IPConfigHCode.addOutputNewline("#ifndef FREERTOS_IP_CONFIG_H");
		_IPConfigHCode.addOutputNewline("#define FREERTOS_IP_CONFIG_H");
		
		//Set to 1 to print debugging and non debugging messages.
		// If set to 1, then FreeRTOS_printf must be set to the function used to print out messages
		_IPConfigHCode.addOutputNewline("#define ipconfigHAS_DEBUG_PRINTF	0");
		_IPConfigHCode.addOutputNewline("#define ipconfigHAS_PRINTF 0");	
		// Byte order of the target
		_IPConfigHCode.addOutputNewline("#define ipconfigBYTE_ORDER pdFREERTOS_LITTLE_ENDIAN");
		// Must be set to 1 if the card/driver includes the checksum offloading
		_IPConfigHCode.addOutputNewline("#define ipconfigDRIVER_INCLUDED_RX_IP_CHECKSUM   1");
		// Default timeouts
		_IPConfigHCode.addOutputNewline("#define ipconfigSOCK_DEFAULT_RECEIVE_BLOCK_TIME ( 5000 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigSOCK_DEFAULT_SEND_BLOCK_TIME ( 5000 )");
		//Include support for DNS caching
		_IPConfigHCode.addOutputNewline("#define ipconfigUSE_DNS_CACHE ( 1 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigDNS_CACHE_NAME_LENGTH ( 16 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigDNS_CACHE_ENTRIES ( 4 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigDNS_REQUEST_ATTEMPTS ( 2 )");
		
		_IPConfigHCode.addOutputNewline("#define ipconfigIP_TASK_PRIORITY ( configMAX_PRIORITIES - 2 )");
		// set the size in words of the stack allocated to FreeRTOS+TCP task
		_IPConfigHCode.addOutputNewline("#define ipconfigIP_TASK_STACK_SIZE_WORDS ( configMINIMAL_STACK_SIZE * 5 )");
		
		_IPConfigHCode.addOutputNewline("#define ipconfigUSE_NETWORK_EVENT_HOOK 0");
		
		_IPConfigHCode.addOutputNewline("#define ipconfigUDP_MAX_SEND_BLOCK_TIME_TICKS ( 5000U / portTICK_PERIOD_MS )");
		_IPConfigHCode.addOutputNewline("#define ipconfigUSE_DHCP 1");
		_IPConfigHCode.addOutputNewline("#define ipconfigMAXIMUM_DISCOVER_TX_PERIOD	( 120000U / portTICK_PERIOD_MS )");
		_IPConfigHCode.addOutputNewline("#define ipconfigARP_CACHE_ENTRIES 6");
		_IPConfigHCode.addOutputNewline("#define ipconfigMAX_ARP_RETRANSMISSIONS ( 5 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigMAX_ARP_AGE 150");
		_IPConfigHCode.addOutputNewline("#define ipconfigINCLUDE_FULL_INET_ADDR	1");
		_IPConfigHCode.addOutputNewline("#define ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS	60");
		_IPConfigHCode.addOutputNewline("#define ipconfigEVENT_QUEUE_LENGTH	( ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS + 5 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigALLOW_SOCKET_SEND_WITHOUT_BIND 1");
		// set TTL
		_IPConfigHCode.addOutputNewline("#define ipconfigUDP_TIME_TO_LIVE 128");
		_IPConfigHCode.addOutputNewline("#define ipconfigTCP_TIME_TO_LIVE 128");
		
		_IPConfigHCode.addOutputNewline("#define ipconfigUSE_TCP ( 1 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigUSE_TCP_WIN ( 1 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigNETWORK_MTU 1200U");
		_IPConfigHCode.addOutputNewline("#define ipconfigUSE_DNS 1");
		_IPConfigHCode.addOutputNewline("#define ipconfigREPLY_TO_INCOMING_PINGS 1");
		_IPConfigHCode.addOutputNewline("#define ipconfigSUPPORT_OUTGOING_PINGS	0");
		_IPConfigHCode.addOutputNewline("#define ipconfigSUPPORT_SELECT_FUNCTION 1");
		_IPConfigHCode.addOutputNewline("#define ipconfigFILTER_OUT_NON_ETHERNET_II_FRAMES 1");
		_IPConfigHCode.addOutputNewline("#define ipconfigETHERNET_DRIVER_FILTERS_FRAME_TYPES 1");
		_IPConfigHCode.addOutputNewline("#define configWINDOWS_MAC_INTERRUPT_SIMULATOR_DELAY ( 20 / portTICK_PERIOD_MS )");
		_IPConfigHCode.addOutputNewline("#define ipconfigPACKET_FILLER_SIZE 2U");
		_IPConfigHCode.addOutputNewline("#define ipconfigTCP_WIN_SEG_COUNT 240");
		_IPConfigHCode.addOutputNewline("#define ipconfigTCP_RX_BUFFER_LENGTH ( 1000 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigTCP_TX_BUFFER_LENGTH ( 1000 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigIS_VALID_PROG_ADDRESS(x) ( (x) != NULL )");
		_IPConfigHCode.addOutputNewline("#define ipconfigTCP_HANG_PROTECTION ( 1 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigTCP_HANG_PROTECTION_TIME	( 30 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigTCP_KEEP_ALIVE	( 1 )");
		_IPConfigHCode.addOutputNewline("#define ipconfigTCP_KEEP_ALIVE_INTERVAL ( 20 )");
		_IPConfigHCode.addOutputNewline("#define portINLINE __inline");
		_IPConfigHCode.addOutputNewline("#endif /* FREERTOS_IP_CONFIG_H */");
		
	}
	
	@Override
	  public TargetProperties
	      getSystemTargetConfiguration(EList<SystemImplementation> si,
	              IProgressMonitor monitor) throws GenerationException
	  {
	    return new RoutingProperties();
	  }
}