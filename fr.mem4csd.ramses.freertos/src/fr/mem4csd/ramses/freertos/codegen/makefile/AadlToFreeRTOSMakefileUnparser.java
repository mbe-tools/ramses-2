package fr.mem4csd.ramses.freertos.codegen.makefile;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set ;

import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.ProcessImplementation;
import org.osate.aadl2.ProcessSubcomponent;
import org.osate.aadl2.ProcessorSubcomponent;
import org.osate.aadl2.Subcomponent;
import org.osate.utils.internal.PropertyUtils;

import fr.mem4csd.ramses.core.codegen.AadlToDefaultMakefileUnparser;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;

public class AadlToFreeRTOSMakefileUnparser extends AadlToDefaultMakefileUnparser{
	
	public static final String FREERTOS_TARGET_ID = "FREERTOS";
		
	public final static String FREERTOS_RUNTIME_VAR_ENV = "FREERTOS_PATH" ;
	
	public AadlToFreeRTOSMakefileUnparser() {
		super();
	}
	
	@Override
	public void initializeTargetBuilder() {
		super.initializeTargetBuilder();
	}
	
	@Override
	public String getTargetName() {
		return FREERTOS_TARGET_ID;	
	}
	
	@Override
	protected String getAdditionalPreprocessingOptions(ProcessSubcomponent process) {
		
		Subcomponent processor =  AadlHelperImpl.getDeloymentProcessorSubcomponent(process);
		String portableArchiName = PropertyUtils.getEnumValue(processor, "Portable_Architecture");
		
		if (portableArchiName.equals("posix"))
			return "-DUSE_FREERTOS -DUSE_POSIX";
		else 
			return "-DUSE_FREERTOS";
	}
	
	@Override
	protected String getAdditionalLinkingOptions(ProcessSubcomponent process) {
		
		Subcomponent processor =  AadlHelperImpl.getDeloymentProcessorSubcomponent(process);
		ProcessImplementation procImpl = (ProcessImplementation) process.getSubcomponentType();
		String portableArchiName = PropertyUtils.getEnumValue(processor, "Portable_Architecture");
		
		String opt = "";
		
		if (portableArchiName.equals("posix"))
			opt += "-lpthread -lrt";
		
		if(GeneratorUtils.processUsesSOCKET(procImpl))
			opt += " -lpcap";
		
		return opt;
	}
	
	@Override
	protected void setTargetIncludeDirectories(ProcessSubcomponent process)
	{
		Set<URI> includeDirURI = new HashSet<URI>();
		includeDirURI.add(URI.createURI( "FreeRTOS" ).appendSegment( "Source" ).appendSegment( "include" ));
		
		Subcomponent processor =  AadlHelperImpl.getDeloymentProcessorSubcomponent(process);
		ProcessImplementation procImpl = (ProcessImplementation) process.getSubcomponentType();
		String portableArchiName = PropertyUtils.getEnumValue(processor, "Portable_Architecture");
		
		switch (portableArchiName)
		{
		case("posix") :
			includeDirURI.add(URI.createURI( "FreeRTOS" ).appendSegment( "Source" ).appendSegment( "portable" ).appendSegment( "ThirdParty" ).appendSegment( "GCC" ).appendSegment( "Posix" ));
			includeDirURI.add(URI.createURI( "FreeRTOS" ).appendSegment( "Source" ).appendSegment( "portable" ).appendSegment( "ThirdParty" ).appendSegment( "GCC" ).appendSegment( "Posix" ).appendSegment( "utils" ));
			break;
		
		case("arm_cm0") :
			includeDirURI.add(URI.createURI( "FreeRTOS" ).appendSegment( "Source" ).appendSegment( "portable" ).appendSegment( "GCC" ).appendSegment( "ARM_CM0" ));
			break;
		
		case("arm_cm3") :
			includeDirURI.add(URI.createURI( "FreeRTOS" ).appendSegment( "Source" ).appendSegment( "portable" ).appendSegment( "GCC" ).appendSegment( "ARM_CM3" ));
			break;
			
		case("arm_cm3_mpu") :
			includeDirURI.add(URI.createURI( "FreeRTOS" ).appendSegment( "Source" ).appendSegment( "portable" ).appendSegment( "GCC" ).appendSegment( "ARM_CM3_MPU" ));
			break;
		
		case("arm_cm4f") :
			includeDirURI.add(URI.createURI( "FreeRTOS" ).appendSegment( "Source" ).appendSegment( "portable" ).appendSegment( "GCC" ).appendSegment( "ARM_CM4F" ));
			break;
		
		case("arm_cm4_mpu") :
			includeDirURI.add(URI.createURI( "FreeRTOS" ).appendSegment( "Source" ).appendSegment( "portable" ).appendSegment( "GCC" ).appendSegment( "ARM_CM4_MPU" ));
			break;
		
		case("arm_cm7") :
			includeDirURI.add(URI.createURI( "FreeRTOS" ).appendSegment( "Source" ).appendSegment( "portable" ).appendSegment( "GCC" ).appendSegment( "ARM_CM7" ));
			break;
			
		default :		
		}
		//Include directories for TCP/UDP
		if(GeneratorUtils.processUsesSOCKET(procImpl))
		{
			includeDirURI.add(URI.createURI( "FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("include"));
			includeDirURI.add(URI.createURI( "FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("portable").appendSegment("Compiler").appendSegment("GCC"));		
		}
		
		//Add include directories to includeDirManager
		for (URI includeDir : includeDirURI)
		{
			_includeDirManager.addCommonDependency( container.getTargetInstallDirectoryURI().appendSegments( includeDir.segments() ) );
		}
		return;
	}
	
	@Override
	protected Set<URI> getTargetSourceFiles(ProcessSubcomponent process)
	{		
		Set<URI> result = new HashSet<URI>();
		Set<URI> includeDirURI = new HashSet<URI>();
		
		includeDirURI.add(URI.createURI( "FreeRTOS" ).appendSegment( "Source" ).appendSegment( "include" ));
		
		for(URI sourceFile: sourceFilesURISet)
		{
			if ( "aadl_dispatch.c".equals( sourceFile.lastSegment() ) )
			{
				result.add(container.getTargetRuntimeDirectoryURI().appendSegment( "aadl_dispatch_freertos.c" ) );
			}
			else if ( "aadl_ports_standard.c".equals( sourceFile.lastSegment() ) )
			{
				result.add(container.getTargetRuntimeDirectoryURI().appendSegment( "aadl_ports_standard_freertos.c" ) );
			}
		}

		Subcomponent processor =  AadlHelperImpl.getDeloymentProcessorSubcomponent(process);
		ProcessImplementation procImpl = (ProcessImplementation) process.getSubcomponentType();
		
		String heapNumber = PropertyUtils.getEnumValue(processor, "Heap");
		String portableArchiName = PropertyUtils.getEnumValue(processor, "Portable_Architecture");
		
		if (heapNumber != null)
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS").appendSegment("Source").appendSegment("portable").appendSegment("MemMang").appendSegment(heapNumber).appendFileExtension("c"));
		
		switch (portableArchiName)
		{
		case("posix") :
			// Add specific source files
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS").appendSegment("Source").appendSegment("portable").appendSegment("ThirdParty").appendSegment("GCC").appendSegment("Posix").appendSegment("utils").appendSegment("wait_for_event").appendFileExtension("c"));
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS").appendSegment("Source").appendSegment("portable").appendSegment("ThirdParty").appendSegment("GCC").appendSegment("Posix").appendSegment("port").appendFileExtension("c"));
			break;
		
		case("arm_cm0") :
			// Add specific source files
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS").appendSegment("Source").appendSegment("portable").appendSegment("GCC").appendSegment("ARM_CM0").appendSegment("port").appendFileExtension("c"));
			break;
		
		case("arm_cm3") :
			// Add specific source files
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS").appendSegment("Source").appendSegment("portable").appendSegment("GCC").appendSegment("ARM_CM3").appendSegment("port").appendFileExtension("c"));
			break;
			
		case("arm_cm3_mpu") :
			// Add specific source files
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS").appendSegment("Source").appendSegment("portable").appendSegment("GCC").appendSegment("ARM_CM3_MPU").appendSegment("port").appendFileExtension("c"));
			break;
		
		case("arm_cm4f") :
			// Add specific source files
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS").appendSegment("Source").appendSegment("portable").appendSegment("GCC").appendSegment("ARM_CM4F").appendSegment("port").appendFileExtension("c"));
			// Select specific include dir
			includeDirURI.add(URI.createURI( "FreeRTOS" ).appendSegment( "Source" ).appendSegment( "portable" ).appendSegment( "GCC" ).appendSegment( "ARM_CM4F" ));
			break;
		
		case("arm_cm4_mpu") :
			// Add specific source files
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS").appendSegment("Source").appendSegment("portable").appendSegment("GCC").appendSegment("ARM_CM4_MPU").appendSegment("port").appendFileExtension("c"));
			break;
		
		case("arm_cm7") :
			// Add specific source files
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS").appendSegment("Source").appendSegment("portable").appendSegment("GCC").appendSegment("ARM_CM7").appendSegment("port").appendFileExtension("c"));
			break;
			
		default :		
		}
		
		// Add generic FreeRTOS files
		String[] freertosFiles;
		File freertosDir = new File(container.getTargetInstallDirectoryURI().toString() + "/FreeRTOS/Source");
		
		FilenameFilter filter = new FilenameFilter() {
	        @Override
	        public boolean accept(File f, String name) {
	            return name.endsWith(".c");
	        }
	    };
	    
		freertosFiles = freertosDir.list(filter);
		for (String file : freertosFiles)
		{
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS").appendSegment("Source").appendSegment(file));
		}
		
		if(GeneratorUtils.processUsesSOCKET(procImpl))
		{
			//Add TCP/UDP source files
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("FreeRTOS_IP").appendFileExtension("c"));
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("FreeRTOS_ARP").appendFileExtension("c"));
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("FreeRTOS_DHCP").appendFileExtension("c"));
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("FreeRTOS_DNS").appendFileExtension("c"));
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("FreeRTOS_Sockets").appendFileExtension("c"));
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("FreeRTOS_TCP_IP").appendFileExtension("c"));
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("FreeRTOS_UDP_IP").appendFileExtension("c"));
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("FreeRTOS_TCP_WIN").appendFileExtension("c"));
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("FreeRTOS_Stream_Buffer").appendFileExtension("c"));
		
			result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("portable").appendSegment("BufferManagement").appendSegment("BufferAllocation_2").appendFileExtension("c"));
		//Add Network Interface, specific to target
			switch (portableArchiName)
			{
			case("posix"):
				result.add(container.getTargetInstallDirectoryURI().appendSegment("FreeRTOS-Plus").appendSegment("Source").appendSegment("FreeRTOS-Plus-TCP").appendSegment("portable").appendSegment("NetworkInterface").appendSegment("linux").appendSegment("NetworkInterface").appendFileExtension("c"));
				break;
				
			default:
			}
		}
		
		return result;
	}
	
	@Override
	public boolean validateTargetPath(URI runtimePath) {
		if(runtimePath!=null)
		{
			final String path;
			if(runtimePath.isFile())
				path = runtimePath.toFileString();
			else
				path = runtimePath.toString();
			File result = new File(path +File.separator+ "FreeRTOS");
			if(result.exists())
				return true;
		}
		return false;

	}
}
