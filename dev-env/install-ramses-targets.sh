# install necessary tools with apt
if ! wine 2> /dev/null; then
	echo "installing wine"
	 sudo apt install 'wine-stable'
fi

if ! make 2> /dev/null; then
	sudo apt install make
fi

#Installing needed packages
sudo apt install gcc
sudo apt install binutils
sudo apt install perl
sudo apt install libxml-xpath-perl
sudo apt install libxml-libxml-perl
sudo apt install mtools
sudo apt install qemu
sudo apt install zip
#Required to use MQTT
sudo apt install mosquitto
pip install paho-mqtt
#Required to display charts with Python
sudo apt-get install python-tk
#Required for FreeRTOS
sudo apt install libpcap-dev

# Path where pok and ntxOSEK will be installed. If you need to run the tests, keep it default.

TARGET_PATH="$HOME/ramses-resources"

if [ $# -eq 1 ]; then
	TARGET_PATH=$1
fi

echo "ntxOSEK and pok will be installed in $TARGET_PATH."
### TODO: allow to change target path while running the script

echo "Checking if $TARGET_PATH exists."

if ! [ -d $TARGET_PATH ]; then
	echo "creating $TARGET_PATH"
	if ! mkdir $TARGET_PATH 2> /dev/null; then
		echo "could not create $TARGET_PATH. Please check that the parent directory exists."
		exit 1
	fi
fi

### installs ntxOSEK

# change this to the path where you put your download of ntxOSEK. you can get it at https://sourceforge.net/projects/lejos-osek/files/nxtOSEK/

NTX_PATH=~/Downloads/nxtOSEK_v214b3.zip

# change this were you put your download of gnuarm. You can get it on the documentation's page

ARM_PATH=~/Downloads/gnuarm.zip

#change this to where you want gnuarm to be installed

TARGET_ARM_PATH=$TARGET_PATH

# change this were you put your download of sg.exe. You can get it on the documentation's page

SG_PATH=~/Downloads/sg.zip
ARM_PATH=~/Downloads/gnuarm.zip
POK_PATH=~/Downloads/pok-master.zip
FREERTOS_PATH=~/Downloads/FreeRTOSv202012.00.zip

SG_URL=https://mem4csd.telecom-paristech.fr/blog/wp-content/uploads/2020/10/sg.zip
ARM_URL=perso.telecom-paris.fr/borde/gnuarm.zip
POK_URL=https://github.com/pok-kernel/pok/archive/master.zip
FREERTOS_URL=https://github.com/FreeRTOS/FreeRTOS/releases/download/202012.00/FreeRTOSv202012.00.zip

wget $SG_URL -O $SG_PATH
wget $ARM_URL -O $ARM_PATH
wget $POK_URL -O $POK_PATH
wget $FREERTOS_URL -O $FREERTOS_PATH


#DO NOT CHANGE
TARGET_SG= $TARGET_PATH/nxtOSEK/toppers_osek/sg/

# TODO: allow gnuarm target path to be changed while running the script

unzip $ARM_PATH -d $TARGET_ARM_PATH

unzip $NTX_PATH -d $TARGET_PATH

unzip $SG_PATH -d $TARGET_SG

unzip $FREERTOS_PATH -d $TARGET_FREERTOS

### DO NOT CHANGE
TOOL_GCC=$TARGET_PATH/nxtOSEK/ecrobot/tool_gcc.mak
ECROBOT=$TARGET_PATH/nxtOSEK/ecrobot/ecrobot.mak
TO_CHANGE='/cygdrive/C/cygwin/GNUARM'
CHANGE=$TARGET_ARM_PATH/gnuarm
TEMP=temp

cat $TOOL_GCC | sed s,"$TO_CHANGE","$CHANGE", > $TEMP
cat $TEMP > $TOOL_GCC

TO_CHANGE='WINECONSOLE := wineconsole'
CHANGE="$TO_CHANGE\nWINE := wine"

cat $ECROBOT | sed s,"$TO_CHANGE","$CHANGE", > $TEMP
cat $TEMP > $ECROBOT

TO_CHANGE='$(WINECONSOLE) $(TOPPERS_OSEK_ROOT_SG)'
CHANGE='$(WINE) $(TOPPERS_OSEK_ROOT_SG)'

cat $ECROBOT | sed s,"$TO_CHANGE","$CHANGE", > $TEMP
cat $TEMP > $ECROBOT

### test of install
sudo apt-get install tk-dev ncurses-dev libmpfr-dev texinfo
sudo ln -s /usr/lib/x86_64-linux-gnu/libmpfr.so.6 /usr/lib/x86_64-linux-gnu/libmpfr.so.4

echo "Testing ntxOSEK"

cd $TARGET_PATH/nxtOSEK/samples_c/helloworld
make all

#echo "Go back on the doc to finish the installation."

#if make all > /dev/null; then
#	"Everything went acording to plan"
#else
#	"Something went wrong while installing ntxOSEK. Please try manually"
#	exit 1
#fi

### installs pok

# change this to the path where you put your download of pok. You can get it at https://github.com/pok-kernel/pok.

POK_PATH=~/Downloads/pok.zip

### TODO: Allow to change the path while running the script

echo "Installing pok"
unzip $POK_PATH -d $TARGET_PATH
mv $TARGET_PATH/pok-master $TARGET_PATH/pok
cd $TARGET_PATH/pok
make configure

echo "pok installed"
cowsay "have a nice day!"

### installs freertos

echo "Installing FreeRTOS"
unzip $FREERTOS_PATH -d $TARGET_PATH
mv $TARGET_PATH/FreeRTOSv202012.00 $TARGET_PATH/freertos

echo "FreeRTOS installed"
