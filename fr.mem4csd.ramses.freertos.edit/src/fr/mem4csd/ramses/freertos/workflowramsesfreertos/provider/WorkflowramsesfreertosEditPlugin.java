/**
 */
package fr.mem4csd.ramses.freertos.workflowramsesfreertos.provider;

import de.mdelab.workflow.provider.WorkflowEditPlugin;

import fr.mem4csd.ramses.core.arch_trace.provider.Arch_traceEditPlugin;

import fr.mem4csd.ramses.core.workflowramses.provider.WorkflowramsesEditPlugin;

import fr.mem4csd.ramses.linux.workflowramseslinux.provider.WorkflowramseslinuxEditPlugin;

import org.eclipse.emf.common.EMFPlugin;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.provider.EcoreEditPlugin;

import org.osate.aadl2.provider.Aadl2EditPlugin;

/**
 * This is the central singleton for the Workflowramsesfreertos edit plugin.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public final class WorkflowramsesfreertosEditPlugin extends EMFPlugin {
	/**
	 * Keep track of the singleton.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final WorkflowramsesfreertosEditPlugin INSTANCE = new WorkflowramsesfreertosEditPlugin();

	/**
	 * Keep track of the singleton.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static Implementation plugin;

	/**
	 * Create the instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramsesfreertosEditPlugin() {
		super
		  (new ResourceLocator [] {
		     Aadl2EditPlugin.INSTANCE,
		     Arch_traceEditPlugin.INSTANCE,
		     EcoreEditPlugin.INSTANCE,
		     WorkflowEditPlugin.INSTANCE,
		     WorkflowramsesEditPlugin.INSTANCE,
		     WorkflowramseslinuxEditPlugin.INSTANCE,
		   });
	}

	/**
	 * Returns the singleton instance of the Eclipse plugin.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the singleton instance.
	 * @generated
	 */
	@Override
	public ResourceLocator getPluginResourceLocator() {
		return plugin;
	}

	/**
	 * Returns the singleton instance of the Eclipse plugin.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the singleton instance.
	 * @generated
	 */
	public static Implementation getPlugin() {
		return plugin;
	}

	/**
	 * The actual implementation of the Eclipse <b>Plugin</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static class Implementation extends EclipsePlugin {
		/**
		 * Creates an instance.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public Implementation() {
			super();

			// Remember the static instance.
			//
			plugin = this;
		}
	}

}
