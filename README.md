# RAMSES-2

The source code of RAMSES-2 is organized as follows:

## ramses-2 main directory

 - fr.mem4csd.ramses.core: contains most of RAMSES-2 code, including the ramses Model-view-viewmodel pattern (modeled with an EMF meta-model), workflows templates, shared ramses workflow components (modeled with an EMF meta-model), utilities function, and shared ATL model transformations.

 - fr.mem4csd.ramses.${target}.*: contain the target-specific plugins of RAMSES. Some of these plugins share operating systems standard (i.e. arinc653, osek, posix) projects.

 - fr.mem4csd.ramses.${protocol}.*: contain the protocol-specific plugins of RAMSES.

 - fr.mem4csd.ramses.site: contains the project with the definition of the content of RAMSES update site.
 
 - fr.mem4csd.ramses.ui: contains the plugin to integrate ramses into OSATE.

 - fr.mem4csd.ramses.integration.all.*: contains the plugin to define the workflows that integrate all the protocols and targets supported by RAMSES.

## Subdirectories

### features

This directory contains the features published in RAMSES update site. A feature is defined for each target, and gathers: the plugins corresponding to this target (including operating systems standard plugins and supported protocols) and an illustrative example.

### plugins

This directory contains the C source code for RAMSES runtime. Here again, the features are divided into a similar structure as RAMSES plugins: core/operating systems standard/communicaiton protocols/targets.

### tests

This directory contains the J-Unit test plugins, as well as the test sources of RAMSES-2

### examples

contains the AADL and C source code of examples that are zipped and included in Example wizards. Here is a guide to produce new examples.

Examples archives (zip) are constructed as illustrated hereafter:

zip -r sampled-communications-linux.zip ./* .project

Then the archive is move to the examples project

mv sampled-communications-linux.zip ../../../fr.mem4csd.ramses.linux.examples/examples_src/

