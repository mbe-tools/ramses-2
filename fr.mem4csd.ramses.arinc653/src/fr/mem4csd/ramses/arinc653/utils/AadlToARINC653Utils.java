package fr.mem4csd.ramses.arinc653.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.xtext.EcoreUtil2;
import org.osate.aadl2.BooleanLiteral;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.ProcessorImplementation;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.VirtualProcessorSubcomponent;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.modelsupport.util.AadlUtil;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.codegen.utils.GenerationUtilsC;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;

public class AadlToARINC653Utils {

	public static ComponentInstance getPartition(ComponentInstance entry)
	{
		ComponentInstance partition=null;
		if(entry.getCategory().equals(ComponentCategory.PROCESS))
		{
			partition = AadlHelperImpl.getActualProcessorBinding(entry);
		}
		else if(entry.getCategory().equals(ComponentCategory.VIRTUAL_PROCESSOR))
		{
			partition = entry;
		}
		return partition;
	}

	
	public static ComponentInstance getProcessor(ComponentInstance entry)
	  {
		  ComponentInstance processor=null;
		  if(entry.getCategory().equals(ComponentCategory.THREAD))
		  {
			  ComponentInstance process = entry.getContainingComponentInstance();
			  while(!process.getCategory().equals(ComponentCategory.PROCESS) && 
					  process!=null)
				  process = process.getContainingComponentInstance();
			  if(process!=null)
				  return getProcessor(process);
			  else
				  return null;
		  }
		  if(entry.getCategory().equals(ComponentCategory.PROCESS))
		  {
			  processor = getProcessor(AadlToARINC653Utils.getPartition(entry));
		  }
		  else if(entry.getCategory().equals(ComponentCategory.VIRTUAL_PROCESSOR))
		  {
			  processor = AadlHelperImpl.getDeloymentProcessorInstance(entry);
			  if(processor == null)
			  {
				  processor = getProcessor(entry.getContainingComponentInstance());
			  }
		  }
		  else if(GenerationUtilsC.isProcessor(entry) || entry.getCategory().equals(ComponentCategory.PROCESSOR))
		  {
			  processor = entry;
		  }
		  return processor;
	  }
	
	public static List<ComponentInstance> getBindedVPI(ComponentInstance processor)
	  {
		  List<ComponentInstance> bindedVPI =
				  new ArrayList<ComponentInstance>() ;
		  for(ComponentInstance ci : EcoreUtil2.getAllContentsOfType(processor.eResource().getContents().get(0), ComponentInstance.class))
		  {
			if(!ci.getCategory().equals(ComponentCategory.VIRTUAL_PROCESSOR))
				continue;
			if(!AadlToARINC653Utils.getProcessor(ci).equals(processor))
				continue;
			bindedVPI.add(ci);
		  }
		  return bindedVPI;
	  }
	
	 public static int getPartitionIndex(VirtualProcessorSubcomponent ne, List<ComponentInstance> bindedVPI, ArchTraceSpec traces)
	  {
		  int referencedComponentId;
		  if(isSystemPartition(ne))
			  referencedComponentId = 0;
		  else
		  {
			  ComponentInstance inst = (ComponentInstance) traces.getTransformationTrace(ne);
			  if(inst == null &&  ne.eContainer() instanceof ProcessorImplementation)
			  {
				  ProcessorImplementation pi = (ProcessorImplementation) ne.eContainer();
				  referencedComponentId = pi.getOwnedVirtualProcessorSubcomponents().indexOf(ne);
			  }
			  else
				  referencedComponentId = bindedVPI.indexOf(inst) ;
		  }
		  return referencedComponentId;
	  }
	 
	 public static  boolean isSystemPartition(ComponentInstance entry) {
		  ComponentInstance partition = AadlToARINC653Utils.getPartition(entry);
		  if(partition==null)
			  return false;
		  return isSystemPartition((NamedElement) entry);
	  }

	  
	  private static boolean isSystemPartition(NamedElement entry) {
		  PropertyAssociation pa=AadlUtil.findOwnedPropertyAssociation(entry, "System_Partition");
		  if(pa != null)
		  {
			  ModalPropertyValue mpv = pa.getOwnedValues().get(0);
			  BooleanLiteral bl = (BooleanLiteral) mpv.getOwnedValue();
			  return bl.getValue();
		  }
		  return false;
	  }
}
