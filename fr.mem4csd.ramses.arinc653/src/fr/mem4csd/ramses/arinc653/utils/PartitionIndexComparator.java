package fr.mem4csd.ramses.arinc653.utils;

import java.util.Comparator;
import java.util.List;

import org.osate.aadl2.ProcessSubcomponent;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.VirtualProcessorSubcomponent;
import org.osate.aadl2.instance.ComponentInstance;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.arch_trace.util.ArchTraceSourceRetreival;

public class PartitionIndexComparator implements Comparator<ProcessSubcomponent> {
	
	private List<ComponentInstance> bindedVPI;
	private ArchTraceSpec traces;
	
	public PartitionIndexComparator(Subcomponent processor)
	{
		traces = ArchTraceSourceRetreival.getTracesFromNamedElement(processor);
        ComponentInstance processorInSourceModel = (ComponentInstance) traces.getTransformationTrace(processor);
        bindedVPI = AadlToARINC653Utils.getBindedVPI(processorInSourceModel);
	}
	
    @Override
    public int compare(ProcessSubcomponent a, ProcessSubcomponent b) {
    	ComponentInstance aInSourceModel = (ComponentInstance) traces.getTransformationTrace(a);
        VirtualProcessorSubcomponent vps_a = (VirtualProcessorSubcomponent) AadlToARINC653Utils.getPartition(aInSourceModel).getSubcomponent();
        ComponentInstance bInSourceModel = (ComponentInstance) traces.getTransformationTrace(b);
        VirtualProcessorSubcomponent vps_b = (VirtualProcessorSubcomponent) AadlToARINC653Utils.getPartition(bInSourceModel).getSubcomponent();
        
		int index_a = AadlToARINC653Utils.getPartitionIndex(vps_a, bindedVPI, traces);
		int index_b = AadlToARINC653Utils.getPartitionIndex(vps_b, bindedVPI, traces);
    	
        return index_a < index_b ? -1 : index_a == index_b ? 0 : 1;
    }
}