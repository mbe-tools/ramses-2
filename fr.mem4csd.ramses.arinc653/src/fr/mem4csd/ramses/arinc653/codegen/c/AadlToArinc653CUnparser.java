/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.arinc653.codegen.c;

import java.util.List ;

import org.eclipse.core.runtime.IProgressMonitor ;
import org.eclipse.emf.common.util.EList ;
import org.osate.aadl2.DirectionType ;
import org.osate.aadl2.ProcessImplementation ;
import org.osate.aadl2.ProcessSubcomponent ;
import org.osate.aadl2.SystemImplementation ;
import org.osate.aadl2.ThreadSubcomponent ;
import org.osate.aadl2.instance.SystemInstance ;
import org.osate.aadl2.modelsupport.UnparseText ;
import org.osate.utils.internal.PropertyUtils ;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.codegen.AadlToXUnparser ;
import fr.mem4csd.ramses.core.codegen.GenerationException;
import fr.mem4csd.ramses.core.codegen.GenerationInfos.QueueInfo ;
import fr.mem4csd.ramses.core.codegen.GenerationInfos.SampleInfo ;
import fr.mem4csd.ramses.core.codegen.ProcessProperties ;
import fr.mem4csd.ramses.core.codegen.RoutingProperties ;
import fr.mem4csd.ramses.core.codegen.c.AadlToCUnparser;
import fr.mem4csd.ramses.core.codegen.utils.GenerationUtilsC;
import fr.mem4csd.ramses.core.workflowramses.TargetProperties;

public abstract class AadlToArinc653CUnparser extends AadlToXUnparser
{
  
  protected UnparseText _deploymentHCode;
  
  
  
  protected void genQueueMainImpl(ProcessProperties pp)
  {
    for(QueueInfo info : pp.queueInfo)
    {
      _mainCCode.addOutput("CREATE_QUEUING_PORT (\"") ;
      _mainCCode.addOutput(info.uniqueId);
      _mainCCode.addOutputNewline("\",");
      _mainCCode.addOutput("    sizeof( "+info.dataType+" ), ");
      _mainCCode.addOutput(Long.toString(info.size)) ;
      _mainCCode.addOutput(", ");
      
      String direction = null ;
      if(DirectionType.IN == info.direction)
      {
        direction = "DESTINATION" ;
      }
      else
      {
        direction = "SOURCE" ;
      }
      
      _mainCCode.addOutput(direction);
      _mainCCode.addOutput(", ") ;
      _mainCCode.addOutput(info.type.toUpperCase());
      _mainCCode.addOutputNewline(",");
      _mainCCode.addOutput("      &(") ;
      _mainCCode.addOutput(info.id+"_QUEUEING") ;
      _mainCCode.addOutputNewline("), &(ret));") ;
      
      _mainCCode.addOutputNewline(info.id+".partition_port = &"+info.id+"_QUEUEING;");
    }
  }
  
  protected void genSampleMainImpl(UnparseText _mainCCode,
                                 ProcessProperties pp)
  {
    for(SampleInfo info : pp.sampleInfo)
    {
      _mainCCode.addOutput("CREATE_SAMPLING_PORT (\"") ;
      _mainCCode.addOutput(info.uniqueId);
      _mainCCode.addOutputNewline("\",");
      _mainCCode.addOutput("    sizeof( " + info.dataType + " ), ");
      
      String direction = null ;
      if(DirectionType.IN == info.direction)
      {
        direction = "DESTINATION" ;
      }
      else
      {
        direction = "SOURCE" ;
      }
      
      _mainCCode.addOutput(direction);
      _mainCCode.addOutput(", ") ;
      _mainCCode.addOutput(Long.toString(info.refresh)) ;
      _mainCCode.addOutputNewline(",");
      _mainCCode.addOutput("      &(") ;
      _mainCCode.addOutput(info.id+"_SAMPLING") ;
      _mainCCode.addOutputNewline("), &(ret));") ;
      _mainCCode.addOutputNewline(info.id+".partition_port = &"+info.id+"_SAMPLING;");

    }
  }
  
  
  protected void genGlobalVariablesMainOptional(ProcessImplementation process,
		UnparseText _mainCCode){}

  @Override
  protected void genFileIncludedMainImpl()
  {
	  super.genFileIncludedMainImpl();
	  _deploymentHCode.addOutputNewline("#include \"main.h\"") ;
  }

  private void genThreadErrorHandlerImpl(ThreadSubcomponent thread,
        UnparseText _mainCCode)
  {
    _mainCCode.addOutput("tattr.ENTRY_POINT = ") ;
	_mainCCode.addOutput(thread.getName()) ;
	_mainCCode.addOutputNewline(GenerationUtilsC.THREAD_SUFFIX + ';') ;
	_mainCCode
    .addOutput("CREATE_ERROR_HANDLER (tattr.ENTRY_POINT,");
	_mainCCode
    .addOutput("8192,");
    _mainCCode
    	    .addOutputNewline("&(ret));");
  }
  
  private void genSemaphoreMainImpl(ProcessProperties pp)
  {
    _mainCCode.addOutput("CREATE_SEMAPHORE (") ;
    _mainCCode.addOutput("pok_arinc653_semaphores_names[0]");
    _mainCCode.addOutput(", 0, "
    		+ pp.tasksNb +
    		", PRIORITY, ") ;
    _mainCCode.addOutput("&init_barrier_event,");
    _mainCCode.addOutputNewline("& (ret));");

    _mainCCode.addOutput("CREATE_SEMAPHORE (") ;
    _mainCCode.addOutput("pok_arinc653_semaphores_names[1]");
    _mainCCode.addOutput(", 1, 1, PRIORITY, ") ;
    _mainCCode.addOutput("&init_barrier_mutex,");
    _mainCCode.addOutputNewline("& (ret));");
    
	int i=2;
    // For each semaphore
    for(String info : pp.waitMessageNames)
    {
      _mainCCode.addOutput("CREATE_SEMAPHORE (");
      _mainCCode.addOutput("pok_arinc653_semaphores_names["+i+"]") ;
      _mainCCode.addOutput(", "
      		+ 0
      		+ ", "
      		+ 1
      		+ ", PRIORITY, &") ;
      _mainCCode.addOutput(info+".event");
      _mainCCode.addOutputNewline(", &(ret));") ;
      i++;
      _mainCCode.addOutput("CREATE_SEMAPHORE (");
      _mainCCode.addOutput("pok_arinc653_semaphores_names["+i+"]") ;
      _mainCCode.addOutput(", "
      		+ 1
      		+ ", "
      		+ 1
      		+ ", PRIORITY, &") ;
      _mainCCode.addOutput(info+".rez");
      _mainCCode.addOutputNewline(", &(ret));") ;
      i++;
    }
  }
  
  protected void genMainImpl(ProcessSubcomponent process,
                           ProcessProperties pp, ArchTraceSpec traces, List<String> additionalHeaders)
  {
	ProcessImplementation processImpl = (ProcessImplementation) 
              process.getComponentImplementation() ;

    EList<ThreadSubcomponent> lthreads =
    		processImpl.getOwnedThreadSubcomponents() ;
    
    // Included files.
    genFileIncludedMainImpl() ;
    
    // Global files.
    AadlToCUnparser.getAadlToCUnparser().genMainGlobalVariables(process, _mainCCode);
    genGlobalVariablesMainImpl(process, lthreads, _mainCCode, pp);
    
    _mainCCode.addOutputNewline(GenerationUtilsC.generateSectionMark()) ;
    _mainCCode.addOutputNewline(GenerationUtilsC
            .generateSectionTitle("MODE INIT")) ;
    AadlToCUnparser.getAadlToCUnparser().genModeInit(process, _mainCCode);
    addModeInitPostfix(process);
    _mainCCode.decrementIndent();
    _mainCCode.addOutputNewline("}");
    _mainCCode.addOutputNewline("");
    
    
    // main function declaration.
    _mainCCode.addOutputNewline(GenerationUtilsC.generateSectionMark()) ;
    _mainCCode.addOutputNewline(GenerationUtilsC
          .generateSectionTitle("MAIN")) ;
    
    _mainCCode.addOutputNewline("int main ()") ;
    _mainCCode.addOutputNewline("{") ;
    
    _mainCCode.incrementIndent();
    
    _mainCCode.addOutputNewline("PROCESS_ATTRIBUTE_TYPE tattr;") ;
    _mainCCode.addOutputNewline("RETURN_CODE_TYPE ret;") ;
    
    // Semaphore declarations.
    genSemaphoreMainImpl(pp) ;
    
    // Queue declarations.
    genQueueMainImpl(pp) ;
    
    // Sample declarations.
    genSampleMainImpl(_mainCCode, pp) ;
    
    // For each declared thread
    // Zero stands for ARINC's IDLZ thread.
    // int threadIndex = 1 ;
    
    // Thread declarations.
    for(ThreadSubcomponent thread : lthreads)
    {
      Boolean foundHM = PropertyUtils.getBooleanValue(thread, "Error_Handling") ;
      if(foundHM == null)
      {
        foundHM = false ;
      }
      
      if(foundHM)
      {
        genThreadErrorHandlerImpl(thread, _mainCCode) ;
      }
      else
      {
        generateErrorHandlerCreation(process);
        break;
      }
    }
    AadlToCUnparser.getAadlToCUnparser().genMainCCode(process, _mainCCode, traces, additionalHeaders);
    
    
    _mainCCode.addOutputNewline("tattr.ENTRY_POINT = mode_init;");
    _mainCCode.addOutputNewline("tattr.PERIOD = INFINITE_TIME_VALUE;");
    _mainCCode.addOutputNewline("tattr.DEADLINE = INFINITE_TIME_VALUE;");
    _mainCCode.addOutputNewline("tattr.TIME_CAPACITY = INFINITE_TIME_VALUE;");
    _mainCCode.addOutputNewline("tattr.BASE_PRIORITY = MAX_PRIORITY_VALUE;");
    _mainCCode.addOutputNewline("strcpy(tattr.NAME, \"mode_init\");");
    _mainCCode.addOutputNewline("PROCESS_ID_TYPE pid;");
    _mainCCode.addOutputNewline("CREATE_PROCESS (&(tattr), &pid, &(ret));");
    _mainCCode.addOutputNewline("START(pid, &ret);");
    
    _mainCCode.addOutputNewline("SET_PARTITION_MODE (NORMAL, &(ret));") ;

    
    _mainCCode.addOutputNewline("return (0);") ;
    _mainCCode.decrementIndent();
    _mainCCode.addOutputNewline("}") ;
  }

  protected abstract void generateErrorHandlerCreation(ProcessSubcomponent process);

protected void genGlobalVariablesMainImpl(ProcessSubcomponent process, EList<ThreadSubcomponent> lthreads,
		UnparseText _mainCCode, ProcessProperties pp)
  {
	  
	  _mainCCode .addOutputNewline("SEMAPHORE_ID_TYPE init_barrier_event;");
	  _mainCCode .addOutputNewline("SEMAPHORE_ID_TYPE init_barrier_mutex;");

	  // Generate queue names array.
	  if(pp.hasQueue)
	  {
		  for(QueueInfo info : pp.queueInfo)
		  {
			  _mainCCode.addOutput("QUEUING_PORT_ID_TYPE ") ;
			  _mainCCode.addOutput(info.id+"_QUEUEING") ;
			  _mainCCode.addOutputNewline(";") ;
		  }
	  }

	  // Generate sample names array.
	  if(pp.hasSample)
	  {
		  for(SampleInfo info : pp.sampleInfo)
		  {
			  _mainCCode.addOutput("SAMPLING_PORT_ID_TYPE ") ;
			  _mainCCode.addOutput(info.id+"_SAMPLING") ;
			  _mainCCode.addOutputNewline(";") ;
		  }
	  }

  }

protected void addModeInitPostfix(ProcessSubcomponent process) {
	  _mainCCode.addOutputNewline("    STOP_SELF();");
  }

  protected void genDeploymentHeaderEnd(UnparseText deploymentHeaderCode){}

  @Override
  public TargetProperties getSystemTargetConfiguration(EList<SystemImplementation> siList,
                                  IProgressMonitor monitor)
	     	                                             throws GenerationException
	{
	  RoutingProperties routing = new RoutingProperties();

	  for(int i=0;i<siList.size();i++)
	  {
		  ArchTraceSpec traces = getCodeGenWorkflowComponent().getModelTransformationTracesList().get(i);
		  routing.setTraces(traces);
		  SystemInstance system = (SystemInstance) 
				  traces.getTransformationTrace(siList.get(i)) ;
	      
		  routing.setRoutingProperties(system); 
	  }	  
	  return routing ;
  }


  @Override
  protected String getGenerationIdentifier(String prefix)
  {
    return GenerationUtilsC.getGenerationCIdentifier(prefix) ;
  }

}
