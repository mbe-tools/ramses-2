<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowosate="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowosate" xmi:id="_utIloNj9EeiIc5Eb0h0CDw" name="workflow" description="Operations of validation for arinc653">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_IHxPUOXAEem9M-u-TfGZKA" name="load_ramses_aadl_resources_core" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/load_ramses_aadl_resources_core.workflow">
    <propertyValues xmi:id="_vQXasE4nEeqIOpzmJvnc-w" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_y5oncE4nEeqIOpzmJvnc-w" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_44HYEE4nEeqIOpzmJvnc-w" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_h-d4odkTEeiIc5Eb0h0CDw" name="${NameInputResourcesReader}" description="" modelSlot="sourceAadlModel" modelURI="${source_aadl_file}" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflowosate:AadlInstanceModelCreator" xmi:id="_h-d4otkTEeiIc5Eb0h0CDw" name="aadlInstanceModelCreator" description="" systemImplementationName="${system_implementation_name}" packageModelSlot="sourceAadlModel" systemInstanceModelSlot="sourceAadlInstance"/>
  <components xsi:type="workflow.components:ModelWriter" xmi:id="_JxKyEKpJEeaQaI5SJTCSUw" name="modelWriter" modelSlot="sourceAadlInstance" modelURI="${instance_model_file}" cloneModel="false" deresolvePluginURIs="true" resolveURI="false"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_jPWjoNkREeiIc5Eb0h0CDw" name="valivate_arinc653" description="" workflowURI="${scheme}${ramses_arinc653_plugin}ramses/arinc653/workflows/validate_arinc653_instance.workflow">
    <propertyValues xmi:id="_VnqN4KXZEeqzUc24dshnew" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_fKt4gKXZEeqzUc24dshnew" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_kUIpYKXZEeqzUc24dshnew" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_3Yv24Lb9EeqkNY6l67PJRQ" name="atl_transformation_utilities_plugin" defaultValue="${atl_transformation_utilities_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_9_UqoKXYEeqzUc24dshnew" name="source_aadl_file" defaultValue="${source_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_AuUngKXZEeqzUc24dshnew" name="system_implementation_name" defaultValue="${system_implementation_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_Cvf1EaXZEeqzUc24dshnew" name="validation_report_file" defaultValue="${validation_report_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_Cvf1EqXZEeqzUc24dshnew" name="instance_model_file" defaultValue="${instance_model_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_LPUhEKXZEeqzUc24dshnew" name="core_runtime_dir" defaultValue="${core_runtime_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_QFgpAF7BEeqTbYoc6DiMAg" name="source_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_eFej0F7BEeqTbYoc6DiMAg" name="validation_report_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_czBA8F7CEeqTbYoc6DiMAg" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_zINNUKtNEeq11-G80If4fQ" name="instance_model_file" defaultValue="${instance_model_file}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_sPQ2oE4mEeqIOpzmJvnc-w" fileURI="default_arinc653.properties"/>
  <propertiesFiles xmi:id="_BXjMINsKEeq05enNFbiy6w" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
