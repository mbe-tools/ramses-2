<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_1bNJoEgDEeqsT5NnXFzohg" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="__gTxYEgDEeqsT5NnXFzohg" name="load_refinement_core" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/load_ramses_aadl_resources_core.workflow">
    <propertyValues xmi:id="_EAsO0Lb-EeqkNY6l67PJRQ" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_Xa0OYEk8Eeux84O3cpDbTg" name="${NameRamsesAadlResourcesReader}" modelSlot="ARINC653_runtime" modelURI="${scheme}${ramses_arinc653_plugin}ramses/arinc653/packages/ARINC653_runtime.aadl" modelElementIndex="0"/>
  <propertiesFiles xmi:id="_C7pdEE4pEeqIOpzmJvnc-w" fileURI="default_arinc653.properties"/>
  <propertiesFiles xmi:id="_SGHcMNr_Eeq05enNFbiy6w" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
