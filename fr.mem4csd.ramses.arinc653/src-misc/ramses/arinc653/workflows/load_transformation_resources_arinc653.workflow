<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_1bNJoEgDEeqsT5NnXFzohg" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="__gTxYEgDEeqsT5NnXFzohg" name="load_refinement_core" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/load_transformation_resources_core.workflow">
    <propertyValues xmi:id="_EAsO0Lb-EeqkNY6l67PJRQ" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_Fte6ELb5EeqkNY6l67PJRQ" name="atl_transformation_utilities_plugin" defaultValue="${atl_transformation_utilities_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_OTCXIP8lEeqjUK5cPU4ZFw" name="load_refinement_core" workflowURI="${scheme}${ramses_arinc653_plugin}ramses/arinc653/workflows/load_refinement_arinc653.workflow">
    <propertyValues xmi:id="_OTCXIf8lEeqjUK5cPU4ZFw" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_emSjMP8lEeqjUK5cPU4ZFw" name="load_refinement_core" workflowURI="${scheme}${ramses_arinc653_plugin}ramses/arinc653/workflows/load_validation_arinc653.workflow">
    <propertyValues xmi:id="_emSjMf8lEeqjUK5cPU4ZFw" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_EVPtQE4pEeqIOpzmJvnc-w" name="ramses_arinc653_transformation_path" defaultValue="${scheme}${ramses_arinc653_plugin}ramses/arinc653/transformations/atl/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_VLB-oE4pEeqIOpzmJvnc-w" name="ramses_arinc653_transformation_refinement_path" defaultValue="${ramses_arinc653_transformation_path}refinement/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_C7pdEE4pEeqIOpzmJvnc-w" fileURI="default_arinc653.properties"/>
  <propertiesFiles xmi:id="_SGHcMNr_Eeq05enNFbiy6w" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
