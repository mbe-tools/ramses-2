<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_16JoEKTMEeqi7MtCpWPvXA" name="workflow">
  <components xsi:type="workflow.components:ModelReader" xmi:id="_y7EV4KTOEeq1YeAByobchg" name="${NameLoadValidationTransformationModules}" modelSlot="Arinc653Properties" modelURI="${scheme}${ramses_arinc653_plugin}ramses/arinc653/transformations/atl/validation/ArincProperties.emftvm" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="__Pn3wKTOEeq1YeAByobchg" name="${NameLoadValidationTransformationModules}" modelSlot="InterPartitionCommunications" modelURI="${scheme}${ramses_arinc653_plugin}ramses/arinc653/transformations/atl/validation/InterPartitionCommunications.emftvm" modelElementIndex="0"/>
  <propertiesFiles xmi:id="_leTbQAS0EeuJWt_xi0KeeQ" fileURI="default_arinc653.properties"/>
  <propertiesFiles xmi:id="_SGHcMNr_Eeq05enNFbiy6w" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
