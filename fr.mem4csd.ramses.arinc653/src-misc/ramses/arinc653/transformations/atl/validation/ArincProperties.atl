--
-- RAMSES 2.0
-- 
-- Copyright © 2012-2015 TELECOM Paris and CNRS
-- Copyright © 2016-2020 TELECOM Paris
-- 
-- TELECOM Paris
-- 
-- Authors: see AUTHORS
--
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program. 
-- If not, see https://www.eclipse.org/legal/epl-2.0/
--

-- @nsURI AADLBA=/fr.tpt.aadl.annex.behavior/model/aadlba.ecore
-- @atlcompiler emftvm

module ArincProperties;
create OUT : CV 	from 		IN : AADLI,
								TIMING_PROPERTIES : AADLI,
								ARINC653 : AADLI;

-- @extends check_ProcessorInstance
rule check_ProcessorInstance_Arinc
{
	from
		obj: AADLI!ComponentInstance
		(obj.isBoundToRefinementTarget())
	to
	do
	{
		thisModule.ulr_Check_PortsFlushTime(obj);
		thisModule.ulr_Check_ProcessorHasARINC653Scheduler(obj);
		thisModule.ulr_Check_ArincProcessorBinding(obj);
		thisModule.ulr_Check_HasMajorFrame(obj);
		thisModule.ulr_Check_HasModuleSchedule(obj);
	}
}

-- @extends ulr_Check_ComponentInstance
unique lazy rule ulr_Check_PortsFlushTime
{
	from
	 	obj:AADLI!ComponentInstance
		(
			obj.hasProperty('PortsFlushTime')
			and
			obj.ownedPropertyAssociation->any(e|e.property.name = 'Ports_Flush_Time').ownedValue->first()
			.ownedValue.namedValue.name = 'Minor_Frame_Switch'
			and not obj.hasProperty('Module_Minor_Frame'))
	to
		err: CV!Error
		(message <- 'ARINC653: when ports are flushed at minor frames switch, ARINC653::Module_Minor_Frame must be set' )
}

-- @extends ulr_Check_ComponentInstance
unique lazy rule ulr_Check_ProcessorHasARINC653Scheduler
{
	from
	 	obj:AADLI!ComponentInstance
		(
			obj.hasProperty('Scheduling_Protocol')
			and
			not obj.ownedPropertyAssociation->any(
				e | e.property.name.toLower() = 'scheduling_protocol'
			).isValidARINC653Scheduler()
		)
	to
		err: CV!Error
		(message <- 'Invalid scheduling_protocol for '+obj.subcomponent.name+ 
		' should be: arinc653' )
}

-- @extends ulr_Check_ComponentInstance
unique lazy rule ulr_Check_ArincProcessorBinding
{
	from
		obj: AADLI!ComponentInstance(
			not obj.getActualProcessorBinding().oclIsUndefined()
			and
			not obj.getActualProcessorBinding()->isEmpty()
			and
			not (obj.getActualProcessorBinding()->first().category.getName() = 'virtual processor')
		)
	to
		err: CV!Error
		(message <- 'ARINC653 process subcomponent '+obj.subcomponent.name + ' must be deployed on a virtual processor' )
}

-- @extends ulr_Check_ComponentInstance
unique lazy rule ulr_Check_HasMajorFrame
{
	from
	 	obj:AADLI!ComponentInstance
		(not obj.hasProperty('Module_Major_Frame'))
	to
		err: CV!Error
		(message <- 'ARINC653 processor '+ obj.subcomponent.name + 
			' must have a property value for ARINC653::Module_Major_Frame' )
}

-- @extends ulr_Check_ComponentInstance
unique lazy rule ulr_Check_HasModuleSchedule
{
	from
	 	obj:AADLI!ComponentInstance
		(
			not obj.hasProperty('Module_Schedule')
		)
	to
		err: CV!Error
		(message <- obj.subcomponent.name + '; In ARINC653, each processor must'+ 
			' have a property value for ARINC653::Module_Schedule' )
}

rule check_EachPartitionHasScheduleSlot
{
	from
	 	obj:AADLI!ComponentInstance,
		part:AADLI!ComponentInstance
		(
		  obj.isProcessor()
		  and
		  part.category.getName()='virtual processor'
		  and
		  obj.hasProperty('Module_Schedule')
		  and
		  obj.componentInstance->contains(part)
		  and
		  not obj.ownedPropertyAssociation->any(pa|pa.property.name='Module_Schedule').ownedValue->first().ownedValue.ownedListElement->exists(e|
		  	e.ownedFieldValue->any(e| e.property.name.toLower()='partition').ownedValue.referencedInstanceObject = part)
		)
	to
		err: CV!Error
		(message <- 'ARINC653::Slots_Allocation property must reference all the partitions; '+part.name+
			' is not referenced in the ARINC653::Slots_Allocation property')
}


-- @extends check_ProcessInstance
rule check_ProcessInstance_ARINC
{
	from
		obj: AADLI!ComponentInstance
	to
	do
	{
		thisModule.ulr_Check_ArincUniqueProcessorBinding(obj);
	}
}

-- @extends ulr_Check_ComponentInstance
unique lazy rule ulr_Check_ArincUniqueProcessorBinding
{
	from
		obj: AADLI!ComponentInstance
		(	not obj.getActualProcessorBinding().oclIsUndefined()
			and
			not obj.getActualProcessorBinding()->isEmpty()
			and
		 	obj.getActualProcessorBinding()->first().category.getName() = 'virtual processor'
		 	and
		 	obj.isNotAlone()
		)
	to
		err: CV!Error
		(message <- 'In ARINC653, each process subcomponent must'+ 
			' be deployed on a different virtual processor. This is not the case for '+obj.subcomponent.name )
}

--@extends check_VirtualProcessorInstance
rule check_VirtualProcessorInstance_ARINC
{
	from
		obj: AADLI!ComponentInstance
	to
	do
	{
		thisModule.ulr_Check_HasPeriod(obj);
		thisModule.ulr_Check_HasPartitionWindow(obj);
	}
}

-- @extends ulr_Check_ComponentInstance
unique lazy rule ulr_Check_HasPeriod
{
	from
	 	obj:AADLI!ComponentInstance
		(not obj.hasProperty('Period'))
	to
		err: CV!Error
		(message <- 'Virtual processor component '+obj.name+' must '+ 
			'have a property value for Period (ARINC653)' )
}

-- @extends ulr_Check_ComponentInstance
unique lazy rule ulr_Check_HasPartitionWindow
{
	from
		obj:AADLI!ComponentInstance
		(
			let processors : Set(AADLI!ComponentInstance) = obj.getProcessorBinding() in
			not processors->isEmpty()
			and
			(not processors->at(1).hasProperty('Module_Schedule')
			or
			not processors->at(1).getScheduledVirtualProcessor()->contains(obj)
			)
		)
	to
		err: CV!Error
		(message <- 'Virtual processor component '+obj.qualifiedName+' must '+ 
			'be associated with a partition window using property ARINC653::Module_Schedule' )
}

helper context AADLI!ComponentInstance def: getScheduledVirtualProcessor(): Set(AADLI!ComponentInstance) =
	let pa: AADLI!PropertyAssociation = thisModule.theTOOLS.findPropertyAssociation('Module_Schedule', self) in 
	let mpv: AADLI!ModalPropertyValue = pa.ownedValue->first() in
	let lv: AADLI!ListValue = mpv.ownedValue in 
	lv.ownedListElement->collect(
		e | let rv:AADLI!RecordValue = e in
			rv.ownedFieldValue->select( 
				f | let bpa: AADLI!BasicPropertyAssociation = f in
					bpa.ownedValue.oclIsKindOf(AADLI!InstanceReferenceValue)
			)
			->collect(f|
				let bpa: AADLI!BasicPropertyAssociation = f in
					bpa.ownedValue.referencedInstanceObject
				)
	)->asSet()->flatten()
;

helper context AADLI!ComponentInstance def: isNotAlone() : Boolean =
	let selfProcessorBinding : Sequence(AADLI!ComponentInstance) = self.getActualProcessorBinding() in
	if selfProcessorBinding.oclIsUndefined() or selfProcessorBinding->isEmpty() then
		false
	else
		thisModule.getAllInstances('process')->excluding(self)->select(e| (not e.getActualProcessorBinding().oclIsUndefined()) and (not e.getActualProcessorBinding()->isEmpty()))
		->collect(e | e.getActualProcessorBinding()->first())
		->exists(e | e=self.getActualProcessorBinding()->first())
	endif
;

helper context AADLI!PropertyAssociation def : isValidARINC653Scheduler() : Boolean =
	let schedValue: String = self.ownedValue->first().ownedValue.ownedListElement->first().namedValue.name in
	schedValue.toLower() = 'arinc653'
;
