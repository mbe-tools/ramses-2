/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.osek.codegen.c;

import java.io.IOException ;
import java.util.ArrayList ;
import java.util.Collection;
import java.util.HashMap;
import java.util.List ;
import java.util.Map ;

import org.apache.log4j.Logger ;
import org.eclipse.core.runtime.IProgressMonitor ;
import org.eclipse.emf.common.util.EList ;
import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.DataSubcomponent ;
import org.osate.aadl2.DirectionType;
import org.osate.aadl2.Mode ;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.ProcessImplementation ;
import org.osate.aadl2.ProcessSubcomponent ;
import org.osate.aadl2.Subcomponent ;
import org.osate.aadl2.SystemImplementation ;
import org.osate.aadl2.ThreadSubcomponent ;
import org.osate.aadl2.instance.ComponentInstance ;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.FeatureCategory;
import org.osate.aadl2.instance.FeatureInstance ;
import org.osate.aadl2.modelsupport.UnparseText ;
import org.osate.utils.internal.PropertyUtils ;
import org.osate.xtext.aadl2.properties.util.AadlProject;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.codegen.AadlToXUnparser;
import fr.mem4csd.ramses.core.codegen.GenerationException;
import fr.mem4csd.ramses.core.codegen.ProcessProperties;
import fr.mem4csd.ramses.core.codegen.RoutingProperties;
import fr.mem4csd.ramses.core.codegen.utils.FileUtils;
import fr.mem4csd.ramses.core.codegen.utils.GenerationUtilsC;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;
import fr.mem4csd.ramses.core.helpers.impl.CodeGenHelperImpl;
import fr.mem4csd.ramses.core.workflowramses.TargetProperties;
import fr.mem4csd.ramses.osek.codegen.ast.Alarm;
import fr.mem4csd.ramses.osek.codegen.ast.Alarm.Action;
import fr.mem4csd.ramses.osek.codegen.ast.Counter;
import fr.mem4csd.ramses.osek.codegen.ast.Cpu;
import fr.mem4csd.ramses.osek.codegen.ast.Hook;
import fr.mem4csd.ramses.osek.codegen.ast.OIL;
import fr.mem4csd.ramses.osek.codegen.ast.Os;
import fr.mem4csd.ramses.osek.codegen.ast.Os.Status;
import fr.mem4csd.ramses.osek.codegen.ast.Subprogram;
import fr.mem4csd.ramses.osek.codegen.ast.Task;
import fr.mem4csd.ramses.osek.codegen.ast.Task.Schedule;

/**
 * Unparser to generate an oil file for OSEK from aadl.
 */
public class AadlToOSEKCUnparser extends AadlToXUnparser  {

	private final static String MAIN_APP_MODE = "default_mode";

	/**
	 * Variable OIL
	 */
	private final static String OIL_VERSION = "2.5";

	/**
	 * Counter
	 */
	private final static String COUNTER_NAME = "SystemCounter";

	/**
	 * Thread
	 */
	private final static int THREAD_ACTIVATION = 1;
	private final static Long THREAD_STACKSIZE = 512l;

	/**
	 * OIL AST
	 */
	private OIL oil;

	/**
	 * Buffer for OIL generation
	 */
	private UnparseText _oilCode;

	/**
	 * Buffer for C code generation that interfaces with code generated
	 * from oil files.
	 */
	private UnparseText _mainCCode;
	private UnparseText _mainHCode;
	//	private File _generatedCodeDirectory;
	private Hook _startupHook;
	private Hook _shutdownHook;


	private Map<Object, Map<NamedElement, String>> _identifierMappingWithContext = new HashMap<Object, Map<NamedElement, String>>();

	private List<String> _additionalHeaders = new ArrayList<String>();
	//	private Map<DataAccess, String> dataAccessMapping = new LinkedHashMap<DataAccess, String>();

	private static Logger _LOGGER = Logger.getLogger(AadlToOSEKCUnparser.class) ;


	/**
	 * Methode appelée sur chaque noeud implémentant un système.
	 */
	@Override
	public TargetProperties getSystemTargetConfiguration(EList<SystemImplementation> siList,
			IProgressMonitor monitor)
					throws GenerationException
	{
		oil = new OIL();
		oil.setVersion(OIL_VERSION);
		//	  _generatedCodeDirectory = outputDir;
		_oilCode = new UnparseText();
		_mainCCode = new UnparseText();
		_mainHCode = new UnparseText();
		_startupHook = new Hook();
		_shutdownHook = new Hook();

		return new RoutingProperties();
	}

	/**
	 * Generate the OIL/C code for a processor
	 * 
	 * @param processor
	 *            L'AST du processeur.
	 */
	private void genCpu(ProcessSubcomponent ps, ArchTraceSpec traces) {

		Cpu cpu = oil.getCpu();
		Os os = cpu.getOs();
		_mainHCode.addOutputNewline("#include \"kernel.h\"");
		_mainHCode.addOutputNewline("#include \"kernel_id.h\"");
		_mainHCode.addOutputNewline("#include \"aadl_dispatch.h\"");
		_mainHCode.addOutputNewline("#define NULL 0");

		/* Generate code for threads process */
		ProcessImplementation pi = (ProcessImplementation) ps.getComponentImplementation();
		cpu.addAllDataSubcomponent(pi.getOwnedDataSubcomponents());
		//		buildDataAccessMapping(pi, dataAccessMapping) ;

		EList<ThreadSubcomponent> subcomponents = pi.getOwnedThreadSubcomponents();

		_mainCCode.addOutputNewline("mode_id_t current_mode;");
		_mainCCode.addOutputNewline("char * tasks_in_mode[MODES_NB] = {};");

		_mainCCode.addOutputNewline("/*********** Tasks ***********/");
		for (ThreadSubcomponent threadSubcomponent : subcomponents) {
			genTask(ps, threadSubcomponent, COUNTER_NAME, traces);
			genCTask(ps, threadSubcomponent, traces);
		}

		/* Generate code for OS */

		/*
		 * If one data contains Concurrency_Control_Protocol at
		 * Priority_Ceiling STATUS => EXTENDED
		 */
		if (os.getStatus() == Status.STANDARD) {
			ProcessImplementation processImpl = (ProcessImplementation) ps.getComponentImplementation();

			EList<Subcomponent> subcmpts = processImpl.getAllSubcomponents();

			for(Subcomponent s : subcmpts)
			{
				if(s instanceof DataSubcomponent)
				{
					String value = PropertyUtils.getEnumValue(s,
							"Concurrency_Control_Protocol") ;
					if(value != null)
					{
						if(value.equals("Priority_Ceiling"))
						{
							os.setStatus(Status.EXTENDED) ;
							break ;
						}
					}
					else
					{
						String errMsg = "cannot fetch Concurrency_Control_Protocol for \'" +
								s.getName() + '\'' ;
						_LOGGER.error(errMsg) ;
					}
				}
			}
		}

		List<String> modeIdList = getProcessModesIdentifiers(ps);
		cpu.setAppmode(modeIdList);

		cpu.setName(ps.getName());
		genOsConfig(ps);
	}


	List<String> getProcessModesIdentifiers(ProcessSubcomponent ps)
	{
		List<String> result = new ArrayList<String>();
		ProcessImplementation pi = (ProcessImplementation) ps.getSubcomponentType();
		List<Mode> modeList = pi.getOwnedModes();
		if(modeList.isEmpty())
			result.add(MAIN_APP_MODE);
		for(Mode m: modeList)
		{
			if(m.isInitial())
				result.add(m.getName());
		}
		for(Mode m: modeList)
		{
			if(!m.isInitial())
				result.add(m.getName());
		}
		return result;
	}
	/**
	 * Configuration de l'OS
	 * 
	 * @param processSubcomponent
	 */
	private void genOsConfig(ProcessSubcomponent processSubcomponent) {

		/* C Data declaration */
		Os os = oil.getCpu().getOs();
		os.setName("config");
		os.setAppName(processSubcomponent.getName());

		_mainCCode.addOutputNewline("/*********** Data ***********/");

		_mainCCode.addOutputNewline("");
	}

	/**
	 * Génération des counters de l'OIL.
	 */
	private void genCounters(Subcomponent processor) {

		Counter counter = oil.getCpu().getCounter();

		Long maxValue = PropertyUtils.getIntValue(processor, "SystemCounter_MaxAllowedValue") ;
		Long ticksPerBase = PropertyUtils.getIntValue(processor, "SystemCounter_TicksPerBase");
		Long minCycle = PropertyUtils.getIntValue(processor, "SystemCounter_MinCycle");

		if(maxValue == null || ticksPerBase == null || minCycle == null)
		{
			String what = "";
			if(maxValue == null)
			{
				what = "SystemCounter_MaxAllowedValue" ;
				maxValue = -1l ;
			}

			if(ticksPerBase == null)
			{
				what += " SystemCounter_TicksPerBase" ;
				ticksPerBase = -1l ;
			}

			if(minCycle == null)
			{
				what += " SystemCounter_MinCycle" ;
				minCycle = -1l ;
			}

			String errMsg = "cannot fetch " + what + " for \'" + processor.getName() + '\'';
			_LOGGER.error(errMsg);
		}

		counter.setName(processor.getName()+"_"+COUNTER_NAME);
		counter.setMaxAllowedValue(maxValue.intValue());
		counter.setTicksPerBase(ticksPerBase.intValue());
		counter.setMinCycle(minCycle.intValue());
	}

	private void addResource(Task task, FeatureInstance fi)
	{
		if(fi.getCategory()==FeatureCategory.EVENT_DATA_PORT
				|| fi.getCategory()==FeatureCategory.EVENT_PORT)
		{
			task.addResource(fi.getComponentInstance().getName()+"_globalQueue") ;	
		}
		else if(fi.getCategory()==FeatureCategory.DATA_PORT)
		{
			task.addResource(fi.getComponentInstance().getName()+"_globalQueue") ;	
		}
	}

	private void addEvent(Task task, FeatureInstance fi)
	{
		if(fi.getCategory()==FeatureCategory.EVENT_DATA_PORT
				|| fi.getCategory()==FeatureCategory.EVENT_PORT)
		{
			task.addEvent(fi.getComponentInstance().getName()+"_globalQueue") ;
		}
	}

	/**
	 * Génération du code OIL d'un thread.
	 * 
	 * @param ps
	 *            Le processeur conteannt les threads.
	 * @param thread
	 *            L'AST du thread.
	 * @param counterName
	 *            Le nom du compteur sur lequel la tâche se synchronise
	 */
	private void genTask(ProcessSubcomponent ps, 
			ThreadSubcomponent thread, 
			String counterName,
			ArchTraceSpec traces) {

		final Cpu cpu = oil.getCpu();
		final Counter counter = cpu.getCounter();
		final Os os = cpu.getOs();

		/*
		 * If one thread/device contains Initialize_Entrypoint STARTUPHOOK =
		 * true
		 */
		if (os.getStartupHook() == false) {
			if (PropertyUtils.findPropertyAssociation("Initialize_Entrypoint", thread) != null)
				os.setStartupHook(true);
		}

		/*
		 * If one thread/device contains Finalize_Entrypoint SHUTDOWNHOOK = true
		 */
		if (os.getShutdownHook() == false) {
			if (PropertyUtils.findPropertyAssociation("Finalize_Entrypoint", thread) != null)
				os.setShutdownHook(true);
		}

		/* Begin task */

		Task task = new Task();
		Schedule schedule;
		Long stackSize = PropertyUtils.getIntValue(thread, "Stack_Size", AadlProject.B_LITERAL);
		Long priority = PropertyUtils.getIntValue(thread, "Priority");

		if(priority == null)
		{
			String errMsg =  "cannot fetch Priority for \'" + thread.getName() + '\'' ;
			_LOGGER.error(errMsg);
			priority = -1l ;
		}

		if(stackSize == null)
		{
			stackSize = THREAD_STACKSIZE;
			String msg =  "set default stack size for \'" + thread.getName() + '\'' ;
			_LOGGER.warn(msg);
		}

		Boolean scheduler = PropertyUtils.getBooleanValue(ps, "Preemptive_Scheduler") ;
		if(scheduler == null)
		{
			schedule = Schedule.FULL ;
		}
		else
		{
			if(scheduler)
				schedule = Schedule.FULL ;
			else
				schedule = Schedule.NON ;
		}

		task.setName(thread.getName());
		task.setPriority(priority.intValue());
		task.setActivation(THREAD_ACTIVATION);

		task.setSchedule(schedule);
		task.setStacksize(stackSize.intValue());

		ComponentInstance threadInst = (ComponentInstance) traces.
				getTransformationTrace(thread) ;

		boolean treatedInputs=false;
		for(FeatureInstance fi:threadInst.getFeatureInstances())
		{
			if(fi.getDirection()==DirectionType.OUT)
			{
				for(ConnectionInstance cnxInst:fi.getSrcConnectionInstances())
				{
					addResource(task, (FeatureInstance) cnxInst.getDestination());
				}
			}
			else
			{
				if(treatedInputs)
					continue;
				treatedInputs = true;

				addResource(task, fi);
				addEvent(task, fi);
			}
		}

		/* End task */

		/*
		 * Generate alarme associated to periodic tasks
		 */
		{
			String dispatchProtocol = PropertyUtils.getEnumValue(thread, "Dispatch_Protocol");

			Long period = PropertyUtils.getIntValue(thread, "Period", AadlProject.MS_LITERAL);
			if(period == null)
			{
				String errMsg =  "cannot fetch Period for " + thread.getName() ;
				_LOGGER.error(errMsg);
				period = -1l ;
			}
			
			int alarmTime=0;
			try{
				alarmTime = 1;
			} catch(Exception exc)
			{
			}
			List<String> inModeIdList = new ArrayList<String>();
			ProcessImplementation pi = (ProcessImplementation) ps.getSubcomponentType();
			if(pi.getOwnedModes().isEmpty())
				inModeIdList.add(MAIN_APP_MODE);
			else
			{
				if(thread.getAllInModes().isEmpty())
				{
					for(Mode m:pi.getOwnedModes())
					{
						inModeIdList.add(m.getName());
					}
				}
				else
				{
					for(Mode m:thread.getAllInModes())
					{
						inModeIdList.add(m.getName());
					}
				}
			}

			Alarm alarm = new Alarm(counter, task, cpu, inModeIdList);
			alarm.setName("wakeUp" + thread.getName());
			
			alarm.setAutostart(true);
			alarm.setAlarmTime(alarmTime);
			alarm.setCycleTime(period.intValue());
			
			if ("Periodic".equalsIgnoreCase(dispatchProtocol)) {
				alarm.setAction(Action.ACTIVATETASK);
			} 
			else if ("Sporadic".equalsIgnoreCase(dispatchProtocol)) {
				alarm.setAction(Action.ACTIVATETASK);
			}
			else { 
				alarm.setAction(Action.SETEVENT);
				alarm.setEventName(task.getName()+"_globalQueue_OSEK_evt");
			}
			
			task.setAutostart(false);

			cpu.addTask(task);
			cpu.addAlarm(alarm);
		}
	}

	/**
	 * Méthode appelée après le parcours de l'AST via les methodes process.
	 * @param archTraceSpec 
	 * @param process 
	 */
	private void close(ProcessSubcomponent process, ArchTraceSpec archTraceSpec) {

		String msg = "Hooks generation" ;
		_LOGGER.trace(msg);

		_mainCCode.addOutputNewline("void StartupHook(void)");
		_mainCCode.addOutputNewline("{");
		_mainCCode.incrementIndent();
		
		ProcessImplementation processImpl = (ProcessImplementation) 
				process.getComponentImplementation() ;

		EList<ThreadSubcomponent> subcomponents = processImpl.getOwnedThreadSubcomponents();

		_mainCCode.addOutputNewline("/*********** Tasks init ***********/");
		for (ThreadSubcomponent threadSubcomponent : subcomponents) {

			String recoveryEntryPoint = GeneratorUtils.getEntryPoint(threadSubcomponent, "Recover_Entrypoint", "Recover_Entrypoint_Source_Text", _additionalHeaders );
			if(recoveryEntryPoint == null)
				recoveryEntryPoint = "NULL";
			
			String initializeEntryPoint = GeneratorUtils.getEntryPoint(threadSubcomponent, "Initialize_Entrypoint", "Initialize_Entrypoint_Source_Text", _additionalHeaders);
			if(initializeEntryPoint == null)
				initializeEntryPoint = "NULL";
			
			String finalizeEntryPoint = GeneratorUtils.getEntryPoint(threadSubcomponent, "Finalize_Entrypoint", "Finalize_Entrypoint_Source_Text", _additionalHeaders);
			if(finalizeEntryPoint == null)
				finalizeEntryPoint = "NULL";
			
			String activateEntryPoint = GeneratorUtils.getEntryPoint(threadSubcomponent, "Activate_Entrypoint", "Activate_Entrypoint_Source_Text", _additionalHeaders);
			if(activateEntryPoint == null)
				activateEntryPoint = "NULL";
			
			String deactivateEntryPoint = GeneratorUtils.getEntryPoint(threadSubcomponent, "Deactivate_Entrypoint", "Deactivate_Entrypoint_Source_Text", _additionalHeaders);
			if(deactivateEntryPoint == null)
				deactivateEntryPoint = "NULL";

			
			String computeEntryPoint = "NULL";
			computeEntryPoint = threadSubcomponent.getName()+"_Job";

			_mainCCode.addOutputNewline("Init_Config (&"+threadSubcomponent.getName()+"_config, "
					+ recoveryEntryPoint + ","
					+ computeEntryPoint + ","
					+ initializeEntryPoint + ","
					+ finalizeEntryPoint + ","
					+ activateEntryPoint + ","
					+ deactivateEntryPoint + ","
					+subcomponents.indexOf(threadSubcomponent)+");");
			_mainCCode.addOutputNewline(threadSubcomponent.getName()+"_config.target_thread = (void*) &"+threadSubcomponent.getName()+";");
			_mainCCode.addOutputNewline(threadSubcomponent.getName()+"_config.name = \""+threadSubcomponent.getName()+"\";");

			ComponentInstance threadInst = (ComponentInstance) archTraceSpec.
					getTransformationTrace(threadSubcomponent) ;

			for(FeatureInstance fi:threadInst.getFeatureInstances())
			{
				if(fi.getDirection().equals(DirectionType.IN) || fi.getDirection().equals(DirectionType.IN_OUT))
				{
					if(fi.getCategory()==FeatureCategory.EVENT_DATA_PORT
							|| fi.getCategory()==FeatureCategory.EVENT_PORT
							|| fi.getCategory()==FeatureCategory.DATA_PORT)
					{
						_mainCCode.addOutputNewline(threadSubcomponent.getName()+"_config.global_q->event->rez = (ResourceType*)&" + fi.getComponentInstance().getName()+"_globalQueue_OSEK_rez" + ";");
						_mainCCode.addOutputNewline(threadSubcomponent.getName()+"_config.global_q->event->event = (EventMaskType*)&" + fi.getComponentInstance().getName()+"_globalQueue_OSEK_evt" + ";");
						break;
					}
				}
			}
			
			String dispatchProtocol = PropertyUtils.getEnumValue(threadSubcomponent, "Dispatch_Protocol");
			if ("Periodic".equalsIgnoreCase(dispatchProtocol))
				continue;
				
			String dispatchSpecificConfigType = "";
			if ("Hybrid".equalsIgnoreCase(dispatchProtocol))
			{
				dispatchSpecificConfigType = "target_hybrid_thread_config_t";
			}
			else if ("Timed".equalsIgnoreCase(dispatchProtocol))
			{
				dispatchSpecificConfigType = "target_timed_thread_config_t";
			}
			else if ("Sporadic".equalsIgnoreCase(dispatchProtocol))
			{
				dispatchSpecificConfigType = "target_sporadic_thread_config_t";
			}
			else if ("Aperiodic".equalsIgnoreCase(dispatchProtocol))
			{
				dispatchSpecificConfigType = "target_apediodic_thread_config_t";
				
			}
			if(!dispatchSpecificConfigType.isEmpty())
			{
				_mainCCode.addOutputNewline(dispatchSpecificConfigType+"* dispatch_thread_config = ("+dispatchSpecificConfigType+"*)"+ threadSubcomponent.getName()+"_config.thread;");
				_mainCCode.addOutputNewline("dispatch_thread_config->alrm = "+"wakeUp" +threadSubcomponent.getName()+"_default_mode;");
			}

		}
		
		
		for (Subprogram subprogram : _startupHook.getCalls()) {
			_mainCCode.addOutput(subprogram.getName());
			_mainCCode.addOutput("(");

			for(String parameter: subprogram.getParameters())
				_mainCCode.addOutput(parameter);

			_mainCCode.addOutputNewline(");");
		}
		_mainCCode.decrementIndent();
		_mainCCode.addOutputNewline("}");
		_mainCCode.addOutputNewline("");

		_mainCCode.addOutputNewline("void ShutdownHook(StatusType ercd)");
		_mainCCode.addOutputNewline("{");
		_mainCCode.incrementIndent();
		for (Subprogram subprogram : _shutdownHook.getCalls()) {
			_mainCCode.addOutput(subprogram.getName());
			_mainCCode.addOutput("(");

			for(String parameter: subprogram.getParameters())
				_mainCCode.addOutput(parameter);

			_mainCCode.addOutputNewline(");");
		}
		_mainCCode.decrementIndent();
		_mainCCode.addOutputNewline("}");
		_mainCCode.addOutputNewline("");

		_mainHCode.addOutputNewline("DeclareCounter("+oil.getCpu().getCounter().getName()+");");
		_mainCCode.addOutputNewline("");


		_mainCCode.addOutputNewline("unsigned long osek_absolute_time_ms=0;");

		/* LEJOS OSEK hook to be invoked from an ISR in category 2 */
		_mainCCode.addOutputNewline("void user_1ms_isr_type2(void)");
		_mainCCode.addOutputNewline("{");
		_mainCCode.incrementIndent();
		_mainCCode.addOutputNewline("StatusType ercd;");


		_mainCCode.addOutputNewline("ercd = SignalCounter("+oil.getCpu().getCounter().getName()+"); /* Increment OSEK Alarm Counter */"); 
		_mainCCode.addOutputNewline("if(ercd != E_OK)");
		_mainCCode.addOutputNewline("{");
		_mainCCode.incrementIndent();
		_mainCCode.addOutputNewline("ShutdownOS(ercd);");
		_mainCCode.decrementIndent();
		_mainCCode.addOutputNewline("}");
		_mainCCode.addOutputNewline("osek_absolute_time_ms++;");
		_mainCCode.decrementIndent();
		_mainCCode.addOutputNewline("}");
		_mainCCode.decrementIndent();

		// Generate OIL
		oil.generateOil(_oilCode);

		_mainHCode.addOutputNewline("#include \"gtypes.h\"");
		_mainHCode.addOutputNewline("#include \"aadl_multiarch.h\"");
		_mainHCode.addOutputNewline("#endif");
	}

	private void genCTask(ProcessSubcomponent ps, ThreadSubcomponent thread, ArchTraceSpec traces) {
		String threadName = thread.getName();
		threadName = GenerationUtilsC.getGenerationCIdentifier(thread.getContainingClassifier(), thread, _identifierMappingWithContext);
		ProcessImplementation pi = (ProcessImplementation) ps.getComponentImplementation();    

		EList<ThreadSubcomponent> subcomponents = pi.getOwnedThreadSubcomponents();

		_mainCCode.addOutputNewline("/*** Task " + threadName + " ***/");
		_mainCCode.addOutputNewline("extern void " + threadName + "_Job(void);");
		_mainCCode.addOutputNewline("extern thread_config_t "+ thread.getName() +"_config;");
		_mainCCode.addOutputNewline("");
		
		ComponentInstance threadInst = (ComponentInstance) traces.
				getTransformationTrace(thread) ;

		for(FeatureInstance fi:threadInst.getFeatureInstances())
		{
			
			if(fi.getDirection().equals(DirectionType.IN) || fi.getDirection().equals(DirectionType.IN_OUT))
			{
				if(fi.getCategory()==FeatureCategory.EVENT_DATA_PORT
						|| fi.getCategory()==FeatureCategory.EVENT_PORT
						|| fi.getCategory()==FeatureCategory.DATA_PORT)
				{
					_mainHCode.addOutputNewline("DeclareResource(" + fi.getComponentInstance().getName()+"_globalQueue_OSEK_rez" + ");");
					_mainHCode.addOutputNewline("DeclareEvent(" + fi.getComponentInstance().getName()+"_globalQueue_OSEK_evt" + ");");
					break;
				}
			}
		}
		
		String dispatchProtocol = PropertyUtils.getEnumValue(thread, "Dispatch_Protocol");
		if (false=="Periodic".equalsIgnoreCase(dispatchProtocol))
			_mainHCode.addOutputNewline("DeclareAlarm(wakeUp"+thread.getName()+"_default_mode);");
		
		_mainHCode.addOutputNewline("DeclareTask(" + thread.getName() + ");");
		_mainCCode.addOutputNewline("");

		_mainCCode.addOutputNewline("TASK(" + thread.getName() + ")");
		_mainCCode.addOutputNewline("{");
		_mainCCode.incrementIndent();

		_mainCCode.addOutputNewline("get_time_multiarch(&("+thread.getName() +"_config.dispatch_time));");

		

		_mainCCode.addOutputNewline(threadName + "_Job();");

		
		_mainCCode.decrementIndent();
		_mainCCode.addOutputNewline("}");
		_mainCCode.addOutputNewline("");
	}

	// XXX: Just implemented to respect the interface definition, but they are
	// not used in case of OSEK
	@Override
	public void generateProcessorTargetConfiguration(Subcomponent processor,
			URI outputDir,
			IProgressMonitor monitor)
					throws GenerationException
	{
		genCounters(processor);
	}

	@Override
	public void generateProcessTargetConfiguration(ProcessSubcomponent process,
			URI outputDir,
			IProgressMonitor monitor)
	{

		// Generate main.h
		genMainHeader() ;
		
		// Generate main.c
		genMainImpl(process, getCurrentModelTransformationTrace()) ;

		close(process, getCurrentModelTransformationTrace());
		
		for(String h: _additionalHeaders)
			_mainHCode.addOutputNewline("#include \""+h+"\"");
		
		try
		{
			FileUtils.saveFile(outputDir, "main.h",
					_mainHCode.getParseOutput()) ;

			FileUtils.saveFile(outputDir, "main.c",
					_mainCCode.getParseOutput()) ;

			FileUtils.saveFile(outputDir, process.getName() + ".oil",
					_oilCode.getParseOutput()) ;
		}
		catch(IOException e)
		{
			String errMsg = "cannot save the generated files" ;
			_LOGGER.fatal(errMsg, e) ;
			throw new RuntimeException(errMsg, e) ;
		}
	}

	private void genMainHeader()
	{
		String guard = GenerationUtilsC.generateHeaderInclusionGuard("main.h") ;
		_mainHCode.addOutputNewline(guard) ;
	}

	private void genMainImpl(ProcessSubcomponent process, ArchTraceSpec traces)
	{
		_mainCCode.addOutputNewline("#include \"main.h\"");
		genCpu(process, traces);

		StringBuilder sb = new StringBuilder(process.getQualifiedName());
		ProcessProperties pp = new ProcessProperties(sb.substring(0, sb.lastIndexOf("::")+2)) ;

		ProcessImplementation processImpl = (ProcessImplementation) 
				process.getComponentImplementation() ;

		this.findCommunicationMechanism(processImpl, pp, traces);
	}

	@Override
	protected String getFeatureLocalIdentifier(FeatureInstance fi)
	{
		return GenerationUtilsC.getGenerationCIdentifier(fi.getComponentInstance(), fi, _identifierMappingWithContext);
	}

	@Override
	protected String getGenerationIdentifier(String prefix)
	{
		return GenerationUtilsC.getGenerationCIdentifier(prefix);
	}

}
