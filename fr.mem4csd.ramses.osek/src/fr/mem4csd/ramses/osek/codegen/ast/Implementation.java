/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.osek.codegen.ast;


public class Implementation {

	//private String name;
	private ImplementationTask task;
	private ImplementationIsr isr;

	public Implementation() {
		task = new ImplementationTask();
		isr = new ImplementationIsr();
	}
	
	public ImplementationTask getTask() {
		return task;
	}
	
	public ImplementationIsr getIsr() {
		return isr;
	}
//	
//	public void setName(String name) {
//		this.name = name;
//	}
}