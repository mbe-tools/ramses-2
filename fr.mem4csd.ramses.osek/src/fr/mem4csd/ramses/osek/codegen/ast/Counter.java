/**
 * RAMSES 2.0
 * 
 * Copyright © 2014-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.osek.codegen.ast;

import org.osate.aadl2.modelsupport.UnparseText;

public class Counter {

	private String name;
	private String source;
	private int maxAllowedValue;
	private int ticksPerBase;
	private int minCycle;

	public Counter() {
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setMaxAllowedValue(int maxAllowedValue) {
		this.maxAllowedValue = maxAllowedValue;
	}

	public void setTicksPerBase(int ticksPerBase) {
		this.ticksPerBase = ticksPerBase;
	}

	public void setMinCycle(int minCycle) {
		this.minCycle = minCycle;
	}

	public void generateOil(UnparseText code) {
		code.addOutputNewline("COUNTER " + name);
		code.addOutputNewline("{");
		code.incrementIndent();
		code.addOutputNewline("MAXALLOWEDVALUE = " + maxAllowedValue + ";");
		code.addOutputNewline("TICKSPERBASE = " + ticksPerBase + ";");
		code.addOutputNewline("MINCYCLE = " + minCycle + ";");
		code.decrementIndent();
		code.addOutputNewline("};");
		code.addOutputNewline("");
	}

	public String getName() {
		return name;
	}
}