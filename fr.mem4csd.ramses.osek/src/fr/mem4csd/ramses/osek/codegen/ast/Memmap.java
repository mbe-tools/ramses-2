/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.osek.codegen.ast;


public class Memmap {

	private String compiler;
	private String linker;
	private String assembler;
	private boolean memoryProtection;

	public void setCompiler(String compiler) {
		this.compiler = compiler;
	}

	public void setLinker(String linker) {
		this.linker = linker;
	}

	public void setAssembler(String assembler) {
		this.assembler = assembler;
	}

	public void setMemoryProtection(boolean memoryProtection) {
		this.memoryProtection = memoryProtection;
	}
}