<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_KcMFkEgDEeqsT5NnXFzohg" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_bgFuUAVnEeuDOaqjzi9xMA" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/load_transformation_resources_core.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_8tfcwKTPEeq1YeAByobchg" name="${NameLoadValidationTransformationModules}" modelSlot="OsekProperties" modelURI="${scheme}${ramses_osek_plugin}ramses/osek/transformations/atl/validation/OsekProperties.emftvm" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_hHh38AVnEeuDOaqjzi9xMA" name="${NameLoadRefinementTransformationModules}" modelSlot="ExpandThreadsDispatchProtocolOsek" modelURI="${ramses_osek_transformation_refinement_path}ExpandThreadsDispatchProtocol.emftvm" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_lsiUMAVnEeuDOaqjzi9xMA" name="${NameLoadRefinementTransformationModules}" modelSlot="ExpandThreadsFeaturesOsek" modelURI="${ramses_osek_transformation_refinement_path}ExpandThreadsFeatures.emftvm" modelElementIndex="0"/>
  <propertiesFiles xmi:id="_GSoLsE5EEeqIOpzmJvnc-w" fileURI="default_osek.properties"/>
  <propertiesFiles xmi:id="_4ytpsNx8Eeq6KstHiQyO6A" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
