<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_1bNJoEgDEeqsT5NnXFzohg" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_ocFiUAV1EeuDOaqjzi9xMA" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/load_ramses_aadl_resources_core.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_ABtMMOXpEem9hPkQPXJd7Q" name="${NameRamsesAadlResourcesReader}" modelSlot="osek_properties" modelURI="${scheme}${ramses_osek_plugin}ramses/osek/propertysets/osek_properties.aadl" modelElementIndex="0"/>
  <propertiesFiles xmi:id="_574t8E5CEeqIOpzmJvnc-w" fileURI="default_osek.properties"/>
  <propertiesFiles xmi:id="_V6gxMNx7Eeq6KstHiQyO6A" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
