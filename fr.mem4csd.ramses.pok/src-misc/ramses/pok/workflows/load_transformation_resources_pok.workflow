<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_1bNJoEgDEeqsT5NnXFzohg" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="__gTxYEgDEeqsT5NnXFzohg" name="load_refinement_core" workflowURI="${scheme}${ramses_arinc653_plugin}ramses/arinc653/workflows/load_transformation_resources_arinc653.workflow"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_FbMUEKTNEeqi7MtCpWPvXA" name="load_validation_core" description="" workflowURI="${scheme}${ramses_arinc653_plugin}ramses/arinc653/workflows/load_validation_arinc653.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_qGKgwEjDEeqafNRi83mxbA" name="${NameLoadRefinementTransformationModules}" modelSlot="Refinement${target}" modelURI="${ramses_pok_transformation_refinement_path}PokTarget.emftvm" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_KvRAUP8jEeqjUK5cPU4ZFw" name="${NameRamsesAadlResourcesReader}" modelSlot="PokProperties" modelURI="${scheme}${ramses_pok_plugin}ramses/pok/transformations/atl/validation/pok/PokProperties.emftvm" modelElementIndex="0"/>
  <properties xmi:id="_EVPtQE4pEeqIOpzmJvnc-w" name="ramses_pok_transformation_path" defaultValue="${scheme}${ramses_pok_plugin}ramses/pok/transformations/atl/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_VLB-oE4pEeqIOpzmJvnc-w" name="ramses_pok_transformation_refinement_path" defaultValue="${ramses_pok_transformation_path}refinement/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_C7pdEE4pEeqIOpzmJvnc-w" fileURI="default_pok.properties"/>
  <propertiesFiles xmi:id="_SGHcMNr_Eeq05enNFbiy6w" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
