<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowramsespok="https://mem4csd.telecom-paris.fr/ramses/workflowramsespok" xmi:id="_YKYe0D6KEeq5xtZ5_imDzA" name="workflow">
  <components xsi:type="workflow.components:ModelReader" xmi:id="_p1Di0ATlEeuDOaqjzi9xMA" name="${NameRefinedResourceReader}" modelSlot="refinedAadlModelPok" modelURI="${refined_aadl_file}" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_wLYywATlEeuDOaqjzi9xMA" name="${NameRefinementTraceReader}" modelSlot="refinedTraceModelPok" modelURI="${refined_trace_file}" modelElementIndex="0"/>
  <components xsi:type="workflowramsespok:CodegenPok" xmi:id="_Yfe_sE4uEeqIOpzmJvnc-w" name="${NameCodeGeneration}" aadlModelSlot="refinedAadlModelPok" traceModelSlot="refinedTraceModelPok" outputDirectory="${output_dir}" targetInstallDir="${target_install_dir}" includeDir="${include_dir}" coreRuntimeDir="${core_runtime_dir}" targetRuntimeDir="${target_runtime_dir}"/>
  <properties xmi:id="_PK8IQEGjEeqBM5afCympPg" name="include_dir" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_mzRWYEGhEeqLOKt47Neqmg" name="output_dir" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_hoh6oD6SEeq5xtZ5_imDzA" name="refined_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_ah57kD6lEeqUz4Y2Tk3Zrg" name="refined_trace_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_dlGTQF7EEeqTbYoc6DiMAg" name="target_install_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_PJvckNr9Eeq05enNFbiy6w" fileURI="default_pok.properties"/>
  <propertiesFiles xmi:id="_c9L24E4qEeqIOpzmJvnc-w" fileURI="platform:/plugin/fr.mem4csd.ramses.arinc653/ramses/arinc653/workflows/default_arinc653.properties" resolveURI="false"/>
  <propertiesFiles xmi:id="_Kbu5EATxEeuDOaqjzi9xMA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
