<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowemftvm="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowemftvm" xmlns:workflowramses="https://mem4csd.telecom-paris.fr/ramses/workflowramses" xmi:id="_PAkpAMixEei-D6bRtwoJ1Q" name="workflow" description="EMFTVM operations needed by posix">
  <components xsi:type="workflow.components:StringToBoolEvaluator" xmi:id="_8qv_0KzoEeqYtKBdcZJ7ng" name="exposeRuntimeSharedResources" modelSlot="exposeRuntimeSharedResources" stringValue="${expose_runtime_shared_resources}"/>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_6yF1EKzoEeqYtKBdcZJ7ng" name="exposeRuntimeSharedResources" conditionSlot="exposeRuntimeSharedResources">
    <onTrue xmi:id="_HDRS0KzpEeqYtKBdcZJ7ng" name="onTrue">
      <components xsi:type="workflowemftvm:EmftVmTransformer" xmi:id="_K0PqEKzpEeqYtKBdcZJ7ng" name="${NameExecuteModelRefinement} for POK" registerDependencyModels="true" discardExtraRootElements="true">
        <rulesModelSlot>IOHelpers</rulesModelSlot>
        <rulesModelSlot>AADLCopyHelpers</rulesModelSlot>
        <rulesModelSlot>AADLICopyHelpers</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexServices</rulesModelSlot>
        <rulesModelSlot>PropertiesTools</rulesModelSlot>
        <rulesModelSlot>PackagesTools</rulesModelSlot>
        <rulesModelSlot>FeaturesTools</rulesModelSlot>
        <rulesModelSlot>Uninstanciate</rulesModelSlot>
        <rulesModelSlot>Features</rulesModelSlot>
        <rulesModelSlot>Implementations</rulesModelSlot>
        <rulesModelSlot>Properties</rulesModelSlot>
        <rulesModelSlot>Modes</rulesModelSlot>
        <rulesModelSlot>Flows</rulesModelSlot>
        <rulesModelSlot>Types</rulesModelSlot>
        <rulesModelSlot>Connections</rulesModelSlot>
        <rulesModelSlot>Services</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexTools</rulesModelSlot>
        <rulesModelSlot>BehaviorActionBlock</rulesModelSlot>
		<rulesModelSlot>BehaviorCondition</rulesModelSlot>
		<rulesModelSlot>BehaviorSpecification</rulesModelSlot>
		<rulesModelSlot>BehaviorTime</rulesModelSlot>
		<rulesModelSlot>ElementHolders</rulesModelSlot>
        <rulesModelSlot>ACGServices</rulesModelSlot>
        <rulesModelSlot>UninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>DataUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>SubprogramsUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>ThreadsUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>ProcessesUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>SubprogramCallsCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>PortsCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>ExpandProcessesPorts</rulesModelSlot>
        <rulesModelSlot>DispatchCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsDispatchProtocol</rulesModelSlot>
        <rulesModelSlot>SharedRules</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsPorts</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsPortsWithSharedResources</rulesModelSlot>
        <rulesModelSlot>SharedRules_No_Mutex</rulesModelSlot>
        <rulesModelSlot>EventDataPorts_LowET</rulesModelSlot>
        <rulesModelSlot>LanguageSpecificitiesC</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsDispatchProtocolTarget</rulesModelSlot>
        <rulesModelSlot>HealthMonitoring</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsPortsTarget</rulesModelSlot>
        <rulesModelSlot>Refinement${target}</rulesModelSlot>
        <inputModels xmi:id="_K0PqEazpEeqYtKBdcZJ7ng" modelName="IN" modelSlot="sourceAadlInstance" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqE6zpEeqYtKBdcZJ7ng" modelName="TOOLS" modelSlot="atlHelper" metamodelName="ATLHELPER" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/helpers"/>
        <inputModels xmi:id="_K0PqFKzpEeqYtKBdcZJ7ng" modelName="BASE_TYPES" modelSlot="Base_Types" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba"/>
        <inputModels xmi:id="_K0PqFazpEeqYtKBdcZJ7ng" modelName="ARINC653_RUNTIME" modelSlot="ARINC653_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux9qyREeqXL6k3aNztMA" modelName="PERIODICDELAYEDMUTEX_RUNTIME" modelSlot="PeriodicDelayedMutex_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux9qyREeqXL6k3aNztMA" modelName="PERIODICDELAYED_RUNTIME" modelSlot="PeriodicDelayed_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqFqzpEeqYtKBdcZJ7ng" modelName="AADL_RUNTIME" modelSlot="aadl_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqF6zpEeqYtKBdcZJ7ng" modelName="RAMSES_PROCESSORS" modelSlot="RAMSES_processors" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqGKzpEeqYtKBdcZJ7ng" modelName="RAMSES_BUSES" modelSlot="RAMSES_buses" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqGazpEeqYtKBdcZJ7ng" modelName="AADL_PROJECT" modelSlot="AADL_Project" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqGqzpEeqYtKBdcZJ7ng" modelName="COMMUNICATION_PROPERTIES" modelSlot="Communication_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqG6zpEeqYtKBdcZJ7ng" modelName="DATA_MODEL" modelSlot="Data_Model" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqHKzpEeqYtKBdcZJ7ng" modelName="DEPLOYMENT_PROPERTIES" modelSlot="Deployment_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqHazpEeqYtKBdcZJ7ng" modelName="MEMORY_PROPERTIES" modelSlot="Memory_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqHqzpEeqYtKBdcZJ7ng" modelName="MODELING_PROPERTIES" modelSlot="Modeling_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqH6zpEeqYtKBdcZJ7ng" modelName="PROGRAMMING_PROPERTIES" modelSlot="Programming_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqIKzpEeqYtKBdcZJ7ng" modelName="THREAD_PROPERTIES" modelSlot="Thread_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqIazpEeqYtKBdcZJ7ng" modelName="TIMING_PROPERTIES" modelSlot="Timing_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqIqzpEeqYtKBdcZJ7ng" modelName="ARINC653" modelSlot="ARINC653" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqI6zpEeqYtKBdcZJ7ng" modelName="CODE_GENERATION_PROPERTIES" modelSlot="Code_Generation_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqJKzpEeqYtKBdcZJ7ng" modelName="POK" modelSlot="pok_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqJazpEeqYtKBdcZJ7ng" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_K0PqJ6zpEeqYtKBdcZJ7ng" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <outputModels xmi:id="_K0PqKKzpEeqYtKBdcZJ7ng" modelName="OUT_TRACE" modelSlot="refinedTraceModelPok" metamodelName="ARCH_TRACE" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/arch_trace"/>
        <inputOutputModels xmi:id="_K0PqKazpEeqYtKBdcZJ7ng" modelName="OUT" modelSlot="refinedAadlModelPok" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba" extension="aadl"/>
      </components>
    </onTrue>
    <onFalse xmi:id="_IkspUKzpEeqYtKBdcZJ7ng" name="onFalse">
      <components xsi:type="workflowemftvm:EmftVmTransformer" xmi:id="_EmUF9fGhEemHmY6XjiEv8w" name="${NameExecuteModelRefinement} for POK" registerDependencyModels="true" discardExtraRootElements="true">
        <rulesModelSlot>IOHelpers</rulesModelSlot>
        <rulesModelSlot>AADLCopyHelpers</rulesModelSlot>
        <rulesModelSlot>AADLICopyHelpers</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexServices</rulesModelSlot>
        <rulesModelSlot>PropertiesTools</rulesModelSlot>
        <rulesModelSlot>PackagesTools</rulesModelSlot>
        <rulesModelSlot>FeaturesTools</rulesModelSlot>
        <rulesModelSlot>Uninstanciate</rulesModelSlot>
        <rulesModelSlot>Features</rulesModelSlot>
        <rulesModelSlot>Implementations</rulesModelSlot>
        <rulesModelSlot>Properties</rulesModelSlot>
        <rulesModelSlot>Modes</rulesModelSlot>
        <rulesModelSlot>Flows</rulesModelSlot>
        <rulesModelSlot>Types</rulesModelSlot>
        <rulesModelSlot>Connections</rulesModelSlot>
        <rulesModelSlot>Services</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexTools</rulesModelSlot>
        <rulesModelSlot>BehaviorActionBlock</rulesModelSlot>
		<rulesModelSlot>BehaviorCondition</rulesModelSlot>
		<rulesModelSlot>BehaviorSpecification</rulesModelSlot>
		<rulesModelSlot>BehaviorTime</rulesModelSlot>
		<rulesModelSlot>ElementHolders</rulesModelSlot>
        <rulesModelSlot>ACGServices</rulesModelSlot>
        <rulesModelSlot>UninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>DataUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>SubprogramsUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>ThreadsUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>ProcessesUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>SubprogramCallsCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>PortsCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>ExpandProcessesPorts</rulesModelSlot>
        <rulesModelSlot>DispatchCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>BehaviorActionBlock</rulesModelSlot>
		<rulesModelSlot>BehaviorCondition</rulesModelSlot>
		<rulesModelSlot>BehaviorSpecification</rulesModelSlot>
		<rulesModelSlot>BehaviorTime</rulesModelSlot>
		<rulesModelSlot>ElementHolders</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsDispatchProtocol</rulesModelSlot>
        <rulesModelSlot>SharedRules</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsPorts</rulesModelSlot>
        <rulesModelSlot>SharedRules_No_Mutex</rulesModelSlot>
        <rulesModelSlot>EventDataPorts_LowET</rulesModelSlot>
        <rulesModelSlot>LanguageSpecificitiesC</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsDispatchProtocolTarget</rulesModelSlot>
        <rulesModelSlot>HealthMonitoring</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsPortsTarget</rulesModelSlot>
        <rulesModelSlot>Refinement${target}</rulesModelSlot>
        <inputModels xmi:id="_EmUF9vGhEemHmY6XjiEv8w" modelName="IN" modelSlot="sourceAadlInstance" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUF-fGhEemHmY6XjiEv8w" modelName="TOOLS" modelSlot="atlHelper" metamodelName="ATLHELPER" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/helpers"/>
        <inputModels xmi:id="_EmUF-vGhEemHmY6XjiEv8w" modelName="BASE_TYPES" modelSlot="Base_Types" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba"/>
        <inputModels xmi:id="_EmUF_fGhEemHmY6XjiEv8w" modelName="ARINC653_RUNTIME" modelSlot="ARINC653_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux9qyREeqXL6k3aNztMA" modelName="PERIODICDELAYEDMUTEX_RUNTIME" modelSlot="PeriodicDelayedMutex_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux9qyREeqXL6k3aNztMA" modelName="PERIODICDELAYED_RUNTIME" modelSlot="PeriodicDelayed_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGAPGhEemHmY6XjiEv8w" modelName="AADL_RUNTIME" modelSlot="aadl_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGBPGhEemHmY6XjiEv8w" modelName="RAMSES_PROCESSORS" modelSlot="RAMSES_processors" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGBfGhEemHmY6XjiEv8w" modelName="RAMSES_BUSES" modelSlot="RAMSES_buses" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGBvGhEemHmY6XjiEv8w" modelName="AADL_PROJECT" modelSlot="AADL_Project" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGB_GhEemHmY6XjiEv8w" modelName="COMMUNICATION_PROPERTIES" modelSlot="Communication_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGCPGhEemHmY6XjiEv8w" modelName="DATA_MODEL" modelSlot="Data_Model" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGCfGhEemHmY6XjiEv8w" modelName="DEPLOYMENT_PROPERTIES" modelSlot="Deployment_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGCvGhEemHmY6XjiEv8w" modelName="MEMORY_PROPERTIES" modelSlot="Memory_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGC_GhEemHmY6XjiEv8w" modelName="MODELING_PROPERTIES" modelSlot="Modeling_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGDPGhEemHmY6XjiEv8w" modelName="PROGRAMMING_PROPERTIES" modelSlot="Programming_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGDfGhEemHmY6XjiEv8w" modelName="THREAD_PROPERTIES" modelSlot="Thread_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGDvGhEemHmY6XjiEv8w" modelName="TIMING_PROPERTIES" modelSlot="Timing_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGD_GhEemHmY6XjiEv8w" modelName="ARINC653" modelSlot="ARINC653" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGEPGhEemHmY6XjiEv8w" modelName="CODE_GENERATION_PROPERTIES" modelSlot="Code_Generation_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGEfGhEemHmY6XjiEv8w" modelName="POK" modelSlot="pok_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGE_GhEemHmY6XjiEv8w" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGFvGhEemHmY6XjiEv8w" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <outputModels xmi:id="_VmBpwPmAEemTadmi341e_w" modelName="OUT_TRACE" modelSlot="refinedTraceModelPok" metamodelName="ARCH_TRACE" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/arch_trace"/>
        <inputOutputModels xmi:id="_e6V98PGiEemHmY6XjiEv8w" modelName="OUT" modelSlot="refinedAadlModelPok" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba" extension="aadl"/>
      </components>
    </onFalse>
  </components>
  <components xsi:type="workflowramses:AadlWriter" xmi:id="_hizN8PGiEemHmY6XjiEv8w" name="modelWriter" modelSlot="refinedAadlModelPok" modelURI="${refined_aadl_file}" cloneModel="false" unloadAfter="true" resolveURI="false"/>
  <components xsi:type="workflowramses:TraceWriter" xmi:id="_M0JUEF3_EeqYp8ezKVdS_w" name="traceWriter" modelSlot="refinedTraceModelPok" modelURI="${refined_trace_file}" cloneModel="false" unloadAfter="true" resolveURI="false"/>
  <properties xmi:id="_FJh8QF7EEeqTbYoc6DiMAg" name="refined_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Fa7OQF7EEeqTbYoc6DiMAg" name="refined_trace_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_zdN5QKzoEeqYtKBdcZJ7ng" name="expose_runtime_shared_resources" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_9jzccNsDEeq05enNFbiy6w" fileURI="default_pok.properties"/>
  <propertiesFiles xmi:id="_JO-40E4oEeqIOpzmJvnc-w" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
