/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.pok.workflowramsespok.impl;

import org.eclipse.emf.ecore.EClass;

import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator;
import fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl;
import fr.mem4csd.ramses.pok.codegen.c.AadlToPokCUnparser;
import fr.mem4csd.ramses.pok.codegen.makefile.AadlToPokMakefileUnparser;
import fr.mem4csd.ramses.pok.workflowramsespok.CodegenPok;
import fr.mem4csd.ramses.pok.workflowramsespok.WorkflowramsespokPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Codegen Pok</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CodegenPokImpl extends CodegenImpl implements CodegenPok {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodegenPokImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsespokPackage.Literals.CODEGEN_POK;
	}

	/**
	 * 
	 * @generated not
	 */
	@Override
	public AadlToTargetConfigurationGenerator getAadlToTargetConfiguration() {
		if(aadlToTargetConfiguration==null)
			setAadlToTargetConfiguration(new AadlToPokCUnparser());
		return aadlToTargetConfiguration;
	}
	
	/**
	 * 
	 * @generated not
	 */
	@Override
	public AadlToTargetBuildGenerator getAadlToTargetBuild() {
		if(aadlToTargetBuild==null)
			setAadlToTargetBuild(new AadlToPokMakefileUnparser());
		return aadlToTargetBuild;
	}
	
} //CodegenPokImpl
