/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.pok.workflowramsespok.impl;

import de.mdelab.workflow.WorkflowPackage;

import de.mdelab.workflow.components.ComponentsPackage;
import de.mdelab.workflow.helpers.HelpersPackage;
import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import fr.mem4csd.ramses.pok.workflowramsespok.CodegenPok;
import fr.mem4csd.ramses.pok.workflowramsespok.WorkflowramsespokFactory;
import fr.mem4csd.ramses.pok.workflowramsespok.WorkflowramsespokPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowramsespokPackageImpl extends EPackageImpl implements WorkflowramsespokPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codegenPokEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.ramses.pok.workflowramsespok.WorkflowramsespokPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkflowramsespokPackageImpl() {
		super(eNS_URI, WorkflowramsespokFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WorkflowramsespokPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkflowramsespokPackage init() {
		if (isInited) return (WorkflowramsespokPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowramsespokPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorkflowramsespokPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WorkflowramsespokPackageImpl theWorkflowramsespokPackage = registeredWorkflowramsespokPackage instanceof WorkflowramsespokPackageImpl ? (WorkflowramsespokPackageImpl)registeredWorkflowramsespokPackage : new WorkflowramsespokPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		WorkflowPackage.eINSTANCE.eClass();
		WorkflowramsesPackage.eINSTANCE.eClass();
		ComponentsPackage.eINSTANCE.eClass();
		HelpersPackage.eINSTANCE.eClass();
		Arch_tracePackage.eINSTANCE.eClass();
		Aadl2Package.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkflowramsespokPackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowramsespokPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorkflowramsespokPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkflowramsespokPackage.eNS_URI, theWorkflowramsespokPackage);
		return theWorkflowramsespokPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCodegenPok() {
		return codegenPokEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WorkflowramsespokFactory getWorkflowramsespokFactory() {
		return (WorkflowramsespokFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		codegenPokEClass = createEClass(CODEGEN_POK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		WorkflowramsesPackage theWorkflowramsesPackage = (WorkflowramsesPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowramsesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		codegenPokEClass.getESuperTypes().add(theWorkflowramsesPackage.getCodegen());

		// Initialize classes, features, and operations; add parameters
		initEClass(codegenPokEClass, CodegenPok.class, "CodegenPok", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //WorkflowramsespokPackageImpl
