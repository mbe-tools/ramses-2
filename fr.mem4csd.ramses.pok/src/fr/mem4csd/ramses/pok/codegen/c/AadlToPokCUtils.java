/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.pok.codegen.c;

import org.osate.aadl2.instance.ComponentInstance ;
import org.osate.aadl2.instance.FeatureInstance ;

import fr.mem4csd.ramses.arinc653.utils.AadlToARINC653Utils;
import fr.mem4csd.ramses.core.codegen.RoutingProperties.VirtualPort;
import fr.mem4csd.ramses.core.codegen.utils.GenerationUtilsC;

public class AadlToPokCUtils extends AadlToARINC653Utils
{
  private static String getProcessPortName(FeatureInstance fi)
  {
	  return fi.getComponentInstance().getName();
  }
  
  public static String getFeatureLocalIdentifier(FeatureInstance fi)
  {
    return GenerationUtilsC.getGenerationCIdentifier(getProcessPortName(fi)+"_"+getFeatureInstanceName(fi));
  }
  
  private static String getFeatureInstanceName(FeatureInstance fi) {
	if(fi.eContainer() instanceof FeatureInstance)
	{
		FeatureInstance fg = (FeatureInstance) fi.eContainer();
		return getFeatureInstanceName(fg)+"_"+fi.getName();
	}
	else
		return fi.getName();
  }

  public static String getFeatureGlobalIdentifier(FeatureInstance fi)
  {
    return GenerationUtilsC.getGenerationCIdentifier(getProcessPortName(fi)+"_"+getFeatureInstanceName(fi)+"_global");
  }
  
  public static String getFeatureLocalIdentifier(VirtualPort vp)
  {
	String destName = "";
	if(vp.getDestinationProcess()!=null)
		destName = AadlToPokCUtils.getComponentInstanceIdentifier(vp.getDestinationProcess())+"_";
	return GenerationUtilsC.getGenerationCIdentifier(AadlToPokCUtils.getComponentInstanceIdentifier(vp.getProcess())+"_"+destName+
			  AadlToPokCUtils.getFeatureLocalIdentifier(vp.getBusAccess()));
  }
  
  public static String getFeatureGlobalIdentifier(VirtualPort vp)
  {
	  String destName = "";
	  if(vp.getDestinationProcess()!=null)
		  destName = AadlToPokCUtils.getComponentInstanceIdentifier(vp.getDestinationProcess())+"_";
	  return GenerationUtilsC.getGenerationCIdentifier(AadlToPokCUtils.getComponentInstanceIdentifier(vp.getProcess())+"_"+destName+
			  AadlToPokCUtils.getFeatureGlobalIdentifier(vp.getBusAccess()));
  }
  
  public static String getComponentInstanceIdentifier(ComponentInstance instance)
  {
    return GenerationUtilsC.getGenerationCIdentifier(instance.getComponentInstancePath());
  }
  
}