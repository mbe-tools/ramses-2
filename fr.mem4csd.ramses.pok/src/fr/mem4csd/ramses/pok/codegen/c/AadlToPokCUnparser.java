/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.pok.codegen.c;

import java.io.IOException ;
import java.util.ArrayList ;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List ;
import java.util.Set ;

import org.apache.log4j.Logger ;
import org.eclipse.core.runtime.IProgressMonitor ;
import org.eclipse.emf.common.util.EList ;
import org.eclipse.emf.common.util.URI;
import org.eclipse.xtext.EcoreUtil2;
import org.osate.aadl2.BasicPropertyAssociation ;
import org.osate.aadl2.BooleanLiteral ;
import org.osate.aadl2.ComponentCategory ;
import org.osate.aadl2.DataSubcomponent;
import org.osate.aadl2.DirectionType ;
import org.osate.aadl2.EnumerationLiteral ;
import org.osate.aadl2.IntegerLiteral ;
import org.osate.aadl2.ListValue ;
import org.osate.aadl2.MemorySubcomponent ;
import org.osate.aadl2.ModalPropertyValue ;
import org.osate.aadl2.NamedElement ;
import org.osate.aadl2.NamedValue ;
import org.osate.aadl2.ProcessImplementation ;
import org.osate.aadl2.ProcessSubcomponent ;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertyAssociation ;
import org.osate.aadl2.PropertyExpression ;
import org.osate.aadl2.RecordValue ;
import org.osate.aadl2.ReferenceValue ;
import org.osate.aadl2.StringLiteral ;
import org.osate.aadl2.Subcomponent ;
import org.osate.aadl2.SystemImplementation ;
import org.osate.aadl2.ThreadSubcomponent ;
import org.osate.aadl2.VirtualProcessorSubcomponent ;
import org.osate.aadl2.instance.ComponentInstance ;
import org.osate.aadl2.instance.FeatureCategory ;
import org.osate.aadl2.instance.FeatureInstance ;
import org.osate.aadl2.instance.SystemInstance ;
import org.osate.aadl2.modelsupport.UnparseText ;
import org.osate.utils.internal.PropertyUtils ;
import org.osate.xtext.aadl2.properties.util.AadlProject;
import org.osate.xtext.aadl2.properties.util.GetProperties;

import fr.mem4csd.ramses.arinc653.codegen.c.AadlToArinc653CUnparser;
import fr.mem4csd.ramses.arinc653.utils.AadlToARINC653Utils;
import fr.mem4csd.ramses.arinc653.utils.PartitionIndexComparator;
import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.arch_trace.util.ArchTraceSourceRetreival;
import fr.mem4csd.ramses.core.codegen.GenerationException;
import fr.mem4csd.ramses.core.codegen.ProcessProperties ;
import fr.mem4csd.ramses.core.codegen.ProcessorProperties;
import fr.mem4csd.ramses.core.codegen.RoutingProperties ;
import fr.mem4csd.ramses.core.codegen.RoutingProperties.VirtualPort;
import fr.mem4csd.ramses.core.codegen.utils.FileUtils;
import fr.mem4csd.ramses.core.codegen.utils.GenerationUtilsC;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils ;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;
import fr.mem4csd.ramses.core.workflowramses.TargetProperties;

public class AadlToPokCUnparser extends AadlToArinc653CUnparser
{

  private final static long DEFAULT_REQUIRED_STACK_SIZE = 16384 ;
  

  private static final String VIRTUAL_PORT_SUFFIX = "_virtual_port" ;
  
 // private Map<Object, Map<NamedElement, String>> _identifierMappingWithContext = new HashMap<Object, Map<NamedElement, String>>();
  
  private ProcessorProperties _processorProp;
  
  private int partitionId=0;
  
  private static Logger _LOGGER = Logger.getLogger(AadlToPokCUnparser.class) ;
  
  @Override
  public void generateProcessTargetConfiguration(ProcessSubcomponent process,
		  URI outputDir,
		  IProgressMonitor monitor)
  {
	_mainCCode = new UnparseText();
	_mainHCode = new UnparseText();
	_deploymentHCode = new UnparseText();
	
	StringBuilder sb = new StringBuilder(process.getQualifiedName());
    ProcessProperties pp = new ProcessProperties(sb.substring(0, sb.lastIndexOf("::")+2)) ;
    
    ProcessImplementation processImpl = (ProcessImplementation) 
                                          process.getComponentImplementation() ;
    
    ArchTraceSpec traces = getCurrentModelTransformationTrace();
    this.findCommunicationMechanism(processImpl, pp, traces);
    
    List<String> additionalHeaders = new ArrayList<String>();
    // Generate main.c, do before header in order to populate additional headers
    genMainImpl(process, pp, traces, additionalHeaders) ;
    
    genMainHeader(process, _processorProp, pp, traces, additionalHeaders);
    
    
    
    try
    {
      FileUtils.saveFile(outputDir, "main.h",
               _mainHCode.getParseOutput()) ;
      
      FileUtils.saveFile(outputDir, "deployment.h",
              _deploymentHCode.getParseOutput()) ;
      
      FileUtils.saveFile(outputDir, "main.c",
               _mainCCode.getParseOutput()) ;
    }
    catch(IOException e)
    {
      String errMsg = "cannot save the generated files for \'" +
                                                            process.getName() + '\'' ;
      _LOGGER.fatal(errMsg, e) ;
      throw new RuntimeException(errMsg, e) ;
    }
  }

  
  @Override
  public void generateProcessorTargetConfiguration(Subcomponent processorSubcomponent,
		  URI outputDir,
		  IProgressMonitor monitor) throws GenerationException
  { 
    ProcessorProperties processorProp = new ProcessorProperties() ;
    ArchTraceSpec traces = getCurrentModelTransformationTrace();
    ComponentInstance processorInst = (ComponentInstance) traces.
                                             getTransformationTrace(processorSubcomponent) ;
    
    RoutingProperties routing = (RoutingProperties) getCodeGenWorkflowComponent().getTargetProperties() ;
    
    // Discard older processor properties !
    _processorProp = processorProp ;
    try
    {
    	// Generate deployment.h
    	UnparseText deploymentHeaderCode = new UnparseText() ;
    	genDeploymentHeader(processorSubcomponent, deploymentHeaderCode, routing, traces) ;

    	FileUtils.saveFile(outputDir, "deployment.h",
    			deploymentHeaderCode.getParseOutput()) ;

    	// Generate deployment.c
    	UnparseText deploymentImplCode = new UnparseText() ;
    	genDeploymentImpl(processorSubcomponent, deploymentImplCode, processorProp) ;

    	FileUtils.saveFile(outputDir, "deployment.c",
                deploymentImplCode.getParseOutput()) ;

    	// Generate routing.h
    	UnparseText routingHeaderCode = new UnparseText() ;
    	genRoutingHeader(processorInst, routingHeaderCode, routing, traces) ;

    	FileUtils.saveFile(outputDir, "routing.h", routingHeaderCode.getParseOutput()) ;

    	// Generate routing.c
    	UnparseText routingImplCode = new UnparseText() ;
    	genRoutingImpl(processorInst, routingImplCode, routing) ;
      
    	FileUtils.saveFile(outputDir, "routing.c", routingImplCode.getParseOutput()) ;
    }
    catch(IOException e)
    {
      String errMsg = "cannot save the generated files" ;
      _LOGGER.fatal(errMsg, e) ;
      throw new RuntimeException(errMsg, e) ;
    }
  }
  
  
  List<String> takenSemaphoreId = new ArrayList<String>();
  
  //Generate global variables.
  protected void genGlobalVariablesMainImpl(ProcessSubcomponent processSub, EList<ThreadSubcomponent> lthreads,
                                          UnparseText _mainCCode,
                                          ProcessProperties pp)
  {
	ProcessImplementation processImpl = (ProcessImplementation) 
              processSub.getComponentImplementation() ;
    _mainCCode.addOutputNewline(GenerationUtilsC.generateSectionMark()) ;
    _mainCCode.addOutputNewline(GenerationUtilsC
          .generateSectionTitle("GLOBAL VARIABLES")) ;
    
    // Generate semaphore names array.
    _mainCCode.addOutput("char* pok_arinc653_semaphores_names[POK_CONFIG_ARINC653_NB_SEMAPHORES] = "
    		+ "{\""+processSub.getName()+"_mode_init_event\","
    		+ "\""+processSub.getName()+"_mode_init_mutex\""
    		+ ",") ;

    for(String name : pp.waitMessageNames)
    {
    	// limit string lentgh for semaphores names on pok
    	name = GeneratorUtils.limitCharNb(name,30,takenSemaphoreId);
    	_mainCCode.addOutput("\"") ;
    	_mainCCode.addOutput(name+"_evt") ;
    	_mainCCode.addOutput("\",") ;
    	_mainCCode.addOutput("\"") ;
    	_mainCCode.addOutput(name+"_rez") ;
    	_mainCCode.addOutput("\",") ;
    	
    }

    _mainCCode.addOutputNewline("};") ;

    
    super.genGlobalVariablesMainImpl(processSub, lthreads, _mainCCode, pp);
    
    genGlobalVariablesMainOptional(processImpl, _mainCCode);

  }
  
  protected void genGlobalVariablesMainOptional(ProcessImplementation process,
		UnparseText _mainCCode){}
  
  protected void addModeInitPostfix(ProcessSubcomponent process) {
	  _mainCCode.addOutputNewline("    STOP_SELF();");
  }
  
  private ProcessProperties genMainHeader(ProcessSubcomponent processSubcomponent,
                                            ProcessorProperties processorProp,
                                            ProcessProperties pp,
                                            ArchTraceSpec traces,
                                            List<String> additionalHeaders)
  {
			  
    ProcessImplementation process = (ProcessImplementation) 
			  processSubcomponent.getComponentImplementation() ;

    List<ThreadSubcomponent> bindedThreads =
            process.getOwnedThreadSubcomponents() ;
    boolean foundProcessHMActions = false;
    
    String guard = GenerationUtilsC.generateHeaderInclusionGuard("main.h") ;
    boolean foundHM = false;
    for(ThreadSubcomponent ts: process.getOwnedThreadSubcomponents())
    {
      for(PropertyAssociation pa: ts.getOwnedPropertyAssociations())
      {
        if(pa.getProperty().getName().equalsIgnoreCase("Error_Handling"))
        {
          BooleanLiteral bl = (BooleanLiteral) pa.
              getOwnedValues().get(0).getOwnedValue();
          foundHM = bl.getValue();
          foundProcessHMActions = foundHM;
          if(foundHM)
            break;
        }
        if(pa.getProperty().getName().equalsIgnoreCase("HM_Error_ID_Actions"))
        {
        	foundHM = true;
        	foundProcessHMActions = true;
        	break;
        }
      }
      
      if(foundHM)
  		break;
    }
    _mainHCode.addOutputNewline(guard) ;
    _mainHCode.addOutputNewline(GenerationUtilsC.getAdditionalHeader(additionalHeaders)) ;
    /**** #DEFINE ****/

    _mainHCode.addOutputNewline("#define POK_GENERATED_CODE 1") ;
    _mainHCode.addOutputNewline("#define POK_CONFIG_NEEDS_FUNC_MEMCPY 1");
    
    if(processorProp.consoleFound == true)
    {
      _mainHCode.addOutputNewline("#define POK_NEEDS_CONSOLE 1") ;
    }

    if(processorProp.stdioFound == true)
    {
      _mainHCode
            .addOutputNewline("#define POK_NEEDS_LIBC_STDIO 1") ;
    }

    if(processorProp.stringFound == true)
    {
      _mainHCode
            .addOutputNewline("#define POK_NEEDS_STRING 1") ;
    }
    
    if(processorProp.stdlibFound == true)
    {
      _mainHCode
            .addOutputNewline("#define POK_NEEDS_LIBC_STDLIB 1") ;
    }
    
    if(foundHM)
      _mainHCode
        	.addOutputNewline("#define POK_NEEDS_ARINC653_ERROR 1") ;
    if(foundProcessHMActions)
      _mainHCode
  			.addOutputNewline("#define POK_USE_GENERATED_ERROR_HANDLER 1");
    
    int internalThreads = 2;
    if(needsGeneratedErrorHandler(processSubcomponent))
    	internalThreads++;
    _mainHCode
          .addOutputNewline("#define POK_CONFIG_NB_THREADS " +
                Integer.toString(bindedThreads.size() + internalThreads )) ;
    _mainHCode
          .addOutputNewline("#define POK_NEEDS_THREADS 1");

    _mainHCode.addOutputNewline("#define POK_NEEDS_ARINC653_SEMAPHORE 1") ;
    int nbSemaph = 2*pp.waitMessageNames.size()+2; // 2 semaphores per event + 2 semaphores for mode init
    _mainHCode.addOutputNewline("#define POK_CONFIG_ARINC653_NB_SEMAPHORES " +
    		nbSemaph); 
    if(pp.hasQueue)
    {
      _mainHCode.addOutputNewline("#define POK_NEEDS_ARINC653_QUEUEING 1") ;
      
      // XXX ARBITRARY
      _mainHCode.addOutputNewline("#define POK_NEEDS_LIBC_STRING 1");
      _mainHCode.addOutputNewline("#define POK_NEEDS_PARTITIONS 1");
      _mainHCode.addOutputNewline("#define POK_NEEDS_PROTOCOLS 1") ;
      
    }
    
    if(pp.hasSample)
    {
      _mainHCode.addOutputNewline("#define POK_NEEDS_ARINC653_SAMPLING 1");
      // XXX ARBITRARY
      _mainHCode.addOutputNewline("#define POK_NEEDS_LIBC_STRING 1");
      _mainHCode.addOutputNewline("#define POK_NEEDS_PARTITIONS 1");
      _mainHCode.addOutputNewline("#define POK_NEEDS_PROTOCOLS 1") ;
    }
    
    _mainHCode
    	.addOutputNewline("#define POK_CONFIG_STACKS_SIZE " +
    			Long.toString(processorProp.requiredStackSizePerPartition.get(process)));
    // XXX Is there any condition for the generation of theses directives ?
    // XXX ARBITRARY
    _mainHCode
          .addOutputNewline("#define POK_NEEDS_ARINC653_TIME 1") ;
    _mainHCode
          .addOutputNewline("#define POK_NEEDS_FUNCTION_TIME_GET 1");
    _mainHCode
          .addOutputNewline("#define POK_CONFIG_NEEDS_FUNC_UDIVDI3 1");
    _mainHCode
    	  .addOutputNewline("#define POK_CONFIG_NEEDS_FUNC_UMODDI3 1");
    _mainHCode
          .addOutputNewline("#define POK_NEEDS_ARINC653_PROCESS 1") ;
    _mainHCode
          .addOutputNewline("#define POK_NEEDS_ARINC653_PARTITION 1") ;
    
    // XXX Is there any condition for the generation of POK_NEEDS_MIDDLEWARE ?
    // XXX ARBITRARY
    _mainHCode
          .addOutputNewline("#define POK_NEEDS_MIDDLEWARE 1") ;
    
    // The macro POK_NEEDS_PORTS_VIRTUAL is used to include functions definition
    // create and use virtual ports (for remote communications for instance)
    for(DataSubcomponent d : process.getOwnedDataSubcomponents())
    {
      if(d.getDataSubcomponentType().getName().equals("ProcessorLinkType"))
      {
    	_mainHCode.addOutputNewline("#define POK_NEEDS_PORTS_VIRTUAL 1");
    	_mainHCode.addOutputNewline("#define POK_NEEDS_RTL8029 1");
    	_mainHCode.addOutputNewline("#define POK_NEEDS_PCI 1");
    	_mainHCode.addOutputNewline("#define POK_NEEDS_IO 1");
    	if(_processorProp.hwAdress!=null)
    	  _mainHCode.addOutputNewline("#define POK_NEEDS_MAC_ADDR 1");
    	break;
      }
    }
    
    /**** "#INCLUDE ****/
    
    _mainHCode.addOutputNewline("");
    
    // always files included:
    _mainHCode.addOutputNewline("#include <arinc653/types.h>");
    _mainHCode.addOutputNewline("#include <arinc653/process.h>");
    _mainHCode.addOutputNewline("#include <arinc653/partition.h>");
    _mainHCode.addOutputNewline("#include <arinc653/time.h>");
    _mainHCode.addOutputNewline("#include \"gtypes.h\"");
    
    // conditioned files included:
        
    if(pp.hasQueue)
    {
      _mainHCode.addOutputNewline("#include <arinc653/queueing.h>");
      
      // XXX ARBITRARY
      _mainHCode.addOutputNewline("#include <protocols/protocols.h>");
    }
    
    if(pp.hasSample)
    {
      _mainHCode.addOutputNewline("#include <arinc653/sampling.h>");
      
      // XXX ARBITRARY
      _mainHCode.addOutputNewline("#include <protocols/protocols.h>");
    }
    
    if(foundHM)
      _mainHCode.addOutputNewline("#include <arinc653/error.h>");
    
    _mainHCode.addOutputNewline("\n#endif") ;
    
    return pp ;
  }

  private void genDeploymentImpl(Subcomponent processor,
                                 UnparseText deploymentImplCode,
                                 ProcessorProperties pokProp)
  {
    deploymentImplCode.addOutputNewline("#include <types.h>") ;
    deploymentImplCode.addOutputNewline("#include \"deployment.h\"") ;
    
    String propertyName = "HM_Error_ID_Levels";
    Property prop = GetProperties.lookupPropertyDefinition(processor, "ARINC653", propertyName);
    List<? extends PropertyExpression> peList;
    try {
    	peList = processor.getPropertyValueList(prop);
    } catch (Exception e) {
    	peList = Collections.emptyList();
    }
    if(! peList.isEmpty())
    {
      if(needKernelErrorHandler(processor))
      {
        deploymentImplCode.addOutputNewline("void pok_kernel_error");
        deploymentImplCode.incrementIndent();
        deploymentImplCode.incrementIndent();
        deploymentImplCode.addOutputNewline("(uint32_t error)");
        deploymentImplCode.decrementIndent();
        deploymentImplCode.decrementIndent();
        deploymentImplCode.addOutputNewline("{");
        deploymentImplCode.incrementIndent();
        deploymentImplCode.addOutputNewline("switch (error)");
        deploymentImplCode.addOutputNewline("{");
        deploymentImplCode.incrementIndent();
        generateErrorIdSelection(processor, deploymentImplCode, null, null);
        deploymentImplCode.decrementIndent();
        deploymentImplCode.addOutputNewline("} // switch error");
        deploymentImplCode.decrementIndent();
        deploymentImplCode.decrementIndent();
        deploymentImplCode.addOutputNewline("} // pok_kernel_error");
      }
    }
    boolean partitionLevelErrors = false;
	ArchTraceSpec traces = ArchTraceSourceRetreival.getTracesFromNamedElement(processor);
    ComponentInstance processorInSourceModel = (ComponentInstance) traces.getTransformationTrace(processor);
    List<ComponentInstance> bindedVPI = AadlToARINC653Utils.getBindedVPI(processorInSourceModel);
    
    for(ComponentInstance vps: bindedVPI)
    {
      prop = GetProperties.lookupPropertyDefinition(vps, "ARINC653", "HM_Error_ID_Actions");
      try {
    	  peList = vps.getPropertyValueList(prop);
      } catch (Exception e) {
    	  peList = Collections.emptyList();
      }
      if(!peList.isEmpty())
      {
        partitionLevelErrors = true;
        break;
      }
    }
    
    if(partitionLevelErrors==false)
      return;
    
    if(needPartitionErrorHandler(bindedVPI))
    {
      partitionId=0;
      deploymentImplCode.addOutputNewline("void pok_partition_error");
      deploymentImplCode.incrementIndent();
      deploymentImplCode.incrementIndent();
      deploymentImplCode.addOutputNewline("(uint8_t partition, uint32_t error)");
      deploymentImplCode.decrementIndent();
      deploymentImplCode.decrementIndent();
      deploymentImplCode.addOutputNewline("{");
      deploymentImplCode.incrementIndent();
      deploymentImplCode.addOutputNewline("switch (partition)");
      deploymentImplCode.addOutputNewline("{");
      deploymentImplCode.incrementIndent();
      for(ComponentInstance vps: bindedVPI)
      {
        deploymentImplCode.addOutputNewline("case "+Integer.toString(partitionId)+":");
        deploymentImplCode.incrementIndent();
        deploymentImplCode.addOutputNewline("switch (error)");
        deploymentImplCode.addOutputNewline("{");
        deploymentImplCode.incrementIndent();
        generateErrorIdSelection(processor, deploymentImplCode, vps, null);
        deploymentImplCode.decrementIndent();
        deploymentImplCode.addOutputNewline("break;");
        deploymentImplCode.addOutputNewline("}");
        deploymentImplCode.decrementIndent();
        partitionId++;
      }
      deploymentImplCode.decrementIndent();
      deploymentImplCode.addOutputNewline("}");

      deploymentImplCode.decrementIndent();
      deploymentImplCode.addOutputNewline("}");
    }
  }
  
  private void generateErrorIdSelection(NamedElement ne,
		UnparseText deploymentImplCode,
		ComponentInstance vps,
		ComponentInstance ths) {
    
    Property prop = GetProperties.lookupPropertyDefinition(ne, "ARINC653", "HM_Error_ID_Levels");
    List<? extends PropertyExpression> peList;
    try {
    	peList = ne.getPropertyValueList(prop);
    } catch (Exception e) {
    	peList = Collections.emptyList();
    }
    
    for(PropertyExpression pe : peList) {
    	if (pe instanceof RecordValue) {

    		String level = null;
    		String errorCode = null;
    		long errorIdentifier = -1;


    		RecordValue rv = (RecordValue) pe;
    		for(BasicPropertyAssociation bpa: rv.getOwnedFieldValues())
    		{
    			if(bpa.getProperty().getName().equalsIgnoreCase("ErrorIdentifier"))
    				errorIdentifier = ((IntegerLiteral) bpa.getValue()).getValue();
    			else if(bpa.getProperty().getName().equalsIgnoreCase("ErrorLevel"))
    			{
    				NamedValue nv = (NamedValue) bpa.getValue();
    				level = ((EnumerationLiteral) nv.getNamedValue()).getName();
    			}
    			else if(bpa.getProperty().getName().equalsIgnoreCase("ErrorCode"))
    			{
    				NamedValue nv = (NamedValue) bpa.getValue();
    				errorCode = ((EnumerationLiteral) nv.getNamedValue()).getName();
    			}
    		}
    		if(level==null)
    			continue;
    		if(level.equalsIgnoreCase("Module_Level") && (vps!=null||ths!=null))
    			continue;
    		if(level.equalsIgnoreCase("Partition_Level") && (vps==null || ths!=null))
    			continue;
    		if(level.equalsIgnoreCase("Process_Level") && (ths==null||vps!=null))
    			continue;

    		String pokErrorId="POK_ERROR_KIND_INVALID";
    		//Module_Config
    		if(errorCode.equalsIgnoreCase("Module_Config"))
    		{
    			pokErrorId = "POK_ERROR_KIND_KERNEL_CONFIG";
    		}
    		//Module_Init
    		else if(errorCode.equalsIgnoreCase("Module_Init"))
    		{
    			pokErrorId = "POK_ERROR_KIND_KERNEL_INIT";
    		}
    		//Module_Scheduling
    		else if(errorCode.equalsIgnoreCase("Module_Scheduling"))
    		{
    			pokErrorId = "POK_ERROR_KIND_KERNEL_SCHEDULING";
    		}
    		//Partition_Scheduling
    		else if(errorCode.equalsIgnoreCase("Partition_Scheduling"))
    		{
    			pokErrorId = "POK_ERROR_KIND_PARTITION_SCHEDULING";
    		}
    		//Partition_Config
    		else if(errorCode.equalsIgnoreCase("Partition_Config"))
    		{
    			pokErrorId = "POK_ERROR_KIND_PARTITION_CONFIGURATION";
    		}
    		//Partition_Handler
    		else if(errorCode.equalsIgnoreCase("Partition_Handler"))
    		{
    			pokErrorId = "POK_ERROR_KIND_PARTITION_HANDLER";
    		}
    		//Partition_Init
    		else if(errorCode.equalsIgnoreCase("Partition_Init"))
    		{
    			pokErrorId = "POK_ERROR_KIND_PARTITION_INIT";
    		}
    		//Deadline_Miss
    		else if(errorCode.equalsIgnoreCase("Deadline_Miss"))
    		{
    			pokErrorId = "POK_ERROR_KIND_DEADLINE_MISSED";
    		}
    		//Application_Error
    		else if(errorCode.equalsIgnoreCase("Application_Error"))
    		{
    			pokErrorId = "POK_ERROR_KIND_APPLICATION_ERROR";
    		}
    		//Numeric_Error
    		else if(errorCode.equalsIgnoreCase("Numeric_Error"))
    		{
    			pokErrorId = "POK_ERROR_KIND_NUMERIC_ERROR";
    		}
    		//Illegal_Request
    		else if(errorCode.equalsIgnoreCase("Illegal_Request"))
    		{
    			pokErrorId = "POK_ERROR_KIND_ILLEGAL_REQUEST";
    		}
    		//Stack_Overflow
    		else if(errorCode.equalsIgnoreCase("Stack_Overflow"))
    		{
    			pokErrorId = "POK_ERROR_KIND_STACK_OVERFLOW";
    		}
    		//Memory_Violation
    		else if(errorCode.equalsIgnoreCase("Memory_Violation"))
    		{
    			pokErrorId = "POK_ERROR_KIND_MEMORY_VIOLATION";
    		}
    		//Hardware_Fault
    		else if(errorCode.equalsIgnoreCase("Hardware_Fault"))
    		{
    			pokErrorId = "POK_ERROR_KIND_HARDWARE_FAULT";
    		}
    		//Power_Fail
    		else if(errorCode.equalsIgnoreCase("Power_Fail"))
    		{
    			pokErrorId = "POK_ERROR_KIND_POWER_FAIL";
    		}
    		
    		if(pokErrorId=="POK_ERROR_KIND_INVALID")
    			continue;
    		
    		deploymentImplCode.addOutputNewline("case "+pokErrorId+":");
    		deploymentImplCode.addOutputNewline("{");
    		deploymentImplCode.incrementIndent();



    		if(vps==null && ths==null && level.equalsIgnoreCase("Module_Level"))
    		{
    			String actionId = getActionId(ne, errorIdentifier);
    			genModuleErrorAction(deploymentImplCode,actionId);
    		}
    		if(vps!=null && level.equalsIgnoreCase("Partition_Level"))
    		{  
    			String actionId = getActionId(vps, errorIdentifier);
    			genPartitionErrorAction(deploymentImplCode, actionId);
    		}
    		if(ths!=null && level.equalsIgnoreCase("Process_Level"))
    		{  
    			String actionId = getActionId(ths, errorIdentifier);
    			genProcessErrorAction(deploymentImplCode, actionId);
    		}
    		
    		deploymentImplCode.addOutputNewline("break;");
    		deploymentImplCode.decrementIndent();
    		deploymentImplCode.addOutputNewline("}");
    	}
    }
	deploymentImplCode.addOutputNewline("default:");
	deploymentImplCode.addOutputNewline("{");
	deploymentImplCode.incrementIndent();
	
	
	if(vps==null && ths==null)
	{ 
		deploymentImplCode.addOutputNewline("pok_kernel_stop();");
	}
	else if(vps!=null)
		deploymentImplCode.addOutputNewline("pok_partition_set_mode("+partitionId+", POK_PARTITION_MODE_STOPPED);");
	else
		deploymentImplCode.addOutputNewline("pok_partition_set_mode(POK_PARTITION_MODE_STOPPED);");

	deploymentImplCode.addOutputNewline("break;");
	
	deploymentImplCode.decrementIndent();
	deploymentImplCode.addOutputNewline("}");
  }

  private void genProcessErrorAction(UnparseText deploymentImplCode, String actionId) {
	  if(actionId.equalsIgnoreCase("Partition_Stop"))
	  {
		  deploymentImplCode.addOutputNewline("pok_partition_set_mode(POK_PARTITION_MODE_STOPPED);");
	  }
	  else if(actionId.equalsIgnoreCase("Warm_Restart"))
	  {
		  deploymentImplCode.addOutputNewline("pok_partition_set_mode(POK_PARTITION_MODE_INIT_WARM);");
	  }
	  else if(actionId.equalsIgnoreCase("Cold_Restart"))
	  {
		  deploymentImplCode.addOutputNewline("pok_partition_set_mode(POK_PARTITION_MODE_INIT_COLD);");
	  }
	  else if(actionId.equalsIgnoreCase("Ignore"))
	  {
		  deploymentImplCode.addOutputNewline("// Ignore");
	  }
	  // Process_Restart, Process_Stop
	  else if(actionId.equalsIgnoreCase("Process_Restart"))
	  {
		  deploymentImplCode.addOutputNewline("pok_thread_stop(status.failed_thread);");
		  deploymentImplCode.addOutputNewline("pok_thread_delayed_start(status.failed_thread, 0);");
	  }
	  else if(actionId.equalsIgnoreCase("Process_Stop"))
	  {
		  deploymentImplCode.addOutputNewline("pok_thread_stop(status.failed_thread);");
	  }
	  else
	  {
	      deploymentImplCode.addOutputNewline("// Default action; none specified");
	      deploymentImplCode.addOutputNewline("pok_partition_set_mode(POK_PARTITION_MODE_STOPPED);");
	  }
  }


  private String getActionId(NamedElement ne, long errorIdentifier)
  {
	  Property prop = GetProperties.lookupPropertyDefinition(ne, "ARINC653", "HM_Error_ID_Actions");
	  List<? extends PropertyExpression> peList;
	  try {
		  peList = ne.getPropertyValueList(prop);
	  } catch (Exception e) {
		  peList = Collections.emptyList();
	  }

	  for (PropertyExpression pe : peList) {
		  long errorId=-1;
		  String action=null;
		  if(pe instanceof RecordValue)
		  {
			  RecordValue rv = (RecordValue) pe;
			  for(BasicPropertyAssociation bpa: rv.getOwnedFieldValues())
			  {
				  if(bpa.getProperty().getName().equalsIgnoreCase("ErrorIdentifier"))
					  errorId = ((IntegerLiteral) bpa.getValue()).getValue();
				  else if(bpa.getProperty().getName().equalsIgnoreCase("Action"))
				  {
					  StringLiteral sl = (StringLiteral) bpa.getValue();
					  action = sl.getValue();
				  }
			  }
		  }
		  if(errorId==errorIdentifier && action!=null)
			  return action;
	  }
	  return "";
  }

  private void genModuleErrorAction(UnparseText deploymentImplCode, String actionId) {
    // Ignore, Stop, Reset
    if(actionId.equalsIgnoreCase("Stop"))
    {
      deploymentImplCode.addOutputNewline("pok_kernel_stop();");
    }
    else if(actionId.equalsIgnoreCase("Reset"))
    {
      deploymentImplCode.addOutputNewline("pok_kernel_restart();");
    }
    else if(actionId.equalsIgnoreCase("Ignore"))
    {
      deploymentImplCode.addOutputNewline("// Ignore");
    }
  }
  
  private void genPartitionErrorAction(UnparseText deploymentImplCode, String actionId)
  {
	  if(actionId.equalsIgnoreCase("Partition_Stop"))
	  {
		  deploymentImplCode.addOutputNewline("pok_partition_set_mode("+partitionId+", POK_PARTITION_MODE_STOPPED);");
	  }
	  else if(actionId.equalsIgnoreCase("Warm_Restart"))
	  {
		  deploymentImplCode.addOutputNewline("pok_partition_set_mode("+partitionId+", POK_PARTITION_MODE_INIT_WARM);");
	  }
	  else if(actionId.equalsIgnoreCase("Cold_Restart"))
	  {
		  deploymentImplCode.addOutputNewline("pok_partition_set_mode("+partitionId+", POK_PARTITION_MODE_INIT_COLD);");
	  }
	  else if(actionId.equalsIgnoreCase("Ignore"))
	  {
		  deploymentImplCode.addOutputNewline("// Ignore");
	  }
	  else
	  {
		  deploymentImplCode.addOutputNewline("// Default action; none specified");
		  deploymentImplCode.addOutputNewline("pok_partition_set_mode("+partitionId+", POK_PARTITION_MODE_STOPPED);");
	  }	
  }
  
  private void genDeploymentHeader(Subcomponent processor,
                                   UnparseText deploymentHeaderCode,
                                   RoutingProperties routing,
                                   ArchTraceSpec traces)
                                                      throws GenerationException
  {
    _processorProp = new ProcessorProperties() ;
    
    String guard = GenerationUtilsC.generateHeaderInclusionGuard("deployment.h") ;

    deploymentHeaderCode.addOutputNewline(guard) ;

    deploymentHeaderCode.addOutputNewline("#include \"routing.h\"") ;
    // POK::Additional_Features => (libc_stdio,libc_stdlib,console);
    // this property is associated to virtual processors
    
    ComponentInstance processorInSourceModel = (ComponentInstance) traces.getTransformationTrace(processor);
    List<ComponentInstance> bindedVPI = AadlToARINC653Utils.getBindedVPI(processorInSourceModel);
    
    List<String> additionalFeatures = new ArrayList<String>();
    
    // Try to fetch POK properties: Additional_Features.
    for(ComponentInstance vps : bindedVPI)
    {
      Property prop = GetProperties.lookupPropertyDefinition(vps, "POK", "Additional_Features");
      List<? extends PropertyExpression> props;
      try {
    	props = vps.getPropertyValueList(prop);
	  } catch (Exception e) {
	    props = Collections.emptyList();
	  }
      
      for(PropertyExpression pe : props)
      {
    	  NamedValue nv = (NamedValue) pe;
    	  EnumerationLiteral el= (EnumerationLiteral) nv.getNamedValue();
    	  additionalFeatures.add(el.getName());
      }
      
    	if(additionalFeatures!=null)
    	{
    		for(String s : additionalFeatures)
    		{
    			if(s.equalsIgnoreCase("console"))
    			{
    				// POK_NEEDS_CONSOLE has to be in both kernel's deployment.h
    				deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_CONSOLE 1") ;
    				_processorProp.consoleFound = true ;
    				break ;
    			}
    		}

    		for(String s : additionalFeatures)
    		{
    			if(s.equalsIgnoreCase("libc_stdio"))
    			{
    				_processorProp.stdioFound = true ;
    				break ;
    			}
    		}

    		for(String s : additionalFeatures)
    		{
    			if(s.equalsIgnoreCase("libc_stdlib"))
    			{
    				_processorProp.stdlibFound = true ;
    				break ;
    			}
    		}
    		
    		for(String s : additionalFeatures)
    		{
    			if(s.equalsIgnoreCase("libc_string"))
    			{
    				_processorProp.stringFound = true ;
    				break ;
    			}
    		}
    	}
    	else
    	{
    	  String errMsg = "cannot fetch Additional_Features for \'" +
    	      vps.getName() + '\'' ;
    	  _LOGGER.error(errMsg) ;
    	}
    }

    String hwAddr = PropertyUtils.getStringValue(processor, "Address");
    if(hwAddr!=null && false==hwAddr.isEmpty())
      _processorProp.hwAdress = hwAddr;
    
    // TODO: the integer ID in this macro must be set carefully to respect the
    // routing table defined in deployment.c files in the generated code for a
    // partition.
    int id =
          ((SystemImplementation) processor.eContainer())
                .getOwnedProcessorSubcomponents().indexOf(processor) ;
    deploymentHeaderCode.addOutputNewline("#define POK_CONFIG_LOCAL_NODE " +
          Integer.toString(id)) ;
    
    String address = PropertyUtils.getStringValue(processor, "Address");
    if(address!=null)
    {
      deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_PCI 1");
      deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_IO 1");
      deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_PORTS_VIRTUAL 1");
      deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_MAC_ADDR 1");
    }
    // POK_GENERATED_CODE 1 always true in our usage context
    deploymentHeaderCode.addOutputNewline("#define POK_GENERATED_CODE 1") ;

    deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_GETTICK 1") ;
    // POK_NEEDS_THREADS 1 always true in our usage context.
    deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_THREADS 1") ;

    if(bindedVPI.size() > 0)
    {
      deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_PARTITIONS 1") ;
    }
    
    if(_processorProp.stdioFound
    		|| _processorProp.consoleFound)
    {
      deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_USER_DEBUG 1") ;
    }
    
    deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_SCHED 1") ;
    
    
    List<ProcessSubcomponent> bindedProcess =
            AadlHelperImpl.getBindedProcesses(processor) ;
    Collections.sort(bindedProcess, new PartitionIndexComparator(processor));
    
    // The maccro POK_CONFIG_NB_PROCESSORS indicates the number of cores
    Long processorsNb = AadlHelperImpl.getProcessorsNumber(processor);
    deploymentHeaderCode.addOutput("#define POK_CONFIG_NB_PROCESSORS ") ;
    deploymentHeaderCode.addOutputNewline(processorsNb.toString());
    
    List<Long> processorAffinities = new ArrayList<Long>();
    for(ProcessSubcomponent process: bindedProcess)
    {
    	traces = ArchTraceSourceRetreival.getTracesFromNamedElement(process);
    	ComponentInstance processInSrcModel = (ComponentInstance) 
    			traces.getTransformationTrace(process) ;

    	List<Long> processAffinities =
    			AadlHelperImpl.getProcessOrVirtualProcessorAffinities(processInSrcModel);
    	processorAffinities.add(GeneratorUtils.toBitMask(processAffinities));
    }
    
    
    deploymentHeaderCode.addOutput("#define POK_CONFIG_PROCESSOR_AFFINITY {");
    int idx=0;
    for(; idx<processorAffinities.size()-1; idx++)
    {
    	deploymentHeaderCode.addOutput(processorAffinities.get(idx).toString()+",");
    }
    deploymentHeaderCode.addOutput(processorAffinities.get(idx).toString());
    deploymentHeaderCode.addOutputNewline("}");
    // The maccro POK_CONFIG_NB_PARTITIONS indicates the amount of partitions in
    // the current system.It corresponds to the amount of process components in
    // the system.
    deploymentHeaderCode.addOutputNewline("#define POK_CONFIG_NB_PARTITIONS " +
          Integer.toString(bindedVPI.size())) ;
    List<ThreadSubcomponent> bindedThreads =
          new ArrayList<ThreadSubcomponent>() ;
    List<Integer> threadNumberPerPartition = new ArrayList<Integer>() ;

    int internalThreads = 0;
    
    for(ProcessSubcomponent p : bindedProcess)
    {
      ProcessImplementation processImplementation =
            (ProcessImplementation) p.getComponentImplementation() ;
      bindedThreads.addAll(processImplementation.getOwnedThreadSubcomponents()) ;
      threadNumberPerPartition.add(Integer.valueOf(processImplementation
            .getOwnedThreadSubcomponents().size()+2)) ;
      internalThreads = internalThreads + 2;
      if(needsGeneratedErrorHandler(p))
      	internalThreads++;
    }
    for(ThreadSubcomponent th : bindedThreads)
    {
	  ComponentInstance thInst = (ComponentInstance) traces.getTransformationTrace(th) ;
	  String dispatchProtocol = PropertyUtils.getEnumValue(thInst, "Dispatch_Protocol") ;
	  if(dispatchProtocol == null)
        dispatchProtocol = PropertyUtils.getEnumValue(th, "Dispatch_Protocol") ;
      if(dispatchProtocol != null)
      {
        if(dispatchProtocol.equalsIgnoreCase("sporadic"))
        {
          deploymentHeaderCode
                .addOutputNewline("#define POK_NEEDS_THREAD_SLEEP 1") ;
          break ;
        }
      }
      else
      {
        String errMsg =  "cannot fetch the Dispatch_Protocol for \'"+
                                    th.getName() + '\'' ;
        _LOGGER.error(errMsg);
      }
    }
    
    //  The macro POK_CONFIG_NB_THREADS indicates the number of threads used in 
    //  the kernel. It comprises the tasks for the partition and the main task of 
    //  each partition that initialize all resources.
    //  on thread per process is responsible for modes management
    deploymentHeaderCode.addOutputNewline("#define POK_CONFIG_NB_THREADS " +
          Long.toString(1 + internalThreads + bindedThreads.size() + processorsNb)) ;
    //  The macro POK_CONFIG_NB_PARTITIONS_NTHREADS indicates the amount of 
    //  threads in each partition we also add an additional process that 
    //  initialize all partition's entities (communication, threads, ...)
    deploymentHeaderCode.addOutput("#define POK_CONFIG_PARTITIONS_NTHREADS {") ;

    idx = 0 ;
    for(Integer i : threadNumberPerPartition)
    {

      deploymentHeaderCode.addOutput(Integer.toString(i)) ;

      if(idx != (threadNumberPerPartition.size() - 1))
      {
        deploymentHeaderCode.addOutput(",") ;
      }
      idx++ ;
    }

    deploymentHeaderCode.addOutputNewline("}") ;

//    Remove sched protocol at system level, use FPS
    for(ComponentInstance vps : bindedVPI)
    {
      boolean foundSched = false ;

      String protocol = GeneratorUtils.getSchedulingProtocol(vps);
      if(protocol!=null)
      {
        if(protocol.equalsIgnoreCase("RMS") && foundSched == false)
        {
          foundSched = true ;
          deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_SCHED_RMS 1") ;
        }
        else if(protocol.equalsIgnoreCase("Round_Robin_Protocol")
        		&& foundSched == false)
        {
          foundSched = true ;
          deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_SCHED_RR 1") ;
        }
        else if(protocol.equalsIgnoreCase("POSIX_1003_HIGHEST_PRIORITY_FIRST_PROTOCOL"))
        {
            foundSched = true ;
        	deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_SCHED_STATIC 1") ;
        }
        
      }
      else
      {
        String errMsg = "Scheduling_Protocol is not provided for \'" +
                             vps.getName() + '\'' ;
        _LOGGER.error(errMsg);
      }
    }

    deploymentHeaderCode.addOutput("#define POK_CONFIG_PARTITIONS_SCHEDULER {") ;

    for(ComponentInstance vps : bindedVPI)
    {
      String protocol = GeneratorUtils.getSchedulingProtocol(vps);
      if(protocol!=null)
      {
        if(protocol.equalsIgnoreCase("Round_Robin_Protocol"))
        {
          deploymentHeaderCode.addOutput("POK_SCHED_RR") ;
        }
        else if(protocol.equalsIgnoreCase("RMS"))
        {
          deploymentHeaderCode.addOutput("POK_SCHED_RMS") ;
        }
        else if(protocol.equalsIgnoreCase("POSIX_1003_HIGHEST_PRIORITY_FIRST_PROTOCOL"))
        {
          deploymentHeaderCode.addOutput("POK_SCHED_STATIC") ;
        }
        if(bindedVPI.indexOf(vps) != bindedVPI.size() - 1)
        {
          deploymentHeaderCode.addOutput(",") ;
        }
      }
      else
      {
        String errMsg =  "cannot fetch the Scheduling_Protocol for \'" + vps.getName() + '\'';
        _LOGGER.error(errMsg);
      }
    }

    deploymentHeaderCode.addOutputNewline("}") ;

    boolean queueAlreadyAdded = false ;
    boolean sampleAlreadyAdded = false ;
    for(ProcessSubcomponent ps : bindedProcess)
    {
      ProcessImplementation process =
            (ProcessImplementation) ps.getComponentImplementation() ;
      if(!_processorProp.processProperties.containsKey(process))
      {
    	StringBuilder sb = new StringBuilder(process.getQualifiedName());
        ProcessProperties pp = new ProcessProperties(sb.substring(0, sb.lastIndexOf("::")+2)) ;
        _processorProp.processProperties.put(process, pp) ;
        findCommunicationMechanism(process, pp, traces) ;
      }
      ProcessProperties pp = _processorProp.processProperties.get(process) ;
      deploymentHeaderCode
              .addOutputNewline("#define POK_NEEDS_LOCKOBJECTS 1") ;
      
      if(pp.hasSample && false == sampleAlreadyAdded)
      {
        deploymentHeaderCode
                    .addOutputNewline("#define POK_NEEDS_PORTS_SAMPLING 1");
            
        sampleAlreadyAdded = true ;
      }
      
      if(pp.hasQueue && false == queueAlreadyAdded)
      {
        deploymentHeaderCode
                    .addOutputNewline("#define POK_NEEDS_PORTS_QUEUEING 1");
            
        queueAlreadyAdded = true ;
      }
      
    }

    if(sampleAlreadyAdded || queueAlreadyAdded)
    	deploymentHeaderCode
        .addOutputNewline("#define POK_NEEDS_THREAD_ID 1") ;
    int nbLocks = 0;
    deploymentHeaderCode
          .addOutput("#define POK_CONFIG_PARTITIONS_NLOCKOBJECTS {") ;
    for(ProcessSubcomponent ps : bindedProcess)
    {
      ProcessProperties pp =
            _processorProp.processProperties.get((ProcessImplementation) ps
                  .getComponentImplementation()) ;
      int partitionNbLock = pp.queueInfo.size()
              + pp.sampleInfo.size()
              + 2*pp.waitMessageNames.size() // each event is a pair of semaphores
              + 2 // 2 semaphores for mode initialization
              ;
      nbLocks+=partitionNbLock;
      deploymentHeaderCode.addOutput(Integer
            .toString(partitionNbLock)) ;  
      if(bindedProcess.indexOf(ps) < bindedProcess.size() - 1)
      {
        deploymentHeaderCode.addOutput(",") ;
      }
    }
    deploymentHeaderCode.addOutputNewline("}") ;
    
    deploymentHeaderCode.addOutputNewline("#define POK_CONFIG_NB_LOCKOBJECTS "+nbLocks) ;
    //  The maccro POK_CONFIG_PARTTITIONS_SIZE indicates the required amount of 
    //  memory for each partition.This value was deduced from the property 
    //  POK::Needed_Memory_Size of each process
    // comes from property POK::Needed_Memory_Size => XXX Kbyte;
    List<Long> memorySizePerPartition = new ArrayList<Long>() ;

    for(ProcessSubcomponent p : bindedProcess)
    {
      Long value = null;
      
      Property prop = GetProperties.lookupPropertyDefinition(p, "POK", "Needed_Memory_Size");
	  List<? extends PropertyExpression> propList;
	  try {
		  propList = p.getPropertyValueList(prop);
	  } catch (Exception e) {
		  propList = Collections.emptyList();
	  }
      
      for(PropertyExpression pe:propList)
      {
    	  IntegerLiteral il = (IntegerLiteral) pe;
    	  double size = il.getScaledValue(AadlProject.B_LITERAL);
    	  value = (new Double(size)).longValue();
      }
      if(value != null)
      {
        memorySizePerPartition.add(value) ;
      }
      else
      {
        String warnMsg =  "cannot fetch Needed_Memory_Size for \'"+
                          p.getName() + "\'. try to fetch the partition memory";
        _LOGGER.warn(warnMsg);
        
        MemorySubcomponent bindedMemory =
              (MemorySubcomponent) AadlHelperImpl
                    .getDeloymentMemorySubcomponent(p) ;
        
        prop = GetProperties.lookupPropertyDefinition(bindedMemory, "Memory_Properties", "Memory_Size");
        try {
        	propList = bindedMemory.getPropertyValueList(prop);
        } catch (Exception e) {
        	propList = Collections.emptyList();
        }
        
        for(PropertyExpression pe:propList)
        {
        	IntegerLiteral il = (IntegerLiteral) pe;
        	double size = il.getScaledValue(AadlProject.B_LITERAL);
        	value = (new Double(size)).longValue();
        }
        if(value != null)
        {
          memorySizePerPartition.add(value) ;
        }
        else
        {
          String errMsg = "cannot fetch the partition memory (Memory_Size) for \'"+
                                                  bindedMemory.getName() + '\'';
          _LOGGER.error(errMsg);
        }
      }
    }

    deploymentHeaderCode.addOutput("#define POK_CONFIG_PARTITIONS_SIZE {") ;
    idx = 0 ;
    for(Long l : memorySizePerPartition)
    {
      deploymentHeaderCode.addOutput(Long.toString(l)) ;

      if(idx != memorySizePerPartition.size() - 1)
      {
        deploymentHeaderCode.addOutput(",") ;
      }
      idx++ ;
    }

    deploymentHeaderCode.addOutputNewline("}") ;
    
    PropertyAssociation moduleSchedulePA = PropertyUtils.findPropertyAssociation
                                                ("Module_Schedule", processor);
    if(moduleSchedulePA == null)
    {
      String errMsg =  "cannot fetch Module_Schedule for \'"+
          processor.getName() + '\'' ;
      _LOGGER.error(errMsg);
    }
    else
    {
    	ModalPropertyValue mpv = moduleSchedulePA.getOwnedValues().get(0);
    	ListValue lv = (ListValue) mpv.getOwnedValue();

    	deploymentHeaderCode
    	.addOutputNewline("#define POK_CONFIG_SCHEDULING_NBSLOTS " +
    			Integer.toString(lv.getOwnedListElements().size())) ;


    	deploymentHeaderCode.addOutput("#define POK_CONFIG_SCHEDULING_SLOTS {") ;
    	idx = 0 ;
    	for(PropertyExpression pe : lv.getOwnedListElements())
    	{
    		RecordValue rv = (RecordValue) pe;
    		for(BasicPropertyAssociation bpa: rv.getOwnedFieldValues())
    		{
    			if(bpa.getProperty().getName().equalsIgnoreCase("Duration"))
    			{
    				IntegerLiteral il =  (IntegerLiteral) bpa.getValue();
    				deploymentHeaderCode.addOutput(Long.toString((int) Math.ceil(il.getScaledValue(AadlProject.NS_LITERAL)))) ;
    				if(idx != lv.getOwnedListElements().size() - 1)
    				{
    					deploymentHeaderCode.addOutput(",") ;
    				}
    				idx++ ;
    			}
    		}
    	}


    	deploymentHeaderCode.addOutputNewline("}") ;
    	idx = 0;
    	deploymentHeaderCode.addOutput("#define POK_CONFIG_SCHEDULING_SLOTS_ALLOCATION {") ;
    	for(PropertyExpression pe : lv.getOwnedListElements())
    	{
    		RecordValue rv = (RecordValue) pe;
    		for(BasicPropertyAssociation bpa: rv.getOwnedFieldValues())
    		{
    			if(bpa.getProperty().getName().equalsIgnoreCase("Partition"))
    			{
    				ReferenceValue sAllocation = (ReferenceValue) bpa.getValue();
    				int index = sAllocation.getContainmentPathElements().size()-1; 
    				NamedElement ne = sAllocation.getContainmentPathElements().get(index).getNamedElement();
    				int referencedComponentId = AadlToPokCUtils.getPartitionIndex((VirtualProcessorSubcomponent) ne, bindedVPI, traces);
    				
    				deploymentHeaderCode.addOutput(Integer.toString(referencedComponentId)) ;
    				if(idx != lv.getOwnedListElements().size() - 1)
    				{
    					deploymentHeaderCode.addOutput(",") ;
    				}
    				idx++ ;
    			}
    		}
    	}
    	deploymentHeaderCode.addOutputNewline("}") ;
    }
    Long majorFrame =
        PropertyUtils.getIntValue(processor, "Module_Major_Frame", AadlProject.NS_LITERAL) ;
    if(majorFrame != null)
    {
      deploymentHeaderCode
      .addOutputNewline("#define POK_CONFIG_SCHEDULING_MAJOR_FRAME " +
            Long.toString(majorFrame)) ;
    }
    else
    {
      String errMsg = "cannot fetch Module_Major_Frame for \'" +
                                     processor.getName() + '\'' ;
      _LOGGER.error(errMsg);
    }
    
    String portsFlushTime = PropertyUtils.getEnumValue(processor, "Ports_Flush_Time");
    if(portsFlushTime != null)
    {
      if (portsFlushTime.equalsIgnoreCase("Minor_Frame_Switch"))
      {
        Long minorFrame = PropertyUtils.getIntValue(processor, "Module_Minor_Frame", "ns");
        if(minorFrame != null)
        {
          deploymentHeaderCode
          .addOutputNewline("#define POK_FLUSH_PERIOD " + 
              Long.toString(minorFrame)) ;
        }
        else
        {
          String errMsg =  "Ports_Flush_Time was set to Minor_Frame_Switch for \'"
                  +processor.getName()+"\', but cannot fetch Module_Minor_Frame" ;
            _LOGGER.error(errMsg);
        }
      }
      else if (portsFlushTime.equalsIgnoreCase("Partition_Slot_Switch"))
        deploymentHeaderCode
          .addOutputNewline("#define POK_NEEDS_FLUSH_ON_WINDOWS 1") ;
    }
    else
    {
      String warnMsg = "Ports_Flush_Time was not set on \'"+processor.getName()
          +"\', default flush policy will be used" ;
      _LOGGER.warn(warnMsg);
    }

    for(ProcessSubcomponent ps : bindedProcess)
    {
      ProcessImplementation processImplementation =
            (ProcessImplementation) ps.getComponentImplementation() ;

      for(ThreadSubcomponent ts : processImplementation.getOwnedThreadSubcomponents())
      {
        Long partitionStack =
            PropertyUtils.getIntValue(ts, "Stack_Size", "Bytes") ;
      
        if(partitionStack != null)
        {
          _processorProp.requiredStackSize += partitionStack ;
          _processorProp.requiredStackSizePerPartition.put(processImplementation,
                                                           partitionStack) ;
        }
        else
        {
          _processorProp.requiredStackSize += DEFAULT_REQUIRED_STACK_SIZE ;
          _processorProp.requiredStackSizePerPartition.put(processImplementation,
                                                           DEFAULT_REQUIRED_STACK_SIZE) ;
          
          String warnMsg = "Set default required stack size for \'"+processImplementation.getName()
              +"\', default flush policy will be used" ;
          _LOGGER.warn(warnMsg);
        }
      }
    }

    deploymentHeaderCode.addOutputNewline("#define POK_CONFIG_STACKS_SIZE " +
          Long.toString(_processorProp.requiredStackSize)) ;
    
    // XXX is that right ???
    if(routing.buses.isEmpty())
    {
      deploymentHeaderCode.addOutputNewline("#define POK_CONFIG_NB_BUSES 0");
    }
    else
    {
      deploymentHeaderCode.addOutputNewline("#define POK_CONFIG_NB_BUSES 1");
    }

    
    boolean needErrorHandler=false,needPartitionErrorHandler=false,needKernelErrorHandler = false;
    needKernelErrorHandler = needKernelErrorHandler(processor);
    if(needKernelErrorHandler)
      needErrorHandler = true;
    
    needPartitionErrorHandler = needPartitionErrorHandler(bindedVPI);
    if(needPartitionErrorHandler)
        needErrorHandler = true;

    for(ProcessSubcomponent ps : bindedProcess)
    {
      if(needErrorHandler)
        break ;
      ProcessImplementation procImpl =
            (ProcessImplementation) ps.getSubcomponentType() ;
      for(ThreadSubcomponent ts : procImpl.getOwnedThreadSubcomponents())
      {
    	Property prop = GetProperties.lookupPropertyDefinition(ts, "ARINC653", "HM_Error_ID_Actions");
    	List<? extends PropertyExpression> peList;
    	try {
    		peList = processor.getPropertyValueList(prop);
    	} catch (Exception e) {
    		peList = Collections.emptyList();
    	}
    	if(! peList.isEmpty())
        {
          needErrorHandler = true ;
          break ;
        }
        else
        {
          //do nothing
        }
      }
    }
    
    // by default, activate error handling to include the implementation
    // of RESTART, STOP and STOP_SELF functions in pok kernel
    deploymentHeaderCode.addOutputNewline("#define POK_NEEDS_ERROR_HANDLING 1");
    if(needErrorHandler)
    {
      if(needKernelErrorHandler)
        deploymentHeaderCode.addOutputNewline("#define POK_USE_GENERATED_KERNEL_ERROR_HANDLER 1");
      if(needPartitionErrorHandler)
        deploymentHeaderCode.addOutputNewline("#define POK_USE_GENERATED_PARTITION_ERROR_HANDLER 1");
      deploymentHeaderCode.addOutputNewline("#include \"core/partition.h\"") ;
      deploymentHeaderCode.addOutputNewline("#include \"core/error.h\"") ;
      deploymentHeaderCode.addOutputNewline("#include \"core/kernel.h\"") ;
    }

    
    genDeploymentHeaderEnd(deploymentHeaderCode);
    
    deploymentHeaderCode.addOutputNewline("#endif") ;
    
  }                                            
//
//  private NamedElement getRootSystem(AadlPackage pkg)
//  {
//    for(Classifier c: pkg.getOwnedPublicSection().getOwnedClassifiers())
//      if(c instanceof SystemImplementation && isRoot(pkg, (SystemImplementation) c))
//        return c;
//    return null;
//  }

//
//  private boolean isRoot(AadlPackage pkg, SystemImplementation sys)
//  {
//    for(Classifier c: pkg.getOwnedPublicSection().getOwnedClassifiers())
//      if(c instanceof SystemImplementation)
//      {
//        SystemImplementation potentialRoot = (SystemImplementation) c;
//        for(SystemSubcomponent subSys: potentialRoot.getOwnedSystemSubcomponents())
//          if(subSys.getSubcomponentType().equals(sys))
//            return false;
//      }
//    return true;
//  }
  
  private boolean needKernelErrorHandler(Subcomponent processor)
  {
	return PropertyUtils.findPropertyAssociation("HM_Error_ID_Levels", processor) != null
	        && PropertyUtils.findPropertyAssociation("HM_Error_ID_Actions", processor) != null;
  }
  
  private boolean needPartitionErrorHandler(List<ComponentInstance> bindedVPI)
  {
    for(ComponentInstance vps: bindedVPI)
    {
      Property prop = GetProperties.lookupPropertyDefinition(vps, "ARINC653", "HM_Error_ID_Actions");
      List<? extends PropertyExpression> peList;
      try {
    	  peList = vps.getPropertyValueList(prop);
      } catch (Exception e) {
    	  peList = Collections.emptyList();
      }
      if(!peList.isEmpty())
    	  return true;
    }
    return false;
  }
  
  protected void genDeploymentHeaderEnd(UnparseText deploymentHeaderCode){}


  @Override
  public TargetProperties getSystemTargetConfiguration(EList<SystemImplementation> siList,
                                  IProgressMonitor monitor)
	     	                                             throws GenerationException
	{
	  RoutingProperties routing = new RoutingProperties();

	  for(int i=0;i<siList.size();i++)
	  {
		  ArchTraceSpec traces = getCodeGenWorkflowComponent().getTransformationResourcesPairList().get(i).getModelTransformationTrace();
		  routing.setTraces(traces);
		  SystemInstance system = (SystemInstance) 
				  traces.getTransformationTrace(siList.get(i)) ;
	      
		  routing.setRoutingProperties(system); 
	  }	  
	  return routing ;
  }
  
  private Set<FeatureInstance> getLocalPorts(ComponentInstance processor,
		                                          RoutingProperties routeProp)
	                                                    throws GenerationException
  {
    Set<FeatureInstance> localPorts = new LinkedHashSet<FeatureInstance>();
    if(routeProp.processPerProcessor.get(processor) == null
    		|| routeProp.processPerProcessor.get(processor).isEmpty())
      return localPorts;
    for(ComponentInstance deployedProcess:routeProp.processPerProcessor.get(processor))
    {
      for(FeatureInstance fi:routeProp.getProcessPorts(deployedProcess))
    	  if(routeProp.globalPort.contains(fi))
    		  localPorts.add(fi);
    }
    return localPorts;
  }
  
  private List<ComponentInstance> getProcessorList(ComponentInstance cInst)
  {
	  SystemInstance si = cInst.getSystemInstance();
	  while(si.eContainer()!=null)
		  si = si.getSystemInstance();
	  SystemInstance systemIter = si;
	  while(systemIter != null)
	  {
		  SystemImplementation sysImplIter = (SystemImplementation) systemIter.getComponentImplementation();
		  ArchTraceSpec tracesInter = ArchTraceSourceRetreival.getTracesFromNamedElement(sysImplIter);
		  if(tracesInter != null)
		  {
			  systemIter = (SystemInstance) 
					  tracesInter.getTransformationTrace(sysImplIter) ;
		  }
		  else
			  systemIter = null;
		  if(systemIter!=null)
			  si = systemIter;  
	  }
	  List<ComponentInstance> processorList = new ArrayList<ComponentInstance>();
	  for(ComponentInstance ci:EcoreUtil2.getAllContentsOfType(si, ComponentInstance.class))
	  {
		  if(GenerationUtilsC.isProcessor(ci) || ci.getCategory().equals(ComponentCategory.PROCESSOR))
			  processorList.add(ci);
	  }
	  return processorList;
  }
  
  private void genRoutingHeader(ComponentInstance processor,
                                UnparseText routingHeaderCode,
                                RoutingProperties routeProp, ArchTraceSpec traces)
                                                      throws GenerationException
  {
	  List<ComponentInstance> processorList = getProcessorList(processor);
	  
	String guard = GenerationUtilsC.generateHeaderInclusionGuard("routing.h") ;
	routingHeaderCode.addOutput(guard);
	
//	if(routeProp.processPerProcessor.get(processor)!=null)
//	{
	  int globalPortNb = routeProp.globalPort.size() +
	      routeProp.globalVirtualPort.size();
	  routingHeaderCode.addOutputNewline("#define POK_CONFIG_NB_GLOBAL_PORTS " +
			  Integer.toString(globalPortNb));
		
	  Set<FeatureInstance> localPorts = getLocalPorts(processor, routeProp);
	  Set<VirtualPort> localVirtualPorts = routeProp.virtualPortLimitedToProcessor(processor);
	  int localPortNb = localPorts.size()
			  + localVirtualPorts.size();
	  long maxMsgSize=0;

	  int nbInputVirtualPort=0;
	  for(VirtualPort vp: localVirtualPorts)
	  {
		  boolean counted = false;
		  for(FeatureInstance fi: vp.getProcess().getAllFeatureInstances())
		  {
			  if(!routeProp.virtualPortPerPort.containsKey(fi))
				  continue;
			  if(!counted &&
					  AadlHelperImpl.isPort(fi) && 
					  AadlHelperImpl.isInputFeature(fi))
			  {
				  nbInputVirtualPort++;
				  counted = true;
			  }
			  long featureDataSize = RoutingProperties.getFeatureDataSize(fi);
			  if(featureDataSize>maxMsgSize)
				  maxMsgSize = featureDataSize;
			  if(featureDataSize==0 && AadlHelperImpl.isPort(fi) && !fi.getCategory().equals(FeatureCategory.EVENT_PORT))
				  _LOGGER.warn("Did not find data size definition for " + fi.getFullName() + ". Needs to be added using data_size property");
		  }
	  }

	  if(nbInputVirtualPort == 0)
		  nbInputVirtualPort = 1; // avoid compilation error in POK for division by 0
	  routingHeaderCode.addOutputNewline("#define POK_CONFIG_NB_INPUT_VIRTUALPORT "+nbInputVirtualPort);

	  routingHeaderCode.addOutputNewline("#define POK_CONFIG_NB_PORTS " +
			  Integer.toString(localPortNb));

	  routingHeaderCode.addOutputNewline("#define POK_CONFIG_NB_NODES " +
			  Integer.toString(processorList.size())) ;
	  
	  if(maxMsgSize>0)
	  {
		  /* WARNING: this copies the content of aadl_runtime_services.h
		   * Did not find a better way to proceed yet
		   * It MUST be kept consistent (in case of modification of struct message or FRAG_SIZE definition)*/
		  routingHeaderCode.addOutputNewline("// This is a copy of macro from aadl_runtime_services.h");
		  routingHeaderCode.addOutputNewline("#define FRAG_SIZE 30");
		  routingHeaderCode.addOutputNewline("");
		  
		  routingHeaderCode.addOutputNewline("// This is a copy of struct definition from aadl_runtime_services.h");
		  routingHeaderCode.addOutputNewline("struct message");
		  routingHeaderCode.addOutputNewline("{");
		  routingHeaderCode.addOutputNewline("  int aadl_port_nb; // global port port id");
		  routingHeaderCode.addOutputNewline("  int size;");
		  routingHeaderCode.addOutputNewline("  char data[FRAG_SIZE];");
		  routingHeaderCode.addOutputNewline("};");
		  routingHeaderCode.addOutputNewline("");
		  
		  routingHeaderCode.addOutputNewline("#define MAX_MSG_SIZE "+maxMsgSize);
		  
		  routingHeaderCode.addOutputNewline("#if FRAG_SIZE > MAX_MSG_SIZE");
		  routingHeaderCode.addOutputNewline("#define NB_MSG 1");
		  routingHeaderCode.addOutputNewline("#else");
		  routingHeaderCode.addOutputNewline("#define NB_MSG MAX_MSG_SIZE/FRAG_SIZE");
		  routingHeaderCode.addOutputNewline("#endif");
		  
		  routingHeaderCode.addOutputNewline("#define RECV_BUF_SZ (50*(POK_CONFIG_NB_INPUT_VIRTUALPORT*(NB_MSG + 1)*sizeof(struct message)))");
	  }

	  Set<ComponentInstance> bindedPorcesses = routeProp.processPerProcessor.get(processor);
	  
	  if(!localPorts.isEmpty() || !localVirtualPorts.isEmpty())
      {
	    routingHeaderCode.addOutput("#define POK_CONFIG_PARTITIONS_PORTS {");
	    generateVirtualPortsToPartitionsArray(localVirtualPorts, routingHeaderCode, bindedPorcesses, routeProp, traces);
	    generateLocalPortsToPartitionsArray(localPorts, routingHeaderCode, bindedPorcesses, traces);
	    routingHeaderCode.addOutputNewline("}");
      }


	  routingHeaderCode.addOutputNewline("typedef enum") ;
	  routingHeaderCode.addOutputNewline("{") ;
	  routingHeaderCode.incrementIndent() ;
	  
	  int idx=0;
	  
	  for(ComponentInstance node : processorList)
	  {
	    routingHeaderCode.addOutput(AadlToPokCUtils.getComponentInstanceIdentifier(node)) ;
	    routingHeaderCode.addOutput(" = "+Integer.toString(idx));
	    routingHeaderCode.addOutputNewline(",") ;
	    idx++;
	  }
	  routingHeaderCode.decrementIndent() ;
	  routingHeaderCode.addOutputNewline("} pok_node_identifier_t;") ;

	  idx=0;
	  routingHeaderCode.addOutputNewline("typedef enum") ;
	  routingHeaderCode.addOutputNewline("{") ;
	  routingHeaderCode.incrementIndent() ;
	  // Virtual port first
	  // input first
	  // Objective: correct usage of buffers and indexes to be contiguous and bound by POK_CONFIG_NB_INPUT_VIRTUALPORT
	  List<VirtualPort> declaredVirtualPortIdentifiers  = new ArrayList<RoutingProperties.VirtualPort>();
	  outerloop:
	  for(VirtualPort lvp: localVirtualPorts)
      {
		  for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
		  {
			  if(declaredVirtualPortIdentifiers.contains(lvp))
				  continue outerloop;
			  if(!routeProp.virtualPortPerPort.containsKey(fi))
				  continue;
			  if(AadlHelperImpl.isPort(fi) && 
					  AadlHelperImpl.isInputFeature(fi))
			  {
				  declaredVirtualPortIdentifiers.add(lvp);
				  routingHeaderCode.addOutput(getVirtualPortLocalCIdentifier(lvp,VIRTUAL_PORT_SUFFIX));
				  routingHeaderCode.addOutput(" = "+Integer.toString(idx));
				  routingHeaderCode.addOutputNewline(",") ;
				  idx++;
			  }
		  }
      }
	  outerloop:
	  for(VirtualPort lvp: localVirtualPorts)
      {
		  for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
		  {
			  if(declaredVirtualPortIdentifiers.contains(lvp))
				  continue outerloop;
			  if(!routeProp.virtualPortPerPort.containsKey(fi))
				  continue;
			  if(AadlHelperImpl.isPort(fi) && 
					  !AadlHelperImpl.isInputFeature(fi))
			  {
				  declaredVirtualPortIdentifiers.add(lvp);
				  routingHeaderCode.addOutput(getVirtualPortLocalCIdentifier(lvp,VIRTUAL_PORT_SUFFIX));
				  routingHeaderCode.addOutput(" = "+Integer.toString(idx));
				  routingHeaderCode.addOutputNewline(",") ;
				  idx++;
			  }
		  }
      }
	  
	  for(FeatureInstance fi: localPorts)
	  {
	    routingHeaderCode.addOutput(AadlToPokCUtils.getFeatureLocalIdentifier(fi));
	    routingHeaderCode.addOutput(" = "+Integer.toString(idx));
	    routingHeaderCode.addOutputNewline(",") ;
	    idx++;
	  }
	  routingHeaderCode.addOutput("invalid_local_port");
	  routingHeaderCode.addOutputNewline(" = "+Integer.toString(idx));
	  routingHeaderCode.decrementIndent() ;
	  routingHeaderCode.addOutputNewline("} pok_port_local_identifier_t;") ;

	  if(routeProp.globalVirtualPort.isEmpty()==false
			  || routeProp.globalPort.isEmpty()==false)
	  {
		  idx=0;
		  routingHeaderCode.addOutputNewline("typedef enum") ;
		  routingHeaderCode.addOutputNewline("{") ;
		  routingHeaderCode.incrementIndent() ;
		  
		  declaredVirtualPortIdentifiers.clear();
		  outerloop:
		  for(VirtualPort vp: routeProp.globalVirtualPort)
		  {
			  for(FeatureInstance fi: vp.getProcess().getAllFeatureInstances())
			  {
				  if(declaredVirtualPortIdentifiers.contains(vp))
					  continue outerloop;
				  if(!routeProp.virtualPortPerPort.containsKey(fi))
					  continue;
				  declaredVirtualPortIdentifiers.add(vp);
				  routingHeaderCode.addOutput(getVirtualPortGlobalCIdentifier(vp, VIRTUAL_PORT_SUFFIX));
				  routingHeaderCode.addOutput(" = "+Integer.toString(idx));
				  routingHeaderCode.addOutputNewline(",") ;
				  idx++;
				  break;
			  }
		  }


		  for(FeatureInstance fi: routeProp.globalPort)
		  {
			  routingHeaderCode.addOutput(AadlToPokCUtils.getFeatureGlobalIdentifier(fi));
			  routingHeaderCode.addOutput(" = "+Integer.toString(idx));
			  routingHeaderCode.addOutputNewline(",") ;
			  idx++;
		  }

		  routingHeaderCode.decrementIndent() ;
		  routingHeaderCode.addOutputNewline("} pok_port_identifier_t;") ;
	  }
	  idx=0;
	  routingHeaderCode.addOutputNewline("typedef enum") ;
	  routingHeaderCode.addOutputNewline("{") ;
	  routingHeaderCode.incrementIndent() ;
	  for(ComponentInstance bus:routeProp.buses)
	  {
		  routingHeaderCode.addOutput(AadlToPokCUtils.getComponentInstanceIdentifier(bus));
		  routingHeaderCode.addOutput(" = "+Integer.toString(idx));
		  routingHeaderCode.addOutputNewline(",") ;
		  idx++;
	  }
	  routingHeaderCode.addOutputNewline("invalid_bus = "+Integer.toString(idx)) ;
	  routingHeaderCode.decrementIndent() ;
	  routingHeaderCode.addOutputNewline("} pok_bus_identifier_t;") ;  


	  routingHeaderCode.addOutputNewline("#endif");
  }

  
  
  private
      void
      generateVirtualPortsToPartitionsArray(Set<VirtualPort> processorVirtualPorts,
                                            UnparseText routingHeaderCode,
                                            Set<ComponentInstance> bindedPorcesses,
                                            RoutingProperties routeProp,
                                            ArchTraceSpec traces)
  {
	  List<VirtualPort> declaredVirtualPortIdentifiers = new ArrayList<RoutingProperties.VirtualPort>();
	  outerloop:
	  for(VirtualPort lvp: processorVirtualPorts)
      {
		  for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
		  {
			  if(declaredVirtualPortIdentifiers.contains(lvp))
		    		continue outerloop;
			  if(!routeProp.virtualPortPerPort.containsKey(fi))
				  continue;
			  if(AadlHelperImpl.isPort(fi) && 
					  AadlHelperImpl.isInputFeature(fi))
			  {
				  declaredVirtualPortIdentifiers.add(lvp);
				  generateVirtualPortsToPartitionsArray(lvp, routingHeaderCode, bindedPorcesses, routeProp, traces);
			  }
		  }
      }
	  
	  outerloop:
	  for(VirtualPort lvp: processorVirtualPorts)
      {
		  for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
		  {
			  if(declaredVirtualPortIdentifiers.contains(lvp))
		    		continue outerloop;
			  if(!routeProp.virtualPortPerPort.containsKey(fi))
				  continue;
			  if(AadlHelperImpl.isPort(fi) && 
					  false == AadlHelperImpl.isInputFeature(fi))
			  {
				  declaredVirtualPortIdentifiers.add(lvp);
				  generateVirtualPortsToPartitionsArray(lvp, routingHeaderCode, bindedPorcesses, routeProp, traces);
			  }
		  }
      }
  }

  private
  void
  generateVirtualPortsToPartitionsArray(VirtualPort lvp,
                                        UnparseText routingHeaderCode,
                                        Set<ComponentInstance> bindedPorcesses,
                                        RoutingProperties routeProp,
                                        ArchTraceSpec traces)
  {
	  int foundIdx=-1;
	  	  
      for(ComponentInstance process: bindedPorcesses)
      {
    	  List<ComponentInstance> bindedVPI = AadlToARINC653Utils.getBindedVPI(AadlToARINC653Utils.getProcessor(process));

//    	  Subcomponent sub = process.getSubcomponent();
//    	  ArchTraceSpec traces = ArchTraceSourceRetreival.getTracesFromNamedElement(sub);
    	  ComponentInstance processInSourceModel = (ComponentInstance) traces.getTransformationTrace(process.getSubcomponent());
    	  if(processInSourceModel.equals(lvp.getProcess()) || AadlToPokCUtils.isSystemPartition(process))
    	  {
    		  foundIdx = AadlToPokCUtils.getPartitionIndex((VirtualProcessorSubcomponent) AadlToARINC653Utils.getPartition(process).getSubcomponent(), bindedVPI, traces);
//    		  foundIdx = bindedVPI.indexOf(getPartition(process)) ;
    		  break;
    	  }
      }
      if(foundIdx>=0)
    	  routingHeaderCode.addOutput(Integer.toString(foundIdx));
      else
    	  routingHeaderCode.addOutput(Integer.toString(-1));
      routingHeaderCode.addOutput(",");  
  }
  

  private void generateLocalPortsToPartitionsArray(Set<FeatureInstance> localPorts,
                                        UnparseText routingHeaderCode,
                                        Set<ComponentInstance> bindedPorcesses,
                                        ArchTraceSpec traces)
  {
    for(FeatureInstance fi: localPorts)
    {	
      ComponentInstance processInstance = (ComponentInstance) fi.getComponentInstance();
      List<ComponentInstance> bindedVPI = AadlToARINC653Utils.getBindedVPI(AadlToARINC653Utils.getProcessor(processInstance));
      if(processInstance.getCategory() == ComponentCategory.PROCESS)
      {
    	int referencedComponentId = AadlToPokCUtils.getPartitionIndex((VirtualProcessorSubcomponent) AadlToARINC653Utils.getPartition(processInstance).getSubcomponent(), bindedVPI, traces);
        if(referencedComponentId==-1)
        {
        	 int partitionIndex = bindedVPI.indexOf(AadlToPokCUtils.getPartition(processInstance));
             routingHeaderCode.addOutput(Integer.toString(partitionIndex));
        }
        else
        	routingHeaderCode.addOutput(Integer.toString(referencedComponentId));
      }
      routingHeaderCode.addOutput(",");
    }
  }

  private void genRoutingImpl(ComponentInstance processor,
                              UnparseText routingImplCode,
                              RoutingProperties routeProp)
                                                      throws GenerationException
  {
    routingImplCode.addOutputNewline("#include \"routing.h\"") ;
    routingImplCode.addOutputNewline("#include \"middleware/port.h\"") ;
    routingImplCode.addOutputNewline("#include <types.h>") ;

    Set<FeatureInstance> localPorts = getLocalPorts(processor, routeProp);
    Set<VirtualPort> localVirtualPorts = routeProp.virtualPortLimitedToProcessor(processor);

    boolean hasSystemPartition = false;
    for(ComponentInstance deployedProcess : routeProp.processPerProcessor.get(processor))
    {
    	if(AadlToPokCUtils.isSystemPartition(deployedProcess))
    		hasSystemPartition=true;
    	break;
    }
    for(ComponentInstance deployedProcess:routeProp.processPerProcessor.get(processor))
    {
    	String init = "";

    	String processName = deployedProcess.getSubcomponent().getName();
    	int nbPorts = routeProp.getProcessPorts(deployedProcess).size();

    	for(FeatureInstance fi : routeProp.getProcessPorts(deployedProcess))
    	{
    		if(!localPorts.contains(fi))
    			continue;
    		init+=AadlToPokCUtils.getFeatureLocalIdentifier(fi);
    		init+=",";
    	}

    	if(hasSystemPartition)
    	{
    		// compute list of ports for each partition deployed on "processor"
    		if(AadlToPokCUtils.isSystemPartition(deployedProcess))
    			nbPorts+=routeProp.virtualPortLimitedToProcessor(processor).size();

    		if(AadlToPokCUtils.isSystemPartition(deployedProcess))
    		{
    			for(VirtualPort vp : routeProp.virtualPortLimitedToProcessor(processor))
    			{
    				if(!localVirtualPorts.contains(vp))
    					continue;
    				init+=getVirtualPortLocalCIdentifier(vp, VIRTUAL_PORT_SUFFIX);
    				init+=",";
    			}
    		}
    	}
    	else
    	{
    		Subcomponent sub = deployedProcess.getSubcomponent();
    		ArchTraceSpec traces = ArchTraceSourceRetreival.getTracesFromNamedElement(sub);
    		if(traces!=null)
    		{
    			ComponentInstance processInSrcModel = (ComponentInstance) 
    					traces.getTransformationTrace(sub) ;

    			// compute list of ports for each partition deployed on "processor"
    			nbPorts += routeProp.virtualPortLimitedToProcess(processInSrcModel).size();

    			for(VirtualPort vp : routeProp.virtualPortLimitedToProcess(processInSrcModel))
    			{
    				if(!localVirtualPorts.contains(vp))
    					continue;
    				init+=getVirtualPortLocalCIdentifier(vp, VIRTUAL_PORT_SUFFIX);
    				init+=",";
    			}
    		}
    	}
    	routingImplCode.addOutput("uint8_t ");
    	routingImplCode.addOutput(processName+"_partport["+Integer.toString(nbPorts)
    	+"] = {" );
    	routingImplCode.addOutput(init);
    	routingImplCode.addOutputNewline("};");
    	// compute list of destination ports for each port of partitions deployed on "processor"
    	for(FeatureInstance fi : routeProp.getProcessPorts(deployedProcess))
    	{
    		generateDestinationInit(fi, routingImplCode, routeProp, "");
    	}
    }
    for(VirtualPort vp : routeProp.virtualPortLimitedToProcessor(processor))
    {
    	generateDestinationInit(vp, routingImplCode, routeProp, VIRTUAL_PORT_SUFFIX);
    }
    routingImplCode.addOutput("uint8_t pok_global_ports_to_local_ports" +
        "[POK_CONFIG_NB_GLOBAL_PORTS] = {");
    
    List<VirtualPort> declaredVirtualPortIdentifiers  = new ArrayList<RoutingProperties.VirtualPort>();
    outerloop:
    for(VirtualPort vp: routeProp.globalVirtualPort)
    {
    	for(FeatureInstance fi: vp.getProcess().getAllFeatureInstances())
    	{
        	if(declaredVirtualPortIdentifiers.contains(vp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		declaredVirtualPortIdentifiers.add(vp);
    		if(localVirtualPorts.contains(vp))
    			routingImplCode.addOutput(getVirtualPortLocalCIdentifier(vp, VIRTUAL_PORT_SUFFIX));
    		else
    			routingImplCode.addOutput("invalid_local_port");

    		routingImplCode.addOutput(",");

    		break;
    	}
    }

    for(FeatureInstance fi:routeProp.globalPort)
    {
      if(localPorts.contains(fi))
        routingImplCode.addOutput(AadlToPokCUtils.getFeatureLocalIdentifier(fi));
      else
        routingImplCode.addOutput("invalid_local_port");

      routingImplCode.addOutput(",");
    }
    routingImplCode.addOutputNewline("};");

    routingImplCode.addOutput("uint8_t pok_global_ports_to_bus" +
        "[POK_CONFIG_NB_GLOBAL_PORTS] = {");
    
    declaredVirtualPortIdentifiers.clear();
    outerloop:
    for(VirtualPort vp: routeProp.globalVirtualPort)
    {
    	for(FeatureInstance fi: vp.getProcess().getAllFeatureInstances())
    	{
        	if(declaredVirtualPortIdentifiers.contains(vp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		declaredVirtualPortIdentifiers.add(vp);
    		FeatureInstance ba = vp.getBusAccess();
    		ComponentInstance inst ;
    		if(!ba.getSrcConnectionInstances().isEmpty())
    			inst = (ComponentInstance) ba.getSrcConnectionInstances().get(0).getDestination();
    		else
    			inst = (ComponentInstance) ba.getDstConnectionInstances().get(0).getSource();
    		routingImplCode.addOutput(AadlToPokCUtils.getComponentInstanceIdentifier(inst));
    		routingImplCode.addOutput(",");
    		break;
    	}
    }
    
    for(@SuppressWarnings("unused") FeatureInstance fi:routeProp.globalPort)
    {
      routingImplCode.addOutput("invalid_bus");
      routingImplCode.addOutput(",");
    }
    routingImplCode.addOutputNewline("};");
    
    
    routingImplCode.addOutput("uint8_t pok_local_ports_to_global_ports" +
        "[POK_CONFIG_NB_PORTS] = {");
    
    declaredVirtualPortIdentifiers.clear();
    outerloop:
    for(VirtualPort lvp: localVirtualPorts)
    {
    	for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
    	{
    		if(declaredVirtualPortIdentifiers.contains(lvp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		if(AadlHelperImpl.isPort(fi) && 
    				AadlHelperImpl.isInputFeature(fi))
    		{
    			declaredVirtualPortIdentifiers.add(lvp);
    			routingImplCode.addOutput(getVirtualPortGlobalCIdentifier(lvp, VIRTUAL_PORT_SUFFIX));
    			routingImplCode.addOutput(",");
    		}
    	}
    }
    outerloop:
    for(VirtualPort lvp: localVirtualPorts)
    {
    	for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
    	{
        	if(declaredVirtualPortIdentifiers.contains(lvp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		if(AadlHelperImpl.isPort(fi) && 
    				!AadlHelperImpl.isInputFeature(fi))
    		{
    			declaredVirtualPortIdentifiers.add(lvp);
    			routingImplCode.addOutput(getVirtualPortGlobalCIdentifier(lvp, VIRTUAL_PORT_SUFFIX));
    			routingImplCode.addOutput(",");
    		}
    	}
    }

    for(FeatureInstance fi:localPorts)
    {
        routingImplCode.addOutput(AadlToPokCUtils.getFeatureGlobalIdentifier(fi));
        routingImplCode.addOutput(",");
    }
    routingImplCode.addOutputNewline("};");

    routingImplCode.addOutput("uint8_t pok_ports_nodes" +
        "[POK_CONFIG_NB_GLOBAL_PORTS] = {");
    
    declaredVirtualPortIdentifiers.clear();
    outerloop:
    for(VirtualPort vp: routeProp.globalVirtualPort)
    {
    	for(FeatureInstance fi: vp.getProcess().getAllFeatureInstances())
    	{
        	if(declaredVirtualPortIdentifiers.contains(vp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		declaredVirtualPortIdentifiers.add(vp);
    		ComponentInstance inst = routeProp.processorVirtualPort.get(vp);
    		routingImplCode.addOutput(AadlToPokCUtils.getComponentInstanceIdentifier(inst));
    		routingImplCode.addOutput(",");

    		break;
    	}
    }
    
    for(FeatureInstance fi : routeProp.globalPort)
    {
      ComponentInstance inst = routeProp.processorPort.get(fi);
      String id = AadlToPokCUtils.getComponentInstanceIdentifier(inst);
      routingImplCode.addOutput(id);
      routingImplCode.addOutput(",");
    }
    routingImplCode.addOutputNewline("};");

    List<ComponentInstance> processorList = getProcessorList(processor);
    for(ComponentInstance processorInst: processorList)
    { 
        String hwAddr = PropertyUtils.getStringValue(processorInst, "Address");
        if(hwAddr==null)
        	hwAddr="FF:FF:FF:FF:FF:FF";
        
        String[] macAddr = hwAddr.split(":");
        routingImplCode.addOutput("char ");
        routingImplCode.addOutput(AadlToPokCUtils.getComponentInstanceIdentifier(processorInst)+"_mac_addr");
        routingImplCode.addOutput("[6] = {");
        int i=0;
        for(String s: macAddr)
        {
        	i++;
        	routingImplCode.addOutput("0x"+s);
        	if(i==6)
        		break;
        	routingImplCode.addOutput(",");
        }
        routingImplCode.addOutputNewline("};");
    }
    routingImplCode.addOutput("char * pok_nodes_addr" +
            "[POK_CONFIG_NB_NODES] = {");
    int i=0;
    for(ComponentInstance processorInst: processorList)
    {
    	routingImplCode.addOutput(AadlToPokCUtils.getComponentInstanceIdentifier(processorInst)+"_mac_addr");
    	i++;
    	if(processorList.size()==i)
    		break;
    	routingImplCode.addOutput(",");
    }
    routingImplCode.addOutputNewline("};");
    
    routingImplCode.addOutput("uint8_t pok_ports_nb_ports_by_partition" +
    		"[POK_CONFIG_NB_PARTITIONS] = {");
    
    if(routeProp.processPerProcessor.get(processor)!=null)
    {
    	for(ComponentInstance deployedProcess:routeProp.processPerProcessor.get(processor))
    	{
    		int nbPort = routeProp.getProcessPorts(deployedProcess).size();
    		int nbVirtualPort=0;
    		if(hasSystemPartition)
    		{
    			if(AadlToPokCUtils.isSystemPartition(deployedProcess))
    				nbVirtualPort=routeProp.virtualPortLimitedToProcessor(processor).size();
    		}
    		else
    		{
    			Subcomponent sub = deployedProcess.getSubcomponent();
    			ArchTraceSpec traces = ArchTraceSourceRetreival.getTracesFromNamedElement(sub);
    			if(traces!=null)
    			{
    				ComponentInstance processInSrcModel = (ComponentInstance) 
    						traces.getTransformationTrace(sub) ;
    				nbVirtualPort=routeProp.virtualPortLimitedToProcess(processInSrcModel).size();
    			}
    		}
    		routingImplCode.addOutput(Integer.toString(nbPort+nbVirtualPort));
			routingImplCode.addOutput(",");
    	}
    }
    
    routingImplCode.addOutputNewline("};");

    routingImplCode.addOutput("uint8_t* pok_ports_by_partition" +
        "[POK_CONFIG_NB_PARTITIONS] = {");
    if(routeProp.processPerProcessor.get(processor)!=null)
    {
    	for(ComponentInstance deployedProcess:routeProp.processPerProcessor.get(processor))
    	{
    		routingImplCode.addOutput(deployedProcess.getSubcomponent().getName()
    				+"_partport");
    		routingImplCode.addOutput(",");
    	}
    }
    routingImplCode.addOutputNewline("};");
    int max_name_length = 0;
    routingImplCode.addOutput("char* pok_ports_names" +
        "[POK_CONFIG_NB_PORTS] = {");
    
    declaredVirtualPortIdentifiers.clear();
    outerloop:
    for(VirtualPort lvp: localVirtualPorts)
    {
    	for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
    	{
        	if(declaredVirtualPortIdentifiers.contains(lvp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		if(AadlHelperImpl.isPort(fi) && 
    				AadlHelperImpl.isInputFeature(fi))
    		{
    			declaredVirtualPortIdentifiers.add(lvp);
    			String name = getVirtualPortLocalCIdentifier(lvp, VIRTUAL_PORT_SUFFIX);  
    			routingImplCode.addOutput("\""+
    					name
    					+"\"");
    			int length = name.length()+1;
    			if(length>max_name_length)
    				max_name_length = length;
    			routingImplCode.addOutput(",");
    		}
    	}
    }
    
    outerloop:
    for(VirtualPort lvp: localVirtualPorts)
    {
    	for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
    	{
    		if(declaredVirtualPortIdentifiers.contains(lvp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		if(AadlHelperImpl.isPort(fi) && 
    				false==AadlHelperImpl.isInputFeature(fi))
    		{
    			declaredVirtualPortIdentifiers.add(lvp);
    			String name = getVirtualPortLocalCIdentifier(lvp, VIRTUAL_PORT_SUFFIX);  
    			routingImplCode.addOutput("\""+
    					name
    					+"\"");
    			int length = name.length()+1;
    			if(length>max_name_length)
    				max_name_length = length;
    			routingImplCode.addOutput(",");
    		}
    	}
    }
        
    for(FeatureInstance fi: localPorts)
    {
      String name = AadlToPokCUtils.getFeatureLocalIdentifier(fi);
      routingImplCode.addOutput("\""+
          name
          +"\"");
      int length = name.length()+1;
      if(length>max_name_length)
    	  max_name_length = length;
      routingImplCode.addOutput(",");
    }
    routingImplCode.addOutputNewline("};");

    routingImplCode.addOutputNewline("uint32_t pok_ports_names_max_len = "+max_name_length+";") ;

    
    routingImplCode.addOutput("uint8_t pok_ports_identifiers" +
        "[POK_CONFIG_NB_PORTS] = {");
    
    declaredVirtualPortIdentifiers.clear();
    outerloop:
    for(VirtualPort lvp: localVirtualPorts)
    {
    	for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
    	{
        	if(declaredVirtualPortIdentifiers.contains(lvp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		if(AadlHelperImpl.isPort(fi) && 
    				AadlHelperImpl.isInputFeature(fi))
    		{
    			declaredVirtualPortIdentifiers.add(lvp);
    			routingImplCode.addOutput(getVirtualPortLocalCIdentifier(lvp, VIRTUAL_PORT_SUFFIX));
    			routingImplCode.addOutput(",");
    		}
    	}
    }
    
    outerloop:
    for(VirtualPort lvp: localVirtualPorts)
    {
    	for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
    	{
        	if(declaredVirtualPortIdentifiers.contains(lvp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		if(AadlHelperImpl.isPort(fi) && 
    				!AadlHelperImpl.isInputFeature(fi))
    		{
    			declaredVirtualPortIdentifiers.add(lvp);
    			routingImplCode.addOutput(getVirtualPortLocalCIdentifier(lvp, VIRTUAL_PORT_SUFFIX));
    			routingImplCode.addOutput(",");
    		}
    	}
    }
        
    for(FeatureInstance fi: localPorts)
    {
      routingImplCode.addOutput(""+
          AadlToPokCUtils.getFeatureLocalIdentifier(fi)
          +"");
      routingImplCode.addOutput(",");
    }
    routingImplCode.addOutputNewline("};");

    routingImplCode.addOutput("uint8_t pok_ports_nb_destinations" +
        "[POK_CONFIG_NB_PORTS] = {");
    declaredVirtualPortIdentifiers.clear();
    outerloop:
    for(VirtualPort lvp: localVirtualPorts)
    {
    	for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
    	{
        	if(declaredVirtualPortIdentifiers.contains(lvp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		if(AadlHelperImpl.isPort(fi) && 
    				AadlHelperImpl.isInputFeature(fi))
    		{
    			declaredVirtualPortIdentifiers.add(lvp);
    			if(lvp.getDestinationProcess()!=null)
    				routingImplCode.addOutput("1,");
    			else
    				routingImplCode.addOutput("0,");
    		}
    	}
    }
    outerloop:
    for(VirtualPort lvp: localVirtualPorts)
    {
    	for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
    	{
        	if(declaredVirtualPortIdentifiers.contains(lvp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		if(AadlHelperImpl.isPort(fi) && 
    				!AadlHelperImpl.isInputFeature(fi))
    		{
    			declaredVirtualPortIdentifiers.add(lvp);
    			if(lvp.getDestinationProcess()!=null)
    				routingImplCode.addOutput("1,");
    			else
    				routingImplCode.addOutput("0,");
    		}
    	}
    }
    
    for(FeatureInstance fi: localPorts)
    {
      int destNb = RoutingProperties.getFeatureDestinations(fi).size();
      routingImplCode.addOutput(Integer.toString(destNb));
      routingImplCode.addOutput(",");
    }
    routingImplCode.addOutputNewline("};");

    routingImplCode.addOutput("uint8_t* pok_ports_destinations" +
        "[POK_CONFIG_NB_PORTS] = {");
    declaredVirtualPortIdentifiers.clear();
    outerloop:
    for(VirtualPort lvp: localVirtualPorts)
    {
    	for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
    	{
        	if(declaredVirtualPortIdentifiers.contains(lvp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		if(AadlHelperImpl.isPort(fi) && 
    				AadlHelperImpl.isInputFeature(fi))
    		{
    			declaredVirtualPortIdentifiers.add(lvp);
    			if(lvp.getDestinationProcess() == null)
    				routingImplCode.addOutput("NULL");
    			else
    				routingImplCode.addOutput(getVirtualPortLocalCIdentifier(lvp, VIRTUAL_PORT_SUFFIX
    						+"_deployment_destinations"));
    			routingImplCode.addOutput(",");
    		}
    	}
    }
    outerloop:
    for(VirtualPort lvp: localVirtualPorts)
    {
    	for(FeatureInstance fi: routeProp.portsPerVirtualPort.get(lvp))
    	{
        	if(declaredVirtualPortIdentifiers.contains(lvp))
        		continue outerloop;
    		if(!routeProp.virtualPortPerPort.containsKey(fi))
    			continue;
    		if(AadlHelperImpl.isPort(fi) && 
    				!AadlHelperImpl.isInputFeature(fi))
    		{
    			declaredVirtualPortIdentifiers.add(lvp);
    			if(lvp.getDestinationProcess() == null)
    				routingImplCode.addOutput("NULL");
    			else
    				routingImplCode.addOutput(getVirtualPortLocalCIdentifier(lvp, VIRTUAL_PORT_SUFFIX
    						+"_deployment_destinations"));
    			routingImplCode.addOutput(",");
    		}
    	}
    }
    
    for(FeatureInstance fi: localPorts)
    {
      int destNb = RoutingProperties.getFeatureDestinations(fi).size();
      if(destNb==0)
        routingImplCode.addOutput("NULL");
      else
        routingImplCode.addOutput(AadlToPokCUtils.getFeatureLocalIdentifier(fi)
                                  +"_deployment_destinations");
      routingImplCode.addOutput(",");
    }
    routingImplCode.addOutputNewline("};");

    routingImplCode.addOutput("uint8_t pok_ports_kind" +
        "[POK_CONFIG_NB_PORTS] = {");
    for(@SuppressWarnings("unused") VirtualPort vp: localVirtualPorts)
    {
      routingImplCode.addOutput("POK_PORT_KIND_VIRTUAL");
      routingImplCode.addOutput(",");
    }
    for(FeatureInstance fi: localPorts)
    {
      if(fi.getCategory().equals(FeatureCategory.DATA_PORT))
        routingImplCode.addOutput("POK_PORT_KIND_SAMPLING");
      if(fi.getCategory().equals(FeatureCategory.EVENT_DATA_PORT)
          || fi.getCategory().equals(FeatureCategory.EVENT_PORT))
        routingImplCode.addOutput("POK_PORT_KIND_QUEUEING");
      routingImplCode.addOutput(",");
    }
    routingImplCode.addOutputNewline("};");
    


  }
  
  private void generateDestinationInit(VirtualPort vp, UnparseText routingImplCode, RoutingProperties routeProp,
		  String virtualPortSuffix) {
	  
	  Set<VirtualPort> destinations = routeProp.getFeatureDestinations(vp);
      
	  routingImplCode.addOutput("uint8_t ");
      routingImplCode.addOutput(getVirtualPortLocalCIdentifier(vp, VIRTUAL_PORT_SUFFIX
              +"_deployment_destinations"));
      routingImplCode.addOutput("["+Integer.toString(destinations.size())+"] = {");
      for(VirtualPort dst:destinations)
      {
    	  routingImplCode.addOutput(AadlToPokCUtils.getFeatureGlobalIdentifier(dst)+
    			  virtualPortSuffix);
    	  routingImplCode.addOutput(",");
      }
	  
	  routingImplCode.addOutputNewline("};");   
  }


  private void generateDestinationInit(FeatureInstance fi,
                                       UnparseText routingImplCode,
                                       RoutingProperties routeProp,
                                       String portSuffix)
  {
    if(fi.getDirection().equals(DirectionType.OUT)
        || fi.getDirection().equals(DirectionType.IN_OUT))
    {
      Set<FeatureInstance> destinations = RoutingProperties.getFeatureDestinations(fi);
      routingImplCode.addOutput("uint8_t ");
      routingImplCode.addOutput(AadlToPokCUtils.getFeatureLocalIdentifier(fi)+portSuffix+
                                "_deployment_destinations["+
                                Integer.toString(destinations.size())+"] = {");
      
      for(FeatureInstance dst:destinations)
      {
    	if(!dst.getCategory().equals(FeatureCategory.BUS_ACCESS) 
    			&& routeProp.getBoundProcessorFor(dst).equals(routeProp.getBoundProcessorFor(fi)))
    	{
    		routingImplCode.addOutput(AadlToPokCUtils.getFeatureGlobalIdentifier(dst)+portSuffix);
    		routingImplCode.addOutput(",");
    		continue;
    	}
      }
      routingImplCode.addOutputNewline("};");   
    }
  }


  @Override
  protected String getFeatureLocalIdentifier(FeatureInstance fi)
  {
    return AadlToPokCUtils.getFeatureLocalIdentifier(fi) ;
  }


  @Override
  protected String getGenerationIdentifier(String prefix)
  {
    return GenerationUtilsC.getGenerationCIdentifier(prefix) ;
  }
  
  public String getVirtualPortLocalCIdentifier(VirtualPort vp, String suffix) {
	  return AadlToPokCUtils.getFeatureLocalIdentifier(vp)+suffix;
  }
  
  public String getVirtualPortGlobalCIdentifier(VirtualPort vp, String suffix) {
	  return AadlToPokCUtils.getFeatureGlobalIdentifier(vp)+suffix;
  }

  @Override
  protected void genFileIncludedMainImpl()
  {
	  super.genFileIncludedMainImpl();
	  _mainCCode.addOutputNewline("#include <libc/string.h>") ;
	  _mainCCode.addOutputNewline("#include <core/error.h>");
	  _mainCCode.addOutputNewline("#include <core/partition.h>");
	  _mainCCode.addOutputNewline("#include <core/thread.h>");
  }
  
  private boolean needsGeneratedErrorHandler(ProcessSubcomponent process)
  {
	  boolean foundProcessHMActions = false;
	  ProcessImplementation processImpl = (ProcessImplementation) process.getComponentImplementation();
	  for(ThreadSubcomponent ts: processImpl.getOwnedThreadSubcomponents())
	  {
		  for(PropertyAssociation pa: ts.getOwnedPropertyAssociations())
		  {
			  if(pa.getProperty().getName().equalsIgnoreCase("HM_Error_ID_Actions"))
			  {
				  foundProcessHMActions = true;
				  break;
			  }
		  }

		  if(foundProcessHMActions)
			  break;
	  }
	  return foundProcessHMActions;
  }
  
  protected void genMainImpl(ProcessSubcomponent process,
          ProcessProperties pp, ArchTraceSpec traces, List<String> additionalHeaders) {
	  super.genMainImpl(process, pp, traces, additionalHeaders);
	  
	  if(!needsGeneratedErrorHandler(process))
		  return;
	  
	  _mainCCode.addOutputNewline("");
	  _mainCCode.addOutputNewline("void pok_error_handler_worker() {");
	  _mainCCode.incrementIndent();
	  _mainCCode.addOutputNewline("while(1) {");
	  _mainCCode.incrementIndent();
	  _mainCCode.addOutputNewline("STOP_SELF();");
	  
	  _mainCCode.addOutputNewline("pok_error_status_t status;");
	  _mainCCode.addOutputNewline("pok_error_get(&status);");
	  _mainCCode.addOutputNewline("switch(status.failed_thread)");
	  _mainCCode.addOutputNewline("{");
	  _mainCCode.incrementIndent();
	  ProcessImplementation pi = (ProcessImplementation) process.getComponentImplementation();
	  EList<ThreadSubcomponent> subcomponents = pi.getOwnedThreadSubcomponents();
	  int idx=2; // Start with index 2: Idle thread and error handler
	  for (ThreadSubcomponent ts : subcomponents) {
		  _mainCCode.addOutputNewline("case "+idx+":");
		  _mainCCode.addOutputNewline("{");
		  _mainCCode.incrementIndent();
		  _mainCCode.addOutputNewline("switch(status.error_kind)");
		  _mainCCode.addOutputNewline("{");
		  _mainCCode.incrementIndent();
		  
		  ComponentInstance threadInstance = (ComponentInstance) traces.
                  getTransformationTrace(ts) ;
		    
	      
		  ComponentInstance processor = AadlToARINC653Utils.getProcessor(threadInstance);
		  
	      generateErrorIdSelection(processor, _mainCCode, null, threadInstance);
		  
		  _mainCCode.decrementIndent();
		  _mainCCode.addOutputNewline("} // switch error kind");
		  
		  _mainCCode.decrementIndent();
		  _mainCCode.addOutputNewline("break;");
		  _mainCCode.addOutputNewline("} // case "+idx);
		  idx++;
	  }
	  
	  _mainCCode.addOutputNewline("default:");
	  _mainCCode.addOutputNewline("{");
	  _mainCCode.incrementIndent();
	  _mainCCode.addOutputNewline("pok_partition_set_mode(POK_PARTITION_MODE_STOPPED);");
	  _mainCCode.addOutputNewline("break;");
	  _mainCCode.decrementIndent();
	  _mainCCode.addOutputNewline("}");
	  
	  
	  _mainCCode.decrementIndent();
	  _mainCCode.addOutputNewline("} // switch failed thread");
	  
	  _mainCCode.decrementIndent();
	  _mainCCode.addOutputNewline("}");
	  _mainCCode.decrementIndent();
	  _mainCCode.addOutputNewline("}");
  }


  @Override
  protected void generateErrorHandlerCreation(ProcessSubcomponent process) {
	  if(needsGeneratedErrorHandler(process))
		  _mainCCode.addOutputNewline("pok_error_handler_create();");

  }

}
