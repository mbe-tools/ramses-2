/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.pok.codegen.makefile ;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File ;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator ;
import java.util.List ;
import java.util.Set ;

import org.apache.log4j.Logger ;
import org.eclipse.core.runtime.IProgressMonitor ;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.NamedElement ;
import org.osate.aadl2.ProcessImplementation ;
import org.osate.aadl2.ProcessSubcomponent ;
import org.osate.aadl2.Processor;
import org.osate.aadl2.ProcessorSubcomponent ;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.VirtualProcessorSubcomponent;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.modelsupport.UnparseText ;
import org.osate.aadl2.modelsupport.modeltraversal.AadlProcessingSwitch;
import org.osate.aadl2.util.Aadl2Switch ;
import org.osate.utils.internal.PropertyUtils ;

import fr.mem4csd.ramses.arinc653.utils.AadlToARINC653Utils;
import fr.mem4csd.ramses.arinc653.utils.PartitionIndexComparator;
import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.arch_trace.util.ArchTraceSourceRetreival;
import fr.mem4csd.ramses.core.codegen.AbstractAadlToCMakefileUnparser;
import fr.mem4csd.ramses.core.codegen.GenerationException;
import fr.mem4csd.ramses.core.codegen.utils.FileUtils;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration;
import fr.mem4csd.ramses.core.workflowramses.Codegen;
import fr.mem4csd.ramses.pok.codegen.c.AadlToPokCUtils;

public class AadlToPokMakefileUnparser extends AbstractAadlToCMakefileUnparser {

	public static final String POK_TARGET_ID = "POK";

	private final static URI C_RUNTIME_INCL_DIR = URI.createURI( "libpok" ).appendSegment( "include" );

	private final static URI _POK_RUNTIME_PATH = URI.createURI( "misc" ).appendSegment( "mk" ).appendSegment( "config" ).appendFileExtension( "mk" );

	public final static String POK_RUNTIME_VAR_ENV = "POK_PATH" ;

	private static final Logger _LOGGER = Logger.getLogger(AadlToPokMakefileUnparser.class) ;
	private static boolean isFirstConnected=true;
	
	private UnparseText unparserContent ;
	private UnparseText kernelMakefileContent ;
	private UnparseText unparserAcrhContent; 

	private String archMkFileName = "arch.mk";
	private String pokPathMkFileName = "pok_path.mk";
	private URI processOutputDir;
	
	@Override
	public String getTargetName() {
		return POK_TARGET_ID;
	}

	class PokMakefileUnparser extends AadlProcessingSwitch
	{
		public PokMakefileUnparser() {
			container = getCodeGenWorkflowComponent();
			final URI pokCFile = container.getTargetInstallDirectoryURI().appendSegments( C_RUNTIME_INCL_DIR.segments() );
			_includeDirManager.addCommonDependency( pokCFile );
		}
		
		@Override
		protected void initSwitches()
		{
			aadl2Switch = new Aadl2Switch<String>() {

				@Override
				public String caseProcessSubcomponent(ProcessSubcomponent object)
				{
					unparserContent.addOutputNewline("include ../"+archMkFileName);
					unparserContent.addOutputNewline("include ../../"+pokPathMkFileName);
					unparserContent
					.addOutputNewline("export DEPLOYMENT_HEADER=$(shell pwd)/main.h") ;
					unparserContent.addOutputNewline("include $(" + POK_RUNTIME_VAR_ENV +
							")/misc/mk/config.mk") ;
					unparserContent
					.addOutputNewline("export OBJ_DIR = $(shell pwd)/obj");
					unparserContent.addOutputNewline("TARGET_LIBPOK = $(shell pwd)/obj/libpok/libpok.a");

					unparserContent.addOutputNewline("TARGET = " + object.getName() +
							".elf") ;
					process(object.getComponentImplementation()) ;
					return DONE ;
				}

				@Override
				public String caseProcessImplementation(ProcessImplementation object)
				{
					unparserContent
					.addOutput("OBJS = ") ;
					
					sourceFilesURISet.addAll(getGeneratedSourceFiles());
					
					final Set<URI> sourceFileList = getListOfReferencedObjects(object);
					sourceFilesURISet.addAll(sourceFileList);
					
					final Set<URI> targetFileList = getTargetSourceFiles();
					sourceFilesURISet.addAll(targetFileList);
					
					List<String> alreadyAdded = new ArrayList<String>();
					
					for( URI sourceFile : sourceFilesURISet) {
						if ( isSourceOrObjectFile( sourceFile ) ) {
							sourceFile = sourceFile.trimFileExtension().appendFileExtension( "o" );
						}
						else
							continue ;

						String toAdd = toMakefileString( sourceFile );
						if(!alreadyAdded.contains(toAdd))
						{
							alreadyAdded.add(toAdd);
							unparserContent.addOutput( toAdd + " ") ;
						}

						// NEXT is kept to embedd in code of makefile unparser for remote communications
//							unparserContent.addOutput( toMakefileString( container.getTargetRuntimeDirectoryURI().appendSegment( "aadl_ports_standard_arinc653.o") )+ " " );
//							URI valueToConcat = sourceFile.trimSegments( 1 );
//							for(DataSubcomponent d : object.getOwnedDataSubcomponents())
//							{
//								if(d.getDataSubcomponentType().getName().equals("ProcessorLinkType"))
//								{
//									URI nwkValue = valueToConcat.appendSegment( "aadl_ports_network.o" );
//									unparserContent.addOutput( toMakefileString( nwkValue ) + " ");
//									break;
//								}
//							}
						
					}

					unparserContent.addOutput("\n") ;
					unparserContent.addOutputNewline("OBJS := $(patsubst %.o,$(OBJ_DIR)/%.o,$(OBJS))");
					unparserContent.addOutputNewline("OBJDIRS = $(sort $(dir $(OBJS)))");
					unparserContent.addOutputNewline("all: $(OBJDIRS) libpok $(TARGET)\n") ;
					unparserContent.addOutputNewline("$(OBJDIRS):");
					unparserContent.addOutputNewline("\t$(MKDIR) -p $@");
					unparserContent.addOutputNewline("clean: common-clean libpok-clean\n") ;
					unparserContent.addOutputNewline("include $(" + POK_RUNTIME_VAR_ENV +
							")/misc/mk/common-$(ARCH).mk") ;

					Iterator<URI> it = new IncludeDirIterator() ;

					
					if(it.hasNext())
					{
						unparserContent.addOutput("export COPTS= -DUSE_ARINC653 -DUSE_POK -DGENERATED_CODE_PATH $(ADD_OPTS) ") ;
						
						unparserContent.addOutput(" -I\"" + processOutputDir.toFileString() + "\" ") ;
						
						URI include ;
						while(it.hasNext())
						{
							include = it.next() ;
							if(include==null||include.isEmpty())
								continue;
							unparserContent.addOutput("-I\"" + toMakefileString( include ) + "\" ") ;
						}
						
						unparserContent.addOutput("\n") ;
					}

					unparserContent.addOutputNewline("include $(" + POK_RUNTIME_VAR_ENV +
							")/misc/mk/rules-partition.mk") ;
					unparserContent.addOutputNewline("include $(" + POK_RUNTIME_VAR_ENV +
							")/misc/mk/rules-common.mk") ;
					return DONE ;
				}

				@Override
				public String caseSubcomponent(Subcomponent object)
				{
					unparserAcrhContent = new UnparseText();

					String archiName = PropertyUtils.getEnumValue(object, "Architecture") ;
					if(archiName != null)
					{
						unparserAcrhContent.addOutputNewline("export ARCH=" + archiName) ;
					}
					else
					{
						String errMsg =  "Property Architecture from property set POK, not found for " +
								" process subcomponent \'" + object.getName() + '\'' ;
						_LOGGER.error(errMsg);
					}
					unparserContent.addOutputNewline("include "+archMkFileName);
					unparserContent.addOutputNewline("include ../"+pokPathMkFileName);
					String address = PropertyUtils.getStringValue(object, "Address");
					if(address!=null)
					{
						unparserContent.addOutputNewline("MAC_ADDR="+address.toLowerCase());
						String mode = "connect";
						if(isFirstConnected)
						{
							mode="listen";
							isFirstConnected=false;
						}
						unparserContent.addOutputNewline("QEMU_NETWORK_MODE="+mode);
					}
					String bspName = PropertyUtils.getEnumValue(object, "BSP") ;
					if(bspName != null)
					{
						if(bspName.equalsIgnoreCase("x86_qemu"))
						{
							bspName = "x86-qemu" ;
						}

						unparserAcrhContent.addOutputNewline("export BSP=" + bspName) ;
					}
					else
					{
						String errMsg = "Property BSP from property set POK, not found for" +
								" process subcomponent \'" + object.getName() + '\'';
						_LOGGER.error(errMsg);
					}

					List<ProcessSubcomponent> bindedProcess =
							AadlHelperImpl.getBindedProcesses(object) ;
					
					process(object.getSubcomponentType()) ;
					unparserContent.addOutputNewline("");
					unparserContent.addOutputNewline("run: xcov-pre-run spoq-pre-run instrumentation-pre-run launch-run xcov-post-run instrumentation-post-run spoq-post-run");
					unparserContent.addOutputNewline("#To run gdb, launch in a terminal: \n#\tmake run QEMU_MISC+=\" -S -gdb tcp::1234 \"");
					unparserContent.addOutputNewline("#then\n#\tgdb --eval-command=\"target remote :1234\" pok.elf");

					unparserContent.addOutputNewline("");
					
					
					
					unparserContent
					.addOutputNewline("export POK_CONFIG_OPTIMIZE_FOR_GENERATED_CODE=1") ;
					unparserContent
					.addOutputNewline("include $("+POK_RUNTIME_VAR_ENV+")/misc/mk/config.mk") ;
					unparserContent
					.addOutputNewline("include $("+POK_RUNTIME_VAR_ENV+")/misc/mk/common-$(ARCH).mk") ;
					unparserContent.addOutputNewline("TARGET=$(shell pwd)/pok.elf") ;
					unparserContent.addOutput("PARTITIONS=") ;

					// reorder processes according to their virtual processor index
					Collections.sort(bindedProcess, new PartitionIndexComparator(object));
					
					for(ProcessSubcomponent proc:bindedProcess)
					{
						unparserContent.addOutput(proc.getName() + File.separator +
								proc.getName() + ".elf ") ;
					}

					unparserContent.addOutput("\n") ;
					unparserContent.addOutputNewline("KERNEL=kernel/obj/kernel/kernel.lo") ;
					unparserContent
					.addOutputNewline("all: build-kernel partitions $(TARGET)") ;
					unparserContent.addOutputNewline("build-kernel:") ;
					unparserContent.addOutputNewline("\t$(CD) kernel && $(MAKE)") ;
					unparserContent.addOutputNewline("partitions:") ;

					for(ProcessSubcomponent part:bindedProcess)
					{
						unparserContent.addOutputNewline("\t$(CD) " + part.getName() +
								" && $(MAKE)") ;
					}

					unparserContent.addOutputNewline("clean: common-clean") ;
					unparserContent.addOutputNewline("\t$(CD) kernel && $(MAKE) clean") ;

					for(ProcessSubcomponent part:bindedProcess)
					{
						unparserContent.addOutputNewline("\t$(CD) " + part.getName() +
								" && $(MAKE) clean") ;
					}

					unparserContent.addOutputNewline("distclean: clean") ;
					unparserContent.addOutputNewline("\t$(CD) kernel && $(MAKE) distclean") ;

					for(ProcessSubcomponent part:bindedProcess)
					{
						unparserContent.addOutputNewline("\t$(CD) " + part.getName() +
								" && $(MAKE) distclean") ;
					}

					unparserContent.addOutputNewline("benchmark: ") ;
					unparserContent.addOutputNewline("\t@ADD_OPTS=-DBENCHMARK $(MAKE) all") ;

					unparserContent.addOutputNewline("runtime-services-debug:") ;
					unparserContent.addOutputNewline("\t@ADD_OPTS='-DDEBUG -DRUNTIME_DEBUG' $(MAKE) all") ;

					unparserContent.addOutputNewline("include $("+POK_RUNTIME_VAR_ENV+")/misc/mk/rules-common.mk");
					unparserContent.addOutputNewline("include $("+POK_RUNTIME_VAR_ENV+")/misc/mk/rules-main.mk");
					unparserContent.addOutputNewline("include $("+POK_RUNTIME_VAR_ENV+")/misc/mk/install-rules.mk");

					return DONE ;
					
				}
			} ;
		}
	}
	
	private void generateMakefile(NamedElement ne,
			URI outputDir)
	{
		unparserContent = new UnparseText() ;
		processOutputDir = outputDir;
		PokMakefileUnparser unparser = new PokMakefileUnparser();
		unparser.process(ne) ;
		super.saveMakefile(unparserContent, outputDir) ;
	}

	@Override
	public void generateProcessorBuild(Subcomponent processor,
			URI outputDir,
			IProgressMonitor monitor)
	throws GenerationException {

		generateMakefile((NamedElement) processor, outputDir) ;
		kernelMakefileContent = new UnparseText() ;

		final URI kernelDir = outputDir.appendSegment( "kernel" );
		FileUtils.makeDir( kernelDir );
		kernelMakefileContent.addOutputNewline("include ../"+archMkFileName);
		kernelMakefileContent
		.addOutputNewline("export DEPLOYMENT_HEADER=$(shell pwd)/deployment.h") ;
		kernelMakefileContent
		.addOutputNewline("export OBJ_DIR = $(shell pwd)/obj");

		kernelMakefileContent
		.addOutputNewline("include $("+POK_RUNTIME_VAR_ENV+")/misc/mk/config.mk") ;
		kernelMakefileContent.addOutputNewline("TARGET_KERNEL = $(OBJ_DIR)/kernel/pok.lo");
		kernelMakefileContent.addOutputNewline("LO_TARGET = kernel.lo") ;
		kernelMakefileContent.addOutputNewline("LO_OBJS = deployment.o routing.o") ;
		kernelMakefileContent.addOutputNewline("LO_DEPS = kernel/pok.lo") ;
		kernelMakefileContent.addOutputNewline("include $(POK_PATH)/misc/mk/objdir.mk");
		kernelMakefileContent
		.addOutputNewline("all: kernel copy-kernel $(LO_TARGET)") ;
		kernelMakefileContent.addOutputNewline("clean: common-clean kernel-clean") ;
		kernelMakefileContent
		.addOutputNewline("include $("+POK_RUNTIME_VAR_ENV+")/misc/mk/common-$(ARCH).mk") ;
		kernelMakefileContent
		.addOutputNewline("include $("+POK_RUNTIME_VAR_ENV+")/misc/mk/rules-common.mk") ;
		kernelMakefileContent
		.addOutputNewline("include $("+POK_RUNTIME_VAR_ENV+")/misc/mk/rules-kernel.mk") ;
		super.saveMakefile(kernelMakefileContent, kernelDir) ;

		super.saveMakefile(unparserAcrhContent, outputDir, outputDir.appendSegment( archMkFileName ) );

	}

	@Override
	public void generateProcessBuild(ProcessSubcomponent process,
			URI outputDir,
			IProgressMonitor monitor)
					throws GenerationException
	{
		generateMakefile((NamedElement) process, outputDir) ;
	}

	@Override
	public boolean validateTargetPath(URI runtimePath)
	{
		if(runtimePath==null)
			return false;
		final String path;
		if(runtimePath.isFile())
			path = runtimePath.toFileString();
		else
			path = runtimePath.toString();
		File result = new File(path +File.separator+ _POK_RUNTIME_PATH);
		try 
		{
			FileReader fr = new FileReader(result);
			fr.close();
		}
		catch (IOException e)
		{
			return false;
		}

		return true;
	}


	@Override
	public void generateSystemBuild(EList<Subcomponent> processors, URI outputDir, IProgressMonitor monitor) {
		UnparseText unparserPokPathMkFile = new UnparseText();

		Codegen container = this.getCodeGenWorkflowComponent();
		unparserPokPathMkFile.addOutputNewline("export " + POK_RUNTIME_VAR_ENV + "=" +
				toMakefileString(container.getTargetInstallDirectoryURI()));

		URI pokPathMkFile = outputDir.appendSegment( pokPathMkFileName);
		super.saveMakefile(unparserPokPathMkFile, outputDir, pokPathMkFile) ;


		unparserContent = new UnparseText();
		unparserContent.addOutputNewline("include "+ pokPathMkFileName);
		unparserContent.addOutputNewline("") ;
		unparserContent.addOutputNewline("all:") ;

		for(Subcomponent aProcessorSubcomponent : processors)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " all") ;
		}

		unparserContent.addOutputNewline("") ;
		unparserContent.addOutputNewline("benchmark:") ;

		for(Subcomponent aProcessorSubcomponent : processors)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " benchmark") ;
		}

		unparserContent.addOutputNewline("runtime-services-debug:") ;

		for(Subcomponent aProcessorSubcomponent : processors)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " runtime-services-debug") ;
		}

		unparserContent.addOutputNewline("") ;
		unparserContent.addOutputNewline("clean:") ;

		for(Subcomponent aProcessorSubcomponent : processors)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " clean") ;
		}

		unparserContent.addOutputNewline("") ;
		unparserContent.addOutputNewline("run:") ;

		List<Subcomponent> processorsList = processors;
		for(Subcomponent aProcessorSubcomponent : processorsList)
		{
			Boolean isLastProcessor = (processorsList.indexOf(aProcessorSubcomponent) == 
					processorsList.size()-1);
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " run" + (!isLastProcessor? "&":"") ) ;

			String OS = System.getProperty("os.name", "generic").toLowerCase();
			if(OS.indexOf("nux") >= 0)
			{
				Runtime rt = Runtime.getRuntime();
				Process proc;
				try {
					proc = rt.exec("wmctrl help");
					int exitVal = proc.waitFor();
					if(exitVal>0)
					{
						int screenHeight = 800;
						int screenWidth = 800;
						if(RamsesConfiguration.USES_GUI)
						{
							Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
							screenHeight = screenSize.height;
							screenWidth = screenSize.width;
						}

						int minSize = Math.min(screenHeight, screenWidth);
						int idx = processors.indexOf(aProcessorSubcomponent);
						int processorsNb = processors.size();
						int length_x = (int) Math.ceil(Math.sqrt((minSize*minSize)/processorsNb));
						int pos_x = (((idx*length_x)%minSize)/length_x)*length_x;
						int pos_y = ((idx*length_x)/minSize)*length_x;
						unparserContent.addOutputNewline("\tsleep 1");
						unparserContent.addOutputNewline("\twmctrl -r QEMU -N " + aProcessorSubcomponent.getName());
						unparserContent.addOutputNewline("\twmctrl -r "+aProcessorSubcomponent.getName()+" -e 0,"+pos_x+","+pos_y+","+length_x+","+length_x);
					}
				} catch (Exception e) {
					// Do nothing
				}
			}
		}

		unparserContent.addOutputNewline("") ;
		unparserContent.addOutputNewline("test:") ;
		for(Subcomponent aProcessorSubcomponent : processors)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " + 
					aProcessorSubcomponent.getName() + " run " +
					"QEMU_MISC=\"-nographic -serial /dev/stdout > " +
					aProcessorSubcomponent.getName()+".exec_trace\"") ;
		}
		super.saveMakefile(unparserContent, outputDir) ;

	}

	@Override
	public String getTargetShortDescription() {
		return "(http://pok.tuxfamily.org/)";
	}
	
	protected Set<URI> getTargetSourceFiles() {
		Set<URI> result = new HashSet<URI>();
		for( URI sourceFile : sourceFilesURISet ) {
			
			if ( "aadl_dispatch.c".equals( sourceFile.lastSegment() ) ) // FIXME better way to do this ??
			{
				sourceFile = container.getTargetRuntimeDirectoryURI().appendSegment( "aadl_dispatch_arinc653.c" );
				result.add(sourceFile);
			}
			else if ( "aadl_ports_standard.c".equals( sourceFile.lastSegment() ) )
			{
				sourceFile = container.getTargetRuntimeDirectoryURI().appendSegment( "aadl_ports_standard_arinc653.c" );
				result.add(sourceFile);
			}
		}
		return result;
	}
}