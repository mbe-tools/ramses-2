package fr.mem4csd.ramses.nxtosek.examples;

import fr.tpt.mem4csd.utils.eclipse.ui.AbstractExampleWizard;

public class RamsesLineFollowerExampleWizard extends AbstractExampleWizard {
	@Override
	protected String[] getProjectNames() {
		return new String[]{ "line-follower-nxtosek"};
	}

	@Override
	protected String getPluginId() {
		return "fr.mem4csd.ramses.nxtosek.examples";
	}
	@Override
	protected String getExamplesSourceDir() {
		return "examples_src";
	}
}
