#include "aadl_multiarch.h"
#include "aadl_modes_standard.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#ifdef RUNTIME_DEBUG
#include <stdio.h>
#endif

TickType_t start_ticktime;

error_code_t create_thread (unsigned int priority,
		   unsigned int stack_size,
		   void (*start_routine)(void),
		   void * tid,
		   const char * name,
		   unsigned int core_id);

void get_time_multiarch(uint64_t * time)
{
  *time = (xTaskGetTickCount() - start_ticktime) * 1000000000 / configTICK_RATE_HZ ;
}

void get_start_time(uint64_t * time)
{
  *time = (start_ticktime * 1000000000 / configTICK_RATE_HZ) ;
}

void set_start_time_multiarch()
{
  start_ticktime = xTaskGetTickCount();
}

error_code_t set_thread_priority_multiarch(thread_config_t * config, unsigned int priority)
{
  error_code_t ret = RUNTIME_OK;
	TaskHandle_t xTask = config->target_thread->thread_id;
	vTaskPrioritySet( xTask, priority );
    return ret;
}

int init_thread_config_multiarch(thread_config_t * config)
{
  config->wait_mode_event->event = xSemaphoreCreateCounting(1,0);
  return 0;
}

int init_periodic_config_multiarch(target_periodic_thread_config_t * config)
{
  int ret = 0;
  config->iteration_counter=0;
  return ret;
}

extern SemaphoreHandle_t init_barrier_mutex;
extern SemaphoreHandle_t init_barrier_cond;
extern unsigned long reached_init_barrier;
extern mode_id_t current_mode;

error_code_t Await_Mode_multiarch(thread_config_t * config)
{
#if defined RUNTIME_DEBUG
	  printf("Thread %s is in Await_Mode_multiarch\n", config->name);
#endif

	error_code_t ret = RUNTIME_OK;
	if (xSemaphoreTake(init_barrier_mutex, portMAX_DELAY) == pdFALSE)
		ret = RUNTIME_LOCK_ERROR;

  	if(config->state==INITIALIZING_THREAD_STATE
			  && Is_In_Mode(config, current_mode))
	{
	  reached_init_barrier++;
#if defined RUNTIME_DEBUG
	  printf("Thread %s reached barrier, total=%d threads arrived\n", config->name, reached_init_barrier);
#endif
  	}

 	config->state=AWAITING_MODE_THREAD_STATE;

	if(xSemaphoreGive(init_barrier_cond) == pdFALSE)
		ret = RUNTIME_LOCK_ERROR;
  
#if defined RUNTIME_DEBUG
  	printf("Thread %s initialized\n", config->name);
#endif

	if(xSemaphoreGive(init_barrier_mutex) == pdFALSE)
		ret = RUNTIME_LOCK_ERROR;
		
  	if(xSemaphoreTake(config->wait_mode_event->event, portMAX_DELAY) == pdFALSE)
  		ret = RUNTIME_LOCK_ERROR;

#if defined RUNTIME_DEBUG
  	printf("Thread %s released in current mode\n", config->name);
#endif

  	switch (config->protocol)
    {
      	case PERIODIC:
      	{
        	target_periodic_thread_config_t * per_thr = config->thread;
        	per_thr->iteration_counter=0;
        	if(per_thr->parent->offset)
        	{
				if(per_thr->first_sleep)
  				{
	  				per_thr->xLastWakeTime = xTaskGetTickCount();
	  				per_thr->first_sleep = 0;
  				}
        		TickType_t offset_ticks = start_ticktime + (per_thr->parent->offset * configTICK_RATE_HZ / 1000000000);
        		//vTaskDelayUntil(&per_thr->xLastWakeTime, offset_ticks);
        		vTaskDelay(offset_ticks);

        	}
        	break;
      	}
      	default:
    	break;
    }
  	return ret;
}

error_code_t release_task_in_mode_multiarch(thread_config_t * config)
{
	error_code_t ret = RUNTIME_OK;
	ret |= xSemaphoreGive(config->wait_mode_event->event);
	return ret;
}

error_code_t create_thread_multiarch(thread_config_t * config)
{
	error_code_t ret = RUNTIME_OK;
	/* nothing to do here (start_thread_multiarch also create
	   thread on FreeRTOS */
	return ret;
}

error_code_t start_thread_multiarch(thread_config_t * config)
{
	error_code_t ret = RUNTIME_OK;
	ret |= create_thread(config->priority,
			configMINIMAL_STACK_SIZE,
			config->compute_entrypoint,
			config->target_thread->thread_id,
			config->name,
			0);
	return ret;
}

error_code_t stop_thread_multiarch(thread_config_t * config)
{
	error_code_t ret = RUNTIME_OK;
	TaskHandle_t tid = config->target_thread->thread_id;
	
	if(config->global_q != NULL)
	{
		vSemaphoreDelete(config->global_q->event->event);
		vSemaphoreDelete(config->global_q->event->rez);
	}
		

	if (config->protocol == TIME_TRIGGERED)
	{
		target_timetriggered_thread_config_t * tt_thr = config->thread;
		
		if(tt_thr->event !=NULL)
			vSemaphoreDelete(tt_thr->event);
		
		if(tt_thr->rez !=NULL)
			vSemaphoreDelete(tt_thr->rez);		
	}
	if( tid != NULL )
    {
        vTaskDelete( tid );	
    }
	return ret;
}

error_code_t await_periodic_dispatch_multiarch(target_periodic_thread_config_t * info)
{
  error_code_t ret = RUNTIME_OK;

  info->iteration_counter++;
  uint64_t start_time;
  get_start_time(&start_time);

  TickType_t tick_per = (info->parent->period * configTICK_RATE_HZ / 1000000000) ;
  TickType_t now = xTaskGetTickCount();
  
#ifdef RUNTIME_DEBUG
  printf("Periodic wait of %s until %ld ticks, with period (nsec)=%ld @t=%ld\n",info->parent->name, tick_per, info->parent->period, now);
#endif

  if(info->first_sleep)
  {
	  info->xLastWakeTime = xTaskGetTickCount();
	  info->first_sleep = 0;
  }
  vTaskDelayUntil(&info->xLastWakeTime, tick_per);

  info->parent->dispatch_time = info->iteration_counter * info->parent->period + info->parent->offset;

#ifdef RUNTIME_DEBUG
  TickType_t t = xTaskGetTickCount();
  printf("Periodic dispatch of %s, @t=%ld\n", info->parent->name, t);
#endif

  return ret;
}

void sporadic_thread_sleep(target_sporadic_thread_config_t * info)
{
	uint64_t elapsed_time = info->parent->finish_time - info->parent->dispatch_time;
 	if (elapsed_time==0 || elapsed_time >= info->parent->period)
    	return;

	// absolute timeout
	TickType_t sporadic_ticks;
	TickType_t dispatch_time_ticks;
	TickType_t time_interval_ticks;
	TickType_t current_tick = xTaskGetTickCount();

	sporadic_ticks = (info->parent->period) * configTICK_RATE_HZ / 1000000000 ;

#if defined RUNTIME_DEBUG && defined USE_FREERTOS
  	printf("Sporadic wait until tick %ld, period (ns) = %ud, @t=%ld\n", sporadic_ticks, info->parent->period, current_tick);
  	printf("Dispatch time was %ud nsec\n", info->parent->dispatch_time);
#endif

	if(info->first_sleep)
  	{
	  info->xLastWakeTime = xTaskGetTickCount();
	  info->first_sleep = 0;
  	}
	vTaskDelayUntil(&info->xLastWakeTime, sporadic_ticks);
	current_tick = xTaskGetTickCount();
}

error_code_t init_hybrid_timer(target_hybrid_thread_config_t * info)
{
  	info->iteration_counter = 0;
  	return 0;
}

error_code_t hybrid_reset_timer(target_hybrid_thread_config_t * info)
{
  info->iteration_counter += 1;
  return RUNTIME_OK;
}

error_code_t timedwait_hybrid(target_hybrid_thread_config_t * info)
{
	uint64_t start_time;
  	get_start_time(&start_time);
	TickType_t now = xTaskGetTickCount();

	TickType_t time_interval_ticks = (start_time+info->iteration_counter*info->parent->period)  * configTICK_RATE_HZ / 1000000000 - now;
	error_code_t ret = RUNTIME_OK;

#ifdef RUNTIME_DEBUG
  printf("Hybrid wait of %s until max %ld ticks, with period (nsec)=%ld @t=%ld\n",info->parent->name, time_interval_ticks, info->parent->period, now);
#endif

	if( unlock_port_list(info->parent->global_q) == 1)
		return 1;
	xSemaphoreTake( info->parent->global_q->event->event,time_interval_ticks);

	lock_port_list(info->parent->global_q);	
	

#ifdef RUNTIME_DEBUG
  TickType_t t = xTaskGetTickCount();
  printf("Hybrid dispatch of %s, @t=%ld\n", info->parent->name, t);
#endif
	return ret;
}

error_code_t timedwait_timed_thread(target_timed_thread_config_t * info)
{
  	error_code_t ret = RUNTIME_OK;
	TickType_t time_period_ticks;

	time_period_ticks = (info->parent->period * configTICK_RATE_HZ / 1000000000) ;

	ret = unlock_port_list(info->parent->global_q);
	if(xSemaphoreTake( info->parent->global_q->event->event, time_period_ticks) == pdFALSE)
	{
		//ret = RUNTIME_LOCK_ERROR;
		//return ret;
	}
	if(!ret)
		lock_port_list(info->parent->global_q);		
  return ret;
}

error_code_t create_thread (unsigned int priority,
		   unsigned int stack_size,
		   void (*start_routine)(void),
		   void * tid,
		   const char * name,
		   unsigned int core_id)
{
  	int sys_ret = 0;
	BaseType_t xReturned;

#ifdef RUNTIME_DEBUG
	printf("stack size %d\n", stack_size);
#endif
	
	xReturned = xTaskCreate(
                    (TaskFunction_t) start_routine,       
                    name,       
                    stack_size,  
                    NULL,  
                    priority,
                    tid);

    if( xReturned != pdPASS )
	{
		sys_ret = 1;
		#if defined DEBUG
  			printf("Error: xTaskCreate\n");
		#endif
		return sys_ret;
	}
  return sys_ret;
}

error_code_t lock_init_barrier()
{
	error_code_t ret = RUNTIME_OK;
	if (xSemaphoreTake(init_barrier_mutex, portMAX_DELAY))
		return ret;
	ret = RUNTIME_LOCK_ERROR;
	return ret;
}

error_code_t unlock_init_barrier()
{
	error_code_t ret = RUNTIME_OK;
	if (xSemaphoreGive(init_barrier_mutex))
		return ret;
	ret = RUNTIME_LOCK_ERROR;
	return ret;
}

error_code_t wait_threads_init_multiarch(unsigned int threads_under_init)
{
	error_code_t ret = RUNTIME_OK;
	reached_init_barrier = 0;
	while(reached_init_barrier<threads_under_init)
	{	
		if(xSemaphoreGive(init_barrier_mutex) == pdFALSE)
			ret = RUNTIME_LOCK_ERROR;

		if(xSemaphoreTake(init_barrier_cond, portMAX_DELAY) == pdFALSE)
			ret = RUNTIME_LOCK_ERROR;
		
		if(xSemaphoreTake(init_barrier_mutex, portMAX_DELAY) == pdFALSE)
			ret = RUNTIME_LOCK_ERROR;
	}	
	return ret;
}

error_code_t await_time_triggered_dispatch_multiarch(target_timetriggered_thread_config_t * config)
{
  error_code_t ret = RUNTIME_OK;

  if (xSemaphoreTake(config->rez, portMAX_DELAY) == pdFALSE)
  		ret = RUNTIME_LOCK_ERROR;

  if(config->parent->state != RUNNING_THREAD_STATE)
  {
	if (xSemaphoreGive(config->rez) == pdFALSE)
  		ret = RUNTIME_LOCK_ERROR; 

	if (xSemaphoreTake(config->event, portMAX_DELAY) == pdFALSE)
  		ret = RUNTIME_LOCK_ERROR;

	if (xSemaphoreTake(config->rez, portMAX_DELAY) == pdFALSE)
  		ret = RUNTIME_LOCK_ERROR;	
  }

  if (xSemaphoreGive(config->rez) == pdFALSE)
  		ret = RUNTIME_LOCK_ERROR; 

  return ret;
}

error_code_t dispatch_time_triggered_thread(target_timetriggered_thread_config_t * config)
{
  	error_code_t ret = RUNTIME_OK;

	if (xSemaphoreTake(config->rez, portMAX_DELAY) == pdFALSE)
  		ret = RUNTIME_LOCK_ERROR;
	
	config->parent->state = RUNNING_THREAD_STATE;

	if (xSemaphoreGive(config->event) == pdFALSE)
  		ret = RUNTIME_LOCK_ERROR;

	if (xSemaphoreGive(config->rez) == pdFALSE)
  		ret = RUNTIME_LOCK_ERROR; 

  return ret;
}

error_code_t await_time_triggered_delay(uint64_t delay_ns)
{
	TickType_t delay_tick = delay_ns * configTICK_RATE_HZ / 1000000000;
 	vTaskDelay(delay_tick);
}

error_code_t bind_thread_to_core_id(thread_config_t * config, uint8_t core_id)
{
  // do nothing here, until we define how this service for a specific
  // HW platform
  return RUNTIME_OK;
}

error_code_t init_time_triggered_config(target_timetriggered_thread_config_t * config)
{
  error_code_t ret = RUNTIME_OK;
  config->event = xSemaphoreCreateCounting(1,0);
  config->rez = xSemaphoreCreateMutex();
  if (config->parent->global_q != NULL)
    ret |= init_port_list_mutex(config->parent->global_q);
  return ret;
}

void init_dispatch_configuration(thread_config_t * config)
{
  switch (config->protocol)
    {
    case PERIODIC:
      {
	target_periodic_thread_config_t * per_thr = config->thread;
	per_thr->iteration_counter = 0;
	per_thr->first_sleep = 1;
	break;
      }
    case HYBRID:
      {
	target_hybrid_thread_config_t * hyb_thr = config->thread;
	hyb_thr->iteration_counter = 0;
	hyb_thr->first_sleep = 1;
	break;
      }
	case SPORADIC:
	{
	target_sporadic_thread_config_t * per_thr = config->thread;
	per_thr->first_sleep = 1;
	}
    default:
      break;
    }
}

