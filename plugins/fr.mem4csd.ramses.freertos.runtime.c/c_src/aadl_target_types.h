#ifndef _AADL_FREERTOS_TYPES_H
#define _AADL_FREERTOS_TYPES_H

//#include "aadl_runtime_thread.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

struct thread_config_t;

typedef struct timespec target_time_t;

typedef struct aadl_mutex
{
    SemaphoreHandle_t mutex;
} aadl_mutex_t;

typedef struct aadl_semaphore
{
    SemaphoreHandle_t sem;
} aadl_semaphore_t;

typedef struct aadl_task
{
    TaskHandle_t task;
} aadl_task_t;

typedef struct aadl_event
{
    SemaphoreHandle_t cond;
} aadl_event_t;

typedef struct aadl_cond
{
    SemaphoreHandle_t cond;
} aadl_cond_t;

typedef struct aadl_watchdog_t
{
} aadl_watchdog_t;

typedef struct aadl_timedevent_t
{
} aadl_timedevent_t;

typedef struct aadl_time_t
{
  unsigned long sec;
  unsigned long nsec;
} aadl_time_t;

typedef struct target_lock_t
{
  SemaphoreHandle_t mutex;
} target_lock_t;

typedef struct target_periodic_dispatch_t
{
  uint32_t iteration_counter;
} target_periodic_dispatch_t;

typedef struct target_periodic_thread_config
{
  struct thread_config_t * parent;
  uint32_t iteration_counter;
  TickType_t xLastWakeTime;
  int first_sleep;
} target_periodic_thread_config_t;


typedef struct target_sporadic_thread_config
{
  struct thread_config_t * parent;
  uint64_t time_interval;
  TickType_t xLastWakeTime;
  int first_sleep;
} target_sporadic_thread_config_t;

typedef struct target_aperiodic_thread_config
{
  struct thread_config_t * parent;
} target_aperiodic_thread_config_t;

typedef struct target_hybrid_thread_config
{
  struct thread_config_t * parent;
  TickType_t start_ticktime;
  uint64_t iteration_counter;
  uint64_t time_interval;
  TickType_t xLastWakeTime;
  int first_sleep;
} target_hybrid_thread_config_t;

typedef struct target_timetriggered_thread_config
{
  struct thread_config_t * parent;
  SemaphoreHandle_t rez;
  SemaphoreHandle_t event;
} target_timetriggered_thread_config_t;

typedef struct target_timed_thread_config
{
  struct thread_config_t * parent;
  uint64_t time_interval;
} target_timed_thread_config_t;

typedef struct target_process_port_t
{
  void * partition_port; // SAMPLING or QUEUEING
} target_process_port_t;

typedef struct target_processor_port_t
{
} target_processor_port_t;

typedef struct target_watchdog_t
{
} target_watchdog_t;

typedef struct target_thread_t
{
  TaskHandle_t thread_id;
} target_thread_t;

typedef struct target_wait_mode_t
{
  SemaphoreHandle_t rez;
  SemaphoreHandle_t event;
} target_wait_mode_t;

typedef struct target_wait_message_t
{
  SemaphoreHandle_t rez;
  SemaphoreHandle_t event;
} target_wait_message_t;

#endif //_AADL_FREERTOS_TYPES_H
