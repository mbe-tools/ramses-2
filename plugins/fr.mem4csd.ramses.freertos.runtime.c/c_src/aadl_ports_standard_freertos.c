#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "aadl_multiarch.h"
#include "gtypes.h"
#include <stdio.h>


extern process_config_t this_process;
extern processor_inputport_addr global_to_local[];

#ifdef DEBUG
#include <stdio.h>

void print_debug(char * str)
{
    printf("%s", str);
}
#endif

extern TickType_t start_ticktime;

// for aadl_port
error_code_t lock_port_reference(thread_input_port_t * port)
{
#if defined RUNTIME_DEBUG && defined USE_FREERTOS
  printf("Lock port reference\n");
#endif

  if(xSemaphoreTake(port->parent->event->rez, portMAX_DELAY) == pdFALSE)
    return 1;
  return 0;
}

error_code_t unlock_port_reference(thread_input_port_t * port)
{
#if defined RUNTIME_DEBUG && defined USE_FREERTOS
    printf("Unlock port reference\n");
#endif
  if(xSemaphoreGive(port->parent->event->rez) == pdFALSE)
    return 1;
  return 0;
}

error_code_t send_signal(thread_input_port_t * port)
{
  if(xSemaphoreGive(port->parent->event->event) == pdFALSE)
    return 1;
  return 0;
}

error_code_t send_out_process(port_reference_t * source,
    void * value,
    unsigned int msg_size)
{
  return RUNTIME_INVALID_SERVICE_CALL; // Not available on FreeRTOS.
}

error_code_t receive_in_process(port_reference_t * destination,
    void * received_msg)
{
  return RUNTIME_INVALID_SERVICE_CALL; // Not available on FreeRTOS.
}

error_code_t lock_data(lock_access_protocol_t * protocol)
{
  if(xSemaphoreTake(protocol->rez->mutex, portMAX_DELAY) == pdFALSE)
    return 1;
  return 0;
}

error_code_t unlock_data(lock_access_protocol_t * protocol)
{
  if(xSemaphoreGive(protocol->rez->mutex) == pdFALSE)
    return 1;
  return 0;
}

error_code_t icpp_lock_data(icpp_access_protocol_t * protocol)
{
  return lock_data(protocol->lock);
}

error_code_t icpp_unlock_data(icpp_access_protocol_t * protocol)
{
  return unlock_data(protocol->lock);
}

int init_port_list_mutex(port_list_t * info)
{
  info->event->event = xSemaphoreCreateCounting(1,1);
  info->event->rez = xSemaphoreCreateMutex();
  return 0;
}

error_code_t lock_port_list(port_list_t * port_list)
{
#if defined RUNTIME_DEBUG && defined USE_POSIX
  printf("Lock port list\n");
#endif
  if(xSemaphoreTake(port_list->event->rez, portMAX_DELAY) == pdFALSE)
    return 1;
  return 0;
}

error_code_t unlock_port_list(port_list_t * port_list)
{
#if defined RUNTIME_DEBUG && defined USE_POSIX
  printf("Unlock port list\n");
#endif
  if (xSemaphoreGive(port_list->event->rez) == pdFALSE)
    return 1;
  return 0;
}

error_code_t wait_port_list(port_list_t * port_list)
{
  unlock_port_list(port_list);
  if(xSemaphoreTake(port_list->event->event, portMAX_DELAY) == pdFALSE)
    return 1;
  lock_port_list(port_list);
  return 0;
}

error_code_t Init_Resource(resource_t * resource)
{
  error_code_t status = RUNTIME_OK;
  switch(resource->protocol)
  {
  case NONE:
    return status;
  case LOCK:
    {
      lock_access_protocol_t * protocol = (lock_access_protocol_t*) resource->protocol_config;
      protocol->rez->mutex = xSemaphoreCreateMutex();
       
      break;
    }
  }
  return status;
}
