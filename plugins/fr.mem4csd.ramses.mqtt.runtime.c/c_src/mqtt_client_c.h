/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

/*
 * <p>An MQTT driver in plain C, witout allocations and threading.
 * Suitable for systems which are not
 * POSIX-compliant. It connects to the default MQTT server in the system.</p>
 * 
 * <p>Since it depends on small footprint libraries destined to embedded systems,
 * their sources are included together with the driver:
 * <ul>
 *     <li>C client of Eclipse Poha in the subdirectory <code>libmqtt</code>;</li>
 *     <li>Parts of the uth hash library in the subdirectory <code>ut</code>.</li>
 * </ul>
 * </p>
 */

#ifndef MQTT_CLIENT_C_H
#define MQTT_CLIENT_C_H

#ifdef __cplusplus
extern "C" {
#endif

#include "MQTTClient.h"
#include "aadl_runtime_services.h"

/**
 * <p>Opens a connection to a MQTT server, if it is not open yet. Otherwise,
 * does nothing. Must be called before any other function in this driver.</p>
 * 
 * @param model_id shared model id
 * @param client_id unique client id
 * @param server_host host name, null for localhost
 * @param server_port port number, -1 for the default of 1883
 * @return <code>EXIT_SUCCESS</code> or <code>EXIT_FAILURE</code>
 */
int mqtt_init(mqtt_config_t * conf, char* model_id, char* client_id, messageHandler on_message_arrived);
/**
 * <p>Initialises a port, which essentially means, that either a publisher or
 * a subscriber is created, associated with that port.</p>
 * 
 * @param port_id unique identifier of the port
 * @param receiver zero if sender, non-zero if receiver
 * @return <code>EXIT_SUCCESS</code> or <code>EXIT_FAILURE</code>
 */
int mqtt_init_port(processor_link_t* processor_port);
/**
 * Sends out a message.
 * 
 */
int mqtt_send(mqtt_config_t * conf, struct message * out, char * topic);
/**
 * Receives the first message stored in an internal queue. Non-blocking.
 * 
 * @param link contains the identifier of a destination port
 * @param message a message to copy to; if none, then its size is set to zero
 * @return <code>EXIT_SUCCESS</code> or <code>EXIT_FAILURE</code>
 */
int mqtt_receive(mqtt_config_t * conf, struct message * msg);
/**
 * Closes a connection to the MQTT server and the thread of this client.
 * Called also by signal handlers.
 * 
 * @param sig a signal, passes by the signal handler; meaningless if called
 * by application code
 */
void mqtt_done(mqtt_config_t* config, int sig);

error_code_t on_mqtt_message_arrived(mqtt_config_t * conf, MessageData* md);

#ifdef __cplusplus
}
#endif

#endif /* MQTT_CLIENT_C_H */

