/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_ports_mqtt.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

error_code_t processor_port_init_mqtt(processor_link_t* processor_port) {
    return mqtt_init_port(processor_port);
}
/**
 * @see the header file
 */
error_code_t send_out_processor_mqtt(processor_destination_port_t * dst_port,
        struct message msg_sent) {
	mqtt_config_t * conf = (mqtt_config_t*)dst_port->link->link_config;
	mqtt_send(conf, &msg_sent, *((char**)dst_port->destination_id));
}
/**
 * @see the header file
 */
error_code_t receive_in_processor_mqtt(processor_link_t *link,
        struct message *msg) {
    msg->size = 0;
    error_code_t status = EXIT_FAILURE;

    status = mqtt_receive(link->link_config, msg);

    return status;
}

error_code_t close_processor_port(processor_link_t * processor_port)
{
  error_code_t err = RUNTIME_OK;
  socket_config_t * socket_config = (socket_config_t *) processor_port->link_config;
  mqtt_done(socket_config->socket_id, 0);
}
