/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_runtime_services.h"

#include "mqtt_client_c.h"

#ifndef AADL_PORTS_MQTT_H
#define AADL_PORTS_MQTT_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * <p>Connects to a MQTT server, if not done yet. Them initialises
 * a port, which essentially means, that either a publisher or
 * a subscriber is created, associated with that port.</p>
 * 
 * @param processor_port description of the port
 * @return error code or <code>RUNTIME_OK</code>
 */
error_code_t processor_port_init_mqtt(processor_link_t* processor_port);
/**
 * Sends out a message.
 * 
 * @param dst_port contains the identifier of a destination AADL port
 * @param msg_sent message to send
 * @return error code or <code>RUNTIME_OK</code>
 */
error_code_t send_out_processor_mqtt(processor_destination_port_t* dst_port,
        struct message msg_sent);
/**
 * Receives the first message stored in an internal queue.
 * 
 * @param link contains the identifier of a destination AADL port
 * @param msg a message; if none, then its size is set to zero;
 * never none if <code>blocking</code> is non-zero
 * @param blocking if non-zero, waits if no messages
 * @return error code or <code>RUNTIME_OK</code>
 */
error_code_t receive_in_processor_mqtt(processor_link_t *link,
        struct message *msg);

#ifdef __cplusplus
}
#endif

#endif /* AADL_PORTS_MQTT_H */

