/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "mqtt_client_c.h"

#include <signal.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

#include "libmqtt/MQTTPacket.h"
#include "libmqtt/MQTTClient.h"

/**
 * Connection options. Initialised to indicate the lack of connection to the
 * MQTT server.
 */
struct mqtt_opts_struct {
	char* clientid;
	int nodelimiter;
	char* delimiter;
	enum QoS qos;
	char* username;
	char* password;
	char* host;
	int port;
	int showtopics;
} mqtt_opts = {
	NULL, 0, (char*)"\n", QOS2, NULL, NULL, NULL, -1, 0
};

/**
 * A model identifier, shared by all nodes within this model.
 */
char* mqtt_model_id;
/**
 * A subscription topic of the connection.
 */
char mqtt_static_subscription_topic[MQTT_STATIC_MESSAGE_MAX_TOPIC_SIZE];
/**
 * A buffer for general use. 
 */
unsigned char mqtt_static_buf[100];
/**
 * A read buffer for general use. 
 */
unsigned char mqtt_static_readbuf[100];
/**
 * A flag which signals, that this client should terminate.
 */
volatile int mqtt_stop = 0;

/**
 * Produces a topic for a string.
 * 
 * @param port_id unique identifier of the port in the AADL model;
 * if empty, a wildcard topic is produced, which accepts messages destined
 * to all ports
 * @param out filled with the topic
 */
void mqtt_get_topic(char* in, char* out) {
    char* s1 = mqtt_model_id;
    char s2[MQTT_STATIC_MESSAGE_MAX_TOPIC_SIZE];
    if(strcmp(in, "") == 0)
        strcpy(s2, "+");
    else
        strcpy(s2, in);

    if(strlen(s1) + 1 + strlen(s2) + 1 > MQTT_STATIC_MESSAGE_MAX_TOPIC_SIZE) {
#ifdef RUNTIME_DEBUG
        printf("mqtt_static_get_topic: insufficient topic size\n");
#endif
        exit(EXIT_FAILURE);
    }
    strcpy(out, s1);
    strcat(out, "/");
    strcat(out, s2);
}


/**
 * Finds a receiving buffer.
 * 
 * @param topic identifier of the buffer to find
 * @return description of that port or null if not found
 */
struct mqtt_buffer_t* mqtt_find_buffer(mqtt_config_t * conf, char* topic) {
	if(conf->subscribers_config==NULL)
		return NULL;
	mqtt_subscriber_config_t * subs_conf = conf->subscribers_config;
#ifdef RUNTIME_DEBUG
	printf("mqtt_static: search topic %s\n", topic);
#endif
    for(int i = 0; i < subs_conf->topics_nb; ++i)
    {
#ifdef RUNTIME_DEBUG
    	printf("mqtt_static: topic[%d]=%s\n", i, subs_conf->topics[i]);
#endif
    	if(strcmp(topic, subs_conf->topics[i])==0) {
            return &(subs_conf->mqtt_buffers[i]);
        }
    }
    return NULL;
}


error_code_t on_mqtt_message_arrived(mqtt_config_t * conf, MessageData* md)
{
	error_code_t rc = RUNTIME_OK;
	char topic[MQTT_STATIC_MESSAGE_MAX_TOPIC_SIZE];
	if(md->topicName->lenstring.len + 1 > MQTT_STATIC_MESSAGE_MAX_TOPIC_SIZE) {
#ifdef RUNTIME_DEBUG
		printf("mqtt_message_arrived: insufficient topic size\n");
#endif
		exit(EXIT_FAILURE);
	}
	strncpy(topic, md->topicName->lenstring.data, md->topicName->lenstring.len);
	topic[md->topicName->lenstring.len]='\0';

#ifdef RUNTIME_DEBUG
	printf("mqtt: received message, client = %s, topic = %s\n", mqtt_opts.clientid, topic);
#endif
	int port_offset = strlen(mqtt_model_id) + 1;
	char port_topic[MQTT_STATIC_MESSAGE_MAX_TOPIC_SIZE];
	strcpy(port_topic, topic + port_offset);
	struct mqtt_buffer_t * mqtt_buffer = mqtt_find_buffer(conf, port_topic);

	if(!mqtt_buffer)
		return EXIT_FAILURE;

	mqtt_buffer->size = md->message->payloadlen;
	if(mqtt_buffer->size > MQTT_STATIC_MESSAGE_MAX_BUFFER_SIZE) {
#ifdef RUNTIME_DEBUG
		printf("mqtt_message_arrived: insufficient buffer size\n");
#endif
		exit(EXIT_FAILURE);
	}

	memcpy(mqtt_buffer->buffer, md->message->payload, mqtt_buffer->size);
	mqtt_buffer->message_present = 1;

	return rc;
}



/**
 * @see the header file
 */
int mqtt_init_port(processor_link_t* processor_port) {

    mqtt_config_t* config = (mqtt_config_t*) processor_port->link_config;
    communication_role_t direction = processor_port->role;

#ifdef RUNTIME_DEBUG
    printf("mqtt_static: client = %s, connected = %i\n", mqtt_opts.clientid, config->client->isconnected);
#endif

    if(config->subscribers_config==NULL)
    	return EXIT_FAILURE;
    mqtt_subscriber_config_t * subs_conf = config->subscribers_config;


    if(direction == RECEIVER) {
        struct mqtt_buffer_t* buf = NULL;
        for(int i = 0; i < subs_conf->topics_nb; ++i)
        {
        	buf = &(subs_conf->mqtt_buffers[i]);
        	buf->message_present = 0;
        }
    }
    return EXIT_SUCCESS;
}
/**
 * @see the header file
 */
void mqtt_done(mqtt_config_t* config, int sig) {
    MQTTDisconnect(config->client);
    NetworkDisconnect(config->network);
    sleep(1);
}
/**
 * @see the header file
 */
int mqtt_init(mqtt_config_t * conf, char* model_id, char* client_id, messageHandler on_message_arrived) {

	char* server_host = conf->ip_addr;
	int server_port = conf->ip_port;

	mqtt_subscriber_config_t * subs_conf = NULL;
	if(conf->subscribers_config!=NULL)
	{
		subs_conf = conf->subscribers_config;
		subs_conf->on_message_arrived = on_message_arrived;
	}

    mqtt_model_id = model_id;
    int rc = 0;
    struct mqtt_opts_struct opts = {
            strdup(client_id), 0, (char*)"\n", QOS2, NULL, NULL,
            (strlen(server_host) ? server_host : (char*)"localhost"), (server_port != -1 ? server_port : 1883),
            1
    };
    mqtt_opts = opts;

#ifdef RUNTIME_DEBUG
   	printf("mqtt_static: init %s, host %s, port %d\n", mqtt_opts.clientid, mqtt_opts.host, mqtt_opts.port);
#endif

    NetworkInit(conf->network);
    if(NetworkConnect(conf->network, mqtt_opts.host, mqtt_opts.port) != -1) {
        MQTTClientInit(conf->client, conf->network, 1000, mqtt_static_buf, 100, mqtt_static_readbuf, 100);

        MQTTPacket_connectData data = MQTTPacket_connectData_initializer;       
        data.willFlag = 0;
        data.MQTTVersion = 3;
        data.clientID.cstring = mqtt_opts.clientid;
#ifdef RUNTIME_DEBUG
        printf("mqtt_static: about to connect as %s\n", opts.clientid);
#endif
        data.username.cstring = mqtt_opts.username;
        data.password.cstring = mqtt_opts.password;

        data.keepAliveInterval = 10;
        data.cleansession = 1;
#ifdef RUNTIME_DEBUG
        printf("mqtt_static: connecting to %s %d\n", mqtt_opts.host, mqtt_opts.port);
#endif

        rc = MQTTConnect(conf->client, &data);
#ifdef RUNTIME_DEBUG
        printf("mqtt_static: connected %d\n", rc);
#endif
        if(!rc && subs_conf!=NULL) {
            // there is a single subscriber for all ports, created just
            // after the connection to the server
            mqtt_get_topic("", mqtt_static_subscription_topic);
            if(rc = MQTTSubscribe(conf->client, mqtt_static_subscription_topic, mqtt_opts.qos, subs_conf->on_message_arrived)) {
                fprintf(stderr, "mqtt_static: could not subscribe, client %s, topic %s, code %i\n",
                        mqtt_opts.clientid, mqtt_static_subscription_topic, rc);
                mqtt_done(conf, 0);
            } else {
#ifdef RUNTIME_DEBUG
            	printf("mqtt_static: subscribed to %s\n", mqtt_static_subscription_topic);
#endif
            }
        }
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}
/**
 * @see the header file
 */
int mqtt_send(mqtt_config_t * conf, struct message * out, char* topic) {
#ifdef RUNTIME_DEBUG
	printf("mqtt_static: send: name = %s, connected = %i\n", mqtt_opts.clientid, conf->client->isconnected);
#endif
    MQTTYield(conf->client, 0);
    int status = EXIT_SUCCESS;
    char the_topic[MQTT_STATIC_MESSAGE_MAX_TOPIC_SIZE];
    mqtt_get_topic(topic, the_topic);

    MQTTMessage mqtt_message;
    mqtt_message.dup = 0;
    mqtt_message.payload = out->data;
    mqtt_message.payloadlen = out->size;
    mqtt_message.qos = QOS0;
    mqtt_message.retained = 0;


#ifdef RUNTIME_DEBUG
    printf("mqtt_static: about to send a message\n");
#endif
    int error;
    if(error = MQTTPublish(conf->client, the_topic, &mqtt_message)) {
        fprintf(stderr, "mqtt_static: could not send a message, topic %s, error %i\n",
                the_topic, error);
        status = EXIT_FAILURE;
        mqtt_done(conf, 0);
    } else {
#ifdef RUNTIME_DEBUG
    	printf("sent a message, client %s, topic %s\n", mqtt_opts.clientid, topic);
#endif
    }
    return status;
}

/**
 * @see the header file
 */
int mqtt_receive(mqtt_config_t * conf, struct message *msg) {
    int status = RUNTIME_OK;
    MQTTYield(conf->client, 0);

    if(conf->subscribers_config==NULL)
    	return EXIT_FAILURE;
    mqtt_subscriber_config_t * subs_conf = conf->subscribers_config;

    char found_msg = 0;

    int i=-1;
    struct mqtt_buffer_t * buf;
    for(int i=0; i<subs_conf->topics_nb; i++)
    {
    	buf = &(subs_conf->mqtt_buffers[i]);
    	if(buf->message_present)
    	{
#ifdef RUNTIME_DEBUG
    		printf("mqtt_static: message present, topic %s\n", subs_conf->topics[i]);
#endif
    		buf->message_present = 0;
    		found_msg = 1;
    		break;
    	}
    }

    if(!found_msg)
    	return -1;

    msg->size = buf->size;
    memcpy(msg->data, buf->buffer, msg->size);
    msg->aadl_port_nb = subs_conf->topics_global_ports[i];

    return status;
}
