#include "FreeRTOS.h"
#include "FreeRTOS_Sockets.h"
#include "FreeRTOS_IP.h"
#include "FreeRTOSIPConfig.h"

#include "aadl_ports.h"
#include "aadl_modes_standard.h"
#include "aadl_multiarch.h"
#include "gtypes.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//struct freertos_sockaddr xClient;

//#define DEBUG

error_code_t processor_port_init_socket(processor_link_t * processor_port)
{
  error_code_t err = RUNTIME_OK;
  int sockid;
  int stat;
  struct freertos_sockaddr curr, *info;
  BaseType_t sock_type;
  BaseType_t protocol_type;
  socklen_t sockLen = sizeof(struct freertos_sockaddr);
  socket_config_t * socket_config = (socket_config_t *) processor_port->link_config;
  char * ip_addr = socket_config->ip_addr;
  
  if (processor_port->type == SOCKETS_UDP)
  {
    sock_type = FREERTOS_SOCK_DGRAM;
    protocol_type = FREERTOS_IPPROTO_UDP;
  }
  else
  {
    sock_type = FREERTOS_SOCK_STREAM;
    protocol_type = FREERTOS_IPPROTO_TCP;
  }
  
  Socket_t xSocket = FreeRTOS_socket( FREERTOS_AF_INET, sock_type, protocol_type );
  configASSERT( xSocket != FREERTOS_INVALID_SOCKET );
  
  if (processor_port->role == RECEIVER)
  {
    struct freertos_sockaddr xBindAddress;
    xBindAddress.sin_port = ( uint16_t ) atoi(socket_config->ip_port);
    xBindAddress.sin_port = FreeRTOS_htons( xBindAddress.sin_port );
    xBindAddress.sin_addr = FreeRTOS_inet_addr(socket_config->ip_addr);
    BaseType_t xRet = FreeRTOS_bind(xSocket, &xBindAddress, sockLen);    
#ifdef DEBUG
    if (xRet != 0)
      printf("binding failed, xRet %d\n",xRet);
#endif
  }
  else
  {
    //    BaseType_t xRet = FreeRTOS_bind( xSocket, NULL, sockLen);
#ifdef DEBUG
    //if (xRet != 0)
    //  printf("binding failed, xRet %d\n",xRet);
#endif
  }
  if (processor_port->type == SOCKETS_UDP || processor_port->role == SENDER)
    socket_config->socket_id = (void *) xSocket;
  
  if (processor_port->type == SOCKETS_TCP)
  {
    if (processor_port->role == RECEIVER)
    {
      //      vTaskDelay( 3000 / portTICK_PERIOD_MS); // Wait for 3 sec. to make sure connect is called before listen (not sure why this is necessary)
      BaseType_t ret_listen = FreeRTOS_listen(xSocket,10);
      if( ret_listen != 0)
      {
#ifdef DEBUG
	if(ret_listen==-pdFREERTOS_ERRNO_EOPNOTSUPP)
	  printf("try to listen on a closed socket\n");
	else
	  printf("listening failed %d\n", ret_listen);
#endif
      }
      
      struct freertos_sockaddr xClient;
      xClient.sin_addr=0;
      while(xClient.sin_addr==0)
	socket_config->socket_id = (void *) FreeRTOS_accept(xSocket, &xClient, &sockLen);
      
#ifdef DEBUG
      uint8_t connected_ipaddr[16];
      FreeRTOS_inet_ntoa(xClient.sin_addr, connected_ipaddr);
      printf("Accepted %s:%d\n", connected_ipaddr, FreeRTOS_ntohs(xClient.sin_port));
#endif

#if defined DEBUG
      if (socket_config->socket_id == FREERTOS_INVALID_SOCKET || socket_config->socket_id == NULL)
        printf("accepting socket failed");
#endif
      configASSERT( socket_config->socket_id != FREERTOS_INVALID_SOCKET );

      static const TickType_t xReceiveTimeOut = 0; // non-blocking receive
      FreeRTOS_setsockopt( socket_config->socket_id,
			   0,
			   FREERTOS_SO_RCVTIMEO,
			   &xReceiveTimeOut,
			   sizeof( xReceiveTimeOut ) );
      

    }
    else
    {
      struct freertos_sockaddr xRemoteAddress;
      xRemoteAddress.sin_port = ( uint16_t ) atoi(socket_config->ip_port);
      xRemoteAddress.sin_port = FreeRTOS_htons( xRemoteAddress.sin_port );
      xRemoteAddress.sin_addr = FreeRTOS_inet_addr(socket_config->ip_addr);
      BaseType_t ret_connect = FreeRTOS_connect(xSocket, &xRemoteAddress, sockLen);      
    }
  }
}

error_code_t receive_in_processor_socket(processor_link_t *link, struct message *msg)
{

    error_code_t status = RUNTIME_OK;
    
    socket_config_t * socket_config = (socket_config_t *) link->link_config;
    Socket_t xSocket = (Socket_t) socket_config->socket_id;
    static char cRxedData[sizeof(struct message)];
    uint32_t xClientLength = sizeof( struct freertos_sockaddr );
    long lBytesReceived = 0;
    
    if (link->type == SOCKETS_TCP)
    {

#if defined DEBUG
      if (socket_config->socket_id == FREERTOS_INVALID_SOCKET || socket_config->socket_id == NULL)
        printf("Invalid socket in receive\n");
#endif
      
      configASSERT( socket_config->socket_id != FREERTOS_INVALID_SOCKET );

      //      if(!FreeRTOS_issocketconnected(xSocket))
      //      {
      //        socklen_t sockLen = sizeof(struct freertos_sockaddr);
      //        struct freertos_sockaddr xClient;
      //	printf("REACC\n");
      //	socket_config->socket_id = (void *) FreeRTOS_accept(xSocket, &xClient, &sockLen);
      //	xSocket = (Socket_t) socket_config->socket_id;
      //      }
      lBytesReceived = FreeRTOS_recv(xSocket, &cRxedData, sizeof(struct message), 0 );
      
    }
      
    else if (link->type == SOCKETS_UDP)
    {
      struct freertos_sockaddr xClient;
      lBytesReceived = FreeRTOS_recvfrom(xSocket, &cRxedData, sizeof(struct message), 0, &xClient, &xClientLength );
    }
      
    if( lBytesReceived > 0 ) {
#if defined DEBUG && defined USE_FREERTOS
      printf("Received ... OK\n");
#endif
      memcpy(msg, cRxedData, sizeof(struct message));
    }
    else
      {
	if(lBytesReceived == 0)
	  {
#if defined DEBUG && defined USE_FREERTOS
            printf("Receive failed with timeout\n");
#endif
	    return -1;
	  }
#if defined DEBUG && defined USE_FREERTOS
	else if (lBytesReceived == -pdFREERTOS_ERRNO_ENOMEM)
	  printf("Receive failed, memory error\n");
	else if (lBytesReceived == -pdFREERTOS_ERRNO_ENOTCONN)
	  printf("Receive failed, socket closed\n");
	else if (lBytesReceived == -pdFREERTOS_ERRNO_EINVAL)
	  printf("Receive failed, invalid socket\n");
	else
	  printf("Receive failed, unidentified reason\n");
#endif
	return lBytesReceived;
	
      }
    return status;
}

error_code_t send_out_processor_socket(processor_destination_port_t * dst_port, struct message msg_sent)
{
  error_code_t err = RUNTIME_OK;
  int res = 0;

  BaseType_t xAlreadyTransmitted = 0;
  BaseType_t xBytesSent = 0;
  BaseType_t data_sent;

  socket_config_t * socket_config = (socket_config_t *) dst_port->link->link_config;
  Socket_t xSocket = (Socket_t) socket_config->socket_id;
  socklen_t sockLen = sizeof(struct freertos_sockaddr);
  
  struct freertos_sockaddr xRemoteAddress;
  xRemoteAddress.sin_port = ( uint16_t ) atoi(socket_config->ip_port);
  xRemoteAddress.sin_port = FreeRTOS_htons( xRemoteAddress.sin_port );
  xRemoteAddress.sin_addr = FreeRTOS_inet_addr(socket_config->ip_addr);

#if defined DEBUG && defined USE_FREERTOS
  printf("Remote Addr %s\n", socket_config->ip_addr);
  printf("Remote port %d\n", xRemoteAddress.sin_port);
#endif
  
  if (dst_port->link->type == SOCKETS_UDP)
  {
    FreeRTOS_sendto(xSocket, &msg_sent, sizeof(struct message), FREERTOS_ZERO_COPY, &xRemoteAddress, sockLen);
    
    if (data_sent != msg_sent.size)
    {
#if defined DEBUG && defined USE_FREERTOS
      printf("Error while sending\n");
#endif
    }
  }
  else if (dst_port->link->type == SOCKETS_TCP)
  {
    if(!FreeRTOS_issocketconnected(xSocket))
    {
      struct freertos_sockaddr xRemoteAddress;
      xRemoteAddress.sin_port = ( uint16_t ) atoi(socket_config->ip_port);
      xRemoteAddress.sin_port = FreeRTOS_htons( xRemoteAddress.sin_port );
      xRemoteAddress.sin_addr = FreeRTOS_inet_addr(socket_config->ip_addr);
      BaseType_t ret_connect = FreeRTOS_connect(xSocket, &xRemoteAddress, sockLen);
    }
    while( xAlreadyTransmitted < msg_sent.size )
    {
      xBytesSent = FreeRTOS_send(xSocket, &( msg_sent ),sizeof(struct message), 0 );
#if defined DEBUG && defined USE_FREERTOS	    
      printf("Sent %d out of %d\n", xBytesSent, msg_sent.size);
#endif
      if( xBytesSent >= 0 )
      {
	xAlreadyTransmitted += xBytesSent;
      }
      else
      {
	err = xBytesSent;
#if defined DEBUG && defined USE_FREERTOS
	if(xBytesSent == -pdFREERTOS_ERRNO_ENOTCONN) {
          printf("Send failed: socket was closed\n");
        }
	else if(xBytesSent == -pdFREERTOS_ERRNO_ENOMEM)
	  printf("Send failed: insufficient memory\n");
	else if(xBytesSent == -pdFREERTOS_ERRNO_EINVAL)
	  printf("Send failed: invalid socket\n");
	else if (xBytesSent == -pdFREERTOS_ERRNO_ENOSPC)
	  printf("Send failed: timeout\n");
	else
	  printf("Unidentified error while sending\n");
#endif
	break;
      }
    }
  }
  return err; 
}

error_code_t close_processor_port(processor_link_t * processor_port)
{
  error_code_t err = RUNTIME_OK;
  socket_config_t * socket_config = (socket_config_t *) processor_port->link_config;
  Socket_t * xSocket = (Socket_t *) socket_config->socket_id;

  char * buffer;
  const size_t len = 1;
  TickType_t current_tick = xTaskGetTickCount();
  TickType_t delayed_tick;
  
  FreeRTOS_shutdown(*xSocket, FREERTOS_SHUT_RDWR);

  // Wait for the socket to disconnect gracefully before closing the socket
  while( FreeRTOS_recv(*xSocket, buffer, len, 0 ) != FREERTOS_EINVAL )
  {
    vTaskDelay( 0.05 * configTICK_RATE_HZ );
    delayed_tick = xTaskGetTickCount();

    //timeout to prevent infinite loop
    if (delayed_tick > current_tick + 2 * configTICK_RATE_HZ)
    {
      err = RUNTIME_SYSCALL_ERROR;
    #ifdef DEBUG
      printf("socket shutdown failed\n");
    #endif
      break;
    }
  }
  // the socket has shut down and is safe to close
  FreeRTOS_closesocket(*xSocket);

  return err;
}

extern uint32_t ulApplicationGetNextSequenceNumber( uint32_t 
ulSourceAddress,
     uint16_t usSourcePort,
     uint32_t ulDestinationAddress,
     uint16_t usDestinationPort )
{    
     ( void ) ulSourceAddress;
     ( void ) usSourcePort;
     ( void ) ulDestinationAddress;
     ( void ) usDestinationPort;

     return rand();
}

BaseType_t xApplicationGetRandomNumber( uint32_t *pulValue )
{
  *pulValue = rand();
  return pdPASS;
}

