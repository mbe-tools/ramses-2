/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include <pthread.h>
#include <time.h>
#include <signal.h>
#include <arpa/inet.h>
#include <err.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

#include "aadl_ports.h"
#include "aadl_modes_standard.h"
#include "aadl_multiarch.h"
#include "gtypes.h"

struct addrinfo *dst;
struct addrinfo client;

error_code_t processor_port_init_socket(processor_link_t * processor_port)
{
  error_code_t err = RUNTIME_OK;
  int sockid;
  int stat;
  struct addrinfo curr, *info;
  socklen_t struct_size = sizeof(struct addrinfo);

  
  memset(&curr, 0, sizeof(curr));

  socket_config_t * socket_config = (socket_config_t *) processor_port->link_config;

  curr.ai_family = AF_INET;
  if (processor_port->type == SOCKETS_UDP)
    curr.ai_socktype = SOCK_DGRAM;
  else
    curr.ai_socktype = SOCK_STREAM;
  curr.ai_addr = htonl(INADDR_ANY);

  if ((stat = getaddrinfo(socket_config->ip_addr, socket_config->ip_port, &curr, &info)) != 0)
  {
#if defined DEBUG && defined USE_POSIX
    warn("error on getaddrinfo");
#endif
    return 1;
  }

  for (dst = info; dst != NULL; dst = dst->ai_next)
  {
    if((sockid = socket(dst->ai_family, dst->ai_socktype,
	    dst->ai_protocol)) == -1)
      continue;

    int enable = 1;
    stat = setsockopt(sockid, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &enable, sizeof(int));
#if defined DEBUG && defined USE_POSIX
    if ( stat < 0)
        perror("setsockopt(SO_REUSEADDR) failed");
#endif

    if (processor_port->role == RECEIVER)
    {
      if (bind(sockid, dst->ai_addr, dst->ai_addrlen) == -1)
      {
#if defined DEBUG && defined USE_POSIX
	perror("");
#endif
      }
    }
    break;
  }
  socket_config->socket_id = sockid;

  if (processor_port->type == SOCKETS_TCP)
  {
    if (processor_port->role == RECEIVER)
    {
      if(listen(sockid,1) == -1)
      {
#if defined DEBUG && defined USE_POSIX
	perror("");
#endif
      }
      socket_config->socket_id = accept(sockid, dst->ai_addr, &struct_size);
      if (socket_config->socket_id == -1)
      {
#if defined DEBUG && defined USE_POSIX
	perror("");
#endif
      }
      fcntl(socket_config->socket_id, F_SETFL, O_NONBLOCK);
    }
    else
    {
      //An improvement would be to add a timeout, in case the address is not
      //responding
      while (connect(sockid, dst->ai_addr,struct_size)==-1)
    	  continue;
    }

  }

  return err;
}

error_code_t receive_in_processor_socket(processor_link_t *link, struct message *msg)
{


  //      printf("STATUS???\n");
    error_code_t status = RUNTIME_OK;
    ssize_t res = -1;
    char buf[sizeof(struct message)];
    socklen_t addr_len = sizeof(client.ai_addr);
    socket_config_t * socket_config = (socket_config_t *) link->link_config;
    if (link->type == SOCKETS_UDP)
      res = recvfrom(socket_config->socket_id,buf, sizeof(struct message),0,client.ai_addr,&addr_len);
    else if (link->type == SOCKETS_TCP)
      res = recv(socket_config->socket_id,buf,sizeof(struct message),0);
    if (res == -1)
    {
      status = res;
#if defined RUNTIME_DEBUG && defined USE_POSIX
      perror("");
#endif
    }
    if(!status)
      memcpy(msg,buf, sizeof(struct message));

    //printf("STATUS %d\n", status);
    
    return status;
}

error_code_t send_out_processor_socket(processor_destination_port_t * dst_port, struct message
    msg_sent)
{
    error_code_t err = RUNTIME_OK;
    ssize_t res = 0;
  socket_config_t * socket_config = (socket_config_t *) dst_port->link->link_config;
    if (dst_port->link->type == SOCKETS_UDP)
    {
      res = sendto(socket_config->socket_id,&msg_sent,sizeof(struct message),0,
	  dst->ai_addr, dst->ai_addrlen);
    }
    else if (dst_port->link->type == SOCKETS_TCP)
      res = send(socket_config->socket_id,&msg_sent,sizeof(struct message),0);

    if (res == -1)
    {
#if defined DEBUG && defined USE_POSIX
      perror("Error while sending");
#endif
    }
    //    printf("SEND OUT PROCESSOR %d, %d\n", res,socket_config->socket_id);
    return err;
}

error_code_t close_processor_port(processor_link_t * processor_port)
{
  error_code_t err = RUNTIME_OK;
  socket_config_t * socket_config = (socket_config_t *) processor_port->link_config;
  close(socket_config->socket_id);
}
