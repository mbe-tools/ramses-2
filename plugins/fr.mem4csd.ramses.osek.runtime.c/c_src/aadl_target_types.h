/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef TARGET_TYPES_H
#define TARGET_TYPES_H

#include "runtime_stdint.h"

#ifdef USE_NXTOSEK

#include "kernel.h"
#define NULL 0

#include <stddef.h>

extern size_t strlen(const char *str);
char* itoa(int num, char* str, int base);
char *strcat(char *dest, const char *src);

#endif // USE_NXTOSEK

struct thread_config_t;

typedef struct target_wait_mode_t
{
  ResourceType * rez;
  EventMaskType * event;    
} target_wait_mode_t;

typedef struct target_wait_message_t
{
  ResourceType * rez;
  EventMaskType * event;
} target_wait_message_t;

typedef struct target_lock_t
{
  ResourceType * rez;
} target_lock_t;

typedef struct target_thread_t
{
  TaskType * tid;
} target_thread_t;

typedef struct target_periodic_thread_config
{
  struct thread_config_t * parent;
  uint32_t iteration_counter;
} target_periodic_thread_config_t;

typedef struct target_sporadic_thread_config
{
  struct thread_config_t * parent;
  AlarmType alrm;
} target_sporadic_thread_config_t;

typedef struct target_aperiodic_thread_config
{
  struct thread_config_t * parent;
  AlarmType alrm;
} target_aperiodic_thread_config_t;

typedef struct target_timetriggered_thread_config
{
  struct thread_config_t * parent;
  AlarmType alrm;
} target_timetriggered_thread_config_t;

typedef struct target_hybrid_thread_config
{
  struct thread_config_t * parent;
  AlarmType alrm;
} target_hybrid_thread_config_t;

typedef struct target_timed_thread_config
{
  struct thread_config_t * parent;
  AlarmType alrm;
} target_timed_thread_config_t;

typedef struct target_process_port_t
{
    
} target_process_port_t;

typedef struct target_processor_port_t
{
    
} target_processor_port_t;

typedef uint64_t target_time_t;

typedef struct target_watchdog_t
{
} target_watchdog_t;

#endif // TARGET_TYPES_H

