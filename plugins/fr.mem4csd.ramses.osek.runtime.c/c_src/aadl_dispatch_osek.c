/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_multiarch.h"

int init_thread_config_multiarch(thread_config_t * config)
{
  (void) config;
  return 0; // nothing to do
}

int init_periodic_config_multiarch(target_periodic_thread_config_t * config)
{
  (void) config;
  return 0;
}

error_code_t Await_Mode_multiarch(thread_config_t * config)
{
//  if(!Is_In_Current_Mode(config))
//	  TerminateTask();
  return 0;
}

uint64_t start_time = 0;

error_code_t await_periodic_dispatch_multiarch(target_periodic_thread_config_t * config)
{
  (void) config;
  error_code_t ret = E_OK;// dispatch in osek can be managed by the periodic alarm, so doesn't need C code
  config->iteration_counter++;
  config->parent->dispatch_time = start_time+config->iteration_counter*config->parent->period+config->parent->offset;
  TerminateTask(); // Alarm activates the task
  return ret;
}

int is_period_passed(target_hybrid_thread_config_t * config)
{
  (void) config;
  return 0;
}

void sporadic_thread_sleep(target_sporadic_thread_config_t * config)
{
  uint32_t period = config->parent->period/1000000;
  uint32_t dispatch_time = config->parent->dispatch_time/1000000;
  SetAbsAlarm(config->alrm, dispatch_time+period, 0); // 0 means one-shot.
  TerminateTask(); // Alarm activates the task
}

error_code_t init_hybrid_timer(target_hybrid_thread_config_t * config)
{
  return RUNTIME_OK; // this is managed by the cyclic alarm
}

error_code_t hybrid_reset_timer(target_hybrid_thread_config_t * info)
{
  return RUNTIME_OK; // this is managed by the cyclic alarm
}

error_code_t timedwait_hybrid(target_hybrid_thread_config_t * info)
{
  error_code_t status = wait_port_list(info->parent->global_q);
  get_time_multiarch(&info->parent->dispatch_time);
  return status;
}

error_code_t timedwait_timed_thread(target_timed_thread_config_t * info)
{
  int msec = info->parent->period/1000000;
  StatusType status = SetRelAlarm(info->alrm, msec, 0);
  if(status!=E_OK)
    return RUNTIME_SYSCALL_ERROR;
  status = wait_port_list(info->parent->global_q);
  get_time_multiarch(&info->parent->dispatch_time);
  if(status!=E_OK)
    return RUNTIME_SYSCALL_ERROR;
  CancelAlarm(info->alrm);
  return RUNTIME_OK;
}

extern unsigned long osek_absolute_time_ms;
void get_time_multiarch(target_time_t * time)
{
	*time = osek_absolute_time_ms*1000000;
}

void get_start_time(target_time_t * time)
{
  *time = start_time;
}

void set_start_time_multiarch()
{
  start_time = 0;
  return;
}

void init_dispatch_configuration(thread_config_t * config)
{
  switch (config->protocol)
  {
    case HYBRID:
      {
	target_hybrid_thread_config_t * hyb_thr = config->thread;
	int period = config->period/1000000;
	SetAbsAlarm(hyb_thr->alrm, period, period);
	break;
      }
    default:
      break;
  }
}
