/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "ecrobot_interface.h"

#include "aadl_multiarch.h"

size_t strlen(const char *str)
{
  unsigned int i=0;
  while(str[i]!='\0')
	i++;
  return i-1;
}

char * strcat(char *dest, const char *src)
{
    size_t i,j;
    for (i = 0; dest[i] != '\0'; i++)
        ;
    for (j = 0; src[j] != '\0'; j++)
        dest[i+j] = src[j];
    dest[i+j] = '\0';
    return dest;
}

void swap(char c1, char c2)
{
  char temp = c1;
  c1 = c2;
  c2 = temp;
}

/* A utility function to reverse a string  */
void reverse(char str[], int length)
{
    int start = 0;
    int end = length -1;
    while (start < end)
    {
        swap(*(str+start), *(str+end));
        start++;
        end--;
    }
}

// Implementation of itoa()
char* itoa(int num, char* str, int base)
{
    int i = 0;
    char isNegative = 0;

    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }

    // In standard itoa(), negative numbers are handled only with
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = 1;
        num = -num;
    }

    // Process individual digits
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
        num = num/base;
    }

    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';

    str[i] = '\0'; // Append string terminator

    // Reverse the string
    reverse(str, i);

    return str;
}

error_code_t lock_port_reference(thread_input_port_t * port)
{
  return GetResource(*(port->parent->event->rez));
}

error_code_t unlock_port_reference(thread_input_port_t * port)
{
  return ReleaseResource(*(port->parent->event->rez));
}

error_code_t send_signal(thread_input_port_t * port)
{
  TaskType * tid = (TaskType *) port->parent->parent->target_thread->tid;
  return SetEvent(*tid, *(port->parent->event->event));
}

error_code_t send_out_process(port_reference_t * source,
		void * value,
		unsigned int msg_size)
{
	return RUNTIME_INVALID_SERVICE_CALL; // Not available on osek.
}

error_code_t receive_in_process(port_reference_t * destination,
		void * received_msg)
{
	return RUNTIME_INVALID_SERVICE_CALL; // Not available on osek.
}

error_code_t lock_data(lock_access_protocol_t * protocol)
{
  return GetResource(*(protocol->rez->rez));
}

error_code_t unlock_data(lock_access_protocol_t * protocol)
{
  return ReleaseResource(*(protocol->rez->rez));
}

error_code_t icpp_lock_data(icpp_access_protocol_t * protocol)
{
  return -1; // ICPP is not supported by OSEK platforms
}

error_code_t icpp_unlock_data(icpp_access_protocol_t * protocol)
{
  return -1; // ICPP is not supported by OSEK platforms
}

int init_port_list_mutex(port_list_t * info)
{
  return 0;
}

error_code_t lock_port_list(port_list_t * port_list)
{
  return GetResource(*(port_list->event->rez));
}

error_code_t unlock_port_list(port_list_t * port_list)
{
  return ReleaseResource(*(port_list->event->rez));
}

error_code_t wait_port_list(port_list_t * port_list)
{
  ReleaseResource(*(port_list->event->rez));
  return WaitEvent(*(port_list->event->event));
}

error_code_t await_time_triggered_dispatch_multiarch(target_timetriggered_thread_config_t * config)
{
	// not implemented yet
	return -100;
}

error_code_t init_time_triggered_config(target_timetriggered_thread_config_t * config)
{
	// nothing to do here
	return 0;
}
