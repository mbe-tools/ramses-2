/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef TARGET_TYPES_H
#define TARGET_TYPES_H

#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <stdint.h>

#define USE_PRINTF
#include <stdio.h>
#include <stdarg.h>

struct thread_config_t;

typedef struct target_wait_mode_t
{
  pthread_mutex_t rez;
  pthread_cond_t event;
} target_wait_mode_t;

typedef struct target_wait_message_t
{
  pthread_mutex_t rez;
  pthread_cond_t event;
} target_wait_message_t;

typedef struct target_lock_t 
{
  pthread_mutex_t mutex;
} target_lock_t;

typedef struct target_thread_t
{
  pthread_t thread_id;
} target_thread_t;

typedef uint64_t target_time_t;

typedef struct target_watchdog_t
{
  int wd_alrm;
  timer_t timer_id;
  struct itimerspec tDef;
  struct periodic_thread_config *task_descriptor;
} target_watchdog_t;

typedef struct target_periodic_thread_config
{
    uint64_t iteration_counter;
    struct thread_config_t * parent;
} target_periodic_thread_config_t;

typedef struct target_sporadic_thread_config
{  
  struct thread_config_t * parent;
} target_sporadic_thread_config_t;

typedef struct target_aperiodic_thread_config
{
  struct thread_config_t * parent;
} target_aperiodic_thread_config_t;

typedef struct target_timetriggered_thread_config
{
  struct thread_config_t * parent;
  pthread_mutex_t rez;
  pthread_cond_t event;
} target_timetriggered_thread_config_t;

typedef struct target_hybrid_thread_config
{
  struct thread_config_t * parent;
  uint64_t iteration_counter;
} target_hybrid_thread_config_t;

typedef struct target_timed_thread_config
{
  struct thread_config_t * parent;
} target_timed_thread_config_t;

typedef struct target_process_port_t
{
    
} target_process_port_t;

typedef struct target_processor_port_t
{
    
} target_processor_port_t;

#endif // TARGET_TYPES_H

