/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include <pthread.h>
#include <time.h>
#include <signal.h>
#include <arpa/inet.h>
#include <err.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "aadl_multiarch.h"
#include "gtypes.h"



extern process_config_t this_process;
extern processor_inputport_addr global_to_local[];
extern struct timespec start_timespec;

// for aadl_port
error_code_t lock_port_reference(thread_input_port_t * port)
{
#if defined RUNTIME_DEBUG && defined USE_POSIX
  printf("Lock port reference\n");
#endif
  int sys_ret = pthread_mutex_lock(&port->parent->event->rez);
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t unlock_port_reference(thread_input_port_t * port)
{
#if defined RUNTIME_DEBUG && defined USE_POSIX
  printf("Unlock port reference\n");
#endif
  int sys_ret = pthread_mutex_unlock(&port->parent->event->rez);
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t send_signal(thread_input_port_t * port)
{
  int sys_ret = pthread_cond_signal(&port->parent->event->event);
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t send_out_process(port_reference_t * source,
    void * value,
    unsigned int msg_size)
{
  return RUNTIME_INVALID_SERVICE_CALL; // Not available on osek.
}

error_code_t receive_in_process(port_reference_t * destination,
    void * received_msg)
{
  return RUNTIME_INVALID_SERVICE_CALL; // Not available on osek.
}

error_code_t Init_Resource(resource_t * resource)
{
  error_code_t status = RUNTIME_OK;
  pthread_mutexattr_t mutex_attr;
  switch(resource->protocol)
  {
  case NONE:
    return status;
  case LOCK:
    {
      lock_access_protocol_t * protocol = (lock_access_protocol_t*) resource->protocol_config;
      pthread_mutex_init(&protocol->rez->mutex, NULL);
      break;
    }
  case ICPP:
    {
      icpp_access_protocol_t * protocol = (icpp_access_protocol_t*) resource->protocol_config;
      pthread_mutexattr_setprotocol(&mutex_attr, PTHREAD_PRIO_PROTECT);
      pthread_mutexattr_setprioceiling(&mutex_attr, protocol->ceiling_priority);
      pthread_mutex_init(&protocol->lock->rez->mutex, &mutex_attr);
      break;
    }
  }
  return status;
}

error_code_t lock_data(lock_access_protocol_t * protocol)
{
  int sys_ret = pthread_mutex_lock(&protocol->rez->mutex);
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t unlock_data(lock_access_protocol_t * protocol)
{
  int sys_ret = pthread_mutex_unlock(&protocol->rez->mutex);
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t icpp_lock_data(icpp_access_protocol_t * protocol)
{
  int sys_ret = lock_data(protocol->lock);
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t icpp_unlock_data(icpp_access_protocol_t * protocol)
{
  int sys_ret = unlock_data(protocol->lock);
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

int init_port_list_mutex(port_list_t * info)
{
  int sys_ret = pthread_cond_init(&(info->event->event), NULL);
  sys_ret |= pthread_mutex_init(&(info->event->rez), NULL);
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t lock_port_list(port_list_t * port_list)
{
#if defined RUNTIME_DEBUG && defined USE_POSIX
  printf("Lock port list\n");
#endif
  int sys_ret = pthread_mutex_lock(&(port_list->event->rez));
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t unlock_port_list(port_list_t * port_list)
{
#if defined RUNTIME_DEBUG && defined USE_POSIX
  printf("Unlock port list\n");
#endif
  int sys_ret = pthread_mutex_unlock(&(port_list->event->rez));
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t wait_port_list(port_list_t * port_list)
{
  int sys_ret = pthread_cond_wait(&(port_list->event->event),
      &(port_list->event->rez));
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

