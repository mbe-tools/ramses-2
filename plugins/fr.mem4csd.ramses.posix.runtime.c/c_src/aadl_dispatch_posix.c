/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */


#define _GNU_SOURCE 1
#include "aadl_multiarch.h"
#include "aadl_modes_standard.h"
#include <pthread.h>
#include <time.h>
#include <sched.h>
#include <limits.h>
#include <unistd.h>
#include <errno.h>

#if defined DEBUG
#include <stdio.h>
#endif

uint64_t target_start_time = 0;

int bind_thread_to_core_set(pthread_t * config, cpu_set_t * cpuset);

error_code_t create_thread (unsigned int priority,
		   unsigned int stack_size,
		   void (*start_routine)(void),
		   pthread_t * tid,
		   int policy,
		   cpu_set_t * cpuset);

void get_time_multiarch(target_time_t * time)
{
  struct timespec current_time;
  clock_gettime(CLOCK_REALTIME, &current_time);
  *time = (((uint64_t)current_time.tv_sec) *1000000000
	   + (uint64_t) current_time.tv_nsec) - target_start_time;
}

void get_start_time(uint64_t * time)
{
  *time = target_start_time;
}

void set_start_time_multiarch()
{
  struct timespec start_timespec;
  clock_gettime(CLOCK_REALTIME, &start_timespec);
  target_start_time = ((uint64_t)start_timespec.tv_sec)*1000000000
    + (uint64_t)start_timespec.tv_nsec;
  uint64_t oth = start_timespec.tv_sec*1000000000;
}

error_code_t set_thread_priority_multiarch(thread_config_t * config, unsigned int priority)
{
	error_code_t ret = RUNTIME_OK;
	pthread_t * tid = (pthread_t*) config->target_thread->thread_id;
	if(pthread_setschedprio(*tid, priority))
		return RUNTIME_SYSCALL_ERROR;
}

int init_thread_config_multiarch(thread_config_t * config)
{
  if(pthread_cond_init(&config->wait_mode_event->event, NULL))
    return RUNTIME_SYSCALL_ERROR;
  else
    return RUNTIME_OK;
}

int init_periodic_config_multiarch(target_periodic_thread_config_t * config)
{
  int ret = 0;
  config->iteration_counter=0;
  return ret;
}

extern pthread_mutex_t init_barrier_mutex;
extern pthread_cond_t init_barrier_cond;
extern unsigned long reached_init_barrier;
extern mode_id_t current_mode;

error_code_t Await_Mode_multiarch(thread_config_t * config)
{
  error_code_t ret = RUNTIME_OK;
  int sys_ret = 0;
  sys_ret |= pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  sys_ret |= pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
  sys_ret |= pthread_mutex_lock(&init_barrier_mutex);
  if(config->state==INITIALIZING_THREAD_STATE
		  && Is_In_Mode(config, current_mode))
	  reached_init_barrier++;
  sys_ret |= pthread_cond_signal(&init_barrier_cond);
#if defined RUNTIME_DEBUG
  printf("Thread %s initialized\n", config->name);
#endif
  config->state=AWAITING_MODE_THREAD_STATE;
  sys_ret |= pthread_cond_wait(&config->wait_mode_event->event, &init_barrier_mutex);
  struct timespec offset_timespec;
  sys_ret |= pthread_mutex_unlock(&init_barrier_mutex);
#if defined RUNTIME_DEBUG
  printf("Thread %s released in current mode\n", config->name);
#endif
  switch (config->protocol)
    {
      case PERIODIC:
      {
        target_periodic_thread_config_t * per_thr = (target_periodic_thread_config_t *) config->thread;
        per_thr->iteration_counter=0;
        if(per_thr->parent->offset)
        {
        	uint64_t sec_off = per_thr->parent->offset/1000000000;
        	uint64_t ns_off = per_thr->parent->offset%1000000000;
        	offset_timespec.tv_sec = target_start_time/1000000000+sec_off;
        	offset_timespec.tv_nsec = target_start_time%1000000000+ns_off;
        	clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &offset_timespec, NULL);
        }
        break;
      }
      default:
	break;
    }
  return ret;
}

error_code_t release_task_in_mode_multiarch(thread_config_t * config)
{
	error_code_t ret = RUNTIME_OK;
	int sys_ret = pthread_cond_signal(&config->wait_mode_event->event);
	if(sys_ret)
		ret = RUNTIME_SYSCALL_ERROR;
	return ret;
}

error_code_t create_thread_multiarch(thread_config_t * config)
{
	error_code_t ret = RUNTIME_OK;
	// nothing to do here (start_thread_multiarch also create
	// thread on POSIX
	return ret;
}

error_code_t start_thread_multiarch(thread_config_t * config)
{
	error_code_t ret = RUNTIME_OK;


	int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);

	// TODO: get core_id array length should be retreived from config
	int core_id_length = 1; // config->core_arra_length;
	int core_id = 0;
	int idx;
	for(idx=0; idx<core_id_length; idx++)
	{
	  // TODO: get core_id from array
	  // core_id = config->core_array[idx];
	  if (core_id < 0 || core_id > num_cores-1)
	  {
#if defined DEBUG
	      printf("Error: invalid core_id when binding thread %d\n", core_id);
#endif
	      return RUNTIME_INVALID_PARAMETER;
	  }
	  CPU_SET(core_id, &cpuset);
	  
	}
	ret = create_thread(config->priority,
			PTHREAD_STACK_MIN,
			config->compute_entrypoint,
			&config->target_thread->thread_id,
			SCHED_FIFO,
			&cpuset);
	return ret;
}

error_code_t stop_thread_multiarch(thread_config_t * config)
{
	pthread_t * tid = (pthread_t*) config->target_thread->thread_id;
	int sys_ret = pthread_cancel(*tid);
	if(sys_ret)
		return RUNTIME_SYSCALL_ERROR;
	else
		return RUNTIME_OK;
}

error_code_t await_periodic_dispatch_multiarch(target_periodic_thread_config_t * info)
{
  error_code_t ret = RUNTIME_OK;
  
  info->iteration_counter++;

  uint64_t next_dispatch = target_start_time+(info->iteration_counter*info->parent->period)+info->parent->offset;

  struct timespec periodic_timespec;
  periodic_timespec.tv_sec = next_dispatch/1000000000;
  periodic_timespec.tv_nsec = next_dispatch%1000000000;

#ifdef RUNTIME_DEBUG
  printf("Periodic wait until %ld sec %ld ns, with period (nsec)=%lu\n",periodic_timespec.tv_sec, periodic_timespec.tv_nsec, info->parent->period);
#endif
    
  clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &periodic_timespec, NULL);

  info->parent->dispatch_time = next_dispatch-target_start_time;
  
#ifdef RUNTIME_DEBUG
  target_time_t t;
  get_time_multiarch(&t);
  printf("Periodic dispatch of %s, @t=%lu\n", info->parent->name, t);
#endif

  return ret;
}

void sporadic_thread_sleep(target_sporadic_thread_config_t * info)
{
  uint64_t elapsed_time = info->parent->finish_time - info->parent->dispatch_time;
  if(elapsed_time==0 || 
	elapsed_time >= info->parent->period)
    return;

  struct timespec sporadic_timespec;
  sporadic_timespec.tv_sec = (target_start_time+info->parent->dispatch_time + info->parent->period)/1000000000;
  sporadic_timespec.tv_nsec = (target_start_time+info->parent->dispatch_time + info->parent->period)%1000000000;
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
  printf("Sporadic wait until sec %ld nsec %ld, period (ns) = %lu\n", sporadic_timespec.tv_sec, sporadic_timespec.tv_nsec, info->parent->period);
  printf("Dispatch time was %lu nsec\n", info->parent->dispatch_time);
#endif

  clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &sporadic_timespec, NULL);
}

error_code_t init_hybrid_timer(target_hybrid_thread_config_t * info)
{
  info->iteration_counter = 0;
  return RUNTIME_OK;
}

error_code_t hybrid_reset_timer(target_hybrid_thread_config_t * info)
{
  info->iteration_counter += 1;
  return RUNTIME_OK;
}

error_code_t timedwait_hybrid(target_hybrid_thread_config_t * info)
{
  uint64_t period = info->parent->period;

  uint64_t next_dispatch = target_start_time+info->iteration_counter*period;

  struct timespec next_period;
  next_period.tv_sec = next_dispatch/1000000000;
  next_period.tv_nsec = next_dispatch%1000000000 ;

  int sys_ret = pthread_cond_timedwait(&info->parent->global_q->event->event,
                                   &info->parent->global_q->event->rez,
                                   &next_period);

  if(sys_ret==ETIMEDOUT)
    info->parent->dispatch_time = next_dispatch;
  else if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  else
    return RUNTIME_OK;
}

error_code_t timedwait_timed_thread(target_timed_thread_config_t * info)
{
  // relative timeout
  struct timespec timed_timespec;
  clock_gettime(CLOCK_REALTIME, &timed_timespec);
  uint64_t now = timed_timespec.tv_sec*1000000000+timed_timespec.tv_nsec;
  uint64_t next_dispatch = now+info->parent->period;
  timed_timespec.tv_sec = next_dispatch/1000000000;
  timed_timespec.tv_nsec = next_dispatch%1000000000;

#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
  printf("Timed wait until sec %ld nsec %ld, timeout (ms) = %lu\n", timed_timespec.tv_sec, timed_timespec.tv_nsec, info->parent->period);
  printf("Dispatch time was %ld nsec\n", info->parent->dispatch_time);
#endif
  int ret = pthread_cond_timedwait(&info->parent->global_q->event->event,
                                   &info->parent->global_q->event->rez,
				   &timed_timespec);
  if(ret==ETIMEDOUT)
    info->parent->dispatch_time = next_dispatch;
  else if(ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t create_thread (unsigned int priority,
		   unsigned int stack_size,
		   void (*start_routine)(void),
		   pthread_t * tid,
		   int policy,
		   cpu_set_t * cpuset)
{
  int         sys_ret;
  pthread_attr_t     attr;
  struct sched_param param;
  
  sys_ret = pthread_attr_init (&attr);
  if (sys_ret != 0)
    {
#if defined DEBUG
      printf("Error: thread attr init %d\n", sys_ret);
#endif
      return RUNTIME_SYSCALL_ERROR;
    }

  sys_ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
#if defined DEBUG
  if(sys_ret != 0)
  	printf("Error: set inherit sched\n");
#endif

  sys_ret = pthread_attr_setscope (&attr, PTHREAD_SCOPE_SYSTEM);
  if (sys_ret != 0)
    {
#ifdef DEBUG
      printf("Error: thread attr set scope %d\n", sys_ret);
#endif
      return RUNTIME_SYSCALL_ERROR;
    }

  if (stack_size != 0)
    {
  // On some systems, pthread_attr_setstacksize() can fail with the error
  // EINVAL if stacksize is not a multiple of the system page size.
      sys_ret = pthread_attr_setstacksize (&attr, stack_size);
      if (sys_ret != 0)
	{
#ifdef DEBUG
          printf("Error: thread attr set stack size %d\n", sys_ret);
#endif
	  return RUNTIME_SYSCALL_ERROR;
	}
    }

  sys_ret = pthread_attr_setschedpolicy(&attr, policy);
#if defined DEBUG
  if(sys_ret != 0)
  {
  	printf("Error: set sched policy\n");
  	return RUNTIME_SYSCALL_ERROR;
  }
#endif

  param.sched_priority = priority;
  sys_ret = pthread_attr_setschedparam (&attr, &param);
  if (sys_ret != 0)
    {
#ifdef DEBUG
      printf("Error: thread attr set sched param %d\n", sys_ret);
#endif
      return RUNTIME_SYSCALL_ERROR;
    }

  sys_ret = pthread_create (tid, &attr, (void* (*)(void*))start_routine, NULL);

  if(sys_ret != 0)
  {
#if defined DEBUG
  	printf("Error: pthread_create\n");
#endif
  	return RUNTIME_SYSCALL_ERROR;
  }

  if(cpuset == NULL)
  {
    cpu_set_t default_cpuset;
    CPU_ZERO(&default_cpuset);
    CPU_SET(0, &default_cpuset);
    sys_ret = bind_thread_to_core_set((pthread_t*)tid, &default_cpuset);
  }
  else
    sys_ret = bind_thread_to_core_set((pthread_t*)tid, cpuset);

  return RUNTIME_OK;
}

int bind_thread_to_core_set(pthread_t * tid, cpu_set_t * cpuset) {
  
  int sys_ret = pthread_setaffinity_np(*tid, sizeof(cpu_set_t), cpuset);
#if defined DEBUG
  if(sys_ret != 0)
  	printf("Error: set affinity failed\n");
#endif
  return sys_ret;

}

error_code_t bind_thread_to_core_id(thread_config_t * config, uint8_t core_id) {
  int sys_ret = 0;
  cpu_set_t cpuset;
  pthread_t * tid = (pthread_t*) config->target_thread->thread_id;
	
  CPU_ZERO(&cpuset);
  CPU_SET(core_id, &cpuset);
  sys_ret = pthread_setaffinity_np(*tid, sizeof(cpu_set_t), &cpuset);
#if defined DEBUG
  if(sys_ret != 0)
  	printf("Error: set affinity failed\n");
#endif
  return sys_ret;
}

error_code_t lock_init_barrier()
{
	error_code_t ret = RUNTIME_OK;
	int sys_ret = pthread_mutex_lock(&init_barrier_mutex);
	if(sys_ret)
		return RUNTIME_SYSCALL_ERROR;
	return ret;
}

error_code_t unlock_init_barrier()
{
	error_code_t ret = RUNTIME_OK;
	int sys_ret = pthread_mutex_unlock(&init_barrier_mutex);
	if(sys_ret)
		return RUNTIME_SYSCALL_ERROR;
	return ret;
}

error_code_t wait_threads_init_multiarch(unsigned int threads_under_init)
{
	error_code_t ret = RUNTIME_OK;
	reached_init_barrier = 0;
	int sys_ret = 0;
	while(reached_init_barrier<threads_under_init)
	{
		sys_ret |= pthread_cond_wait(&init_barrier_cond, &init_barrier_mutex);
	}
	if(sys_ret)
		return RUNTIME_SYSCALL_ERROR;
	return ret;
}

error_code_t await_time_triggered_dispatch_multiarch(target_timetriggered_thread_config_t * config)
{
  error_code_t ret = RUNTIME_OK; 
  int sys_ret = pthread_mutex_lock(&config->rez);;
  if(config->parent->state != RUNNING_THREAD_STATE)
    sys_ret |= pthread_cond_wait(&config->event, &config->rez);
  sys_ret |= pthread_mutex_unlock(&config->rez);
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return ret;
}

error_code_t await_time_triggered_delay(uint64_t delay_ns)
{
  struct timespec tim,tim2 = {0,0};  
  tim.tv_sec = delay_ns/1000000000;
  tim.tv_nsec = delay_ns%1000000000;
  do {
    nanosleep(&tim , &tim2);
  } while(tim2.tv_sec!=0 || tim2.tv_nsec!=0);  
  return RUNTIME_OK;
}

error_code_t init_time_triggered_config(target_timetriggered_thread_config_t * config)
{
  error_code_t ret = RUNTIME_OK;
  int sys_ret = pthread_cond_init(&config->event, NULL);
  sys_ret = pthread_mutex_init(&config->rez, NULL);
  if (config->parent->global_q != NULL)
    sys_ret |= init_port_list_mutex(config->parent->global_q);
  if(sys_ret)
    return RUNTIME_SYSCALL_ERROR;
  return ret;
}

void init_dispatch_configuration(thread_config_t * config)
{
  switch (config->protocol)
    {
    case PERIODIC:
      {
	target_periodic_thread_config_t * per_thr = (target_periodic_thread_config_t *) config->thread;
	per_thr->iteration_counter = 0;
	break;
      }
    case HYBRID:
      {
	target_hybrid_thread_config_t * hyb_thr = (target_hybrid_thread_config_t *) config->thread;
	hyb_thr->iteration_counter = 0;
	break;
      }
    default:
      break;
    }
}

error_code_t dispatch_time_triggered_thread(target_timetriggered_thread_config_t * config)
{
  pthread_cond_signal(&config->event);
}

