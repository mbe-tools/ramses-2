/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef __AADL_RUNTIME_SERVICES_H_
#define __AADL_RUNTIME_SERVICES_H_

#include "runtime_stdint.h"
#include "dimensions.h"

#ifndef TASKS_NB
#define TASKS_NB 1
#endif

#ifndef MODES_NB
#define MODES_NB 1
#endif

#if MODES_NB == 1
typedef int mode_id_t;
#endif

#ifndef CORES_NB
#define CORES_NB 1
#endif

typedef void * runtime_addr_t;

#include "aadl_error_types.h"
#include "aadl_runtime_mqtt.h"
#include "aadl_runtime_socket.h"
#include "aadl_runtime_port.h"
#include "aadl_runtime_thread.h"
#include "aadl_error.h"
#include "aadl_target_types.h"

typedef enum access_protocol_t {
  NONE = 1,
  LOCK = 2,
  ICPP = 3
} access_protocol_t;

typedef struct resource_t {
  access_protocol_t protocol;
  void * protocol_config; // can be NULL, lock_access_protocol_t or icpp_access_protocol_t
} resource_t;

typedef struct lock_access_protocol_t {
  target_lock_t * rez;
} lock_access_protocol_t;

typedef struct icpp_access_protocol_t {
  lock_access_protocol_t * lock;
  unsigned int ceiling_priority;
  unsigned int base_priority; // set dynamically when calling Get_Resource
} icpp_access_protocol_t;

typedef struct process_config_t
{
  struct thread_config_t * threads[TASKS_NB];
  uint8_t criticality_levels_nb;
  uint8_t criticality[MODES_NB];
  uint64_t hyperperiod;
} process_config_t;

typedef struct data_access_reference_t {
  void * data;
  resource_t * resource;
  int size;
} data_access_reference_t;

typedef data_access_reference_t* data_access_reference_addr;

struct thread_config_t;

typedef struct port_list_t {
  struct thread_config_t * parent;
  char waiting;
  unsigned int msg_nb;
  unsigned int msg_nb_at_dispatch;
  int ports_nb;
  port_reference_t ** port_references;
  target_wait_message_t * event;
} port_list_t;

typedef char aadl_runtime_string[200];
typedef char * aadl_runtime_char_addr;

/* FRAG_SIZE is given in bytes*/
#define FRAG_SIZE 30

struct message
{
  uint16_t aadl_port_nb; // global port port id
  int size;
  char data[FRAG_SIZE];
};

typedef enum mode_switch_protocol_t
{
  EMERGENCY_MODE_SWITCH,
  PLANNED_MODE_SWITCH
} mode_switch_protocol_t;

typedef struct time_triggered_schedule_event
{
  uint32_t sched_time;
  // tasks to release
  uint16_t task_release_nb;
  uint16_t task_release_idx[TASKS_NB];
  // tasks deadline
  uint16_t task_deadline_nb;
  uint16_t task_deadline_idx[TASKS_NB];
  // tasks placement
  uint16_t task_placement_nb; // note: initialize to tasks_nb at time 0
  uint16_t task_placement_idx[TASKS_NB]; // note: used to bind all threads at time 0
  uint16_t placement_core[CORES_NB];
  // tasks promotion
  uint16_t task_promotion_nb;
  uint16_t task_promotion_idx[TASKS_NB];
  // tasks degradation
  uint16_t task_degradation_nb;
  uint16_t task_degradation_idx[TASKS_NB];
} time_triggered_schedule_event_t;

typedef time_triggered_schedule_event_t * time_triggered_schedule_event_addr_t;

typedef enum connection_protocol_type_t {
	SAMPLED,
	IMMEDIATE,
	PDP_LOWMFP,
	PDP_LOWET,
	PDP_LOWMC
} connection_protocol_type_t;

typedef struct connection_protocol_t
{
	connection_protocol_type_t protocol_type;
	void * protocol_attr; // can be PDP_lowET_attr_t, PDP_lowMC_attr_t
} connection_protocol_t;

typedef connection_protocol_t * connection_protocol_addr;

typedef struct PDP_lowMFP_attr_t
{
	uint16_t read_lookup_table_size;
	uint16_t write_lookup_table_size;
} PDP_lowMFP_attr_t;

typedef struct PDP_lowET_attr_t
{
	uint16_t * read_lookup_table;
	uint16_t read_lookup_table_size;
	uint16_t * write_lookup_table;
	uint16_t write_lookup_table_size;
} PDP_lowET_attr_t;

typedef PDP_lowET_attr_t * PDP_lowET_attr_addr;

error_code_t Put_Value(port_reference_t * port, runtime_addr_t value);

error_code_t Send_Output(port_reference_t * port);

error_code_t Next_Value(port_reference_t * port);

error_code_t Get_Value(port_reference_t * port, runtime_addr_t dst);

error_code_t Receive_Input(port_reference_t * port);

error_code_t Get_Count(port_reference_t * port, uint16_t * count_res);

error_code_t Updated(port_reference_t * port, uint8_t * fresh_flag);

error_code_t Await_Mode(thread_config_t * config);

error_code_t Await_Dispatch(thread_config_t * global_q);


// TODO: put in aadl_error.h after having defined aadl_thread_types.h, etc

error_code_t Error_Handler(thread_config_t * config, error_code_t error);

void Raise_Error (thread_config_t * config, error_code_t error);

error_code_t Get_Error_Code(thread_config_t * config);

error_code_t ignore_error(thread_config_t * config, error_code_t error);

const char * get_error_code_string(error_code_t error);


#endif
