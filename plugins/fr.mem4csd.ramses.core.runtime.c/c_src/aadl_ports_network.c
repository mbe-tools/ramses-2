/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_multiarch.h"
#include "aadl_ports.h"
#include "aadl_ports_network.h"

#include "main.h"

#include <string.h>

extern process_config_t this_process;                                           
extern processor_inputport_addr global_to_local[];  

error_code_t processor_port_init(processor_link_t * link)
{
  error_code_t ret = RUNTIME_OK;
#ifdef USE_SOCKET
  if(link->type == SOCKETS_UDP || link->type == SOCKETS_TCP)
    return processor_port_init_socket(link);
#endif
#ifdef USE_MQTT
  if(link->type == MQTT)
    return processor_port_init_mqtt(link);
#endif
#ifdef USE_POK
  if(link->type == POK_QEMU)
    return processor_port_init_pok_qemu(link);
#endif
  return ret;
}

error_code_t receive_in_processor(processor_link_t *link, unsigned int * payload_size)
{
  error_code_t status = RUNTIME_OK;
  struct message msg;
  for(unsigned int i = 0; i < FRAG_SIZE; i++)
  {
    msg.data[i] = 0;
  }
  msg.size = 0;
  msg.aadl_port_nb = -1;
  
  port_reference_t * port = NULL;

  do {
#ifdef USE_SOCKET
    if(link->type == SOCKETS_UDP || link->type == SOCKETS_TCP)
      status = receive_in_processor_socket(link, &msg);      
#endif
#ifdef USE_MQTT
    if(link->type == MQTT)
      status = receive_in_processor_mqtt(link, &msg);
#endif
#ifdef USE_POK
    if(link->type == POK_QEMU)
      status = receive_in_processor_pok_qemu(link, &msg);
#endif

    if(status!=0)
      return status;

#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
    printf("Received data (remote) to global port %d\n", msg.aadl_port_nb);
#endif
    //Global_port_id contained in msg
    processor_inputport_t * src = global_to_local[msg.aadl_port_nb];
    if(src==NULL)
    {
    	continue;
    }
    src->frag_number++;

    char * tmp_buf = src->tmp_buf;

    memcpy(&tmp_buf[(src->frag_number-1)*FRAG_SIZE],msg.data, msg.size);

    // check if message is complete
    if(src->frag_number < ((*payload_size) / FRAG_SIZE))
    {
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
    printf("Copied data (remote) to temporary buffer %d\n", msg.aadl_port_nb);
#endif
    	continue;
    }

#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
    printf("Transmit (complete) message (remote) to local port\n");
#endif

    int dest_idx;
    for(dest_idx=0;dest_idx<src->destination_nb;dest_idx++)
    {
      unsigned int thread_idx = src->local_destinations[dest_idx].thread_idx;
      unsigned int port_idx = src->local_destinations[dest_idx].port_idx;
      #if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
    	printf("Transmit (complete) message (Put_Value) to thread %d, port %d\n", thread_idx, port_idx);
      #endif
      port = this_process.threads[thread_idx]->out_ports[port_idx];
      Put_Value(port,tmp_buf);
    }
    src->frag_number = 0;
  } while (status==0);

#ifdef USE_POSIX
  memset(msg.data, 0, FRAG_SIZE);
#endif

  return status;
}

error_code_t send_out_processor(processor_outputport_t * port, void * data, unsigned int * payload_size)
{
  error_code_t ret = 0;
  int i;
  for(i=0; i < port->destination_nb;i++) {
    ret |= send_out_processor_port_data(port->destinations[i], data, payload_size);
  }
  return ret;
}

error_code_t send_out_processor_port_data(processor_destination_port_t * dst_port,
		char * data,
		unsigned int * payload_size)
{
  error_code_t err = RUNTIME_OK;
  struct message msg_sent;
  for(unsigned int i = 0; i < FRAG_SIZE; i++)
  {
	  msg_sent.data[i] = 0;
  }

  if((*payload_size) > FRAG_SIZE)
  {
	unsigned int nb_frag = (*payload_size) / FRAG_SIZE;
	for (size_t i = 0; i < nb_frag; ++i)
    {
      for (unsigned int j = 0; j < FRAG_SIZE; ++j)
          msg_sent.data[j] = (data[i *
          FRAG_SIZE  + j]);

      msg_sent.size = FRAG_SIZE;
#ifdef USE_SOCKET
      if (dst_port->link->type == SOCKETS_UDP || dst_port->link->type == SOCKETS_TCP)
      {
    	msg_sent.aadl_port_nb = *((uint16_t*)dst_port->destination_id);
	//	printf("\tSEND %d\n", msg_sent.aadl_port_nb);
        send_out_processor_socket(dst_port, msg_sent);
      }
#endif
#ifdef USE_MQTT
      if(dst_port->link->type == MQTT)
      {
    	send_out_processor_mqtt(dst_port, msg_sent);
      }
#endif
#ifdef USE_POK
	  if(dst_port->link->type == POK_QEMU)
	  {
        msg_sent.aadl_port_nb = *((int*)dst_port->destination_id);
        send_out_processor_msg_pok_qemu(dst_port, msg_sent);
	  }
#endif
    }

	if((*payload_size)-nb_frag*FRAG_SIZE > 0)
	{
      for (unsigned int j = 0; j < (*payload_size)-nb_frag*FRAG_SIZE; ++j)
        msg_sent.data[j] = (data[nb_frag *
            FRAG_SIZE  + j]);
      msg_sent.size = (*payload_size)-nb_frag*FRAG_SIZE;
	}
	else
      return err;
#ifdef USE_SOCKET
    if (dst_port->link->type == SOCKETS_UDP || dst_port->link->type == SOCKETS_TCP)
    {
      msg_sent.aadl_port_nb = *((int*)dst_port->destination_id);
      send_out_processor_socket(dst_port, msg_sent);
    }
#endif
#ifdef USE_MQTT
    if(dst_port->link->type == MQTT)
      send_out_processor_mqtt(dst_port, msg_sent);
#endif
#ifdef USE_POK
    if(dst_port->link->type == POK_QEMU)
    {
      msg_sent.aadl_port_nb = *((int*)dst_port->destination_id);
      send_out_processor_msg_pok_qemu(dst_port, msg_sent);
    }
#endif

  }
  else
  {
	msg_sent.size = (*payload_size);
	for (unsigned int j = 0; j < (*payload_size); ++j)
	      msg_sent.data[j] = data[j];

#ifdef USE_SOCKET
    if (dst_port->link->type == SOCKETS_UDP || dst_port->link->type == SOCKETS_TCP)
    {
      msg_sent.aadl_port_nb = *((int*)dst_port->destination_id);
      send_out_processor_socket(dst_port, msg_sent);
    }
#endif
#ifdef USE_MQTT
    if(dst_port->link->type == MQTT)
      send_out_processor_mqtt(dst_port, msg_sent);
#endif
#ifdef USE_POK
    if(dst_port->link->type == POK_QEMU)
    {
      msg_sent.aadl_port_nb = *((int*)dst_port->destination_id);
      send_out_processor_msg_pok_qemu(dst_port, msg_sent);
    }
#endif
  }
  return err;
}
