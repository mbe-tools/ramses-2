/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_error.h"
#include "aadl_ports.h"

error_code_t Error_Handler(thread_config_t * config, error_code_t error)
{

  error_code_t (*recover_entrypoint)(runtime_addr_t context) = config->recover_entrypoint;
  error_code_t recover_res = error;
  config->execution_error = error;
  if(recover_entrypoint != NULL)
    recover_res = (*recover_entrypoint)(config->context);
  else
    recover_res = ignore_error(config, error);
  
  if(recover_res)
    Raise_Error (config, error);

  return recover_res;
}

void Raise_Error (thread_config_t * config, error_code_t error)
{
  error_code_t raised_err = error;
  config->execution_error = error;
  if (error != RUNTIME_OK)
  {
    if (config->error_port_idx != 0xFF)
    {
      port_reference_t * error_port = config->out_ports[config->error_port_idx]; 
      error_code_t res = Put_Value(error_port, &raised_err);
      res = Send_Output(error_port);
      if (res != 0)
      {
        Raise_Error (config, res);
      }
    }
  }
  return;
}

error_code_t Get_Error_Code(thread_config_t * config)
{
  return config->execution_error;
}

error_code_t ignore_error(thread_config_t * config, error_code_t error)
{
#ifdef RUNTIME_DEBUG
	aadl_runtime_debug("Ignore error %s in %s\n", get_error_code_string(error), config->name);
#endif //RUNTIME_DEBUG
  (void) config; // ignore error, unused parameter
  return error;
}

const char * get_error_code_string(error_code_t error)
{
  switch(error)
  { 
	case RUNTIME_OK:
		return "RUNTIME_OK";
	case RUNTIME_EMPTY_QUEUE:
		return "RUNTIME_EMPTY_QUEUE";
	case RUNTIME_FULL_QUEUE:
		return "RUNTIME_FULL_QUEUE";
	case RUNTIME_LOCK_ERROR:
		return "RUNTIME_LOCK_ERROR";
	case RUNTIME_OUT_OF_BOUND:
		return "RUNTIME_OUT_OF_BOUND";
	case RUNTIME_INVALID_PARAMETER:
		return "RUNTIME_INVALID_PARAMETER";
	case RUNTIME_INVALID_SERVICE_CALL:
		return "RUNTIME_INVALID_SERVICE_CALL";
	case RUNTIME_OUTDATED_INPUT:
		return "RUNTIME_OUTDATED_INPUT";
	case RUNTIME_SYSCALL_ERROR:
		return "RUNTIME_SYSCALL_ERROR";
	case RUNTIME_START_THREAD_ERROR:
		return "RUNTIME_START_THREAD_ERROR";
	case RUNTIME_WAIT_DISPATCH_ERROR:
		return "RUNTIME_WAIT_DISPATCH_ERROR";
	case RUNTIME_DEADLINE_MISS:
		return "RUNTIME_DEADLINE_MISS";
	default:
		return "UNKNOWN";
  }
}
