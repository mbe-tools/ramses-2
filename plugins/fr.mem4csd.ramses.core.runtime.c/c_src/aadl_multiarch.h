/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef AADL_MULTIARCH_H
#define AADL_MULTIARCH_H

#include "aadl_runtime_services.h"
#include "aadl_target_types.h"

void get_time_multiarch(uint64_t * time);
void init_dispatch_configuration(thread_config_t * config);

error_code_t release_task_in_mode_multiarch(thread_config_t * config);
error_code_t create_thread_multiarch(thread_config_t * config);
error_code_t start_thread_multiarch(thread_config_t * config);
error_code_t stop_thread_multiarch(thread_config_t * config);
error_code_t lock_init_barrier();
error_code_t unlock_init_barrier();
error_code_t wait_threads_init_multiarch(unsigned int threads_under_init);
error_code_t set_thread_priority_multiarch(thread_config_t * config, unsigned int priority);

error_code_t lock_port_reference(thread_input_port_t * port);
error_code_t unlock_port_reference(thread_input_port_t * port);
error_code_t send_signal(thread_input_port_t * port);

error_code_t send_out_process(port_reference_t * source,
		void * value,
		unsigned int msg_size);

error_code_t receive_in_process(port_reference_t * destination,
		void * received_msg);

error_code_t lock_data(lock_access_protocol_t * protocol);
error_code_t unlock_data(lock_access_protocol_t * protocol);

error_code_t icpp_lock_data(icpp_access_protocol_t * protocol);
error_code_t icpp_unlock_data(icpp_access_protocol_t * protocol);

int init_port_list_mutex(port_list_t * info);
error_code_t lock_port_list(port_list_t * port_list);
error_code_t unlock_port_list(port_list_t * port_list);
error_code_t wait_port_list(port_list_t * port_list);
void sporadic_thread_sleep(target_sporadic_thread_config_t * config);
void set_start_time_multiarch();

int init_thread_config_multiarch(thread_config_t * config);

int init_periodic_config_multiarch(target_periodic_thread_config_t * config);
error_code_t Await_Mode_multiarch(thread_config_t * config);
error_code_t await_periodic_dispatch_multiarch(target_periodic_thread_config_t * config);

error_code_t init_hybrid_timer(target_hybrid_thread_config_t * config);
error_code_t hybrid_reset_timer(target_hybrid_thread_config_t * config);
error_code_t timedwait_hybrid(target_hybrid_thread_config_t * config);

error_code_t timedwait_timed_thread(target_timed_thread_config_t * config);

error_code_t init_time_triggered_config(target_timetriggered_thread_config_t * config);
error_code_t await_time_triggered_dispatch_multiarch(target_timetriggered_thread_config_t * config);
error_code_t dispatch_time_triggered_thread(target_timetriggered_thread_config_t * config);

#ifdef USE_POSIX
error_code_t processor_port_init_socket(processor_link_t * processor_port);
error_code_t send_out_processor_port_msg_socket(processor_destination_port_t * dst_port, struct message *msg);
error_code_t receive_in_processor_socket(processor_link_t *link, struct message *msg);
error_code_t send_out_processor_socket(processor_destination_port_t * dst_port, struct message msg_sent);
error_code_t receive_in_processor_msg_socket(processor_link_t *link, struct message *msg);

error_code_t processor_port_init_mqtt(processor_link_t * processor_port);
error_code_t send_out_processor_port_msg_mqtt(processor_destination_port_t * dst_port, struct message *msg);
error_code_t receive_in_processor_mqtt(processor_link_t *link, struct message *msg);
error_code_t send_out_processor_mqtt(processor_destination_port_t * dst_port, struct message msg_sent);
error_code_t receive_in_processor_msg_mqtt(processor_link_t *link, struct message *msg);

#ifdef MQTTCLIENT_PLATFORM_HEADER
int mqtt_init(mqtt_config_t * conf, char* model_id, char* client_id, messageHandler on_message_arrived);
#endif

#endif

#ifdef USE_POK
error_code_t send_out_processor_msg_pok_qemu(processor_destination_port_t * dst_port,struct message msg_sent);
error_code_t processor_port_init_pok_qemu(processor_link_t * link);
error_code_t receive_in_processor_pok_qemu(processor_link_t *link, struct message *msg);
#endif

#endif // AADL_MULTIARCH_H
