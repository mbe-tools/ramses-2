/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_ports.h"
#include <string.h>
#include "aadl_multiarch.h"

error_code_t Put_Value(port_reference_t * port, void * value)
{
  error_code_t status = RUNTIME_OK;

  port_direction_t direction = port->direction;
  if(direction!=OUT){
	status = RUNTIME_INVALID_PARAMETER;
  }

  thread_output_port_t * out_port = (thread_output_port_t*) port->directed_port;
  out_port->updated = 1;
  if(value != NULL)
    memcpy(out_port->port_variable, value, port->msg_size);

  return status;
}

error_code_t Send_Output(port_reference_t * src_port)
{
  error_code_t status = RUNTIME_OK;
  port_direction_t direction = src_port->direction;
  if(direction!=OUT && direction!=INOUT) {
    status = RUNTIME_INVALID_PARAMETER;
#if defined DEBUG && defined USE_POSIX
          printf("Send_Output: ERROR, parameter is not output port\n");
#endif
  }

  component_t parent_type = src_port->parent_type;
  if(parent_type!=THREAD) {
	  status = RUNTIME_INVALID_PARAMETER;
#if defined DEBUG && defined USE_POSIX
	  printf("Send_Output: ERROR, parameter is not a thread port\n");
#endif
  }

  thread_output_port_t * out_port = (thread_output_port_t*) src_port->directed_port;
  if(out_port->updated==0)
    return status;
  
  status = Do_Send_Output(src_port);

  out_port->updated=0;
  return status;
}

/* Note: Is_Empty cannot be replaced by Get_Count since
 * Is_Empty is supposed to be used in a critical section
 * Get_Count implements its own critical section
 */
char Is_Empty(port_reference_t * port)
{
	char is_empty = 0;
	thread_input_port_t * in_port = (thread_input_port_t*) port->directed_port;
	if (port->type == EVENT_DATA)
	  {
	    event_data_port_t * port_ed = (event_data_port_t *) in_port->content;
	    if(port_ed->beg_read_idx == port_ed->end_read_idx)
	    {
	    	is_empty = 1;
	    }
    }
	else if (port->type == EVENT)
	  {
	    event_port_t * port_e = (event_port_t *) in_port->content;
	    if(port_e->beg_read_idx == port_e->end_read_idx)
	    {
	    	is_empty = 1;
	    }
    }
	return is_empty;
}

error_code_t Next_Value(port_reference_t * dst_port)
{
  error_code_t status = RUNTIME_OK;

  port_direction_t direction = dst_port->direction;
  if(direction!=IN) {
    status = RUNTIME_INVALID_PARAMETER;
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
          printf("Next_Value: ERROR, parameter is not an input port\n");
#endif
  }

  component_t parent_type = dst_port->parent_type;
  if(parent_type!=THREAD) {
	  status = RUNTIME_INVALID_PARAMETER;
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
	  printf("Next_Value: ERROR, parameter is not a thread port\n");
#endif
  }

  thread_input_port_t * in_port = (thread_input_port_t*) dst_port->directed_port;

  status = lock_port_reference(in_port);

  if (status != RUNTIME_OK)
  {
    Error_Handler(in_port->parent->parent, status);
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
    printf("Next value: ERROR %d when acquiring lock\n", status);
#endif
    return status;
  }

  if(Is_Empty(dst_port))
  {
	  status = RUNTIME_EMPTY_QUEUE;
	  unlock_port_reference(in_port);
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
      printf("Next value: ERROR, empty queue\n");
#endif
  }
  if(status!=RUNTIME_OK)
  {
    Error_Handler(in_port->parent->parent, status);
    return status;
  }
  if (dst_port->type == DATA)
  {
    in_port->status = DEFAULT;
  }    
  else if (dst_port->type == EVENT_DATA)
  {
    event_data_port_t * port_ed = (event_data_port_t *) in_port->content;
    port_ed->beg_read_idx = (port_ed->beg_read_idx+1)%port_ed->queue_size;

#if (defined USE_POSIX || defined USE_POK) && defined RUNTIME_DEBUG
    printf("Next value: first index is %d, %d\n", port_ed->beg_read_idx);
#endif
  }
  else if (dst_port->type == EVENT)
  {
    event_port_t * port_event = (event_port_t *) in_port->content;
    port_event->beg_read_idx=(port_event->beg_read_idx+1)%port_event->queue_size;

#if (defined USE_POSIX || defined USE_POK) && defined RUNTIME_DEBUG
    printf("Next value: first index is %d, %d\n", port_event->beg_read_idx);
#endif
  }

  if(Is_Empty(dst_port))
  {
	  status = RUNTIME_EMPTY_QUEUE;
	  unlock_port_reference(in_port);
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
      printf("Next value: ERROR, empty queue\n");
#endif
  }
  if(status!=RUNTIME_OK)
  {
	  Error_Handler(in_port->parent->parent, status);
	  return status;
  }

  in_port->parent->msg_nb--;
  
  status = unlock_port_reference(in_port);
  if (status != RUNTIME_OK)
    Error_Handler(in_port->parent->parent, status);

  return status;
}

error_code_t Get_Value(port_reference_t * dst_port, void * dst)
{
  error_code_t status = RUNTIME_OK;

  port_direction_t direction = dst_port->direction;
  if(direction!=IN) {
    status = RUNTIME_INVALID_PARAMETER;
#if defined DEBUG && defined USE_POSIX
          printf("Get_Value: ERROR, parameter is not an input port\n");
#endif
  }

  component_t parent_type = dst_port->parent_type;
  if(parent_type!=THREAD) {
	  status = RUNTIME_INVALID_PARAMETER;
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
	  printf("Get_Value: ERROR, parameter is not a thread port\n");
#endif
  }

  thread_input_port_t * in_port = (thread_input_port_t*) dst_port->directed_port;

  lock_port_reference(in_port);

  if(Is_Empty(dst_port))
  {
	  status = RUNTIME_EMPTY_QUEUE;
	  unlock_port_reference(in_port);
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
      printf("Get_Value: ERROR, empty queue\n");
#endif
  }
  if(status!=RUNTIME_OK)
  {
	  Error_Handler(in_port->parent->parent, status);
	  return status;
  }

  if (dst_port->type == DATA)
  {
    data_port_t * port_data = (data_port_t *) in_port->content;
    mode_id_t current_mode;
	Current_System_Mode(&current_mode);
    void * addr = port_data->data_rw_per_mode[port_data->src_index_by_mode[current_mode]].reading_data;
    memcpy(dst, addr, dst_port->msg_size);
  }
  else if (dst_port->type == EVENT_DATA)
  {
    event_data_port_t * port_ed = (event_data_port_t *) in_port->content;
    char * buf = (char*) port_ed->offset;
/*    int i=0;
    printf("queue content: ");
    for(i=0;i<port_ed->queue_size;i++)
    {
    	printf("%d ", buf[i*(dst_port->msg_size/sizeof(char))]);
    }
    printf("\n");
    printf("get value @index = %d\n", (port_ed->beg_read_idx+1)); */

    memcpy(dst,
    		&buf[(port_ed->beg_read_idx)*(dst_port->msg_size/sizeof(char))],
			dst_port->msg_size); //No more read at beg_read_idx + 1
  }
  else
  {
    event_port_t * port_event = (event_port_t *) in_port->content;
    char * buf = (char*) port_event->offset;
    memcpy(dst,
           &buf[port_event->beg_read_idx+1],
		   dst_port->msg_size);
  }

  unlock_port_reference(in_port);

  return status;
}

error_code_t Receive_Input(port_reference_t * dst_port)
{
  error_code_t status = RUNTIME_OK;

  port_direction_t direction = dst_port->direction;
  if(direction!=IN) {
    return RUNTIME_INVALID_PARAMETER;
  }

  component_t parent_type = dst_port->parent_type;
  if(parent_type!=THREAD) {
	  return RUNTIME_INVALID_PARAMETER;
  }

  thread_input_port_t * in_port = (thread_input_port_t *) dst_port->directed_port;

#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
	printf("Receive_Input connection numbers = %d\n", in_port->connected_ports_length);
#endif

  int i;
  for(i=0; i<in_port->connected_ports_length; i++) {
    port_connection_t * cnx = in_port->sources[i];
    status = Receive_Input_Source(cnx);
    if(status!=RUNTIME_OK)
  	  return status;
  }

  in_port->parent->msg_nb_at_dispatch = in_port->parent->msg_nb;

  return status;
}

error_code_t Get_Count(port_reference_t * port, uint16_t * count_res)
{
  error_code_t status = RUNTIME_OK;
  * count_res = 0;
  
  port_direction_t direction = port->direction;
  if(direction!=IN) {
    status = RUNTIME_INVALID_PARAMETER;
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
          printf("Get_Count: ERROR, parameter is not an input port\n");
#endif
  }

  component_t parent_type = port->parent_type;
  if(parent_type!=THREAD) {
	  status = RUNTIME_INVALID_PARAMETER;
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
	  printf("Get_Count: ERROR, parameter is not a thread port\n");
#endif
  }

  thread_input_port_t * in_port = (thread_input_port_t*) port->directed_port;

  status = lock_port_reference(in_port);
  if (status != RUNTIME_OK)
  {
    Error_Handler(in_port->parent->parent, status);
    return status;
  }

  if (port->type == DATA)
  {
    if(in_port->status==FRESH)
      *count_res = 1;
    else
      *count_res = 0;
  }
  else if (port->type == EVENT_DATA)
  {
    event_data_port_t * port_ed = (event_data_port_t *) in_port->content;
    if(port_ed->end_read_idx < port_ed->beg_read_idx)
      *count_res = port_ed->queue_size-port_ed->beg_read_idx+port_ed->end_read_idx;
    else
      *count_res = port_ed->end_read_idx - port_ed->beg_read_idx;
  }
  else
  {
    event_port_t * port_event = (event_port_t *) in_port->content;
    if(port_event->end_read_idx < port_event->beg_read_idx)
      *count_res = port_event->queue_size-port_event->beg_read_idx+port_event->end_read_idx;
    else
      *count_res = port_event->end_read_idx - port_event->beg_read_idx;
  }

  status = unlock_port_reference(in_port);
  if (status != RUNTIME_OK)
    Error_Handler(in_port->parent->parent, status);
#if defined RUNTIME_DEBUG && (defined USE_POK || defined USE_POSIX)
  printf("Get_Count result on port = %d, on thread %s\n", *count_res, in_port->parent->parent->name);
#endif
  return status;
}

error_code_t Updated(port_reference_t * port, uint8_t * fresh_flag)
{
  error_code_t status = RUNTIME_OK;
  port_direction_t direction = port->direction;
  if(direction!=IN) {
	  status = RUNTIME_INVALID_PARAMETER;
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
	  printf("Updated: ERROR, parameter is not an input port\n");
#endif
  }

  component_t parent_type = port->parent_type;
  if(parent_type!=THREAD) {
	  status = RUNTIME_INVALID_PARAMETER;
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
	  printf("Updated: ERROR, parameter is not a thread port\n");
#endif
  }

  thread_input_port_t * in_port = (thread_input_port_t*) port->directed_port;

  status = lock_port_list(in_port->parent);
  *fresh_flag = (in_port->status==UPDATED);
  status = unlock_port_list(in_port->parent);

  return status;
}

