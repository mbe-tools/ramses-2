/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef AADL_PORTS_NETWORK_H
#define AADL_PORTS_NETWORK_H

#include "aadl_runtime_services.h"

error_code_t processor_port_init(processor_link_t * processor_port);
error_code_t send_out_processor(processor_outputport_t * port, void * data, unsigned int * len);
error_code_t send_out_processor_port_data(processor_destination_port_t * dst_port, char * data, unsigned int * len);
error_code_t receive_in_processor(processor_link_t *link, unsigned int * len);
error_code_t receive_in_processor_msg_socket(processor_link_t *link, struct message *msg);
error_code_t send_out_processor_port_msg_socket(processor_destination_port_t * dst_port,struct message *msg);
#endif //AADL_PORTS_NETWORK_H
