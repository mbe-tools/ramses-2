#ifndef __AADL_RUNTIME_SOCKET_TYPES_H_
#define __AADL_RUNTIME_SOCKET_TYPES_H_

typedef struct socket_config_t
{
	void * socket_id;
	char * ip_addr;
	char * ip_port;
} socket_config_t;


#endif
