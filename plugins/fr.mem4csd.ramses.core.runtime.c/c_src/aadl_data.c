/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_data.h"
#include "aadl_multiarch.h"

error_code_t Get_Resource(resource_t * resource)
{
  error_code_t status = RUNTIME_OK;
  void* access_protocol;
  switch(resource->protocol)
  {
    	case NONE:
    	  return status;
    	case LOCK:
      access_protocol = (lock_access_protocol_t*) resource->protocol_config;
    	  status = lock_data((lock_access_protocol_t*)access_protocol);
      break;
    	case ICPP:
    	  access_protocol = (icpp_access_protocol_t*) resource->protocol_config;
    	  status = icpp_lock_data((icpp_access_protocol_t*) access_protocol);
    	  break;
  }
  return status;
}

error_code_t Release_Resource(resource_t * resource)
{
  error_code_t status = RUNTIME_OK;
  void * access_protocol;
  switch(resource->protocol)
  {
    	case NONE:
    	  return status;
    	case LOCK:

      access_protocol = (lock_access_protocol_t*) resource->protocol_config;
      status = unlock_data((lock_access_protocol_t*) access_protocol);
      break;
    	case ICPP:
    	  access_protocol = (icpp_access_protocol_t*) resource->protocol_config;
    	  status = icpp_unlock_data((icpp_access_protocol_t*) access_protocol);
    	  break;
  }  return status;
}

error_code_t Get_Access_Resource(data_access_reference_t * access_ref)
{
	if(access_ref->resource == NULL)
		return RUNTIME_LOCK_ERROR;
	return Get_Resource(access_ref->resource);
}

error_code_t Release_Access_Resource(data_access_reference_t * access_ref)
{
	if(access_ref->resource == NULL)
		return RUNTIME_LOCK_ERROR;
	return Release_Resource(access_ref->resource);
}

