#ifndef __AADL_RUNTIME_MQTT_TYPES_H_
#define __AADL_RUNTIME_MQTT_TYPES_H_

#ifdef MQTTCLIENT_PLATFORM_HEADER
#include "MQTTClient.h"

#define MQTT_STATIC_MESSAGE_MAX_PORT_ID_SIZE    256
#define MQTT_STATIC_MESSAGE_MAX_BUFFER_SIZE     1024
#define MQTT_STATIC_MESSAGE_MAX_IN_PORTS_NUM    16
#define MQTT_STATIC_MESSAGE_MAX_TOPIC_SIZE      256

typedef struct mqtt_buffer_t {
    /**
     * Non-zero if there is a pending message.
     */
    char message_present;
    /**
     * It is assumed, that the models used with this packed have no need
     * for buffering incoming messages.
     */
    char buffer[MQTT_STATIC_MESSAGE_MAX_BUFFER_SIZE];
    /**
     * Can not exceed <code>MQTT_STATIC_MESSAGE_MAX_BUFFER_SIZE</code>
     */
    int size;
} mqtt_buffer_t;

typedef struct mqtt_subscriber_config_t {
    uint8_t topics_nb;
//    char * topics[MQTT_STATIC_MESSAGE_MAX_IN_PORTS_NUM]; // size = client's topics number (topics_nb)
    char ** topics; // size = client's topics number (topics_nb)
//    struct mqtt_rport_t * mqtt_ports[MQTT_STATIC_MESSAGE_MAX_IN_PORTS_NUM]; // size = client's topics number (topics_nb)
    struct mqtt_buffer_t * mqtt_buffers; // size = client's topics number (topics_nb)
//    uint16_t topics_global_ports[MQTT_STATIC_MESSAGE_MAX_IN_PORTS_NUM]; // size = client's topics number (topics_nb)
    uint16_t * topics_global_ports; // size = client's topics number (topics_nb)
    messageHandler on_message_arrived;
} mqtt_subscriber_config_t;

typedef struct mqtt_config_t {
	MQTTClient * client;
	Network * network;
	char * ip_addr;
	int ip_port;
	mqtt_subscriber_config_t * subscribers_config;
} mqtt_config_t;

#endif

#endif
