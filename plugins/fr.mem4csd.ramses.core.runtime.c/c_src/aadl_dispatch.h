/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef __AADL_DISPATCH_H__
#define __AADL_DISPATCH_H__

#ifdef USE_MACH
#include <mach/clock.h>
#include <mach/mach.h>
#include <sys/time.h>
#endif

#include "aadl_runtime_services.h"

// To keep in private
//int init_periodic_config (periodic_thread_config_t * config);
//int init_sporadic_config (sporadic_thread_config_t * config);

error_code_t Stop_Thread(thread_config_t * config);
error_code_t Start_Thread(thread_config_t * config);
error_code_t Create_Thread(thread_config_t * config);

int Init_Config (thread_config_t * config,
		 error_code_t (*recover_entrypoint)(runtime_addr_t context),
		 void (*compute_entrypoint)(),
		 error_code_t (*initialize_entrypoint)(runtime_addr_t context),
		 error_code_t (*finalize_entrypoint)(runtime_addr_t context),
		 error_code_t (*activate_entrypoint)(runtime_addr_t context),
		 error_code_t (*deactivate_entrypoint)(runtime_addr_t context),
		 unsigned int id);

uint64_t get_next_dispatch_time(thread_config_t * config);
void wait_next_period (thread_config_t *config);

int mask_realtime_signals();

// To keep in private
//error_code_t await_periodic_dispatch(periodic_thread_config_t * global_q);
//error_code_t await_sporadic_dispatch(sporadic_thread_config_t * global_q);

uint64_t get_start_time_ns();
void get_start_time(uint64_t * time);

error_code_t keep_busy_until(uint64_t finish_time_ns);

int init_thread_config(thread_config_t * config);
int init_sporadic_config (target_sporadic_thread_config_t * info);
int init_aperiodic_config (target_aperiodic_thread_config_t * info);
int init_periodic_config (target_periodic_thread_config_t * info);
int init_hybrid_config (target_hybrid_thread_config_t * info);
int init_timed_config (target_timed_thread_config_t * info);

error_code_t bind_thread_to_core_id(thread_config_t * config, uint8_t core_id);
error_code_t await_time_triggered_delay(uint64_t delay_ns);

#endif
