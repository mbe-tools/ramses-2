/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_dispatch.h"
#include "aadl_ports.h"
#include "aadl_multiarch.h"
#include "aadl_error.h"
#include "aadl_runtime_debug.h"

error_code_t Stop_Thread(thread_config_t * config)
{
#if defined(RUNTIME_DEBUG)
  aadl_runtime_debug("Stopping thread %s\n",config->name);
#endif
	config->state=STOPPED_THREAD_STATE;
	return stop_thread_multiarch(config);
}

error_code_t Create_Thread(thread_config_t * config)
{
	return create_thread_multiarch(config);
}

error_code_t Start_Thread(thread_config_t * config)
{
#if defined(RUNTIME_DEBUG)
  aadl_runtime_debug("Starting thread %s\n",config->name);
#endif

  int ret = init_thread_config(config);
  if(ret)
    return RUNTIME_INVALID_PARAMETER;

  switch (config->protocol)
  {
    case PERIODIC:
    {
      target_periodic_thread_config_t * per_thr = (target_periodic_thread_config_t *) config->thread;
      per_thr->parent = config;
      init_periodic_config(per_thr);
      break;
    }
    case SPORADIC:
    {
      target_sporadic_thread_config_t * spo_thr = (target_sporadic_thread_config_t *) config->thread;
      spo_thr->parent = config;
      init_sporadic_config(spo_thr);
      break;
    }
    case APERIODIC:
    {
      target_aperiodic_thread_config_t * aper_thr = (target_aperiodic_thread_config_t *) config->thread;
      aper_thr->parent = config;
      init_aperiodic_config(aper_thr);
      break;
    }
    case HYBRID:
    {
      target_hybrid_thread_config_t * hyb_thr = (target_hybrid_thread_config_t *) config->thread;
      hyb_thr->parent = config;
      init_hybrid_config(hyb_thr);
      break;
    }
    case TIMED:
    {
      target_timed_thread_config_t * tim_thr = (target_timed_thread_config_t *) config->thread;
      tim_thr->parent = config;
      init_timed_config(tim_thr);
      break;
    }
    case TIME_TRIGGERED:
    {
      target_timetriggered_thread_config_t * tt_config = (target_timetriggered_thread_config_t *) config->thread;
      tt_config->parent = config;
      init_time_triggered_config(tt_config);
      break;
    }
    default:
      return RUNTIME_INVALID_PARAMETER;
  }

  config->state = INITIALIZING_THREAD_STATE;
  return start_thread_multiarch(config);
}


int init_thread_config(thread_config_t * config)
{
  int ret = init_thread_config_multiarch(config);
  return ret;
}

int init_sporadic_config (target_sporadic_thread_config_t * info)
{
  int ret = init_port_list_mutex(info->parent->global_q);
  return ret;
}

int init_aperiodic_config (target_aperiodic_thread_config_t * info)
{
  int ret = init_port_list_mutex(info->parent->global_q);
  return ret;
}

int init_periodic_config (target_periodic_thread_config_t * info)
{
  int ret = RUNTIME_OK;

  if (info->parent->global_q != NULL)
    ret = init_port_list_mutex(info->parent->global_q);
  ret = init_periodic_config_multiarch(info);
  return ret;
}

int init_hybrid_config (target_hybrid_thread_config_t * info)
{
  int ret = RUNTIME_OK;
  if (info->parent->global_q != NULL)
    init_port_list_mutex(info->parent->global_q);
  init_hybrid_timer(info);
  return ret;
}

int init_timed_config (target_timed_thread_config_t * info)
{
  int ret = init_port_list_mutex(info->parent->global_q);
  return ret;
}

// implementation if init_time_triggered_config in multiarch
// int init_time_triggered_config (time_triggered_thread_config_t * info);

int Init_Config (thread_config_t * config,
		 error_code_t (*recover_entrypoint)(runtime_addr_t context),
		 void (*compute_entrypoint)(),
		 error_code_t (*initialize_entrypoint)(runtime_addr_t context),
		 error_code_t (*finalize_entrypoint)(runtime_addr_t context),
		 error_code_t (*activate_entrypoint)(runtime_addr_t context),
		 error_code_t (*deactivate_entrypoint)(runtime_addr_t context),
		 unsigned int id)
{
  config->recover_entrypoint = recover_entrypoint;
  config->compute_entrypoint = compute_entrypoint;
  config->initialize_entrypoint = initialize_entrypoint;
  if(initialize_entrypoint!=NULL)
	(*initialize_entrypoint)(config->context);
  config->finalize_entrypoint = finalize_entrypoint;
  config->activate_entrypoint = activate_entrypoint;
  config->deactivate_entrypoint = deactivate_entrypoint;
  config->id = id;
  if (config->global_q != NULL)
  {
    config->global_q->parent = config;
  }

  return 0;
}


error_code_t await_periodic_dispatch(target_periodic_thread_config_t * info)
{
  error_code_t ret = await_periodic_dispatch_multiarch(info);
  return ret;
}

error_code_t await_time_triggered_dispatch(target_timetriggered_thread_config_t * config)
{
  error_code_t ret = await_time_triggered_dispatch_multiarch(config);
  return ret;
}

error_code_t await_sporadic_dispatch(target_sporadic_thread_config_t * info)
{
  error_code_t status = RUNTIME_OK;

  // wait next period
  sporadic_thread_sleep(info);
  
  // wait for messages
  status = lock_port_list(info->parent->global_q);

  if (status != RUNTIME_OK)
    return status;

  if(info->parent->global_q->msg_nb_at_dispatch>=info->parent->global_q->msg_nb)
  {
    info->parent->global_q->waiting = info->parent->global_q->waiting+1;
    status = wait_port_list(info->parent->global_q);
    info->parent->global_q->waiting = info->parent->global_q->waiting-1;
  }

  status = unlock_port_list(info->parent->global_q);
  	
  return status;
}

error_code_t await_aperiodic_dispatch(target_aperiodic_thread_config_t * info)
{
  error_code_t status = lock_port_list(info->parent->global_q);
  if (status != RUNTIME_OK)
    return status;


  if(info->parent->global_q->msg_nb_at_dispatch>=info->parent->global_q->msg_nb)
  {
	  info->parent->global_q->waiting += 1;
	  status = wait_port_list(info->parent->global_q);// FIXME manage error
	  info->parent->global_q->waiting -= 1;
  }
  status = unlock_port_list(info->parent->global_q);

  return status;
}

error_code_t await_hybrid_dispatch(target_hybrid_thread_config_t * info)
{
  error_code_t status = lock_port_list(info->parent->global_q);
  if (status != RUNTIME_OK)
    return status;

  if(info->parent->global_q->msg_nb_at_dispatch>=info->parent->global_q->msg_nb)
  {
    info->parent->global_q->waiting += 1;
    status = timedwait_hybrid(info);
    info->parent->global_q->waiting -= 1;

    // If it's not an event which woke up the hybrid thread
    if(info->parent->global_q->msg_nb_at_dispatch>=info->parent->global_q->msg_nb)
      hybrid_reset_timer(info);

    // FIXME Need to call ClearEvent for osek
  }

  status = unlock_port_list(info->parent->global_q);
  return status;
}

error_code_t await_timed_dispatch(target_timed_thread_config_t * info)
{
  error_code_t status = RUNTIME_OK;
  lock_port_list(info->parent->global_q);

  if(info->parent->global_q->msg_nb_at_dispatch>=info->parent->global_q->msg_nb)
  {
    info->parent->global_q->waiting += 1;
    status = timedwait_timed_thread(info);
    info->parent->global_q->waiting -= 1;
  }

  unlock_port_list(info->parent->global_q);
  return status;
}

uint64_t get_next_dispatch_time(thread_config_t * config) {
  if(config->protocol==PERIODIC)
    return config->dispatch_time+config->period;
  return 0;
}

#if defined BENCHMARK
uint64_t max_exec_time[TASKS_NB];
#endif
extern process_config_t this_process;

error_code_t Await_Dispatch(thread_config_t * config)
{
  error_code_t ret = RUNTIME_OK;
  // reset freshness of input data ports
  int port_iter;
  for(port_iter=0; port_iter<config->in_ports_size;port_iter++) {
    port_reference_t * port_ref = config->in_ports[port_iter];
    if(port_ref->type != DATA)
      continue;
    thread_input_port_t * in_port = (thread_input_port_t *) port_ref->directed_port;
    if(in_port->status == FRESH) 
      in_port->status = DEFAULT;
    else if(in_port->status == UPDATED) // UPDATED means fresh for next dispatch
      in_port->status = FRESH;
  }
  // if not in mode, call await_mode
  // may occur if mode changed while thread was running (not synchronized thread)
  if(!Is_In_Current_Mode(config)) {
    Await_Mode(config);
    init_dispatch_configuration(config);
  }
  else
  {
	config->state = SUSPENDED_THREAD_STATE;
	get_time_multiarch(&config->finish_time);
#if defined BENCHMARK && (defined USE_POSIX || defined USE_POK || defined USE_FREERTOS)
	uint64_t exec_time = config->finish_time-config->dispatch_time;
	if(exec_time>max_exec_time[config->id])
	    max_exec_time[config->id] = exec_time;
	printf("Thread: %s\n", config->name);
	printf("Max Response Time (ms) = %u\n", max_exec_time[config->id]);

printf("FIN %ld", config->finish_time);
#endif
#if defined BENCHMARK && defined USE_OSEK
	unsigned int exec_time = config->finish_time-config->dispatch_time;
	display_goto_xy(0, config->id*2);
	if(strlen(config->name)<15)
		display_string(config->name);
	else
	{
		char txt1[16];
		int i, k=0;

		for(i=strlen(config->name)-10;i<=strlen(config->name);i++)
		{
			txt1[k]=config->name[i];
			k++;
		}
		display_string(txt1);
	}


    char txt2[11] = "RT (ms) = ";
    char txt3[5];
    if(finish_t.sec > 3 && exec_time>max_exec_time[config->id])
    	max_exec_time[config->id] = exec_time;

    itoa(max_exec_time[config->id], txt3, 10);
    display_goto_xy(0, config->id*2+1);
    display_string(strcat(txt2,txt3));
    display_update();

#endif
    switch (config->protocol)
    {
      case PERIODIC:
        await_periodic_dispatch((target_periodic_thread_config_t*)config->thread);
        break;
      case SPORADIC:
    	  await_sporadic_dispatch((target_sporadic_thread_config_t*) config->thread);
    	  break;
      case APERIODIC:
        await_aperiodic_dispatch((target_aperiodic_thread_config_t*)config->thread);
        break;
      case HYBRID:
        await_hybrid_dispatch((target_hybrid_thread_config_t*)config->thread);
        break;
      case TIMED:
        await_timed_dispatch((target_timed_thread_config_t*)config->thread);
        break;
      case TIME_TRIGGERED:
    	await_time_triggered_dispatch((target_timetriggered_thread_config_t*)config->thread);
    	break;
      default:
        return RUNTIME_INVALID_PARAMETER;
    }

    // if not in mode, call await_mode
    // may occur if mode changed while thread was suspended
    if(!Is_In_Current_Mode(config)) {
    	Await_Mode(config);
	init_dispatch_configuration(config);
    }
  }

#if defined(RUNTIME_DEBUG)
  uint64_t dispatch_t;
  get_time_multiarch(&dispatch_t);
  aadl_runtime_debug("Dispatch %s @t=%lu (ns)\n", config->name, dispatch_t);
#endif

  // check inputs with immediate connections are available
  int i;
  for(i=0; i<config->in_ports_size; i++)
  {
	  port_reference_t * port_ref = config->in_ports[i];
	  thread_input_port_t * in_port = (thread_input_port_t*) port_ref->directed_port;

	  if(!in_port->in_immediate_connection)
		  continue;
	  
	  uint8_t fresh;
	  Updated(port_ref, &fresh);
	  if(!fresh)
	  {
#if defined RUNTIME_DEBUG && (defined USE_POK || defined USE_POSIX)
		  aadl_runtime_debug("released task but port content is not fresh!\n");
#endif
		  error_code_t err = Error_Handler(in_port->parent->parent, RUNTIME_OUTDATED_INPUT);
		  if(err)
			  return err;
	  }
  }
  config->state = RUNNING_THREAD_STATE;
  return ret;
}

error_code_t keep_busy_until(uint64_t finish_time_ns)
{
  uint64_t current_time;

#if defined(RUNTIME_DEBUG)
  get_time_multiarch(&current_time);
  aadl_runtime_debug("Busy from %lu\n", current_time);
#endif
  char reached = 0;
  while(!reached)
    {
      get_time_multiarch(&current_time);
      if(current_time >= finish_time_ns)
    	  reached = 1;
    }
#if defined(RUNTIME_DEBUG)
  get_time_multiarch(&current_time);
  aadl_runtime_debug("Busy until %lu\n", current_time);
#endif
  return RUNTIME_OK;
}
