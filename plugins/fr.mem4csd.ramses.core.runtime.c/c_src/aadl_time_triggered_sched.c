/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_runtime_services.h"
#include "aadl_multiarch.h"
#include "aadl_modes_standard.h"
#include "aadl_ports.h"
#include "aadl_dispatch.h"
#include "dimensions.h"
#include "globals.h"

#define LAST_SCHED_INDEX -1

void reinit_tfe_history(unsigned int task_id)
{
	if(this_process.threads[task_id]->m_k_constraint==NULL)
		return;

	this_process.threads[task_id]->m_k_constraint->tfe_count=0;

	char * array = this_process.threads[task_id]->m_k_constraint->tfe_history;
	unsigned int size = this_process.threads[task_id]->m_k_constraint->tfe_count_frame;
	memset(array, size, 0);
}

void enqueue_tfe_history(unsigned int task_id, char tfe)
{
	if(this_process.threads[task_id]->m_k_constraint==NULL)
			return;
	if(tfe)
		this_process.threads[task_id]->m_k_constraint->tfe_count++;
	unsigned int * idx = &this_process.threads[task_id]->m_k_constraint->tfe_history_idx;
	unsigned int size = this_process.threads[task_id]->m_k_constraint->tfe_count_frame;
	this_process.threads[task_id]->m_k_constraint->tfe_history[*idx] = tfe;
	*idx = *idx + 1;
	*idx = *idx % size;
}

void dequeue_tfe_history(unsigned int task_id)
{
	if(this_process.threads[task_id]->m_k_constraint==NULL)
			return;

	unsigned int size = this_process.threads[task_id]->m_k_constraint->tfe_count_frame;
	unsigned int last_idx = this_process.threads[task_id]->m_k_constraint->tfe_history_idx + size - 1;
	last_idx = last_idx % size;
	char is_tfe = this_process.threads[task_id]->m_k_constraint->tfe_history[last_idx];
	if(!is_tfe && this_process.threads[task_id]->m_k_constraint->tfe_count > 0)
	    this_process.threads[task_id]->m_k_constraint->tfe_count--;
}

extern mode_id_t source_mode;

void time_triggered_sched()
{
  unsigned int current_slot_idx = 0;
  unsigned int current_sched_time = 0;
  time_triggered_schedule_event_t * sched_table;

  thread_config_t * faulty = NULL;
  
  while(1) {
    mode_id_t mode;
    Current_System_Mode(&mode);
    if(mode==mode_transition_in_progress)
      mode = source_mode;

    sched_table = schedule_table_per_mode[mode];

    // wait next scheduling time
    unsigned int next_sched_time = sched_table[current_slot_idx].sched_time;
    if(next_sched_time > current_sched_time) {
      unsigned long nsec = (next_sched_time - current_sched_time)*1000000;
      await_time_triggered_delay(nsec);
      current_sched_time = next_sched_time;
    }
    
#ifdef RUNTIME_DEBUG
    printf("Schedule table index %d in mode %d \n", current_slot_idx, mode);
#endif
      
    // check task deadline is enforced
    char TFE = 0;
    int i, to_continue=0;
    
    for(i=0;i<sched_table[current_slot_idx].task_deadline_nb;i++) {
      unsigned int task_dea_idx = sched_table[current_slot_idx].task_deadline_idx[i];
      
      TFE = (faulty!=this_process.threads[task_dea_idx]) && (this_process.threads[task_dea_idx]->state == RUNNING_THREAD_STATE);
      if(TFE) {
	faulty = this_process.threads[task_dea_idx];
    	  if(this_process.threads[task_dea_idx]->criticality[mode] > this_process.criticality[mode]) {
    		  reinit_tfe_history(task_dea_idx);
		  
		  // fautly thread will be stopped as part of the mode
		  // transition
#if defined(RUNTIME_DEBUG) && (defined(USE_POSIX) || defined(USE_POK))
		  printf("Send timing failure event to change mode\n");
#endif
    		  int ret = Put_Value(&this_processor_timing_failure_event, NULL);
#if defined(DEBUG) && (defined(USE_POSIX) || defined(USE_POK))
		  if(ret!=0)
		    printf("ERROR puting tfe out\n");
#endif
    		  ret = Send_Output(&this_processor_timing_failure_event);
#if defined(DEBUG) && (defined(USE_POSIX) || defined(USE_POK))
		  if(ret!=0)
		    printf("ERROR sending tfe\n");
#endif
    		  to_continue = 1;
		  Put_Value(&this_processor_hyperperiod, NULL);
		  Send_Output(&this_processor_hyperperiod);
    	  }
    	  else {
    		  enqueue_tfe_history(task_dea_idx, 1);
    		  if(this_process.threads[task_dea_idx]->m_k_constraint!=NULL
    				  && this_process.threads[task_dea_idx]->m_k_constraint->tfe_count <= this_process.threads[task_dea_idx]->m_k_constraint->tfe_limit)
    			  Do_Send_Output_Data_Thread(this_process.threads[task_dea_idx]);

    		  // restart faulty thread
		  Stop_Thread(this_process.threads[task_dea_idx]);
		  Start_Thread(this_process.threads[task_dea_idx]);
    	  }
#if defined USE_LINUX || USE_FREERTOS
	  printf("TFE in task %s\n", this_process.threads[task_dea_idx]->name);
#endif
      }
      else if(faulty!=this_process.threads[task_dea_idx])
      {
    	  enqueue_tfe_history(task_dea_idx, 0);
      }
      dequeue_tfe_history(task_dea_idx);
    }
    
    // no more tasks to schedule
    if(sched_table[current_slot_idx].task_release_nb == (uint16_t)LAST_SCHED_INDEX) {
      current_slot_idx = 0;
      current_sched_time = 0;
      to_continue = 0;
      faulty = NULL;
      continue;
    } else if(to_continue) {
      current_slot_idx++;
      continue;
    }

    // place tasks on cores
    for(i=0;i<sched_table[current_slot_idx].task_placement_nb;i++) {
      unsigned int task_mig_idx = sched_table[current_slot_idx].task_placement_idx[i];
      unsigned int core_mig = sched_table[current_slot_idx].placement_core[i];
      bind_thread_to_core_id(this_process.threads[task_mig_idx], core_mig);
    }

    // execute next tasks
    for(i=0;i<sched_table[current_slot_idx].task_release_nb;i++) {
      unsigned int task_rel_idx = sched_table[current_slot_idx].task_release_idx[i];
      target_timetriggered_thread_config_t * tt_thread = (target_timetriggered_thread_config_t *) this_process.threads[task_rel_idx]->thread;
#ifdef RUNTIME_DEBUG
      printf("Release task %s @slot idx %d\n", this_process.threads[task_rel_idx]->name, current_slot_idx);
#endif
      dispatch_time_triggered_thread(tt_thread);
      tt_thread->parent->dispatch_time = sched_table[current_slot_idx].sched_time;
    }

    // promote tasks if necessary
    for(i=0;i<sched_table[current_slot_idx].task_promotion_nb;i++) {
      unsigned int task_prom_idx = sched_table[current_slot_idx].task_promotion_idx[i];
      set_thread_priority_multiarch(this_process.threads[task_prom_idx], this_process.threads[task_prom_idx]->priority+1);
    }

    // degrade tasks priority if necessary
    for(i=0;i<sched_table[current_slot_idx].task_degradation_nb;i++) {
      unsigned int task_deg_idx = sched_table[current_slot_idx].task_degradation_idx[i];
      set_thread_priority_multiarch(this_process.threads[task_deg_idx], this_process.threads[task_deg_idx]->priority-1);
    }

    current_slot_idx++;
  }
}
