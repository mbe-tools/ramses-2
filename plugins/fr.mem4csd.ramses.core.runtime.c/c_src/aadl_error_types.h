/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef _AADL_ERROR_TYPES_H_
#define _AADL_ERROR_TYPES_H_

typedef enum error_code_t
{
  RUNTIME_OK,
  RUNTIME_EMPTY_QUEUE,
  RUNTIME_FULL_QUEUE,
  RUNTIME_LOCK_ERROR,
  RUNTIME_OUT_OF_BOUND,
  RUNTIME_INVALID_PARAMETER,
  RUNTIME_INVALID_SERVICE_CALL,
  RUNTIME_OUTDATED_INPUT, // not fresh for an input port of an immediate connection
  RUNTIME_SYSCALL_ERROR,
  RUNTIME_START_THREAD_ERROR,
  RUNTIME_WAIT_DISPATCH_ERROR,
  RUNTIME_DEADLINE_MISS,
  RUNTIME_STOP_THREAD // in case of an active thread during mode switch 
} error_code_t; // required by aadl_error.h and aadl_runtime_services.h

#endif
