/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef AADL_PORTS_H
#define AADL_PORTS_H

#include "aadl_runtime_services.h"
#include "aadl_error.h"
#include "aadl_modes_standard.h"
#include "aadl_multiarch.h"

#ifdef USE_POSIX

#ifdef DEBUG
#include <stdio.h>
#endif // DEBUG
#endif // USE_POSIX


// Variants of standard calls
error_code_t Do_Send_Output_Data_Thread(thread_config_t * thread);
error_code_t Do_Send_Output(port_reference_t * src_port);

error_code_t Send_Output_Destination(port_connection_t * connection);
error_code_t Send_Output_Inter_Thread(port_connection_t * connection);

error_code_t Send_Output_Inter_Thread_Fifo(port_reference_t * source, port_reference_t * destination);
error_code_t Send_Output_Inter_Thread_Fifo_Data(void * data, port_reference_t * destination);

error_code_t Send_Output_Inter_Thread_Sampled(port_reference_t * source, port_reference_t * destination);
error_code_t Send_Output_Inter_Thread_Sampled_Data(void * data, port_reference_t * destination);

error_code_t Send_Output_Inter_Thread_PDP_LowET(port_connection_t * connection);
error_code_t Send_Output_Inter_Thread_PDP_LowMFP(port_connection_t * connection);

error_code_t Send_Output_Inter_Process(port_connection_t * connection);

error_code_t Receive_Input_Source(port_connection_t * connection);
error_code_t Receive_Input_Inter_Thread(port_connection_t * connection);
error_code_t Receive_Input_Inter_Thread_Fifo(port_reference_t * destination);
error_code_t Receive_Input_Inter_Thread_Sampled(port_reference_t * destination);
error_code_t Receive_Input_Inter_Thread_PDP_LowET(port_connection_t * connection);

error_code_t Receive_Input_Inter_Process(port_connection_t * connection);


// operations on all ports of a thread
error_code_t Send_Output_Thread(thread_config_t * thread);
error_code_t Receive_Input_Thread(thread_config_t * thread);

error_code_t Flush_AADL_Port(port_reference_t * port);
error_code_t Flush_AADL_Thread_Ports(thread_config_t * config);



#endif //AADL_PORTS_H
