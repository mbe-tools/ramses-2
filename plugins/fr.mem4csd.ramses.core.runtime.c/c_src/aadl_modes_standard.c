/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_runtime_services.h"
#include "aadl_dispatch.h"
#include "aadl_multiarch.h"
#include "aadl_ports.h"
#include "bitset.h"
#include "globals.h"
#include "dimensions.h"

extern mode_id_t current_mode;
extern mode_id_t source_mode;
extern mode_id_t target_mode;

error_code_t Current_System_Mode(mode_id_t * mode)
{
  error_code_t ret = RUNTIME_OK;
  // lock
  *mode = current_mode;
  //unlock
  return ret;
}

char Is_Thread_Active(thread_config_t * config)
{
	return (config->state != STOPPED_THREAD_STATE
			&& config->state != INITIALIZING_THREAD_STATE);
}

void Restart_Thread(thread_config_t * config)
{
	if(config->state == STOPPED_THREAD_STATE)
	{
		Start_Thread(config);
	}
}

extern char * tasks_in_mode[MODES_NB];

char Is_In_Mode(thread_config_t * config, mode_id_t mode)
{
	char * tasks = tasks_in_mode[mode];
	return BITTEST(tasks, config->id);
}

char Is_In_Current_Mode(thread_config_t * config)
{
#if(MODES_NB > 1)
  if(current_mode == mode_transition_in_progress
     && Is_In_Mode(config, source_mode)
     && Is_In_Mode(config, target_mode))
    return 1;
  return Is_In_Mode(config, current_mode);
#else
  return 1;
#endif
}

extern unsigned long reached_init_barrier;

error_code_t perform_activation_or_deactivation(thread_config_t * config, unsigned long isInSrcMode, unsigned long isInTgtMode)
{
  error_code_t ret = RUNTIME_OK;
  // deactivate
  if(isInSrcMode && !isInTgtMode)
  {
    if(config->deactivate_entrypoint!=NULL)
    {
      error_code_t (*deactivate_entrypoint)(runtime_addr_t context) = config->deactivate_entrypoint;
      ret = (*deactivate_entrypoint)(config->context);
    }
    else
    {
      ret |= Flush_AADL_Thread_Ports(config);
    }
  }
  // activate
  if(!isInSrcMode && isInTgtMode)
  {
    if(config->activate_entrypoint!=NULL)
    {
      error_code_t (*activate_entrypoint)(runtime_addr_t context) = config->activate_entrypoint;
      ret = (*activate_entrypoint)(config->context);
    }
  }
  return ret;
}


#if(MODES_NB > 1)
error_code_t Mode_Transition_In_Progress(mode_id_t * mode)
{
  error_code_t ret = RUNTIME_OK;
  source_mode = current_mode;
  current_mode = mode_transition_in_progress;
  target_mode = *mode;
  unsigned int i;
  for(i=0; i<TASKS_NB; i++) {
    thread_config_t * config = this_process.threads[i];
    unsigned long isInSrcMode = Is_In_Mode(config, source_mode);
    unsigned long isInTgtMode = Is_In_Mode(config, target_mode);

    ret = perform_activation_or_deactivation(config, isInSrcMode, isInTgtMode);
  }

  return ret;
}
#endif

/**
 * Set_System_Mode should be executed in a mode transition
 * handler (thread) at higher priority than mode-dependent
 * threads.
 */
error_code_t Set_System_Mode(mode_id_t * mode, mode_switch_protocol_t protocol)
{
  error_code_t ret = RUNTIME_OK;
  ret = lock_init_barrier();
  
  target_mode = *mode;
  unsigned int i;
  for(i=0; i<TASKS_NB; i++) {
    thread_config_t * config = this_process.threads[i];
    unsigned long isInSrcMode = Is_In_Mode(config, source_mode);
    unsigned long isInTgtMode = Is_In_Mode(config, target_mode);
#if defined RUNTIME_DEBUG && (defined USE_POK || defined USE_POSIX)
    printf("Setting mode, %s in src mode: %ld; in tgt mode: %ld\n", config->name, isInSrcMode, isInTgtMode);
#endif
    if(protocol == EMERGENCY_MODE_SWITCH) {
      // this has to be performed since mtip does not exist for emergency mode switches
      ret = perform_activation_or_deactivation(config, isInSrcMode, isInTgtMode);
      
    } else {
      unsigned long isInCurMode = Is_In_Current_Mode(config);
      if(!isInCurMode && isInSrcMode && !isInTgtMode && config->state==RUNNING_THREAD_STATE) {
	Stop_Thread(config);
	config->state = STOPPED_THREAD_STATE;
	if(config->recover_entrypoint!=NULL)
	{
	  config->execution_error = RUNTIME_STOP_THREAD;
	  error_code_t (*recover_entrypoint)(runtime_addr_t context) = config->recover_entrypoint;
	  ret = (*recover_entrypoint)(config->context);
	}
      }
    }
    if(!isInSrcMode && isInTgtMode)
    {
      if(config->state==STOPPED_THREAD_STATE)
	Start_Thread(config); // restart thread if stopped
    }
  }
  
  current_mode = target_mode;

  // count already active threads in mode
  /// and start stopped threads
  int already_active_threads=0;
  for(i=0; i<thread_number_in_all_modes;i++)
  {
	  thread_config_t * config = thread_configs_in_all_modes[i];
	  if(Is_Thread_Active(config))
		  already_active_threads++;
  }

  for(i=0; i<thread_number_per_mode[current_mode];i++)
  {
	  thread_config_t * config = thread_configs_per_mode[current_mode][i];
	  if(Is_Thread_Active(config))
	  	  already_active_threads++;
  }

  int number_of_missing_threads = thread_number_in_all_modes
		  + thread_number_per_mode[current_mode]
		  - already_active_threads;
#if defined RUNTIME_DEBUG
    printf("Wait for %d threads\n", number_of_missing_threads);
#endif

  if(number_of_missing_threads>0)
	  wait_threads_init_multiarch(number_of_missing_threads);

#if defined RUNTIME_DEBUG 
  printf("All threads ready\n");
#endif

  set_start_time();

#if defined RUNTIME_DEBUG 
  uint64_t start_t;
  get_start_time(&start_t);
  printf("Start time %lu nsec\n", start_t);
#endif
	  
  for(i=0; i<thread_number_in_all_modes;i++)
  {
	  thread_config_t * config = thread_configs_in_all_modes[i];
	  release_task_in_mode_multiarch(config);
  }

  for(i=0; i<thread_number_per_mode[current_mode];i++)
  {
  	  thread_config_t * config = thread_configs_per_mode[current_mode][i];
  	  release_task_in_mode_multiarch(config);
  }

  unlock_init_barrier();
  return ret;
}

void set_start_time()
{
  int i;
  for(i=0; i<TASKS_NB; i++) {
    thread_config_t * config = this_process.threads[i];
    init_dispatch_configuration(config);
  }
  set_start_time_multiarch();
}


error_code_t Await_Mode(thread_config_t * config)
{
  error_code_t ret = Await_Mode_multiarch(config);
  if(config->protocol!=PERIODIC)
    return Await_Dispatch(config);
  return ret;
}
