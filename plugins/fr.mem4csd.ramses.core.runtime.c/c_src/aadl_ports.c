/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_ports.h"
#include "aadl_dispatch.h"
#include <string.h>


extern process_config_t this_process;

error_code_t Send_Output_Destination(port_connection_t * connection) {
	error_code_t status = RUNTIME_OK;
	port_reference_t * destination = connection->destination;

#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
	printf("Send output destination type = %d\n", connection->destination->parent_type);
#endif
	if(destination->parent_type == THREAD) {
	  	thread_input_port_t * in_port = (thread_input_port_t *) connection->destination->directed_port;
		status = Send_Output_Inter_Thread(connection);
		if(status!=RUNTIME_OK) {
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
			printf("Send_Output_Destination: ERROR, local send_output failed\n");
#endif
			return status;
		}
	}
	else if(destination->parent_type == PROCESS) {
		status = Send_Output_Inter_Process(connection);
		if(status!=RUNTIME_OK) {
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
			printf("Send_Output: ERROR, inter process send_output failed\n");
#endif
			return status;
		}
	}
	return status;
}

error_code_t Send_Output_Inter_Process(port_connection_t * connection)
{
  port_reference_t * source = connection->source;
  thread_output_port_t * out_port = (thread_output_port_t*) source->directed_port;
  void * value = out_port->port_variable;
  unsigned int msg_size = source->msg_size;
  return send_out_process(connection->destination, value, msg_size);
}

error_code_t Send_Output_Inter_Thread_Sampled(port_reference_t * source, port_reference_t * destination)
{
	thread_output_port_t * out_port = (thread_output_port_t*) source->directed_port;
	void * value = out_port->port_variable;
	return Send_Output_Inter_Thread_Sampled_Data(value, destination);
}

error_code_t Send_Output_Inter_Thread_Sampled_Data(void * data, port_reference_t * destination)
{
	error_code_t status = RUNTIME_OK;
	thread_input_port_t * in_port = (thread_input_port_t *) destination->directed_port;
	status = lock_port_reference(in_port);
	if (status != RUNTIME_OK)
	{
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
		printf("Send_Output_Inter_Thread: ERROR %d when acquiring lock\n", status);
#endif
		return status;
	}

	status = Send_Output_Inter_Thread_Fifo_Data(data, destination);

	status = unlock_port_reference(in_port);
	
	return status;
}

error_code_t Send_Output_Inter_Thread_PDP_LowMFP(port_connection_t * connection)
{
	error_code_t status = RUNTIME_OK;

	port_reference_t * source = connection->source;
	port_reference_t * destination = connection->destination;

	thread_input_port_t * in_port = (thread_input_port_t *) destination->directed_port;

	PDP_lowMFP_attr_t * attr = (PDP_lowMFP_attr_t *) connection->protocol->protocol_attr;

	thread_output_port_t * out_port = (thread_output_port_t*) source->directed_port;
	void * value = out_port->port_variable;

	thread_config_t * thread_config = (thread_config_t *) source->parent;
	target_periodic_thread_config_t * periodic_config = (target_periodic_thread_config_t *) thread_config->thread;
	// TODO: define as a target service
	int iteration_counter = periodic_config->iteration_counter % attr->write_lookup_table_size;

	int64_t tmp;
	int SEJD = 0;
	int CDW = 0;

	uint16_t writer_task_period = thread_config->period;
	uint16_t writer_task_deadline = thread_config->deadline;
	uint32_t writing_time = (uint32_t) iteration_counter * writer_task_period + writer_task_deadline;
	int simultaneous_writers = 0;
	uint32_t other_period, other_deadline;
	int cnx_idx;
	char itself = 0;
	thread_config_t * other_writer;
	for(cnx_idx=0; cnx_idx<in_port->connected_ports_length;cnx_idx++)
	{
	    other_writer = (thread_config_t*) in_port->sources[cnx_idx]->source->parent;
	    other_period = other_writer->period;
	    other_deadline = other_writer->deadline;
	    // (t - Dj)/Pj can be negative, and its floor equals to -1
	    // we don't handle cases where it equals less than -1...
	    if(other_writer->id==thread_config->id)
	      itself = 1;
	    if ((tmp = writing_time - other_deadline) >= 0) {
	      SEJD += ((uint64_t)tmp / other_period)+1; // + 1 ? 
	    }
	    else
	    {
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
	      printf("ERROR, negative writing time %d \n",writing_time - other_deadline );
#endif
	    }
	    if (((writing_time - other_deadline) % other_period) == 0
	    		&& itself==1) {
	      simultaneous_writers++;
	    }
	}
	// TODO add mode specific writers

	CDW = SEJD - simultaneous_writers - 1; // - 1 to write at the beginning of the queue
	// TODO data port
	if (destination->type == EVENT_DATA) {
		event_data_port_t * edp = (event_data_port_t*) in_port->content;
		char * buf = (char * ) edp->offset;
		CDW = CDW % edp->queue_size;
		if(CDW < 0)
			CDW += edp->queue_size;

#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
		printf("send in %d\n", CDW);
#endif
		memcpy(&buf[CDW*(destination->msg_size/sizeof(char))],
				value,
				destination->msg_size);
	}
	// TODO event port
	return status;
}

error_code_t Send_Output_Inter_Thread_PDP_LowET(port_connection_t * connection)
{
	error_code_t status = RUNTIME_OK;

	port_reference_t * source = connection->source;
	port_reference_t * destination = connection->destination;

	thread_input_port_t * in_port = (thread_input_port_t *) destination->directed_port;

	PDP_lowET_attr_t * attr = (PDP_lowET_attr_t *) connection->protocol->protocol_attr;

	thread_output_port_t * out_port = (thread_output_port_t*) source->directed_port;
	void * value = out_port->port_variable;

	thread_config_t * thread_config = (thread_config_t *) source->parent;
	target_periodic_thread_config_t * periodic_config = (target_periodic_thread_config_t*) thread_config->thread;

	if (destination->type == DATA) {
		data_port_t * port_data = (data_port_t *) in_port->content;

		int iteration_counter = periodic_config->iteration_counter%attr->write_lookup_table_size;
		mode_id_t current_mode;
		Current_System_Mode(&current_mode);
		void * addr;
		if(attr->write_lookup_table[iteration_counter])
			addr = port_data->data_rw_per_mode[port_data->src_index_by_mode[current_mode]].writing_data;
		else
			addr = port_data->data_rw_per_mode[port_data->src_index_by_mode[current_mode]].reading_data;

		memcpy(addr,
				value,
				destination->msg_size);
	}
	else if (destination->type == EVENT_DATA) {
		event_data_port_t * edp = (event_data_port_t *) in_port->content;
		char * buf = (char * ) edp->offset;
		int iteration_counter = (periodic_config->iteration_counter)%(attr->write_lookup_table_size);
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
		printf("send in %d\n", attr->write_lookup_table[iteration_counter]);
#endif
		memcpy(&buf[attr->write_lookup_table[iteration_counter]*(destination->msg_size/sizeof(char))],
							value,
							destination->msg_size);
	}
	else if (destination->type == EVENT) {
		event_port_t * ep = (event_port_t *) in_port->content;
		char * buf = (char * ) ep->offset;
		int iteration_counter = (periodic_config->iteration_counter)%(attr->write_lookup_table_size);
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
		printf("send in %d\n", attr->write_lookup_table[iteration_counter]);
#endif
		memcpy(&buf[attr->write_lookup_table[iteration_counter]*(destination->msg_size/sizeof(char))],
							value,
							1);
	}
    return status;
}

char Is_Full(port_reference_t * port)
{
  int nb_msg=0;
  thread_input_port_t * in_port = (thread_input_port_t*) port->directed_port;
  if (port->type == EVENT_DATA) {
    event_data_port_t * port_ed = (event_data_port_t *) in_port->content;
    if(port_ed->beg_read_idx > port_ed->end_read_idx)
      nb_msg += (port_ed->queue_size - port_ed->beg_read_idx + port_ed->end_read_idx);
    else
      nb_msg += (port_ed->end_read_idx - port_ed->beg_read_idx);
    if(port_ed->end_write_idx < port_ed->end_read_idx)
      nb_msg += (port_ed->queue_size - port_ed->end_read_idx + port_ed->end_write_idx);
    else
      nb_msg += (port_ed->end_write_idx - port_ed->end_read_idx);
    return (nb_msg >= port_ed->queue_size);
  }
  else if (port->type == EVENT) {
    event_port_t * port_ed = (event_port_t *) in_port->content;
    if(port_ed->beg_read_idx > port_ed->end_read_idx)
      nb_msg += (port_ed->queue_size - port_ed->beg_read_idx + port_ed->end_read_idx);
    else
      nb_msg += (port_ed->end_read_idx - port_ed->beg_read_idx);
    if(port_ed->end_write_idx < port_ed->end_read_idx)
      nb_msg += (port_ed->queue_size - port_ed->end_read_idx + port_ed->end_write_idx);
    else
      nb_msg += (port_ed->end_write_idx - port_ed->end_read_idx);
    return (nb_msg >= port_ed->queue_size);
  }
  return 0;
}

error_code_t Send_Output_Inter_Thread_Fifo(port_reference_t * source, port_reference_t * destination)
{
	thread_output_port_t * out_port = (thread_output_port_t*) source->directed_port;
	void * value = out_port->port_variable;
	return Send_Output_Inter_Thread_Fifo_Data(value, destination);
}

error_code_t Send_Output_Inter_Thread_Fifo_Data(void * data, port_reference_t * destination)
{
	error_code_t status = RUNTIME_OK;
	thread_input_port_t * in_port = (thread_input_port_t *) destination->directed_port;

	thread_config_t * th = in_port->parent->parent;

	uint64_t now;
	get_time_multiarch(&now);
	
	uint64_t next_dispatch_time = 0;
	if(in_port->input_time==DISPATCH_TIME)
	{
	  next_dispatch_time = get_next_dispatch_time(th);

	  if(th->protocol!=PERIODIC // next_dispatch_time = 0 in this case.
	     && in_port->parent->waiting
	     && (destination->type == EVENT_DATA || destination->type == EVENT_DATA))
	  {
	    next_dispatch_time = now;
	  }
	}
	if (destination->type == DATA)
	{
		data_port_t * port_data = (data_port_t *) in_port->content;
		mode_id_t current_mode;
		status = Current_System_Mode(&current_mode);
		void * writing_addr = port_data->data_rw_per_mode[port_data->src_index_by_mode[current_mode]].writing_data;
		memcpy(writing_addr,
				data,
				destination->msg_size);
		in_port->parent->msg_nb++;
		if(th->finish_time>=th->dispatch_time &&
		   (next_dispatch_time==0 || now<=next_dispatch_time))
		  in_port->status=FRESH;
		else
		  in_port->status=UPDATED; // fresh for next dispatch

#if defined RUNTIME_DEBUG && defined USE_POSIX
		printf("Write @%p\n", writing_addr/*, *((char*) value)*/);
#endif
	}
	else if (destination->type == EVENT_DATA)
	{
		event_data_port_t * port_ed = (event_data_port_t *) in_port->content;
		if (Is_Full(destination)) {
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
			printf("Send_Output_Inter_Thread: ERROR, full queue\n");
#endif
			return RUNTIME_FULL_QUEUE;
		} else {

			in_port->parent->msg_nb++;

			if(in_port->parent->waiting)
			{
			  th->dispatch_time = now;
			  send_signal(in_port);
			}
			char * buf = (char * ) port_ed->offset;
			memcpy(&buf[port_ed->end_write_idx*(destination->msg_size)],
					data,
					destination->msg_size);
			port_ed->end_write_idx = (port_ed->end_write_idx+1) % port_ed->queue_size;
			if(next_dispatch_time==0 || now <= next_dispatch_time) {
			  port_ed->freeze_idx = port_ed->end_write_idx;
			}
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
			printf("Write in index %d, (event data port)\n", port_ed->end_write_idx/*, *((char*) value)*/);
#endif
		}
	}
	else
	{
		event_port_t * port_event = (event_port_t *) in_port->content;
		if (Is_Full(destination)) {
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
		  printf("Send_Output_Inter_Thread: ERROR, full queue\n");
#endif
			return RUNTIME_FULL_QUEUE;
		} else {
			in_port->parent->msg_nb++;
			if(in_port->parent->waiting)
			{
			  th->dispatch_time = now;
			  send_signal(in_port);
			}
			
			port_event->end_write_idx = (port_event->end_write_idx+1) % port_event->queue_size;
			if(next_dispatch_time==0 || now <= next_dispatch_time) {
			  port_event->freeze_idx = port_event->end_write_idx;
			}

#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
			printf("Write in index %d\n", port_event->end_write_idx);
#endif
		}
	}
	return status;

}

error_code_t Send_Output_Inter_Thread(port_connection_t * connection)
{
  port_reference_t * source = connection->source;
  port_reference_t * destination = connection->destination;

  switch(connection->protocol->protocol_type) {
  	  case SAMPLED :
  		  return Send_Output_Inter_Thread_Sampled(source, destination);
  	  case IMMEDIATE :
  		  return Send_Output_Inter_Thread_Fifo(source, destination);
  	  case PDP_LOWET:
  		  return Send_Output_Inter_Thread_PDP_LowET(connection);
  	  case PDP_LOWMFP:
  		  return Send_Output_Inter_Thread_PDP_LowMFP(connection);
  	  default:
  		return RUNTIME_INVALID_PARAMETER;
  }
}

error_code_t Do_Send_Output_Data_Thread(thread_config_t * thread)
{
	error_code_t status = RUNTIME_OK;
	int i;
	for (i = 0; i < thread->out_ports_size; i++)
	{
	  port_reference_t * src_port = thread->out_ports[i];
	  if(src_port->type != DATA)
		  continue;
	  status = Do_Send_Output(src_port);
	  if (status != RUNTIME_OK)
	    return status;
	}
	return status;
}

#if(MODES_NB>1)
extern mode_id_t target_mode;
#endif

error_code_t Do_Send_Output(port_reference_t * src_port)
{
	error_code_t status = RUNTIME_OK;
	thread_output_port_t * out_port = (thread_output_port_t*) src_port->directed_port;

	int i;
	for(i=0; i<out_port->connected_ports_length; i++) {
		port_connection_t * connection = out_port->destinations[i];
		error_code_t sod_status = Send_Output_Destination(connection);
		if(sod_status!=RUNTIME_OK)
		  status = sod_status;
	}

#if(MODES_NB>1)
	mode_id_t current_mode;
	status = Current_System_Mode(&current_mode);
	if(current_mode == mode_transition_in_progress)
	  current_mode = target_mode;
	int j;
	for (j = out_port->dest_index_by_mode[current_mode];
			j < out_port->dest_index_by_mode[current_mode+1];
			j++)
	{
		port_connection_t * connection = out_port->destinations[j];
		error_code_t sod_status = Send_Output_Destination(connection);
		if(sod_status!=RUNTIME_OK)
		  status = sod_status;
	}
#endif
	return status;
}

error_code_t Send_Output_Thread(thread_config_t * thread)
{
  error_code_t status = RUNTIME_OK;
  int i;
  for (i = 0; i < thread->out_ports_size; i++)
  {
    error_code_t so_status = Send_Output((thread->out_ports)[i]);
    if(so_status!=RUNTIME_OK)
 	status = so_status;
  }
  return status;
}

error_code_t Receive_Input_Thread(thread_config_t * thread)
{
  error_code_t status = RUNTIME_OK;
  int i;
  for (i = 0; i < thread->in_ports_size; i++)
  {
	error_code_t ri_status = Receive_Input((thread->in_ports)[i]);
	if(ri_status!=RUNTIME_OK)
 		status = ri_status;
  }
  return status;
}

error_code_t Receive_Input_Inter_Thread_Fifo(port_reference_t * destination)
{
	error_code_t status = RUNTIME_OK;
	int discarded_msg_nb = 0;

	thread_input_port_t * in_port = (thread_input_port_t*) destination->directed_port;

	if (destination->type == DATA)
	{
		data_port_t * port_data = (data_port_t *) in_port->content;
		if (in_port->status==FRESH)
		{
			mode_id_t current_mode;
			Current_System_Mode(&current_mode);
			void * read_addr = port_data->data_rw_per_mode[port_data->src_index_by_mode[current_mode]].reading_data;
			port_data->data_rw_per_mode[port_data->src_index_by_mode[current_mode]].reading_data = port_data->data_rw_per_mode[port_data->src_index_by_mode[current_mode]].writing_data;
			port_data->data_rw_per_mode[port_data->src_index_by_mode[current_mode]].writing_data = read_addr;
			discarded_msg_nb++;
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
			printf("Receive input local on data port\n");
#endif

		}
	}
	else if (destination->type == EVENT_DATA)
	{
		event_data_port_t * port_ed = (event_data_port_t *) in_port->content;
		if(port_ed->end_read_idx < port_ed->beg_read_idx)
			discarded_msg_nb = port_ed->queue_size-port_ed->beg_read_idx+port_ed->end_read_idx;
		else
			discarded_msg_nb = port_ed->end_read_idx-port_ed->beg_read_idx;

		port_ed->beg_read_idx = port_ed->end_read_idx;
		port_ed->end_read_idx = port_ed->freeze_idx;
		port_ed->freeze_idx = port_ed->end_write_idx;
		
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
		printf("Receive inputs local on event data port; first: %d, last: %d\n", port_ed->beg_read_idx, port_ed->end_read_idx);
#endif
	}
	else
	{
		event_port_t * port_event = (event_port_t *) in_port->content;
		if(port_event->end_read_idx < port_event->beg_read_idx)
			discarded_msg_nb = port_event->queue_size-port_event->beg_read_idx+port_event->end_read_idx;
		else
			discarded_msg_nb = port_event->end_read_idx-port_event->beg_read_idx;

		port_event->beg_read_idx = port_event->end_read_idx;
		port_event->end_read_idx = port_event->freeze_idx;
		port_event->freeze_idx = port_event->end_write_idx;
		
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
		printf("Receive inputs local on event port; first: %d, last: %d\n", port_event->beg_read_idx, port_event->end_read_idx);
#endif
	}

#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
	printf("Receive inputs local: number of discarded messages is %d\n", discarded_msg_nb);
#endif

	in_port->parent->msg_nb-=discarded_msg_nb;

	return status;
}

error_code_t Receive_Input_Inter_Thread_Sampled(port_reference_t * destination)
{
	error_code_t status = RUNTIME_OK;

	thread_input_port_t * in_port = (thread_input_port_t*) destination->directed_port;
	status = lock_port_reference(in_port);
	if (status != RUNTIME_OK)
	{
		Error_Handler(in_port->parent->parent, status);
#if defined DEBUG && (defined USE_POSIX || defined USE_POK)
		printf("Receive inputs local: ERROR %d when acquiring lock\n", status);
#endif
		return status;
	}

	status = Receive_Input_Inter_Thread_Fifo(destination);

	status = unlock_port_reference(in_port);
	if (status != RUNTIME_OK)
	{
#if defined DEBUG && defined USE_POSIX
		printf("Receive inputs local: ERROR %d when releasing lock\n", status);
#endif
	}
	return status;
}



error_code_t Receive_Input_Inter_Thread_PDP_LowMFP(port_connection_t * connection)
{
	error_code_t status = RUNTIME_OK;
	port_reference_t * destination = connection->destination;
	port_reference_t * source = connection->source;

	thread_input_port_t * in_port = (thread_input_port_t*) destination->directed_port;

	thread_config_t * thread_config = (thread_config_t *) destination->parent;
	target_periodic_thread_config_t * periodic_config = (target_periodic_thread_config_t *) thread_config->thread;

	PDP_lowMFP_attr_t * attr = (PDP_lowMFP_attr_t *) connection->protocol->protocol_attr;

	int CPR = 0, NPR=0;
	int reader_task_period = thread_config->period;
	int iteration_counter = periodic_config->iteration_counter % attr->read_lookup_table_size;

	int reading_time = iteration_counter * reader_task_period;
	int next_reading_time = ((periodic_config->iteration_counter+1)%attr->read_lookup_table_size) * reader_task_period;

	int writer_period, writer_deadline;
	int idx;
	thread_config_t * writer;
	// TODO miss modes for input port connection references (writers might be mode specific)
	for (idx=0 ; idx < in_port->connected_ports_length ; idx++) {
		writer = (thread_config_t*) in_port->sources[idx]->source->parent;
		writer_period = writer->period;
		writer_deadline = writer->deadline;
		CPR += ((this_process.hyperperiod+reading_time - writer_deadline) / writer_period) + 1;
		NPR += ((this_process.hyperperiod+next_reading_time - writer_deadline) / writer_period) + 1;
	}

	static char after_hyperperiod=0;

	if(!after_hyperperiod && thread_config->protocol == PERIODIC) {
		target_periodic_thread_config_t * periodic_config = (target_periodic_thread_config_t *) thread_config->thread;
		if(periodic_config->iteration_counter*thread_config->period>=this_process.hyperperiod)
			after_hyperperiod = 1;
	}

	if (source->type == EVENT_DATA) {
		event_data_port_t * port_ed = (event_data_port_t *) in_port->content;
		CPR = CPR % port_ed->queue_size;
		NPR = NPR % port_ed->queue_size;
		if(CPR < 0)
			CPR += port_ed->queue_size;
		if(NPR < 0)
			NPR += port_ed->queue_size;

		char fresh = (NPR!=CPR);

		if(fresh && after_hyperperiod) {
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
			printf("CPR = %d\n", CPR);
#endif
			port_ed->beg_read_idx = CPR; // -1 because get_value takes beg_read_idx+1;
			port_ed->end_read_idx = NPR;
		}
		else {
			port_ed->end_read_idx = port_ed->beg_read_idx;
			return RUNTIME_EMPTY_QUEUE;
		}
	} else {
		return RUNTIME_INVALID_PARAMETER; // implement when tested on event data ports
	}

	return status;
}

error_code_t Receive_Input_Inter_Thread_PDP_LowET(port_connection_t * connection)
{
	error_code_t ret = RUNTIME_OK;

	port_reference_t * destination = connection->destination;
	port_reference_t * source = connection->source;

	thread_input_port_t * in_port = (thread_input_port_t*) destination->directed_port;

	thread_config_t * thread_config = (thread_config_t *) destination->parent;
	target_periodic_thread_config_t * periodic_config = (target_periodic_thread_config_t *) thread_config->thread;
	PDP_lowET_attr_t * protocol_attr = (PDP_lowET_attr_t *) connection->protocol->protocol_attr;
	unsigned int iteration_counter = periodic_config->iteration_counter%protocol_attr->read_lookup_table_size;
	unsigned int next = (iteration_counter + 1) % protocol_attr->read_lookup_table_size;
	char fresh = (protocol_attr->read_lookup_table[next] != protocol_attr->read_lookup_table[iteration_counter]);
	// nothing to do in case of data port
	static char after_hyperperiod=0;
	if(!after_hyperperiod && thread_config->protocol == PERIODIC) {
		target_periodic_thread_config_t * periodic_config = (target_periodic_thread_config_t *) thread_config->thread;
		if(periodic_config->iteration_counter*thread_config->period>=this_process.hyperperiod)
			after_hyperperiod = 1;
	}


	if (source->type == EVENT_DATA) {
		event_data_port_t * port_ed = (event_data_port_t *) in_port->content;
		if(fresh && after_hyperperiod) {
			port_ed->beg_read_idx = protocol_attr->read_lookup_table[iteration_counter];
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
			printf("CPR = %d\n", port_ed->beg_read_idx+1);
#endif
			//-1 at the end not useful if not present above
			port_ed->end_read_idx = protocol_attr->read_lookup_table[next];
		}
		else {
			port_ed->end_read_idx = port_ed->beg_read_idx;
			return RUNTIME_EMPTY_QUEUE;
		}
	} else {
		return RUNTIME_INVALID_PARAMETER; // implement when tested on event data ports
	}
	return ret;
}

error_code_t Receive_Input_Inter_Thread(port_connection_t * connection)
{
	error_code_t status = RUNTIME_OK;
	switch(connection->protocol->protocol_type) {
		case SAMPLED :
			return Receive_Input_Inter_Thread_Sampled(connection->destination);
		case IMMEDIATE :
			return Receive_Input_Inter_Thread_Fifo(connection->destination);
		case PDP_LOWET:
			return Receive_Input_Inter_Thread_PDP_LowET(connection);
		case PDP_LOWMFP:
			return Receive_Input_Inter_Thread_PDP_LowMFP(connection);
		default:
			return RUNTIME_INVALID_PARAMETER;
	}

	return status;
}

error_code_t Receive_Input_Inter_Process(port_connection_t * connection)
{
	error_code_t status=RUNTIME_OK;
	port_reference_t * source = connection->source;
	port_reference_t * destination = connection->destination;

	thread_input_port_t * in_port = (thread_input_port_t*) destination->directed_port;

	void * dest_addr = NULL;
	if(source->type==DATA)
	{
		data_port_t * dp = (data_port_t *) in_port->content;
		mode_id_t current_mode;
		Current_System_Mode(&current_mode);

		dest_addr = dp->data_rw_per_mode[dp->src_index_by_mode[current_mode]].reading_data;
		status = receive_in_process(source,
				dest_addr);
		if(status==RUNTIME_OK)
		{
		  in_port->status=FRESH;
		  in_port->parent->msg_nb++;
		}
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
		printf("Receive inputs process on data port; %d messages\n", in_port->parent->msg_nb);
#endif
	}
	else if(source->type==EVENT_DATA)
	{
		event_data_port_t * edp = (event_data_port_t*) in_port->content;
		char * buf = (char *) edp->offset;
		edp->beg_read_idx = edp->end_read_idx;

		while(status==RUNTIME_OK)
		{
			unsigned int idx = (edp->end_read_idx)%edp->queue_size;
			dest_addr = &buf[idx*source->msg_size];
			status = receive_in_process(source,
					dest_addr);
			if(status==RUNTIME_OK)
			{
				edp->end_read_idx = (edp->end_read_idx+1) % edp->queue_size;
				in_port->parent->msg_nb++;
			}
		}
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
		printf("Receive inputs process on event data port; %d messages\n", in_port->parent->msg_nb);
#endif

	}
	else
	{
		event_port_t * ep = (event_port_t*) in_port->content;
		char * buf = (char *) ep->offset;
		ep->beg_read_idx = ep->end_read_idx;

		while(status==RUNTIME_OK)
		{
			unsigned int idx = (ep->end_read_idx)%ep->queue_size;
			dest_addr = &buf[idx*source->msg_size];
			status = receive_in_process(source,
					dest_addr);
			if(status==RUNTIME_OK)
			{
				ep->end_read_idx= (ep->end_read_idx+1) % ep->queue_size;
				in_port->parent->msg_nb++;
			}
		}
	}

	return status;
}

error_code_t Receive_Input_Source(port_connection_t * connection)
{
	error_code_t status = RUNTIME_OK;
	port_reference_t * source = connection->source;
	
#if defined RUNTIME_DEBUG && (defined USE_POSIX || defined USE_POK)
	printf("Receive_Input_Source type = %d\n", connection->source->parent_type);
#endif
	if(source->parent_type == THREAD) {
		status = Receive_Input_Inter_Thread(connection);
	}
	else if(source->parent_type == PROCESS) {
		status = Receive_Input_Inter_Process(connection);
	}
	return status;

}

error_code_t Flush_AADL_Port(port_reference_t * port)
{
  error_code_t ret = RUNTIME_OK;
  if(port->direction!=IN && port->direction!=INOUT)
    return RUNTIME_INVALID_PARAMETER;
  thread_input_port_t * in_port = (thread_input_port_t*) port->directed_port;

  in_port->parent->waiting = 0;
  in_port->status = DEFAULT;
  
  switch(port->type)
  {
  case DATA:
    break;
  case EVENT_DATA:
  {
    event_data_port_t * port_ed = (event_data_port_t *) in_port->content;
    port_ed->beg_read_idx = 0;
    port_ed->end_read_idx = 0;
    port_ed->end_write_idx = 0;
    port_ed->freeze_idx = 0;
    break;
  }
  case EVENT:
  {
    event_port_t * port_e = (event_port_t *) in_port->content;
    port_e->beg_read_idx = 0;
    port_e->end_read_idx = 0;
    port_e->end_write_idx = 0;
    port_e->freeze_idx = 0;
    break;
  }
  default:
    return RUNTIME_INVALID_PARAMETER;
  }
  return ret;
}

error_code_t Flush_AADL_Thread_Ports(thread_config_t * config)
{
  error_code_t ret = RUNTIME_OK;
  int i;
  for(i=0; i<config->in_ports_size; i++)
  {
    error_code_t fap_ret = Flush_AADL_Port(config->in_ports[i]);
    if(fap_ret!=RUNTIME_OK)
      ret = fap_ret;
  }
  return ret;
}

