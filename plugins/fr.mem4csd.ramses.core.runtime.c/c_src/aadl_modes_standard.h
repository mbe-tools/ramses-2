/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef __AADL_MODES_STANDARD_H__
#define __AADL_MODES_STANDARD_H__

#include "aadl_runtime_services.h"
#include "gtypes.h"
#include "bitset.h"

error_code_t Current_System_Mode(mode_id_t * mode);
error_code_t Mode_Transition_In_Progress(mode_id_t * mode);
error_code_t Set_System_Mode(mode_id_t * mode, mode_switch_protocol_t protocol);
char Is_In_Mode(thread_config_t * config, mode_id_t mode);
char Is_In_Current_Mode(thread_config_t * config);

void set_start_time();

#endif
