#ifndef AADL_RUNTIME_THREAD_TYPES
#define AADL_RUNTIME_THREAD_TYPES

#include "aadl_error_types.h"
#include "aadl_runtime_port.h"
#include "aadl_target_types.h"

typedef struct thread_input_port_t {
  struct port_list_t * parent;
  port_reference_t * port_ref;

  void * content; // can be event_port_t or data_port_t or event_data_port_t

  int connected_ports_length; // number of sources of a dst port

  port_connection_addr * sources;

  aadl_port_status_t status;
  char in_immediate_connection;
  aadl_input_time_t input_time;
} thread_input_port_t;

typedef struct thread_output_port_t {
  void * port_variable;
  char updated;

  /*
     * destinations contains references of ports connected to self
     *
     * from index 0 to index connected_ports_length-1 we have references
     * to ports connected to self in all modes
     *
     * from index connected_ports_length to index
     * connected_ports_length+dest_index_by_mode[nb_mode]
     * we have references to ports connected to self in each mode
     *
     * To access connected ports in mode m:
     *
     * for(j=dest_index_by_mode[m]; j<dest_index_by_mode[m+1];j++)
     *   destinations[j]
     *
     * */

  unsigned int nb_mode;
  int connected_ports_length; // number of destinations of a src port
  int16_t * dest_index_by_mode; // array of indexes in destinations
  port_connection_addr * destinations;
} thread_output_port_t;

typedef enum thread_state_t
{
  STOPPED_THREAD_STATE,
  INITIALIZING_THREAD_STATE,
  AWAITING_MODE_THREAD_STATE,
  SUSPENDED_THREAD_STATE,
  RUNNING_THREAD_STATE
} thread_state_t;

typedef enum dispatch_protocol_t {
  PERIODIC,
  SPORADIC,
  HYBRID,
  APERIODIC,
  BACKGROUND,
  TIMED,
  TIME_TRIGGERED
} dispatch_protocol_t;

typedef struct m_k_firm_t
{
	unsigned int tfe_count;
	unsigned const int tfe_limit; // m in m-k firm
	unsigned const int tfe_count_frame; // k in m-k firm
	unsigned char * tfe_history; // array of size k
	unsigned int tfe_history_idx;
} m_k_firm_t;


typedef struct thread_config_t
{
  dispatch_protocol_t protocol;
  struct port_list_t * global_q;
  // TODO: add the following and configure ICPP lock accordingly on POSIX
//  data_access_list_t * shared_resources;
//  const uint16_t data_access_nb;
  void * thread;  // can be periodic_thread_config_t or sporadic_thread_config_t
  	  	  // or timed_thread_config_t or hybrid_thread_config_t
  	  	  // or time_triggered_thread_config_t

  const uint64_t period;
  const uint64_t offset;
  const uint16_t priority;
  const uint64_t deadline;

  uint8_t criticality_levels_nb;
  uint8_t criticality[MODES_NB];
  target_thread_t * target_thread;
// fields for waiting mode
  target_wait_mode_t * wait_mode_event;
  uint8_t out_ports_size;
  port_reference_t ** out_ports;
  uint8_t in_ports_size;
  port_reference_t ** in_ports;

  m_k_firm_t * m_k_constraint;

  error_code_t execution_error;
  uint8_t error_port_idx;

  thread_state_t state;
  unsigned int id;
  error_code_t (*recover_entrypoint)(runtime_addr_t context);
  void (*compute_entrypoint)();
  error_code_t (*initialize_entrypoint)(runtime_addr_t context);
  error_code_t (*finalize_entrypoint)(runtime_addr_t context);
  error_code_t (*activate_entrypoint)(runtime_addr_t context);
  error_code_t (*deactivate_entrypoint)(runtime_addr_t context);
  const char * name;
  runtime_addr_t context;
  uint64_t dispatch_time;
  uint64_t finish_time;
  target_watchdog_t * watchdog;
} thread_config_t;

typedef thread_config_t * thread_config_addr;
typedef thread_config_addr * thread_config_addr_array;

#endif
