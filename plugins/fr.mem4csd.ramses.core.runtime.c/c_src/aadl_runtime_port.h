#ifndef AADL_RUNTIME_PORT_TYPES
#define AADL_RUNTIME_PORT_TYPES

#include <stdint.h>
#include "aadl_target_types.h"

typedef int port_id_t;

typedef enum port_type_t {
  DATA,
  EVENT,
  EVENT_DATA
} port_type_t;

typedef enum communication_role_t {
  SENDER,
  RECEIVER 
} communication_role_t;

typedef enum aadl_port_status_t {
  DEFAULT,
  FRESH,
  UPDATED
} aadl_port_status_t;

typedef enum aadl_input_time_t {
  DISPATCH_TIME,
  START_TIME
} aadl_input_time_t;


typedef enum port_direction_t {
  IN = 1,
  OUT = 2,
  INOUT = 3
} port_direction_t;

typedef struct event_port_t {
  uint16_t queue_size;    // max size of the queue
  uint16_t beg_read_idx;  // Begining of reading
  uint16_t end_read_idx;  // End of reading
  uint16_t end_write_idx; // End of writing
  uint16_t freeze_idx;    // Received before dispatch
  void * offset;
} event_port_t;

typedef struct event_data_port_t {
  uint16_t queue_size;    // max size of the queue
  uint16_t beg_read_idx;  // Begining of reading
  uint16_t end_read_idx;  // End of reading
  uint16_t end_write_idx; // End of writing
  uint16_t freeze_idx;    // Received before dispatch
  void * offset;
} event_data_port_t;

typedef struct double_buffer_t {
	void * reading_data;
	void * writing_data;
} double_buffer_t;

typedef struct data_port_t {
  unsigned char nb_mode;
  int16_t * src_index_by_mode; // array of indexes in [reading/writing]_data_per_mode
  double_buffer_t * data_rw_per_mode;
} data_port_t;

struct port_list_t;

typedef enum component_t {
	THREAD,
	PROCESS,
	PROCESSOR
} component_t;

struct connection_protocol_t;

typedef struct port_reference_t {
	port_type_t type;
	port_direction_t direction;
	component_t parent_type;
	void * parent; // can be thread_config_t or process_config_t
	void * directed_port; 	// can be thread_output_port_t or thread_input_port_t
				// or process_port_t or processor_port_t
	unsigned int msg_size;
} port_reference_t;

typedef port_reference_t* port_reference_addr;

typedef struct port_connection_t {
	port_reference_addr source;
	port_reference_addr destination;
	struct connection_protocol_t * protocol;
} port_connection_t;

typedef port_connection_t * port_connection_addr;

typedef struct process_port_t {
//	unsigned int identifier; // TODO: check if used or to be put in target_process_port_t?
	target_process_port_t * sys_port;
} process_port_t;

typedef process_port_t* process_port_addr;

typedef struct processor_port_t {
  target_processor_port_t * sys_port;
} processor_port_t;

#ifdef USE_POK
typedef struct pok_virtual_port_config_t
{
	char * port_name;
	pok_port_id_t * pok_port_id;
} pok_virtual_port_config_t;
#endif

enum communication_type
{
  SOCKETS_TCP,
  SOCKETS_UDP,
  MQTT,
  POK_QEMU
};

typedef struct processor_link_t
{
  enum communication_type type;
  /**
   * Points to a structure specific to a communication infrastructure:
   * <ul>
   *    <li><code>socket_config_t</code> in case of POSIX sockets;</li>
   *    <li><code>pok_virtual_port_config_t</code> in case of POK;</li>
   *    <li><code>ps_config_t</code> in case of Poha MQTT C client.</li>
   * </ul>
   */
  void * link_config; // may be socket_config_t or mqtt_config_t
  communication_role_t role;
} processor_link_t;

typedef struct processor_destination_port_t
{
  processor_link_t * link;
  void * destination_id; // could be a int * (port id for socket based comm') or char * (topic for mqtt based comm')
} processor_destination_port_t;

typedef processor_destination_port_t * processor_destination_port_addr;

typedef struct processor_outputport_t
{
  int destination_nb;
  processor_destination_port_addr * destinations;
} processor_outputport_t;

typedef struct local_destination_id_t
{
  unsigned int thread_idx;
  unsigned int port_idx;
} local_destination_id_t;

typedef struct processor_inputport_t
{
  processor_link_t * link;
  void * tmp_buf;
  unsigned int destination_nb;
  local_destination_id_t * local_destinations;
  unsigned int frag_number; // Number of the fragment received; useful for network communication
} processor_inputport_t;

typedef processor_inputport_t * processor_inputport_addr;


#endif
