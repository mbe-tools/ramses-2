/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef _AADL_DATA_H_
#define _AADL_DATA_H_

#include "aadl_runtime_services.h"
#include "aadl_error.h"

error_code_t Init_Resource(resource_t * resource);

error_code_t Get_Resource(resource_t * resource);

error_code_t Release_Resource(resource_t * resource);

error_code_t Get_Access_Resource(data_access_reference_t * access_ref);

error_code_t Release_Access_Resource(data_access_reference_t * access_ref);

#endif // _AADL_DATA_H_
