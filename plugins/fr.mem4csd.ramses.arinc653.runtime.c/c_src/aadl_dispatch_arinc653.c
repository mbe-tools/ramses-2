/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include <libc/string.h>
#include "aadl_multiarch.h"
#include "aadl_modes_standard.h"
#include "arinc653/blackboard.h"
#include "main.h"

void __stack_chk_fail(){}


int init_thread_config_multiarch(thread_config_t * config)
{
  (void) config;
  return 0; // init wait_mode_rez and wait_mode_event
}

int init_periodic_config_multiarch(target_periodic_thread_config_t * config)
{
  (void) config;
  return 0; // nothing to do
}

void get_time_multiarch(uint64_t * time)
{
  RETURN_CODE_TYPE ret = RUNTIME_OK;
  GET_TIME((SYSTEM_TIME_TYPE*)time, &ret);
}

uint64_t start_time;

void get_start_time(uint64_t * time)
{
  *time = start_time;
}

extern SEMAPHORE_ID_TYPE init_barrier_event;
extern SEMAPHORE_ID_TYPE init_barrier_mutex;

extern unsigned long reached_init_barrier;
extern mode_id_t current_mode;

error_code_t Await_Mode_multiarch(thread_config_t * config)
{
  error_code_t ret = RUNTIME_OK;
  if(config->state==INITIALIZING_THREAD_STATE
  		  && Is_In_Mode(config, current_mode))
  {
	  reached_init_barrier++;
#if defined RUNTIME_DEBUG
	  aadl_runtime_debug("Thread %s reached barrier, total=%d threads arrived\n", config->name, reached_init_barrier);
#endif
  }

  config->state=AWAITING_MODE_THREAD_STATE;

  RETURN_CODE_TYPE arinc653_ret = 0;
  SIGNAL_SEMAPHORE(init_barrier_event,
		    &arinc653_ret);
  SIGNAL_SEMAPHORE(init_barrier_mutex,
  		    &arinc653_ret);

#if defined RUNTIME_DEBUG
	  aadl_runtime_debug("Thread %s about to wait mode\n", config->name, reached_init_barrier);
#endif
  WAIT_SEMAPHORE (config->wait_mode_event->event,
		      INFINITE_TIME_VALUE,
  		      &arinc653_ret);
  return ret;
}

error_code_t await_periodic_dispatch_multiarch(target_periodic_thread_config_t * config)
{
  error_code_t ret;
  config->iteration_counter++;
  RETURN_CODE_TYPE arinc653_ret = 0;
  PERIODIC_WAIT(&arinc653_ret);
  config->parent->dispatch_time = config->iteration_counter*config->parent->period+config->parent->offset;
  if(arinc653_ret)
    ret = RUNTIME_WAIT_DISPATCH_ERROR;
  return ret;
}

void sporadic_thread_sleep(target_sporadic_thread_config_t * config)
{
  RETURN_CODE_TYPE arinc653_ret = 0;
  // relative timeout
  uint64_t elapsed_time = config->parent->finish_time - config->parent->dispatch_time;
  if(elapsed_time==0 ||
     elapsed_time >= config->parent->period)
    return;
#ifdef RUNTIME_DEBUG
  printf("Sporadic task %s about to wait for %lld microseconds\n", config->parent->name, (config->parent->period - elapsed_time)/1000);
#endif
  TIMED_WAIT((SYSTEM_TIME_TYPE)(config->parent->period - elapsed_time), &arinc653_ret);
}

error_code_t await_time_triggered_dispatch_multiarch(target_timetriggered_thread_config_t * config)
{
  error_code_t ret = RUNTIME_OK;
  RETURN_CODE_TYPE arinc653_ret = 0;
  WAIT_SEMAPHORE (config->event,
  		      INFINITE_TIME_VALUE,
    		  &arinc653_ret);
  return ret;
}

error_code_t await_time_triggered_delay(uint64_t delay_ns)
{
  RETURN_CODE_TYPE arinc653_ret = 0;
  TIMED_WAIT((SYSTEM_TIME_TYPE) delay_ns, &arinc653_ret);
  if(arinc653_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t bind_thread_to_core_id(thread_config_t * config, uint8_t core_id)
{
  // ONLY VALID DURING THE SYSTEM INITIALIZATION
  PROCESS_ID_TYPE tid = config->target_thread->thread_id;
  RETURN_CODE_TYPE arinc653_ret = 0;
  // uncomment when supported by targets
  //  INITIALIZE_PROCESS_CORE_AFFINITY(tid, (PROCESSOR_CORE_ID_TYPE) core_id, &arinc653_ret);
  if(arinc653_ret)
    return RUNTIME_SYSCALL_ERROR;
  return RUNTIME_OK;
}

error_code_t init_time_triggered_config(target_timetriggered_thread_config_t * config)
{
  (void) config;
  return RUNTIME_OK;
}

error_code_t stop_thread_multiarch(thread_config_t * config)
{
  error_code_t ret = RUNTIME_OK;
  RETURN_CODE_TYPE arinc653_ret = 0;
  
#ifdef RUNTIME_DEBUG
  aadl_runtime_debug("About to stop thread %s\n", config->name);
#endif
  PROCESS_ID_TYPE tid = config->target_thread->thread_id;
  STOP(tid, &arinc653_ret);
  return ret;
}

error_code_t init_hybrid_timer(target_hybrid_thread_config_t * config)
{
  config->iteration_counter = 0;
  return 0;
}

error_code_t hybrid_reset_timer(target_hybrid_thread_config_t * config)
{
  (void) config;
  return 0;
}

error_code_t timedwait_hybrid(target_hybrid_thread_config_t * config)
{
  RETURN_CODE_TYPE arinc653_ret = 0;
  SYSTEM_TIME_TYPE now;
  GET_TIME(&now,&arinc653_ret);
  SYSTEM_TIME_TYPE delay = start_time+config->iteration_counter*config->parent->period - now;
  if(delay>0)
    WAIT_SEMAPHORE (config->parent->global_q->event->event,
		    delay,
		    &arinc653_ret);
  if(!config->iteration_counter)
    config->iteration_counter++;
  else if(arinc653_ret==TIMED_OUT)
  {
     config->iteration_counter++;
     config->parent->dispatch_time = now;
  }
  return 0;
}

error_code_t timedwait_timed_thread(target_timed_thread_config_t * config)
{
  error_code_t ret = RUNTIME_OK;
#if defined(USE_POK) && defined(POK_NEEDS_ARINC653_SEMAPHORE)
  int unlock_ret = unlock_port_list(config->parent->global_q);
#ifdef RUNTIME_DEBUG
  aadl_runtime_debug("Timed thread wait %d\n", config->parent->period);
#endif
  RETURN_CODE_TYPE arinc653_ret = 0;
  WAIT_SEMAPHORE (config->parent->global_q->event->event,
		  config->parent->period,
		  &arinc653_ret);

  if(arinc653_ret==TIMED_OUT)
  {
    SYSTEM_TIME_TYPE now;
    GET_TIME(&now,&arinc653_ret);
    config->parent->dispatch_time = now;
  }
  
#ifdef RUNTIME_DEBUG
  aadl_runtime_debug("Timed thread released\n");
#endif
  if(!unlock_ret)
	  lock_port_list(config->parent->global_q);
#endif
  return ret;
}



void set_start_time_multiarch()
{
  RETURN_CODE_TYPE ret;
  GET_TIME((SYSTEM_TIME_TYPE*)&start_time, &ret);
}

error_code_t lock_init_barrier()
{
	error_code_t ret = RUNTIME_OK;
	RETURN_CODE_TYPE arinc653_ret = 0;
	WAIT_SEMAPHORE(init_barrier_mutex,
					  INFINITE_TIME_VALUE,
					  &arinc653_ret);
	return ret;
}

error_code_t unlock_init_barrier()
{
	error_code_t ret = RUNTIME_OK;
	RETURN_CODE_TYPE arinc653_ret = 0;
	SIGNAL_SEMAPHORE(init_barrier_mutex,
					  &arinc653_ret);
	return ret;
}

error_code_t wait_threads_init_multiarch(unsigned int threads_under_init)
{
	error_code_t ret = RUNTIME_OK;
	RETURN_CODE_TYPE arinc653_ret = 0;
	while(reached_init_barrier<threads_under_init)
	{
		SIGNAL_SEMAPHORE(init_barrier_mutex,
				  &arinc653_ret);
#ifdef RUNTIME_DEBUG
		aadl_runtime_debug("In mode manager thread, wait %d threads init, %d arrived\n", threads_under_init, reached_init_barrier);
#endif
		WAIT_SEMAPHORE (init_barrier_event,
			      INFINITE_TIME_VALUE,
	  		      &arinc653_ret);
#ifdef RUNTIME_DEBUG
		aadl_runtime_debug("Mode manager woke up, passed barrier\n");
#endif
		WAIT_SEMAPHORE(init_barrier_mutex,
							  INFINITE_TIME_VALUE,
							  &arinc653_ret);
	}
	return ret;
}

error_code_t release_task_in_mode_multiarch(thread_config_t * config)
{
	error_code_t ret = RUNTIME_OK;
#if defined RUNTIME_DEBUG
	aadl_runtime_debug("Thread %s released in current mode\n", config->name);
#endif
	RETURN_CODE_TYPE arinc653_ret = 0;
	SIGNAL_SEMAPHORE(config->wait_mode_event->event, &arinc653_ret);
	return ret;
}

error_code_t create_thread_multiarch(thread_config_t * config)
{
	error_code_t ret = RUNTIME_OK;

	PROCESS_ATTRIBUTE_TYPE tattr;
	tattr.ENTRY_POINT = config->compute_entrypoint;

	if(config->protocol==PERIODIC)
	{
		tattr.PERIOD = config->period;
		tattr.DEADLINE = config->deadline;
		// TODO: retreive WCET
		tattr.TIME_CAPACITY = config->period;
	}
	else
	{
		tattr.PERIOD = INFINITE_TIME_VALUE;
		tattr.DEADLINE = INFINITE_TIME_VALUE;
		// TODO: retreive WCET
		tattr.TIME_CAPACITY = INFINITE_TIME_VALUE;
	}
	tattr.BASE_PRIORITY = config->priority;
	strcpy(tattr.NAME, config->name);

	tattr.STACK_SIZE = 1000000;

	RETURN_CODE_TYPE arinc653_ret = 0;
	CREATE_PROCESS (&(tattr), &(config->target_thread->thread_id), &arinc653_ret);
	return ret;
}

void init_dispatch_configuration(thread_config_t * config)
{
  switch (config->protocol)
    {
    case PERIODIC:
      {
	target_periodic_thread_config_t * per_thr = config->thread;
	per_thr->iteration_counter = 0;
	break;
      }
    case HYBRID:
      {
	target_hybrid_thread_config_t * hyb_thr = config->thread;
	hyb_thr->iteration_counter = 0;
	break;
      }
    default:
      break;
    }
}

error_code_t dispatch_time_triggered_thread(target_timetriggered_thread_config_t * config)
{
  (void) config;
  return 0;
}
