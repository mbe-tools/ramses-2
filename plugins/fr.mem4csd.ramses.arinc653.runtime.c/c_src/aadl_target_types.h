/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#ifndef TARGET_TYPES_H
#define TARGET_TYPES_H

#include <arinc653/types.h>
#include <arinc653/semaphore.h>
#include <arinc653/event.h>

#if __has_include(<stdint.h>)
#include <stdint.h>
#else
#ifdef USE_POK
#include <types.h>
#endif
#endif // has stdint.h


#ifdef USE_POK

#if defined(RUNTIME_DEBUG) || defined(POK_NEEDS_USER_DEBUG)
#define USE_PRINTF
#include <libc/stdio.h>
#endif

#include "kernel/include/libc.h"

#ifdef POK_NEEDS_ARINC653_SEMAPHORE
#define RUNTIME_NEEDS_ARINC653_SEMAPHORE
#endif //POK_NEEDS_ARINC653_SEMAPHORE

#ifdef POK_NEEDS_ARINC653_QUEUEING
#define RUNTIME_NEEDS_ARINC653_QUEUEING
#endif //POK_NEEDS_ARINC653_QUEUEING

#ifdef POK_NEEDS_ARINC653_SAMPLING
#define RUNTIME_NEEDS_ARINC653_SAMPLING
#endif //POK_NEEDS_ARINC653_SAMPLING

#else

#define RUNTIME_NEEDS_ARINC653_SEMAPHORE
#define RUNTIME_NEEDS_ARINC653_QUEUEING
#define RUNTIME_NEEDS_ARINC653_SAMPLING

#endif // USE_POK

struct thread_config_t;

typedef struct target_wait_mode_t
{
  SEMAPHORE_ID_TYPE rez;
  SEMAPHORE_ID_TYPE event;
} target_wait_mode_t;

typedef struct target_wait_message_t
{
  SEMAPHORE_ID_TYPE rez;
  SEMAPHORE_ID_TYPE event;
} target_wait_message_t;

typedef struct target_lock_t
{
  SEMAPHORE_ID_TYPE rez;
} target_lock_t;

typedef struct target_thread_t
{
  PROCESS_ID_TYPE thread_id;
} target_thread_t;

typedef struct target_periodic_thread_config
{
  struct thread_config_t * parent;
  uint32_t iteration_counter;
} target_periodic_thread_config_t;

typedef struct target_sporadic_thread_config
{
  struct thread_config_t * parent;
} target_sporadic_thread_config_t;

typedef struct target_aperiodic_thread_config
{
  struct thread_config_t * parent;
} target_aperiodic_thread_config_t;

typedef struct target_timetriggered_thread_config
{
  struct thread_config_t * parent;
  SEMAPHORE_ID_TYPE rez;
  SEMAPHORE_ID_TYPE event;
} target_timetriggered_thread_config_t;

typedef struct target_hybrid_thread_config
{
  struct thread_config_t * parent;
  uint32_t iteration_counter;
} target_hybrid_thread_config_t;

typedef struct target_timed_thread_config
{
  struct thread_config_t * parent;
  SYSTEM_TIME_TYPE time_interval;
} target_timed_thread_config_t;

typedef struct target_process_port_t
{
  void * partition_port; // SAMPLING or QUEUEING
} target_process_port_t;

typedef struct target_processor_port_t
{
} target_processor_port_t;

typedef SYSTEM_TIME_TYPE aadl_time_t;

typedef struct target_watchdog_t
{
} target_watchdog_t;

#endif
