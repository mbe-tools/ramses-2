/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_multiarch.h"

#include "arinc653/sampling.h"
#include "arinc653/queueing.h"
#include "arinc653/semaphore.h"
#include "arinc653/event.h"

#include "aadl_target_types.h"
#include "main.h"

// for aadl_port
error_code_t lock_port_reference(thread_input_port_t * port)
{
  error_code_t ret = RUNTIME_OK;
#ifdef RUNTIME_DEBUG
  aadl_runtime_debug("About to lock %s\n", port->parent->parent->name);
#endif
#if defined(RUNTIME_NEEDS_ARINC653_SEMAPHORE)
  RETURN_CODE_TYPE arinc653_ret = 0;	
  WAIT_SEMAPHORE(port->parent->event->rez,
		  INFINITE_TIME_VALUE,
		  &arinc653_ret);
#endif
  // DO NOT USE printf inside critical sections: it locks
  // the std output file and may provoke deadlocks
#if defined RUNTIME_DEBUG
  if(arinc653_ret!=0)
		aadl_runtime_debug("ERROR in wait semaphore %d\n", arinc653_ret);
#endif
  return ret;
}

error_code_t unlock_port_reference(thread_input_port_t * port)
{
  error_code_t ret = RUNTIME_OK;
  RETURN_CODE_TYPE arinc653_ret = 0;	
  // DO NOT USE printf inside critical sections: it locks
  // the std output file and may provoke deadlocks
#if defined(RUNTIME_NEEDS_ARINC653_SEMAPHORE)
  SIGNAL_SEMAPHORE(port->parent->event->rez, &arinc653_ret);
  if(arinc653_ret)
	ret = RUNTIME_LOCK_ERROR;
#ifdef RUNTIME_DEBUG
  aadl_runtime_debug("Unlocked %s, returned %d\n", port->parent->parent->name, arinc653_ret);
#endif
#endif
  return ret;
}

error_code_t send_signal(thread_input_port_t * port)
{
#if defined(RUNTIME_NEEDS_ARINC653_SEMAPHORE)
  error_code_t ret = RUNTIME_OK;
  RETURN_CODE_TYPE arinc653_ret = 0;
  SIGNAL_SEMAPHORE(port->parent->event->event, &arinc653_ret);
  return ret;
#else
  (void) port;
#endif
  
}

error_code_t start_thread_multiarch(thread_config_t * config)
{
	error_code_t ret = RUNTIME_OK;
	RETURN_CODE_TYPE arinc653_ret = 0;
	PROCESS_ID_TYPE proc_id = config->target_thread->thread_id;
	switch (config->protocol)
	{
		case PERIODIC:
		{
			target_periodic_thread_config_t * per_thr = config->thread;
			per_thr->iteration_counter=0;
			if(config->offset)
			{
				SYSTEM_TIME_TYPE offset = (SYSTEM_TIME_TYPE) config->offset;
				DELAYED_START(proc_id, offset, &arinc653_ret);
			}
			else
				START(proc_id, &arinc653_ret);
			break;
		}
		default:
			START(proc_id, &arinc653_ret);
			break;
	}
	if(arinc653_ret)
		ret = RUNTIME_START_THREAD_ERROR;
	return ret;
}

error_code_t send_out_process(port_reference_t * source,
		void * value,
		unsigned int msg_size)
{
	error_code_t ret = RUNTIME_OK;
#if !defined(RUNTIME_NEEDS_ARINC653_QUEUEING) && !defined(RUNTIME_NEEDS_ARINC653_SAMPLING)
	(void) msg_size;
	(void) value;
	(void) source;
#else
	process_port_t * src_port = (process_port_t *) source->directed_port;
	RETURN_CODE_TYPE arinc653_ret = 0;
	if(source->type==DATA)
	{

#if defined(RUNTIME_NEEDS_ARINC653_SAMPLING)
	  // sampling
	  SAMPLING_PORT_ID_TYPE * sampling_port = (SAMPLING_PORT_ID_TYPE *) src_port->sys_port->partition_port;
	  WRITE_SAMPLING_MESSAGE(*sampling_port,
				 (MESSAGE_ADDR_TYPE) value,
				 (MESSAGE_SIZE_TYPE) msg_size,
				 &arinc653_ret);
#ifdef RUNTIME_DEBUG
	  aadl_runtime_debug("Value returned by WRITE_SAMPLING_MESSAGE %d\n", arinc653_ret);
#endif //DEBUG
#endif
	}
	else
	{
#if defined(RUNTIME_NEEDS_ARINC653_QUEUEING)
	  // queueing
	  QUEUING_PORT_ID_TYPE * queuing_port = (QUEUING_PORT_ID_TYPE *) src_port->sys_port->partition_port;
	  SEND_QUEUING_MESSAGE(*queuing_port,
			       (MESSAGE_ADDR_TYPE) value,
			       (MESSAGE_SIZE_TYPE) msg_size,
			       INFINITE_TIME_VALUE,
			       &arinc653_ret
			       );
#ifdef DEBUG
	  aadl_runtime_debug("Value returned by SEND_QUEUING_MESSAGE %d\n", arinc653_ret);
#endif //DEBUG
#endif // RUNTIME_NEEDS_ARINC653_QUEUING
	}
	if(arinc653_ret)
		ret = RUNTIME_FULL_QUEUE;
#endif //!defined(RUNTIME_NEEDS_ARINC653_QUEUEING) && !defined(RUNTIME_NEEDS_ARINC653_SAMPLING)
	return ret;
}


error_code_t receive_in_process(port_reference_t * destination,
		void * received_msg)
{
	error_code_t ret = RUNTIME_OK;
#if !defined(RUNTIME_NEEDS_ARINC653_SAMPLING) && !defined(RUNTIME_NEEDS_ARINC653_QUEUEING)
	(void) received_msg;
	(void) destination;
#else
	RETURN_CODE_TYPE arinc653_ret=0;
	
	if(destination->type==DATA)
	{
#if defined(RUNTIME_NEEDS_ARINC653_SAMPLING)
		// sampling
		MESSAGE_SIZE_TYPE read_data_size=0;
		VALIDITY_TYPE is_valid;
		process_port_t * dst_port = (process_port_t *) destination->directed_port;
		SAMPLING_PORT_ID_TYPE * sampling_port = (SAMPLING_PORT_ID_TYPE *) dst_port->sys_port->partition_port;
		READ_SAMPLING_MESSAGE(*sampling_port,
				(MESSAGE_ADDR_TYPE) received_msg,
				&read_data_size,
				&is_valid,
				&arinc653_ret);
#ifdef RUNTIME_DEBUG
		aadl_runtime_debug("Value returned by READ_SAMPLING_MESSAGE = %d\n", arinc653_ret);
#endif //DEBUG
#endif // RUNTIME_NEEDS_ARINC653_SAMPLING
	}
	else
	{
#if defined(RUNTIME_NEEDS_ARINC653_QUEUEING)
		// queueing
		MESSAGE_SIZE_TYPE msg_size = (MESSAGE_SIZE_TYPE) destination->msg_size;
		process_port_t * dst_port = (process_port_t *) destination->directed_port;
		QUEUING_PORT_ID_TYPE * queuing_port = (QUEUING_PORT_ID_TYPE *) dst_port->sys_port->partition_port;
		RECEIVE_QUEUING_MESSAGE(*queuing_port,
				INFINITE_TIME_VALUE,
				(MESSAGE_ADDR_TYPE) received_msg,
				&msg_size,
				&arinc653_ret
		);
#ifdef RUNTIME_DEBUG
		aadl_runtime_debug("Value returned by RECEIVE_QUEUEING_MESSAGE = %d\n", arinc653_ret);
#endif //DEBUG
		

#endif // RUNTIME_NEEDS_ARINC653_QUEUING
	}
	if(arinc653_ret!=0)
		ret = RUNTIME_EMPTY_QUEUE;
#endif // !defined(RUNTIME_NEEDS_ARINC653_SAMPLING) && !defined(RUNTIME_NEEDS_ARINC653_QUEUEING)
	return ret;
}

error_code_t lock_data(lock_access_protocol_t * protocol)
{
  error_code_t ret = RUNTIME_OK;
  RETURN_CODE_TYPE arinc653_ret;
#if defined(RUNTIME_NEEDS_ARINC653_SEMAPHORE)
  WAIT_SEMAPHORE(protocol->rez->rez,
  		 INFINITE_TIME_VALUE,
  		 &arinc653_ret);
#endif
  // DO NOT USE printf inside critical sections: it locks
  // the std output file and may provoke deadlocks
  return ret;
}

error_code_t unlock_data(lock_access_protocol_t * protocol)
{
  error_code_t ret = RUNTIME_OK;
  RETURN_CODE_TYPE arinc653_ret;
  // DO NOT USE printf inside critical sections: it locks
  // the std output file and may provoke deadlocks
#if defined(RUNTIME_NEEDS_ARINC653_SEMAPHORE)
  SIGNAL_SEMAPHORE(protocol->rez->rez, &arinc653_ret);
#endif
  return ret;
}

error_code_t icpp_lock_data(icpp_access_protocol_t * protocol)
{ 
  error_code_t ret = RUNTIME_OK;
  RETURN_CODE_TYPE arinc653_ret;
  
  PRIORITY_TYPE ceiling_priority = protocol->ceiling_priority;

  if(ceiling_priority==0)
	return -1;

  PROCESS_ID_TYPE me;
  GET_MY_ID(&me, &arinc653_ret);

  PROCESS_STATUS_TYPE status;
  GET_PROCESS_STATUS(me, &status, &arinc653_ret);

  PRIORITY_TYPE base_priority = status.ATTRIBUTES.BASE_PRIORITY;

  SET_PRIORITY(me, ceiling_priority, &arinc653_ret);
  
  if(protocol->base_priority>0)
	return -1;

  protocol->base_priority = base_priority;
  return ret;
}

error_code_t icpp_unlock_data(icpp_access_protocol_t * protocol)
{
  error_code_t ret = RUNTIME_OK;
  RETURN_CODE_TYPE arinc653_ret;

  PROCESS_ID_TYPE me;
  GET_MY_ID(&me, &arinc653_ret);
  if(ret!=0)
  	return ret;

  PRIORITY_TYPE base_priority = protocol->base_priority;
  protocol->base_priority = 0;
  SET_PRIORITY(me, base_priority, &arinc653_ret);
  return ret;
}

int init_port_list_mutex(port_list_t * port_list)
{
  // TODO: strcpy name in pok_arinc653_semaphores_names, etc...
  (void) port_list;
  return 0;
}

error_code_t lock_port_list(port_list_t * port_list)
{
  error_code_t ret = RUNTIME_OK;
  RETURN_CODE_TYPE arinc653_ret;
#if defined(RUNTIME_NEEDS_ARINC653_SEMAPHORE)
#ifdef RUNTIME_DEBUG
  aadl_runtime_debug("About to lock %s\n", port_list->parent->name);
#endif
  WAIT_SEMAPHORE(port_list->event->rez,
		 INFINITE_TIME_VALUE,
		 &arinc653_ret);
#endif
  // DO NOT USE printf inside critical sections: it locks
  // the std output file and may provoke deadlocks
  return ret;
}

error_code_t unlock_port_list(port_list_t * port_list)
{
  error_code_t ret = RUNTIME_OK;
  RETURN_CODE_TYPE arinc653_ret;
  // DO NOT USE printf inside critical sections: it locks
  // the std output file and may provoke deadlocks
#if defined(RUNTIME_NEEDS_ARINC653_SEMAPHORE)
  SIGNAL_SEMAPHORE(port_list->event->rez, &arinc653_ret);
#ifdef RUNTIME_DEBUG
  aadl_runtime_debug("Unlocked %s, returned %d\n", port_list->parent->name, ret);
#endif
#endif
  return ret;
}

error_code_t wait_port_list(port_list_t * port_list)
{
  error_code_t ret = RUNTIME_OK;
  RETURN_CODE_TYPE arinc653_ret = 0;
#if defined(RUNTIME_NEEDS_ARINC653_SEMAPHORE)
  unlock_port_list(port_list);
  WAIT_SEMAPHORE (port_list->event->event,
	  	  INFINITE_TIME_VALUE,
		  (RETURN_CODE_TYPE*) &arinc653_ret);
  lock_port_list(port_list);
#endif
  return ret;
}

