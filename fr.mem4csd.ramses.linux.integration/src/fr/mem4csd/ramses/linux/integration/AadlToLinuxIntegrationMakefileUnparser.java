package fr.mem4csd.ramses.linux.integration;

import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.ProcessImplementation;
import org.osate.aadl2.ProcessSubcomponent;

import fr.mem4csd.ramses.core.codegen.AbstractAadlToCMakefileUnparser;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;
import fr.mem4csd.ramses.linux.codegen.makefile.*;
import fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration;
import fr.mem4csd.ramses.mqtt.MQTTGeneratorUtils;
import fr.mem4csd.ramses.sockets.SocketsGeneratorUtils;

public class AadlToLinuxIntegrationMakefileUnparser extends AadlToLinuxMakefileUnparser {

	@Override
	protected Set<URI> getTargetSourceFiles(ProcessSubcomponent process)
	{
		CodegenLinuxIntegration codegenLinuxIntegration = 
				(CodegenLinuxIntegration) container;
		URI mqttDirURI = codegenLinuxIntegration.getMqttRuntimeDirectoryURI().appendSegment(AbstractAadlToCMakefileUnparser.C_BASIC_SUB_PATH);
		URI socketsDirURI = codegenLinuxIntegration.getSocketsRuntimeDirectoryURI().appendSegment(AbstractAadlToCMakefileUnparser.C_BASIC_SUB_PATH);

		boolean usesNetwork = false;
		Set<URI> result = super.getTargetSourceFiles(process);
		ProcessImplementation pi = (ProcessImplementation) process.getSubcomponentType();
		if(GeneratorUtils.processUsesMQTT(pi))
		{
			usesNetwork = true;
			MQTTGeneratorUtils.addMQTTRuntimeFiles(mqttDirURI, result, includeDirURISet, "linux");
		}
		if(GeneratorUtils.processUsesSOCKET(pi))
		{
			usesNetwork = true;
			SocketsGeneratorUtils.addSocketsRuntimeFiles(socketsDirURI, result, includeDirURISet, "linux");
		}
		if(usesNetwork)
		{
			URI networkPortURI = container.getCoreRuntimeDirectoryURI().appendSegment("aadl_ports_network").appendFileExtension("c");
			sourceFilesURISet.add(networkPortURI) ;
		}
		
		return result;
	}

}
