/**
 */
package fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration;

import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.WorkflowramseslinuxintegrationFactory
 * @model kind="package"
 * @generated
 */
public interface WorkflowramseslinuxintegrationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowramseslinuxintegration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/ramses/workflowramseslinuxintegration";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workflowramseslinuxintegration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowramseslinuxintegrationPackage eINSTANCE = fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.impl.WorkflowramseslinuxintegrationPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.impl.CodegenLinuxIntegrationImpl <em>Codegen Linux Integration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.impl.CodegenLinuxIntegrationImpl
	 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.impl.WorkflowramseslinuxintegrationPackageImpl#getCodegenLinuxIntegration()
	 * @generated
	 */
	int CODEGEN_LINUX_INTEGRATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__NAME = WorkflowramseslinuxPackage.CODEGEN_LINUX__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__DESCRIPTION = WorkflowramseslinuxPackage.CODEGEN_LINUX__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__ENABLED = WorkflowramseslinuxPackage.CODEGEN_LINUX__ENABLED;

	/**
	 * The feature id for the '<em><b>Debug Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__DEBUG_OUTPUT = WorkflowramseslinuxPackage.CODEGEN_LINUX__DEBUG_OUTPUT;

	/**
	 * The feature id for the '<em><b>Aadl Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__AADL_MODEL_SLOT = WorkflowramseslinuxPackage.CODEGEN_LINUX__AADL_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Trace Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__TRACE_MODEL_SLOT = WorkflowramseslinuxPackage.CODEGEN_LINUX__TRACE_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__OUTPUT_DIRECTORY = WorkflowramseslinuxPackage.CODEGEN_LINUX__OUTPUT_DIRECTORY;

	/**
	 * The feature id for the '<em><b>Target Install Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__TARGET_INSTALL_DIR = WorkflowramseslinuxPackage.CODEGEN_LINUX__TARGET_INSTALL_DIR;

	/**
	 * The feature id for the '<em><b>Include Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__INCLUDE_DIR = WorkflowramseslinuxPackage.CODEGEN_LINUX__INCLUDE_DIR;

	/**
	 * The feature id for the '<em><b>Core Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__CORE_RUNTIME_DIR = WorkflowramseslinuxPackage.CODEGEN_LINUX__CORE_RUNTIME_DIR;

	/**
	 * The feature id for the '<em><b>Target Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__TARGET_RUNTIME_DIR = WorkflowramseslinuxPackage.CODEGEN_LINUX__TARGET_RUNTIME_DIR;

	/**
	 * The feature id for the '<em><b>Aadl To Source Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__AADL_TO_SOURCE_CODE = WorkflowramseslinuxPackage.CODEGEN_LINUX__AADL_TO_SOURCE_CODE;

	/**
	 * The feature id for the '<em><b>Aadl To Target Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__AADL_TO_TARGET_CONFIGURATION = WorkflowramseslinuxPackage.CODEGEN_LINUX__AADL_TO_TARGET_CONFIGURATION;

	/**
	 * The feature id for the '<em><b>Aadl To Target Build</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__AADL_TO_TARGET_BUILD = WorkflowramseslinuxPackage.CODEGEN_LINUX__AADL_TO_TARGET_BUILD;

	/**
	 * The feature id for the '<em><b>Transformation Resources Pair List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__TRANSFORMATION_RESOURCES_PAIR_LIST = WorkflowramseslinuxPackage.CODEGEN_LINUX__TRANSFORMATION_RESOURCES_PAIR_LIST;

	/**
	 * The feature id for the '<em><b>Target Properties</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__TARGET_PROPERTIES = WorkflowramseslinuxPackage.CODEGEN_LINUX__TARGET_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Progress Monitor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__PROGRESS_MONITOR = WorkflowramseslinuxPackage.CODEGEN_LINUX__PROGRESS_MONITOR;

	/**
	 * The feature id for the '<em><b>Uri Converter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__URI_CONVERTER = WorkflowramseslinuxPackage.CODEGEN_LINUX__URI_CONVERTER;

	/**
	 * The feature id for the '<em><b>Target Install Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__TARGET_INSTALL_DIRECTORY_URI = WorkflowramseslinuxPackage.CODEGEN_LINUX__TARGET_INSTALL_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Output Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__OUTPUT_DIRECTORY_URI = WorkflowramseslinuxPackage.CODEGEN_LINUX__OUTPUT_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Core Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__CORE_RUNTIME_DIRECTORY_URI = WorkflowramseslinuxPackage.CODEGEN_LINUX__CORE_RUNTIME_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Target Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__TARGET_RUNTIME_DIRECTORY_URI = WorkflowramseslinuxPackage.CODEGEN_LINUX__TARGET_RUNTIME_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Include Directory URI List</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__INCLUDE_DIRECTORY_URI_LIST = WorkflowramseslinuxPackage.CODEGEN_LINUX__INCLUDE_DIRECTORY_URI_LIST;

	/**
	 * The feature id for the '<em><b>Mqtt Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__MQTT_RUNTIME_DIR = WorkflowramseslinuxPackage.CODEGEN_LINUX_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sockets Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__SOCKETS_RUNTIME_DIR = WorkflowramseslinuxPackage.CODEGEN_LINUX_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Mqtt Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI = WorkflowramseslinuxPackage.CODEGEN_LINUX_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sockets Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI = WorkflowramseslinuxPackage.CODEGEN_LINUX_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Codegen Linux Integration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION_FEATURE_COUNT = WorkflowramseslinuxPackage.CODEGEN_LINUX_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = WorkflowramseslinuxPackage.CODEGEN_LINUX___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int CODEGEN_LINUX_INTEGRATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT = WorkflowramseslinuxPackage.CODEGEN_LINUX___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = WorkflowramseslinuxPackage.CODEGEN_LINUX___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION___CHECK_CANCELED__IPROGRESSMONITOR = WorkflowramseslinuxPackage.CODEGEN_LINUX___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Get Model Transformation Traces List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION___GET_MODEL_TRANSFORMATION_TRACES_LIST = WorkflowramseslinuxPackage.CODEGEN_LINUX___GET_MODEL_TRANSFORMATION_TRACES_LIST;

	/**
	 * The operation id for the '<em>Get Target Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION___GET_TARGET_NAME = WorkflowramseslinuxPackage.CODEGEN_LINUX___GET_TARGET_NAME;

	/**
	 * The number of operations of the '<em>Codegen Linux Integration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_LINUX_INTEGRATION_OPERATION_COUNT = WorkflowramseslinuxPackage.CODEGEN_LINUX_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration <em>Codegen Linux Integration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Codegen Linux Integration</em>'.
	 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration
	 * @generated
	 */
	EClass getCodegenLinuxIntegration();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration#getMqttRuntimeDir <em>Mqtt Runtime Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mqtt Runtime Dir</em>'.
	 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration#getMqttRuntimeDir()
	 * @see #getCodegenLinuxIntegration()
	 * @generated
	 */
	EAttribute getCodegenLinuxIntegration_MqttRuntimeDir();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration#getSocketsRuntimeDir <em>Sockets Runtime Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sockets Runtime Dir</em>'.
	 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration#getSocketsRuntimeDir()
	 * @see #getCodegenLinuxIntegration()
	 * @generated
	 */
	EAttribute getCodegenLinuxIntegration_SocketsRuntimeDir();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration#getMqttRuntimeDirectoryURI <em>Mqtt Runtime Directory URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mqtt Runtime Directory URI</em>'.
	 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration#getMqttRuntimeDirectoryURI()
	 * @see #getCodegenLinuxIntegration()
	 * @generated
	 */
	EAttribute getCodegenLinuxIntegration_MqttRuntimeDirectoryURI();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration#getSocketsRuntimeDirectoryURI <em>Sockets Runtime Directory URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sockets Runtime Directory URI</em>'.
	 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration#getSocketsRuntimeDirectoryURI()
	 * @see #getCodegenLinuxIntegration()
	 * @generated
	 */
	EAttribute getCodegenLinuxIntegration_SocketsRuntimeDirectoryURI();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkflowramseslinuxintegrationFactory getWorkflowramseslinuxintegrationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.impl.CodegenLinuxIntegrationImpl <em>Codegen Linux Integration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.impl.CodegenLinuxIntegrationImpl
		 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.impl.WorkflowramseslinuxintegrationPackageImpl#getCodegenLinuxIntegration()
		 * @generated
		 */
		EClass CODEGEN_LINUX_INTEGRATION = eINSTANCE.getCodegenLinuxIntegration();

		/**
		 * The meta object literal for the '<em><b>Mqtt Runtime Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_LINUX_INTEGRATION__MQTT_RUNTIME_DIR = eINSTANCE.getCodegenLinuxIntegration_MqttRuntimeDir();

		/**
		 * The meta object literal for the '<em><b>Sockets Runtime Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_LINUX_INTEGRATION__SOCKETS_RUNTIME_DIR = eINSTANCE.getCodegenLinuxIntegration_SocketsRuntimeDir();

		/**
		 * The meta object literal for the '<em><b>Mqtt Runtime Directory URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_LINUX_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI = eINSTANCE.getCodegenLinuxIntegration_MqttRuntimeDirectoryURI();

		/**
		 * The meta object literal for the '<em><b>Sockets Runtime Directory URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_LINUX_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI = eINSTANCE.getCodegenLinuxIntegration_SocketsRuntimeDirectoryURI();

	}

} //WorkflowramseslinuxintegrationPackage
