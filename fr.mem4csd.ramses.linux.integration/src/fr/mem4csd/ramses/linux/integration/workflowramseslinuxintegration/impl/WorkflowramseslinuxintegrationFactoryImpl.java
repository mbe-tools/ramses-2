/**
 */
package fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.impl;

import fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowramseslinuxintegrationFactoryImpl extends EFactoryImpl implements WorkflowramseslinuxintegrationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WorkflowramseslinuxintegrationFactory init() {
		try {
			WorkflowramseslinuxintegrationFactory theWorkflowramseslinuxintegrationFactory = (WorkflowramseslinuxintegrationFactory)EPackage.Registry.INSTANCE.getEFactory(WorkflowramseslinuxintegrationPackage.eNS_URI);
			if (theWorkflowramseslinuxintegrationFactory != null) {
				return theWorkflowramseslinuxintegrationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WorkflowramseslinuxintegrationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramseslinuxintegrationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WorkflowramseslinuxintegrationPackage.CODEGEN_LINUX_INTEGRATION: return createCodegenLinuxIntegration();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodegenLinuxIntegration createCodegenLinuxIntegration() {
		CodegenLinuxIntegrationImpl codegenLinuxIntegration = new CodegenLinuxIntegrationImpl();
		return codegenLinuxIntegration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramseslinuxintegrationPackage getWorkflowramseslinuxintegrationPackage() {
		return (WorkflowramseslinuxintegrationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WorkflowramseslinuxintegrationPackage getPackage() {
		return WorkflowramseslinuxintegrationPackage.eINSTANCE;
	}

} //WorkflowramseslinuxintegrationFactoryImpl
