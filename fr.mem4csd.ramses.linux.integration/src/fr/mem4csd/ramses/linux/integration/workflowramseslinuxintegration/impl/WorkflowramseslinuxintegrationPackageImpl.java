/**
 */
package fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.impl;

import de.mdelab.workflow.WorkflowPackage;

import de.mdelab.workflow.components.ComponentsPackage;

import de.mdelab.workflow.helpers.HelpersPackage;

import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;

import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.CodegenLinuxIntegration;
import fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.WorkflowramseslinuxintegrationFactory;
import fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.WorkflowramseslinuxintegrationPackage;

import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowramseslinuxintegrationPackageImpl extends EPackageImpl implements WorkflowramseslinuxintegrationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codegenLinuxIntegrationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.WorkflowramseslinuxintegrationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkflowramseslinuxintegrationPackageImpl() {
		super(eNS_URI, WorkflowramseslinuxintegrationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WorkflowramseslinuxintegrationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkflowramseslinuxintegrationPackage init() {
		if (isInited) return (WorkflowramseslinuxintegrationPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowramseslinuxintegrationPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorkflowramseslinuxintegrationPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WorkflowramseslinuxintegrationPackageImpl theWorkflowramseslinuxintegrationPackage = registeredWorkflowramseslinuxintegrationPackage instanceof WorkflowramseslinuxintegrationPackageImpl ? (WorkflowramseslinuxintegrationPackageImpl)registeredWorkflowramseslinuxintegrationPackage : new WorkflowramseslinuxintegrationPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Aadl2Package.eINSTANCE.eClass();
		Arch_tracePackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();
		HelpersPackage.eINSTANCE.eClass();
		ComponentsPackage.eINSTANCE.eClass();
		WorkflowramsesPackage.eINSTANCE.eClass();
		WorkflowramseslinuxPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkflowramseslinuxintegrationPackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowramseslinuxintegrationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorkflowramseslinuxintegrationPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkflowramseslinuxintegrationPackage.eNS_URI, theWorkflowramseslinuxintegrationPackage);
		return theWorkflowramseslinuxintegrationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodegenLinuxIntegration() {
		return codegenLinuxIntegrationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenLinuxIntegration_MqttRuntimeDir() {
		return (EAttribute)codegenLinuxIntegrationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenLinuxIntegration_SocketsRuntimeDir() {
		return (EAttribute)codegenLinuxIntegrationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenLinuxIntegration_MqttRuntimeDirectoryURI() {
		return (EAttribute)codegenLinuxIntegrationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenLinuxIntegration_SocketsRuntimeDirectoryURI() {
		return (EAttribute)codegenLinuxIntegrationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramseslinuxintegrationFactory getWorkflowramseslinuxintegrationFactory() {
		return (WorkflowramseslinuxintegrationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		codegenLinuxIntegrationEClass = createEClass(CODEGEN_LINUX_INTEGRATION);
		createEAttribute(codegenLinuxIntegrationEClass, CODEGEN_LINUX_INTEGRATION__MQTT_RUNTIME_DIR);
		createEAttribute(codegenLinuxIntegrationEClass, CODEGEN_LINUX_INTEGRATION__SOCKETS_RUNTIME_DIR);
		createEAttribute(codegenLinuxIntegrationEClass, CODEGEN_LINUX_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI);
		createEAttribute(codegenLinuxIntegrationEClass, CODEGEN_LINUX_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		WorkflowramseslinuxPackage theWorkflowramseslinuxPackage = (WorkflowramseslinuxPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowramseslinuxPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		HelpersPackage theHelpersPackage = (HelpersPackage)EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		codegenLinuxIntegrationEClass.getESuperTypes().add(theWorkflowramseslinuxPackage.getCodegenLinux());

		// Initialize classes, features, and operations; add parameters
		initEClass(codegenLinuxIntegrationEClass, CodegenLinuxIntegration.class, "CodegenLinuxIntegration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCodegenLinuxIntegration_MqttRuntimeDir(), theEcorePackage.getEString(), "mqttRuntimeDir", null, 1, 1, CodegenLinuxIntegration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegenLinuxIntegration_SocketsRuntimeDir(), theEcorePackage.getEString(), "socketsRuntimeDir", null, 1, 1, CodegenLinuxIntegration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegenLinuxIntegration_MqttRuntimeDirectoryURI(), theHelpersPackage.getURI(), "mqttRuntimeDirectoryURI", null, 0, 1, CodegenLinuxIntegration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegenLinuxIntegration_SocketsRuntimeDirectoryURI(), theHelpersPackage.getURI(), "socketsRuntimeDirectoryURI", null, 0, 1, CodegenLinuxIntegration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //WorkflowramseslinuxintegrationPackageImpl
