/**
 */
package fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.WorkflowramseslinuxintegrationPackage
 * @generated
 */
public interface WorkflowramseslinuxintegrationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowramseslinuxintegrationFactory eINSTANCE = fr.mem4csd.ramses.linux.integration.workflowramseslinuxintegration.impl.WorkflowramseslinuxintegrationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Codegen Linux Integration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Codegen Linux Integration</em>'.
	 * @generated
	 */
	CodegenLinuxIntegration createCodegenLinuxIntegration();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorkflowramseslinuxintegrationPackage getWorkflowramseslinuxintegrationPackage();

} //WorkflowramseslinuxintegrationFactory
