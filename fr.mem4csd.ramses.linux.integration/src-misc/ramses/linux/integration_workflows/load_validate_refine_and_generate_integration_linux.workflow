<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_PAkpAMixEei-D6bRtwoJ1Q" name="workflow" description="EMFTVM operations needed by posix">
  <components xsi:type="workflow.components:ModelReader" xmi:id="_hhOSoBTDEeuUH7owlkar_Q" name="${NameInputResourcesReader}" modelSlot="sourceAadlInstance" modelURI="${instance_model_file}" modelElementIndex="0"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_co-wcAcREeuDOaqjzi9xMA" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/templates/load_and_validate.workflow">
    <propertyValues xmi:id="_oiK6EAcTEeuDOaqjzi9xMA" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_zDP2sAcUEeuDOaqjzi9xMA" name="validation_workflow" defaultValue="${validation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_8aCm8AcUEeuDOaqjzi9xMA" name="target" defaultValue="${target}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_zxZV0D4tEeuDIczWnIkZCw" name="load_aadl_resources_workflow" defaultValue="${load_aadl_resources_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_LRJTMAcXEeuDOaqjzi9xMA" name="load_transformation_resources_workflow" defaultValue="${load_transformation_resources_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_1tA5sAiSEeuaC9UGkI9whg" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_JqsS0EXNEeuxCP0Ue5LH_Q" name="conditionalExecution" conditionSlot="violates${target}Constraints">
    <onTrue xmi:id="_KcrJYEXNEeuxCP0Ue5LH_Q" name="onTrue, error dected: workflow stops"/>
    <onFalse xmi:id="_MAklEEXNEeuxCP0Ue5LH_Q" name="onFalse, no error: workflow continues">
      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_tKtt8JtkEeqJ3cxvaDjAOg" name="posix_validation" workflowURI="refine_remotes_then_locals_and_generate_integration_linux.workflow">
        <propertyValues xmi:id="_OQ0_cP5bEeqjUK5cPU4ZFw" name="output_dir" defaultValue="${output_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_XTQpQP5bEeqjUK5cPU4ZFw" name="runtime_scheme" defaultValue="${runtime_scheme}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_-tatkAhuEeuaPYhX2kwqog" name="source_file_name" defaultValue="${source_file_name}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_CF_L4AhvEeuaPYhX2kwqog" name="include_dir" defaultValue="${include_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_xoH5AAhzEeuaPYhX2kwqog" name="locals_refinement_workflow" defaultValue="${locals_refinement_workflow}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_mjdJgAimEeulUut1LEEszg" name="code_generation_workflow" defaultValue="${code_generation_workflow}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_Q5CDcAixEeulUut1LEEszg" name="target_install_dir" defaultValue="${target_install_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
      </components>
    </onFalse>
  </components>
  <properties xmi:id="_RAT4AKDmEeqR3ds1UXwt-g" name="system_implementation_name" defaultValue="${system_implementation_name}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_iLkBkO3eEeqXMuraMJ1XYQ" name="remote_communications_file" defaultValue="${remote_communications_file}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_62lFoO3lEeqXMuraMJ1XYQ" name="local_communications_file" defaultValue="${local_communications_file}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_jAEpMKDmEeqR3ds1UXwt-g" name="refined_trace_file" defaultValue="${remote_communications_file}.arch_trace">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_mcjXIKDmEeqR3ds1UXwt-g" name="validation_report_file" defaultValue="${remote_communications_file}.validation_report">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_2FwDgKE8EeqgtNEZpZXkFQ" name="refined_aadl_file" defaultValue="${remote_communications_file}.aadl">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_vyLEAAhzEeuaPYhX2kwqog" name="locals_refinement_workflow" defaultValue="${locals_refinement_workflow}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_YZkB4KEEEeqR3ds1UXwt-g" name="include_dir" description="${description_include_dir}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_5AsSoKDkEeqR3ds1UXwt-g" fileURI="default_integration_linux.properties"/>
  <propertiesFiles xmi:id="_DSP-wOhyEeqy-t86Uy9Gdw" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
