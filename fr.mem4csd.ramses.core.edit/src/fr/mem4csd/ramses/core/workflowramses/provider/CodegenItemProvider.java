/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.provider;


import de.mdelab.workflow.components.provider.WorkflowComponentItemProvider;
import fr.mem4csd.ramses.core.workflowramses.Codegen;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.mem4csd.ramses.core.workflowramses.Codegen} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CodegenItemProvider extends WorkflowComponentItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodegenItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDebugOutputPropertyDescriptor(object);
			addAadlModelSlotPropertyDescriptor(object);
			addTraceModelSlotPropertyDescriptor(object);
			addOutputDirectoryPropertyDescriptor(object);
			addTargetInstallDirPropertyDescriptor(object);
			addIncludeDirPropertyDescriptor(object);
			addCoreRuntimeDirPropertyDescriptor(object);
			addTargetRuntimeDirPropertyDescriptor(object);
			addTransformationResourcesPairListPropertyDescriptor(object);
			addTargetPropertiesPropertyDescriptor(object);
			addProgressMonitorPropertyDescriptor(object);
			addUriConverterPropertyDescriptor(object);
			addTargetInstallDirectoryURIPropertyDescriptor(object);
			addOutputDirectoryURIPropertyDescriptor(object);
			addCoreRuntimeDirectoryURIPropertyDescriptor(object);
			addTargetRuntimeDirectoryURIPropertyDescriptor(object);
			addIncludeDirectoryURIListPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Debug Output feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDebugOutputPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_debugOutput_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_debugOutput_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__DEBUG_OUTPUT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Aadl Model Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAadlModelSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_aadlModelSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_aadlModelSlot_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__AADL_MODEL_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Trace Model Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTraceModelSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_traceModelSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_traceModelSlot_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__TRACE_MODEL_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Output Directory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOutputDirectoryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_outputDirectory_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_outputDirectory_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__OUTPUT_DIRECTORY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Install Dir feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetInstallDirPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_targetInstallDir_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_targetInstallDir_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__TARGET_INSTALL_DIR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Include Dir feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIncludeDirPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_includeDir_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_includeDir_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__INCLUDE_DIR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Core Runtime Dir feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCoreRuntimeDirPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_coreRuntimeDir_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_coreRuntimeDir_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__CORE_RUNTIME_DIR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Runtime Dir feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetRuntimeDirPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_targetRuntimeDir_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_targetRuntimeDir_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__TARGET_RUNTIME_DIR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Transformation Resources Pair List feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransformationResourcesPairListPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_transformationResourcesPairList_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_transformationResourcesPairList_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__TRANSFORMATION_RESOURCES_PAIR_LIST,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Properties feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetPropertiesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_targetProperties_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_targetProperties_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__TARGET_PROPERTIES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Progress Monitor feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProgressMonitorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_progressMonitor_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_progressMonitor_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__PROGRESS_MONITOR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Uri Converter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUriConverterPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_uriConverter_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_uriConverter_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__URI_CONVERTER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Install Directory URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetInstallDirectoryURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_targetInstallDirectoryURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_targetInstallDirectoryURI_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__TARGET_INSTALL_DIRECTORY_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Output Directory URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOutputDirectoryURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_outputDirectoryURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_outputDirectoryURI_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__OUTPUT_DIRECTORY_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Core Runtime Directory URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCoreRuntimeDirectoryURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_coreRuntimeDirectoryURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_coreRuntimeDirectoryURI_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__CORE_RUNTIME_DIRECTORY_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Runtime Directory URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetRuntimeDirectoryURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_targetRuntimeDirectoryURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_targetRuntimeDirectoryURI_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__TARGET_RUNTIME_DIRECTORY_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Include Directory URI List feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIncludeDirectoryURIListPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Codegen_includeDirectoryURIList_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Codegen_includeDirectoryURIList_feature", "_UI_Codegen_type"),
				 WorkflowramsesPackage.Literals.CODEGEN__INCLUDE_DIRECTORY_URI_LIST,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(WorkflowramsesPackage.Literals.CODEGEN__AADL_TO_SOURCE_CODE);
			childrenFeatures.add(WorkflowramsesPackage.Literals.CODEGEN__AADL_TO_TARGET_CONFIGURATION);
			childrenFeatures.add(WorkflowramsesPackage.Literals.CODEGEN__AADL_TO_TARGET_BUILD);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Codegen.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Codegen"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		return "Code Generation for " + ((Codegen)object).getName();
//		String label = ((Codegen)object).getName();
//		return label == null || label.length() == 0 ?
//			getString("_UI_Codegen_type") :
//			getString("_UI_Codegen_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Codegen.class)) {
			case WorkflowramsesPackage.CODEGEN__DEBUG_OUTPUT:
			case WorkflowramsesPackage.CODEGEN__AADL_MODEL_SLOT:
			case WorkflowramsesPackage.CODEGEN__TRACE_MODEL_SLOT:
			case WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY:
			case WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIR:
			case WorkflowramsesPackage.CODEGEN__INCLUDE_DIR:
			case WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIR:
			case WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIR:
			case WorkflowramsesPackage.CODEGEN__PROGRESS_MONITOR:
			case WorkflowramsesPackage.CODEGEN__URI_CONVERTER:
			case WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIRECTORY_URI:
			case WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY_URI:
			case WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIRECTORY_URI:
			case WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIRECTORY_URI:
			case WorkflowramsesPackage.CODEGEN__INCLUDE_DIRECTORY_URI_LIST:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case WorkflowramsesPackage.CODEGEN__AADL_TO_SOURCE_CODE:
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_CONFIGURATION:
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}
