/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.helpers.provider;


import fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper;
import fr.mem4csd.ramses.core.helpers.HelpersPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CommunicationDimensioningHelperItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationDimensioningHelperItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addReaderReceivingTaskInstancePropertyDescriptor(object);
			addWriterInstancesPropertyDescriptor(object);
			addWriterFeatureInstancesPropertyDescriptor(object);
			addCprSizePropertyDescriptor(object);
			addCdwSizePropertyDescriptor(object);
			addCurrentPeriodReadPropertyDescriptor(object);
			addCurrentDeadlineWriteMapPropertyDescriptor(object);
			addBufferSizePropertyDescriptor(object);
			addHyperperiodPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Reader Receiving Task Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReaderReceivingTaskInstancePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CommunicationDimensioningHelper_readerReceivingTaskInstance_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CommunicationDimensioningHelper_readerReceivingTaskInstance_feature", "_UI_CommunicationDimensioningHelper_type"),
				 HelpersPackage.Literals.COMMUNICATION_DIMENSIONING_HELPER__READER_RECEIVING_TASK_INSTANCE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Writer Instances feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWriterInstancesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CommunicationDimensioningHelper_writerInstances_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CommunicationDimensioningHelper_writerInstances_feature", "_UI_CommunicationDimensioningHelper_type"),
				 HelpersPackage.Literals.COMMUNICATION_DIMENSIONING_HELPER__WRITER_INSTANCES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Writer Feature Instances feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWriterFeatureInstancesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CommunicationDimensioningHelper_writerFeatureInstances_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CommunicationDimensioningHelper_writerFeatureInstances_feature", "_UI_CommunicationDimensioningHelper_type"),
				 HelpersPackage.Literals.COMMUNICATION_DIMENSIONING_HELPER__WRITER_FEATURE_INSTANCES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Cpr Size feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCprSizePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CommunicationDimensioningHelper_cprSize_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CommunicationDimensioningHelper_cprSize_feature", "_UI_CommunicationDimensioningHelper_type"),
				 HelpersPackage.Literals.COMMUNICATION_DIMENSIONING_HELPER__CPR_SIZE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Cdw Size feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCdwSizePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CommunicationDimensioningHelper_cdwSize_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CommunicationDimensioningHelper_cdwSize_feature", "_UI_CommunicationDimensioningHelper_type"),
				 HelpersPackage.Literals.COMMUNICATION_DIMENSIONING_HELPER__CDW_SIZE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Current Period Read feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCurrentPeriodReadPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CommunicationDimensioningHelper_currentPeriodRead_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CommunicationDimensioningHelper_currentPeriodRead_feature", "_UI_CommunicationDimensioningHelper_type"),
				 HelpersPackage.Literals.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_PERIOD_READ,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Current Deadline Write Map feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCurrentDeadlineWriteMapPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CommunicationDimensioningHelper_currentDeadlineWriteMap_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CommunicationDimensioningHelper_currentDeadlineWriteMap_feature", "_UI_CommunicationDimensioningHelper_type"),
				 HelpersPackage.Literals.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_DEADLINE_WRITE_MAP,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Buffer Size feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBufferSizePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CommunicationDimensioningHelper_bufferSize_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CommunicationDimensioningHelper_bufferSize_feature", "_UI_CommunicationDimensioningHelper_type"),
				 HelpersPackage.Literals.COMMUNICATION_DIMENSIONING_HELPER__BUFFER_SIZE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Hyperperiod feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHyperperiodPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CommunicationDimensioningHelper_hyperperiod_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CommunicationDimensioningHelper_hyperperiod_feature", "_UI_CommunicationDimensioningHelper_type"),
				 HelpersPackage.Literals.COMMUNICATION_DIMENSIONING_HELPER__HYPERPERIOD,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns CommunicationDimensioningHelper.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/CommunicationDimensioningHelper"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		CommunicationDimensioningHelper communicationDimensioningHelper = (CommunicationDimensioningHelper)object;
		return getString("_UI_CommunicationDimensioningHelper_type") + " " + communicationDimensioningHelper.getCprSize();
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CommunicationDimensioningHelper.class)) {
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CPR_SIZE:
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CDW_SIZE:
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_PERIOD_READ:
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_DEADLINE_WRITE_MAP:
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__BUFFER_SIZE:
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__HYPERPERIOD:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return HelpersEditPlugin.INSTANCE;
	}

}
