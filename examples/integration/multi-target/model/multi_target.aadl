--
-- AADL-RAMSES
-- 
-- Copyright ¬© 2012 TELECOM ParisTech and CNRS
-- 
-- TELECOM ParisTech/LTCI
-- 
-- Authors: see AUTHORS
-- 
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program.  If not, see 
-- http://www.eclipse.org/org/documents/epl-v10.php
--

package sampled_communications
public
with Data_Model,RAMSES_Properties,POK, ARINC653;

  system main
  end main;

  system implementation main.impl
  subcomponents
    the_proc_posix: process proc.impl;
    the_proc_pok: process proc.impl;
    the_cpu_posix: processor cpu.impl {RAMSES_Properties::Target=>"linux";};
    the_cpu_pok: processor cpu.pok {RAMSES_Properties::Target=>"pok";};
    the_mem: memory mem.impl;
  properties
    actual_memory_binding => (reference (the_mem)) applies to the_proc_posix,the_proc_pok;
    actual_processor_binding => (reference (the_cpu_posix)) applies to the_proc_posix;
    actual_processor_binding => (reference (the_cpu_pok)) applies to the_proc_pok;
  end main.impl;

  processor cpu
  end cpu;

  processor implementation cpu.impl
  properties
      Scheduling_Protocol => (RMS) ;      
  end cpu.impl;

  processor implementation cpu.pok
    subcomponents
      the_part:  virtual processor pok_part;
  	properties
	  Scheduling_Protocol => (arinc653) ;  
  	  POK::Architecture => x86;
      POK::BSP => x86_qemu;
      ARINC653::Module_Major_Frame => 2000 ms;
      ARINC653::Module_Schedule => (
       	[Partition => reference(the_part); Duration => 2000ms; Periodic_Processing_Start => true;]
      );
  end cpu.pok;
  
  virtual processor pok_part
    properties
      ARINC653::Partition_Identifier => 1;
      POK::Additional_Features => (libc_stdio, console);
      Scheduling_Protocol => (RMS);
      Period => 1000 Ms;
      ARINC653::HM_Error_ID_Actions => (
    	[ErrorIdentifier => 3; Description => "Restart process"; Action => "Cold_Restart";],
    	[ErrorIdentifier => 4; Description => "Warm partition restart"; Action => "Warm_Restart";]
      );
  end pok_part;
  

  process proc
  end proc;

  process implementation proc.impl
  subcomponents
    the_sender: thread sender.impl;
    the_receiver: thread receiver.impl;
  connections
    cnx: port the_sender.p -> the_receiver.p;
  end proc.impl;

  memory mem
  end mem;

  memory implementation mem.impl
  properties
    Memory_Size => 200000 Bytes;
  end mem.impl;

  thread sender
  features
    p: out data port Integer;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 2000 Ms;
    Priority => 5;
    Data_Size => 40000 bytes;
    Stack_Size => 512 bytes;
    Code_Size => 40 bytes;
  end sender;

  thread implementation sender.impl
  calls
    call : { c : subprogram sender_spg;};
  connections
    cnx: parameter c.result -> p;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end sender.impl;

  subprogram sender_spg
  features
    result : out parameter Integer;
  properties
    source_name => "send";
    source_language => (C);
    source_text => ("../src/user_code.h","../src/user_code.c");
  end sender_spg;

  thread receiver
  features
    p: in data port Integer;
  properties
    Dispatch_Protocol => Periodic;
    Compute_Execution_Time => 0 ms .. 1 ms;
    Period => 1000 Ms;
    Priority => 10;
    Data_Size => 40000 bytes;
    Stack_Size => 512 bytes;
    Code_Size => 40 bytes;
  end receiver;

  thread implementation receiver.impl
  calls
    call : { c : subprogram receiver_spg;};
  connections
    cnx: parameter p -> c.input;
  properties
    Compute_Entrypoint_Call_Sequence => reference (call);
  end receiver.impl;

  subprogram receiver_spg
  features
    input : in parameter Integer;
  properties
    source_name => "receive";
    source_language => (C);
    source_text => ("../src/user_code.h","../src/user_code.c");
  end receiver_spg;

  data Integer
  properties
    Data_Model::Data_Representation => integer;
  end Integer;

end sampled_communications;