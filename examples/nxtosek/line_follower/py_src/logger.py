import serial
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np


NXT = serial.Serial('/dev/rfcomm0')
log_separator = ';'
field_separator = ','
new_line = '\n'
log_file_name = 'RobotLogFile.csv'  # type: str
log_file = open(log_file_name, "w")
measured_error_figure = 'Measured_Error.png'
motor_pwm_figure = 'PID_Output.png'
log_counter = 0

timestamp = []
error = []
motor_adjustment = []
incomplete_log_read = ""
log_incomplete = False
try:
    while 1:
        n = NXT.inWaiting()
        if n != 0:
            robot_log = NXT.read(n)
            robot_log = robot_log[::-1]
            logs_received = robot_log.decode("utf-8")
            if log_incomplete:
                logs_received = logs_received + incomplete_log_read
            log_incomplete = logs_received[:1] != log_separator
            logs_list = logs_received.split(log_separator)
            if log_incomplete:
                incomplete_log_read = logs_list[0]
                logs_list.pop(0)
            for log in reversed(logs_list):
                if log != '':
                    log_counter += 1
                    if log_counter > 2:
                        log_file.write(log + new_line)
                        print(log)

except (KeyboardInterrupt, IOError):
    log_file.close()
    motor_adjustment, error, timestamp = np.loadtxt(log_file_name, delimiter=',', unpack=True)
    if timestamp.size > 0 and error.size > 0 and motor_adjustment.size > 0:
        figure = plt.figure()
        plt.plot(timestamp, error, label='Measured Error')
        plt.xlabel('Time')
        plt.ylabel('Error')
        plt.title('Measured error')
        plt.legend()
        figure.savefig(measured_error_figure)
        figure = plt.figure()
        plt.plot(timestamp, motor_adjustment, label='Motor PWM adjustment')
        plt.xlabel('Time')
        plt.ylabel('PID Output')
        plt.title('PID Output')
        plt.legend()
        figure.savefig(motor_pwm_figure)
NXT.close()
