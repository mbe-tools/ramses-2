#ifndef DATA_TYPES_H
#define DATA_TYPES_H


#include "nxt_motors.h" 
#include "ecrobot_interface.h"

typedef enum Robot_state
{
	FORWARD = 0,
	STOP = 1
} Robot_state;

typedef struct Log {
	U32 system_ticks;
	int error;
	int motor_adjustment;
}Log_t;

#endif
