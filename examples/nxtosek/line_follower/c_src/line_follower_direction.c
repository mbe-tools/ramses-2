#include "line_follower_direction.h"

Robot_state state=FORWARD;

// store the robot state (FORWARD or STOP) into a global variable
void setRobotState(Robot_state *s)
{
	state = *s;
}
