#include "logging.h"
#include "aadl_multiarch.h" // for itoa and strcat functions
#include "ecrobot_interface.h"

void logData(__logData_context * ctx)
{
	char sent_log[256]="";
	char temp_buff[64];
	Log_t log;
	int ret;
	ret = Get_Value(ctx->angle, &log);
	while(!ret){
		itoa(log.system_ticks, temp_buff, BASE);
		strcat(sent_log, temp_buff);
		strcat(sent_log, ",");
		itoa(log.error, temp_buff, BASE);
		strcat(sent_log, temp_buff);
		strcat(sent_log, ",");
		itoa(log.motor_adjustment, temp_buff, BASE);
		strcat(sent_log, temp_buff);
		strcat(sent_log, ";");
		ret = Next_Value(ctx->angle);
		if(!ret)
		  ret = Get_Value(ctx->angle, &log);
	}
	ecrobot_send_bt_packet(sent_log, strlen(sent_log) + 1);
}
