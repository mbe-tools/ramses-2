#include "obstacle_detection.h"

// set state (FORWARD/STOP) according to the measured distance
void selectState(int in_distance, Robot_state *state)
{
	if(in_distance > MAX_DISTANCE)
          *state = FORWARD;
	else
          *state = STOP;
}
