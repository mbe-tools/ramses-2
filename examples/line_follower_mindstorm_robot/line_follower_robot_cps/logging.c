#include "logging.h"
#include "aadl_multiarch.h" // for itoa and strcat functions


#define ANGLE_LOG_SIZE 10 // size depends on the ratio
					      // of periods of control thread
					      // and logging thread

int angle_array[ANGLE_LOG_SIZE];

int iter = 0;
void logData(__log_context * ctx)
{
  char prefix[15] = "Ang=";
  char txt[80];

  int i, angle;
  int ret = Get_Value(ctx->angle, &angle);
  display_goto_xy(0, 6);
  if(!ret) {
    angle_array[0]=angle;
    itoa(angle, txt, 10);
    strcat(prefix,txt);
  }
  for(i=1;i<ANGLE_LOG_SIZE;i++) {
    ret = Next_Value(ctx->angle, &angle);
    if(!ret) {
      angle_array[i] = angle;
      itoa(angle, txt, 10);
      if(strlen(prefix)+strlen(txt)<15)
	strcat(prefix,txt);
    }
  }
  
  
  display_goto_xy(0, 7);
  display_string(prefix);
  display_update();
}
