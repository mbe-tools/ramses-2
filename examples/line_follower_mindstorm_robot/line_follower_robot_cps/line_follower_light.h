#ifndef LINE_FOLLOWER_H
#define LINE_FOLLOWER_H

#include "data_types.h"

/* light sensor characteristics */
#define MIN_LUMINOSITY			0
#define MAX_LUMINOSITY			100
#define	MIN_LUMINOSITY_TRESHOLD	45
#define MAX_LUMINOSITY_TRESHOLD	55

/* motor characteristics */
#define MAX_SPEED				100
/* wheels characteristics, in millimeters */
#define PERIMETER				200

void computePID(int in_light, int *out_angle);
void computeSpeed(int in_angle, int *out_speedLeft, int * out_speedRight, Robot_state * state);

#endif

