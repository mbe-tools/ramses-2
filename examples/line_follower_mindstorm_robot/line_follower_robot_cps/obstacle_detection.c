#include "obstacle_detection.h"

void selectState(int in_distance, Robot_state *state)
{
	if(in_distance < MIN_DISTANCE)
          *state = BACKWARD;
	else if(in_distance > MAX_DISTANCE)
          *state = FORWARD;
	else
          *state = STOP;
}

#define SONAR_SPEED 65
#define ROTATION 60
char direction=1;

void getSonarMove(int count, int* speed, int* brake)
{
  if(direction==1)
  {
    if(count<ROTATION)
    {
      *brake=0;
      *speed = SONAR_SPEED;
    }
    else
    {
      *speed=0;
      *brake=1;
      direction = -1;
    }
  }
  else 
  {
    if(count > -ROTATION)
    {
	*brake=0;
        *speed=-SONAR_SPEED;
    }
    else
    {
	*speed = 0;
	*brake=1;
	direction = 1;
    }
  }
}

/*********************/
/* Ungenerated datas */
/*********************/

#include "kernel.h"

void ecrobot_device_initialize(void)
{
	ecrobot_init_sonar_sensor(NXT_PORT_S1);
}

void ecrobot_device_terminate(void)
{
}

/*************************/
/* End Ungenerated datas */
/*************************/
