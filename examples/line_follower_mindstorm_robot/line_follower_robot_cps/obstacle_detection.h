#ifndef OBSTACLE_DETECTION_H
#define OBSTACLE_DETECTION_H

#include "data_types.h"

/* treshold distances, in centimeters */
#define MIN_DISTANCE			10
#define MAX_DISTANCE			20

void selectState(int in_distance, Robot_state *state);
void getSonarMove(int count, int* speed, int* brake);

#endif

