#ifndef DATA_TYPES_H
#define DATA_TYPES_H


#include "nxt_motors.h" 
#include "ecrobot_interface.h"

typedef enum Robot_state
{
	FORWARD = 0,
	BACKWARD = 1,
	STOP = 2
} Robot_state;

#endif
