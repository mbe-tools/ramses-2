package Line_Follower_Robot_Cps
public
	with Line_Follower_Software, Robots_Library, Warehouse, Physics, Physics_Properties;

	system Line_Follower_Robot_Cps
		features
			light_sensor_in: in feature Physics::Light;
			sonar_in: in feature Physics::Sound;
			sonar_out: out feature Physics::Sound;
			force_left_wheel: out feature Physics::Force;
			force_right_wheel: out feature Physics::Force;
			force_gripper: out feature Physics::Force;
	end Line_Follower_Robot_Cps;

	system implementation Line_Follower_Robot_Cps.nxt
		subcomponents
			app: system Line_Follower_Software::Carry_Object_App.basic;
			robot: system Line_Follower_Robot.nxt;
		connections
			force_left_wheel_conn: feature robot.force_left_wheel -> force_left_wheel;
			force_right_wheel_conn: feature robot.force_right_wheel -> force_right_wheel;
			light_sensor_conn: feature light_sensor_in -> robot.light_sensor_in;
			force_gripper_conn: feature robot.force_gripper -> force_gripper;
			sonar_in_conn: feature sonar_in -> robot.sonar_in;
			sonar_out_conn: feature robot.sonar_out -> sonar_out;
		properties
			Actual_Processor_Binding => (reference (robot.brick.main_processor)) applies to app.carry_object_process;
			Actual_Memory_Binding => (reference (robot.brick.main_processor.ram)) applies to app.carry_object_process;
			Actual_Processor_Binding => (reference (robot.brick.input_circuit)) applies to app.light_sensor_driver;
			Actual_Connection_Binding => (reference (robot.brick.data_bus)) applies to app.light_sensor_process_conn;
			Actual_Processor_Binding => (reference (robot.brick.output_circuit)) applies to app.left_wheel_driver;
			Actual_Connection_Binding => (reference (robot.brick.data_bus)) applies to app.process_left_wheel_conn;
			Actual_Processor_Binding => (reference (robot.brick.output_circuit)) applies to app.right_wheel_driver;
			Actual_Connection_Binding => (reference (robot.brick.data_bus)) applies to app.process_right_wheel_conn;
	end Line_Follower_Robot_Cps.nxt;

	abstract Warehouse_Robots extends Warehouse::Warehouse
		properties
			Physics_Properties::Illuminance => 150.0 lx applies to light_source; 
	end Warehouse_Robots;

	abstract implementation Warehouse_Robots.normal extends Warehouse::Warehouse.basic
		subcomponents
			line: abstract Line;
			line_follower_robot: system Line_Follower_Robot_Cps;
			obstacle: abstract Physics::Reflecting_Object;
		connections
			floor_robot_light_sensor_in: feature floor.light_reflected -> line_follower_robot.light_sensor_in;
			line_robot_light_sensor_in: feature line.light_reflected -> line_follower_robot.light_sensor_in;
			obstacle_robot_sonar_in: feature obstacle.sound_reflected -> line_follower_robot.sonar_in;
			robot_sonar_out_obstacle: feature line_follower_robot.sonar_out -> obstacle.sound_in;
			force_left_wheel_floor: feature line_follower_robot.force_left_wheel -> floor.applied_force;
			force_right_wheel_floor: feature line_follower_robot.force_right_wheel -> floor.applied_force;
			light_source_line: feature light_source -> line.light_in;
			light_source_floor: feature light_source -> floor.light_in;
			Warehouse_Robots_normal_new_connection: feature light_source -> obstacle.light_in;
		properties
			Physics_Properties::Curvature_Radius => 99.0 mm applies to line;
	end Warehouse_Robots.normal;

	abstract Line extends Physics::Reflecting_Object
	end Line;

	system Line_Follower_Robot extends Robots_Library::Robot
		features
			light_sensor_in: in feature Physics::Light;
			sonar_in: in feature Physics::Sound;
			sonar_out: out feature Physics::Sound;
			force_left_wheel: out feature Physics::Force;
			force_right_wheel: out feature Physics::Force;
			force_gripper: out feature Physics::Force;
	end Line_Follower_Robot;

	system implementation Line_Follower_Robot.nxt extends Robots_Library::Robot.basic
		subcomponents
			left_wheel_assembly: device Robots_Library::Wheel_Assembly;
			left_wheel_cable: bus Robots_Library::Actuator_Bus;
			right_wheel_assembly: device Robots_Library::Wheel_Assembly;
			right_wheel_cable: bus Robots_Library::Actuator_Bus;
			light_sensor: device Robots_Library::Light_Sensor;
			light_sensor_cable: bus Robots_Library::Sensor_Bus;
			sonar: device Robots_Library::Sonar;
			sonar_cable: bus Robots_Library::Sensor_Bus;
			gripper: device Robots_Library::Gripper;
			gripper_cable: bus Robots_Library::Actuator_Bus;
			brick: refined to system Robots_Library::NXT_Brick.basic;
		connections
			brick_left_wheel_cable: bus access brick.out_1 -> left_wheel_cable;
			left_wheel_cable_left_wheel_assembly: bus access left_wheel_cable -> left_wheel_assembly.power_in;
			left_wheel_force_robot: feature left_wheel_assembly.force -> force_left_wheel;
			brick_right_wheel_cable: bus access brick.out_2 -> right_wheel_cable;
			right_wheel_cable_right_wheel_assembly: bus access right_wheel_cable -> right_wheel_assembly.power_in;
			right_wheel_force_robot: feature right_wheel_assembly.force -> force_right_wheel;
			brick_gripper_cable: bus access brick.out_3 -> gripper_cable;
			gripper_cable_gripper: bus access gripper_cable -> gripper.power_in;
			gripper_force_robot: feature gripper.force -> force_gripper;
			robot_light_sensor: feature light_sensor_in -> light_sensor.light_in;
			light_sensor_light_sensor_cable: bus access light_sensor.light_data -> light_sensor_cable;
			light_sensor_cable_brick: bus access light_sensor_cable -> brick.in_1;
			sonar_out_robot: feature sonar.sound_out -> sonar_out;
			robot_sonar_in: feature sonar_in -> sonar.sound_in;
			sonar_sonar_cable: bus access sonar.sound_data -> sonar_cable;
			sonar_cable_brick: bus access sonar_cable -> brick.in_2;
	end Line_Follower_Robot.nxt;
	
end Line_Follower_Robot_Cps;