#include "line_follower_light.h"
#include <math.h>
#include "aadl_multiarch.h"

/**
 * Computes the angle the robot has to turn in order to follow the road.
 */

// check if the behavior is sensitive to sensors and factors (KP, KI, error) values

#define KP 62.0 //125
#define KI 4.3//4.8
#define KD 300.0//750
#define NXT_OSEK_LIGHT_MAX 1023.0
// this value should be parameter to the compute PID function?
#define OFFSET 56.0




float integral = 0.0;
float lastError = 0.0;
float derivative = 0.0;
float error = 0.0;


/**
 * Computes the error given the lightsensor read and sets the PID computed value to the variable out_angle that holds the necessary speed adjustment.
 * Initially the sensor read is rescaled to a value between 0 and 100.
 * @param currentLight- the light sensor read
 * @param out_angle - pointer to the variable that holds the necessary PID computed adjustement
 */

void computePID(int currentLight, int *out_angle)
{
	//Rescaled light values to 0-100
	int rescaledLight = ((float)100)/(NXT_OSEK_LIGHT_MAX)*(currentLight - NXT_OSEK_LIGHT_MAX) + 100;

	error = rescaledLight - OFFSET;
	integral = integral + error;
	derivative = (float)error - lastError;
	*out_angle = (float)(KP*error + KI*integral + KD*derivative) / 100;
	lastError = error;
	//ecrobot_debug1(currentLight, (int)(error), *out_angle);

  // memoire --> pour remettre à 0 l'intégral sur changement d'état.
}


/**
 * Computes the speed to apply on each engine to turn the robot in a given (relative) angle.
 * @param in_angle - pointer to the variable that holds the necessary PID computed adjustement
 * This value is computed in the computePID function.
 * @param out_speedRight pointer to the variable that holds the robot's right motor speed
 * @param out_speedLeft pointer to the variable that holds the robot's left motor speed
 * @param state - pointer to the structure related to robot movement status
 */
void computeSpeed(int in_angle, int *out_speedLeft, int *out_speedRight, Robot_state *state)
{
  int speed = MAX_SPEED-20;
  switch(*state)
  {
    case STOP:
      *out_speedLeft = 0;
      *out_speedRight = 0;
      break;
    case FORWARD:


      //Below a motor speed of 50 the motors do not spin. Maybe we should make this value a function parameter?
      //Due to the robot assembly configuration a negative motor speed value makes the robot to go forward.
      if(speed + in_angle >= 50){
    	  *out_speedLeft = -(speed+in_angle);
      }else
    	  *out_speedLeft = (100 - speed+in_angle);
      if(speed-in_angle >= 50){
          *out_speedRight = -(speed-in_angle);
      }else
    	  *out_speedRight = (100 - speed-in_angle);
      //ecrobot_debug1(10, in_angle, *out_speedRight);
      break;

    case BACKWARD:
      *out_speedLeft = -(speed+in_angle); 
      *out_speedRight = -(speed-in_angle);
      break;
  }
  ecrobot_debug1(10, in_angle, *out_speedRight);
}

/*********************/
/* Ungenerated datas */
/*********************/

#include "kernel.h"

void ecrobot_device_initialize(void)
{
	//ecrobot_init_nxtcolorsensor(NXT_PORT_S2, NXT_COLORSENSOR); // initialize a sensor
	ecrobot_set_light_sensor_active(NXT_PORT_S3); //initialize light sensor
	//ecrobot_init_sonar_sensor(NXT_PORT_S1);
	nxt_motor_set_count(NXT_PORT_C, 0);
}

void ecrobot_device_terminate(void)
{
	//ecrobot_term_nxtcolorsensor(NXT_PORT_S2); // terminate a sensor
	ecrobot_set_light_sensor_inactive(NXT_PORT_S3); // set light sesnor inactive
	//ecrobot_term_sonar_sensor(NXT_PORT_S1); // set sonar sensor inactive
}

/*************************/
/* End Ungenerated datas */
/*************************/
