#ifndef _MINEPUMP_H_
#define _MINEPUMP_H_

#include "gtypes.h"
#include "aadl_runtime_services.h"

void waterlevelmonitoring_body(__waterlevelmonitoring_body_context * self);

void methanemonitoring_body (__methanemonitoring_body_context * self);

void pumpctrl_body(__pumpctrl_body_context * self);

void wateralarm_body(__wateralarm_body_context * self);

#endif
