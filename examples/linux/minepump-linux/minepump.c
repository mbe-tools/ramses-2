#include <gtypes.h>
#include <aadl_runtime_services.h>

#include "simu.h" /* MinePump simulator */

#define MS_L1 70
#define MS_L2 100

#define Normal 0
#define Alarm1 1
#define Alarm2 2
#define LowLevel 0
#define HighLevel 1

/*****************************************************************************/
/* WaterLevelMonitoring_Thread is a periodic task, period = 250 ms.
   It has one output port called "WaterAlarm", of integer type

   At each dispatch, it reads the HLS and LLS sensors, using the
   ReadHLS() and RealLLS() functions.

   - If HLS is true, it sends "HighValue" through its out port;
   - else, if LLS is false, it sends "LowValue";
   - otherwise, it sends the previous value.
*/

void waterlevelmonitoring_body(__waterlevelmonitoring_body_context * self) {

  /* Logic of the thread */
  static int waterlvl = LowLevel;

  if (ReadHLS()) {
    waterlvl = HighLevel;
  } else if (!ReadLLS()) {
    waterlvl = LowLevel;
  }

  /* Send waterlvl through the wateralarm port */

  Put_Value(self->wateralarm, &waterlvl);
  
}

/*****************************************************************************/
/* MethaneMonitoring_Thread is a periodic task, period = 100 ms.
   It has one output port called "MethaneLevel", of integer type

   At each dispatch, it reads the MS sensor. Depending on the methane
   level (constant MS_L1 and MS_L2), it sends either normal, Alert1 or
   Alert2 through its output port.
*/

void methanemonitoring_body (__methanemonitoring_body_context * self) {

  /* Logic of the thread */
  int level = Normal;
  BYTE MS;

  MS=ReadMS();
  if (MS>MS_L2) {
    level=Alarm2;
  } else if (MS>MS_L1) {
    level=Alarm1;
  } else {
    level=Normal;
  }

  /* Port management */
  Put_Value(self->methanelevel, &level);

}

/*****************************************************************************/
/* PumpCtrl_Thread is a sporadic task, with a MIAT of 1 ms It is
   triggered by incoming events from sensors.

   It has two input ports (dispatching)
   * MethaneLevel, of integer type
   * WaterLevel, of integer type

   and one output port
   * WaterAlarm, of integer type

   This task manages the alarm logic, and the pump.

   - if the alarm level is different from Normal, it sends the value 1
     through its wateralarm output port, otherwise it sends 0;
   - if the alarm level is Alarm2 then the pump is deactivated (cmd =
     0 sent to CmdPump); else, if the water level is high, then the
     pump is activated (cmd = 1 sent to CmdPump), otherwise the pump
     is left off.
*/

void pumpctrl_body(__pumpctrl_body_context * self) {
  int niveau_eau, niveau_alarme, alarme;
  int cmd=0;

  Get_Value(self->methanelevel, &niveau_alarme);
  Next_Value(self->methanelevel);
  
  if (niveau_alarme==Normal) {
    alarme=0;
  } else {
    alarme=1;
  }

  /* Send alarme value through the WaterAlarm port */

  Put_Value(self->wateralarm, &alarme);

  if (niveau_alarme==Alarm2) {
    cmd=0;
  } else {

    /* Read from the WaterLevel port */
    Get_Value(self->waterlevel, &niveau_eau);
    Next_Value(self->waterlevel);
    
    if (niveau_eau==HighLevel) {
      cmd=1;
    } else if (niveau_eau==LowLevel) {
      cmd=0;
    }
  }

  CmdPump(cmd);
}

/*****************************************************************************/
/* WaterAlarm_Thread is a sporadic task, with a MIAT of 1 ms
   It has one input port (dispatching)
   * WaterAlarm, of integer type

   It calls CmdAlarm with the value read.
*/

void wateralarm_body(__wateralarm_body_context * self) {
  int value = 1;

  /* Read from the WaterAlarm port */
  Get_Value(self->wateralarm, &value);
  Next_Value(self->wateralarm);

  CmdAlarm(value);
}
