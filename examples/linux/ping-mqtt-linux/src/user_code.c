  /**
 * AADL-RAMSES
 * 
 * Copyright ¬© 2012 TELECOM ParisTech and CNRS
 * 
 * TELECOM ParisTech/LTCI
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.  If not, see 
 * http://www.eclipse.org/org/documents/epl-v10.php
 */

#include "user_code.h"

#ifdef USE_LINUX
#include <stdio.h>
#include <stdlib.h>
#endif

void user_receive(uint16_t d)
{
#if defined USE_LINUX
    printf("received value: %d\n", d);
#endif
}

uint16_t value=0;

void user_send(uint16_t * d)
{
  *d = value;
  value++;
#if defined USE_LINUX
    printf("send value: %d\n", *d);
#endif
}
