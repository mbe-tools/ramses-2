/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

#include "aadl_runtime_services.h"
#include "user_code.h"

void t1_body(__t1_body_context *  context)
{
  static uint8_t t1_counter;
  printf("Executing t1\n");
  t1_counter = t1_counter+1;
  if(t1_counter%5==0)
  {
    printf("Send a mode change request\n");
    t1_counter = 0;
    Put_Value(context->req, NULL);
    Send_Output(context->req);
  }
}

void t2_body(__t2_body_context *  context)
{
  printf("Executing t2\n");
}

void t3_body(__t3_body_context *  context)
{
  printf("Executing t3\n");
}
