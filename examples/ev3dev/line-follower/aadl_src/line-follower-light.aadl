
package nxt_line_follower_light
public

with line_follower_subprograms, line_follower_data_types, Data_Model, OSEK, nxt_lib, Code_Generation_Properties, RAMSES_properties;
with Base_Types; renames Base_Types::all;
-- AI

subprogram Log
	features
		angle : in parameter line_follower_data_types::Log;
		result : out parameter String_265;
	properties
 		Source_language => (C);
  		Source_text => ("../c_src/logging.h", "../c_src/logging.c");
  		Source_name => "logData";
  		Code_Generation_Properties::Convention => AADL;	
end Log;



-----------------
--   Threads   --
-----------------

thread ContrThread
features
  in_light: in data port line_follower_data_types::Int;
  direction : requires data access line_follower_data_types::Robot_state;
  out_angle: out data port line_follower_data_types::Log;
end ContrThread;

thread implementation ContrThread.Impl
  subcomponents
    rightSpeedPort: data line_follower_data_types::Int {Data_Model::Initial_Value => ("0");};
    rightBrake: data line_follower_data_types::Int {Data_Model::Initial_Value => ("0");};
    leftSpeedPort: data line_follower_data_types::Int {Data_Model::Initial_Value => ("1");};
    leftBrake: data line_follower_data_types::Int {Data_Model::Initial_Value => ("0");};
  calls
    call1:
    {
      cp   : subprogram line_follower_subprograms::computePID;
      cs   : subprogram line_follower_subprograms::computeSpeed;
      srs   : subprogram line_follower_subprograms::setRobotState;
      ssL  : subprogram nxt_lib::MotorSetSpeedLib;
      ssR  : subprogram nxt_lib::MotorSetSpeedLib;
    };
connections
  cc0 : parameter cp.currentLight -> in_light;
  cc1 : parameter cp.angle -> cs.angle;
  cc2 : parameter cs.speedL -> ssL.speed;
  cc3 : parameter cs.speedR -> ssR.speed;
  cc4:	data access srs.s -> direction;
  cc5 : parameter ssL.portNb -> leftSpeedPort;
  cc6 : parameter ssL.brake -> leftBrake;
  cc7 : parameter ssR.portNb -> rightSpeedPort;
  cc8 : parameter ssR.brake -> rightBrake;
  cc9 : parameter cp.angle -> out_angle;
properties
  Dispatch_Protocol                  => periodic;
  Period                             => 100 ms;
  Deadline                           => 15 ms;
  Compute_Execution_time             => 0 ms .. 5 ms; 
  Priority                           => 3;
  Compute_Entrypoint_Call_Sequence => reference (call1);
  stack_size  => 512 Bytes;
end ContrThread.Impl;

thread SensorValuePolling
	features
		out_value: out data port line_follower_data_types::Int;
end SensorValuePolling;

thread implementation SensorValuePolling.impl
	subcomponents
  		sensorPortNb: data line_follower_data_types::Int {Data_Model::Initial_Value => ("0");};	
  	properties
		Dispatch_Protocol                  => periodic;
  		Period                             => 100 ms;
  		Compute_Execution_time             => 0 ms .. 3 ms;
  		Deadline                           => 10 ms;
  		Priority                           => 2;
  		stack_size  => 512 Bytes;	
end SensorValuePolling.impl;

thread implementation SensorValuePolling.LightSensor extends SensorValuePolling.impl
calls
  poll:
  {
  	light   : subprogram nxt_lib::GetLight;
  };
connections
  cc0 : parameter out_value -> light.light_out;
  cc01: parameter light.portId -> sensorPortNb;
properties
  Compute_Entrypoint_Call_Sequence => reference (poll);
end SensorValuePolling.LightSensor;



thread Logging
	features
		angle: in event data port line_follower_data_types::Log {Queue_Size => 20;};
		p_out: out event data port String_265 {Output_Time => ([Time => Deadline; Offset => 0 ns .. 0 ns;]);};
end Logging;

thread implementation Logging.impl
calls
  log_seq:
  {
  	l: subprogram log;
  };
connections
  c1 : parameter angle -> l.angle;
  c2 : parameter l.result -> p_out;
properties
  Dispatch_Protocol                  => periodic;
  Period                             => 200 ms;
  Compute_Execution_time             => 0 ms .. 10 ms;
  Deadline                           => 200 ms;
  Priority                           => 1;
  Compute_Entrypoint_Call_Sequence => reference (log_seq);
  stack_size  => 512 Bytes;
  Code_Size => 40 bytes;
end Logging.impl;

-----------------
--  Processes  --
-----------------

process Proc
	features
    p_out: out event data port String_265 {	Queue_Size => 5; 
    							          				Queue_Processing_Protocol => FIFO;};
end Proc;

process implementation Proc.simple
	subcomponents
		C_Th   : thread ContrThread.Impl;
		Bg_Th  : thread SensorValuePolling;
		Log_Th : thread Logging.impl;
  		Direction      : data line_follower_data_types::Robot_state{Data_Model::Initial_Value=>("FORWARD");};
connections
  c1 : data access Direction -> C_Th.direction;
  c2: port Bg_Th.out_value -> C_Th.in_light;
  c3: port C_Th.out_angle -> Log_Th.angle;
  c4: port log_th.p_out -> p_out;
end Proc.simple;



process implementation Proc.LightSensor extends Proc.simple
	subcomponents
		Bg_Th  : refined to thread SensorValuePolling.LightSensor;
	properties
		Prototype_Substitution_Rule => Type_Extension;
end Proc.LightSensor;

system nxt
end nxt;

system implementation nxt.impl
subcomponents 
  nxt_cpu      :  processor arm.impl;
  nxt_mem	:	memory nxt_mem.impl;	
end nxt.impl;

system lineFollower extends nxt
end lineFollower;

virtual bus mqtt_ps
  	features
  		p: in out event data port String_265;
--  	properties
--  		RAMSES_Properties::Communication_Address => "137.194.218.46";
  end mqtt_ps;
  

data String_265
	properties
		RAMSES_properties::Communication_Topic => "line_follower";
		Data_Model::Data_Representation => Array;
		Data_Model::Dimension => (265);
		Data_Model::Base_Type => (classifier(Base_Types::Character));
		Data_Size => 265 bytes;
end String_265;


system implementation lineFollower.simple extends nxt.impl
subcomponents 
	BlackLineFollower     :  process Proc.simple;
	the_net: virtual bus mqtt_ps;
connections
	proc_cnx : port BlackLineFollower.p_out -> the_net.p;	
properties
  Actual_Processor_Binding => (reference (nxt_cpu)) applies to BlackLineFollower;
  Actual_Memory_Binding => (reference (nxt_mem)) applies to BlackLineFollower;
  RAMSES_properties::Communication_Protocol => "MQTT" applies to the_net;
end lineFollower.simple;

system implementation lineFollower.LightSensor extends lineFollower.simple
	subcomponents 
		BlackLineFollower     : refined to process Proc.LightSensor;
--	properties
--		AI::root_system => "SELECTED";
end lineFollower.LightSensor;


---------------
-- Processor --
---------------

processor arm
end arm;

processor implementation arm.impl
properties
  Scheduling_Protocol => (RMS) ;
  OSEK::SystemCounter_MaxAllowedValue => 2000;
  OSEK::SystemCounter_TicksPerBase => 1;
  OSEK::SystemCounter_MinCycle => 1;
  Assign_Time => [Fixed => 0ms..1ms;];
  RAMSES_properties::Target => "Ev3dev";
end arm.impl;

---------------
--   Memory  --
---------------

memory nxt_mem
end nxt_mem;

memory implementation nxt_mem.impl
end nxt_mem.impl;



end nxt_line_follower_light;

