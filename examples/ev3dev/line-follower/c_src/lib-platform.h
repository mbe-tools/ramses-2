#ifndef LIB_PLATFORM_H
#define LIB_PLATFORM_H

#include "/usr/local/include/ev3.h"
#include "/usr/local/include/ev3_port.h"
#include "/usr/local/include/ev3_sensor.h"
#include <stdio.h>
#include <stddef.h>

extern int set_light_sensor_active();

extern int get_light_sensor(int portID);

extern void set_light_sensor_inactive(int portID);

extern int get_color_Id(int portID);

extern void init_color_sensor(int portId, int mode);

extern void set_bg_color_sensor(void);

extern int get_color_light(int portId);

extern int get_sonar_sensor_lib(int portId);

extern int init_sonar_sensor();

extern void close_sonar_sensor(int portId);

extern void motor_set_speed(int portId, int speed, int brake);

extern int motor_get_count(int portId);

extern void motor_set_count(int portId, int count);

#endif
