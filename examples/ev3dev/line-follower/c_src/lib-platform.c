/***
  This file is used to implement wrapper around needed function for the line-follower.aadl model.
  Please refer to lib-platform.h to see wich function are to be implemented and how.
 ***/


#include "lib-platform.h"
#include <stdio.h>
#include <stddef.h>
#include "ev3_tacho.h"

/***	Light sensor	***/


static int light_port_id = -1;
static int sonar_port_id = -1;

int set_light_sensor_active() {
	int portID = -1;
	int sn_color;
	if (ev3_search_sensor( LEGO_EV3_COLOR, &sn_color, 0 )) {
		portID = sn_color;
	}
	ev3_sensor_init();
	set_sensor_mode(portID, "COL-REFLECT");

	return portID;
}


int get_light_sensor(int portID) {
	
	if (light_port_id < 0) {
		light_port_id = set_light_sensor_active();
	}
	portID = light_port_id;

	int val;
	printf("portID = %d\n", portID);
	if (!get_sensor_value(0, portID, &val)) {
		printf("could not get light sensor value. Please check youre robots cables.\n");
		val = 255;
	}	
	printf("light value is %d\n", val);
	fflush(stdout);
	return val;
}


void set_light_sensor_inactive(int portID) {
	return;
}


/***	Color sensor	***/


int get_color_Id(int portID) {
	int sn_color;
	portID = ev3_search_sensor( LEGO_EV3_COLOR, &sn_color, 0 );
	int val;
	if (!get_sensor_value(0, portID, &val)) {
		printf("could not get color sensor value. Please check youre robots cables.\n");
		val = 255;
	}	
	printf("color ID is %d\n", val);
	return val;
}


void init_color_sensor(int portID, int mode) {
	int sn_color;
	portID = ev3_search_sensor( LEGO_EV3_COLOR, &sn_color, 0 );
	ev3_sensor_init();
	set_sensor_mode(portID, "COL-COLOR");
}

void set_bg_color_sensor(void) {
	return;
}

int get_color_light(int portID) {
	int sn_color;
	portID = ev3_search_sensor( LEGO_EV3_COLOR, &sn_color, 0 );
	int val;

	if (!get_sensor_value0(portID, &val)) {
		printf("could not get color sensor value. Please check youre robots cables.\n");
		val = 255;
	}	
	
	printf("color value is %d\n", val);
	return val;
}

/***	Sonar sensor 	***/


int get_sonar_sensor_lib(int portID) {
	int val;

	if (sonar_port_id < 0) {
		sonar_port_id = init_sonar_sensor();
	}
	portID = sonar_port_id;

	printf("sonar portId = %d\n", portID);
	if (!get_sensor_value(0, portID, &val)) {
		printf("could not get sonar sensor value. Please check youre robots cables.\n");
		val = -1;
	}

	printf("val sonar = %d\n", val);
	return val / 10;
}


int init_sonar_sensor() {
	int portID = -1;
	int sn_sonar;
	ev3_sensor_init();
	if (ev3_search_sensor( LEGO_EV3_US, &sn_sonar, 0 )) {
		portID = sn_sonar;
	}
	set_sensor_mode(portID,	"US-DIST-CM");
	return portID;
}


void close_sonar_sensor(int portID) {
	return;
}

/***	Motors	***/


void motor_set_speed(int portID, int speed, int brake) {
	printf("speed = %d, brake = %d\n", speed, brake);
	fflush(stdout);
	ev3_tacho_init();
	set_tacho_stop_action_inx( portID, TACHO_COAST );
	
	if (brake == 0)
		brake = 1;

	set_tacho_speed_sp(portID, speed / brake);
	set_tacho_time_sp( portID, 5000 );
	set_tacho_ramp_up_sp( portID, 2000 );
	set_tacho_ramp_down_sp( portID, 2000 );
	set_tacho_command_inx( portID, TACHO_RUN_TIMED );
}

int motor_get_count(int portID) {
	ev3_search_tacho( LEGO_EV3_L_MOTOR, &portID, 0 );
	int buff;
	get_tacho_count_per_rot( portID, &buff);
	return buff;
}

void motor_set_count(int portID, int count) {
	return;
}
