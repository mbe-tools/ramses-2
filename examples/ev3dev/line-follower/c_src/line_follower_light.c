#include "line_follower_light.h"
#include <math.h>
#include <time.h>
#include "aadl_multiarch.h"
#include "ev3_tacho.h"

// memorized values for the PID controller
float integral = 0.0;
float lastError = 0.0;
float derivative = 0.0;
float error = 0.0;

// state defining if the robot can go forward or not
extern Robot_state state;


/**
 * Computes the error given the lightsensor read and sets the PID computed value to the variable out_angle that holds the necessary speed adjustment.
 * Initially the sensor read is rescaled to a value between 0 and 100.
 * @param currentLight- the light sensor read
 * @param out_angle - pointer to the variable that holds the necessary PID computed adjustement
 * LIGHT_OFFSET
 */

void computePID(int currentLight, Log_t *out_angle)
{
	printf("derivb = %f, errorb = %f, last errorb = %f\n", derivative, error, lastError);
	printf("offset = %f\n", OFFSET);
	//Rescaled light values to 0-100
	printf("currentLight = %d\n", currentLight);
	//int rescaledLight = ((float)100)/(NXT_OSEK_LIGHT_SENSOR_MAX)*(currentLight - NXT_OSEK_LIGHT_SENSOR_MAX)
	//		+ RESCALED_LIGHT_SENSOR_MAX;
	error = currentLight - OFFSET;
	printf("offset = %f\n", OFFSET);
	integral = integral + error;
	printf("error = %f\n", error);
	derivative = error - lastError;
	printf("deriv = %f, error = %f, last error = %f\n", derivative, error, lastError);
	lastError = error;
	struct timespec ts;
	clock_gettime( CLOCK_REALTIME, &ts);
	unsigned tick = ts.tv_nsec / 1000000 + ts.tv_sec * 1000;
	Log_t logToAdd = {tick, error, (float)(KP*error + KI*integral + KD*derivative) / RESCALED_LIGHT_SENSOR_MAX};
	*out_angle = logToAdd;
}


/**
 * Computes the speed to apply on each engine to turn the robot in a given (relative) angle.
 * @param in_angle - pointer to the variable that holds the necessary PID computed adjustement
 * This value is computed in the computePID function.
 * @param out_speedRight pointer to the variable that holds the robot's right motor speed
 * @param out_speedLeft pointer to the variable that holds the robot's left motor speed
 */
void computeSpeed(Log_t in_angle, int *out_speedLeft, int *out_speedRight)
{
	int speed_right_motor;
	int speed_left_motor;
	int max_speed = 10;
//	get_tacho_max_speed(0, &max_speed);

	speed_right_motor = max_speed - in_angle.motor_adjustment;
/*	if(speed_right_motor>100)
		speed_right_motor = 100;
	else if(speed_right_motor<-100)
		speed_right_motor = -100;*/

//	get_tacho_max_speed(1, &max_speed);
	speed_left_motor = max_speed + in_angle.motor_adjustment;
/*	if(speed_left_motor>100)
		speed_left_motor = 100;
	else if(speed_left_motor<-100)
		speed_left_motor = -100;*/

	switch(state)
	{
    	case STOP:
    		*out_speedLeft = 0;
    		*out_speedRight = 0;
    		break;
    	case FORWARD:
    		//Below a motor speed of 50 the motors do not spin. Maybe we should make this value a function parameter?
    		//Due to the robot assembly configuration a negative motor speed value makes the robot to go forward.
    		if(speed_left_motor >= MOTOR_NO_ROTATION)
    			*out_speedLeft = -(speed_left_motor);
    		else
    			*out_speedLeft = (MOTOR_MAX_PWD - speed_left_motor);
    		if( speed_right_motor >= MOTOR_NO_ROTATION)
    			*out_speedRight = -(speed_right_motor);
    		else
    			*out_speedRight = (MOTOR_MAX_PWD - speed_right_motor);
    		break;
	}
	printf("Speed: (%d, %d)\n", *out_speedLeft, *out_speedRight);
}

/*********************/
/* Ungenerated datas */
/*********************/


void ecrobot_device_initialize(void)
{
	return;
	/*
	ecrobot_init_bt_slave("1234"); // initialize bluetooth
	ecrobot_set_light_sensor_active(NXT_PORT_S3); //initialize light sensor
	ecrobot_init_sonar_sensor(NXT_PORT_S2); // initialize sonar (distance) sensor
	*/
}

void ecrobot_device_terminate(void)
{
	return;
	/*
	ecrobot_set_light_sensor_inactive(NXT_PORT_S3); // set light sesnor inactive
	ecrobot_term_sonar_sensor(NXT_PORT_S2); // set sonar sensor inactive
	*/
}

/*************************/
/* End Ungenerated datas */
/*************************/
