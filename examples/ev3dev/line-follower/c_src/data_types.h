#ifndef DATA_TYPES_H
#define DATA_TYPES_H


/**#include "nxt_motors.h" 
#include "ecrobot_interface.h"**/
#include <stddef.h>
#include <stdint.h>

typedef enum Robot_state
{
	FORWARD = 0,
	STOP = 1
} Robot_state;

typedef enum Robot_color
{
	NOT_RECOGNISE = 0,
	BLACK,
	BLUE,
	GREEN,
	YELLOW,
	RED,
	WHITE,
	BROWN
} Robot_color;

typedef struct Log {
	uint32_t system_ticks;
	int error;
	int motor_adjustment;
}Log_t;

#endif
