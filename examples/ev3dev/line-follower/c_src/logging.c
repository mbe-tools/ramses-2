#include "logging.h"
#include <string.h>// for itoa and strcat functions
#include <stdio.h>
#include <stdlib.h>
//#include "ecrobot_interface.h"
//#include "kernel.h"
//#include "kernel_id.h"
//#include "ecrobot_bluetooth.h"

void logData(__logData_context * ctx)
{
	char sent_log[256];
	char temp_buff[64];
	memset(temp_buff, 0, 64);
	memset(sent_log, 0, 256);
	Log_t log;
	int ret = 0;
	uint16_t count;
	do{
 		Get_Count(ctx->angle, &count);
		if(count==0)
		  break;
		ret = Get_Value(ctx->angle, &log);
		sprintf(temp_buff, "%d,%d,%d;", log.system_ticks, log.error, log.motor_adjustment);
		//itoa(log.system_ticks, temp_buff, BASE);
		strcat(sent_log, temp_buff);
		//strcat(sent_log, ",");
		//itoa(log.error, temp_buff, BASE);
		//strcat(sent_log, temp_buff);
		//strcat(sent_log, ",");
		//itoa(log.motor_adjustment, temp_buff, BASE);
		//strcat(sent_log, temp_buff);
		//strcat(sent_log, ";");
		ret = Next_Value(ctx->angle);
	} while(!ret);
	/*printf("%s\n", sent_log);
	*/
	int log_len = strlen(sent_log);
        if(log_len>0)
	{
		sent_log[log_len] = '\0';
		Put_Value(ctx->result, sent_log);
		printf("sent_log : %s\n", sent_log);
	}
}
