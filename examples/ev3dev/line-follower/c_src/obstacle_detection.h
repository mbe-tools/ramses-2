#ifndef OBSTACLE_DETECTION_H
#define OBSTACLE_DETECTION_H

#include "data_types.h"

/* treshold distance, in centimeters */
#define MAX_DISTANCE			25

void selectState(int in_distance, Robot_state *state);

#endif

