#ifndef LINE_FOLLOWER_H
#define LINE_FOLLOWER_H

#include "data_types.h"
#include <ev3.h>
#include <ev3_port.h>

/* light sensor characteristics */
#define MIN_LUMINOSITY			0
#define MAX_LUMINOSITY			100
#define	MIN_LUMINOSITY_TRESHOLD	45
#define MAX_LUMINOSITY_TRESHOLD	55


/* PID Constants */
// #define KP 60.0
// #define KI 4.1
// #define KD 250
// #define NXT_OSEK_LIGHT_SENSOR_MAX 100
// #define RESCALED_LIGHT_SENSOR_MAX 100
// #define MOTOR_MAX_PWD 100
// #define MOTOR_NO_ROTATION 30
// #define MAX_PWD 80
// #define OFFSET 45


#define KP 20.0
#define KI 1.2
#define KD 250
// #define NXT_OSEK_LIGHT_SENSOR_MAX 100
#define RESCALED_LIGHT_SENSOR_MAX 100
#define MOTOR_MAX_PWD 50
#define MOTOR_NO_ROTATION 50
#define MAX_PWD 80
#define OFFSET 45

/* motor characteristics */
#define MAX_SPEED				300

void computePID(int in_light, Log_t *out_angle);
void computeSpeed(Log_t in_angle, int *out_speedLeft, int * out_speedRight);

#endif

