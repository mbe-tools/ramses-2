#include "obstacle_detection.h"

// set state (FORWARD/STOP) according to the measured distance
void selectState(int in_distance, Robot_state *state)
{
	printf("obst. detect. distance = %d\n", in_distance); 
	if(in_distance > MAX_DISTANCE)
          *state = FORWARD;
	else
          *state = STOP;
}
