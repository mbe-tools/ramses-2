import paho.mqtt.client as mqtt
import time

log_file = open('logs.txt', 'w')


def write_logs(message):
    log_file.write(message.replace(";", "\n"))

############
def on_message(client, userdata, message):
    try:
        msg = str(message.payload.decode("utf-8"))
        if msg[0]=='\0':
                return

        messages=msg.split("\0")
        for split_msg in messages:
            if len(split_msg)>0:
                print("message received "+split_msg)
                print "string is UTF-8, length %d bytes" % len(split_msg)
                write_logs(split_msg)

    except UnicodeError:
        print "string is not UTF-8"
        return
    
    print("message topic="+message.topic)
    print("message qos="+str(message.qos))
    print("message retain flag="+str(message.retain))
########################################
broker_address="127.0.0.1"
#broker_address="iot.eclipse.org"
print("creating new instance")
client = mqtt.Client("P1") #create new instance
client.on_message=on_message #attach function to callback
print("connecting to broker")
client.connect(broker_address) #connect to broker
client.loop_start() #start the loop
print("Subscribing to topic","mqtt_ramses_model/line_follower")
client.subscribe("mqtt_ramses_model/line_follower")
time.sleep(400) # wait
client.loop_stop() #stop the loop
