import matplotlib.pyplot as plt
import csv

def display():
    x = []
    y = []
    z = []

    with open('logs.txt','r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')
        iterrows = iter(plots)
        #ignore first two elements (noise)
        next(iterrows)
        next(iterrows)

        
        for row in iterrows:
            x.append(int(row[0]))
            y.append(int(row[1]))
            z.append(int(row[2]))

        plt.subplot(1, 2, 1)        
        plt.plot(x,y, label='Measured Error')
        plt.xlabel('Time')
        plt.ylabel('Error')
        plt.title('Measured Error')
        plt.legend()

        plt.subplot(1, 2, 2)
        plt.plot(x,z, label='Motor PWM adjustment')
        plt.xlabel('Time')
        plt.ylabel('PWM adjustment')
        plt.title('Motor PWM adjustment')
        plt.legend()

        plt.show()


display()
