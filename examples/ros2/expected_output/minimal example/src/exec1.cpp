#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int32.hpp"
//#include <iostream>

using namespace std::chrono_literals;

/* This example creates a subclass of Node and uses std::bind() to register a
 * member function as a callback from the timer. */

class pub : public rclcpp::Node
{
public:
	pub()
  : Node("producer_xs")//, count_(0)
  {
    publisher_ = this->create_publisher<std_msgs::msg::Int32>("topic1", 10);
    timer_ = this->create_wall_timer(
      500ms, std::bind(&pub::timer_callback, this));
  }

private:
  void timer_callback()
  {
    auto message = std_msgs::msg::Int32();
    message.data = rand();
    RCLCPP_INFO(this->get_logger(), "Publishing: '%i'", message.data);//uncomment to get verbose
    //std::cout << "sending";
    publisher_->publish(message);
  }
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<std_msgs::msg::Int32>::SharedPtr publisher_;
};


int main(int argc, char * argv[])
{
	rclcpp::init(argc, argv);
/*  rclcpp::executors::SingleThreadedExecutor executor;
  auto producer = std::make_shared<Publisher_t>();
  auto consumer = std::make_shared<Subscriber_t>();

  executor.add_node(producer);
  executor.add_node(consumer);
  executor.spin();*/


  	rclcpp::executors::SingleThreadedExecutor exec1;
    auto t_pub = std::make_shared<pub>();

    exec1.add_node(t_pub);
    exec1.spin();



  rclcpp::shutdown();
  return 0;
}
