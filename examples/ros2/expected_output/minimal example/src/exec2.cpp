#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int32.hpp"
//#include <iostream>

using namespace std::chrono_literals;

/* This example creates a subclass of Node and uses std::bind() to register a
 * member function as a callback from the timer. */

class t_pub2_node : public rclcpp::Node
// TODO does the entire thing need to be rewritten everytime?
{
public:
	t_pub2_node()
  : Node("t_pub2_name")//, count_(0)
  {
    publisher_ = this->create_publisher<std_msgs::msg::Int32>("topic2", 10);
    timer_ = this->create_wall_timer(
      500ms, std::bind(&t_pub2_node::timer_callback, this));
  }

private:
  void timer_callback()
  {
    auto message = std_msgs::msg::Int32();
    message.data = rand();
    RCLCPP_INFO(this->get_logger(), "Publishing: '%i'", message.data);//uncomment to get verbose
    //std::cout << "sending";
    publisher_->publish(message);
  }
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<std_msgs::msg::Int32>::SharedPtr publisher_;
};

class sub : public rclcpp::Node
{
public:
	sub()
  : Node("consumer_xs")
  {
    subscription_ = this->create_subscription<std_msgs::msg::Int32>(
      "topic1", 10, std::bind(&sub::subscription_callback, this, std::placeholders::_1));
  }

private:
  void subscription_callback(const std_msgs::msg::Int32 & msg) const
  {
    RCLCPP_INFO(this->get_logger(), "I heard: '%i'", msg.data);//uncomment to get verbose
    //std::cout << "receiving";
  }
  rclcpp::Subscription<std_msgs::msg::Int32>::SharedPtr subscription_;
};

class sub2 : public rclcpp::Node
{
public:
	sub2()
  : Node("consumer_xs2")
  {
    subscription_ = this->create_subscription<std_msgs::msg::Int32>(
      "topic2", 10, std::bind(&sub2::subscription_callback, this, std::placeholders::_1));
  }

private:
  void subscription_callback(const std_msgs::msg::Int32 & msg) const
  {
    RCLCPP_INFO(this->get_logger(), "I heard: '%i'", msg.data);//uncomment to get verbose
    //std::cout << "receiving";
  }
  rclcpp::Subscription<std_msgs::msg::Int32>::SharedPtr subscription_;
};

int main(int argc, char * argv[])
{
	rclcpp::init(argc, argv);
/*  rclcpp::executors::SingleThreadedExecutor executor;
  auto producer = std::make_shared<Publisher_t>();
  auto consumer = std::make_shared<Subscriber_t>();

  executor.add_node(producer);
  executor.add_node(consumer);
  executor.spin();*/



    rclcpp::executors::SingleThreadedExecutor exec2;
    	auto t_sub = std::make_shared<sub>();
    	auto t_sub2 = std::make_shared<sub2>();
        auto t_pub2 = std::make_shared<pub2>();

        exec2.add_node(t_sub);
        exec2.add_node(t_sub2);
        exec2.add_node(t_pub2);
        exec2.spin();


  rclcpp::shutdown();
  return 0;
}
