<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_1bNJoEgDEeqsT5NnXFzohg" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="__gTxYEgDEeqsT5NnXFzohg" name="load_refinement_core" workflowURI="${ramses_posix_workflows_path}load_transformation_resources_posix.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_qGKgwEjDEeqafNRi83mxbA" name="${NameLoadRefinementTransformationModules}" modelSlot="Refinement${target}" modelURI="${ramses_ev3dev_transformation_refinement_path}Ev3devTarget.emftvm" modelElementIndex="0"/>
  <propertiesFiles xmi:id="_C7pdEE4pEeqIOpzmJvnc-w" fileURI="default_ev3dev.properties"/>
  <propertiesFiles xmi:id="_SGHcMNr_Eeq05enNFbiy6w" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
