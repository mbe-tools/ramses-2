<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_9UTd8F4qEeqYp8ezKVdS_w" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_Jq1SUF7GEeqTbYoc6DiMAg" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/templates/load_validate_refine_and_generate.workflow">
    <propertyValues xmi:id="_oyAigATGEeuI-KRpR8uPjw" name="target_install_dir" defaultValue="${target_install_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_12YCoATEEeuI-KRpR8uPjw" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_8EbgUATEEeuI-KRpR8uPjw" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_Di_joATFEeuI-KRpR8uPjw" name="refined_trace_file" defaultValue="${refined_trace_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_gXh_oAS9EeuI-KRpR8uPjw" name="target" defaultValue="${target}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_5jfZIAS6EeuI-KRpR8uPjw" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_AnvfAAS7EeuI-KRpR8uPjw" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_bZN_oF7GEeqTbYoc6DiMAg" name="validation_report_file" defaultValue="${validation_report_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_KsiTMOe7Eeqy-t86Uy9Gdw" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_IULv4P8hEeqjUK5cPU4ZFw" name="load_aadl_resources_workflow" defaultValue="${load_aadl_resources_workflow}"/>
    <propertyValues xmi:id="_RgoRUP8hEeqjUK5cPU4ZFw" name="load_transformation_resources_workflow" defaultValue="${load_transformation_resources_workflow}"/>
    <propertyValues xmi:id="_zOUJYP8gEeqjUK5cPU4ZFw" name="validation_workflow" defaultValue="${validation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_407MkP8gEeqjUK5cPU4ZFw" name="refinement_workflow" defaultValue="${refinement_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_A4CHYP8hEeqjUK5cPU4ZFw" name="code_generation_workflow" defaultValue="${code_generation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_oMX0oCNsEeuYccen4kof_w" name="code_compilation_workflow" defaultValue="${code_compilation_workflow}"/>
    <propertyValues xmi:id="_N76b8AZJEeuDOaqjzi9xMA" name="expose_runtime_shared_resources" defaultValue="${expose_runtime_shared_resources}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_Ik0gwAcgEeuBsNtPqQbVlg" name="instance_model_file" defaultValue="${instance_model_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_jTiBMJoWEeqgb4uBb0dwFA" name="target_install_dir" defaultValue="${target_install_dir}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_RvqH0F4rEeqYp8ezKVdS_w" name="output_dir" defaultValue="${output_dir}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Wro5oF4rEeqYp8ezKVdS_w" name="include_dir" description="${description_include_dir}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_auww0F4rEeqYp8ezKVdS_w" name="source_aadl_file" defaultValue="${source_aadl_file}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_4lCdAF4rEeqYp8ezKVdS_w" name="system_implementation_name" defaultValue="${system_implementation_name}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_OYkawF4sEeqYp8ezKVdS_w" name="instance_model_file" defaultValue="${instance_model_file}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_XbVbwF4sEeqYp8ezKVdS_w" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_douyIF4sEeqYp8ezKVdS_w" name="refined_trace_file" defaultValue="${refined_trace_file}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_fp81AF7GEeqTbYoc6DiMAg" name="validation_report_file" defaultValue="${validation_report_file}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_RnPc8NsBEeq05enNFbiy6w" fileURI="default_ev3dev.properties"/>
  <propertiesFiles xmi:id="_P1YMcF4rEeqYp8ezKVdS_w" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
