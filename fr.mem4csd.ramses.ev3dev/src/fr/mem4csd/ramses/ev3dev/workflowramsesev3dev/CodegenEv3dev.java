/**
 */
package fr.mem4csd.ramses.ev3dev.workflowramsesev3dev;

import fr.mem4csd.ramses.linux.workflowramseslinux.CodegenLinux;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Codegen Ev3dev</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.Workflowramsesev3devPackage#getCodegenEv3dev()
 * @model
 * @generated
 */
public interface CodegenEv3dev extends CodegenLinux {
} // CodegenEv3dev
