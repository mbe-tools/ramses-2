/**
 */
package fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.impl;

import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;
import fr.mem4csd.ramses.ev3dev.codegen.ev3dev.AadlToEv3devMakefileUnparser;
import fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.CodegenEv3dev;
import fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.Workflowramsesev3devPackage;
import fr.mem4csd.ramses.linux.workflowramseslinux.impl.CodegenLinuxImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Codegen Ev3dev</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CodegenEv3devImpl extends CodegenLinuxImpl implements CodegenEv3dev {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodegenEv3devImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Workflowramsesev3devPackage.Literals.CODEGEN_EV3DEV;
	}
	
	/**
	 * 
	 * @generated not
	 */
	@Override
	public AadlToTargetBuildGenerator getAadlToTargetBuild() {
		if(aadlToTargetBuild==null)
			setAadlToTargetBuild(new AadlToEv3devMakefileUnparser());
		return aadlToTargetBuild;
	}

} //CodegenEv3devImpl
