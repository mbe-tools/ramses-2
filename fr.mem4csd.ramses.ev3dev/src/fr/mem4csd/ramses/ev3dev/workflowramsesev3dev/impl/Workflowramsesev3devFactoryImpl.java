/**
 */
package fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.impl;

import fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Workflowramsesev3devFactoryImpl extends EFactoryImpl implements Workflowramsesev3devFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Workflowramsesev3devFactory init() {
		try {
			Workflowramsesev3devFactory theWorkflowramsesev3devFactory = (Workflowramsesev3devFactory)EPackage.Registry.INSTANCE.getEFactory(Workflowramsesev3devPackage.eNS_URI);
			if (theWorkflowramsesev3devFactory != null) {
				return theWorkflowramsesev3devFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Workflowramsesev3devFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Workflowramsesev3devFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Workflowramsesev3devPackage.CODEGEN_EV3DEV: return createCodegenEv3dev();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodegenEv3dev createCodegenEv3dev() {
		CodegenEv3devImpl codegenEv3dev = new CodegenEv3devImpl();
		return codegenEv3dev;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Workflowramsesev3devPackage getWorkflowramsesev3devPackage() {
		return (Workflowramsesev3devPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Workflowramsesev3devPackage getPackage() {
		return Workflowramsesev3devPackage.eINSTANCE;
	}

} //Workflowramsesev3devFactoryImpl
