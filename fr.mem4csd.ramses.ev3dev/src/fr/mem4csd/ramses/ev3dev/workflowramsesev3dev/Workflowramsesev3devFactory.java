/**
 */
package fr.mem4csd.ramses.ev3dev.workflowramsesev3dev;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.Workflowramsesev3devPackage
 * @generated
 */
public interface Workflowramsesev3devFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Workflowramsesev3devFactory eINSTANCE = fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.impl.Workflowramsesev3devFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Codegen Ev3dev</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Codegen Ev3dev</em>'.
	 * @generated
	 */
	CodegenEv3dev createCodegenEv3dev();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Workflowramsesev3devPackage getWorkflowramsesev3devPackage();

} //Workflowramsesev3devFactory
