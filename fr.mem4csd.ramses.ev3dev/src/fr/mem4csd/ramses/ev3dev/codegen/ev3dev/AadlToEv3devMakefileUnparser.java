package fr.mem4csd.ramses.ev3dev.codegen.ev3dev;

import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.ProcessImplementation;
import org.osate.aadl2.ProcessSubcomponent;
import org.osate.aadl2.ProcessorSubcomponent;
import org.osate.aadl2.modelsupport.UnparseText;

import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;
import fr.mem4csd.ramses.linux.codegen.makefile.AadlToLinuxMakefileUnparser;
import fr.mem4csd.ramses.mqtt.MQTTGeneratorUtils;
import fr.mem4csd.ramses.sockets.SocketsGeneratorUtils;

public class AadlToEv3devMakefileUnparser extends AadlToLinuxMakefileUnparser {
	public static final String EV3_TARGET_ID = "Ev3Dev";
	
	public AadlToEv3devMakefileUnparser() {
		super();
	}

	@Override
	public String getTargetName() {
		return EV3_TARGET_ID;
	}
	
	@Override
	protected String getAdditionalPreprocessingOptions(ProcessSubcomponent process) {
		String res = super.getAdditionalPreprocessingOptions(process);
		res+=" -I/usr/local/include -DUSE_POSIX -DUSE_EV3DEV ";
		return res;
	}
	
	@Override
	protected String getAdditionalLinkingOptions(ProcessSubcomponent process) {
		String res = super.getAdditionalLinkingOptions(process);
		res+=" -lev3dev-c ";
		return res; 
	}
	
	@Override
	protected void generateMakefile(NamedElement ne, URI outputDir) {
		unparserContent = new UnparseText() ;
		if(ne instanceof ProcessSubcomponent)
			unparserContent.addOutputNewline("CC=arm-linux-gnueabi-gcc");
		if(ne instanceof ProcessorSubcomponent)
		{
			String outputDirPath = outputDir.isFile() ? outputDir.toFileString() : outputDir.toString();
			String homeDir = System.getProperty("user.home");
			unparserContent.addOutputNewline("MAKE=docker run -v "+homeDir+":"+homeDir+" -w "+ outputDirPath +" ev3cc make");
		}
		DefaultMakefileUnparser unparser = new DefaultMakefileUnparser();
		unparser.process(ne);
		super.saveMakefile(unparserContent, outputDir);
	}
	
}
