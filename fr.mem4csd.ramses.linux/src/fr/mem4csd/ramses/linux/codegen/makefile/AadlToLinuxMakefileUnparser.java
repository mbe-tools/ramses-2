/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.linux.codegen.makefile;

import java.util.HashSet;
import java.util.Set ;

import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.ProcessSubcomponent ;

import fr.mem4csd.ramses.core.codegen.AadlToDefaultMakefileUnparser;

public class AadlToLinuxMakefileUnparser extends AadlToDefaultMakefileUnparser {

	public static final String LINUX_TARGET_ID = "Linux";

	public AadlToLinuxMakefileUnparser() {
		super();
	}
	
	@Override
	public String getTargetName() {
		return LINUX_TARGET_ID;
	}
	
	@Override
	protected String getAdditionalLinkingOptions(ProcessSubcomponent process) {
		String res = super.getAdditionalLinkingOptions(process);
		res+=" -lpthread -lrt";
		return res;
	}
	
	@Override
	protected String getAdditionalPreprocessingOptions(ProcessSubcomponent process) {
		return "-DUSE_POSIX -DUSE_LINUX ";
	}
	
	protected String getProcessMQTTOptions() {
		return " -DMQTTCLIENT_PLATFORM_HEADER=MQTTLinux.h";
	}

	@Override
	protected Set<URI> getTargetSourceFiles(ProcessSubcomponent process)
	{
		Set<URI> result = new HashSet<URI>();
		
		for(URI sourceFile: sourceFilesURISet)
		{
			if ( "aadl_dispatch.c".equals( sourceFile.lastSegment() ) )
			{
				result.add(container.getTargetRuntimeDirectoryURI().appendSegment( "aadl_dispatch_posix.c" ) );
			}
			else if ( "aadl_ports_standard.c".equals( sourceFile.lastSegment() ) )
			{
				result.add(container.getTargetRuntimeDirectoryURI().appendSegment( "aadl_ports_standard_posix.c" ) );
			}
		}
		return result;
	}
}