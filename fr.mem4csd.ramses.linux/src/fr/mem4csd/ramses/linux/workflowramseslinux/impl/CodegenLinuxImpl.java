/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.linux.workflowramseslinux.impl;

import org.eclipse.emf.ecore.EClass;

import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator;
import fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl;
import fr.mem4csd.ramses.linux.codegen.makefile.AadlToLinuxMakefileUnparser;
import fr.mem4csd.ramses.linux.workflowramseslinux.CodegenLinux;
import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;
import fr.mem4csd.ramses.posix.codegen.c.AadlToPOSIXCUnparser;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Codegen Linux</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CodegenLinuxImpl extends CodegenImpl implements CodegenLinux {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodegenLinuxImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramseslinuxPackage.Literals.CODEGEN_LINUX;
	}

	/**
	 * 
	 * @generated not
	 */
	@Override
	public AadlToTargetConfigurationGenerator getAadlToTargetConfiguration() {
		if(aadlToTargetConfiguration==null)
			setAadlToTargetConfiguration(new AadlToPOSIXCUnparser());
		return aadlToTargetConfiguration;
	}
	
	/**
	 * 
	 * @generated not
	 */
	@Override
	public AadlToTargetBuildGenerator getAadlToTargetBuild() {
		if(aadlToTargetBuild==null)
			setAadlToTargetBuild(new AadlToLinuxMakefileUnparser());
		return aadlToTargetBuild;
	}

} //CodegenLinuxImpl
