<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_PAkpAMixEei-D6bRtwoJ1Q" name="workflow" description="EMFTVM operations needed by OSEK">
  <components xsi:type="workflow.components:ModelReader" xmi:id="_W-ghoPw0Eeqc7s89p3UfMw" name="${NameLoadValidationTransformationModules}" modelSlot="Refinement${target}" modelURI="${ramses_linux_transformation_refinement_path}LinuxTarget.emftvm" modelElementIndex="0"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_wnSLANDkEeqtbeq_dJTm6w" name="validate_posix" workflowURI="${scheme}${ramses_posix_plugin}ramses/posix/workflows/validate_posix.workflow">
    <propertyValues xmi:id="_iSvIMP5TEeqjUK5cPU4ZFw" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_psLv0AZHEeuDOaqjzi9xMA" name="target" defaultValue="${target}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_ftJmAP5TEeqjUK5cPU4ZFw" name="validation_report_file" defaultValue="${validation_report_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_-K8sYAV-EeuDOaqjzi9xMA" name="validation_report_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_V1PXAPw1Eeqc7s89p3UfMw" fileURI="default_linux.properties"/>
  <propertiesFiles xmi:id="_LPW5QNx_Eeq6KstHiQyO6A" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
