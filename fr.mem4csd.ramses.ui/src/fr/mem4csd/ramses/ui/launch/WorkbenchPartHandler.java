package fr.mem4csd.ramses.ui.launch;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.projection.ProjectionDocument;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.XtextSourceViewer;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.outline.impl.EObjectNode;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;

/**
 * Handler to allow extracting EMF model elements from an {@link ISelection} from various workbench parts
 * 
 * @author Dominique Blouin
 *
 */
public class WorkbenchPartHandler {
	
	private final EObjectAtOffsetHelper eObjectOffsetHelper;

	public WorkbenchPartHandler() {
		eObjectOffsetHelper = new EObjectAtOffsetHelper();
	}


	/**
	 * Extract selected semantic elements from an {@link ExecutionEvent}.
	 * 
	 * @param event
	 *            This is the click event to create a trace
	 * @return A list of all the selected elements
	 */
	public NamedElement extractSelectedElement( final ExecutionEvent event ) {
		final IWorkbenchPart workbenchPart = HandlerUtil.getActivePart( event );
		final ISelection currentSelection;
		
		// For some reason HandlerUtil.getCurrentSelection(event) returns the previous selection in some cases so we look for 
		// the selection from the selection provider first
		if ( workbenchPart.getSite().getSelectionProvider() != null ) {
			currentSelection = workbenchPart.getSite().getSelectionProvider().getSelection();
		}
		else {
			currentSelection = HandlerUtil.getCurrentSelection( event );
		}

		if (  currentSelection instanceof IStructuredSelection ) {
			final Object obj = ( (IStructuredSelection) currentSelection ).getFirstElement();
			
			if ( obj instanceof EObjectNode ) {
				final EObjectNode outlineObject = (EObjectNode) obj;
				final Resource resource = getResource( obj );

				if ( resource != null ) {
					final EObject eObject = outlineObject.getEObject( resource );
					
					if ( eObject instanceof NamedElement ) {
						return (NamedElement) eObject;
					}
				}
			}
			else if ( obj instanceof IFile ) {
				final IEditorPart editorPart = HandlerUtil.getActiveEditor( event );
				ResourceSet resSet = getResourceSet( editorPart );
				
				if ( resSet == null ) {
					resSet = new ResourceSetImpl();
				}

				final URI resUri = OsateResourceUtil.toResourceURI( (IFile) obj );
				final Resource res = resSet.getResource( resUri, true );
				
				final EObject root = res.getContents().get( 0 );
				
				if ( root instanceof SystemInstance ) {
					return (SystemInstance) root;
				}
			}
		}
		else if ( currentSelection instanceof ITextSelection ) {
			final Resource resource = getResource( workbenchPart );
			
			if ( resource instanceof XtextResource ) {
				final ITextSelection textSelection = (ITextSelection) currentSelection;
				final EObject selectedElement = eObjectOffsetHelper.resolveElementAt( (XtextResource) resource, textSelection.getOffset() );
			
				if ( selectedElement instanceof NamedElement ) {
					return (NamedElement) selectedElement;
				}
			}
		}
		
		return null;
	}
	
	public static IXtextDocument get(Object ctx) {
		if (ctx instanceof IXtextDocument) {
			return (IXtextDocument) ctx;
		}
		
		if (ctx instanceof ProjectionDocument) {
			return get(((ProjectionDocument) ctx).getMasterDocument());
		}
		
		if (ctx instanceof XtextSourceViewer) {
			return ((XtextSourceViewer) ctx).getXtextDocument();
		}
		
		if (ctx instanceof ITextViewer) {
			return get(((ITextViewer) ctx).getDocument());
		}
		
		if (ctx instanceof XtextEditor) {
			return ((XtextEditor) ctx).getDocument();
		}
		
		if (ctx instanceof EObjectNode ) {
			final URI objResUri = ( (EObjectNode) ctx ).getEObjectURI().trimFragment();
			final IFile file = OsateResourceUtil.toIFile( objResUri ); 
			
			return get( file );
		}
		
		if (ctx instanceof IFile) {
			IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			
			return get(activePage.findEditor(new FileEditorInput((IFile) ctx)));
		}

		return null;
	}
	
	protected Resource getResource( final Object ctx ) {
		final IXtextDocument document = get( ctx );
		
		if ( document != null ) {
			return document.readOnly( new IUnitOfWork<XtextResource, XtextResource>() {
	
					@Override
					public XtextResource exec(XtextResource state)
					throws Exception {
						return state;
					}
				} );
		}
		
		return null;
	}

	public ResourceSet getResourceSet( final IWorkbenchPart workbenchPart ) {
		final Resource resource = getResource( workbenchPart );
		
		return resource == null ? null : resource.getResourceSet();
	}
}
