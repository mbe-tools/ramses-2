package fr.mem4csd.ramses.ui.launch;

import java.io.IOException;

import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.resource.Resource;

import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel;

public class RamsesJob extends WorkspaceJob {
	
	public static String PLUGIN_ID = "fr.mem4csd.ramses.ui";
	
	private final RamsesConfiguration config;

	private final Resource sourceFile;

	private final String systemImplName;
	
	public RamsesJob( 	final RamsesConfiguration config,
						final Resource sourceFile,
						final String systemImplName ) {
		super( "RAMSES Code Generation" );

		this.config = config;
		this.sourceFile = sourceFile;
		this.systemImplName = systemImplName;
		setUser( true );
	}

	@Override
	public IStatus runInWorkspace( final IProgressMonitor monitor ) 
	throws CoreException {
		monitor.beginTask( "Starting RAMSES...", IProgressMonitor.UNKNOWN );

		if ( monitor.isCanceled() ) {
			throw new OperationCanceledException( "Generation has been canceled at the begining" );
		}

		try {
			RamsesWorkflowViewModel.INSTANCE.executeWorkflow( config, sourceFile, systemImplName, monitor );

			if ( monitor.isCanceled() ) {
				throw new OperationCanceledException( "Generation has been canceled after code generation" );
			}

			return new Status( IStatus.OK, PLUGIN_ID, "Success" );
		} 
		catch ( final WorkflowExecutionException | IOException ex ) {
			throw new CoreException( new Status(IStatus.ERROR, PLUGIN_ID, ex.getMessage() ) );
		}
	}
}
