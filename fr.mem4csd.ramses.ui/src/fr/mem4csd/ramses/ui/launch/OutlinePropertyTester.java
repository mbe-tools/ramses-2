/**
 * RAMSES 2.0
 * 
 * Copyright © 2014-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.ui.launch;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.xtext.ui.editor.outline.impl.EObjectNode ;

import fr.mem4csd.ramses.core.codegen.utils.Names ;

public class OutlinePropertyTester extends PropertyTester {
	
	private static final String CAN_GENERATE_PROPERTY = "canGenerate" ;

	@Override
	public boolean test(	Object receiver,
							String property,
							Object[] args,
							Object expectedValue ) {
		if ( CAN_GENERATE_PROPERTY.equals( property ) )	{
			final EObjectNode selectedNode = (EObjectNode) receiver; 
			final String nodeClassName = selectedNode.getEClass().getInstanceClassName();

			return Names.GENERATION_ROOT_OBJECT_CLASS.equals( nodeClassName );
		}

		return false;
	}
}