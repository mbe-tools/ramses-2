/*******************************************************************************
 * Copyright (c) 2016, 2019 Chalmers | University of Gothenburg, rt-labs and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *  
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *      Chalmers | University of Gothenburg and rt-labs - initial API and implementation and/or initial documentation
 *      Chalmers | University of Gothenburg - additional features, updated API
 *******************************************************************************/
package fr.mem4csd.ramses.ui.launch;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;

import fr.mem4csd.ramses.core.ramsesviewmodel.ConfigurationException;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelFactory;
import fr.mem4csd.ramses.ui.preferences.RamsesPropertyPage;

public class RamsesHandler extends AbstractHandler {
	
	private final WorkbenchPartHandler workbenchPartHandler;
	
	public RamsesHandler() {
		workbenchPartHandler = new WorkbenchPartHandler();
	}

	@Override
	public Object execute( final ExecutionEvent event )
	throws ExecutionException {
		final NamedElement element = workbenchPartHandler.extractSelectedElement( event );
		
		if ( element != null ) {
			try {
				final Resource res =  element.eResource();
				final URI resUri = res.getURI();
				final RamsesConfiguration config = fetchConfig( resUri );
				
				if ( config != null ) {
					final RamsesJob job = new RamsesJob( config, res, element.getName() );
					job.schedule();
				}
			} 
			catch ( final ConfigurationException | CoreException e) {
				throw new ExecutionException( null, e );
			}
		}

		return null;
	}
	
	private RamsesConfiguration fetchConfig( final URI resUri )
	throws ConfigurationException, CoreException {
		final IResource res = OsateResourceUtil.toIFile( resUri );
		
		if ( res != null ) {
			final RamsesConfiguration config = RamsesviewmodelFactory.eINSTANCE.createRamsesConfiguration();
			final IProject project = res.getProject();
			
			try	{
				config.fetchProperties( project );
				
				return config;
			}
			catch ( final ConfigurationException ex ) {
				if ( RamsesPropertyPage.openPropertyDialog( project ) ) {
					// User has provided a config, load it.
					config.fetchProperties( project );
					
					return config;
				}
				
				// User has pressed cancel, configuration is incorrect
				throw ex;
			}
		}
		
		return null;
	}
}
