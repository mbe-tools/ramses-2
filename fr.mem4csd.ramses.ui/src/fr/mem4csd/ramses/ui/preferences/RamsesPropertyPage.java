/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.ui.preferences;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPropertyPage;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.eclipse.ui.dialogs.PropertyPage;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osate.utils.internal.FileUtils;

import com.google.common.base.Splitter;

import fr.mem4csd.ramses.core.ramsesviewmodel.ConfigurationException;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelFactory;
import fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowViewModelImpl;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;


public class RamsesPropertyPage extends PropertyPage implements IWorkbenchPropertyPage {
	
	private static final String PATH_TITLE = "Output directory (where code will be generated)";

	private static final String _PROPERTY_PAGE_ID = "fr.mem4csd.ramses.ui.RamsesPropertyPage" ;

	protected static final int TEXT_FIELD_WIDTH = 43;
	private String DEFAULT_PATH;
	private String PROJECT_NAME;  
	
	private static Logger _LOGGER = Logger.getLogger(RamsesPropertyPage.class) ;
	private static StatusManager _MANAGER = StatusManager.getManager() ;
	  
	protected RamsesConfiguration _config = null;
	IProject _project;

	protected Text outputDirText;
	private Button target;
	private Button exposeRuntimeSharedResources;
	private Text runtimePathText;
	private Label selectedPathLabel;
	private Button newProperty = null;
	private Button RemoveProperty = null;

	/**
	 * Constructor for SamplePropertyPage.
	 */
	public RamsesPropertyPage() {
		super();
	}

	private void loadConfig()
	{
		try
		{
			_project = (IProject) getElement() ;
			if(_config==null)
				_config =  RamsesviewmodelFactory.eINSTANCE.createRamsesConfiguration();
			_config.fetchProperties(_project) ;
		}
		catch(ConfigurationException ee)
		{
			// Missing configuration or first time configuration.
			_config = RamsesviewmodelFactory.eINSTANCE.createRamsesConfiguration();
		}
		catch(Exception e)
		{
			String msg = "cannot load RAMSES configuration" ;
			_LOGGER.fatal(msg, e) ;
			throw new RuntimeException(msg, e) ;
		}
	}

	private void addInformationSection(Composite parent) {
		Composite composite = createDefaultComposite(parent);

		//Label for information
		Label pathLabel = new Label(composite, SWT.CENTER);
		pathLabel.setText("This property page enable you to configure your project for RAMSES");
	}

	protected void addSeparator(Composite parent) {
		Label separator = new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		separator.setLayoutData(gridData);
	}

	private static String getDefaultOutputDir(IResource resource)
	{
		return resource.getLocation().append("output").makeAbsolute().toOSString();
	}

	protected void addOutputDirectorySection(Composite parent,
			String labelMessage)
	{
		Label label = new Label(parent, SWT.BOLD);
		label.setText("1 - " + labelMessage);

		Composite composite = createDefaultComposite(parent);

		// Label for output directory field
		Label ownerLabel = new Label(composite, SWT.BOLD);
		ownerLabel.setText(PATH_TITLE);

		// output Directory button field
		outputDirText = new Text(composite, SWT.SINGLE | SWT.BORDER);
		GridData gd = new GridData();
		gd.widthHint = convertWidthInCharsToPixels(TEXT_FIELD_WIDTH);
		outputDirText.setLayoutData(gd);
//		outputDirText.setEditable(false) ;

		// Populate output dir text field
		DEFAULT_PATH = getDefaultOutputDir((IResource) getElement());
		PROJECT_NAME = (((IResource) getElement()).getName()) ;

		if (_config.getRamsesOutputDir() == null)
			outputDirText.setText(DEFAULT_PATH);
		else
			outputDirText.setText(_config.getRamsesOutputDir().getAbsolutePath());

		// Move the cursor to the end.
		outputDirText.setSelection(outputDirText.getText().length()) ;

		Button button = new Button(composite, SWT.PUSH);
		button.setText("Browse existing directories in workspace...");
		button.setAlignment(SWT.LEFT);

		button.addSelectionListener( new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e) 
			{
				ContainerSelectionDialog browseWorkspace = 
						new ContainerSelectionDialog(getShell(),
								(IContainer) getElement(),
								true,
								"Select output directory for generated code"
								);
				if (browseWorkspace.open() == ContainerSelectionDialog.OK) {
					Object[] result = browseWorkspace.getResult();
					if (result != null && result.length > 0) {
						Path outputDir = (Path) result[0];

						outputDirText.setText(convertToAbsolutePath(outputDir));
						// Move the cursor to the end.
						outputDirText.setSelection(outputDirText.getText().length()) ;
					}
				}
			}
		});
	}

	private String convertToAbsolutePath(Path relativePath)
	{
		String root = File.separator + PROJECT_NAME ;

		if(root.equals(relativePath.toOSString()))
		{
			return DEFAULT_PATH ;
		}
		else
		{
			return DEFAULT_PATH + File.separator +
					relativePath.removeFirstSegments(1).toOSString() ;
		}
	}

	protected Composite createDefaultComposite(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);

		GridData data = new GridData();
		data.verticalAlignment = GridData.FILL;
		data.horizontalAlignment = GridData.FILL;
		composite.setLayoutData(data);

		return composite;
	}

	/**
	 * @see PreferencePage#createContents(Composite)
	 */
	@Override
	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.V_SCROLL);
		GridLayout layout = new GridLayout();
		composite.setLayout(layout);
		GridData data = new GridData(GridData.FILL);
		data.grabExcessHorizontalSpace = true;
		composite.setLayoutData(data);

		loadConfig() ; 

		addInformationSection(composite);
		addSeparator(composite);
		
		addLinkToPreferencePage(composite);
		addSeparator(composite);
		
		addOutputDirectorySection(composite,
				"Select output directory for generated code");
		addSeparator(composite);
		addTargetSection(composite);
		addSeparator(composite);
		addPropertiesSection(composite);

		addSeparator(composite);
		exposeRuntimeSharedResources = new Button(composite,SWT.CHECK);
		exposeRuntimeSharedResources.setText("Expose runtime shared resources in refined model");

		
		return composite;
	}

	
	private void addLinkToPreferencePage(Composite composite) {
		Link link = new Link(composite, SWT.NONE);
		link.setText("<a>Configure RAMSES workspace...</a>");
		
		// Event handling when users click on links.
		link.addSelectionListener(new SelectionAdapter()  {
		 
		    @Override
		    public void widgetSelected(SelectionEvent e) {
		    	PreferenceDialog pref = PreferencesUtil.createPreferenceDialogOn(getShell(), "fr.mem4csd.ramses.core.RamsesTargetPreferencePage", null, null);
	            if (pref != null) {
	                pref.open();
	            }
		    }
		     
		});
	}

	private void addTargetSection(Composite composite)
	{
		Label targetInfo = new Label(composite, SWT.BOLD);
		targetInfo.setText("2 - Select one of the following target platforms to generate code for:");

		// Create checkboxes for targets supported by RAMSES

		Map<String, AadlToTargetBuildGenerator> generators = RamsesWorkflowViewModel.INSTANCE.getAvailableGenerators();

		Map<String, Button> buttonMap = new HashMap<String, Button>();

		for(String generatorName: generators.keySet())
		{
			Button b = new Button(composite, SWT.RADIO);
			b.setText(generatorName + " " + generators.get(generatorName).getTargetShortDescription());
			b.setData(generatorName);
			buttonMap.put(generatorName, b);
		}

		if(_config.getTargetId() != null)
		{
			target = buttonMap.get(_config.getTargetId());
			if(target!=null)
				target.setSelection(true) ;
		}

		Listener listener = new Listener()
		{
			@Override
			public void handleEvent(Event event)
			{
				Button button = (Button)(event.widget);
				TargetChangedEvent e = new TargetChangedEvent();
				selectedPathLabel.setText("Select path for " + button.getText());
				selectedPathLabel.redraw();
				if (button.getSelection())
				{
					target = button;
					_config.setTargetId((String)button.getData());
					e.TargetChanged();

					if (runtimePathText != null) {
						if (_config.getTargetId() != null) {
							
							try {
								_config.updateTargetPath();
							} catch (ConfigurationException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							Map<String, AadlToTargetBuildGenerator> Generators = RamsesWorkflowViewModelImpl.INSTANCE.getAvailableGenerators();
							AadlToTargetBuildGenerator buildGen = Generators.get(_config.getTargetId());
							String value = _config.getPropertyMap().get(buildGen.getTargetName().toLowerCase());

							if (value != null) {
								runtimePathText.setText(value);
							}
							else
							{
								runtimePathText.setText("");
							}
						}
					}
				}
			}
		};

		for(Button b:buttonMap.values())
		{
			b.addListener(SWT.Selection, listener);
		}

		
		Button pathButton = new Button(composite, SWT.PUSH);
		pathButton.setText("Choose the target platform path");
		pathButton.setAlignment(SWT.RIGHT);

		selectedPathLabel = new Label(composite, SWT.BOLD); 
		selectedPathLabel.setText("Path for target not selected ...");
		GridData grdPath = new GridData();
		grdPath.widthHint = convertWidthInCharsToPixels(100);
		selectedPathLabel.setLayoutData(grdPath) ;

		runtimePathText = new Text(composite, SWT.BOLD | SWT.SINGLE | SWT.BORDER);
		GridData grd = new GridData();
		grd.widthHint = convertWidthInCharsToPixels(TEXT_FIELD_WIDTH);
		runtimePathText.setLayoutData(grd) ;

		if (_config.getRuntimePath() != null)
				runtimePathText.setText(_config.getRuntimePath());

		pathButton.addSelectionListener( new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{       
				DirectoryDialog ddg = new DirectoryDialog(getShell());
				File selectedFile = null;
				String file = ddg.open();
				if(file != null && file.length() > 2)
				{
					selectedFile = new File(file);
					runtimePathText.setText(selectedFile.getAbsolutePath());
					RamsesPropertyMap.GetInstance().updateTable(_config.getTargetId(), _config.getTargetId(), runtimePathText.getText());
				}
			}
		});
	}

	private void addTableButton(Composite composite, TableHelper table) {
	    addSeparator(composite);

	    if (newProperty != null) {
	    	newProperty.redraw();
	    }
	    else {
		    newProperty = new Button(composite, SWT.PUSH);
		    newProperty.setText("Add property");
		    newProperty.setAlignment(SWT.RIGHT);

		    newProperty.addListener(SWT.Selection, new Listener() {
				
				@Override
				public void handleEvent(Event arg0) {
					AddProperty addApplication = new AddProperty(getShell());
					addApplication.setTable(table);
					addApplication.setTarget(_config.getTargetId());
					addApplication.setConfig(_config);
					addApplication.open();
					addApplication.setBlockOnOpen(true);
					Shell newap = addApplication.getShell();
					newap.setActive();
				}
			});
			newProperty.setSize(100, 50);
		}
	    
	    if (RemoveProperty != null) {
	    	RemoveProperty.redraw();
	    }
	    else {
		    RemoveProperty = new Button(composite, SWT.PUSH);
		    RemoveProperty.setText("Remove property");
		    RemoveProperty.setAlignment(SWT.RIGHT);
	
		    RemoveProperty.addListener(SWT.Selection, new Listener() {
				
				@Override
				public void handleEvent(Event arg0) {
					RemoveProperty removeApplication = new RemoveProperty(getShell(), table, _config);
					removeApplication.open();
					Shell newap = removeApplication.getShell();
					removeApplication.setBlockOnOpen(true);
					newap.setActive();
				}
			});
		    RemoveProperty.setSize(100, 50);
	    }
	}

	private void addPropertiesSection(Composite composite) {
		Label installDirInfo = new Label(composite, SWT.BOLD);
		installDirInfo.setText("3 - Specify missing properties");

		TableHelper table = new TableHelper(composite, _config);
		table.createTable(composite, _config);

		 Listener listenerChangeTarget = new Listener()
			{
				@Override
				public void handleEvent(Event event)
				{
					try {
						table.changeTableProperties(_config);
						addTableButton(composite, table);
					} catch (Exception e) {
						TargetChangedEvent.removeListener(this);
					}
				}
			};

		    TargetChangedEvent.addListener(listenerChangeTarget);
		    addTableButton(composite, table);
	}

	/**
	 * @see PreferencePage#performDefaults()
	 */
	@Override
	protected void performDefaults() {
		super.performDefaults();
		outputDirText.setText(DEFAULT_PATH);
	}

	/**
	 * @see PreferencePage#performOk()
	 */
	@Override
	public boolean performOk()
	{
		RamsesPropertyMap.GetInstance().updateTable(_config.getTargetId(), RamsesConfiguration.OUTPUT_DIR, outputDirText.getText());
		RamsesPropertyMap.GetInstance().updateTable(_config.getTargetId(), _config.getTargetId(), runtimePathText.getText());
		Boolean exposeSharedResources = exposeRuntimeSharedResources.getSelection();
		RamsesPropertyMap.GetInstance().updateTable(_config.getTargetId(), RamsesConfiguration.EXPOSE_RUNTIME_SHARED_RESOURCES, exposeSharedResources.toString());

		Map<String, Map<String, String>> prop = RamsesPropertyMap.GetInstance().getProperties();

		for (String name : RamsesWorkflowViewModel.INSTANCE.getAvailableGeneratorNames()) {
			String value;

			if (prop.get(name) == null) {
				value = _config.getPropertyMap().get(name);
			}
			else
				value = prop.get(name).get(name);

			if (value != null) {
				RamsesPropertyMap.GetInstance().updateTable(_config.getTargetId(), name, value);
			}
		}

		try
		{
			short errno = configChecker() ;

			if(errno == 0)
			{
				_config.saveConfig(_project, RamsesPropertyMap.GetInstance().getRelevantProperties(_config.getTargetId()));
				RamsesPropertyMap.resetInstance();
				TargetChangedEvent.resetList();
				return true;
			}
			else
			{
				String errorMsg = popupConfigurationErrorMessage(errno);
				Status status = new Status(IStatus.ERROR, "RAMSES", errorMsg) ;
				_MANAGER.handle(status, StatusManager.BLOCK);
				return false;
			}
		}
		catch (CoreException e)
		{
			String msg = "cannot save RAMSES configuration" ;
			_LOGGER.fatal(msg, e);
			throw new RuntimeException(msg, e) ;
		}
	}

	private short configChecker()
	{
		short result = 0 ; // Zero means no error.

		if(outputDirText.getText() == null || outputDirText.getText().isEmpty())
		{
			result += 10 ;
		}

		if(target != null && target.getData() != null)
		{
			// Get the target_specific_generator
			AadlToTargetBuildGenerator gen = RamsesWorkflowViewModel.INSTANCE.getGeneratorFromTargetName( target.getData().toString() ) ;
			if(gen != null)
			{
				try
				{
					URI runtimePath = null ;

					if(runtimePathText.getText() != null &&
							! runtimePathText.getText().isEmpty() )
					{
						String runtimePathStr = runtimePathText.getText();
						if(runtimePathStr.endsWith(","))
							runtimePathStr = runtimePathStr.substring(0, runtimePathStr.lastIndexOf(","));
						if(runtimePathStr.contains("="))
						{
							Map<String, String> installPaths = Splitter.on(",").withKeyValueSeparator("=").split(runtimePathStr);
							for(String singlePath: installPaths.values())
							{
								if(singlePath==null || singlePath.isEmpty() || singlePath.equals("null"))
									continue;
								runtimePath = URI.createFileURI( FileUtils.stringToFile(singlePath).getAbsolutePath() );
							}
						}
						else
							runtimePath = URI.createFileURI( FileUtils.stringToFile(runtimePathStr).getAbsolutePath() );
					}
					// the runtime path can be null.
					if(! gen.validateTargetPath(runtimePath))
					{
						result +=400 ;
					}
				}
				catch (FileNotFoundException e)
				{
					result +=300 ;
				}
			}
			else
			{
				result +=200 ;
			}
		}
		else
		{
			result += 100 ;
		}

		return result ;
	}

	private String popupConfigurationErrorMessage(short errno)
	{
		StringBuilder msg =
				new StringBuilder("Cannot save RAMSES configuration:\n\n");

		if(errno == 1)
		{
			msg.append("\n\tCan't fetch the project.") ;
		}
		else
		{
			int tmp = errno/100 ;

			switch(tmp)
			{
			case 1 :
			{
				msg.append("\n\tTarget is missing.") ;
				break ;
			}

			case 2 :
			{
				msg.append("\n\tThe target " + target.getData().toString() + " is not supported.") ;
				break ;
			}

			case 3 :
			{
				msg.append("\n\tThe target selection is missing.") ;
				break ;
			}

			case 4 :
			{
				msg.append("\n\tThe given install directory for " + _config.getTargetId() + " is invalid") ;
				break ;
			}
			}

			if(errno%100 != 0)
			{
				msg.append("\n\tMissing output directory.") ;
			}
		}
		
		return msg.toString();
		
	}

	// Return false if user has canceled.
	public static boolean openPropertyDialog(IProject project)
	{
		Shell shell = getCurrentShell() ;
		Rectangle screenSize = Display.getCurrent().getPrimaryMonitor().getBounds();
		shell.setLocation((screenSize.width - shell.getBounds().width) / 2, 0);


		//Instantiate the project propertyPage.
		PreferenceDialog prefDiag = PreferencesUtil.
				createPropertyDialogOn(shell, project, _PROPERTY_PAGE_ID, null,
						null);

		// TODO: display the missing informations.

		return prefDiag.open() == Window.OK ;
	}

	public static Shell getCurrentShell()
	{
		Display display = Display.getCurrent();
		return new Shell(display, SWT.BORDER | SWT.CENTER);
	}
}
