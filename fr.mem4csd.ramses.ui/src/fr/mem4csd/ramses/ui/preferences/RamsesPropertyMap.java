/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.ui.preferences;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import de.mdelab.workflow.PropertiesFile;
import de.mdelab.workflow.Workflow;
import de.mdelab.workflow.WorkflowProperty;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel;

public class RamsesPropertyMap {
	private Map<String, Map<String, String>> _properties = new HashMap<String, Map<String, String>>();
	public Map<String, String> descriptions = new HashMap<String, String>();
	private static RamsesPropertyMap instance = null;

	private RamsesPropertyMap() {}
	
	public static RamsesPropertyMap GetInstance() {
		if (instance == null)
			instance = new RamsesPropertyMap();

		return instance;
	}

	public Map<String, Map<String, String>> getProperties() {
		return _properties;
	}
	
	public void updateTable(String target_id, String key, String new_value) {
		if(_properties.get(target_id)==null)
		{
			Map<String, String> properties = new HashMap<String, String>();
			properties.put(key, new_value);
			_properties.put(target_id, properties);
		}
		_properties.get(target_id).put(key, new_value);
	}


	public Map<String, String> getRelevantProperties(String target_id) {
		return _properties.get(target_id);
	}


	public Workflow fetchCurrentWorkflow(RamsesConfiguration config) {
		ResourceSet resourceSet = new ResourceSetImpl();
		URI WorkflowName = RamsesWorkflowViewModel.INSTANCE.getTargetWorkflow(config.getTargetId());
	//	final URI workflowURI = RamsesWorkflowViewModel.instance.getAvailableWorkflows().get(WorkflowName);

		final Resource workflowResource = resourceSet.getResource(WorkflowName, true);

		final Workflow workflow = (Workflow) workflowResource.getContents().get(0);

		return workflow;
	}

	public Map<String, String> createOrFetchNecessaryPropertiesMap(RamsesConfiguration config) {
		String target_id = config.getTargetId();
		Map<String, String> config_properties = config.getPropertyMap();

//		EB: commented to display property description
//		if (_properties.get(target_id) != null) {
//			return _properties.get(target_id);
//		}

		final Workflow workflow = fetchCurrentWorkflow(config);
		EList<WorkflowProperty> workflow_properties = workflow.getProperties();
		Map<String, String> properties = new HashMap<String, String>();
		EList<PropertiesFile> workflowPropertiesFile = workflow.getPropertiesFiles();
		for(PropertiesFile pf : workflowPropertiesFile)
		{
			try {
				final Properties p = pf.load( pf.eResource().getURI() );
				for (final Entry<Object, Object> e : p.entrySet()) {
					properties.put("${" + e.getKey() + "}", (String) e.getValue());
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		Map<String, String> result = new HashMap<String, String>();

		for (WorkflowProperty prop : workflow_properties) {
			if (prop.getName() == RamsesConfiguration.RUNTIME_PATH)
				continue;

			String property = prop.getName();
			String value = prop.getDefaultValue();
			String config_value = config_properties.get(property);

			if (!result.containsKey(property)) {
				if (value == null) {
					if (config_value != null)
						result.put(property, config_value);
					else
						result.put(property, "");

					String description = prop.getDescription() == null? "" : prop.getDescription();
					if(description!=null && !description.isEmpty())
					{
						for(String s: properties.keySet())
						{
							description = description.replace(s, properties.get(s));
						}						
					}
					descriptions.put(property, description);
				}
			}
		}
		
		String runtimeDir = config_properties.get(target_id);

		if (runtimeDir != null)
			result.put(target_id, runtimeDir);
		
		_properties.put(target_id, result);
		return result;
	}

	public void setDefault() {
		_properties.clear();
	}
	
	public void removeProperty(String targetId, String property) {
		_properties.get(targetId).remove(property);
	}
	
	public static void resetInstance() {
		instance = null;
	}
}
