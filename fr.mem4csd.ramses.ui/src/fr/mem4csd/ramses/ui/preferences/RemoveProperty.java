/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.ui.preferences;

import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration;

public class RemoveProperty extends ApplicationWindow {
	private TableHelper _table;
	private RamsesConfiguration _config;

	public RemoveProperty(Shell parentShell, TableHelper table, RamsesConfiguration config) {
		super(parentShell);
		_table = table;
		_config = config;
	}
	
	protected Control createContents(Composite parent) {
		Composite RemovePropertyPage = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		RemovePropertyPage.setLayout(layout);

		GridData data = new GridData();
		data.verticalAlignment = GridData.FILL;
		data.horizontalAlignment = GridData.FILL;
		RemovePropertyPage.setLayoutData(data);
		
		Text property = new Text(RemovePropertyPage, SWT.BORDER);

		Button Ok = new Button(RemovePropertyPage, SWT.PUSH);
		ApplicationWindow currentWindow = this;
		Ok.setText("Ok");

		Ok.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				RamsesPropertyMap.GetInstance().removeProperty(_config.getTargetId(), property.getText());

				try {
					_table.changeTableProperties(_config);
				} catch (Exception e) {
					e.printStackTrace();
				}

				currentWindow.close();
			}
		});
		
		Button Cancel = new Button(RemovePropertyPage, SWT.PUSH);
		Cancel.setText("Cancel");

		Cancel.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				currentWindow.close();
			}
		});

		RemovePropertyPage.pack();
		return super.createContents(parent);
	}

}
