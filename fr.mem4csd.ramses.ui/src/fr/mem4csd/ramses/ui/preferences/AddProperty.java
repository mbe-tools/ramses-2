/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.ui.preferences;

import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration;

public class AddProperty extends ApplicationWindow {
	private TableHelper _table;
	private String _target;
	private RamsesConfiguration _config;

	public AddProperty(Shell shell) {
		super(shell);
	}

	protected Control createContents(Composite parent) {
		Composite AddPropertyPage = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		AddPropertyPage.setLayout(layout);

		GridData data = new GridData();
		data.verticalAlignment = GridData.FILL;
		data.horizontalAlignment = GridData.FILL;
		AddPropertyPage.setLayoutData(data);
		
		Text property = new Text(AddPropertyPage, SWT.SINGLE | SWT.BORDER);
		property.setEditable(true);
		Text value = new Text(AddPropertyPage, SWT.SINGLE | SWT.BORDER);
		value.setEditable(true);
		property.setText("Property");
		value.setText("value");

		Button Ok = new Button(AddPropertyPage, SWT.PUSH);
		ApplicationWindow currentWindow = this;
		Ok.setText("Ok");

		Ok.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				_table.updateTable(_target, property.getText(), value.getText());
				try {
					_table.changeTableProperties(_config);
				} catch (Exception e) {
					e.printStackTrace();
				}
				currentWindow.close();
			}
		});
		
		Button Cancel = new Button(AddPropertyPage, SWT.PUSH);
		Cancel.setText("Cancel");

		Cancel.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				currentWindow.close();
			}
		});

		AddPropertyPage.pack();
		return super.createContents(parent);
	}

	public void setTarget(String target) {
		_target = target;
	}

	public void setTable(TableHelper table) {
		_table = table;
	}

	public void setConfig(RamsesConfiguration config) {
		_config = config;
	}
}
