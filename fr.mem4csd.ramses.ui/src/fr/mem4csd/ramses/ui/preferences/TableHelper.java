/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.ui.preferences;

import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel;

public class TableHelper {
	private RamsesPropertyMap _properties;
	private Table _table;
	Composite _composite;
	private String[] Names = {"Property", "Value", "Description"};
	private int style = SWT.SINGLE | SWT.BORDER | SWT.H_SCROLL| SWT.V_SCROLL| SWT.FULL_SELECTION;

	TableHelper(Composite composite, RamsesConfiguration config) {
		_properties = RamsesPropertyMap.GetInstance();
		_composite = composite;
	}


	private void addTableEditor(RamsesConfiguration config) {
		final TableEditor editor = new TableEditor(_table);
	    editor.horizontalAlignment = SWT.LEFT;
	    editor.grabHorizontal = true;
	    editor.minimumWidth = 50;
	    // editing the second column
	    final int EDITABLECOLUMN = 1;

	    _table.addSelectionListener(new SelectionAdapter() {
	      public void widgetSelected(SelectionEvent e) {
	      //  Clean up any previous editor control
	        Control oldEditor = editor.getEditor();
	        if (oldEditor != null)
	          oldEditor.dispose();

	        // Identify the selected row
	        TableItem item = (TableItem) e.item;

	        if (item == null)
	          return;

	        // The control that will be the editor must be a child of the
	        // Table
	        Text newEditor = new Text(_table, SWT.NONE);
	        newEditor.setText(item.getText(EDITABLECOLUMN));
	        
	        newEditor.addModifyListener(new ModifyListener() {
   	
		        public void modifyText(ModifyEvent me) {
		        	
	
		            Text text = (Text) editor.getEditor();
		            editor.getItem()
		                .setText(EDITABLECOLUMN, text.getText());
		            updateTable(config.getTargetId(), item.getText(0), text.getText());
		        }
		    });
		        
		        newEditor.selectAll();
		        newEditor.setFocus();
		        editor.setEditor(newEditor, item, EDITABLECOLUMN);
		      }
		 });
	}


	public void createTable (Composite composite, RamsesConfiguration config) {
		_table = new Table(composite,style);
		GridData grd = new GridData();
		grd.widthHint = 800;
		grd.heightHint = 80;
		grd.minimumHeight = 20;
		_table.setLayoutData(grd) ;
		
		_table.setLinesVisible(true);
	    _table.setHeaderVisible(true);

	    for (int i = 0; i < Names.length; i++) {
			org.eclipse.swt.widgets.TableColumn column1 = new org.eclipse.swt.widgets.TableColumn(_table, SWT.LEFT);
			column1.setText(Names[i]);
		}

		for (int i=0; i< 3; i++) {
	        _table.getColumn (i).pack ();
	    }

	    addTableEditor(config);
		_table.getColumn(0).setWidth(150);
		_table.getColumn(1).setWidth(200);
		_table.getColumn(2).setWidth(650);

	    if (config.getTargetId() != null) {
			try {
				changeTableProperties(config);

			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
	}


	public void changeTableProperties(RamsesConfiguration config) throws Exception {
		Map<String, String> properties = _properties.createOrFetchNecessaryPropertiesMap(config);

		if (_table.isDisposed()) {
			throw new Exception();
		}

		_table.removeAll();
		Set<String> targets = RamsesWorkflowViewModel.INSTANCE.getAvailableGeneratorNames();

		for (String key : properties.keySet()) {
			if (key != RamsesConfiguration.OUTPUT_DIR &&
			key != RamsesConfiguration.RUNTIME_PATH && !targets.contains(key)) {
				org.eclipse.swt.widgets.TableItem item = new org.eclipse.swt.widgets.TableItem(_table, SWT.NONE);
				item.setText(0, key);
				item.setText(1, properties.get(key));

				if (RamsesPropertyMap.GetInstance().descriptions.get(key) != null) {
					item.setText(2, RamsesPropertyMap.GetInstance().descriptions.get(key));
				}
			}
		}

		for (int i=0; i< 3; i++) {
	        _table.getColumn (i).pack ();
	    }

		_table.getColumn(0).setWidth(150);		
		_table.getColumn(1).setWidth(200);
		_table.getColumn(2).setWidth(650);
	}


	public void updateTable(String target_id, String key, String new_value) {
		_properties.updateTable(target_id, key, new_value);;
	}


	public Table getTable() {
		return _table;
	}


	public void addListener(SelectionListener selectionListener) {
		_table.addSelectionListener(selectionListener);
	}
}
