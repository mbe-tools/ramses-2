/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.ui.preferences;

import java.util.Map;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;

public class RamsesTargetPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

    public RamsesTargetPreferencePage()
    {
    	super(GRID);
    }
    
    @Override
    public void init(IWorkbench workbench) {
        // second parameter is typically the plug-in id
        setPreferenceStore(new ScopedPreferenceStore(InstanceScope.INSTANCE, "fr.mem4csd.ramses.core.RamsesTargetPreferencePage"));
        setDescription("Set target &install directory of:");
    }

	@Override
	protected void createFieldEditors() {
		
		Map<String, AadlToTargetBuildGenerator> generators = RamsesWorkflowViewModel.INSTANCE.getAvailableGeneratorsFromTargetId();
		for(String target: generators.keySet())
		{
			AadlToTargetBuildGenerator gen = generators.get(target);
			if(gen.validateTargetPath(URI.createURI("")))
				continue;
			Label label = new Label(getFieldEditorParent(), SWT.SEPARATOR | SWT.HORIZONTAL);
	        label.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 3, 1));

			DirectoryFieldEditor pathEditor = new DirectoryFieldEditor(target, generators.get(target).getTargetName()+":", getFieldEditorParent()); 
			addField(pathEditor);
		}
		Label label = new Label(getFieldEditorParent(), SWT.SEPARATOR | SWT.HORIZONTAL);
        label.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 3, 1));

	}

	@Override
	public boolean isValid() {
		
//		Map<String, AadlToTargetBuildGenerator> generators = RamsesWorkflowViewModel.instance.getAvailableGeneratorsFromTargetId();
//		for(String target: generators.keySet())
//		{
//			String value = Platform.getPreferencesService().
//				    getString("fr.mem4csd.ramses.core.RamsesTargetPreferencePage", target, null, null);
//			AadlToTargetBuildGenerator gen = generators.get(target);
//			if(gen.validateTargetPath(URI.createURI("")))
//				continue;
//			if(value == null || value.isEmpty())
//				continue;
//			if(!generators.get(target).validateTargetPath(URI.createFileURI(value)))
//				return false;
//		}
		return super.isValid();
	}
	
	@Override
	public String getErrorMessage() {
//		Map<String, AadlToTargetBuildGenerator> generators = RamsesWorkflowViewModel.instance.getAvailableGenerators();
//		String errorMsg = "";
//		for(String target: generators.keySet())
//		{
//			String value = Platform.getPreferencesService().
//				    getString("fr.mem4csd.ramses.core.RamsesTargetPreferencePage", target, null, null);
//			if(value == null)
//				continue;
//			if(!generators.get(target).validateTargetPath(URI.createFileURI(value)))
//				errorMsg += "Install directory for "+generators.get(target).getTargetName()+" is not valid\n";
//		}
//		if(!errorMsg.isEmpty())
//			return errorMsg;
		return super.getErrorMessage();
	}
	
//	@Override
//	public boolean performOk() {
//		return isValid();
//	}
}
