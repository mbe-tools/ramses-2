\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage[htt]{hyphenat}

\usepackage{graphicx}
\graphicspath{ {./figures/} }

\usepackage{hyperref}

\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}

\usepackage{glossaries}

\newcommand{\ramses}{$\mathcal{R}AMSES\ 2$}

\makeglossaries

\newglossaryentry{ramses}
{
    name=\ramses{},
    description={Refinement of AADL models for the Synthesis of Embedded Systems}
}

\newglossaryentry{osate}
{
    name=OSATE,
    description={Open Source AADL Toolsuite Environment, \url{https://osate.org/}}
}

\newglossaryentry{aadl}
{
    name=AADL,
    description={Architecture Analysis and Design Language, standardized by the Society of Automotive Engineering (SAE), is a modelling language for critical, real-time, and/or embedded systems.}
}

\newglossaryentry{host}
{
    name=host,
    description={Identifier of the machine on which you will run \ramses{}}
}

\newglossaryentry{target}
{
    name=target,
    description={Identifier of the machine (its operating system in fact) for which you want \ramses{} to synthesize code}
}

\newglossaryentry{aadl runtime services}
{
    name=AADL runtime services,
    description={as defined by the AADL standard document, runtime services implement the activation of AADL components, as well as communications among them.}
}

\newglossaryentry{local communications}
{
    name=local communications,
    description={Communications among AADL threads contained in the same AADL process component.}
}

\newglossaryentry{remote communications}
{
    name=remote communications,
    description={Communications among AADL threads contained in different AADL process component.}
}


% \usepackage{fourier} %% for warning (\danger) symbol

\usepackage{stackengine}
\usepackage{scalerel}
\usepackage{xcolor}

\newcommand\Warning[1][2ex]{%
  \renewcommand\stacktype{L}%
  \scaleto{\stackon[1.3pt]{\color{red}$\triangle$}{\tiny !}}{#1}%
}

\newcommand\Note[1][2ex]{%
  \renewcommand\stacktype{L}%
  \scaleto{\stackon[1.3pt]{\color{green}$\triangle$}{\tiny !}}{#1}%
}

\usepackage{listings}

\definecolor{mGreen}{rgb}{0,0.6,0}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\definecolor{mPurple}{rgb}{0.58,0,0.82}
\definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{CStyle}{
%%    backgroundcolor=\color{backgroundColour},   
    commentstyle=\color{mGreen},
    keywordstyle=\bfseries \color{magenta},
    numberstyle=\tiny\color{mGray},
    stringstyle=\color{mPurple},
    basicstyle=\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    language=C,
    morekeywords={int8_t, int16_t, int32_t, int64_t, uint8_t, uint16_t, uint32_t, uint64_t},
    morekeywords={\#include, \#define, \#ifdef}
}

\lstdefinelanguage{aadl}{
  morekeywords={in,out,package,end,bus,data,thread,port,group,process,processor,
system,memory,device,subprogram,private,event,property,set,applies,to,
units,type,implementation,parameter,reference,mode,final,initial,
transitions},
  morekeywords={extends,properties,features,annex,modes,connections,flows, subcomponents,calls,binding,states,initial,requires,access,prototypes,state, in, binding, computation, !<, !>, {**, **}},
  morekeywords={aadlinteger,aadlboolean,aadlstring,aadlfloat, behavior_specification},
  morecomment=[l]{--},
  morestring=*[d]{"}
}

\lstdefinestyle{AADLStyle}{
%%    backgroundcolor=\color{backgroundColour},   
    commentstyle=\color{mGreen},
    keywordstyle=\bfseries\color{magenta},
    numberstyle=\tiny\color{mGray},
    stringstyle=\color{mPurple},
    basicstyle=\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    language=aadl
}

\usepackage[
backend=biber,
style=alphabetic,
sorting=ynt
]{biblatex}

\addbibresource{Ramses_User_Manual.bib}

\title{\ramses{} User Manual}
\author{Etienne Borde and Dominique Blouin}
\date{July 2020}

\begin{document}

\maketitle

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{logo_ramses}
\end{figure}

\newpage

\tableofcontents
\newpage

\printglossary

\clearpage

\Warning[10ex] Add examples in RAMSES and pointers to these examples for each part of the documentation

\section{Introduction}

\gls{ramses} is a model refinement framework, based on the Architecture Analysis and Design Language (\gls{aadl}), aiming the synthesis of embedded systems source code. \gls{ramses} is a plugin of the Open Source \gls{aadl} Toolsuite Environment (\gls{osate}). Figure~\ref{RAMSES_general_context} provides an overview of RAMSES functionalities: AADL model transformations, analysis, and code generation.

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{ramses_illustration.jpg}
\caption{\ramses{}, a Framework for Computer Assisted Software Design}
\label{RAMSES_general_context}
\end{figure}

This document is the user manual of \gls{ramses}. To better understand this manual, readers are expected to have basic knowledge about:
\begin{itemize}
    \item the C programming language;
    \item the \gls{aadl} modeling language;
    \item the Eclipse integrated development environment and its \gls{osate} perspective.
\end{itemize}

This user manual is structured as follows:

\begin{itemize}
    
    \item [section \ref{before_starting}.] Installation procedure; list of supported \gls{target} platforms;
    
    \item [section \ref{overview}.] Overview of the code synthesis process; introductions of terminology.
    
    \item [section \ref{run_ramses}.] Configuration and launch procedure.
    
    \item [section \ref{user_code_integration}] How to write source code in order to integrate it with the source code synthesized by \ramses.
    
    \item [section \ref{input_model_requirements}] Specification of requirements for an input AADL model to be legit for \ramses. Includes general requirements, to be met by any model, platform specific requirements, to be met by a model used to synthesize code for a target platform, etc.
    
\end{itemize}

\clearpage
\section{Before starting}
\label{before_starting}

\subsection{Supported \gls{host} platforms}

Being mostly implemented in Java, \gls{ramses} can run on any host platform on which a Java virtual machine (with a version number $\geq$ 1.8) is running.

\Warning[6ex] Be aware that \gls{target} platforms supported by \gls{ramses} might not be usable on every \gls{host}. Refer to subsection~\ref{supported_target_platforms} for the list of supported target platforms and a link to their documentation. 

\Warning[6ex] This user manual only describes how to configure the supported \gls{target} platforms on Linux, which \textbf{we recommend to use as the \gls{host}}.


\subsection{Installation}

Before installing \gls{ramses}, you must install the \textbf{latest version} of \gls{osate}. See the installation procedure of \gls{osate} \href{https://osate.org/download-and-install.html}{here}. 

The install procedure of \gls{ramses} is documented \href{https://mem4csd.telecom-paristech.fr/blog/index.php/ramses/ramses-installation-and-use-with-osate/}{on this webpage.}

\subsection{Supported \gls{target} platforms}
\label{supported_target_platforms}
\gls{ramses} supports three types of \gls{target} platform:

\begin{enumerate}
    \item POSIX compliant operating systems;
    \item OSEK compliant operating systems;
    \item ARINC653 compliant operating systems.
\end{enumerate}

For each of these types, \gls{ramses} has been tested on a specific implementation:

\begin{enumerate}
    \item Linux for POSIX-compliant operating systems;
    \item nxtOSEK (\url{http://lejos-osek.sourceforge.net/}) for OSEK compliant operating systems;
    \item POK (\url{https://pok-kernel.github.io/}) for ARINC653 compliant operating systems.
\end{enumerate}

We describe how to install and configure nxtOSEK and POK on a Linux \gls{host} \href{https://mem4csd.telecom-paristech.fr/blog/index.php/ramses-install-targets/}{on this webpage}.

\subsection{Supported programming language}

\gls{ramses} can be used to integrate user code written using the C programming language. The way to integrate user code is described in section~\ref{user_code_integration}.

\clearpage
\section{\ramses{}, code synthesis process overview}
\label{overview}
%% show process (with input model, refined model, user source code, etc.)

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{ramses_acg_process.jpg}
\caption{\ramses{}, Model Transformation, Code Generation, and User Code Integration}
\label{ramses_acg_process}
\end{figure}

Figure~\ref{ramses_acg_process} illustrates the main components of \gls{ramses}, as well as the artefacts they use and produce. On the left part of the figure, user's artefacts are listed: the input AADL model, and the associated user code. In section~\ref{user_code_integration}, we explain how to write the user code whereas the section~\ref{input_model_requirements} specifies what the input AADL model should contain. \gls{ramses} transforms the input AADL model into a (set of) refined AADL model(s), which can be analysed by AADL analysis tools. By producing and analysing a (set of) refined AADL model(s), the objective is to include the generated code, data, and associated runtime services in the analysis. This reduces the gap between the analysis results and generated code. Interested users can read our publication at RSP 2014~\cite{RSP2014} for more information on this topic. The refined model is then used by RAMSES to generate source code and associated make files. The generated code is then compiled with the user code to produce the executable code.

\clearpage
\section{Configuration and launch}
\label{run_ramses}

\subsection{\ramses{} preference page}
\label{preference_page}

The preference page of \gls{ramses} should be used to define the install directory of the targets you plan to use. To open the preference page of \gls{ramses}, click on "Window $\rightarrow$ Preferences". The preference page of RAMSES is part of the listed preference pages on the left hand side. The preference page can also be opened from the property page, as explained in the next subsection.

\subsection{\ramses{} property page}
\label{property_page}
To open the configuration page of \gls{ramses}, right click on your AADL project and select ``Properties'' (or select the project on select in the menu bar: ``Project $\rightarrow$ Properties''). On the left side of the project property page, click on RAMSES.

The RAMSES property page appears on the right side.  It contains a link to the preference page defined above. It also contains different fields to fill in:

\begin{itemize}
    \item output directory: this is the location where \gls{ramses} will store refined models, generated source code, build files, and binary code.
    \item target selection: chose the \gls{target} patform you want to synthesize code for.
    \item target installation directory: if using Linux or All as a target, you can skip this. Otherwise, select the root directory of the target operating system (pok or nxtOSEK). For pok, this directory should contain the configuration file in \texttt{misc/config.mk}; for nxtOSEK, the root directory should contain the \texttt{ecrobot} subdirectory.
    \item properties table: when selecting a target, additional configuration properties may become available. If this is the case, the corresponding properties are listed in the property table, along with their description. Users can then define property values as strings.
    \item expand runtime share resources: check this box if you want to proceed to a timing analysis of the refined model and take into account the time spent in critical sections of the \gls{aadl runtime services}.
\end{itemize}

The configuration is checked when saved and an error message appears if the configuration is incorrect.

\subsection{\ramses{} launch procedure}

\gls{ramses} can be launched either from the outline view of OSATE, or from an instance model. Both procedures are described bellow.

\paragraph{Launch from the outline view.} By default, the outline view of \gls{osate} appears on the right side. It represents the content of an AADL package as an unfold-able tree of AADL elements. In the outline view, select the AADL system implementation for which you want to synthesize code. 

\Warning[6ex] the selected system implementation, and all the AADL elements it contains, must conform to the requirements expressed in section~\ref{input_model_requirements}.

Right click on the system implementation in the outline view, and click on ``Launch RAMSES". If the AADL project containing the selected system implementation is not configured properly, the project's property page is automatically open. See section~\ref{property_page} for more information about the different fields of the property page.

\paragraph{Launch from an instance model.} From the outline view, you can instantiate a system implementation: right-click on the system implementation of your choice and click on ``Instantiate". As a result, a directory named \texttt{instances} is created in your AADL project with an instance model in it (the file name of the instance model ends with an \texttt{aaxl2} extension).\\

Select the instance model file, then you can:
\begin{itemize}
    \item select the ``RAMSES 2'' menu in the menu bar (top of the Eclipse window by default) and click on ``Launch RAMSES".
    \item [OR]
    \item click on the RAMSES icon, present among the set of icons bellow the menu bar.
\end{itemize}

\clearpage
\section{Illustrative examples}
\label{illustrative_examples}
Once installed, \gls{ramses} comes with a set of illustrative examples. They are accessible by clicking on "File $\rightarrow$ New $\rightarrow$ Examples...". Then look for the RAMSES category. A step-by-step tutorial to execute one of these examples is available \href{https://mem4csd.telecom-paristech.fr/blog/index.php/ramses/ramses-generate-code-from-an-example/}{here}. Provided examples are listed bellow.\\

For Linux:
\begin{itemize}
    \item RAMSES Sampled Communications Example for Linux
    \item RAMSES Ping Sockets Example for Linux
    \item RAMSES Ping MQTT Example for Linux
    \item RAMSES Multi-mode threads Example for Linux
    \item RAMSES mine pump Example for Linux
\end{itemize}

For nxtOSEK:
\begin{itemize}
  \item RAMSES Sampled Communications Example for nxtOSEK
  \item RAMSES Line Follower Example for NxtOSEK
\end{itemize}

For POK:
\begin{itemize}
    \item RAMSES Sampled Communications Example for POK
\end{itemize}

For Ev3Dev:
\begin{itemize}
  \item RAMSES Sampled Communications Example for Ev3dev
\end{itemize}
  
For multi-targets:
\begin{itemize}
    \item RAMSES multi-target Example
\end{itemize}

\clearpage
\section{User code integration}
\label{user_code_integration}

In this section, we show how to write source code and AADL components in order to integrate user code in the code synthesized by \gls{ramses}. The section is divided into two subsection: fisrt, how to integrate legacy code; second, how to write user code containing interations with AADL components features (ports in particular).

\Note[6ex] Note that these two variants correspond to the two possible values of the Code\_Generation\_Properties::Convention property. The Legacy value of this property is to use following the guidelines of subsection~\ref{legacy_convention}; the AADL value of this property is to use following the guidelines of subsection~\ref{aadl_convention}. Requirements on the use of this property are defined in section~\ref{input_model_requirements}. 

%% Integrating source code that has not been written along with the input AADL model, or integrating source code that "matches" the AADL model.

\subsection{User code: legacy integration}
\label{legacy_convention}
In this subsection, we focus on the integration of legacy code. The motivation is to model existing code, written independently of the input AADL model, and to represent this code into an AADL model. This model can then be composed with other AADL models to define the AADL system implementation from which \gls{ramses} will generate code.

Modelling legacy code also requires to model legacy data types. The former relies on properties of the core AADL standard and the standard code generation annex; the latter relies on the standard data modeling annex.

\paragraph{Legacy data types modelling.} \Note[3ex] A good way to use basic data types (int, unsigned int, etc.) is to import and use data types defined in the Base\_Types package.

The set of useful properties for modeling data types are listed bellow. They are to be attached to AADL data components.

From the AADL core standard:
\begin{itemize}
    \item Source\_Text: list of files necessary to include when compiling the legacy code that uses the existing data type. The path provided for a file must either be an absolute path, a relative path from the AADL model, or a relative path from one of the included directory (see generated make files for more details on this last point). The parent directory of the file(s) with a .h extension is (are) added to the list of included directories.
    \item Source\_Name (optional): name of the data type in the source files. If not provided, the name of the aadl data component (as defined in the code generation annex) is used.
\end{itemize}

From the standard data modeling annex, all the properties are relevant to model data types. We invite interested readers to read the data modeling annex for more detailed information.

\begin{figure}[h!]
  \begin{lstlisting}[style=AADLStyle]
    data Int
      properties
        Data_Model::Data_Representation => integer;
    end Int;

    data Color
      properties
        Data_Model::Data_Representation => Enum;
        Data_Model::Enumerators => ("NXT_COLOR_Black",
                                    "NXT_COLOR_RED",
                                    "NXT_COLOR_BLUE");
        Source_Name => "COLOR_TYPE";
        Source_Text => ("ecrobot_interface.h");
    end Color;
  \end{lstlisting}
%\centering
\caption{AADL model of existing data types used in legacy source code}
\label{legacy_data_types}
\end{figure}


Figure~\ref{legacy_data_types} illustrate the use of these different properties to model an integer data type, and an enumeration. Both will be used in the example of legacy source code integration bellow. This model means that Int is an integer and Color is an enumeration defined as COLOR\_TYPE in ecrobot\_interface.h which is located in one of the directories of the nxtOSEK target platform.

\paragraph{Legacy source code modelling.} The set of useful properties for legacy code modeling are listed bellow. They are to be attached to AADL subprograms modelling functions in the user code.

From the AADL core standard:
\begin{itemize}
    \item Source\_Language (optional): list of programming languages used to implement the function. Only supported value for \gls{ramses} is (C).
    \item Source\_Text: list of files necessary to include/compile when integrating the legacy code into the synthesized code. The path provided for a file must either be an absolute path, a relative path from the AADL model, or a relative path from one of the included directory (see generated make files for more details on this last point). The extension of the file(s) is (are) used to decide if the source file(s) with a .c extension is (are) compiled or if the parent directory of the file(s) with a .h extension is (are) added to the list of included directories when compiling source files. 
    \item Source\_Name (optional): name of the function in the source code. If not provided, the name of the aadl subprogram (as defined in the code generation annex) is used.
\end{itemize}

From the standard code generation annex:
\begin{itemize}
    \item Code\_Generation\_Properties::Return\_Parameter; by default, the value of this property is false; set to true in case you need to represent a return parameter of your function. It can only be associated to one of the parameters and this parameter must be an out parameter.
    \item Code\_Generation\_Properties::Parameter\_Usage; this allows to specify if the data passed as parameter of the function is the data value or a reference to the data. 
\end{itemize}


\begin{figure}[h]
  \begin{lstlisting}[style=AADLStyle]
    subprogram compute_pid
      features
        currentColor: in parameter Color;
        colorToFollow: in parameter Color;
        angle: out parameter Int;
      properties
        Source_Language => (C);
        Source_Text => ("robot.h", "robot.c");
        Source_Name => "computePID";
     end compute_pid;
  \end{lstlisting}
%\centering
\caption{AADL model of an existing function in legacy source code}
\label{legacy_function_model}
\end{figure}

Figure~\ref{legacy_function_model} illustrates the use of these properties to model an existing C function called computePID in a ``robot.c" source file located next to the AADL model. The signature of the function is :\\

\begin{figure}[h]
\hspace*{1cm}
\begin{lstlisting}[style=CStyle]
  void computePID(Color currentColor,
                  Color colorToFollow,
                  int * angle);
\end{lstlisting}
\end{figure}

\subsection{Writing AADL specific code}
\label{aadl_convention}

The main use-case for writing AADL specific code is to be able to interact with AADL components features in source code; for instance to manage the access to data queued in the input event data ports, or to interact with ports (\textit{e.g.} send outputs) only under specific conditions detected at runtime. \gls{ramses} will apply the code integration method defined in the code generation annex if (i) the AADL value is associated to the Convention property, or if (ii) the subprogram has event or event data ports. Otherwise, the default semantics of AADL applies: read inputs, execute, write outputs.

Here is the list of signatures of functions (defined in \texttt{aadl\_runtime\_services.h}) user code can use:
\begin{itemize}
    \item \texttt{error\_code\_t Put\_Value(port\_reference\_t * port, void * value);}
    \item \texttt{error\_code\_t Send\_Output(port\_reference\_t * port);}
    \item \texttt{error\_code\_t Next\_Value(port\_reference\_t * port, void * dst);}
    \item \texttt{error\_code\_t Get\_Value(port\_reference\_t * port, void * dst);}
    \item \texttt{error\_code\_t Receive\_Input(port\_reference\_t * port);}
    \item \texttt{error\_code\_t Get\_Count(port\_reference\_t * port, uint16\_t * count\_res);}
    \item \texttt{error\_code\_t Updated(port\_reference\_t * port, uint8\_t * fresh\_flag);}
    \item \texttt{error\_code\_t Await\_Mode(thread\_config\_t * config);}
    \item \texttt{error\_code\_t Await\_Dispatch(thread\_config\_t * global\_q);}
\end{itemize}

Type \texttt{error\_code\_t} is defined as an enumeration, admiting the following values: \texttt{RUNTIME\_OK, RUNTIME\_EMPTY\_QUEUE, RUNTIME\_FULL\_QUEUE, RUNTIME\_LOCK\_ERROR, RUNTIME\_OUT\_OF\_BOUND, RUNTIME\_INVALID\_PARAMETER, RUNTIME\_INVALID\_SERVICE\_CALL, RUNTIME\_MISSING\_INPUT, RUNTIME\_INIT\_HYPERPERIOD}.

We provide an example of AADL model in figure~\ref{aadl_function_model}, which is rather simple; figure~\ref{legacy_function_model} provides source code with an example of interaction with a subprogram port. As you can see, because the AADL subprogram has an event port, the AADL convention mapping (defined in the code generation annex) is applied: the \texttt{C} function has a unique parameter, which is of type \texttt{\_\_check\_obstacle\_context}. This context is actually a generated data structure, generated by \gls{ramses} in a file name \texttt{gtypes.h}. This data structure is composed of fields that matches the set of features of the AADL subprogram: \texttt{portId}, of type corresponding to \texttt{nxt\_sensor\_port} (using the same type mapping as for the legacy code) and \texttt{obstacle\_detection} of type \texttt{port\_reference\_t}. In its code, a user can then use the runtime services defined in the C code of \gls{ramses}. In this example, \texttt{\_\_aadl\_send\_output} is used to send an event (the second parameter value can be ignored since we are just sending an event).

\begin{figure}[h]
  \begin{lstlisting}[style=AADLStyle]
    subprogram Check_Obstacle
      features
        portId: in parameter nxt_sensor_port;
        obstacle_detected: out event port;
      properties
        Source_Language => (C);
        Source_Text => ("source.h", "source.c");
    end Check_Obstacle;
  \end{lstlisting}
%\centering
\caption{AADL model of a subprogram with an output event port}
\label{aadl_function_model}
\end{figure}

\begin{figure}[h]
\begin{lstlisting}[style=CStyle]
  #define MINDISTANCE 20
  char obstacle_detected = 0;

  void check_pbstacle(__check_obstacle_context * ctx) {
    uint32_t dist = ecrobot_get_sonar_sensor(ctx->portId);
    obstacle_detected = (dist>0 && dist <= MIN_DISTANCE);
    if(obstacle_detected) {
      // Put_Value on out event port
      Put_Value(ctx->obstacle_detected, NULL);
      // Send_Output
      Send_Output(ctx->obstacle_detected);
    }
  }
\end{lstlisting}
\caption{User code with a runtime service call to send output in case of close obstacle}
\label{legacy_function_model}
\end{figure}

\clearpage
\section{Input model requirements}
\label{input_model_requirements}

In this section, we describe what an input AADL model must contain (or not) in order to be usable with RAMSES. Obviously, users should also make sure their models respect the naming, legality, and consistency rules as defined in the AADL standard.

\subsection{General requirements}

\paragraph{Root system instance} 

\begin{itemize}
    \item From the root system, at least one process and one processor component should be defined, possibly traversing subcomponents of the root system. In other words, there must exist at least one process component instance and one processor component instance in the instance model.
\end{itemize}

\paragraph{Processor and Virtual Processor component instance} 
\begin{itemize}
    \item The \texttt{RAMSES\_Properties::Target} property must be associated to every Processor component instance. It is a string that identifies the \gls{target} platform of the AADL processor (``Linux", ``nxtOSEK", or ``POK"). 
    \item The \texttt{Scheduling\_Protocol} property must be associated to every Processor or Virtual Processor component instance.
    \item Every Virtual Processor component instance must be either (i) a subcomponent of a processor instance, or (ii) a subcomponent of a virtual processor component instance satisfying the current requirement, or (iii) bound (using the \texttt{Actual\_Processor\_Binding} property) to a processor component instance, or (iv) bound (using the \texttt{Actual\_Processor\_Binding} property) to a virtual processor component instance satisfying the current requirement.
\end{itemize}

\paragraph{Process component instance} 
\begin{itemize}
    \item Every process component instance must be bound (using the \texttt{Actual\_Processor\_Binding} property) to exactly one processor or virtual processor component instance.
    \item Every process component instance must have at least one thread subcomponent, possibly traversing subcomponents (\textit{thread groups}).
\end{itemize}

\paragraph{Thread component instance}
\begin{itemize}
    \item Every thread component instance must have a dispatch protocol: make sure the \texttt{Dispatch\_Protocol} property is associated to every thread component instance.
    \item Every thread component instance with a \texttt{Periodic, Sporadic, Timed,} or \texttt{Hybrid} dispatch protocol must have a \texttt{Period} property assigned to it.
    \item Every thread component instance must have a ``behavior": either the thread component instance contains a behavior annex subclause, or the \texttt{Compute\_Entrypoint\_Call\_Sequence} is associated to the thread component instance. Note \Warning[3ex]{} the behavior annex is only partially supported by \gls{ramses}.
\end{itemize}

\paragraph{Feature instances}
\begin{itemize}
    \item Every feature instance must be part of a connection instance and/or used of at least one mode transition condition.
    \item Every feature instance of kind ``feature group" must contain at least one feature instance.
    \item Every feature instance of kind ``data port" or ``event data port" must have a data classifier, and the data classifier must be attached a programming language data type (using the data modeling annex or the \texttt{Base\_Types} package, see section~\ref{legacy_data_types} for more information on how to do this).
\end{itemize}

\paragraph{Subprogram components and calls}

\begin{itemize}
    \item Every subprogram referenced in a subprogram call must either have a behavior annex subclause, or a \texttt{Source\_Text} property attached to it. See section~\ref{legacy_function_model} for more information about the meaning of this property.
    \item Every feature of the called subprogram must be connected to another feature.
    \item The default value for the Code\_Generation\_Properties::Convention property is Legacy. When the Legacy value is assigned to the subprogram, it can only have parameters (no port or data access). It cannot extend subprograms (or extend subprograms that extend subprograms...) having parameters because the ordering of parameters in the C code becomes more difficult to determine for users. If the AADL value is used, the \texttt{Parameter\_Usage} value attached to parameters is ignored.
\end{itemize}

\subsection{Target specific requirements}

\subsubsection{Linux}

\begin{itemize}
    \item Virtual processor components are not supported.
    \item The supported scheduling protocols on Linux, for AADL processors, are: \texttt{POSIX\_1003\_HIGHEST\_PRIORITY\_FIRST\_PROTOCOL, RMS, DMS, FixedTimeline}. \Warning[3ex]{} When using a \texttt{FixedTimeline} scheduling protocol, threads priority value are ignore. When using fixed priority scheduling, priority values are used as a reference. This means users must pre-compute priority values even when using a \texttt{RMS} or \texttt{DMS} scheduling.
\end{itemize}

\subsubsection{nxtOSEK}

\begin{itemize}
    \item Virtual processor components are not supported.
    \item The supported scheduling protocols on nxtOSEK, for AADL processors, are: \texttt{POSIX\_1003\_HIGHEST\_PRIORITY\_FIRST\_PROTOCOL, RMS,} and \texttt{DMS.}
    \item Every processor component instance must have the following properties : \texttt{OSEK::SystemCounter\_MaxAllowedValue}, \texttt{OSEK::SystemCounter\_TicksPerBase}, and \texttt{OSEK::SystemCounter\_MinCycle}. These are properties for the configuration of the operating system; see the OSEK standard for the definition of these configuration parameters. 
\end{itemize}

\subsubsection{POK}

\begin{itemize}
    \item Virtual processor components are use to represent the module partitions environment (see the standard ARINC653 annex).
    \item The supported scheduling protocol on POK, for AADL processors, is: \texttt{ARINC653}.
    \item The supported scheduling protocols on POK, for AADL virtual processors, are: \texttt{RMS}, \texttt{POSIX\_1003\_HIGHEST\_PRIORITY\_FIRST\_PROTOCOL}, \texttt{Round\_Robin\_Protocol}, and \texttt{FixedTimeline}.
    \item Every processor component instance must have the following properties : \texttt{POK::Architecture}, \texttt{POK::BSP}, \texttt{ARINC653::Module\_Major\_Frame}, \texttt{ARINC653::Module\_Schedule}. See the documentation of POK and the AADL ARINC653 annex for the definition of these configuration parameters.
\end{itemize}

\subsection{Requirements on local communications}

\gls{local communications}:
\begin{itemize}
    \item must not be bound to a bus or to a virtual bus using the \texttt{Actual\_Connection\_Binding} property.
    \item must connect thread features of components.
    \item can be configures using the \texttt{Timing} property, with values \texttt{Immediate, Sampled,} or \texttt{Delayed}. The default value is \texttt{Sampled} and no restriction apply to it. 
    \\\Warning[3ex]{} using the \texttt{Sampled} property value means the underlying runtime will use locks. If you want to exhibit the use of locks in the refined model, use the appropriate option on the \gls{ramses} property page (see section~\ref{property_page}). \\\Warning[3ex]{} When using the \texttt{Immediate} property value, you should make sure the scheduling configuration you use is compatible with this communication type: the thread with the output port of the connection should have a higher (or equal) frequency than the thread with the input port. The thread with the output port should also have a strictly higher priority (thus a smaller deadline or period in case of RMS and DMS respectively); except if you use a FixedTimeLine scheduling protocol (priorities are ignored in this case and the task set can be transformed into a simple directed acyclic graph of tasks with precedency constraints). \\\Warning[3ex]{}Using the \texttt{Delayed} property value, a deadline can be provided for threads to compute the date of visibility of data on the output port(s). If not provided, \gls{ramses} considers implicit deadlines (deadline equals period).
\end{itemize}


\subsection{Requirements on remote communications}

\gls{remote communications} may be connections (i) among processes bound to the same processor, (ii) among processes bound to different processors, or (iii) among processes and virtual busses.

\subsubsection{Among processes bound to the same processor}

This is only supported on POK, using the sampling and queueing services ot ARINC653; use the localhost and sockets to implement this communication type on Linux.



\subsubsection{Among processes bound to the different processors}

\gls{ramses} supports two types of communication through message queues. Message queue communications are modelled with a connection among ports of processes bound to different processors. Such a connection must:
\begin{itemize}
    \item eventually connect features of thread components, or be involved in a mode transition in the destination process.
    \item connect ports for which the exchanged data size is known (using the \texttt{Data\_Size} property or the data modeling annex).
    \item be bound to exactly one bus which connects the source and destination processors, or to a virtual bus bound to a bus that connects the source and target processor, or to a virtual bus bound to a virtual bus bound to a bus that connects the source and target processor, etc.
    \item use the default Timing property (Sampling).
\end{itemize}

The bus or virtual bus a remote connection is bound to must identify a protocol (e.g. sockets) and should be configured accordingly. Message queues can be configured using sockets (tcp or udp) for the Linux target and pok-qemu-remote for the pok target.

The AADL property used to identify the communication protocol is \texttt{RAMSES\_properties::Communication\_Protocol}. It applies to bus or virtual bus components. It is an enum, and its potential values are \texttt{SOCKETS\_TCP}, \texttt{SOCKETS\_UDP}, and \texttt{POK\_QEMU} for message queues. 

When using Sockets (\texttt{SOCKET\_TCP} or \texttt{SOCKET\_UDP}) the bus accesses connected to the bus (or virtual bus) supporting the remote connection must have an address, and a port. The AADL property used to identify the address is \texttt{RAMSES\_properties::Communication\_Address}. The AADL property used to identify the port is \texttt{RAMSES\_properties::Communication\_Port}. Both are of type string.

Code generation for remote communications on POK-QEMU are only supported in a private plugin of RAMSES; contact the authors of this manual if you are interested.

%% change to string???

\subsubsection{Among processes and virtual busses}

Publish subscribe communications are modelled with a connection among ports of processes and ports of a bus or virtual bus.

Such a connection must:
\begin{itemize}
    \item on the process side, be connected to features of thread components, or be involved in a mode transition in the destination process.
    \item connect ports for which the exchanged data size is known (using the \texttt{Data\_Size} property or the data modeling annex).
    \item exchanged data type should be associated with a topic identifier (the \texttt{RAMSES::Communication\_Topic} property).
    \item use the default Timing property (Sampling).
    \item for a given virtual bus connected to a recipient process, there can only be one input port per topic. This is not really a restriction as the input port can then be connected to different threads of the recipient process. Given a recipient process, if the same topic is provided through N different MQTT networks, messages received on one port will be transmitted to the N input ports of the recipient process (they all subscribed to the same topic).
\end{itemize}

The virtual busses of such a remote connections must identify a protocol (e.g. MQTT) and should be configured accordingly. Publish/subscribe can be configured using MQTT for the Linux target.

The AADL property used to identify the communication protocol is \texttt{RAMSES\_properties::Communication\_Protocol}. It applies to bus or virtual bus components. It is an enum, and its values must be \texttt{MQTT}, for publish subscribe.

When using MQTT protocol, the connected bus should provide a host name and a port. If none is given, default values are used: localhost, 1883. Host name and port can be provided using the \texttt{RAMSES\_properties::Communication\_Host} and \texttt{RAMSES\_properties::Communication\_Port} properties respectively.

\subsection{Requirements on modes specification}

\gls{ramses} supports modes and mode switches to represent:
\begin{itemize}
    \item activation/deactivation of thread components;
    \item mode-specific ports and data access connections;
\end{itemize}

subject to the following constraints:

\begin{itemize}
    \item modes state machine can only be defined in process implementation components.
    \item the source of a mode change is an input event port connected to an output event port of a thread or to one of the predefined processor (or system representing a processor) ports (timing\_failure\_event, hyperperiod) as defined for the modeling of mixed-criticality directed acyclic graphs of tasks.
\end{itemize}

\printbibliography

\end{document}
