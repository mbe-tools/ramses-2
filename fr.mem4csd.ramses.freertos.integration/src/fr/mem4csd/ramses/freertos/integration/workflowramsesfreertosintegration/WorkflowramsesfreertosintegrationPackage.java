/**
 */
package fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration;

import fr.mem4csd.ramses.freertos.workflowramsesfreertos.WorkflowramsesfreertosPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationFactory
 * @model kind="package"
 * @generated
 */
public interface WorkflowramsesfreertosintegrationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowramsesfreertosintegration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/ramses/workflowramsesfreertosintegration";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workflowramsesfreertosintegration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowramsesfreertosintegrationPackage eINSTANCE = fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.impl.WorkflowramsesfreertosintegrationPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.impl.CodegenFreertosIntegrationImpl <em>Codegen Freertos Integration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.impl.CodegenFreertosIntegrationImpl
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.impl.WorkflowramsesfreertosintegrationPackageImpl#getCodegenFreertosIntegration()
	 * @generated
	 */
	int CODEGEN_FREERTOS_INTEGRATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__NAME = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__DESCRIPTION = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__ENABLED = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__ENABLED;

	/**
	 * The feature id for the '<em><b>Debug Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__DEBUG_OUTPUT = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__DEBUG_OUTPUT;

	/**
	 * The feature id for the '<em><b>Aadl Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__AADL_MODEL_SLOT = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__AADL_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Trace Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__TRACE_MODEL_SLOT = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__TRACE_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__OUTPUT_DIRECTORY = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__OUTPUT_DIRECTORY;

	/**
	 * The feature id for the '<em><b>Target Install Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__TARGET_INSTALL_DIR = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__TARGET_INSTALL_DIR;

	/**
	 * The feature id for the '<em><b>Include Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__INCLUDE_DIR = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__INCLUDE_DIR;

	/**
	 * The feature id for the '<em><b>Core Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__CORE_RUNTIME_DIR = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__CORE_RUNTIME_DIR;

	/**
	 * The feature id for the '<em><b>Target Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__TARGET_RUNTIME_DIR = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__TARGET_RUNTIME_DIR;

	/**
	 * The feature id for the '<em><b>Aadl To Source Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__AADL_TO_SOURCE_CODE = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__AADL_TO_SOURCE_CODE;

	/**
	 * The feature id for the '<em><b>Aadl To Target Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__AADL_TO_TARGET_CONFIGURATION = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__AADL_TO_TARGET_CONFIGURATION;

	/**
	 * The feature id for the '<em><b>Aadl To Target Build</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__AADL_TO_TARGET_BUILD = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__AADL_TO_TARGET_BUILD;

	/**
	 * The feature id for the '<em><b>Transformation Resources Pair List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__TRANSFORMATION_RESOURCES_PAIR_LIST = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__TRANSFORMATION_RESOURCES_PAIR_LIST;

	/**
	 * The feature id for the '<em><b>Target Properties</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__TARGET_PROPERTIES = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__TARGET_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Progress Monitor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__PROGRESS_MONITOR = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__PROGRESS_MONITOR;

	/**
	 * The feature id for the '<em><b>Uri Converter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__URI_CONVERTER = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__URI_CONVERTER;

	/**
	 * The feature id for the '<em><b>Target Install Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__TARGET_INSTALL_DIRECTORY_URI = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__TARGET_INSTALL_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Output Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__OUTPUT_DIRECTORY_URI = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__OUTPUT_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Core Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__CORE_RUNTIME_DIRECTORY_URI = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__CORE_RUNTIME_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Target Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__TARGET_RUNTIME_DIRECTORY_URI = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__TARGET_RUNTIME_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Include Directory URI List</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__INCLUDE_DIRECTORY_URI_LIST = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS__INCLUDE_DIRECTORY_URI_LIST;

	/**
	 * The feature id for the '<em><b>Mqtt Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__MQTT_RUNTIME_DIR = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sockets Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__SOCKETS_RUNTIME_DIR = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Mqtt Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sockets Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Codegen Freertos Integration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION_FEATURE_COUNT = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int CODEGEN_FREERTOS_INTEGRATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION___CHECK_CANCELED__IPROGRESSMONITOR = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Get Model Transformation Traces List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION___GET_MODEL_TRANSFORMATION_TRACES_LIST = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS___GET_MODEL_TRANSFORMATION_TRACES_LIST;

	/**
	 * The operation id for the '<em>Get Target Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION___GET_TARGET_NAME = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS___GET_TARGET_NAME;

	/**
	 * The number of operations of the '<em>Codegen Freertos Integration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FREERTOS_INTEGRATION_OPERATION_COUNT = WorkflowramsesfreertosPackage.CODEGEN_FREERTOS_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration <em>Codegen Freertos Integration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Codegen Freertos Integration</em>'.
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration
	 * @generated
	 */
	EClass getCodegenFreertosIntegration();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getMqttRuntimeDir <em>Mqtt Runtime Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mqtt Runtime Dir</em>'.
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getMqttRuntimeDir()
	 * @see #getCodegenFreertosIntegration()
	 * @generated
	 */
	EAttribute getCodegenFreertosIntegration_MqttRuntimeDir();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getSocketsRuntimeDir <em>Sockets Runtime Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sockets Runtime Dir</em>'.
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getSocketsRuntimeDir()
	 * @see #getCodegenFreertosIntegration()
	 * @generated
	 */
	EAttribute getCodegenFreertosIntegration_SocketsRuntimeDir();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getMqttRuntimeDirectoryURI <em>Mqtt Runtime Directory URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mqtt Runtime Directory URI</em>'.
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getMqttRuntimeDirectoryURI()
	 * @see #getCodegenFreertosIntegration()
	 * @generated
	 */
	EAttribute getCodegenFreertosIntegration_MqttRuntimeDirectoryURI();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getSocketsRuntimeDirectoryURI <em>Sockets Runtime Directory URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sockets Runtime Directory URI</em>'.
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getSocketsRuntimeDirectoryURI()
	 * @see #getCodegenFreertosIntegration()
	 * @generated
	 */
	EAttribute getCodegenFreertosIntegration_SocketsRuntimeDirectoryURI();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkflowramsesfreertosintegrationFactory getWorkflowramsesfreertosintegrationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.impl.CodegenFreertosIntegrationImpl <em>Codegen Freertos Integration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.impl.CodegenFreertosIntegrationImpl
		 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.impl.WorkflowramsesfreertosintegrationPackageImpl#getCodegenFreertosIntegration()
		 * @generated
		 */
		EClass CODEGEN_FREERTOS_INTEGRATION = eINSTANCE.getCodegenFreertosIntegration();

		/**
		 * The meta object literal for the '<em><b>Mqtt Runtime Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_FREERTOS_INTEGRATION__MQTT_RUNTIME_DIR = eINSTANCE.getCodegenFreertosIntegration_MqttRuntimeDir();

		/**
		 * The meta object literal for the '<em><b>Sockets Runtime Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_FREERTOS_INTEGRATION__SOCKETS_RUNTIME_DIR = eINSTANCE.getCodegenFreertosIntegration_SocketsRuntimeDir();

		/**
		 * The meta object literal for the '<em><b>Mqtt Runtime Directory URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_FREERTOS_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI = eINSTANCE.getCodegenFreertosIntegration_MqttRuntimeDirectoryURI();

		/**
		 * The meta object literal for the '<em><b>Sockets Runtime Directory URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_FREERTOS_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI = eINSTANCE.getCodegenFreertosIntegration_SocketsRuntimeDirectoryURI();

	}

} //WorkflowramsesfreertosintegrationPackage
