/**
 */
package fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationPackage
 * @generated
 */
public interface WorkflowramsesfreertosintegrationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowramsesfreertosintegrationFactory eINSTANCE = fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.impl.WorkflowramsesfreertosintegrationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Codegen Freertos Integration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Codegen Freertos Integration</em>'.
	 * @generated
	 */
	CodegenFreertosIntegration createCodegenFreertosIntegration();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorkflowramsesfreertosintegrationPackage getWorkflowramsesfreertosintegrationPackage();

} //WorkflowramsesfreertosintegrationFactory
