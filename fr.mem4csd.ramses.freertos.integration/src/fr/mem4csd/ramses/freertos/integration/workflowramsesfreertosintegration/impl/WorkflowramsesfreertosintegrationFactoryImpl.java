/**
 */
package fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.impl;

import fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowramsesfreertosintegrationFactoryImpl extends EFactoryImpl implements WorkflowramsesfreertosintegrationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WorkflowramsesfreertosintegrationFactory init() {
		try {
			WorkflowramsesfreertosintegrationFactory theWorkflowramsesfreertosintegrationFactory = (WorkflowramsesfreertosintegrationFactory)EPackage.Registry.INSTANCE.getEFactory(WorkflowramsesfreertosintegrationPackage.eNS_URI);
			if (theWorkflowramsesfreertosintegrationFactory != null) {
				return theWorkflowramsesfreertosintegrationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WorkflowramsesfreertosintegrationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramsesfreertosintegrationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WorkflowramsesfreertosintegrationPackage.CODEGEN_FREERTOS_INTEGRATION: return createCodegenFreertosIntegration();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodegenFreertosIntegration createCodegenFreertosIntegration() {
		CodegenFreertosIntegrationImpl codegenFreertosIntegration = new CodegenFreertosIntegrationImpl();
		return codegenFreertosIntegration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramsesfreertosintegrationPackage getWorkflowramsesfreertosintegrationPackage() {
		return (WorkflowramsesfreertosintegrationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WorkflowramsesfreertosintegrationPackage getPackage() {
		return WorkflowramsesfreertosintegrationPackage.eINSTANCE;
	}

} //WorkflowramsesfreertosintegrationFactoryImpl
