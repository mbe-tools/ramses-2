/**
 */
package fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration;

import fr.mem4csd.ramses.freertos.workflowramsesfreertos.CodegenFreertos;

import org.eclipse.emf.common.util.URI;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Codegen Freertos Integration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getMqttRuntimeDir <em>Mqtt Runtime Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getSocketsRuntimeDir <em>Sockets Runtime Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getMqttRuntimeDirectoryURI <em>Mqtt Runtime Directory URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getSocketsRuntimeDirectoryURI <em>Sockets Runtime Directory URI</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationPackage#getCodegenFreertosIntegration()
 * @model
 * @generated
 */
public interface CodegenFreertosIntegration extends CodegenFreertos {
	/**
	 * Returns the value of the '<em><b>Mqtt Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mqtt Runtime Dir</em>' attribute.
	 * @see #setMqttRuntimeDir(String)
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationPackage#getCodegenFreertosIntegration_MqttRuntimeDir()
	 * @model required="true"
	 * @generated
	 */
	String getMqttRuntimeDir();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getMqttRuntimeDir <em>Mqtt Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mqtt Runtime Dir</em>' attribute.
	 * @see #getMqttRuntimeDir()
	 * @generated
	 */
	void setMqttRuntimeDir(String value);

	/**
	 * Returns the value of the '<em><b>Sockets Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sockets Runtime Dir</em>' attribute.
	 * @see #setSocketsRuntimeDir(String)
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationPackage#getCodegenFreertosIntegration_SocketsRuntimeDir()
	 * @model required="true"
	 * @generated
	 */
	String getSocketsRuntimeDir();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getSocketsRuntimeDir <em>Sockets Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sockets Runtime Dir</em>' attribute.
	 * @see #getSocketsRuntimeDir()
	 * @generated
	 */
	void setSocketsRuntimeDir(String value);

	/**
	 * Returns the value of the '<em><b>Mqtt Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mqtt Runtime Directory URI</em>' attribute.
	 * @see #setMqttRuntimeDirectoryURI(URI)
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationPackage#getCodegenFreertosIntegration_MqttRuntimeDirectoryURI()
	 * @model dataType="de.mdelab.workflow.helpers.URI" transient="true"
	 * @generated
	 */
	URI getMqttRuntimeDirectoryURI();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getMqttRuntimeDirectoryURI <em>Mqtt Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mqtt Runtime Directory URI</em>' attribute.
	 * @see #getMqttRuntimeDirectoryURI()
	 * @generated
	 */
	void setMqttRuntimeDirectoryURI(URI value);

	/**
	 * Returns the value of the '<em><b>Sockets Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sockets Runtime Directory URI</em>' attribute.
	 * @see #setSocketsRuntimeDirectoryURI(URI)
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationPackage#getCodegenFreertosIntegration_SocketsRuntimeDirectoryURI()
	 * @model dataType="de.mdelab.workflow.helpers.URI" transient="true"
	 * @generated
	 */
	URI getSocketsRuntimeDirectoryURI();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration#getSocketsRuntimeDirectoryURI <em>Sockets Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sockets Runtime Directory URI</em>' attribute.
	 * @see #getSocketsRuntimeDirectoryURI()
	 * @generated
	 */
	void setSocketsRuntimeDirectoryURI(URI value);

} // CodegenFreertosIntegration
