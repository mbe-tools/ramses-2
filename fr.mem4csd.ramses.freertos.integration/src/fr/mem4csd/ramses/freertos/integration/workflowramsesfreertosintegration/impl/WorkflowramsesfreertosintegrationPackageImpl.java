/**
 */
package fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.impl;

import de.mdelab.workflow.WorkflowPackage;

import de.mdelab.workflow.components.ComponentsPackage;

import de.mdelab.workflow.helpers.HelpersPackage;

import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;

import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.CodegenFreertosIntegration;
import fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationFactory;
import fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationPackage;

import fr.mem4csd.ramses.freertos.workflowramsesfreertos.WorkflowramsesfreertosPackage;

import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowramsesfreertosintegrationPackageImpl extends EPackageImpl implements WorkflowramsesfreertosintegrationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codegenFreertosIntegrationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.ramses.freertos.integration.workflowramsesfreertosintegration.WorkflowramsesfreertosintegrationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkflowramsesfreertosintegrationPackageImpl() {
		super(eNS_URI, WorkflowramsesfreertosintegrationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WorkflowramsesfreertosintegrationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkflowramsesfreertosintegrationPackage init() {
		if (isInited) return (WorkflowramsesfreertosintegrationPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowramsesfreertosintegrationPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorkflowramsesfreertosintegrationPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WorkflowramsesfreertosintegrationPackageImpl theWorkflowramsesfreertosintegrationPackage = registeredWorkflowramsesfreertosintegrationPackage instanceof WorkflowramsesfreertosintegrationPackageImpl ? (WorkflowramsesfreertosintegrationPackageImpl)registeredWorkflowramsesfreertosintegrationPackage : new WorkflowramsesfreertosintegrationPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Aadl2Package.eINSTANCE.eClass();
		Arch_tracePackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();
		HelpersPackage.eINSTANCE.eClass();
		ComponentsPackage.eINSTANCE.eClass();
		WorkflowramsesPackage.eINSTANCE.eClass();
		WorkflowramsesfreertosPackage.eINSTANCE.eClass();
		WorkflowramseslinuxPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkflowramsesfreertosintegrationPackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowramsesfreertosintegrationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorkflowramsesfreertosintegrationPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkflowramsesfreertosintegrationPackage.eNS_URI, theWorkflowramsesfreertosintegrationPackage);
		return theWorkflowramsesfreertosintegrationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodegenFreertosIntegration() {
		return codegenFreertosIntegrationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenFreertosIntegration_MqttRuntimeDir() {
		return (EAttribute)codegenFreertosIntegrationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenFreertosIntegration_SocketsRuntimeDir() {
		return (EAttribute)codegenFreertosIntegrationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenFreertosIntegration_MqttRuntimeDirectoryURI() {
		return (EAttribute)codegenFreertosIntegrationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenFreertosIntegration_SocketsRuntimeDirectoryURI() {
		return (EAttribute)codegenFreertosIntegrationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramsesfreertosintegrationFactory getWorkflowramsesfreertosintegrationFactory() {
		return (WorkflowramsesfreertosintegrationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		codegenFreertosIntegrationEClass = createEClass(CODEGEN_FREERTOS_INTEGRATION);
		createEAttribute(codegenFreertosIntegrationEClass, CODEGEN_FREERTOS_INTEGRATION__MQTT_RUNTIME_DIR);
		createEAttribute(codegenFreertosIntegrationEClass, CODEGEN_FREERTOS_INTEGRATION__SOCKETS_RUNTIME_DIR);
		createEAttribute(codegenFreertosIntegrationEClass, CODEGEN_FREERTOS_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI);
		createEAttribute(codegenFreertosIntegrationEClass, CODEGEN_FREERTOS_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		WorkflowramsesfreertosPackage theWorkflowramsesfreertosPackage = (WorkflowramsesfreertosPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowramsesfreertosPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		HelpersPackage theHelpersPackage = (HelpersPackage)EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		codegenFreertosIntegrationEClass.getESuperTypes().add(theWorkflowramsesfreertosPackage.getCodegenFreertos());

		// Initialize classes, features, and operations; add parameters
		initEClass(codegenFreertosIntegrationEClass, CodegenFreertosIntegration.class, "CodegenFreertosIntegration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCodegenFreertosIntegration_MqttRuntimeDir(), theEcorePackage.getEString(), "mqttRuntimeDir", null, 1, 1, CodegenFreertosIntegration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegenFreertosIntegration_SocketsRuntimeDir(), theEcorePackage.getEString(), "socketsRuntimeDir", null, 1, 1, CodegenFreertosIntegration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegenFreertosIntegration_MqttRuntimeDirectoryURI(), theHelpersPackage.getURI(), "mqttRuntimeDirectoryURI", null, 0, 1, CodegenFreertosIntegration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegenFreertosIntegration_SocketsRuntimeDirectoryURI(), theHelpersPackage.getURI(), "socketsRuntimeDirectoryURI", null, 0, 1, CodegenFreertosIntegration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //WorkflowramsesfreertosintegrationPackageImpl
