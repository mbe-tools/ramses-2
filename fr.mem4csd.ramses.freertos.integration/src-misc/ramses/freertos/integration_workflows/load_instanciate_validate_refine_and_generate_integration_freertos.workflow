<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_PAkpAMixEei-D6bRtwoJ1Q" name="workflow" description="EMFTVM operations needed by posix">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_co-wcAcREeuDOaqjzi9xMA" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/templates/load_instanciate_and_validate.workflow">
    <propertyValues xmi:id="_WvZFQKDnEeqR3ds1UXwt-g" name="source_aadl_file" defaultValue="${source_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_G214MKDnEeqR3ds1UXwt-g" name="system_implementation_name" defaultValue="${system_implementation_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_oiK6EAcTEeuDOaqjzi9xMA" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_zDP2sAcUEeuDOaqjzi9xMA" name="validation_workflow" defaultValue="${validation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_8aCm8AcUEeuDOaqjzi9xMA" name="target" defaultValue="${target}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_LRJTMAcXEeuDOaqjzi9xMA" name="load_transformation_resources_workflow" defaultValue="${load_transformation_resources_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_1tA5sAiSEeuaC9UGkI9whg" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_1rSeMEfGEeux84O3cpDbTg" name="load_aadl_resources_workflow" defaultValue="${load_aadl_resources_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_Xkv9cEXOEeu5mq30jrChGg" name="conditionalExecution" conditionSlot="violates${target}Constraints">
    <onTrue xmi:id="_clIuIEXOEeu5mq30jrChGg" name="onTrue, error dected: workflow stops"/>
    <onFalse xmi:id="_frTN0EXOEeu5mq30jrChGg" name="onFalse, no error: workflow continues">
      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_tKtt8JtkEeqJ3cxvaDjAOg" name="posix_validation" workflowURI="refine_remotes_then_locals_and_generate_integration_freertos.workflow">
        <propertyValues xmi:id="_OQ0_cP5bEeqjUK5cPU4ZFw" name="output_dir" defaultValue="${output_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_XTQpQP5bEeqjUK5cPU4ZFw" name="runtime_scheme" defaultValue="${runtime_scheme}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_-tatkAhuEeuaPYhX2kwqog" name="source_file_name" defaultValue="${source_file_name}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_CF_L4AhvEeuaPYhX2kwqog" name="include_dir" defaultValue="${include_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_xoH5AAhzEeuaPYhX2kwqog" name="locals_refinement_workflow" defaultValue="${locals_refinement_workflow}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_mjdJgAimEeulUut1LEEszg" name="code_generation_workflow" defaultValue="${code_generation_workflow}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_Q5CDcAixEeulUut1LEEszg" name="target_install_dir" defaultValue="${target_install_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
      </components>
    </onFalse>
  </components>
  <properties xmi:id="_RAT4AKDmEeqR3ds1UXwt-g" name="system_implementation_name" defaultValue="${system_implementation_name}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_iLkBkO3eEeqXMuraMJ1XYQ" name="remote_communications_file" defaultValue="${remote_communications_file}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_62lFoO3lEeqXMuraMJ1XYQ" name="local_communications_file" defaultValue="${local_communications_file}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_jAEpMKDmEeqR3ds1UXwt-g" name="refined_trace_file" defaultValue="${remote_communications_file}.arch_trace">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_mcjXIKDmEeqR3ds1UXwt-g" name="validation_report_file" defaultValue="${remote_communications_file}.validation_report">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_2FwDgKE8EeqgtNEZpZXkFQ" name="refined_aadl_file" defaultValue="${remote_communications_file}.aadl">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_vyLEAAhzEeuaPYhX2kwqog" name="locals_refinement_workflow" defaultValue="${locals_refinement_workflow}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_YZkB4KEEEeqR3ds1UXwt-g" name="include_dir" description="${description_include_dir}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_5AsSoKDkEeqR3ds1UXwt-g" fileURI="default_integration_freertos.properties"/>
  <propertiesFiles xmi:id="_DSP-wOhyEeqy-t86Uy9Gdw" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
  <allProperties xmi:id="_ZAcQcGVsEeuMzacQL6_RpQ" name="ramses_freertos_workflows_path" defaultValue="${scheme}${ramses_freertos_plugin}ramses/freertos/workflows/"/>
  <allProperties xmi:id="_ZAcQcWVsEeuMzacQL6_RpQ" name="ramses_mqtt_workflows_path" defaultValue="${scheme}${ramses_mqtt_plugin}ramses/mqtt/workflows/"/>
  <allProperties xmi:id="_ZAcQcmVsEeuMzacQL6_RpQ" name="scheme" defaultValue="platform:/plugin/"/>
  <allProperties xmi:id="_ZAcQc2VsEeuMzacQL6_RpQ" name="validation_workflow" defaultValue="${scheme}${ramses_freertos_plugin}ramses/freertos/workflows/validate_freertos.workflow"/>
  <allProperties xmi:id="_ZAcQdGVsEeuMzacQL6_RpQ" name="ramses_freertos_integration_plugin" defaultValue="fr.mem4csd.ramses.freertos.integration/"/>
  <allProperties xmi:id="_ZAcQdWVsEeuMzacQL6_RpQ" name="ramses_mqtt_plugin" defaultValue="fr.mem4csd.ramses.mqtt/"/>
  <allProperties xmi:id="_ZAcQdmVsEeuMzacQL6_RpQ" name="load_transformation_resources_workflow" defaultValue="load_transformation_resources_freertos_integration.workflow"/>
  <allProperties xmi:id="_ZAcQd2VsEeuMzacQL6_RpQ" name="load_aadl_resources_workflow" defaultValue="${ramses_core_workflows_path}load_ramses_aadl_resources_core.workflow"/>
  <allProperties xmi:id="_ZAcQeGVsEeuMzacQL6_RpQ" name="sockets_runtime_dir" defaultValue="${runtime_scheme}fr.mem4csd.ramses.sockets.runtime.c/"/>
  <allProperties xmi:id="_ZAcQeWVsEeuMzacQL6_RpQ" name="mqtt_runtime_dir" defaultValue="${runtime_scheme}fr.mem4csd.ramses.mqtt.runtime.c/"/>
  <allProperties xmi:id="_ZAcQemVsEeuMzacQL6_RpQ" name="ramses_posix_plugin" defaultValue="fr.mem4csd.ramses.posix/"/>
  <allProperties xmi:id="_ZAcQe2VsEeuMzacQL6_RpQ" name="code_generation_workflow" defaultValue="${ramses_freertos_integration_workflows_path}generate_freertos_integration.workflow"/>
  <allProperties xmi:id="_ZAcQfGVsEeuMzacQL6_RpQ" name="load_transformation_resources_target" defaultValue="${scheme}${ramses_posix_plugin}ramses/posix/workflows/load_transformation_resources_posix.workflow"/>
  <allProperties xmi:id="_ZAcQfWVsEeuMzacQL6_RpQ" name="target" defaultValue="freertos"/>
  <allProperties xmi:id="_ZAcQfmVsEeuMzacQL6_RpQ" name="ramses_freertos_plugin" defaultValue="fr.mem4csd.ramses.freertos/"/>
  <allProperties xmi:id="_ZAcQf2VsEeuMzacQL6_RpQ" name="load_sockets_transformation_resources_workflow" defaultValue="${ramses_sockets_workflows_path}load_transformation_resources_sockets.workflow"/>
  <allProperties xmi:id="_ZAcQgGVsEeuMzacQL6_RpQ" name="load_aadl_resources_target" defaultValue="${ramses_freertos_workflows_path}load_aadl_resources_freertos.workflow"/>
  <allProperties xmi:id="_ZAcQgWVsEeuMzacQL6_RpQ" name="load_mqtt_transformation_resources_workflow" defaultValue="${ramses_mqtt_workflows_path}load_transformation_resources_mqtt.workflow"/>
  <allProperties xmi:id="_ZAcQgmVsEeuMzacQL6_RpQ" name="ramses_freertos_integration_workflows_path" defaultValue="${scheme}${ramses_freertos_integration_plugin}ramses/freertos/integration_workflows/"/>
  <allProperties xmi:id="_ZAcQg2VsEeuMzacQL6_RpQ" name="ramses_sockets_workflows_path" defaultValue="${scheme}${ramses_sockets_plugin}ramses/sockets/workflows/"/>
  <allProperties xmi:id="_ZAcQhGVsEeuMzacQL6_RpQ" name="target_runtime_dir" defaultValue="${runtime_scheme}fr.mem4csd.ramses.posix.runtime.c/"/>
  <allProperties xmi:id="_ZAc3gGVsEeuMzacQL6_RpQ" name="ramses_sockets_plugin" defaultValue="fr.mem4csd.ramses.sockets/"/>
  <allProperties xmi:id="_ZAc3gWVsEeuMzacQL6_RpQ" name="ramses_freertos_workflows_path" defaultValue="${scheme}${ramses_freertos_plugin}ramses/freertos/workflows/"/>
  <allProperties xmi:id="_ZAdekGVsEeuMzacQL6_RpQ" name="ramses_mqtt_workflows_path" defaultValue="${scheme}${ramses_mqtt_plugin}ramses/mqtt/workflows/"/>
  <allProperties xmi:id="_ZAdekWVsEeuMzacQL6_RpQ" name="scheme" defaultValue="platform:/plugin/"/>
  <allProperties xmi:id="_ZAdekmVsEeuMzacQL6_RpQ" name="validation_workflow" defaultValue="${scheme}${ramses_freertos_plugin}ramses/freertos/workflows/validate_freertos.workflow"/>
  <allProperties xmi:id="_ZAdek2VsEeuMzacQL6_RpQ" name="ramses_freertos_integration_plugin" defaultValue="fr.mem4csd.ramses.freertos.integration/"/>
  <allProperties xmi:id="_ZAdelGVsEeuMzacQL6_RpQ" name="ramses_mqtt_plugin" defaultValue="fr.mem4csd.ramses.mqtt/"/>
  <allProperties xmi:id="_ZAdelWVsEeuMzacQL6_RpQ" name="load_transformation_resources_workflow" defaultValue="load_transformation_resources_freertos_integration.workflow"/>
  <allProperties xmi:id="_ZAdelmVsEeuMzacQL6_RpQ" name="load_aadl_resources_workflow" defaultValue="${ramses_core_workflows_path}load_ramses_aadl_resources_core.workflow"/>
  <allProperties xmi:id="_ZAdel2VsEeuMzacQL6_RpQ" name="sockets_runtime_dir" defaultValue="${runtime_scheme}fr.mem4csd.ramses.sockets.runtime.c/"/>
  <allProperties xmi:id="_ZAdemGVsEeuMzacQL6_RpQ" name="mqtt_runtime_dir" defaultValue="${runtime_scheme}fr.mem4csd.ramses.mqtt.runtime.c/"/>
  <allProperties xmi:id="_ZAdemWVsEeuMzacQL6_RpQ" name="ramses_posix_plugin" defaultValue="fr.mem4csd.ramses.posix/"/>
  <allProperties xmi:id="_ZAdemmVsEeuMzacQL6_RpQ" name="code_generation_workflow" defaultValue="${ramses_freertos_integration_workflows_path}generate_freertos_integration.workflow"/>
  <allProperties xmi:id="_ZAdem2VsEeuMzacQL6_RpQ" name="load_transformation_resources_target" defaultValue="${scheme}${ramses_posix_plugin}ramses/posix/workflows/load_transformation_resources_posix.workflow"/>
  <allProperties xmi:id="_ZAdenGVsEeuMzacQL6_RpQ" name="target" defaultValue="freertos"/>
  <allProperties xmi:id="_ZAdenWVsEeuMzacQL6_RpQ" name="ramses_freertos_plugin" defaultValue="fr.mem4csd.ramses.freertos/"/>
  <allProperties xmi:id="_ZAdenmVsEeuMzacQL6_RpQ" name="load_sockets_transformation_resources_workflow" defaultValue="${ramses_sockets_workflows_path}load_transformation_resources_sockets.workflow"/>
  <allProperties xmi:id="_ZAden2VsEeuMzacQL6_RpQ" name="load_aadl_resources_target" defaultValue="${ramses_freertos_workflows_path}load_aadl_resources_freertos.workflow"/>
  <allProperties xmi:id="_ZAdeoGVsEeuMzacQL6_RpQ" name="load_mqtt_transformation_resources_workflow" defaultValue="${ramses_mqtt_workflows_path}load_transformation_resources_mqtt.workflow"/>
  <allProperties xmi:id="_ZAdeoWVsEeuMzacQL6_RpQ" name="ramses_freertos_integration_workflows_path" defaultValue="${scheme}${ramses_freertos_integration_plugin}ramses/freertos/integration_workflows/"/>
  <allProperties xmi:id="_ZAdeomVsEeuMzacQL6_RpQ" name="ramses_sockets_workflows_path" defaultValue="${scheme}${ramses_sockets_plugin}ramses/sockets/workflows/"/>
  <allProperties xmi:id="_ZAdeo2VsEeuMzacQL6_RpQ" name="target_runtime_dir" defaultValue="${runtime_scheme}fr.mem4csd.ramses.posix.runtime.c/"/>
  <allProperties xmi:id="_ZAdepGVsEeuMzacQL6_RpQ" name="ramses_sockets_plugin" defaultValue="fr.mem4csd.ramses.sockets/"/>
</workflow:Workflow>
