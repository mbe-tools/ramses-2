--
-- RAMSES 2.0
-- 
-- Copyright © 2012-2015 TELECOM Paris and CNRS
-- Copyright © 2016-2020 TELECOM Paris
-- 
-- TELECOM Paris
-- 
-- Authors: see AUTHORS
--
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program. 
-- If not, see https://www.eclipse.org/legal/epl-2.0/
--

-- @nsURI AADLI=http:///AADL2/instance
-- @atlcompiler emftvm

module Uninstanciate;

create 
	OUT 			: AADLBA,
	OUT_TRACE		: ARCH_TRACE
from 
	IN 				: AADLI,
	TOOLS			: ATLHELPER;


rule Comment {
	from
		c : AADLI!Comment
	to
		c2 : AADLBA!Comment(body <- c.body, ownedComment <- c.ownedComment)
}


-------------------------------------------------------------------------------------
---  RULES FOR KINDS OF COMPONENT INSTANCES
-------------------------------------------------------------------------------------

rule m_RootSystem_Instance {
	from
		c : AADLI!SystemInstance (c.category = #system and c.eContainer().oclIsUndefined())
	using
	{
		src_package : AADLI!AadlPackage = c.componentImplementation.getNamespace().getOwner();
	}
	to
		section : AADLBA!PublicPackageSection,
		pkg : AADLBA!AadlPackage (
			name <- thisModule.theTOOLS.getOutputPackageName(),
			ownedPublicSection <- section ),
		refinementTraceSystemInstance: ARCH_TRACE!ArchRefinementTrace (
			--sourceElement <- c,
			targetElement <- c.componentImplementation.resolve(),
			transformationRule <- 'm_RootSystem_Instance'
		)
	do
	{
		thisModule.allImportedUnits <- thisModule.allImportedUnits->including(src_package.ownedPublicSection.importedUnit);
		if(AADLBA!SystemImplementation.allInstancesFrom('OUT')->contains(c.componentImplementation.resolve()))
		{
			c.componentImplementation.resolve().ownedPropertyAssociation <- c.getPropertyAssociationImg()
																		->union(c.getInstanceReferenceValue()->collect (p |  thisModule.resolveMatchedSingleIfExists(p, 'p2')))
																		->union(c.connectionInstance->collect(cnx | cnx.ownedPropertyAssociation
																			->select(e|e.isApplicableTo(e.eContainer().resolve()))->collect (p |  thisModule.resolveMatchedSingleIfExists(p, 'p2'))))->flatten()
																		->union(
																			AADLI!PropertyAssociation.allInstancesFrom('IN')
																													->select(e|
																																(e.referencesFeatureInNotCopiedType()
																																or
																																(e.referencesNotCopiedInstance()
																																and 
																																e.referencesCopiedInstance())
																																)
																																and 
																																e.isApplicableTo(e.eContainer().resolve()) 
																													)
																													->collect (p |  thisModule.resolveMatchedSingleIfExists(p, 'p2'))
																		)
																		->union(c.collectSelfPropertyAssociationImg())
																		->union(c.collectExtraPropertyAssociation())
																		->excluding(OclUndefined);
		}
		section.ownedClassifier <- AADLBA!Classifier.allInstancesFrom('OUT');
		refinementTraceSystemInstance.setSourceElement(c);
		refinementTraceSystemInstance.setTargetElement(c.componentImplementation.resolve());
		thisModule.traceList <- thisModule.traceList.including(refinementTraceSystemInstance);
	}
}

helper context AADLI!SystemInstance def: collectExtraPropertyAssociation() : Sequence(AADLBA!PropertyAssociation) =
	Sequence{}
;

endpoint rule addImportedPackages()
{
	to
	traceSpecVar: ARCH_TRACE!ArchTraceSpec (
		archRefinementTrace <- thisModule.traceList,
		name <- 'Endpoint_Trace_Override'
	)
	do
	{
--		for (trace in thisModule.traceList) {
--			traceSpecVar.addTrace(trace.getTargetElement(), trace.getSourceElement());
--		}
		thisModule.public().importedUnit <- thisModule.allImportedUnits->flatten()->asSet()->asSequence()->excluding(OclUndefined);
	}
}

rule m_System_Instance {
	from
		c : AADLI!ComponentInstance (c.category = #system and not c.eContainer().oclIsUndefined())
	to
		sub : AADLBA!SystemSubcomponent
		(
			name <- c.subcomponent.name,
			systemSubcomponentType <- c.getTypeOrImpl(),
			ownedPropertyAssociation  <- c.collectSelfPropertyAssociationImg()->excluding(OclUndefined)
		),
		refinementTraceSystem: ARCH_TRACE!ArchRefinementTrace (
			--sourceElement <- c,
			targetElement <- sub,
			transformationRule <- 'm_System_Instance'
		)
	do
	{
		if(AADLBA!SystemImplementation.allInstancesFrom('OUT')->contains(c.subcomponent.systemSubcomponentType.resolve()))
			c.subcomponent.systemSubcomponentType.resolve().ownedPropertyAssociation <- c.getPropertyAssociationImg();
		refinementTraceSystem.setSourceElement(c);
		thisModule.traceList <- thisModule.traceList.including(refinementTraceSystem);
		
	}
}


rule m_Mode_Instance {
	from
		m: AADLI!ModeInstance
--		(m.eContainer().oclIsTypeOf(AADLI!ComponentInstance))
	to
		mode: AADLBA!Mode
		(
			name <- m.mode.name,
			initial <- m.initial
		)
}

rule m_ModeTransition_Instance {
	from
		m: AADLI!ModeTransitionInstance
--		(m.eContainer().oclIsTypeOf(AADLI!ComponentInstance))
	to
		transition: AADLBA!ModeTransition
		(
			name <- m.name,
			source <- m.source,
			destination <- m.destination,
			ownedTrigger <- m.modeTransition.ownedTrigger->collect(e|
						thisModule.ulr_copyModeTransitionTrigger(e)
					)
		)
}

unique lazy rule ulr_copyModeTransitionTrigger
{
	from
		m_in: AADLI!ModeTransitionTrigger
	to
		m_out: AADLBA!ModeTransitionTrigger
		(
			context <- m_in.context,
			triggerPort <- m_in.triggerPort
		)
	do
	{
		m_out;
	}
}

helper context OclAny def: needsCopy() : Boolean = true;

abstract rule m_Component_Instance {
	from
		c : AADLI!ComponentInstance in IN
		(c.needsCopy())
	to
		sub: AADLBA!Subcomponent(
				name <- c.uniqueName().concat(thisModule.theTOOLS.getIndex(c)).debug('subcomponent created'),
				ownedPropertyAssociation  <- c.collectSelfPropertyAssociationImg()->excluding(OclUndefined),
				ownedModeBinding <- c.inMode->collect(e|
										thisModule.createModeBinding(thisModule.resolveTemp(e, 'mode'))
									)
			),
		refinementTraceComponentInstance: ARCH_TRACE!ArchRefinementTrace (
			--sourceElement <- c,
			targetElement <- sub,
			transformationRule <- 'm_Component_Instance'
		)
	do
	{
		-- The type is not correct when we pass by the affectation of ATL
		-- Bug in ATL or problem during the transformation ?
		refinementTraceComponentInstance.setSourceElement(c);
		thisModule.traceList <- thisModule.traceList.including(refinementTraceComponentInstance);
		if(not c.getTypeOrImpl().oclIsUndefined())
			thisModule.addImportedUnitFromInputModel(thisModule.public(), c.getTypeOrImpl());
		
	}
}

lazy rule createModeBinding
{
	from
		m: AADLBA!Mode
	to
		b: AADLBA!ModeBinding
		(
			parentMode <- m
		)
}

-- @extends m_Component_Instance
rule m_Abstract_Instance {
	from
		c : AADLI!ComponentInstance (c.category.getName() = 'abstract')
	to
		sub : AADLBA!AbstractSubcomponent
		(
			abstractSubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}

-- @extends m_Component_Instance
rule m_Memory_Instance {
	from
		c : AADLI!ComponentInstance (c.category = #memory)
	to
		sub : AADLBA!MemorySubcomponent
		(
			memorySubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}

-- @extends m_Component_Instance
rule m_Bus_Instance {
	from
		c : AADLI!ComponentInstance (c.category = #bus)
	to
		sub : AADLBA!BusSubcomponent
		(
			busSubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}

-- @extends m_Component_Instance
rule m_VirtualBus_Instance {
	from
		c : AADLI!ComponentInstance (c.category.getName() = 'virtual bus')
	to
		sub : AADLBA!VirtualBusSubcomponent
		(
			virtualBusSubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}

-- @extends m_Component_Instance
rule m_Device_Instance {
	from
		c : AADLI!ComponentInstance (c.category = #device)
	to
		sub : AADLBA!DeviceSubcomponent
		(
			deviceSubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}

-- @extends m_Component_Instance
rule m_Processor_Instance {
	from
		c : AADLI!ComponentInstance (c.category = #processor)
	to
		sub : AADLBA!ProcessorSubcomponent
		(
			processorSubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}

-- @extends m_Component_Instance
rule m_VirtualProcessor_Instance {
	from
		c : AADLI!ComponentInstance (c.category.getName() = 'virtual processor')
	to
		sub : AADLBA!VirtualProcessorSubcomponent
		(
			virtualProcessorSubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}

-- @extends m_Component_Instance
rule m_Thread_Instance {
	from
		c : AADLI!ComponentInstance (c.category = #thread)
	to 
		sub : AADLBA!ThreadSubcomponent
		(
			threadSubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}

-- @extends m_Component_Instance
rule m_ThreadGroup_Instance {
	from
		c : AADLI!ComponentInstance (c.category.getName() = 'thread group')
	to 
		sub : AADLBA!ThreadGroupSubcomponent
		(
			threadGroupSubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}

-- @extends m_Component_Instance
rule m_Subprogram_Instance {
	from
		c : AADLI!ComponentInstance (c.category = #subprogram)
	to 
		sub : AADLBA!SubprogramSubcomponent
		(
			subprogramSubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}

-- @extends m_Component_Instance
rule m_Process_Instance {
	from
		c : AADLI!ComponentInstance (c.category = #process)
	to 
		sub : AADLBA!ProcessSubcomponent
		(
			processSubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}

-- @extends m_Component_Instance
rule m_Data_Instance {
	from
		c : AADLI!ComponentInstance (c.category = #data)
	to
		sub : AADLBA!DataSubcomponent
		(
			-- EB: use name instead of uniqueName for data subcomponent as they might be referenced
			-- in Initial_Value property with a string.
			name <- c.name.concat(thisModule.theTOOLS.getIndex(c)).debug('data subcomponent created'),
			dataSubcomponentType <- c.classifier --subcomponent.subcomponentType
		)
}	

------------------------------------------------------------------------------------
--- Filters
------------------------------------------------------------------------------------
		
abstract rule m_Thread_Instance_Filter {
	from 
    	c : AADLI!ComponentInstance
		(c.category=#thread)
}

abstract rule m_Process_Instance_Filter {
	from 
    	c : AADLI!ComponentInstance
		(c.category=#process)
}

abstract rule m_BA_Filter {
	from 
    	behavior: AADLI!AnnexSubclause
		(behavior.oclIsTypeOf(AADLBA!BehaviorAnnex))
}
		
abstract rule m_DataPort_Filter {
	from
		fi: AADLI!FeatureInstance
		(fi.isDataPort())
}

abstract rule m_EventOrEnvetDataPort_Filter
{
	from
		fi: AADLI!FeatureInstance
		(fi.isEventOrEventDataPort())
}

abstract rule m_EventPort_Filter {
	from
		fi: AADLI!FeatureInstance
		(fi.isEventPort())
}

abstract rule m_EventDataPort_Filter {
	from
		fi: AADLI!FeatureInstance
		(fi.isEventDataPort())
}

abstract rule m_ThreadPort_Filter
{
	from
		fi: AADLI!FeatureInstance
		(fi.isThreadPort())
}

abstract rule m_OutputFeature_Filter
{
	from
		fi: AADLI!FeatureInstance
		(fi.isOutputFeature())
}

abstract rule m_InputFeature_Filter
{
	from
		fi: AADLI!FeatureInstance
		(fi.isInputFeature())
}