--
-- RAMSES 2.0
-- 
-- Copyright © 2012-2015 TELECOM Paris and CNRS
-- Copyright © 2016-2020 TELECOM Paris
-- 
-- TELECOM Paris
-- 
-- Authors: see AUTHORS
--
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program. 
-- If not, see https://www.eclipse.org/legal/epl-2.0/
--

-- @atlcompiler emftvm

module CopyElementHolders;
create 	OUT : AADLBA,
		OUT_TRACE : ARCH_TRACE
from 	IN : AADLBA;

abstract rule m_ElementHolder
{
	from
		src_BehaviorElement: AADLBA!ElementHolder
		(src_BehaviorElement.isThreadBehaviorElement())
	to
		target_BehaviorElement: AADLBA!ElementHolder
		(
			element<-src_BehaviorElement.element	
		)
}

-- @extends m_ElementHolder
rule m_SubprogramAccessHolder
{
	from
		src_BehaviorElement: AADLBA!SubprogramAccessHolder
	to
		target_BehaviorElement: AADLBA!SubprogramAccessHolder
}

-- @extends m_ElementHolder
rule m_EventPortHolder
{
	from
		src_BehaviorElement: AADLBA!EventPortHolder
	to
		target_BehaviorElement: AADLBA!EventPortHolder
}

-- @extends m_ElementHolder
rule m_EventDataPortHolder
{
	from
		src_BehaviorElement:AADLBA!EventDataPortHolder
	to
		target_BehaviorElement: AADLBA!EventDataPortHolder
}

-- @extends m_ElementHolder
rule m_DataPortHolder
{
	from
		src_BehaviorElement:AADLBA!DataPortHolder
	to
		target_BehaviorElement: AADLBA!DataPortHolder
}

-- @extends m_ElementHolder
rule m_CalledSubprogramHolder
{
	from
		src_BehaviorElement:AADLBA!CalledSubprogramHolder
	to
		target_BehaviorElement:AADLBA!CalledSubprogramHolder
}

-- @extends m_ElementHolder
rule m_DataAccessHolder
{
	from
		src_BehaviorElement:AADLBA!DataAccessHolder
	to
		target_BehaviorElement:AADLBA!DataAccessHolder
}

-- @extends m_ElementHolder
rule m_BehaviorVariableHolder
{
	from
		src_BehaviorElement: AADLBA!BehaviorVariableHolder
	to
		target_BehaviorElement: AADLBA!BehaviorVariableHolder
}

-- @extends m_ElementHolder
rule m_PortFreshValue
{
	from
		src_BehaviorElement: AADLBA!PortFreshValue
	to
		target_BehaviorElement: AADLBA!PortFreshValue
}
