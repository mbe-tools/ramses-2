--
-- RAMSES 2.0
-- 
-- Copyright © 2012-2015 TELECOM Paris and CNRS
-- Copyright © 2016-2020 TELECOM Paris
-- 
-- TELECOM Paris
-- 
-- Authors: see AUTHORS
--
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program. 
-- If not, see https://www.eclipse.org/legal/epl-2.0/
--

-- @nsURI AADLBA=/fr.tpt.aadl.annex.behavior/model/aadlba.ecore
-- @atlcompiler emftvm

module ThreadsUninstanciateOverride;
create	OUT : AADLBA,
		OUT_TRACE : ARCH_TRACE
from 	IN : AADLI,
		AADL_RUNTIME: AADLBA,
		DATA_MODEL: AADLBA,
		PROGRAMMING_PROPERTIES: AADLBA;

helper context AADLI!ComponentImplementation def: getAllSubprogramCallSequence() :Sequence(AADLI!SubprogramCallSequence) =
	self.getSelfPlusAllExtended()
	->collect(e|e.ownedSubprogramCallSequence)
	->excluding(OclUndefined)->flatten()
;

-- @extends m_Component_Instance
abstract rule m_create_Thread_BA
{
	from
		c : AADLI!ComponentInstance (c.category = #thread)
	using
 	{
-- 		impl : AADLI!ThreadImplementation = c.subcomponent.subcomponentType;
 		typeOrImpl : AADLI!Thread = c.getTypeOrImpl();
		toBeSure: AADLBA!BehaviorTransition = thisModule.ulr_ComputeEntrypointCallSequenceForBA(c, typeOrImpl.getBA());
 	}
	to
		sub : AADLBA!ThreadSubcomponent(
			threadSubcomponentType <- implImg
		),
		implImg : AADLBA!ThreadImplementation (
 			name <- c.uniqueClassifierName().concat('.impl'), -- name thread implmentation
			ownedRealization <- thisModule.Realization(typeImg),
			ownedSubprogramSubcomponent 	<- c.componentInstance->select(e|e.category=#subprogram),
			ownedDataSubcomponent 			<- c.componentInstance->select(e|e.category=#data)
												->union
												(
													c.getAllFeatureInstances()->select(fi| fi.feature.oclIsKindOf(AADLI!Port) and fi.isUsedInFresh())
													->collect(e|thisModule.createFreshnessIndicator(e))
												)
												->union
												(
													thisModule.collectThreadVariables(c)
												)
												->flatten()->excluding(OclUndefined),
			ownedSubprogramCallSequence     <- Sequence{
 															thisModule.resolveMatchedSingleIfExists(c, 'callSequenceImg'),
 															thisModule.collectInitCallSequence(c)
														}
 												->excluding(OclUndefined),
			ownedParameterConnection		<- if(typeOrImpl.oclIsKindOf(AADLI!ThreadImplementation)) then
			
												typeOrImpl.getAllConnections()->select(e| e.oclIsKindOf(AADLI!ParameterConnection) )->select (e|
												e.source.oclIsKindOf(AADLI!Parameter) and e.destination.oclIsKindOf(AADLI!Parameter))
												->collect(e| thisModule.ParameterConnection(e))
												->union(
													typeOrImpl.getAllConnections()->select(e| e.oclIsKindOf(AADLI!ParameterConnection) )->select (e| e.source.connectionEnd.oclIsKindOf(AADLI!Parameter) and not e.source.context.eContainer().isComputeEntryPointOf(c))
													->collect(e| thisModule.ParameterConnection(e))
												)
												->union(
													typeOrImpl.getAllConnections()->select(e| e.oclIsKindOf(AADLI!ParameterConnection) )->select (e| e.destination.connectionEnd.oclIsKindOf(AADLI!Parameter) and not e.destination.context.eContainer().isComputeEntryPointOf(c))
													->collect(e| thisModule.ParameterConnection(e))
												)
												->union
												(
													typeOrImpl.getAllConnections()->select(e| e.oclIsKindOf(AADLI!ParameterConnection) )->select (e|
														e.source.connectionEnd.oclIsKindOf(AADLI!DataSubcomponent) 
													)
													->collect(e|
														typeOrImpl.getAllSubprogramCallSequence()
														->collect(callSequence|
															
															-- PERF change to lazy rule
															let p:OclAny = thisModule.ulr_Parameter_to_DataInstance_SourceConnection(c.componentInstance->any(f|f.subcomponent = e.source.connectionEnd), callSequence, e) in
															if not p.oclIsUndefined() then
																p->at(1)
															else
																OclUndefined
															endif
--															thisModule.resolveMatchedTupleIfExists(Sequence{c.componentInstance->any(f|f.subcomponent = e.source.connectionEnd), callSequence, e}, 'parameterConnection')
														)
													)
												)
												->union
												(
													typeOrImpl.getAllConnections()->select(e| e.oclIsKindOf(AADLI!ParameterConnection) )->select (e|
														e.destination.connectionEnd.oclIsKindOf(AADLI!DataSubcomponent) 
													)
													->collect(e|
														typeOrImpl.getAllSubprogramCallSequence()
														->collect(callSequence|
															
															-- PERF change to lazy rule
															let p:OclAny = thisModule.ulr_Parameter_to_DataInstance_DestinationConnection(c.componentInstance->any(f|f.subcomponent = e.destination.connectionEnd), callSequence, e) in
															if not p.oclIsUndefined() then
																p->at(1)
															else
																OclUndefined
															endif
--															thisModule.resolveMatchedTupleIfExists(Sequence{c.componentInstance->any(f|f.subcomponent = e.destination.connectionEnd), callSequence, e}, 'parameterConnection')
														)
													)
												)
												else
													Sequence{}
 												endif
												->flatten()->excluding(OclUndefined),
			ownedAccessConnection			<- c.getAllFeatureInstances()->select(fi|fi.feature.oclIsKindOf(AADLI!Port))
												->collect(fi| 
													if(typeOrImpl.oclIsKindOf(AADLI!ThreadImplementation)) then
			
														typeOrImpl.getAllSubprogramCallSequence()
														->collect(callSequence|
															if(fi.isInputFeature()) then
																fi.collectInputFeatureConnectionImg(callSequence)
															else
																fi.collectOutputFeatureConnectionImg(callSequence)
															endif
														)
														->flatten()
													else
														Sequence{}
													endif
												)
												->union(
													c.getAllFeatureInstances()
													->collect(fi|
														fi.srcConnectionInstance->collect( cnxInst |
															thisModule.resolveMatchedSingleIfExists(cnxInst, 'f_connection_src_entrypoint')
														)
														->union(
															fi.dstConnectionInstance->collect( cnxInst |
																thisModule.resolveMatchedSingleIfExists(cnxInst, 'f_connection_dst_entrypoint')
															)
														)
														->append(thisModule.resolveMatchedSingleIfExists(fi, 'cnx_entrypoint'))
													)
												)
												->flatten()
--												->union(
--													c.getAllFeatureInstances()->select(fi|fi.feature.oclIsKindOf(AADLI!Port))
--													->collect(fi|
--															if(fi.isInputFeature()) then
--																thisModule.resolveMatchedTupleIfExists(Sequence{fi,impl.getBA()}, 'f_connection')
--															else
--																fi.srcConnectionInstance->collect( cnxInst |
--																	thisModule.resolveMatchedTupleIfExists(Sequence{fi,impl.getBA(),cnxInst.connectionReference->first().connection}, 'f_connection')
--																)
--															endif
--														)
--												)
--												->flatten()
												->union(
													
													let holders: Sequence(AADLBA!DataSubcomponentHolder) = AADLBA!DataSubcomponentHolder->allInstances()->select(holder | c.componentInstance->exists(e|e.subcomponent=holder.element))
													in
													
													c.componentInstance->select(e| holders->exists(f|f.element = e.subcomponent))
													->collect(e| thisModule.createAccessConnectionForBA(e,c))
																										
												)
												->union(
													if typeOrImpl.oclIsKindOf(AADLI!ThreadImplementation) then
													
														typeOrImpl.getAllSubprogramCallSequence()
														->collect(callSequence | c.subcomponent.classifier.ownedAccessConnection
															-> select(accessCnx| 
--      														condition commented to manage access connections with init entry point
--																accessCnx.isAccessToParamConnection()
--																and
																accessCnx.isPartOfCallSequence(callSequence)
--																and
--																callSequence.isComputeEntryPointOf(c)
															)
															->collect(
																accessCnx | 
																	let res: AADLBA!DataAccess = 	if thisModule.ulr_Parameter_to_DataAccess_DestinationConnection(c, callSequence, accessCnx).oclIsUndefined() then
																		thisModule.ulr_Parameter_to_DataAccess_SourceConnection(c, callSequence, accessCnx)->at(2)
																	else
																		thisModule.ulr_Parameter_to_DataAccess_DestinationConnection(c, callSequence, accessCnx)->at(2)
																	endif in
																	res
--																	thisModule.resolveMatchedTupleIfExists(Sequence{c, callSequence, accessCnx}, 'accessConnection')
															)														
													)
													
													else
														Sequence{}
													endif
												)	
												->union(
													if typeOrImpl.oclIsKindOf(AADLI!ThreadImplementation) then
													
														typeOrImpl.getAllSubprogramCallSequence()
														->collect(callSequence | c.subcomponent.classifier.ownedParameterConnection
															-> select(accessCnx| 
																accessCnx.isPartOfCallSequence(callSequence) and
																callSequence.isComputeEntryPointOf(c) and
																(accessCnx.source.connectionEnd.oclIsTypeOf(AADLI!DataAccess) or accessCnx.destination.connectionEnd.oclIsTypeOf(AADLI!DataAccess))
																)
																->collect(
																accessCnx | Sequence{
																	thisModule.map_Parameter_Connection_to_DataAccessConnection(c, callSequence, accessCnx.source.connectionEnd),
																	thisModule.map_Parameter_Connection_to_DataAccessConnection(c, callSequence, accessCnx.destination.connectionEnd)
																	}
																)
															)
													else
														Sequence{}
													endif
												)
												->union(
													c.getAllFeatureInstances()->select(e|e.category=#dataAccess)
													->collect(e|thisModule.create_DataAccessConnection(e))
												)
												->flatten()
												->append
												(
													thisModule.resolveLazyTupleIfExists(Sequence{c,typeOrImpl.getBA()},'ulr_ComputeEntrypointCallSequenceForBA','accessConnection_CURRENT_STATE')
												)
--												->union
--												(
--													c.getAllFeatureInstances()->select(fi| fi.feature.oclIsKindOf(AADLI!Port) and fi.isUsedInFresh())
--													->collect(e|thisModule.resolveLazySingleIfExists(e, 'createFreshnessIndicator','freshnessIndicator_Connection'))	
--												)
												->union(
													if typeOrImpl.oclIsKindOf(AADLI!ThreadImplementation) then
													
														typeOrImpl.getAllSubprogramCallSequence()->collect(callSequence |
															thisModule.collectPeriodicDelayed_LocalVariablesConnections(c,callSequence)
														)
													else
														Sequence{}
													endif
												)
												->union(
													Sequence{
														thisModule.resolveMatchedSingleIfExists(c, 'accessConnection_entrypoint')
													}
												)
												->flatten()->excluding(OclUndefined)--,
--				ownedMode <- c.modeInstance
		),
		f: AADLBA!DataAccess,
		typeImg : AADLBA!ThreadType (
 	 		name <- c.uniqueClassifierName(),
			ownedDataAccess <- Sequence{f}->union(
									c.getAllFeatureInstances()->select(e| e.feature.oclIsTypeOf(AADLI!DataAccess))->
									collect
									(
											f|f.collectInputFeatureImg()
									)
								)
								->union
								(
									c.getAllFeatureInstances()->select(e| (e.isThreadPort() and e.isInputFeature()))->
									collect
									(
										f|f.collectInputFeatureImg()
									)
								)
								->union(
									c.getAllFeatureInstances()->select(e|e.isThreadPort() and e.isOutputFeature())
									->collect(
	 									f| f.collectOutputFeatureImg()
										)->flatten()
								)
								->append
								(
									thisModule.resolveLazyTupleIfExists(Sequence{c,typeOrImpl.getBA()},'ulr_ComputeEntrypointCallSequenceForBA','currentStateAccessThread')
								)
								->union(
									c.getAllFeatureInstances()
									->collect(
	 									f | f.srcConnectionInstance->collect(
	 											cnxInst | thisModule.resolveMatchedSingleIfExists(cnxInst, 'f_src')
											)
											->union(
												f.dstConnectionInstance->collect(
	 											cnxInst | thisModule.resolveMatchedSingleIfExists(cnxInst, 'f_dst')
											)
										)
										->append(thisModule.resolveMatchedSingleIfExists(f, 'f'))
									)
								)
								->union(
									thisModule.collectPeriodicDelayed_GlobalVariablesAccesses(c)
								)
								->union
								(
									c.getAllFeatureInstances()->select(e|e.feature.oclIsKindOf(AADLBA!DataAccess))
								)
								->append(
									thisModule.resolveMatchedSingleIfExists(c, 'dispatch_info')
								)
								->flatten()->asSet()->excluding(OclUndefined),
			derivedModes <- not c.modeInstance->select(e|e.derived)->isEmpty(),
			ownedMode <- c.modeInstance->select(e|e.derived)
 		)
}

unique lazy rule map_Parameter_Connection_to_DataAccessConnection
{
	from 
		c: AADLI!ComponentInstance, 
		behavior: AADLI!SubprogramCallSequence,		
		cnxE_src : AADLI!DataAccess
		(
			behavior.isComputeEntryPointOf(c) and
			cnxE_src.isPartOfCallSequence(behavior,c) 
		)
	using
	{
		paramToData: Boolean = cnxE_src.belongsToParamToDataConnection(c);
		dataToParam: Boolean = cnxE_src.belongsToDataToParamConnection(c);
	}
	to
		accessConnection: AADLBA!AccessConnection (
			accessCategory <- #data,
			name <- cnxE_src.name+'_cnx',
			source <- ce_src,
			destination <- ce_dest
		),
		ce_src : AADLBA!ConnectedElement (
			context <- if(paramToData) then
							thisModule.resolveMatchedSingleIfExists(c, 'callEntryPoint')
						else
							OclUndefined
						endif,
			connectionEnd <- thisModule.map_Parameter_Connection_to_DataAccess(c, behavior, cnxE_src)	
		),
		ce_dest : AADLBA!ConnectedElement (
			context <- if(dataToParam) then
							thisModule.resolveMatchedSingleIfExists(c, 'callEntryPoint')
						else
							OclUndefined
						endif,
			connectionEnd <- thisModule.map_Parameter_Connection_to_DataAccess(c, behavior, cnxE_src)	
		)
	do
	{
		accessConnection;
	}
}

unique lazy rule create_DataAccessConnection
{
	from 
		fi: AADLI!FeatureInstance
		(fi.category=#dataAccess)
	to
		accessConnection: AADLBA!AccessConnection (
			accessCategory <- #data,
			name <- fi.uniqueName()+'_entrypoint_cnx',
			source <- ce_src,
			destination <- ce_dest
		),
		ce_src : AADLBA!ConnectedElement (
			connectionEnd <- thisModule.ulr_copyEntryPointDataAccess(fi)	
		),
		ce_dest : AADLBA!ConnectedElement (
			connectionEnd <- fi	
		)
	do
	{
		ce_src.setContext(thisModule.resolveMatchedSingleIfExists(fi.getComponentInstance(), 'callEntryPoint'));
		accessConnection;
	}
}

unique lazy rule createAccessConnectionForBA
{
	from
		dataInstance:AADLI!ComponentInstance,
		c: AADLI!ComponentInstance
	using
	{
--		impl: AADLI!ThreadImplementation = c.subcomponent.subcomponentType;
		impl: AADLI!ThreadImplementation = c.getTypeOrImpl();
	}
	to
		accessConnectionForBA: AADLBA!AccessConnection
		(
			source <- sourceCE,
			destination <- destinationCE,
			name <- dataInstance.name+'_accessCnxForBA',
			accessCategory <- #data
		),
		sourceCE: AADLBA!ConnectedElement
		(
			connectionEnd <- thisModule.resolveMatchedTupleIfExists(Sequence{c,dataInstance}, 'dataAccess')
		),
		destinationCE: AADLBA!ConnectedElement
		(
			connectionEnd <- dataInstance
		)
	do
	{
		sourceCE.setContext(thisModule.resolveMatchedSingleIfExists(c, 'callEntryPoint'));
		accessConnectionForBA;
	}
}