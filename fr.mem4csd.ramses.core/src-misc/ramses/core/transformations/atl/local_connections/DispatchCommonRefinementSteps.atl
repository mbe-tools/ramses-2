--
-- RAMSES 2.0
-- 
-- Copyright © 2012-2015 TELECOM Paris and CNRS
-- Copyright © 2016-2020 TELECOM Paris
-- 
-- TELECOM Paris
-- 
-- Authors: see AUTHORS
--
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program. 
-- If not, see https://www.eclipse.org/legal/epl-2.0/
--

-- nsURI AADLBA=http:///AADLBA
-- @atlcompiler emftvm


module DispatchCommonRefinementSteps;
create	OUT : AADLBA,
		OUT_TRACE : ARCH_TRACE
from 	IN : AADLI,
		AADL_RUNTIME: AADLBA,
		DATA_MODEL: AADLBA,
		PROGRAMMING_PROPERTIES: AADLBA;




-- @extends m_ThreadEntrypoint
abstract rule m_PeriodicThread
{
	from
		c: AADLI!ComponentInstance
		(c.isPeriodicThread())
	using
	{
		spg : AADLBA!SubprogramType = thisModule.getWaitPeriodSubprogram;
	}
	to
		sub: AADLBA!ThreadSubcomponent,
		initState: AADLBA!BehaviorState,
		callSeqState: AADLBA!BehaviorState,
		returnData: AADLBA!DataSubcomponent,
		waitDispatchCall: AADLBA!SubprogramCallAction
		(
			subprogram<-dispatchHolder,
			parameterLabels <- Sequence{RETURN_DATA_Holder}
		),
		dispatchHolder: AADLBA!CalledSubprogramHolder
		(
			element <- spg
		),
		RETURN_DATA_Holder: AADLBA!DataSubcomponentHolder
		(
			element<-returnData
		)
	do
	{
		thisModule.addImportedUnit(thisModule.public(), thisModule.getRuntimeName);
	}
}



