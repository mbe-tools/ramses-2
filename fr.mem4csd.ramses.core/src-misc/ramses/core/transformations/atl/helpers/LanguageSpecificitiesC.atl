--
-- RAMSES 2.0
-- 
-- Copyright © 2012-2015 TELECOM Paris and CNRS
-- Copyright © 2016-2020 TELECOM Paris
-- 
-- TELECOM Paris
-- 
-- Authors: see AUTHORS
--
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the Eclipse Public License as published by Eclipse,
-- either version 1.0 of the License, or (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- Eclipse Public License for more details.
-- You should have received a copy of the Eclipse Public License
-- along with this program. 
-- If not, see https://www.eclipse.org/legal/epl-2.0/
--

-- @nsURI AADLBA=/fr.tpt.aadl.annex.behavior/model/aadlba.ecore
-- @atlcompiler emftvm


module LanguageSpecificitiesC;
create OUT : AADLBA from IN : AADLI;

helper def: structInitInitiator: String = '{';
helper def: structInitTerminator: String = '}';

rule concatStringsAsStringList(list: Sequence(String))
{
	using
  {
  	result: String = '';
  	sep: String = ', ';
  	subSeq: Sequence(Integer) = list->subSequence(1, list->size()-1);
  }
  do
  {
	result <- subSeq->iterate(i; result:String = thisModule.structInitInitiator | 
		result + '\\"' + i + '\\"' + ','
	);
	result <- result + '\\"' + list->last() + '\\"' + thisModule.structInitTerminator;
	result;
  }
}

rule concatStrings(list: Sequence(String))
{
  using
  {
  	result: String = '';
  	sep: String = ', ';
  	subSeq: Sequence(Integer) = list->subSequence(1, list->size()-1);
  }
  do
  {
	result <- subSeq->iterate(i; result:String = thisModule.structInitInitiator | 
		result + i + ','
	);
	result <- result + list->last() + thisModule.structInitTerminator;
	result;
  }
}

rule concatToString(list: Sequence(Integer))
{
	
  using
  {
  	result: String = '';
  	sep: String = ', ';
  	subSeq: Sequence(Integer) = if(list->size()>0) then 
									list->subSequence(1, list->size()-1)
								else
									Sequence{}
  								endif;
  }
  do
  {
  	if(list->size()>0)
  	{
		result <- subSeq->iterate(i; result:String = thisModule.structInitInitiator | 
			result + i.toString() + ','
		);
		result <- result + list->last().toString() + thisModule.structInitTerminator;
  	}
  	else
  		result <- result + thisModule.structInitInitiator + thisModule.structInitTerminator;
  	
	result;
  }
}

rule concatToStringList(list: Sequence(Sequence(Integer)))
{
	using
	{
  	  result: String = '';
  	  sep: String = ', ';
  	  subSeq: Sequence(Integer) = list->subSequence(1, list->size()-1);
    }
	do
	{
		result <- subSeq->iterate(i; result:String = thisModule.structInitInitiator | 
			result + thisModule.concatToString(i) + ','
		);
		result <- result + thisModule.concatToString(list->last()) + thisModule.structInitTerminator;
		result;
	}
}

helper def: getSizeOfType(dataType: String): String =
	'sizeof('+dataType+')';

helper context OclAny def: getProgrammationLanguageIdentifier(): String =
	OclUndefined
;

helper context AADLBA!NamedElement def: getProgrammationLanguageIdentifier(): String =
	let sourceName: String = self.getSourceName() in
	if not sourceName.oclIsUndefined() then
		sourceName
	else
		self.getQualifiedName().replaceAll('::', '__').replaceAll('\\.','_')
	endif
;

helper context String def: getProgrammationLanguageIdentifier(): String =
	self.replaceAll('::', '__').replaceAll('\\.','_')
;

helper context String def: getAADLIdentifier(): String =
	self.replaceAll('::', '_').replaceAll('\\.','_')
;


helper def:getProgrammationLanguageIdentifier(identifier:String): String =
	identifier.replaceAll('\\.','_').replaceAll('::','__');
