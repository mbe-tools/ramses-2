<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_nlxhYNj9EeiIc5Eb0h0CDw" name="workflow" description="Operations of validation for every architecture">
  <components xsi:type="workflow.components:ModelReader" xmi:id="_zwvZoP8jEeqjUK5cPU4ZFw" name="${NameLoadRefinementTransformationModules}" modelSlot="atlHelper" modelURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/atlHelper.helpers" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_p_SfgAbrEeuDOaqjzi9xMA" name="${NameLoadRefinementTransformationModules}" modelSlot="atlModesHelper" modelURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/modes_refinement.helpers" modelElementIndex="0"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_BPfdwP8kEeqjUK5cPU4ZFw" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/load_refinement_core.workflow">
    <propertyValues xmi:id="_Yvtm0P8kEeqjUK5cPU4ZFw" name="ramses_core_transformation_path" defaultValue="${ramses_core_transformation_path}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_T2mi0P8kEeqjUK5cPU4ZFw" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/load_validation_core.workflow">
    <propertyValues xmi:id="_bldr8P8kEeqjUK5cPU4ZFw" name="ramses_core_transformation_path" defaultValue="${ramses_core_transformation_path}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_-EfTkEjHEeqn2-IY0znzPQ" name="ramses_core_transformation_path" defaultValue="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/transformations/atl/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_R7PqEE5DEeqIOpzmJvnc-w" name="ramses_core_transformation_pdc_path" defaultValue="${ramses_core_transformation_path}local_connections/PeriodicDelayedCommunications/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_-EfTkUjHEeqn2-IY0znzPQ" name="ramses_core_transformation_helpers_path" defaultValue="${ramses_core_transformation_path}helpers/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_-EfTkkjHEeqn2-IY0znzPQ" name="ramses_core_transformation_tools_path" defaultValue="${ramses_core_transformation_path}tools/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_l14lEEjIEeqn2-IY0znzPQ" name="ramses_core_transformation_uninstantiate_path" defaultValue="${ramses_core_transformation_path}uninstantiate/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_-EfTk0jHEeqn2-IY0znzPQ" name="ramses_core_transformation_refinement_path" defaultValue="${ramses_core_transformation_path}local_connections/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Cf6-QKDGEeqcwfXgm86T9A" name="ramses_core_transformation_remote_connections_refinement_path" defaultValue="${ramses_core_transformation_path}remote_connections/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_cpUKQKT-Eeqi7MtCpWPvXA" name="ramses_core_transformation_modes_refinement_path" defaultValue="${ramses_core_transformation_path}modes/"/>
  <properties xmi:id="_aJjDgKgwEeqI3K8yQutKtw" name="ramses_core_transformation_ba_path" defaultValue="${ramses_core_transformation_path}ba/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_3R3QkEjEEeqn2-IY0znzPQ" fileURI="default.properties"/>
</workflow:Workflow>
