<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowramsesmodes="https://mem4csd.telecom-paris.fr/ramses/workflowramsesmodes" xmi:id="_9UTd8F4qEeqYp8ezKVdS_w" name="workflow">
  <components xsi:type="workflowramsesmodes:ConditionEvaluationModes" xmi:id="_sYfrYKfcEeqjWv6l-exT6w" name="conditionEvaluationModes" instanceModelSlot="sourceAadlInstance" resultModelSlot="hasModes"/>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_fEm60KfZEeqkUpYD5uVZcw" name="hasModes" conditionSlot="hasModes">
    <onTrue xmi:id="_6bgTIKfcEeqjWv6l-exT6w" name="onTrue">
      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_GlMW4KffEeqjWv6l-exT6w" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/refine_modes_core.workflow">
        <propertyValues xmi:id="_wi_H8AbpEeuDOaqjzi9xMA" name="refined_aadl_file" defaultValue="${modes_file}.aadl">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_NbncJKffEeqjWv6l-exT6w" name="refined_trace_file" defaultValue="${modes_file}.arch_tarce">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_g4ZtgKf2EeqjWv6l-exT6w" name="output_dir" defaultValue="${output_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
      </components>
      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_yaro4P5jEeqjUK5cPU4ZFw" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/instanciate.workflow">
        <propertyValues xmi:id="_5uB7MAbpEeuDOaqjzi9xMA" name="source_aadl_file" defaultValue="${modes_file}.aadl">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_-jzXUAbpEeuDOaqjzi9xMA" name="system_implementation_name" defaultValue="refined_model_for_modes.impl">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
      </components>
      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_LcCxEKffEeqjWv6l-exT6w" name="workflowDelegation" workflowURI="${refinement_workflow}">
        <propertyValues xmi:id="_LcCxFKffEeqjWv6l-exT6w" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_LcCxFaffEeqjWv6l-exT6w" name="refined_trace_file" defaultValue="${refined_trace_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_YvY_AKf2EeqjWv6l-exT6w" name="output_dir" defaultValue="${output_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_t4u5QP0GEeq3Gs5l9Sfxsw" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_9U6-0AboEeuDOaqjzi9xMA" name="source_file_name" defaultValue="${source_file_name}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_CjG68AbpEeuDOaqjzi9xMA" name="target" defaultValue="${target}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
      </components>
    </onTrue>
    <onFalse xmi:id="_7J0KoKfcEeqjWv6l-exT6w" name="onFalse">
      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_Blx6MF4rEeqYp8ezKVdS_w" name="workflowDelegation" workflowURI="${refinement_workflow}">
        <propertyValues xmi:id="_U35OoF4sEeqYp8ezKVdS_w" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_a8OIkF4sEeqYp8ezKVdS_w" name="refined_trace_file" defaultValue="${refined_trace_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_dZ8HoH_rEeqTUOoTKckx6g" name="core_runtime_dir" defaultValue="${core_runtime_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_GLkBAKgCEeqjWv6l-exT6w" name="output_dir" defaultValue="${output_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_yJ8PwP0GEeq3Gs5l9Sfxsw" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file}"/>
        <propertyValues xmi:id="_SYrtgAlPEeuixIGZFNyO-g" name="target" defaultValue="${target}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
      </components>
    </onFalse>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_iXtpwF4sEeqYp8ezKVdS_w" name="workflowDelegation" workflowURI="${code_generation_workflow}">
    <propertyValues xmi:id="_c5Ho8ATGEeuI-KRpR8uPjw" name="target_install_dir" defaultValue="${target_install_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_tSi_oF4sEeqYp8ezKVdS_w" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_2ok64F4sEeqYp8ezKVdS_w" name="refined_trace_file" defaultValue="${refined_trace_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_QNdvsOe8Eeqy-t86Uy9Gdw" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_-EMX4F4sEeqYp8ezKVdS_w" name="include_dir" defaultValue="${include_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_7D_9UF4sEeqYp8ezKVdS_w" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_jpTz8H_rEeqTUOoTKckx6g" name="core_runtime_dir" defaultValue="${core_runtime_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_NhciEF7HEeqTbYoc6DiMAg" name="workflowDelegation" workflowURI="${code_compilation_workflow}">
    <propertyValues xmi:id="_QyifYF7HEeqTbYoc6DiMAg" name="working_dir" defaultValue="${output_dir}/generated-code/${target}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_7D_9UF4sEeqYp8ezKVdS_w" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <propertiesFiles xmi:id="_WCU0QAcLEeuDOaqjzi9xMA" fileURI="../default.properties"/>
</workflow:Workflow>
