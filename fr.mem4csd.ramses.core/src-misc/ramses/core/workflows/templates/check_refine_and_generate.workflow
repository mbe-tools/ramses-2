<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowramses="https://mem4csd.telecom-paris.fr/ramses/workflowramses" xmi:id="_9UTd8F4qEeqYp8ezKVdS_w" name="workflow">
  <components xsi:type="workflowramses:ConditionEvaluationTarget" xmi:id="_q7DTYPHSEeqqFvtavUcehg" name="conditionEvaluationTarget" instanceModelSlot="sourceAadlInstance" resultModelSlot="needs${target}Refinement" target="${target}"/>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_23cM4PHSEeqqFvtavUcehg" name="needs ${target} refinement" conditionSlot="needs${target}Refinement">
    <onTrue xmi:id="_-jEb0PHSEeqqFvtavUcehg" name="onTrue">
      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_qvAVIAb_EeuDOaqjzi9xMA" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/templates/refine_and_generate.workflow">
        <propertyValues xmi:id="_zhVtcAb_EeuDOaqjzi9xMA" name="code_generation_workflow" defaultValue="${code_generation_workflow}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_2HdV4DsaEeusgZteSoplVA" name="code_compilation_workflow" defaultValue="${code_compilation_workflow}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_A35lQAcIEeuDOaqjzi9xMA" name="refinement_workflow" defaultValue="${refinement_workflow}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_ECOzQAcIEeuDOaqjzi9xMA" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_GiiM0AcIEeuDOaqjzi9xMA" name="refined_trace_file" defaultValue="${refined_trace_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_JcFM0AcIEeuDOaqjzi9xMA" name="output_dir" defaultValue="${output_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_KcAYwAcIEeuDOaqjzi9xMA" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_LHp90AcIEeuDOaqjzi9xMA" name="source_file_name" defaultValue="${source_file_name}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_Loc9wAcIEeuDOaqjzi9xMA" name="target" defaultValue="${target}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_PdE0AAcKEeuDOaqjzi9xMA" name="modes_file" defaultValue="${modes_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_eAtckAcKEeuDOaqjzi9xMA" name="system_implementation_name" defaultValue="${system_implementation_name}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_00J_MAcKEeuDOaqjzi9xMA" name="source_aadl_file" defaultValue="${source_aadl_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_7LPdEAcKEeuDOaqjzi9xMA" name="core_runtime_dir" defaultValue="${core_runtime_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_D80ncAcLEeuDOaqjzi9xMA" name="target_install_dir" defaultValue="${target_install_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_HlfXQAcLEeuDOaqjzi9xMA" name="runtime_scheme" defaultValue="${runtime_scheme}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_LPVLwAcLEeuDOaqjzi9xMA" name="include_dir" defaultValue="${include_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
      </components>
    </onTrue>
    <onFalse xmi:id="_7KzfgPHSEeqqFvtavUcehg" name="onFalse: exit, nothing to do"/>
  </components>
  <properties xmi:id="_RvqH0F4rEeqYp8ezKVdS_w" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Wro5oF4rEeqYp8ezKVdS_w" name="include_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_auww0F4rEeqYp8ezKVdS_w" name="source_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_4lCdAF4rEeqYp8ezKVdS_w" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_OYkawF4sEeqYp8ezKVdS_w" name="instance_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_XbVbwF4sEeqYp8ezKVdS_w" name="refined_aadl_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_douyIF4sEeqYp8ezKVdS_w" name="refined_trace_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_WgXrsP0GEeq3Gs5l9Sfxsw" name="refinement_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_qGhPwP5iEeqjUK5cPU4ZFw" name="validation_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_YSXcMP5jEeqjUK5cPU4ZFw" name="code_generation_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_79O2cDsaEeusgZteSoplVA" name="code_compilation_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_icT9IP5iEeqjUK5cPU4ZFw" name="target">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_y9mXMNpdEeq9fI4NRnUBRA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
