<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_9UTd8F4qEeqYp8ezKVdS_w" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_a9nxcP53EeqjUK5cPU4ZFw" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/templates/load_target_resources.workflow">
    <propertyValues xmi:id="_kpTMkP53EeqjUK5cPU4ZFw" name="load_aadl_resources_workflow" defaultValue="${load_aadl_resources_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_nn1aQP53EeqjUK5cPU4ZFw" name="load_transformation_resources_workflow" defaultValue="${load_transformation_resources_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_8IoxUP8mEeqjUK5cPU4ZFw" name="ramses_core_plugin" defaultValue="${ramses_core_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_GEn5UP8nEeqjUK5cPU4ZFw" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_5clOQP53EeqjUK5cPU4ZFw" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/templates/refine_and_generate.workflow">
    <propertyValues xmi:id="__aG8oP53EeqjUK5cPU4ZFw" name="code_generation_workflow" defaultValue="${code_generation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_gcTbkDsbEeusgZteSoplVA" name="code_compilation_workflow" defaultValue="${code_compilation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_ws2IkAS6EeuI-KRpR8uPjw" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_s7_GEAS6EeuI-KRpR8uPjw" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_mF6mIAS6EeuI-KRpR8uPjw" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__aG8of53EeqjUK5cPU4ZFw" name="include_dir" defaultValue="${include_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__aG8pf53EeqjUK5cPU4ZFw" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__aG8pv53EeqjUK5cPU4ZFw" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__aG8p_53EeqjUK5cPU4ZFw" name="refined_trace_file" defaultValue="${refined_trace_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__aG8qP53EeqjUK5cPU4ZFw" name="refinement_workflow" defaultValue="${refinement_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__aHjs_53EeqjUK5cPU4ZFw" name="target" defaultValue="${target}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__aHjtP53EeqjUK5cPU4ZFw" name="validation_report_file" defaultValue="${validation_report_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__aHjtf53EeqjUK5cPU4ZFw" name="validation_workflow" defaultValue="${validation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_5w1pwAZIEeuDOaqjzi9xMA" name="expose_runtime_shared_resources" defaultValue="${expose_runtime_shared_resources}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_YSXcMP5jEeqjUK5cPU4ZFw" name="code_generation_workflow"/>
  <properties xmi:id="_jT1pYDsbEeusgZteSoplVA" name="code_compilation_workflow"/>
  <properties xmi:id="_Wro5oF4rEeqYp8ezKVdS_w" name="include_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_OYkawF4sEeqYp8ezKVdS_w" name="instance_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_ry6TkP53EeqjUK5cPU4ZFw" name="load_aadl_resources_workflow" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_s1SWkP53EeqjUK5cPU4ZFw" name="load_transformation_resources_workflow" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_RvqH0F4rEeqYp8ezKVdS_w" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_XbVbwF4sEeqYp8ezKVdS_w" name="refined_aadl_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_douyIF4sEeqYp8ezKVdS_w" name="refined_trace_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_WgXrsP0GEeq3Gs5l9Sfxsw" name="refinement_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_auww0F4rEeqYp8ezKVdS_w" name="source_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_4lCdAF4rEeqYp8ezKVdS_w" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_icT9IP5iEeqjUK5cPU4ZFw" name="target">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_fp81AF7GEeqTbYoc6DiMAg" name="validation_report_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_qGhPwP5iEeqjUK5cPU4ZFw" name="validation_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_y9mXMNpdEeq9fI4NRnUBRA" fileURI="../default.properties"/>
</workflow:Workflow>
