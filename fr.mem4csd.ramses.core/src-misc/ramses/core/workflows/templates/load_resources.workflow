<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_nlxhYNj9EeiIc5Eb0h0CDw" name="workflow" description="Operations of validation for every architecture">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_1GojAORGEem2b9tZ1uGgew" name="load_aadl_resources" description="" workflowURI="${load_aadl_resources_workflow}"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_rWPbIP52EeqjUK5cPU4ZFw" name="load_transformation_resources" workflowURI="${load_transformation_resources_workflow}"/>
  <properties xmi:id="_H5NFUP53EeqjUK5cPU4ZFw" name="load_aadl_resources_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_KXOm8P53EeqjUK5cPU4ZFw" name="load_transformation_resources_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
</workflow:Workflow>
