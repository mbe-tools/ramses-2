<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_9UTd8F4qEeqYp8ezKVdS_w" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_a9nxcP53EeqjUK5cPU4ZFw" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/templates/load_resources.workflow">
    <propertyValues xmi:id="_kpTMkP53EeqjUK5cPU4ZFw" name="load_aadl_resources_workflow" defaultValue="${load_aadl_resources_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_nn1aQP53EeqjUK5cPU4ZFw" name="load_transformation_resources_workflow" defaultValue="${load_transformation_resources_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_Wgtr0P54EeqjUK5cPU4ZFw" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/instanciate.workflow">
    <propertyValues xmi:id="_5COYEP54EeqjUK5cPU4ZFw" name="instance_model_file" defaultValue="${instance_model_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_yh--MP54EeqjUK5cPU4ZFw" name="source_aadl_file" defaultValue="${source_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_1KlYIP54EeqjUK5cPU4ZFw" name="system_implementation_name" defaultValue="${system_implementation_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_5clOQP53EeqjUK5cPU4ZFw" name="workflowDelegation" workflowURI="${validation_workflow}">
    <propertyValues xmi:id="_Sx4CQAS9EeuI-KRpR8uPjw" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_V_GzQAS9EeuI-KRpR8uPjw" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__aG8pf53EeqjUK5cPU4ZFw" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__aHjs_53EeqjUK5cPU4ZFw" name="target" defaultValue="${target}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__aHjtP53EeqjUK5cPU4ZFw" name="validation_report_file" defaultValue="${validation_report_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_Wro5oF4rEeqYp8ezKVdS_w" name="include_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_OYkawF4sEeqYp8ezKVdS_w" name="instance_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_ry6TkP53EeqjUK5cPU4ZFw" name="load_aadl_resources_workflow" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_s1SWkP53EeqjUK5cPU4ZFw" name="load_transformation_resources_workflow" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_RvqH0F4rEeqYp8ezKVdS_w" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_auww0F4rEeqYp8ezKVdS_w" name="source_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_4lCdAF4rEeqYp8ezKVdS_w" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_icT9IP5iEeqjUK5cPU4ZFw" name="target">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_fp81AF7GEeqTbYoc6DiMAg" name="validation_report_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_qGhPwP5iEeqjUK5cPU4ZFw" name="validation_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_y9mXMNpdEeq9fI4NRnUBRA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
