<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_PAkpAMixEei-D6bRtwoJ1Q" name="workflow" description="EMFTVM operations needed">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_kWs2AKDHEeqcwfXgm86T9A" name="workflowDelegation" workflowURI="${ramses_core_workflows_path}templates/refine_remotes.workflow">
    <propertyValues xmi:id="_btCD4Oh4Eeqy-t86Uy9Gdw" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_oaDC8KEEEeqR3ds1UXwt-g" name="include_dir" defaultValue="${include_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_8cvn4KGWEeq8nqBB_6ScqA" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_BtSUUKgBEeqjWv6l-exT6w" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_wMqMsAjYEeuixIGZFNyO-g" name="refinement_workflow" defaultValue="${locals_refinement_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_wMqMsgjYEeuixIGZFNyO-g" name="target_install_dir" defaultValue="${target_install_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_1H3ZQKduEeqFSPQQ-jzDEA" name="workflowDelegation" workflowURI="${ramses_core_workflows_path}templates/refine_and_generate.workflow">
    <propertyValues xmi:id="_Vp6q4Oh4Eeqy-t86Uy9Gdw" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_1H3ZS6duEeqFSPQQ-jzDEA" name="include_dir" defaultValue="${include_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_1H3ZTaduEeqFSPQQ-jzDEA" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_JbiZcKgBEeqjWv6l-exT6w" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_iwYV8AilEeulUut1LEEszg" name="refinement_workflow" defaultValue="${locals_refinement_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_mmuVYAilEeulUut1LEEszg" name="code_generation_workflow" defaultValue="${code_generation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_6K788DsbEeusgZteSoplVA" name="code_compilation_workflow" defaultValue="${code_compilation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_3JSu0AiwEeulUut1LEEszg" name="target_install_dir" defaultValue="${target_install_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_R4JPcAlTEeuixIGZFNyO-g" name="target" defaultValue="${target}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_RAT4AKDmEeqR3ds1UXwt-g" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_iLkBkO3eEeqXMuraMJ1XYQ" name="remote_communications_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_62lFoO3lEeqXMuraMJ1XYQ" name="local_communications_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_jAEpMKDmEeqR3ds1UXwt-g" name="refined_trace_file" defaultValue="${remote_communications_file}.arch_trace">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_mcjXIKDmEeqR3ds1UXwt-g" name="validation_report_file" defaultValue="${remote_communications_file}.validation_report">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_2FwDgKE8EeqgtNEZpZXkFQ" name="refined_aadl_file" defaultValue="${remote_communications_file}.aadl">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_YZkB4KEEEeqR3ds1UXwt-g" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_QAWnUAemEeu1QfSkbofkZQ" name="has_remote_connections_cond">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Px0pwAhvEeuaPYhX2kwqog" name="remotes_refinement_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_2eVaAAhzEeuaPYhX2kwqog" name="locals_refinement_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_g3Qf4AifEeulUut1LEEszg" name="protocols" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_DSP-wOhyEeqy-t86Uy9Gdw" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
