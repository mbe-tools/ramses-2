<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowramses="https://mem4csd.telecom-paris.fr/ramses/workflowramses" xmi:id="_PAkpAMixEei-D6bRtwoJ1Q" name="workflow" description="EMFTVM operations">
  <components xsi:type="workflowramses:ConditionEvaluationProtocol" xmi:id="_t3VgcAieEeulUut1LEEszg" name="conditionEvaluationProtocol" instanceModelSlot="sourceAadlInstance" resultModelSlot="${has_remote_connections_cond}" protocols="${protocols}"/>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_jfO8AKcpEeqR56JNvTqeEA" name="hasRemoteConnections" conditionSlot="${has_remote_connections_cond}">
    <onTrue xmi:id="_n0uVkKcpEeqR56JNvTqeEA" name="onTrue">
      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_YowQUJtlEeqJ3cxvaDjAOg" name="workflowDelegation" workflowURI="${remotes_refinement_workflow}">
        <propertyValues xmi:id="_VCf9QKD1EeqDXb0OjUP0yw" name="refined_trace_file" defaultValue="${remote_communications_file}.arch_trace">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_eDYKEKEEEeqR3ds1UXwt-g" name="output_dir" defaultValue="${output_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_NE2cgKE9EeqxdbEz6rHidg" name="refined_aadl_file" defaultValue="${remote_communications_file}.aadl">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
      </components>
    </onTrue>
  </components>
  <properties xmi:id="_RAT4AKDmEeqR3ds1UXwt-g" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_iLkBkO3eEeqXMuraMJ1XYQ" name="remote_communications_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_62lFoO3lEeqXMuraMJ1XYQ" name="local_communications_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_jAEpMKDmEeqR3ds1UXwt-g" name="refined_trace_file" defaultValue="${remote_communications_file}.arch_trace">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_mcjXIKDmEeqR3ds1UXwt-g" name="validation_report_file" defaultValue="${remote_communications_file}.validation_report">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_2FwDgKE8EeqgtNEZpZXkFQ" name="refined_aadl_file" defaultValue="${remote_communications_file}.aadl">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_YZkB4KEEEeqR3ds1UXwt-g" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_QAWnUAemEeu1QfSkbofkZQ" name="has_remote_connections_cond">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Px0pwAhvEeuaPYhX2kwqog" name="remotes_refinement_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_2eVaAAhzEeuaPYhX2kwqog" name="locals_refinement_workflow">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_g3Qf4AifEeulUut1LEEszg" name="protocols" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_DSP-wOhyEeqy-t86Uy9Gdw" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
