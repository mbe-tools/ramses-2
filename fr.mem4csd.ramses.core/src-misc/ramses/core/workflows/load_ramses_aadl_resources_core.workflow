<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_m-_JkMrYEeihRZsfUgo3mA" name="workflow" description="AADL operations needed by every architecture">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_Z6ZwMONfEemk0ZYOnPTD3A" name="workflowDelegation" description="load aadl files required for next steps" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/load_aadl_resources_core.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJZMrZEeihRZsfUgo3mA" name="${NameRamsesAadlResourcesReader}" modelSlot="aadl_runtime" modelURI="${predefined_aadl_resource_dir}packages/aadl_runtime.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_TKBwEFTsEeuZSNkeb-Yc7A" name="${NameRamsesAadlResourcesReader}" modelSlot="PeriodicDelayed_runtime" modelURI="${predefined_aadl_resource_dir}packages/PeriodicDelayed_runtime.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_Vi42IFTyEeuZSNkeb-Yc7A" name="${NameRamsesAadlResourcesReader}" modelSlot="PeriodicDelayedMutex_runtime" modelURI="${predefined_aadl_resource_dir}packages/PeriodicDelayedMutex_runtime.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJacrZEeihRZsfUgo3mA" name="${NameRamsesAadlResourcesReader}" modelSlot="RAMSES_processors" modelURI="${predefined_aadl_resource_dir}packages/RAMSES_processors.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJasrZEeihRZsfUgo3mA" name="${NameRamsesAadlResourcesReader}" modelSlot="RAMSES_buses" modelURI="${predefined_aadl_resource_dir}packages/RAMSES_buses.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJeMrZEeihRZsfUgo3mA" name="${NameRamsesAadlResourcesReader}" modelSlot="RAMSES_properties" modelURI="${predefined_aadl_resource_dir}propertysets/RAMSES.aadl" resolveURI="false" modelElementIndex="0"/>
  <properties xmi:id="_QYRvgH_oEeqTUOoTKckx6g" name="predefined_aadl_resource_dir" defaultValue="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_YQb28EhjEeqn2-IY0znzPQ" fileURI="default.properties"/>
</workflow:Workflow>
