<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflowemftvm="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowemftvm" xmlns:workflowramses="https://mem4csd.telecom-paris.fr/ramses/workflowramses" xmi:id="_d5lC0KT_Eeqi7MtCpWPvXA" name="workflow">
  <components xsi:type="workflowemftvm:EmftVmTransformer" xmi:id="_c3FOkKUAEeq1YeAByobchg" name="Refinement transformation for modes and mode switches" registerDependencyModels="true" discardExtraRootElements="true">
    <rulesModelSlot>Services</rulesModelSlot>
    <rulesModelSlot>ACGServices</rulesModelSlot>
    <rulesModelSlot>PackagesTools</rulesModelSlot>
    <rulesModelSlot>PropertiesTools</rulesModelSlot>
    <rulesModelSlot>BehaviorAnnexTools</rulesModelSlot>
    <rulesModelSlot>AADLICopyHelpers</rulesModelSlot>
    <rulesModelSlot>AADLCopyHelpers</rulesModelSlot>
    <rulesModelSlot>Uninstanciate</rulesModelSlot>
    <rulesModelSlot>Features</rulesModelSlot>
    <rulesModelSlot>Connections</rulesModelSlot>
    <rulesModelSlot>Types</rulesModelSlot>
    <rulesModelSlot>Implementations</rulesModelSlot>
    <rulesModelSlot>Modes</rulesModelSlot>
    <rulesModelSlot>Properties</rulesModelSlot>
    <rulesModelSlot>UninstanciateOverride</rulesModelSlot>
    <rulesModelSlot>ThreadsUninstanciateOverride</rulesModelSlot>
    <rulesModelSlot>SubprogramsUninstanciateOverride</rulesModelSlot>
    <rulesModelSlot>DataUninstanciateOverride</rulesModelSlot>
    <rulesModelSlot>ModesCommonRefinementSteps</rulesModelSlot>
    <rulesModelSlot>LanguageSpecificitiesC</rulesModelSlot>
    <inputModels xmi:id="_EmUF9vGhEemHmY6XjiEv8w" modelName="IN" modelSlot="sourceAadlInstance" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUF-fGhEemHmY6XjiEv8w" modelName="TOOLS" modelSlot="atlModesHelper" metamodelName="ATLHELPER" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/helpers"/>
    <inputModels xmi:id="_EmUF-vGhEemHmY6XjiEv8w" modelName="BASE_TYPES" modelSlot="Base_Types" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba"/>
    <inputModels xmi:id="_EmUGCPGhEemHmY6XjiEr8w" modelName="DATA_MODEL" modelSlot="Data_Model" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGAPGhEemHmY6XjiEv8w" modelName="AADL_RUNTIME" modelSlot="aadl_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGBPGhEemHmY6XjiEv8w" modelName="RAMSES_PROCESSORS" modelSlot="RAMSES_processors" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGBfGhEemHmY6XjiEv8w" modelName="RAMSES_BUSES" modelSlot="RAMSES_buses" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGBvGhEemHmY6XjiEv8w" modelName="AADL_PROJECT" modelSlot="AADL_Project" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGB_GhEemHmY6XjiEv8w" modelName="COMMUNICATION_PROPERTIES" modelSlot="Communication_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGCfGhEemHmY6XjiEv8w" modelName="DEPLOYMENT_PROPERTIES" modelSlot="Deployment_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGCvGhEemHmY6XjiEv8w" modelName="MEMORY_PROPERTIES" modelSlot="Memory_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGC_GhEemHmY6XjiEv8w" modelName="MODELING_PROPERTIES" modelSlot="Modeling_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGDPGhEemHmY6XjiEv8w" modelName="PROGRAMMING_PROPERTIES" modelSlot="Programming_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGDfGhEemHmY6XjiEv8w" modelName="THREAD_PROPERTIES" modelSlot="Thread_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGDvGhEemHmY6XjiEv8w" modelName="TIMING_PROPERTIES" modelSlot="Timing_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGD_GhEemHmY6XjiEv8w" modelName="ARINC653" modelSlot="ARINC653" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGEPGhEemHmY6XjiEv8w" modelName="CODE_GENERATION_PROPERTIES" modelSlot="Code_Generation_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGE_GhEemHmY6XjiEv8w" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGFvGhEemHmY6XjiEv8w" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <outputModels xmi:id="_VmBpwPmAEemTadmi341e_w" modelName="OUT_TRACE" modelSlot="modesRefinedTraceModel" metamodelName="ARCH_TRACE" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/arch_trace"/>
    <inputOutputModels xmi:id="_e6V98PGiEemHmY6XjiEv8w" modelName="OUT" modelSlot="modesRefinedAadlModel" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba" extension="aadl"/>
  </components>
  <components xsi:type="workflowramses:AadlWriter" xmi:id="_iRH_8KUBEeq1YeAByobchg" name="${NameSaveRefinedAADLModel}" modelSlot="modesRefinedAadlModel" modelURI="${refined_aadl_file}" unloadAfter="true" resolveURI="false"/>
  <components xsi:type="workflowramses:TraceWriter" xmi:id="_J03xkKgkEeqIAr4DIJa9bg" name="${NameSaveTransformationTraceModel}" modelSlot="modesRefinedTraceModel" modelURI="${refined_trace_file}"/>
  <properties xmi:id="_9rIxUAbmEeuDOaqjzi9xMA" name="refined_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="__hUP0AbmEeuDOaqjzi9xMA" name="refined_trace_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_dT5P8KUMEeq1YeAByobchg" fileURI="default.properties"/>
</workflow:Workflow>
