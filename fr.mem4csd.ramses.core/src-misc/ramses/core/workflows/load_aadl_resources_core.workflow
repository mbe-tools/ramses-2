<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_m-_JkMrYEeihRZsfUgo3mA" name="workflow" description="AADL operations needed by every architecture">
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJa8rZEeihRZsfUgo3mA" name="${NameOsateResourcesReader}" description="" modelSlot="AADL_Project" modelURI="${predeclared_property_sets_path}AADL_Project.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJbsrZEeihRZsfUgo3mA" name="${NameOsateResourcesReader}" modelSlot="Deployment_Properties" modelURI="${predeclared_property_sets_path}Deployment_Properties.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJbMrZEeihRZsfUgo3mA" name="${NameOsateResourcesReader}" modelSlot="Communication_Properties" modelURI="${predeclared_property_sets_path}Communication_Properties.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJb8rZEeihRZsfUgo3mA" name="${NameOsateResourcesReader}" modelSlot="Memory_Properties" modelURI="${predeclared_property_sets_path}Memory_Properties.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJccrZEeihRZsfUgo3mA" name="${NameOsateResourcesReader}" modelSlot="Programming_Properties" modelURI="${predeclared_property_sets_path}Programming_Properties.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJcsrZEeihRZsfUgo3mA" name="${NameOsateResourcesReader}" modelSlot="Thread_Properties" modelURI="${predeclared_property_sets_path}Thread_Properties.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJc8rZEeihRZsfUgo3mA" name="${NameOsateResourcesReader}" modelSlot="Timing_Properties" modelURI="${predeclared_property_sets_path}Timing_Properties.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJcMrZEeihRZsfUgo3mA" name="${NameOsateResourcesReader}" modelSlot="Modeling_Properties" modelURI="${predeclared_property_sets_path}Modeling_Properties.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LneJbcrZEeihRZsfUgo3mA" name="${NameOsateResourcesReader}" description="" modelSlot="Data_Model" modelURI="${sei_predeclared_property_sets_path}properties/Data_Model.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_kiIHgEhiEeqn2-IY0znzPQ" name="${NameOsateResourcesReader}" modelSlot="ARINC653" modelURI="${sei_predeclared_property_sets_path}properties/ARINC653.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LndiUMrZEeihRZsfUgo3mA" name="${NameOsateResourcesReader}" modelSlot="Base_Types" modelURI="${sei_predeclared_property_sets_path}packages/Base_Types.aadl" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_WW5l4H_vEeqTUOoTKckx6g" name="${NameOsateResourcesReader}" modelSlot="Code_Generation_Properties" modelURI="${sei_predeclared_property_sets_path}properties/Code_Generation_Properties.aadl" resolveURI="false" modelElementIndex="0"/>
  <properties xmi:id="_e9sW0NSsEemSLuQ4bXS-7Q" name="predeclared_property_sets_path" defaultValue="${scheme}${predeclared_property_sets_plugin}resources/properties/Predeclared_Property_Sets/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_oBvKMNSsEemSLuQ4bXS-7Q" name="sei_predeclared_property_sets_path" defaultValue="${scheme}${sei_predeclared_property_sets_plugin}resources/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_uHWAMEhgEeqn2-IY0znzPQ" fileURI="default.properties"/>
</workflow:Workflow>
