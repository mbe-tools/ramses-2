<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_nlxhYNj9EeiIc5Eb0h0CDw" name="workflow" description="Operations of validation for every architecture">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_ZtjYILbjEeqC0OmnAei34g" name="workflowDelegation" workflowURI="${scheme}${atl_transformation_utilities_plugin}utils/workflows/load_atl_transformation_utils.workflow">
    <propertyValues xmi:id="_dYhOALbjEeqC0OmnAei34g" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_ZkFYcLb2EeqC0OmnAei34g" name="atl_transformation_utilities_plugin" defaultValue="${atl_transformation_utilities_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_Dwdv0PWCEemgUp7UyM-vIw" name="${NameLoadRefinementTransformationModules}" modelSlot="Uninstanciate" modelURI="${ramses_core_transformation_uninstantiate_path}Uninstanciate.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_Xa4E8PWCEemgUp7UyM-vIw" name="${NameLoadRefinementTransformationModules}" modelSlot="Features" modelURI="${ramses_core_transformation_uninstantiate_path}Features.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_Zv9F8PWCEemgUp7UyM-vIw" name="${NameLoadRefinementTransformationModules}" modelSlot="Implementations" modelURI="${ramses_core_transformation_uninstantiate_path}Implementations.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_f-XwAPWCEemgUp7UyM-vIw" name="${NameLoadRefinementTransformationModules}" modelSlot="Properties" modelURI="${ramses_core_transformation_uninstantiate_path}Properties.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_iZtKAPWCEemgUp7UyM-vIw" name="${NameLoadRefinementTransformationModules}" modelSlot="Modes" modelURI="${ramses_core_transformation_uninstantiate_path}Modes.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_7JSCIPWCEemgUp7UyM-vIw" name="${NameLoadRefinementTransformationModules}" modelSlot="Flows" modelURI="${ramses_core_transformation_uninstantiate_path}Flows.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_9xR_IPWCEemgUp7UyM-vIw" name="${NameLoadRefinementTransformationModules}" modelSlot="Types" modelURI="${ramses_core_transformation_uninstantiate_path}Types.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_-x2rUPWCEemgUp7UyM-vIw" name="${NameLoadRefinementTransformationModules}" modelSlot="Connections" modelURI="${ramses_core_transformation_uninstantiate_path}Connections.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_CEn-wNj-EeiIc5Eb0h0CDw" name="${NameLoadRefinementTransformationModules}" modelSlot="ACGServices" modelURI="${ramses_core_transformation_helpers_path}ACGServices.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_1vT9wPTHEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="UninstanciateOverride" modelURI="${ramses_core_transformation_refinement_path}UninstanciateOverride.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_7GLUwPTHEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="DataUninstanciateOverride" modelURI="${ramses_core_transformation_refinement_path}DataUninstanciateOverride.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_9YNBwPTHEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="SubprogramsUninstanciateOverride" modelURI="${ramses_core_transformation_refinement_path}SubprogramsUninstanciateOverride.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_AFGF0PTIEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="ThreadsUninstanciateOverride" modelURI="${ramses_core_transformation_refinement_path}ThreadsUninstanciateOverride.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_CgvB0PTIEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="ProcessesUninstanciateOverride" modelURI="${ramses_core_transformation_refinement_path}ProcessesUninstanciateOverride.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_FLeJwPTIEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="SubprogramCallsCommonRefinementSteps" modelURI="${ramses_core_transformation_refinement_path}SubprogramCallsCommonRefinementSteps.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_HwlK0PTIEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="PortsCommonRefinementSteps" modelURI="${ramses_core_transformation_refinement_path}PortsCommonRefinementSteps.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_LMJ50PTIEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="DispatchCommonRefinementSteps" modelURI="${ramses_core_transformation_refinement_path}DispatchCommonRefinementSteps.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_VucM0PTIEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="BehaviorAnnexCommonRefinementSteps" modelURI="${ramses_core_transformation_refinement_path}BehaviorAnnexCommonRefinementSteps.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_XkZB0PTIEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="ExpandProcessesPorts" modelURI="${ramses_core_transformation_refinement_path}ExpandProcessesPorts.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_aGwQ4PTIEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="ExpandThreadsPorts" modelURI="${ramses_core_transformation_refinement_path}ExpandThreadsPorts.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_4UWMAC5oEeuiYPByRdHPMg" name="${NameLoadRefinementTransformationModules}" modelSlot="ExpandThreadsPortsTarget" modelURI="${ramses_core_transformation_refinement_path}ExpandThreadsPortsTarget.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_d0Lk4PTIEemedaL5fW-w2g" name="${NameLoadRefinementTransformationModules}" modelSlot="ExpandThreadsDispatchProtocol" modelURI="${ramses_core_transformation_refinement_path}ExpandThreadsDispatchProtocol.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_DwPnkC5pEeuiYPByRdHPMg" name="${NameLoadRefinementTransformationModules}" modelSlot="ExpandThreadsDispatchProtocolTarget" modelURI="${ramses_core_transformation_refinement_path}ExpandThreadsDispatchProtocolTarget.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_w2N_gPTVEem6xNJJeqmUXg" name="${NameLoadRefinementTransformationModules}" modelSlot="SharedRules" modelURI="${ramses_core_transformation_refinement_path}SharedRules.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_6xTvoPTVEem6xNJJeqmUXg" name="${NameLoadRefinementTransformationModules}" modelSlot="SharedRules_No_Mutex" modelURI="${ramses_core_transformation_refinement_path}SharedRules_No_Mutex.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_72a9kPTVEem6xNJJeqmUXg" name="${NameLoadRefinementTransformationModules}" modelSlot="EventDataPorts_LowET" modelURI="${ramses_core_transformation_refinement_path}EventDataPorts_LowET.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_vpIHMKTSEeq1YeAByobchg" name="${NameLoadRefinementTransformationModules}" modelSlot="EventDataPorts_LowMFP" modelURI="${ramses_core_transformation_refinement_path}EventDataPorts_LowMFP.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_idLAoKyQEeqP46nF7LuYqg" name="${NameLoadRefinementTransformationModules}" description="" modelSlot="ExpandThreadsPortsWithSharedResources" modelURI="${ramses_core_transformation_refinement_path}ExpandThreadsPortsWithSharedResources.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_QGk5gPTVEem6xNJJeqmUXg" name="${NameLoadRefinementTransformationModules}" modelSlot="LanguageSpecificitiesC" modelURI="${ramses_core_transformation_helpers_path}LanguageSpecificitiesC.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_JYCFkKDGEeqcwfXgm86T9A" name="${NameLoadRefinementTransformationModules}" modelSlot="RemoteConnectionsCommonRefinementSteps" modelURI="${ramses_core_transformation_remote_connections_refinement_path}RemoteConnectionsCommonRefinementSteps.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_U_pB4KDGEeqcwfXgm86T9A" name="${NameLoadRefinementTransformationModules}" modelSlot="UninstanciateOverrideForRemoteConnections" modelURI="${ramses_core_transformation_remote_connections_refinement_path}UninstanciateOverrideForRemoteConnections.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_0bBwIKT3Eeqi7MtCpWPvXA" name="${NameLoadRefinementTransformationModules}" modelSlot="ModesCommonRefinementSteps" modelURI="${ramses_core_transformation_modes_refinement_path}ModesCommonRefinementSteps.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_dIjLQKgwEeqI3K8yQutKtw" name="${NameLoadRefinementTransformationModules}" modelSlot="BehaviorActionBlock" modelURI="${ramses_core_transformation_ba_path}BehaviorActionBlock.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_di3IsKgwEeqI3K8yQutKtw" name="${NameLoadRefinementTransformationModules}" modelSlot="BehaviorCondition" modelURI="${ramses_core_transformation_ba_path}BehaviorCondition.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_dzAdgKgwEeqI3K8yQutKtw" name="${NameLoadRefinementTransformationModules}" modelSlot="BehaviorSpecification" modelURI="${ramses_core_transformation_ba_path}BehaviorSpecification.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_eB3Y4KgwEeqI3K8yQutKtw" name="${NameLoadRefinementTransformationModules}" modelSlot="BehaviorTime" modelURI="${ramses_core_transformation_ba_path}BehaviorTime.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_eP2KgKgwEeqI3K8yQutKtw" name="${NameLoadRefinementTransformationModules}" modelSlot="ElementHolders" modelURI="${ramses_core_transformation_ba_path}ElementHolders.emftvm" resolveURI="false" modelElementIndex="0"/>
  <properties xmi:id="_-EfTkEjHEeqn2-IY0znzPQ" name="ramses_core_transformation_path" defaultValue="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/transformations/atl/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_R7PqEE5DEeqIOpzmJvnc-w" name="ramses_core_transformation_pdc_path" defaultValue="${ramses_core_transformation_path}local_connections/PeriodicDelayedCommunications/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_-EfTkUjHEeqn2-IY0znzPQ" name="ramses_core_transformation_helpers_path" defaultValue="${ramses_core_transformation_path}helpers/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_-EfTkkjHEeqn2-IY0znzPQ" name="ramses_core_transformation_tools_path" defaultValue="${ramses_core_transformation_path}tools/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_l14lEEjIEeqn2-IY0znzPQ" name="ramses_core_transformation_uninstantiate_path" defaultValue="${ramses_core_transformation_path}uninstantiate/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_-EfTk0jHEeqn2-IY0znzPQ" name="ramses_core_transformation_refinement_path" defaultValue="${ramses_core_transformation_path}local_connections/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Cf6-QKDGEeqcwfXgm86T9A" name="ramses_core_transformation_remote_connections_refinement_path" defaultValue="${ramses_core_transformation_path}remote_connections/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_cpUKQKT-Eeqi7MtCpWPvXA" name="ramses_core_transformation_modes_refinement_path" defaultValue="${ramses_core_transformation_path}modes/"/>
  <properties xmi:id="_aJjDgKgwEeqI3K8yQutKtw" name="ramses_core_transformation_ba_path" defaultValue="${ramses_core_transformation_path}ba/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_3R3QkEjEEeqn2-IY0znzPQ" fileURI="default.properties"/>
</workflow:Workflow>
