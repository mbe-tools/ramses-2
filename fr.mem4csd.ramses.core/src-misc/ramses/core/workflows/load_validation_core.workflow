<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_nlxhYNj9EeiIc5Eb0h0CDw" name="workflow" description="Operations of validation for every architecture">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_9DBWQLb2EeqC0OmnAei34g" name="workflowDelegation" workflowURI="${scheme}${atl_transformation_utilities_plugin}utils/workflows/load_atl_transformation_utils.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_CEn-xdj-EeiIc5Eb0h0CDw" name="${NameLoadValidationTransformationModules}" modelSlot="Common" modelURI="${ramses_core_transformation_validation_path}Common.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_CEn-xtj-EeiIc5Eb0h0CDw" name="${NameLoadValidationTransformationModules}" modelSlot="DataInstances" modelURI="${ramses_core_transformation_validation_path}DataInstances.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_CEn-yNj-EeiIc5Eb0h0CDw" name="${NameLoadValidationTransformationModules}" modelSlot="ProcessInstances" modelURI="${ramses_core_transformation_validation_path}ProcessInstances.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_CEn-ydj-EeiIc5Eb0h0CDw" name="${NameLoadValidationTransformationModules}" modelSlot="ConnectionInstances" modelURI="${ramses_core_transformation_validation_path}ConnectionInstances.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_CEn-ytj-EeiIc5Eb0h0CDw" name="${NameLoadValidationTransformationModules}" modelSlot="ProcessorInstances" modelURI="${ramses_core_transformation_validation_path}ProcessorInstances.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_CEn-y9j-EeiIc5Eb0h0CDw" name="${NameLoadValidationTransformationModules}" modelSlot="VirtualProcessorInstances" modelURI="${ramses_core_transformation_validation_path}VirtualProcessorInstances.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_GLJW4EBGEemO67IzcmkMoQ" name="${NameLoadValidationTransformationModules}" modelSlot="VirtualBusInstances" modelURI="${ramses_core_transformation_validation_path}VirtualBusInstances.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_qZMnQNSgEemv4IlehDjRQw" name="${NameLoadValidationTransformationModules}" modelSlot="ThreadInstances" modelURI="${ramses_core_transformation_validation_path}ThreadInstances.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_CEn-zdj-EeiIc5Eb0h0CDw" name="${NameLoadValidationTransformationModules}" modelSlot="FeatureInstances" modelURI="${ramses_core_transformation_validation_path}FeatureInstances.emftvm" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_CEn-ztj-EeiIc5Eb0h0CDw" name="${NameLoadValidationTransformationModules}" modelSlot="Subprograms" modelURI="${ramses_core_transformation_validation_path}Subprograms.emftvm" resolveURI="false" modelElementIndex="0"/>
  <properties xmi:id="_x7vBkN-WEemPcpOhoF9RQA" name="ramses_core_transformation_path" defaultValue="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/transformations/atl/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_rY3-cEjGEeqn2-IY0znzPQ" name="ramses_core_transformation_helpers_path" defaultValue="${ramses_core_transformation_path}helpers/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_1ZxboEjGEeqn2-IY0znzPQ" name="ramses_core_transformation_tools_path" defaultValue="${ramses_core_transformation_path}tools/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_HkuJIEjHEeqn2-IY0znzPQ" name="ramses_core_transformation_validation_path" defaultValue="${ramses_core_transformation_path}validation/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_V78OUEhvEeqn2-IY0znzPQ" fileURI="default.properties"/>
</workflow:Workflow>
