<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowosate="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowosate" xmi:id="_9UTd8F4qEeqYp8ezKVdS_w" name="workflow">
  <components xsi:type="workflow.components:ModelReader" xmi:id="_EmUF8_GhEemHmY6XjiEv8w" name="${NameInputResourcesReader}" description="" modelSlot="srcAadlModel" modelURI="${source_aadl_file}" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflowosate:AadlInstanceModelCreator" xmi:id="_Q_KMoKflEeqjWv6l-exT6w" name="${NameInputAadlModelInstantiation}" systemImplementationName="${system_implementation_name}" packageModelSlot="srcAadlModel" systemInstanceModelSlot="sourceAadlInstance"/>
  <properties xmi:id="_auww0F4rEeqYp8ezKVdS_w" name="source_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_4lCdAF4rEeqYp8ezKVdS_w" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_8CzoMNpdEeq9fI4NRnUBRA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
