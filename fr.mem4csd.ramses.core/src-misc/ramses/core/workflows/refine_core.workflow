<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowemftvm="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowemftvm" xmlns:workflowramses="https://mem4csd.telecom-paris.fr/ramses/workflowramses" xmi:id="_PAkpAMixEei-D6bRtwoJ1Q" name="workflow" description="EMFTVM operations">
  <components xsi:type="workflow.components:StringToBoolEvaluator" xmi:id="_4e1qkKycEeqYtKBdcZJ7ng" name="exposeRuntimeSharedResources" modelSlot="exposeRuntimeSharedResources" stringValue="${expose_runtime_shared_resources}"/>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_cB37sKyREeqXL6k3aNztQA" name="exposeRuntimeSharedResources" conditionSlot="exposeRuntimeSharedResources">
    <onTrue xmi:id="_dBVNkKyREeqXL6k3aNztQA" name="onTrue">
      <components xsi:type="workflowemftvm:EmftVmTransformer" xmi:id="_gvux8KyREeqXL6k3aNztQA" name="${NameExecuteModelRefinement}" registerDependencyModels="true" discardExtraRootElements="true">
        <rulesModelSlot>IOHelpers</rulesModelSlot>
        <rulesModelSlot>AADLCopyHelpers</rulesModelSlot>
        <rulesModelSlot>AADLICopyHelpers</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexServices</rulesModelSlot>
        <rulesModelSlot>PropertiesTools</rulesModelSlot>
        <rulesModelSlot>PackagesTools</rulesModelSlot>
        <rulesModelSlot>FeaturesTools</rulesModelSlot>
        <rulesModelSlot>Uninstanciate</rulesModelSlot>
        <rulesModelSlot>Features</rulesModelSlot>
        <rulesModelSlot>Implementations</rulesModelSlot>
        <rulesModelSlot>Properties</rulesModelSlot>
        <rulesModelSlot>Modes</rulesModelSlot>
        <rulesModelSlot>Flows</rulesModelSlot>
        <rulesModelSlot>Types</rulesModelSlot>
        <rulesModelSlot>Connections</rulesModelSlot>
        <rulesModelSlot>Services</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexTools</rulesModelSlot>
        <rulesModelSlot>BehaviorActionBlock</rulesModelSlot>
        <rulesModelSlot>BehaviorCondition</rulesModelSlot>
        <rulesModelSlot>BehaviorSpecification</rulesModelSlot>
        <rulesModelSlot>BehaviorTime</rulesModelSlot>
        <rulesModelSlot>ElementHolders</rulesModelSlot>
        <rulesModelSlot>ACGServices</rulesModelSlot>
        <rulesModelSlot>UninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>DataUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>SubprogramsUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>ThreadsUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>ProcessesUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>SubprogramCallsCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>PortsCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>ExpandProcessesPorts</rulesModelSlot>
        <rulesModelSlot>DispatchCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsDispatchProtocol</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsDispatchProtocolTarget</rulesModelSlot>
        <rulesModelSlot>SharedRules</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsPorts</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsPortsTarget</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsPortsWithSharedResources</rulesModelSlot>
        <rulesModelSlot>SharedRules_No_Mutex</rulesModelSlot>
        <rulesModelSlot>EventDataPorts_LowET</rulesModelSlot>
        <rulesModelSlot>LanguageSpecificitiesC</rulesModelSlot>
        <rulesModelSlot>Refinement${target}</rulesModelSlot>
        <inputModels xmi:id="_gvux8ayREeqXL6k3aNztQA" modelName="IN" modelSlot="sourceAadlInstance" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux86yREeqXL6k3aNztQA" modelName="TOOLS" modelSlot="atlHelper" metamodelName="ATLHELPER" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/helpers"/>
        <inputModels xmi:id="_gvux9KyREeqXL6k3aNztQA" modelName="BASE_TYPES" modelSlot="Base_Types" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba"/>
        <inputModels xmi:id="_gvux9qyREeqXL6k3aNztQA" modelName="AADL_RUNTIME" modelSlot="aadl_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux9qyREeqXL6k3aNztMA" modelName="PERIODICDELAYED_RUNTIME" modelSlot="PeriodicDelayed_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux9qyREeqXL6k3aNztMA" modelName="PERIODICDELAYEDMUTEX_RUNTIME" modelSlot="PeriodicDelayedMutex_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux96yREeqXL6k3aNztQA" modelName="RAMSES_PROCESSORS" modelSlot="RAMSES_processors" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux-KyREeqXL6k3aNztQA" modelName="RAMSES_BUSES" modelSlot="RAMSES_buses" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux-ayREeqXL6k3aNztQA" modelName="AADL_PROJECT" modelSlot="AADL_Project" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux-qyREeqXL6k3aNztQA" modelName="COMMUNICATION_PROPERTIES" modelSlot="Communication_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux-6yREeqXL6k3aNztQA" modelName="DATA_MODEL" modelSlot="Data_Model" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux_KyREeqXL6k3aNztQA" modelName="DEPLOYMENT_PROPERTIES" modelSlot="Deployment_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux_ayREeqXL6k3aNztQA" modelName="MEMORY_PROPERTIES" modelSlot="Memory_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux_qyREeqXL6k3aNztQA" modelName="MODELING_PROPERTIES" modelSlot="Modeling_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux_6yREeqXL6k3aNztQA" modelName="PROGRAMMING_PROPERTIES" modelSlot="Programming_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvuyAKyREeqXL6k3aNztQA" modelName="THREAD_PROPERTIES" modelSlot="Thread_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvuyAayREeqXL6k3aNztQA" modelName="TIMING_PROPERTIES" modelSlot="Timing_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvuyAqyREeqXL6k3aNztQA" modelName="ARINC653" modelSlot="ARINC653" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvuyA6yREeqXL6k3aNztQA" modelName="CODE_GENERATION_PROPERTIES" modelSlot="Code_Generation_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvuyBKyREeqXL6k3aNztQA" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvuyBqyREeqXL6k3aNztQA" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <outputModels xmi:id="_gvuyB6yREeqXL6k3aNztQA" modelName="OUT_TRACE" modelSlot="refinedTraceModel${target}" metamodelName="ARCH_TRACE" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/arch_trace"/>
        <inputOutputModels xmi:id="_gvuyCKyREeqXL6k3aNztQA" modelName="OUT" modelSlot="refinedAadlModel${target}" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba" extension="aadl"/>
      </components>
    </onTrue>
    <onFalse xmi:id="_eKxh4KyREeqXL6k3aNztQA" name="onFalse">
      <components xsi:type="workflowemftvm:EmftVmTransformer" xmi:id="_EmUF9fGhEemHmY6XjiEv8w" name="${NameExecuteModelRefinement}" registerDependencyModels="true" discardExtraRootElements="true">
        <rulesModelSlot>IOHelpers</rulesModelSlot>
        <rulesModelSlot>AADLCopyHelpers</rulesModelSlot>
        <rulesModelSlot>AADLICopyHelpers</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexServices</rulesModelSlot>
        <rulesModelSlot>PropertiesTools</rulesModelSlot>
        <rulesModelSlot>PackagesTools</rulesModelSlot>
        <rulesModelSlot>FeaturesTools</rulesModelSlot>
        <rulesModelSlot>Uninstanciate</rulesModelSlot>
        <rulesModelSlot>Features</rulesModelSlot>
        <rulesModelSlot>Implementations</rulesModelSlot>
        <rulesModelSlot>Properties</rulesModelSlot>
        <rulesModelSlot>Modes</rulesModelSlot>
        <rulesModelSlot>Flows</rulesModelSlot>
        <rulesModelSlot>Types</rulesModelSlot>
        <rulesModelSlot>Connections</rulesModelSlot>
        <rulesModelSlot>Services</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexTools</rulesModelSlot>
        <rulesModelSlot>BehaviorActionBlock</rulesModelSlot>
        <rulesModelSlot>BehaviorCondition</rulesModelSlot>
        <rulesModelSlot>BehaviorSpecification</rulesModelSlot>
        <rulesModelSlot>BehaviorTime</rulesModelSlot>
        <rulesModelSlot>ElementHolders</rulesModelSlot>
        <rulesModelSlot>ACGServices</rulesModelSlot>
        <rulesModelSlot>UninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>DataUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>SubprogramsUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>ThreadsUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>ProcessesUninstanciateOverride</rulesModelSlot>
        <rulesModelSlot>SubprogramCallsCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>PortsCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>ExpandProcessesPorts</rulesModelSlot>
        <rulesModelSlot>DispatchCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>BehaviorAnnexCommonRefinementSteps</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsDispatchProtocol</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsDispatchProtocolTarget</rulesModelSlot>
        <rulesModelSlot>SharedRules</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsPorts</rulesModelSlot>
        <rulesModelSlot>ExpandThreadsPortsTarget</rulesModelSlot>
        <rulesModelSlot>SharedRules_No_Mutex</rulesModelSlot>
        <rulesModelSlot>EventDataPorts_LowET</rulesModelSlot>
        <rulesModelSlot>LanguageSpecificitiesC</rulesModelSlot>
        <rulesModelSlot>Refinement${target}</rulesModelSlot>
        <inputModels xmi:id="_EmUF9vGhEemHmY6XjiEv8w" modelName="IN" modelSlot="sourceAadlInstance" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUF-fGhEemHmY6XjiEv8w" modelName="TOOLS" modelSlot="atlHelper" metamodelName="ATLHELPER" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/helpers"/>
        <inputModels xmi:id="_EmUF-vGhEemHmY6XjiEv8w" modelName="BASE_TYPES" modelSlot="Base_Types" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba"/>
        <inputModels xmi:id="_gvux9qyREeqXL6k3aNztMA" modelName="PERIODICDELAYED_RUNTIME" modelSlot="PeriodicDelayed_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_gvux9qyREeqXL6k3aNztMA" modelName="PERIODICDELAYEDMUTEX_RUNTIME" modelSlot="PeriodicDelayedMutex_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGAPGhEemHmY6XjiEv8w" modelName="AADL_RUNTIME" modelSlot="aadl_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGBPGhEemHmY6XjiEv8w" modelName="RAMSES_PROCESSORS" modelSlot="RAMSES_processors" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGBfGhEemHmY6XjiEv8w" modelName="RAMSES_BUSES" modelSlot="RAMSES_buses" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGBvGhEemHmY6XjiEv8w" modelName="AADL_PROJECT" modelSlot="AADL_Project" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGB_GhEemHmY6XjiEv8w" modelName="COMMUNICATION_PROPERTIES" modelSlot="Communication_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGCPGhEemHmY6XjiEv8w" modelName="DATA_MODEL" modelSlot="Data_Model" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGCfGhEemHmY6XjiEv8w" modelName="DEPLOYMENT_PROPERTIES" modelSlot="Deployment_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGCvGhEemHmY6XjiEv8w" modelName="MEMORY_PROPERTIES" modelSlot="Memory_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGC_GhEemHmY6XjiEv8w" modelName="MODELING_PROPERTIES" modelSlot="Modeling_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGDPGhEemHmY6XjiEv8w" modelName="PROGRAMMING_PROPERTIES" modelSlot="Programming_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGDfGhEemHmY6XjiEv8w" modelName="THREAD_PROPERTIES" modelSlot="Thread_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGDvGhEemHmY6XjiEv8w" modelName="TIMING_PROPERTIES" modelSlot="Timing_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGD_GhEemHmY6XjiEv8w" modelName="ARINC653" modelSlot="ARINC653" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGEPGhEemHmY6XjiEv8w" modelName="CODE_GENERATION_PROPERTIES" modelSlot="Code_Generation_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGE_GhEemHmY6XjiEv8w" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <inputModels xmi:id="_EmUGFvGhEemHmY6XjiEv8w" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
        <outputModels xmi:id="_VmBpwPmAEemTadmi341e_w" modelName="OUT_TRACE" modelSlot="refinedTraceModel${target}" metamodelName="ARCH_TRACE" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/arch_trace"/>
        <inputOutputModels xmi:id="_e6V98PGiEemHmY6XjiEv8w" modelName="OUT" modelSlot="refinedAadlModel${target}" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba" extension="aadl"/>
      </components>
    </onFalse>
  </components>
  <components xsi:type="workflowramses:AadlWriter" xmi:id="_hizN8PGiEemHmY6XjiEv8w" name="${NameSaveRefinedAADLModel}" modelSlot="refinedAadlModel${target}" modelURI="${refined_aadl_file}" cloneModel="false" unloadAfter="true" resolveURI="false"/>
  <components xsi:type="workflowramses:TraceWriter" xmi:id="_JE6UIF39EeqYp8ezKVdS_w" name="${NameSaveTransformationTraceModel}" modelSlot="refinedTraceModel${target}" modelURI="${refined_trace_file}" cloneModel="false" deresolvePluginURIs="true" resolveURI="false"/>
  <properties xmi:id="_knMRsF4vEeqYp8ezKVdS_w" name="refined_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_knMRsF4vEnrYp8ezKVdG_k" name="target">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_rLd3AF4vEeqYp8ezKVdS_w" name="refined_trace_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_0SY1EKyYEeq7xdSO0mbjBg" name="expose_runtime_shared_resources" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_O48AsNpeEeq9fI4NRnUBRA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
