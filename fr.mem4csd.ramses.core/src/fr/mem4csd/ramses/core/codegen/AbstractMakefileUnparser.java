/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen;

import java.io.BufferedWriter ;
import java.io.FileWriter ;
import java.io.IOException ;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator ;
import java.util.LinkedHashSet ;
import java.util.NoSuchElementException ;
import java.util.Set ;

import org.apache.log4j.Logger ;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject ;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.osate.aadl2.AnnexSubclause ;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.DataAccess;
import org.osate.aadl2.DataSubcomponentType;
import org.osate.aadl2.Element ;
import org.osate.aadl2.EventDataPort;
import org.osate.aadl2.ListValue ;
import org.osate.aadl2.ModalPropertyValue ;
import org.osate.aadl2.Parameter;
import org.osate.aadl2.ProcessImplementation ;
import org.osate.aadl2.PropertyAssociation ;
import org.osate.aadl2.PropertyExpression ;
import org.osate.aadl2.StringLiteral ;
import org.osate.aadl2.Subprogram ;
import org.osate.aadl2.SubprogramCall ;
import org.osate.aadl2.SubprogramCallSequence ;
import org.osate.aadl2.SubprogramImplementation ;
import org.osate.aadl2.SubprogramSubcomponent ;
import org.osate.aadl2.SubprogramSubcomponentType ;
import org.osate.aadl2.SubprogramType ;
import org.osate.aadl2.ThreadImplementation ;
import org.osate.aadl2.ThreadSubcomponent ;
import org.osate.aadl2.modelsupport.UnparseText ;
import org.osate.annexsupport.AnnexUtil;
import org.osate.ba.aadlba.AadlBaPackage;
import org.osate.ba.aadlba.BehaviorActionBlock ;
import org.osate.ba.aadlba.BehaviorAnnex ;
import org.osate.ba.aadlba.SubprogramCallAction ;

import fr.mem4csd.ramses.core.codegen.utils.DependencyManager;
import fr.mem4csd.ramses.core.codegen.utils.FileUtils;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;
import fr.mem4csd.ramses.core.workflowramses.Codegen;
import fr.mem4csd.ramses.core.workflowramses.impl.AadlToTargetBuildGeneratorImpl;

public abstract class AbstractMakefileUnparser extends AadlToTargetBuildGeneratorImpl
{
	
	protected final String C_PERIODIC_DELAYED_RUNTIME_DIR = "PeriodicDelayed_runtime" ;

	
	protected URIConverter getURIConverter()
	{
		return getCodeGenWorkflowComponent().getUriConverter();
	}

	protected class IncludeDirIterator implements Iterator<URI>
	{
		private int _count = 0 ;
		private int _max = 0 ;
		private Iterator<URI> _it ;
		Codegen container;
		
		public IncludeDirIterator()
		{
			container = getCodeGenWorkflowComponent();
			_includeDirManager.addCommonDependency(container.getCoreRuntimeDirectoryURI());
			_includeDirManager.addCommonDependency(container.getCoreRuntimeDirectoryURI().appendSegment(C_PERIODIC_DELAYED_RUNTIME_DIR));
			if(container.getTargetRuntimeDirectoryURI()!=null)
				_includeDirManager.addCommonDependency(container.getTargetRuntimeDirectoryURI());
			
			_it = _includeDirManager.getCommonDependencies().iterator() ;
			_max = 1 + container.getIncludeDirectoryURIList().size() + _includeDirManager.getCommonDependencies().size() ;
		}

		@Override
		public boolean hasNext()
		{
			return _max > _count ;
		}

		@Override
		public URI next()
		{
			URI result ;

			if(_count == 0)
			{
				result = container.getTargetInstallDirectoryURI();
			}
			else if(_count <= container.getIncludeDirectoryURIList().size())
			{
				result = container.getIncludeDirectoryURIList().get(_count-1);
			}
			else if(_count < _max)
			{
				result = _it.next() ;
			}
			else
			{
				String msg = "iteration over bound" ;
				_LOGGER.fatal(msg);
				throw new NoSuchElementException(msg) ;
			}

			_count++ ;

			return result ;
		}

		@Override
		public void remove()
		{
			String msg = "remove not supported" ;
			_LOGGER.fatal(msg);
			throw new UnsupportedOperationException(msg) ;
		}
	}

	protected DependencyManager<Element, URI> _includeDirManager = new DependencyManager<Element, URI>();

	private static Logger _LOGGER = Logger.getLogger(AbstractMakefileUnparser.class) ;

	// DB: To also work on windows
	//private static final String PATH_SEPARATOR = "/";

	public Set<URI> getListOfReferencedObjects(ProcessImplementation aProcessImplementation)
	{
		Set<URI> result = _includeDirManager.getDenpendencies(aProcessImplementation) ;

		if(result == null)
		{
			result = new LinkedHashSet<URI>() ;

			for(ThreadSubcomponent aTheadSubcomponent : aProcessImplementation
					.getOwnedThreadSubcomponents())
			{
				if(aTheadSubcomponent.getComponentImplementation() != null)
				{
					getListOfReferencedObjects((ThreadImplementation) aTheadSubcomponent
							.getComponentImplementation(),
							result) ;
				}
				else
				{
					// The thread component instance should reference
					// a thread implementation to call user operations.
					String errMsg =  "the thread component instance should reference a " +
							"thread implementation to call user operations." ;
					_LOGGER.fatal(errMsg);
					//          ServiceProvider.SYS_ERR_REP.error(errMsg, true);
				}
				
				Set<String> additionalSourceFiles = new HashSet<String>();
				
				GeneratorUtils.getEntryPoint(aTheadSubcomponent, "Recover_Entrypoint", "Recover_Entrypoint_Source_Text", new ArrayList<String>(), additionalSourceFiles);
				GeneratorUtils.getEntryPoint(aTheadSubcomponent, "Initialize_Entrypoint", "Initialize_Entrypoint_Source_Text", new ArrayList<String>(), additionalSourceFiles);				
				GeneratorUtils.getEntryPoint(aTheadSubcomponent, "Finalize_Entrypoint", "Finalize_Entrypoint_Source_Text", new ArrayList<String>(), additionalSourceFiles);
				GeneratorUtils.getEntryPoint(aTheadSubcomponent, "Activate_Entrypoint", "Activate_Entrypoint_Source_Text", new ArrayList<String>(), additionalSourceFiles);
				GeneratorUtils.getEntryPoint(aTheadSubcomponent, "Deactivate_Entrypoint", "Deactivate_Entrypoint_Source_Text", new ArrayList<String>(), additionalSourceFiles);
				
				for(String filePathFromSourceTextProperty: additionalSourceFiles)
				{
					result.add(createURIFromSourceTextPropertyValue(aProcessImplementation, filePathFromSourceTextProperty));
				}
			}

			_includeDirManager.addDependencies(aProcessImplementation, result); 
		}

		return result ;
	}

	private URI createURIFromSourceTextPropertyValue(ComponentClassifier cc, String filePathFromSourceTextProperty) {
		
		URIConverter uriConverter = getURIConverter();
		
		URI noPrefixFoundFile =	URI.createFileURI(filePathFromSourceTextProperty);
		if( FileUtils.exists( noPrefixFoundFile, uriConverter ) ) {
			return URI.createURI(noPrefixFoundFile.toFileString()) ;
		}

		final URI dir = getDirectory( cc );
		
		if ( dir == null ) {
//			continue;
		}
		
		final URI foundFile = dir.appendSegments( filePathFromSourceTextProperty.split("/") );
//
//		String filePath = dir + File.separator + value;
//		filePath = filePath.replaceAll(File.separator+"\\."+File.separator, File.separator);

//		File foundFile =
//				new File(filePath) ;

		if ( FileUtils.exists( foundFile, uriConverter ) ) {
//		if(foundFile.exists())
//		{
			
			_includeDirManager.addCommonDependency( dir );
			_includeDirManager.addCommonDependency(  foundFile.trimSegments( 1 ) /*foundFile.getParentFile()*/);
			
			return foundFile ;
		}
		
		//URI includeDir = null ;
		Iterator<URI> it = new IncludeDirIterator();
		
		while( it.hasNext() ) {
			URI res = it.next();
			res = res.appendSegment( filePathFromSourceTextProperty );
			if ( FileUtils.exists( res, uriConverter ) ) {
				return res;
			}
		}
		
		errorReferencedFileNotFound( filePathFromSourceTextProperty, cc );
		return null;
	}

	protected void getListOfReferencedObjects(ThreadImplementation aThreadImplementation,
			Set<URI> result)
	{
		Set<URI> tmp = _includeDirManager.getDenpendencies(aThreadImplementation) ;

		if(tmp == null)
		{
			tmp = new LinkedHashSet<URI>() ;

			for(SubprogramCallSequence aCallSequence : aThreadImplementation
					.getOwnedSubprogramCallSequences())
			{
				for(SubprogramCall aCallSpecification : aCallSequence
						.getOwnedSubprogramCalls())
				{
					getListOfReferencedObjects(aCallSpecification, tmp) ;
				}
			}
			for(SubprogramSubcomponent sc : aThreadImplementation
					.getOwnedSubprogramSubcomponents())
			{
				getListOfReferencedObjects(sc, tmp) ;
			}

			_includeDirManager.addDependencies(aThreadImplementation, tmp) ;

		}// End if.

		result.addAll(tmp) ;
	}

	protected void getListOfReferencedObjects(SubprogramSubcomponent sc,
			Set<URI> result)
	{
		Set<URI> tmp = _includeDirManager.getDenpendencies(sc) ;

		if(tmp == null)
		{
			tmp = new LinkedHashSet<URI>() ;

			SubprogramSubcomponentType sst = sc.getSubprogramSubcomponentType() ;
			for(PropertyAssociation aPropertyAssociation : sst
					.getOwnedPropertyAssociations())
			{
				getListOfReferencedObjects(aPropertyAssociation, tmp) ;
			}
			if(sst instanceof SubprogramImplementation)
			{
				SubprogramImplementation si = (SubprogramImplementation) sst ;
				for(PropertyAssociation aPropertyAssociation : si.getType()
						.getOwnedPropertyAssociations())
				{
					getListOfReferencedObjects(aPropertyAssociation, tmp) ;
				}
			}

			_includeDirManager.addDependencies(sc, tmp) ;
		}

		result.addAll(tmp) ;
	}

	protected void getListOfReferencedObjects(SubprogramCall aCallSpecification,
			Set<URI> result)
	{
		if(aCallSpecification instanceof SubprogramCall)
		{
			SubprogramCall sc = (SubprogramCall) aCallSpecification ;

			Set<URI> tmp = _includeDirManager.getDenpendencies(sc) ;

			if(tmp == null)
			{
				tmp = new LinkedHashSet<URI>() ;

				getListOfReferencedObjects((Subprogram) sc.getCalledSubprogram(),
						tmp) ;

				_includeDirManager.addDependencies(sc, tmp) ;
			}

			result.addAll(tmp) ;
		}
	}

	protected boolean getListOfReferencedObjects(Subprogram aSubprogram,
			Set<URI> result)
	{
		Set<URI> tmp = _includeDirManager.getDenpendencies(aSubprogram) ;

		if(tmp == null)
		{
			tmp = new LinkedHashSet<URI>() ;

			if(aSubprogram instanceof SubprogramType)
			{
				SubprogramType aSubprogramType = (SubprogramType) aSubprogram ;
				for(AnnexSubclause annex : AnnexUtil.
						getAllAnnexSubclauses((Classifier)aSubprogramType, AadlBaPackage.eINSTANCE.getBehaviorAnnex()))
				{
					if(annex instanceof BehaviorAnnex)
					{
						BehaviorAnnex ba = (BehaviorAnnex) annex ;
						getListOfReferencedObjects(ba, tmp) ;
						result.addAll(tmp) ;
						_includeDirManager.addDependencies(aSubprogram, tmp) ;
					}
				}
				for(PropertyAssociation aPropertyAssociation : aSubprogramType
						.getOwnedPropertyAssociations())
				{
					getListOfReferencedObjects(aPropertyAssociation, tmp) ;
				}
				if(aSubprogramType.getOwnedExtension() != null)
				{
					getListOfReferencedObjects((Subprogram) aSubprogramType
							.getOwnedExtension()
							.getExtended(), tmp) ;
				}
				for(Parameter param: aSubprogramType.getOwnedParameters())
				{
					getListOfReferencedObjects(param, tmp) ;
				}
				for(EventDataPort edp: aSubprogramType.getOwnedEventDataPorts())
				{
					getListOfReferencedObjects(edp, tmp) ;
				}
				for(DataAccess da: aSubprogramType.getOwnedDataAccesses())
				{
					getListOfReferencedObjects(da, tmp) ;
				}
			}
			else if(aSubprogram instanceof SubprogramImplementation)
			{
				SubprogramImplementation aSubprogramImplementation =
						(SubprogramImplementation) aSubprogram ;

				for(AnnexSubclause annex : AnnexUtil.
						getAllAnnexSubclauses((Classifier)aSubprogramImplementation, AadlBaPackage.eINSTANCE.getBehaviorAnnex()))
				{
					if(annex instanceof BehaviorAnnex)
					{
						BehaviorAnnex ba = (BehaviorAnnex) annex ;
						getListOfReferencedObjects(ba, tmp) ;
						result.addAll(tmp) ;
						_includeDirManager.addDependencies(aSubprogram, tmp) ;
						return true ;
					}
				}
				for(PropertyAssociation aPropertyAssociation : aSubprogramImplementation
						.getOwnedPropertyAssociations())
				{

					getListOfReferencedObjects(aPropertyAssociation, tmp) ;
				}
				if(aSubprogramImplementation.getOwnedExtension() != null)
				{
					getListOfReferencedObjects((Subprogram) aSubprogramImplementation
							.getOwnedExtension()
							.getExtended(), tmp) ;
					if(tmp.isEmpty())
					{
						getListOfReferencedObjects((Subprogram) aSubprogramImplementation
								.getOwnedExtension()
								.getExtended().getType(),
								tmp) ;
					}
				}
				for(SubprogramCall aCallSpecification : aSubprogramImplementation
						.getSubprogramCalls())
				{
					getListOfReferencedObjects(aCallSpecification, tmp) ;
				}
			}

			_includeDirManager.addDependencies(aSubprogram, tmp) ;
		}// End of the first if.

		result.addAll(tmp) ;

		return ! tmp.isEmpty() ;
	}

	private void getListOfReferencedObjects(DataAccess da, Set<URI> tmp) {
		if(da.getDataFeatureClassifier()!=null)
			getListOfReferencedObjects(da.getDataFeatureClassifier(), tmp);
	}

	private void getListOfReferencedObjects(EventDataPort edp, Set<URI> tmp) {
		if(edp.getDataFeatureClassifier()!=null)
			getListOfReferencedObjects(edp.getDataFeatureClassifier(), tmp);
	}

	private void getListOfReferencedObjects(Parameter param, Set<URI> tmp) {
		if(param.getDataFeatureClassifier()!=null)
			getListOfReferencedObjects(param.getDataFeatureClassifier(), tmp);
	}

	private void getListOfReferencedObjects(DataSubcomponentType dataFeatureClassifier, Set<URI> tmp) {
		for(PropertyAssociation aPropertyAssociation : dataFeatureClassifier
				.getOwnedPropertyAssociations())
		{
			getListOfReferencedObjects(aPropertyAssociation, tmp) ;
		}
	}

	protected void getListOfReferencedObjects(BehaviorAnnex ba,
			Set<URI> result)
	{
		Set<URI> tmp = _includeDirManager.getDenpendencies(ba) ;

		if(tmp == null)
		{
			tmp = new LinkedHashSet<URI>() ;

			for(BehaviorActionBlock bab : ba.getActions())
			{
				Iterator<EObject> iter = bab.eAllContents() ;
				while(iter.hasNext())
				{
					EObject next = iter.next() ;
					if(next instanceof SubprogramCallAction)
					{
						SubprogramCallAction sca = (SubprogramCallAction) next ;
						getListOfReferencedObjects((Subprogram) sca.getSubprogram()
								.getElement(), tmp) ;
					}
				}
			}

			_includeDirManager.addDependencies(ba, tmp) ;
		}

		result.addAll(tmp) ;
	}

	private URI getDirectory(Element e)	{
		//final URI path;
		
		URI dirURI = e.eResource().getURI().trimSegments( 1 );

//		if ( dirURI.isFile() ) {
//			return FileUtils.removeScheme( dirURI );
//		}

		if ( dirURI.isPlatformResource() ) {
			//    	IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			//    	final IFolder folder = root.getFolder( new Path( dirURI.toPlatformString(true) ) );
			//    	return folder.getLocation().toOSString();
			dirURI = CommonPlugin.resolve( dirURI );
			
//			String projectName = dirURI.toPlatformString(true).substring(1);
//			if(projectName.contains("/"))
//				projectName = projectName.substring(0,projectName.indexOf("/"));
//			IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
//
//			path = project.getLocation().toOSString();
//
//			String filePathWithProject = dirURI.toPlatformString(true);
//			filePathWithProject = filePathWithProject.substring(filePathWithProject.indexOf("/")+projectName.length()+1);
//
//			path = path + filePathWithProject ;
//
//			//        int index = path.lastIndexOf(File.separator);
//			//        path = path.substring(0, index+1);
//			return path;
		}

		return dirURI;
	}
	
	protected boolean getListOfReferencedObjects(	PropertyAssociation aPropertyAssociation,
													Set<URI> result ) {
		Set<URI> tmp = _includeDirManager.getDenpendencies(aPropertyAssociation) ;

		boolean isSourceTextPA = false ;
		
		URIConverter uriConverter = getURIConverter();

		if(tmp == null)
		{
			tmp = new LinkedHashSet<URI>() ;

			isSourceTextPA =
					aPropertyAssociation.getProperty().getName() != null &&
					(aPropertyAssociation.getProperty().getName()
							.equalsIgnoreCase("Source_Location") || aPropertyAssociation
							.getProperty().getName().equalsIgnoreCase("Source_Text")) ;
			if(isSourceTextPA)
			{
				for(ModalPropertyValue aModalPropertyValue : aPropertyAssociation
						.getOwnedValues())
				{
					PropertyExpression aPE = aModalPropertyValue.getOwnedValue() ;

					if(aPE instanceof StringLiteral)
					{
						StringLiteral sl = (StringLiteral) aPE ;
						String value = sl.getValue() ;
						boolean found = false ;

						URI noPrefixFoundFile =	URI.createFileURI(value);
						if( FileUtils.exists( noPrefixFoundFile, uriConverter ) ) {
//						if(noPrefixFoundFile.exists())
//						{
							tmp.add(URI.createURI(noPrefixFoundFile.toFileString())) ;
							found = true ;
							continue;
						}

						final URI dir = getDirectory( aPropertyAssociation );
						//String dir = getDirectory(aPropertyAssociation);
						
						if ( dir == null ) {
//							continue;
						}
						
						final URI foundFile = dir.appendSegments( value.split("/") );
//
//						String filePath = dir + File.separator + value;
//						filePath = filePath.replaceAll(File.separator+"\\."+File.separator, File.separator);

//						File foundFile =
//								new File(filePath) ;

						if ( FileUtils.exists( foundFile, uriConverter ) ) {
//						if(foundFile.exists())
//						{
							tmp.add( foundFile );
							_includeDirManager.addCommonDependency( dir );
							_includeDirManager.addCommonDependency(  foundFile.trimSegments( 1 ) /*foundFile.getParentFile()*/);
							found = true;
							
							continue;
						}
						
						//URI includeDir = null ;
						Iterator<URI> it = new IncludeDirIterator();
						
						while( it.hasNext() ) {
							URI includeDir = it.next();
							includeDir = includeDir.appendSegment( value );
//							filePath = includeDir.getAbsoluteFile() + File.separator + value;
//							filePath = filePath.replaceAll(File.separator+"\\."+File.separator, File.separator);
//							foundFile =
//									new File(filePath) ;
							if ( FileUtils.exists( includeDir, uriConverter ) ) {
//							if(foundFile.exists())
//							{
								tmp.add( includeDir.trimSegments( 1 )/*foundFile.getParentFile()*/ );
								found = true ;
								break ;
							}
						}
						
						if ( !found ) {
							errorReferencedFileNotFound( value, aPropertyAssociation );
						}
					}
					else if ( aPE instanceof ListValue ) {
						for(PropertyExpression pe : ((ListValue) aPE).getOwnedListElements())
						{
							StringLiteral sl = (StringLiteral) pe ;
							String value = sl.getValue() ;
							boolean found = false ;
							final URI dir = getDirectory( aPropertyAssociation );

							if ( dir == null ) {
								continue;
							}
							
							final URI noPrexUri = URI.createFileURI( value );
//							File noPrefixFoundFile =
//									new File(value);

							if ( FileUtils.exists( noPrexUri, uriConverter ) ) {
//							if(noPrefixFoundFile.exists())
//							{
								tmp.add(URI.createURI(dir.toFileString()));
								tmp.add(URI.createURI(noPrexUri.toFileString())) ;
								
								found = true;
								
								continue;
							}
							//String filePath = dir + PATH_SEPARATOR /*File.separator*/ + value;
							//filePath = filePath.replaceAll(PATH_SEPARATOR/*File.separator*/+"\\."+ PATH_SEPARATOR/*File.separator*/, PATH_SEPARATOR /*File.separator*/);

							URI foundFile = dir.appendSegments( value.split("/") );
									//new File(filePath) ;

							if ( FileUtils.exists( foundFile, uriConverter ) ) {
//							if(foundFile.exists())
//							{
								tmp.add(foundFile);
								found = true ;
								_includeDirManager.addCommonDependency( dir );
								_includeDirManager.addCommonDependency( foundFile.trimSegments( 1 )/*foundFile.getParentFile()*/);
								
								continue;
							}
							
							//URI includeDir = null ;
							Iterator<URI> it = new IncludeDirIterator() ;
							
							while( it.hasNext() ) {
								URI includeDir = it.next() ;
								
								if (includeDir == null ) {
									continue;
								}

								includeDir = includeDir.appendSegments( value.split("/") );
//								filePath = includeDir.getAbsolutePath() + PATH_SEPARATOR/*File.separator*/ + value;
//								filePath = filePath.replaceAll(PATH_SEPARATOR/*File.separator*/+"\\."+ PATH_SEPARATOR/*File.separator*/, PATH_SEPARATOR/*File.separator*/);
//								foundFile =
//										new File(filePath) ;
								if ( FileUtils.exists( includeDir, uriConverter ) ) {
//								if(foundFile.exists())
//								{
									tmp.add(includeDir) ;
									found = true ;
									break ;
								}
							}
							
							if ( !found ) {
								errorReferencedFileNotFound( value, aPropertyAssociation );
							}
						}
					}
				}
			}// End of if(isSourceTextPA).

			_includeDirManager.addDependencies(aPropertyAssociation, tmp) ;

		}// End of if(tmp == null).
		else
		{
			isSourceTextPA = ! tmp.isEmpty() ;
		}

		result.addAll(tmp) ;

		return isSourceTextPA ;
	}

	private void errorReferencedFileNotFound( final String fileName,
			final PropertyAssociation propAss ) {
		errorReferencedFileNotFound(fileName, propAss.getContainingClassifier());
	}

	private void errorReferencedFileNotFound( final String fileName,
			final Classifier cc ) {
		final String errMsg = "File " + fileName + " referenced in object " +
				cc.getFullName() + " of resource " + 
				cc.eResource().getURI().lastSegment() + " could not be found.";
		_LOGGER.error(errMsg);
		//      ServiceProvider.SYS_ERR_REP.error(errMsg, false);	  
	}
	
	public void saveMakefile(UnparseText text, URI makeFileDir)
	{
		FileUtils.makeDir( makeFileDir );
//		File makeFile = new File(makeFileDir.getAbsolutePath() + "/Makefile") ;
		saveMakefile(text, makeFileDir, makeFileDir.appendSegment( "Makefile" ));
	}

	protected void saveMakefile(UnparseText text, URI makeFileDir, URI destination)
	{
		URIConverter uriConverter = getURIConverter();
		
		try
		{
			if( FileUtils.exists( destination, uriConverter ) ) {
				if ( FileUtils.createNewFile( destination ) ) {
					_LOGGER.info("Makefile file created");
				}
			}
			
			String destinationPath;
			if(destination.isFile())
				destinationPath = destination.toFileString();
			else
				destinationPath = destination.toString();
			FileWriter fileW = new FileWriter(destinationPath) ;

			BufferedWriter output ;

			output = new BufferedWriter(fileW) ;
			output.write(text.getParseOutput()) ;
			output.close() ;
		}
		catch(IOException ex)
		{
			String errMsg = "cannot save the makefile" ;
			_LOGGER.fatal(errMsg  + " at " + destination.toString(), ex) ;
			throw new RuntimeException(errMsg, ex) ;
		}
	}
	
	protected boolean isDirectory( final URI uri ) {
		URIConverter uriConverter = getURIConverter();
		return FileUtils.isDirectory( uri, uriConverter );
	}
	
	protected String toMakefileString( final URI sourceFile ) {
		return sourceFile.isFile() ? sourceFile.toFileString() : sourceFile.toString();
	}
}
