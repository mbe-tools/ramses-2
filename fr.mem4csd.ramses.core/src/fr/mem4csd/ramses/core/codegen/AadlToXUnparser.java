/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen;

import java.util.ArrayList ;
import java.util.List ;

import org.apache.log4j.Logger ;
import org.eclipse.emf.common.util.EList ;
import org.osate.aadl2.DataPort ;
import org.osate.aadl2.DataSubcomponent ;
import org.osate.aadl2.DataSubcomponentType;
import org.osate.aadl2.DirectionType ;
import org.osate.aadl2.EventDataPort ;
import org.osate.aadl2.FeatureGroup;
import org.osate.aadl2.NamedElement ;
import org.osate.aadl2.Port ;
import org.osate.aadl2.ProcessImplementation ;
import org.osate.aadl2.Subcomponent ;
import org.osate.aadl2.instance.ComponentInstance ;
import org.osate.aadl2.instance.ConnectionInstance ;
import org.osate.aadl2.instance.FeatureCategory ;
import org.osate.aadl2.instance.FeatureInstance ;
import org.osate.aadl2.modelsupport.UnparseText ;
import org.osate.utils.internal.PropertyUtils ;
import org.osate.xtext.aadl2.properties.util.AadlProject;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.codegen.GenerationInfos.GlobalQueueInfo;
import fr.mem4csd.ramses.core.codegen.GenerationInfos.QueueInfo;
import fr.mem4csd.ramses.core.codegen.GenerationInfos.SampleInfo;
import fr.mem4csd.ramses.core.codegen.utils.GenerationUtilsC;
import fr.mem4csd.ramses.core.workflowramses.impl.AadlToTargetConfigurationGeneratorImpl;



public abstract class AadlToXUnparser extends AadlToTargetConfigurationGeneratorImpl
{
private static Logger _LOGGER = Logger.getLogger(AadlToXUnparser.class) ;
  
  private String getGlobalQueueType()
  {
	  return "AADL_runtime::PortListType";
  }
  
  private String getWaitMessageType()
  {
	  return "AADL_runtime::TargetWaitMessageType";
  }

  private String getWaitModeType()
  {
	  return "AADL_runtime::TargetWaitModeType";
  }
  
  private String getLockType()
  {
	  return "AADL_runtime::TargetLockType";
  }
  
  protected String getQueuingType()
  {
	  return "AADL_runtime::TargetProcessPortType";
  }

  protected String getSamplingType()
  {
	  return "AADL_runtime::TargetProcessPortType";
  }

  protected String getFeatureLocalIdentifier(FeatureInstance fi)
  {
	  return GenerationUtilsC.getGenerationCIdentifier(fi.getQualifiedName());
  };

  protected String getGenerationIdentifier(String prefix)
  {
	  return GenerationUtilsC.getGenerationCIdentifier(prefix);
  }

  protected UnparseText _mainCCode;
  protected UnparseText _mainHCode;
  
  
  protected void findCommunicationMechanism(ProcessImplementation process,
                                            ProcessProperties pp,
                                            ArchTraceSpec archTraces) {
    final EList<Subcomponent> subcmpts = process.getAllSubcomponents() ;
	
    pp.tasksNb = process.getOwnedThreadSubcomponents().size();
    for(Subcomponent s : subcmpts) {

    	if (s instanceof DataSubcomponent) {
    		final DataSubcomponentType compType = ((DataSubcomponent) s).getDataSubcomponentType();
    		if ( compType != null ) {
    			   			
    			if ( compType.getQualifiedName().equalsIgnoreCase(getGlobalQueueType())) {
    				globalQueueHandler(s.getName(),
    						(ComponentInstance) archTraces.
    						getTransformationTrace(s), pp);
    			}
    			else if ( compType.getQualifiedName().equalsIgnoreCase( getQueuingType() ) ) {
    				FeatureInstance fi = (FeatureInstance) archTraces.
    						getTransformationTrace(s);
    				if(fi.getCategory().equals(FeatureCategory.EVENT_DATA_PORT))
    				{
    					queueHandler(s.getName(), fi, pp);
    					pp.hasQueue = true ;
    				} else if(fi.getCategory().equals(FeatureCategory.DATA_PORT))
    				{
    					sampleHandler(s.getName(), fi, pp);
    					pp.hasSample = true ;
    				}
    			}
    			else if( compType.getQualifiedName()
    					.equalsIgnoreCase(getWaitMessageType()))
    			{
    				String name = s.getName();
    				pp.waitMessageNames.add(name);
    			    
    				pp.hasWaitMessage = true;
    			}
    			else if( compType.getQualifiedName()
    					.equalsIgnoreCase(getWaitModeType()))
    			{
    				String name = s.getName();
    				pp.waitModeNames.add(name);
    			    
    				pp.hasWaitMode = true;
    			}
    			else if( compType.getQualifiedName()
    					.equalsIgnoreCase(getWaitModeType()))
    			{
    				String name = s.getName();
    				pp.lockNames.add(name);
    			    
    				pp.hasLock = true;
    			}
    		}
    	}
    }

    for(QueueInfo qi : pp.queueInfo)
    {
    	boolean found=false;
    	for(FeatureInstance fi: qi.portList)
    	{
    		if(found)
    			break;
    		for(GlobalQueueInfo gqi: pp.globalQueueInfo)
    		{
    			if(gqi.component.equals(fi.getComponentInstance()))
    			{
    				qi.gQueue=gqi;
    				found=true;
    				break;
    			}
    		}
    	}
    }
  }


  private void globalQueueHandler(String name,
                                  ComponentInstance transformationTrace,
                                  ProcessProperties pp)
  {
    GlobalQueueInfo gqi = new GlobalQueueInfo();
    gqi.id=name;
    gqi.component = transformationTrace;
    
    pp.globalQueueInfo.add(gqi);
  }

  private void queueHandler(String id, FeatureInstance fi, ProcessProperties pp)
  {
    Port p = (Port) fi.getFeature() ;

    QueueInfo queueInfo = new QueueInfo() ;

    queueInfo.id = id;
    
    queueInfo.uniqueId = getFeatureLocalIdentifier(fi);
    
    queueInfo.direction = getDirection(fi) ;

    ComponentInstance threadInstance = fi.getComponentInstance();
    List<FeatureInstance> threadInputPortsList = new ArrayList<FeatureInstance>();
    for(FeatureInstance iter: threadInstance.getFeatureInstances())
      if(iter.getDirection().equals(DirectionType.IN) &&
          (iter.getCategory().equals(FeatureCategory.EVENT_DATA_PORT) 
                                     || iter.getCategory().equals(FeatureCategory.EVENT_PORT)))
        threadInputPortsList.add(iter);
    
    queueInfo.threadPortId = threadInputPortsList.indexOf(fi);
    queueInfo.portList = new ArrayList<FeatureInstance>();
    queueInfo.portList.add(fi);
    List<ConnectionInstance> allConnectionInstances = new ArrayList<ConnectionInstance>();
    allConnectionInstances.addAll(fi.getSrcConnectionInstances());
    allConnectionInstances.addAll(fi.getDstConnectionInstances());
    for(ConnectionInstance ci: allConnectionInstances)
    {
      queueInfo.portList.add((FeatureInstance) ci.getSource());
      queueInfo.portList.add((FeatureInstance) ci.getDestination());
    }

    Long timeOut = PropertyUtils.getIntValue(fi, "Timeout", AadlProject.MS_LITERAL);
    if(timeOut==null)
      timeOut=(long) 0;
    queueInfo.timeOut = timeOut;

    if(fi.getCategory() == FeatureCategory.EVENT_DATA_PORT)
    {
      EventDataPort port  = (EventDataPort) fi.getFeature() ;
      String value = PropertyUtils.getStringValue(port.getDataFeatureClassifier(),
    		  "Source_Name") ;
      if(value != null)
      {
    	  queueInfo.dataType=value ;
      }
      else
      {
    	  queueInfo.dataType = getGenerationIdentifier(port.getDataFeatureClassifier().getQualifiedName()) ;
      }
    }
    getQueueInfo(p, queueInfo);
    pp.queueInfo.add(queueInfo) ;
  }

  private void getQueueInfo(Port port, QueueInfo info)
  {
    
    // XXX temporary. Until ATL transformation modifications.
    //  info.id = RoutingProperties.getFeatureLocalIdentifier(fi) ;

    if(info.type == null)
    {
      String value = PropertyUtils.getEnumValue(port, "Queue_Processing_Protocol") ;
      if(value != null)
      {
        info.type = value ;
      }
      else
      {
    	  info.type = "FIFO" ;
      }
    }

    if(info.size == -1)
    {
      Long value = PropertyUtils.getIntValue(port, "Queue_Size") ;
      if(value != null)
      {
        info.size = value ;
      }
      else
      {
    	  info.size = 10;
      }
    }
  }

  private void sampleHandler(String id, FeatureInstance fi, ProcessProperties pp)
  {
    Port p = (Port) fi.getFeature() ;

    SampleInfo sampleInfo = new SampleInfo() ;
    sampleInfo.direction = getDirection(fi) ;

    if(fi.getCategory() == FeatureCategory.DATA_PORT)
    {
    	DataPort port  = (DataPort) fi.getFeature() ;

    	String value = PropertyUtils.getStringValue(port.getDataFeatureClassifier(),
    			"Source_Name") ;
    	if(value != null)
    	{
    		sampleInfo.dataType=value ;
    	}
    	else
    	{
    		sampleInfo.dataType = getGenerationIdentifier(port.getDataFeatureClassifier().getQualifiedName()) ;
    	}
    }

    sampleInfo.id = id;
    sampleInfo.uniqueId = getFeatureLocalIdentifier(fi);

    if(getSampleInfo(p, sampleInfo))
    {
      pp.sampleInfo.add(sampleInfo) ;
    }
  }

  private DirectionType getDirection(FeatureInstance fi) {
	Port p = (Port) fi.getFeature();
	if(p.getDirection().equals(DirectionType.IN_OUT))
		return p.getDirection();
	else if(fi.eContainer() instanceof FeatureInstance)
	{
		FeatureInstance featureGroup = (FeatureInstance) fi.eContainer();
		return getDirection(featureGroup, p.getDirection());
	}
	else
	{
		return p.getDirection();
	}
  }

  private DirectionType getDirection(FeatureInstance featureGroup, DirectionType direction) {
	FeatureGroup fg = (FeatureGroup) featureGroup.getFeature();
	boolean inverse = (fg.isInverse() || fg.getFeatureGroupType().getInverse()!=null);
	if(featureGroup.eContainer() instanceof FeatureInstance)
	{
		if(inverse)
			return getDirection((FeatureInstance) featureGroup.eContainer(), oppositeDirection(direction));
		else
			return getDirection((FeatureInstance) featureGroup.eContainer(), direction);
	}
	else
	{
		if(inverse)
			return oppositeDirection(direction);
		else
			return direction;
	}
  }
  
  private DirectionType oppositeDirection(DirectionType direction) {
	  if(direction.equals(DirectionType.IN))
		  return DirectionType.OUT;
	  else
		  return DirectionType.IN;
  }

  private boolean getSampleInfo(Port port, SampleInfo info)
  {
    boolean result = true ;
    if(info.refresh == -1)
    {
      Long value = PropertyUtils.getIntValue(port,
          "Sampling_Refresh_Period", AadlProject.MS_LITERAL) ;
      if(value != null)
      {
        info.refresh = value ;
      }
      else
      {
        String msg =  "set default Sampling_Refresh_Period value for sampling port \'" +
            port.getQualifiedName() + "\'" ;
        _LOGGER.warn(msg);

        info.refresh = 10l;
        //TODO: restore and resolve issue with pingpong-ba result = false ;
      }
    }

    return result ;
  }

  protected void genFileIncludedMainImpl() {
	  // Files included.
	  _mainCCode.addOutputNewline(GenerationUtilsC.generateSectionMark()) ;
	  _mainCCode.addOutputNewline(GenerationUtilsC
			  .generateSectionTitle("INCLUSION")) ;

	  _mainCCode.addOutputNewline("#include \"globals.h\"") ;
	  _mainCCode.addOutputNewline("#include \"main.h\"") ;
	  _mainCCode.addOutputNewline("#include \"subprograms.h\"") ;
	  _mainCCode.addOutputNewline("#include \"activity.h\"") ;
	  _mainCCode.addOutputNewline("#include \"bitset.h\"");
  }

}
