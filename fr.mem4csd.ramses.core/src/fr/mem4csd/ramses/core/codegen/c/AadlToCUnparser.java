/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen.c;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.AccessSpecification;
import org.osate.aadl2.AccessType;
import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.BehavioredImplementation;
import org.osate.aadl2.CalledSubprogram;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.ClassifierValue;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentPrototypeActual;
import org.osate.aadl2.ComponentPrototypeBinding;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.Connection;
import org.osate.aadl2.ConnectionEnd;
import org.osate.aadl2.DataAccess;
import org.osate.aadl2.DataClassifier;
import org.osate.aadl2.DataImplementation;
import org.osate.aadl2.DataPrototype;
import org.osate.aadl2.DataSubcomponent;
import org.osate.aadl2.DataSubcomponentType;
import org.osate.aadl2.DataType;
import org.osate.aadl2.DefaultAnnexLibrary;
import org.osate.aadl2.DefaultAnnexSubclause;
import org.osate.aadl2.Element;
import org.osate.aadl2.EnumerationLiteral;
import org.osate.aadl2.Feature;
import org.osate.aadl2.FeaturePrototypeBinding;
import org.osate.aadl2.IntegerLiteral;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.Mode;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.NamedValue;
import org.osate.aadl2.Parameter;
import org.osate.aadl2.PortSpecification;
import org.osate.aadl2.ProcessImplementation;
import org.osate.aadl2.ProcessSubcomponent;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.PrototypeBinding;
import org.osate.aadl2.ReferenceValue;
import org.osate.aadl2.StringLiteral;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.SubcomponentType;
import org.osate.aadl2.SubprogramAccess;
import org.osate.aadl2.SubprogramCall;
import org.osate.aadl2.SubprogramCallSequence;
import org.osate.aadl2.SubprogramClassifier;
import org.osate.aadl2.SubprogramGroupAccess;
import org.osate.aadl2.SubprogramImplementation;
import org.osate.aadl2.SubprogramSubcomponent;
import org.osate.aadl2.SubprogramSubcomponentType;
import org.osate.aadl2.SubprogramType;
import org.osate.aadl2.SystemSubcomponent;
import org.osate.aadl2.ThreadClassifier;
import org.osate.aadl2.ThreadImplementation;
import org.osate.aadl2.ThreadSubcomponent;
import org.osate.aadl2.ThreadType;
import org.osate.aadl2.modelsupport.UnparseText;
import org.osate.aadl2.modelsupport.modeltraversal.AadlProcessingSwitch;
import org.osate.aadl2.util.Aadl2Switch;
import org.osate.annexsupport.AnnexRegistry;
import org.osate.annexsupport.AnnexUnparser;
import org.osate.annexsupport.AnnexUnparserRegistry;
import org.osate.ba.AadlBaParserAction;
import org.osate.ba.AadlBaUnParserAction;
import org.osate.ba.aadlba.BehaviorAnnex;
import org.osate.ba.analyzers.TypeHolder;
import org.osate.ba.utils.AadlBaUtils;
import org.osate.ba.utils.DimensionException;
import org.osate.utils.internal.Aadl2Utils;
import org.osate.utils.internal.PropertyUtils;
import org.osate.utils.internal.names.DataModelProperties;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.codegen.GenerationException;
import fr.mem4csd.ramses.core.codegen.c.behaviorannex.AadlBaToCUnparser;
import fr.mem4csd.ramses.core.codegen.c.behaviorannex.AadlBaToCUnparserAction;
import fr.mem4csd.ramses.core.codegen.utils.DataSizeHelper;
import fr.mem4csd.ramses.core.codegen.utils.GenerationUtilsC;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;
import fr.mem4csd.ramses.core.helpers.impl.RamsesException;
import fr.mem4csd.ramses.core.workflowramses.impl.AadlToSourceCodeGeneratorImpl;

public class AadlToCUnparser extends AadlToSourceCodeGeneratorImpl {

	private AadlToCUnparserSwitch unparserSwitch = new AadlToCUnparserSwitch();
	
	private AnnexUnparserRegistry _unparserRegistry = (AnnexUnparserRegistry) AnnexRegistry
			.getRegistry(AnnexRegistry.ANNEX_UNPARSER_EXT_ID);

	private static AadlToCUnparser singleton;
	private AadlBaToCUnparserAction baToCUnparserAction = new AadlBaToCUnparserAction();

	// gtype.c and .h
	protected AadlToCSwitchProcess _gtypesImplCode;
	protected AadlToCSwitchProcess _gtypesHeaderCode;

	// subprogram.c and .h
	protected AadlToCSwitchProcess _subprogramImplCode;
	protected AadlToCSwitchProcess _subprogramHeaderCode;

	// activity.c and .h
	protected AadlToCSwitchProcess _activityImplCode;
	protected AadlToCSwitchProcess _activityHeaderCode;

	// process's globals.c and .h
	protected AadlToCSwitchProcess _globalsImplCode;
	protected AadlToCSwitchProcess _globalsHeaderCode;

	// process's dimensions.h
	protected AadlToCSwitchProcess _dimensionsHeaderCode;

	// Temporary .c and .h files.
	private AadlToCSwitchProcess _currentImplUnparser;
	private AadlToCSwitchProcess _currentHeaderUnparser;

	private Map<AadlToCSwitchProcess, Set<String>> _additionalHeaders;

	private List<String> _processedTypes;

	public Set<NamedElement> additionalUnparsing = new LinkedHashSet<NamedElement>();

	private static final String MAIN_HEADER_INCLUSION = "#include \"main.h\"\n";
	// Map Data Access with their relative Data Subcomponent. Relations
	// are defined in the process implementation via connections.
	private Map<DataAccess, DataSubcomponent> _dataAccessMapping = new HashMap<DataAccess, DataSubcomponent>();

	private NamedElement _owner;

	/**
	 * Stack of subprogram classifiers currently uunparsed. Used to resolve
	 * prototype dependencies between subprogram calls.
	 */
	private List<SubprogramClassifier> subprogramsUnparsingStack = new ArrayList<SubprogramClassifier>();

	private SubprogramCallSequence _currentBehaviorCallSequence = null;
	private SubprogramCallSequence _initBehaviorCallSequence = null;
	private Map<Object, Map<NamedElement, String>> _identifierMappingWithContext = new HashMap<Object, Map<NamedElement, String>>();

	private static Logger _LOGGER = Logger.getLogger(AadlToCUnparser.class);

	public static AadlToCUnparser getAadlToCUnparser() {
		if (singleton == null)
			singleton = new AadlToCUnparser();
		return singleton;
	}

	private AadlToCUnparser() {
		super();
		init();
	}

	class AadlToCUnparserSwitch extends AadlProcessingSwitch
	{

		@Override
		protected void initSwitches() {
			aadl2Switch = new Aadl2Switch<String>() {
				@Override
				public String caseDataType(DataType object) {
					if (_processedTypes.contains(object.getQualifiedName())) {
						return DONE;
					}
					_gtypesHeaderCode.processComments(object);
					getCTypeDeclarator((NamedElement) object);
					_processedTypes.add(object.getQualifiedName());
					return DONE;
				}

				public String caseAadlPackage(AadlPackage object) {
					process(object.getOwnedPublicSection());
					process(object.getOwnedPrivateSection());
					return DONE;
				}

				/**
				 * unparses annex library
				 *
				 * @param al AnnexLibrary object
				 */
				public String caseAnnexLibrary(AnnexLibrary al) {
					//String annexName = al.getName();
					AnnexUnparser unparser = getUnparser(AnnexRegistry.ANNEX_UNPARSER_EXT_ID); // FIXME Maybe not the correct name
//	              ServiceProvider.getServiceRegistry()
//	                    .getUnparser(annexName) ;

					if (unparser != null) {
						unparser.unparseAnnexLibrary(al, _currentImplUnparser.getIndent());
					}

					return DONE;
				}
				
				public AnnexUnparser getUnparser(String annexName)
				{
				  return _unparserRegistry.getAnnexUnparser(annexName.toLowerCase()) ;
				}

				/**
				 * unparses default annex library
				 *
				 * @param dal DefaultAnnexLibrary object
				 */
				public String caseDefaultAnnexLibrary(DefaultAnnexLibrary dal) {
					AnnexUnparser unparser = getUnparser(AnnexRegistry.ANNEX_UNPARSER_EXT_ID); // FIXME Maybe not the correct name
//	              ServiceProvider.getServiceRegistry().getUnparser("*") ;

					if (unparser != null) {
					}

					return DONE;
				}

				public String caseDataImplementation(DataImplementation object) {
					if (_processedTypes.contains(object.getQualifiedName())) {
						return DONE;
					}
					_currentHeaderUnparser = _gtypesHeaderCode;
					_gtypesHeaderCode.processComments(object);
					getCTypeDeclarator((NamedElement) object);	
					_processedTypes.add(object.getQualifiedName());
					return null;
				}

				public String caseDataSubcomponent(DataSubcomponent object) {
					AadlToCSwitchProcess unparser;

					if (object.getContainingComponentImpl() instanceof DataImplementation) {
						unparser = _currentHeaderUnparser;
					} else {
						unparser = _currentImplUnparser;
					}

					unparser.processComments(object);
					DataSubcomponentType dst = object.getDataSubcomponentType();

					processDataSubcomponentType(dst, unparser, _currentHeaderUnparser);

					unparser.addOutput(" ");
					unparser.addOutput(GenerationUtilsC.getGenerationCIdentifier(object.eContainer(), object,
							_identifierMappingWithContext));
					unparser.addOutput(GeneratorUtils.getInitialValue(object, "c"));
					unparser.addOutputNewline(";");

					if (_processedTypes.contains(object.getDataSubcomponentType().getQualifiedName()) == false) {
						_processedTypes.add(object.getQualifiedName());
						process(object.getDataSubcomponentType());
					}

					return DONE;
				}

				public String caseProcessImplementation(ProcessImplementation object) {

					int tasksNb = object.getOwnedThreadSubcomponents().size();
					int modesNb = object.getOwnedModes().size()+1;

					if (modesNb == 0)
						modesNb = 1;
					_dimensionsHeaderCode.addOutputNewline("#define TASKS_NB " + tasksNb);
					_dimensionsHeaderCode.addOutputNewline("#define MODES_NB " + modesNb);

					GeneratorUtils.buildDataAccessMapping(object, _dataAccessMapping);

					processEList(object.getOwnedThreadSubcomponents());

					_currentImplUnparser = _globalsImplCode;
					_owner = object;
					// *** Generate globals.c ***

					for (DataSubcomponent ds : getAllDataSubcomponents(object)) {
						String globalVariableId = null;
						DataSubcomponentType globalVariableType = null;
						globalVariableType = ds.getDataSubcomponentType();
//						if (false == _dataAccessMapping.containsValue(ds.getName())) {
							globalVariableId = GenerationUtilsC.getGenerationCIdentifier(ds.eContainer(), ds,
									_identifierMappingWithContext);
//						} else {
//							if (false == _dataAccessMapping.isEmpty()) {
//								List<String> treatedDeclarations = new ArrayList<String>();
//								for (DataAccess d : _dataAccessMapping.keySet()) {
//									globalVariableId = GenerationUtilsC.getGenerationCIdentifier(
//											d.getContainingClassifier(), d, _identifierMappingWithContext);
//									if (treatedDeclarations.contains(globalVariableId)) {
//										continue;
//									} else if (ds.equals(_dataAccessMapping.get(d))) {
//										//DataSubcomponentType dst = d.getDataFeatureClassifier();
//										treatedDeclarations.add(globalVariableId);
//										break;
//									}
//								}
//							}
	//
//						}
						// Add forward variable declaration in header file
						_globalsHeaderCode.addOutput("extern ");
						processDataSubcomponentType(globalVariableType, _globalsHeaderCode, _globalsHeaderCode);
						_globalsHeaderCode.addOutput(" ");
						_globalsHeaderCode.addOutput(globalVariableId);
						_globalsHeaderCode.addOutputNewline(";");

						processDataSubcomponentType(globalVariableType, _globalsImplCode, _globalsHeaderCode);
						_globalsImplCode.addOutput(" " + ds.getName());
						_globalsImplCode.addOutput(GeneratorUtils.getInitialValue(ds, "c"));
						_globalsImplCode.addOutputNewline(";");
						process(ds.getDataSubcomponentType());
					}
					return DONE;
				}

				public String caseProcessSubcomponent(ProcessSubcomponent object) {
					long coresNb = 1;
					NamedElement cpu = AadlHelperImpl.getDeloymentProcessorSubcomponent(object);

					if (cpu instanceof SystemSubcomponent) {
						coresNb = AadlHelperImpl.getProcessorsNumber((SystemSubcomponent)cpu);
					}

					_dimensionsHeaderCode.addOutputNewline("#define CORES_NB " + coresNb);

					process(object.getComponentImplementation());
					List<NamedElement> additionalNamedElement = new ArrayList<NamedElement>();
					while (false == additionalUnparsing.isEmpty()) {
						additionalNamedElement.addAll(additionalUnparsing);
						additionalUnparsing.clear();
						for (NamedElement ne : additionalNamedElement)
							process(ne);
					}
					return DONE;
				}

				public String caseSubprogramSubcomponent(SubprogramSubcomponent object) {
					process(object.getSubprogramSubcomponentType());
					return DONE;
				}

				public String caseThreadImplementation(ThreadImplementation object) {
					for (SubprogramSubcomponent sc : object.getOwnedSubprogramSubcomponents()) {
						process(sc);
					}
					if (_processedTypes.contains(object.getQualifiedName())) {
						return DONE;
					}
					_processedTypes.add(object.getQualifiedName());
					_currentImplUnparser = _activityImplCode;
					_currentHeaderUnparser = _activityHeaderCode;
					GeneratorUtils.buildDataAccessMapping(object, _dataAccessMapping);
					process(object.getType());

					BehaviorAnnex ba = getAnnexSubclause(object);
					if (ba != null) {
						String aadlComponentCId = GenerationUtilsC.getGenerationCIdentifier(null, object,
								_identifierMappingWithContext);

						_currentImplUnparser.addOutputNewline(aadlComponentCId + "_BA_State_t " + "BA_current_state = "
								+ aadlComponentCId + "_" + AadlBaToCUnparser.getInitialStateIdentifier(ba) + ";");
					}

					_owner = object;

					List<DataSubcomponent> processedDS = new ArrayList<DataSubcomponent>();
					if (_initBehaviorCallSequence != null) {
						for (DataSubcomponent d : getAllDataSubcomponents(object)) {
							if (isUsedInCallSequence(d, _initBehaviorCallSequence)) {
								process(d);
								processedDS.add(d);
							}
						}
						process(_initBehaviorCallSequence);
						_initBehaviorCallSequence = null;
					}

					for (DataSubcomponent d : getAllDataSubcomponents(object)) {
						if (isUsedInCallSequence(d, _currentBehaviorCallSequence) && !processedDS.contains(d))
							process(d);
					}

					processBehavioredImplementation(object);

					if (_currentBehaviorCallSequence != null)
						_currentBehaviorCallSequence = null;

					return null;
				}

				private boolean isUsedInCallSequence(DataSubcomponent d, SubprogramCallSequence callSequence) {
					ComponentImplementation ci = (ComponentImplementation) callSequence.getContainingClassifier();
					for (Connection cnx : ci.getOwnedConnections()) {
						if (cnx.getAllDestinationContext() != null) {
							if (cnx.getAllDestinationContext().eContainer().equals(callSequence)
									&& cnx.getAllSource().equals(d)) {
								return true;
							}
						}
						if (cnx.getAllSourceContext() != null) {
							if (cnx.getAllSourceContext().eContainer().equals(callSequence)
									&& cnx.getAllDestination().equals(d)) {
								return true;
							}
						}
					}
					return false;
				}

				public String caseSubprogramCallSequence(SubprogramCallSequence object) {
					for (SubprogramCall cs : object.getOwnedSubprogramCalls()) {
						if (cs instanceof SubprogramCall) {
							SubprogramCall sc = (SubprogramCall) cs;
							process(sc);
							if (cs.eContainer().eContainer() instanceof ThreadImplementation) {
								_currentImplUnparser = _activityImplCode;
								_currentHeaderUnparser = _activityHeaderCode;
							}
						}
					}
					return null;
				}

				public String caseSubprogramType(SubprogramType object) {
					_currentImplUnparser = _subprogramImplCode;
					_currentHeaderUnparser = _subprogramHeaderCode;

					if (_processedTypes.contains(object.getQualifiedName())) {
						return DONE;
					}
					_processedTypes.add(object.getQualifiedName());

					try {
						resolveExistingCodeDependencies(object, null, _subprogramHeaderCode);
					} catch (Exception e1) {
						caseSubprogramClassifier((SubprogramClassifier) object);

						processBehavioredType(object);

						_subprogramImplCode.decrementIndent();
						_subprogramImplCode.addOutputNewline("}");
					}

					return DONE;
				}

				private BehaviorAnnex getAnnexSubclause(ComponentClassifier object) {
					for (AnnexSubclause as : object.getOwnedAnnexSubclauses()) {
						if (as instanceof BehaviorAnnex) {
							return (BehaviorAnnex) as;
						} else {
							DefaultAnnexSubclause das = (DefaultAnnexSubclause) as;
							AnnexSubclause potentialBa = das.getParsedAnnexSubclause();
							if (potentialBa instanceof BehaviorAnnex)
								return (BehaviorAnnex) potentialBa;
						}
					}
					if (object.getExtended() != null)
						return getAnnexSubclause((SubprogramClassifier) object.getExtended());
					return null;
				}

				public String caseSubprogramClassifier(SubprogramClassifier object) {
					subprogramsUnparsingStack.add(object);

					Parameter returnParameter = null;
					List<Feature> orderedFeatureList = null;
					if (object instanceof SubprogramImplementation) {
						SubprogramImplementation si = (SubprogramImplementation) object;
						orderedFeatureList = Aadl2Utils.orderFeatures(si.getType());
					} else if (object instanceof SubprogramType) {
						SubprogramType st = (SubprogramType) object;
						orderedFeatureList = Aadl2Utils.orderFeatures(st);
					}

					boolean isReturnParam = false;
					for (Feature param : orderedFeatureList) {
						if (param instanceof Parameter) {
							Parameter p = (Parameter) param;
							isReturnParam = GenerationUtilsC.isReturnParameter(p);
						}
					}

					if (!isReturnParam) {
						_subprogramImplCode.addOutput("void ");
						_subprogramHeaderCode.addOutput("void ");
					}

					_subprogramImplCode.addOutput(
							GenerationUtilsC.getGenerationCIdentifier(null, object, _identifierMappingWithContext));
					_subprogramHeaderCode.addOutput(
							GenerationUtilsC.getGenerationCIdentifier(null, object, _identifierMappingWithContext));

					_subprogramImplCode.addOutput("(");
					_subprogramHeaderCode.addOutput("(");

					boolean first = true;
					for (Feature f : orderedFeatureList) {
						if (f instanceof Parameter) {
							Parameter p = (Parameter) f;
							String paramUsage = Aadl2Utils.getParameterUsage(p);
							if (p == returnParameter)
								continue;
							if (first) {
								_subprogramImplCode.addOutput("\n\t");
								_subprogramHeaderCode.addOutput("\n\t");
							} else {
								_subprogramImplCode.addOutput(",\n\t");
								_subprogramHeaderCode.addOutput(",\n\t");
							}
							processDataSubcomponentType(object, p.getDataFeatureClassifier(), _subprogramImplCode,
									_subprogramImplCode);
							processDataSubcomponentType(object, p.getDataFeatureClassifier(), _subprogramHeaderCode,
									_subprogramHeaderCode);
							if (Aadl2Utils.isInOutParameter(p) || Aadl2Utils.isOutParameter(p)
									|| paramUsage.equalsIgnoreCase("by_reference")) {
								_subprogramImplCode.addOutput(" * ");
								_subprogramHeaderCode.addOutput(" * ");
							}
							_subprogramImplCode.addOutput(" " + p.getName());
							_subprogramHeaderCode.addOutput(" " + p.getName());
							first = false;

							process(p.getDataFeatureClassifier());
						} else if (f instanceof DataAccess) {
							DataAccess da = (DataAccess) f;
							if (first) {
								_subprogramImplCode.addOutput("\n\t");
								_subprogramHeaderCode.addOutput("\n\t");
							} else {
								_subprogramImplCode.addOutput(",\n\t");
								_subprogramHeaderCode.addOutput(",\n\t");
							}
							processDataSubcomponentType(object, da.getDataFeatureClassifier(), _subprogramImplCode,
									_currentImplUnparser);
							processDataSubcomponentType(object, da.getDataFeatureClassifier(), _subprogramHeaderCode,
									_subprogramHeaderCode);
							if (da.getKind().equals(AccessType.REQUIRES)) {
								if (Aadl2Utils.isReadWriteDataAccess(da) || Aadl2Utils.isWriteOnlyDataAccess(da)) {
									_subprogramImplCode.addOutput(" * ");
									_subprogramHeaderCode.addOutput(" * ");
								}
								_subprogramImplCode.addOutput(" " + da.getName());
								_subprogramHeaderCode.addOutput(" " + da.getName());
							}
							first = false;

							process(da.getDataFeatureClassifier());
						}
					}

					_subprogramImplCode.addOutputNewline(")");
					_subprogramHeaderCode.addOutputNewline(");");
					_subprogramHeaderCode.addOutputNewline("");

					_subprogramImplCode.addOutputNewline("{");
					_subprogramImplCode.incrementIndent();

					BehaviorAnnex ba = getAnnexSubclause(object);

					if (ba != null) {
						String aadlComponentCId = GenerationUtilsC.getGenerationCIdentifier(null, object,
								_identifierMappingWithContext);

						_subprogramImplCode.addOutputNewline(aadlComponentCId + "_BA_State_t " + "BA_current_state = "
								+ aadlComponentCId + "_" + AadlBaToCUnparser.getInitialStateIdentifier(ba) + ";");
					}
					return null;
				}

				public String caseSubprogramImplementation(SubprogramImplementation object) {
					_currentImplUnparser = _subprogramImplCode;
					_currentHeaderUnparser = _subprogramHeaderCode;

					if (_processedTypes.contains(object.getQualifiedName())) {
						return DONE;
					}

					_processedTypes.add(object.getQualifiedName());

					try {
						resolveExistingCodeDependencies(object, null, _subprogramHeaderCode);
					} catch (Exception e1) {
						caseSubprogramClassifier((SubprogramClassifier) object);
						_owner = object;
						for (DataSubcomponent d : getAllDataSubcomponents(object)) {
							process(d);
						}

						processBehavioredImplementation(object);

						_subprogramImplCode.decrementIndent();
						_subprogramImplCode.addOutputNewline("}");
					}
					_subprogramImplCode.addOutputNewline("");
					return DONE;
				}

				public String caseSubprogramCall(SubprogramCall object) {
					Parameter returnParameter = null;
					final CalledSubprogram calledSubprogram = object.getCalledSubprogram();

					if (calledSubprogram != null) {
						SubprogramType st = null;

						SubprogramSubcomponentType sct;

						if (calledSubprogram instanceof SubprogramAccess) {
							sct = ((SubprogramAccess) calledSubprogram).getSubprogramFeatureClassifier();
						} else if (calledSubprogram instanceof SubprogramSubcomponent) {
							sct = ((SubprogramSubcomponent) calledSubprogram).getSubprogramSubcomponentType();
						} else {
							sct = (SubprogramSubcomponentType) calledSubprogram;
						}

						if (sct instanceof SubprogramType) {
							st = (SubprogramType) sct;
						} else {
							SubprogramImplementation si = (SubprogramImplementation) sct;
							st = si.getType();
						}

						List<Feature> orderedFeatureList = Aadl2Utils.orderFeatures(st);
						List<ConnectionEnd> orderedParamValue = new ArrayList<ConnectionEnd>();
						for (Feature f : orderedFeatureList) {
							ConnectionEnd ce = Aadl2Utils.getConnectedEnd(object, f);
							orderedParamValue.add(ce);
						}

						for (Feature param : orderedFeatureList) {
							if (param instanceof Parameter) {
								Parameter p = (Parameter) param;
								boolean isReturnParam = GenerationUtilsC.isReturnParameter(p);
								;

								if (isReturnParam) {
									returnParameter = p;
									ConnectionEnd ce = orderedParamValue.get(orderedFeatureList.indexOf(p));
									processConnectionEnd(ce);
									_currentImplUnparser.addOutput(" = ");
									break;
								}
							}
						}

						try {
							resolveExistingCodeDependencies(sct, _currentImplUnparser, _currentHeaderUnparser);
						} catch (Exception e1) {
							_currentImplUnparser.addOutput(GenerationUtilsC.getGenerationCIdentifier(
									object.getContainingClassifier(), sct, _identifierMappingWithContext));
						}

						if (st != null) {
							_currentImplUnparser.addOutput(" (");
							boolean first = true;
							for (Feature feature : orderedFeatureList) {
								if (feature instanceof Parameter) {
									Parameter p = (Parameter) feature;
									if (p == returnParameter)
										continue;
									if (first == false)
										_currentImplUnparser.addOutput(", ");
									String paramUsage = Aadl2Utils.getParameterUsage(p);
									if (Aadl2Utils.isInOutParameter(p) || Aadl2Utils.isOutParameter(p)
											|| paramUsage.equalsIgnoreCase("by_reference")) {
										_currentImplUnparser.addOutput("&");
									}
									ConnectionEnd ce = orderedParamValue.get(orderedFeatureList.indexOf(p));
									if (ce != null)
										processConnectionEnd(ce);
									first = false;
								} else if (feature instanceof DataAccess) {
									DataAccess da = (DataAccess) feature;
									if (first == false)
										_currentImplUnparser.addOutput(", ");
									if (da.getKind().equals(AccessType.REQUIRES)) {
										if (Aadl2Utils.isReadWriteDataAccess(da) || Aadl2Utils.isWriteOnlyDataAccess(da)) {
											_currentImplUnparser.addOutput("&");
										}
									}

									String name = null;

									// If a data access mapping is provided:
									// Transforms any data access into the right data subcomponent
									// 's name thanks to the given data access mapping.
									ConnectionEnd parentAccess = orderedParamValue.get(orderedFeatureList.indexOf(da));
									if (_dataAccessMapping != null && parentAccess instanceof DataAccess) {
										DataSubcomponent ds = _dataAccessMapping.get((DataAccess) parentAccess);
										name = GenerationUtilsC.getGenerationCIdentifier(ds.getContainingClassifier(), ds,
												_identifierMappingWithContext);
									}

									if (name != null) {
										_currentImplUnparser.addOutput(name);
									} else {
										processConnectionEnd(parentAccess);
									}
									first = false;
								}

							}

							_currentImplUnparser.addOutputNewline(");");
						}
						process(sct);
					}
					return DONE;
				}

				private void processConnectionEnd(ConnectionEnd ce) {
					if (ce instanceof DataSubcomponent) {
						_currentImplUnparser.addOutput(GenerationUtilsC
								.getGenerationCIdentifier(ce.getContainingClassifier(), ce, _identifierMappingWithContext));
					} else {
						_currentImplUnparser.addOutput(ce.getName());
					}
				}

				public String caseThreadSubcomponent(ThreadSubcomponent object) {
					PropertyAssociation pa = PropertyUtils.findPropertyAssociation("Compute_Entrypoint_Call_Sequence",
							object);

					if (pa != null) {
						ReferenceValue rv = (ReferenceValue) pa.getOwnedValues().get(0).getOwnedValue();
						NamedElement value = rv.getContainmentPathElements().get(0).getNamedElement();
						if (value instanceof SubprogramCallSequence)
							_currentBehaviorCallSequence = (SubprogramCallSequence) value;
						pa = PropertyUtils.findPropertyAssociation("Initialize_Entrypoint_Call_Sequence", object);
						if (pa != null) {
							rv = (ReferenceValue) pa.getOwnedValues().get(0).getOwnedValue();
							_initBehaviorCallSequence = (SubprogramCallSequence) rv.getContainmentPathElements().get(0)
									.getNamedElement();
						}
						_activityImplCode.addOutput("void ");
						_activityImplCode.addOutput(object.getName());
						_activityImplCode.addOutputNewline(GenerationUtilsC.THREAD_SUFFIX + "()");
						_activityImplCode.addOutputNewline("{");
						_activityImplCode.incrementIndent();

						process(object.getComponentImplementation());

//						_activityImplCode.addOutputNewline("return 0;");
						
						_activityImplCode.decrementIndent();
						_activityImplCode.addOutputNewline("}");

						_activityHeaderCode.addOutput("void ");
						_activityHeaderCode.addOutput(object.getName());
						_activityHeaderCode.addOutputNewline(GenerationUtilsC.THREAD_SUFFIX + "();\n");
					}

					return null;
				}

				public String caseThreadType(ThreadType object) {
					if (_processedTypes.contains(object.getQualifiedName())) {
						return null;
					}

					_processedTypes.add(object.getQualifiedName());
					_currentHeaderUnparser = _activityHeaderCode;
					_currentImplUnparser = _activityImplCode;

					for (DataAccess d : object.getOwnedDataAccesses()) {
						if (d.getKind().equals(AccessType.REQUIRES)) {
							process(d);
						}
					}

					return null;
				}

				/**
				 * subprogram group access
				 */
				public String caseSubprogramGroupAccess(SubprogramGroupAccess object) {
					return DONE;
				}

				/**
				 * data access
				 */
				public String caseDataAccess(DataAccess object) {
					_currentImplUnparser.addOutput("extern ");

					String dataSubprogramName = null;

					if (_dataAccessMapping != null) {
						DataSubcomponent ds = _dataAccessMapping.get(object);
						dataSubprogramName = GenerationUtilsC.getGenerationCIdentifier(ds.getContainingClassifier(), ds,
								_identifierMappingWithContext);
					}

					final DataSubcomponentType featClass = object.getDataFeatureClassifier();

					try {
						if (featClass == null) {
							throw new GenerationException("Data feature classifier of " + object.getName() + " is null");
						}

						resolveExistingCodeDependencies(featClass, _currentImplUnparser, _currentHeaderUnparser);
					} catch (GenerationException e) {

						String id = PropertyUtils.getStringValue(featClass, "Source_Name");

						if (id == null) {
							id = GenerationUtilsC.getGenerationCIdentifier(baToCUnparserAction, featClass,
									_identifierMappingWithContext);
						}

						if (id != null) {
							_currentImplUnparser.addOutput(id);
						}
					}
					_currentImplUnparser.addOutput(" ");
					if (dataSubprogramName != null) {
						_currentImplUnparser.addOutput(dataSubprogramName);
					} else {
						_currentImplUnparser.addOutput(GenerationUtilsC.getGenerationCIdentifier(
								object.getFeatureClassifier(), object, _identifierMappingWithContext));
					}

					_currentImplUnparser.addOutputNewline(";");

					return DONE;
				}
			};
		}
	}
	
	class NamedElementSort implements Comparator<NamedElement> 
	{ 
	    // Used for sorting in ascending order of 
	    // roll number 
	    public int compare(NamedElement a, NamedElement b) 
	    { 
	        return a.getName().compareTo(b.getName()); 
	    } 
	} 
	
	public void genMainCCode(ProcessSubcomponent process, UnparseText _mainCCode, ArchTraceSpec traces, List<String> additionalHeaders) {

		ProcessImplementation pi = (ProcessImplementation) process.getComponentImplementation();

		for (DataSubcomponent d : pi.getOwnedDataSubcomponents()) {
			if (d.getDataSubcomponentType().getName().equals("ResourceType")) {
				_mainCCode.addOutputNewline("Init_Resource (&"+d.getName()+");");
			}
		}
		
		EList<ThreadSubcomponent> subcomponents = pi.getOwnedThreadSubcomponents();
		for (ThreadSubcomponent ts : subcomponents) {
			String recoveryEntryPoint = GeneratorUtils.getEntryPoint(ts, "Recover_Entrypoint", "Recover_Entrypoint_Source_Text", additionalHeaders);
			if(recoveryEntryPoint == null)
				recoveryEntryPoint = "NULL";
			
			String initializeEntryPoint = GeneratorUtils.getEntryPoint(ts, "Initialize_Entrypoint", "Initialize_Entrypoint_Source_Text", additionalHeaders);
			if(initializeEntryPoint == null)
				initializeEntryPoint = "NULL";
			
			String finalizeEntryPoint = GeneratorUtils.getEntryPoint(ts, "Finalize_Entrypoint", "Finalize_Entrypoint_Source_Text", additionalHeaders);
			if(finalizeEntryPoint == null)
				finalizeEntryPoint = "NULL";
			
			String activateEntryPoint = GeneratorUtils.getEntryPoint(ts, "Activate_Entrypoint", "Activate_Entrypoint_Source_Text", additionalHeaders);
			if(activateEntryPoint == null)
				activateEntryPoint = "NULL";
			
			String deactivateEntryPoint = GeneratorUtils.getEntryPoint(ts, "Deactivate_Entrypoint", "Deactivate_Entrypoint_Source_Text", additionalHeaders);
			if(deactivateEntryPoint == null)
				deactivateEntryPoint = "NULL";
			
			String computeEntryPoint = "NULL";
			
			computeEntryPoint = ts.getName() + "_Job";
			_mainCCode.addOutputNewline("Init_Config (&" + ts.getName() + "_config, " 
					+ recoveryEntryPoint + ","
					+ computeEntryPoint + ","
					+ initializeEntryPoint + ","
					+ finalizeEntryPoint + ","
					+ activateEntryPoint + ","
					+ deactivateEntryPoint + ","
					+ subcomponents.indexOf(ts) + ");");
			for (Mode m : pi.getOwnedModes()) {
				if (ts.getInModes().contains(m) || ts.getInModes().isEmpty())
					_mainCCode.addOutputNewline(
							"BITSET(tasks_in_mode[" + m.getName() + "], " + ts.getName() + "_config.id);");
				String modeManagementThreadPattern = pi.getTypeName()+"_mode_"+m.getName();
				if(ts.getName().equals(modeManagementThreadPattern) || ts.getInModes().isEmpty()) // special case, mode_transition_in_ptogress
					_mainCCode.addOutputNewline(
							"BITSET(tasks_in_mode[mode_transition_in_progress], " + ts.getName() + "_config.id);");
			}
			if (pi.getOwnedModes().isEmpty())
				_mainCCode.addOutputNewline("BITSET(tasks_in_mode[current_mode], " + ts.getName() + "_config.id);");

		}
		_mainCCode.addOutputNewline("int test_threads = 0;");
		for (ThreadSubcomponent ts : subcomponents) {
			genTaskInitCode(ts, _mainCCode);
		}
		_mainCCode.addOutputNewline("if (test_threads != 0)");
		_mainCCode.addOutputNewline("{");
		_mainCCode.incrementIndent();
		_mainCCode.addOutputNewline("#ifdef DEBUG");
		_mainCCode.addOutputNewline(
				"printf(\"Error during creation of threads (with POSIX as a target, make sure you are root)\\n\");");
		_mainCCode.addOutputNewline("#endif");
		_mainCCode.addOutputNewline("return 1;");
		_mainCCode.decrementIndent();
		_mainCCode.addOutputNewline("}");

	}
	
	public void genStopProcessCode(ProcessSubcomponent process, UnparseText _mainCCode)
	{
		ProcessImplementation pi = (ProcessImplementation) process.getComponentImplementation();
		
		List<DataSubcomponent> processorLinkList = new ArrayList<DataSubcomponent>();
		
		for (DataSubcomponent d : pi.getOwnedDataSubcomponents()) {
			if (d.getDataSubcomponentType().getName().equals("ProcessorLinkType")) {
				processorLinkList.add(d);
			}
		}
		
		processorLinkList.sort(new NamedElementSort());
		
		_mainCCode.addOutputNewline("");
		_mainCCode.addOutputNewline("void stop_process()");
		_mainCCode.addOutputNewline("{");
		_mainCCode.incrementIndent();
		for (DataSubcomponent d : processorLinkList)
		{
			_mainCCode.addOutputNewline("close_processor_port(&" + d.getName() + ");");
		}
		_mainCCode.addOutputNewline("exit(0);");
		_mainCCode.decrementIndent();
		_mainCCode.addOutputNewline("}");
		_mainCCode.addOutputNewline("");
		
	}
	
	private void init() {
		_gtypesImplCode = new AadlToCSwitchProcess(unparserSwitch);
		_gtypesImplCode.addOutputNewline("#include \"gtypes.h\"");

		_gtypesHeaderCode = new AadlToCSwitchProcess(unparserSwitch);

		_subprogramImplCode = new AadlToCSwitchProcess(unparserSwitch);
		_subprogramImplCode.addOutputNewline("#include \"subprograms.h\"");

		_subprogramHeaderCode = new AadlToCSwitchProcess(unparserSwitch);
		_subprogramHeaderCode.addOutputNewline("#include \"gtypes.h\"");
		_subprogramHeaderCode.addOutputNewline("#include \"globals.h\"");

		_activityImplCode = new AadlToCSwitchProcess(unparserSwitch);
		_activityImplCode.addOutputNewline("#include \"activity.h\"");
		_activityImplCode.addOutputNewline(MAIN_HEADER_INCLUSION);

		_activityHeaderCode = new AadlToCSwitchProcess(unparserSwitch);
		_activityHeaderCode.addOutputNewline("#include \"subprograms.h\"");

		_globalsImplCode = new AadlToCSwitchProcess(unparserSwitch);
		_globalsImplCode.addOutputNewline("#include \"globals.h\"");

		_globalsHeaderCode = new AadlToCSwitchProcess(unparserSwitch);
		_globalsHeaderCode.addOutputNewline("#include \"gtypes.h\"");

		_dimensionsHeaderCode = new AadlToCSwitchProcess(unparserSwitch);
		_dimensionsHeaderCode.addOutputNewline("#include \"dimensions.h\"");

		_processedTypes = new ArrayList<String>();

		_additionalHeaders = new HashMap<AadlToCSwitchProcess, Set<String>>();
	}

	public List<PrototypeBinding> getCurrentPrototypeBindings(String ctxt) {
		_LOGGER.trace("Inherited prototype bindings for " + ctxt);

		List<PrototypeBinding> bindings = new ArrayList<PrototypeBinding>();
		for (SubprogramClassifier c : subprogramsUnparsingStack) {
			_LOGGER.trace("prototype bindings from " + c.getName());
			List<PrototypeBinding> cBindings = c.getOwnedPrototypeBindings();
			for (PrototypeBinding b : cBindings) {
				if (b instanceof FeaturePrototypeBinding) {
					FeaturePrototypeBinding cpb = (FeaturePrototypeBinding) b;
					SubcomponentType st = null;
					if (cpb.getActual() instanceof AccessSpecification) {
						st = ((AccessSpecification) cpb.getActual()).getClassifier();
					} else if (cpb.getActual() instanceof PortSpecification) {
						st = ((PortSpecification) cpb.getActual()).getClassifier();
					}
					_LOGGER.trace("prototype binding " + b.getFormal().getName() + " => " + st.getName());
				} else {
					ComponentPrototypeBinding cpb = (ComponentPrototypeBinding) b;
					SubcomponentType st = cpb.getActuals().get(0).getSubcomponentType();
					_LOGGER.trace("    prototype binding " + b.getFormal().getName() + " => " + st.getName());
				}
			}

			bindings.addAll(cBindings);
		}
		return bindings;
	}

	public void saveGeneratedFilesContent(URI targetDirectory) {

		_gtypesHeaderCode.addOutputNewline("\n#endif\n");
		_subprogramHeaderCode.addOutputNewline("\n#endif\n");
		_activityHeaderCode.addOutputNewline("\n#endif\n");
		_globalsHeaderCode.addOutputNewline("\n#endif\n");
		_dimensionsHeaderCode.addOutputNewline("\n#endif\n");

		try {
			String headerGuard = null;
			
			boolean isFile = targetDirectory.isFile();
			if(!isFile)
				targetDirectory = URI.createFileURI(targetDirectory.toString());
			
			// gtypes.c
			FileWriter typesFile_C = new FileWriter( targetDirectory.appendSegment( "gtypes.c" ).toFileString() );
			String addTypeHeader_C = getAdditionalHeader(_gtypesImplCode);
			saveFile(typesFile_C, addTypeHeader_C, _gtypesImplCode.getOutput());

			// gtypes.h
			FileWriter typesFile_H = new FileWriter(targetDirectory.appendSegment( "gtypes.h" ).toFileString() );
			headerGuard = GenerationUtilsC.generateHeaderInclusionGuard("gtypes.h");
			String addTypeHeader_H = getAdditionalHeader(_gtypesHeaderCode);
			saveFile(typesFile_H, headerGuard, addTypeHeader_H, _gtypesHeaderCode.getOutput());

			// subprogram.c
			FileWriter subprogramsFile_C = new FileWriter(targetDirectory.appendSegment( "subprograms.c" ).toFileString() );
			String addSubprogramHeader_C = getAdditionalHeader(_subprogramImplCode);
			saveFile(subprogramsFile_C, addSubprogramHeader_C, _subprogramImplCode.getOutput());

			// subprogram.h
			FileWriter subprogramsFile_H = new FileWriter(targetDirectory.appendSegment( "subprograms.h" ).toFileString() );
			headerGuard = GenerationUtilsC.generateHeaderInclusionGuard("subprograms.h");
			String addSubprogramsHeader_H = getAdditionalHeader(_subprogramHeaderCode);
			saveFile(subprogramsFile_H, headerGuard, addSubprogramsHeader_H, _subprogramHeaderCode.getOutput());

			// activity.c
			FileWriter activityFile_C = new FileWriter(targetDirectory.appendSegment( "activity.c" ).toFileString() );
			String addActivityHeader_C = getAdditionalHeader(_activityImplCode);
			saveFile(activityFile_C, addActivityHeader_C, _activityImplCode.getOutput());

			// activity.h
			FileWriter activityFile_H = new FileWriter(targetDirectory.appendSegment( "activity.h" ).toFileString() );
			headerGuard = GenerationUtilsC.generateHeaderInclusionGuard("activity.h");
			String addActivityHeader_H = getAdditionalHeader(_activityHeaderCode);
			saveFile(activityFile_H, headerGuard, addActivityHeader_H, _activityHeaderCode.getOutput());

			// partition's globals.c
			FileWriter globalsFile_C = new FileWriter(targetDirectory.appendSegment( "globals.c" ).toFileString() );
			String addGlobalstHeader_C = getAdditionalHeader(_globalsImplCode);
			saveFile(globalsFile_C, addGlobalstHeader_C, _globalsImplCode.getOutput());

			// partition's globals.h
			FileWriter globalsFile_H = new FileWriter(targetDirectory.appendSegment( "globals.h").toFileString() );
			headerGuard = GenerationUtilsC.generateHeaderInclusionGuard("globals.h");
			String addGlobalsHeader_H = getAdditionalHeader(_globalsHeaderCode);
			saveFile(globalsFile_H, headerGuard, MAIN_HEADER_INCLUSION, addGlobalsHeader_H,
					_globalsHeaderCode.getOutput());

			// partition's dimensions.h
			FileWriter dimentionsFile_H = new FileWriter(targetDirectory.appendSegment( "dimensions.h" ).toFileString() );
			headerGuard = GenerationUtilsC.generateHeaderInclusionGuard("dimensions.h");
			String addDimensionsHeader_H = getAdditionalHeader(_globalsHeaderCode);
			saveFile(dimentionsFile_H, headerGuard, addDimensionsHeader_H, _dimensionsHeaderCode.getOutput());

			clean();

		} catch (IOException e) {
			String errMsg = "cannot write or save the generated files";
			_LOGGER.fatal(errMsg, e);
			throw new RuntimeException(errMsg, e);
		}
	}

	private void clean() {
		this._additionalHeaders.clear();
		this._dataAccessMapping.clear();
		baToCUnparserAction = new AadlBaToCUnparserAction();
		takenId = new ArrayList<String>();
	}

	private String getAdditionalHeader(AadlToCSwitchProcess fileUnparser) {
		return GenerationUtilsC.getAdditionalHeader(fileUnparser, _additionalHeaders);
	}

	private void saveFile(FileWriter file, String... content) throws IOException {
		BufferedWriter output;
		StringBuilder sb = new StringBuilder();

		for (String s : content) {
			sb.append(s);
		}

		output = new BufferedWriter(file);
		output.write(sb.toString());
		output.close();
	}

	public boolean resolveExistingCodeDependencies(NamedElement object, AadlToCSwitchProcess sourceNameDest,
			AadlToCSwitchProcess sourceTextDest) throws GenerationException {
		Set<String> l;
		if (_additionalHeaders.containsKey(sourceTextDest) == false) {
			l = new HashSet<String>();
			_additionalHeaders.put(sourceTextDest, l);
		} else {
			l = _additionalHeaders.get(sourceTextDest);
		}
		String sourceName = GenerationUtilsC.resolveExistingCodeDependencies(object, l);
		if (sourceName != null) {
			if (sourceNameDest != null)
				sourceNameDest.addOutput(sourceName);
			return true;
		} else {
			String errMsg = "In component " + object.getName() + ": Source_Text "
					+ "property should also reference a header (.h extension) file";
			_LOGGER.fatal(errMsg);
			throw new GenerationException(errMsg);
		}
	}

	protected void processDataSubcomponentType(DataSubcomponentType dst, AadlToCSwitchProcess sourceNameDest,
			AadlToCSwitchProcess sourceTextDest) {
		processDataSubcomponentType((Classifier) _owner, dst, sourceNameDest, sourceTextDest);
	}

	protected void processDataSubcomponentType(Classifier owner, DataSubcomponentType dst,
			AadlToCSwitchProcess sourceNameDest, AadlToCSwitchProcess sourceTextDest) {

		try {
			resolveExistingCodeDependencies(dst, sourceNameDest, sourceTextDest);
		} catch (Exception e) {
			if (dst instanceof DataPrototype && owner != null) {
				for (PrototypeBinding pb : owner.getOwnedPrototypeBindings()) {
					if (pb instanceof FeaturePrototypeBinding
							&& pb.getFormal().getName().equalsIgnoreCase(dst.getName())) {
						FeaturePrototypeBinding fpb = (FeaturePrototypeBinding) pb;
						if (fpb.getActual() instanceof AccessSpecification) {
							AccessSpecification as = (AccessSpecification) fpb.getActual();
							if (as.getClassifier() instanceof DataSubcomponentType)
								processDataSubcomponentType(owner, (DataSubcomponentType) as.getClassifier(),
										sourceNameDest, sourceTextDest);
						} else if (fpb.getActual() instanceof PortSpecification) {
							PortSpecification ps = (PortSpecification) fpb.getActual();
							if (ps.getClassifier() instanceof DataSubcomponentType)
								processDataSubcomponentType(owner, (DataSubcomponentType) ps.getClassifier(),
										sourceNameDest, sourceTextDest);
						}
					}
					if (pb instanceof ComponentPrototypeBinding
							&& pb.getFormal().getName().equalsIgnoreCase(dst.getName())) {
						for (ComponentPrototypeActual pa : ((ComponentPrototypeBinding) pb).getActuals()) {
							if (pa.getSubcomponentType() instanceof DataSubcomponentType) {
								DataSubcomponentType dstActual = (DataSubcomponentType) pa.getSubcomponentType();
								processDataSubcomponentType(owner, dstActual, sourceNameDest, sourceTextDest);
								break;
							}
						}
					}
				}
			} else {
				String id = PropertyUtils.getStringValue(dst, "Source_Name");
				if (id == null)
					id = GenerationUtilsC.getGenerationCIdentifier(owner, dst, _identifierMappingWithContext);
				sourceNameDest.addOutput(id);
			}
		}
	}

	boolean processBehavioredType(ComponentType type) {
		return processBehavioredType(type, type);
	}

	boolean processBehavioredType(ComponentType type, ComponentType owner) {
		boolean foundRestrictedAnnex = false;
		for (AnnexSubclause annex : type.getOwnedAnnexSubclauses()) {
			if (annex.getName().equalsIgnoreCase(AadlBaParserAction.ANNEX_NAME)) {
				foundRestrictedAnnex = true;
				processAnnexSubclause(annex, owner);
				break;
			}
		}
		if (!foundRestrictedAnnex && type.getExtended() != null)
			foundRestrictedAnnex = processBehavioredType(type.getExtended(), owner);
		return foundRestrictedAnnex;
	}

	/**
	 * unparses annex subclause
	 *
	 * @param as AnnexSubclause object
	 */
	public String processAnnexSubclause(AnnexSubclause as, NamedElement owner) {
		AadlToCSwitchProcess codeUnparser;
		AadlToCSwitchProcess headerUnparser;
		if (owner instanceof SubprogramClassifier) {
			codeUnparser = _subprogramImplCode;
			headerUnparser = _subprogramHeaderCode;
		} else {
			codeUnparser = _activityImplCode;
			headerUnparser = _subprogramHeaderCode;
		}

		// XXX May AadlBaToCUnparser have its own interface ???
		// XXX NO, using interfaces and in particular extension points is an overkill

		AadlBaToCUnparser baToCUnparser = (AadlBaToCUnparser) baToCUnparserAction.getUnparser();

		baToCUnparser.setDataAccessMapping(_dataAccessMapping);
		baToCUnparser.setIdentifierMapping(_identifierMappingWithContext);
		baToCUnparser.setInitialIndent(codeUnparser.getIndent());
		baToCUnparser.setOwner(owner);

		baToCUnparserAction.unparseAnnexSubclause(as, codeUnparser.getIndent());

		baToCUnparser.addIndent_C(codeUnparser.getIndent());
		baToCUnparser.addIndent_H(headerUnparser.getIndent());
		codeUnparser.addOutput(baToCUnparser.getCContent());
		headerUnparser.addOutput(baToCUnparser.getHContent());

		if (_additionalHeaders.get(headerUnparser) == null) {
			Set<String> t = new HashSet<String>();
			_additionalHeaders.put(headerUnparser, t);
		}

		_additionalHeaders.get(headerUnparser).addAll(baToCUnparser.getAdditionalHeaders());
		baToCUnparser.getAdditionalHeaders().clear();

		if (owner instanceof SubprogramType) {
			subprogramsUnparsingStack.remove(subprogramsUnparsingStack.size() - 1);
		}

		for (NamedElement ne : baToCUnparser.getCoreElementsToBeUnparsed()) {
			unparserSwitch.process(ne);
		}

		return AadlToCUnparserSwitch.DONE;
	}

	boolean processBehavioredImplementation(BehavioredImplementation object) {
		return processBehavioredImplementation(object, object);
	}

	public boolean processBehavioredImplementation(BehavioredImplementation object, BehavioredImplementation owner) {
		boolean foundRestrictedAnnex = false;
		for (AnnexSubclause annex : object.getOwnedAnnexSubclauses()) {
			if (annex.getName().equalsIgnoreCase(AadlBaUnParserAction.ANNEX_NAME)) {
				foundRestrictedAnnex = true;
				processAnnexSubclause(annex, owner);
				break;
			}
		}
		if (!foundRestrictedAnnex && !object.getOwnedSubprogramCallSequences().isEmpty()) {
			if (object instanceof ThreadClassifier)
				unparserSwitch.process(_currentBehaviorCallSequence);
			else {
				for (SubprogramCallSequence scs : object.getOwnedSubprogramCallSequences()) {
					unparserSwitch.process(scs);
				}
			}
			foundRestrictedAnnex = true;
		}
		if (!foundRestrictedAnnex && object.getExtended() != null)
			foundRestrictedAnnex = processBehavioredImplementation((BehavioredImplementation) object.getExtended(),
					object);
		return foundRestrictedAnnex;
	}

	protected void getCTypeDeclarator(NamedElement object) {
		if (_processedTypes.contains(object.getQualifiedName()))
			return;

		if (_additionalHeaders.containsKey(_gtypesHeaderCode) == false) {
			_additionalHeaders.put(_gtypesHeaderCode, new HashSet<String>());
		}
		if (PropertyUtils.getStringListValue(object, "Source_Text") != null) {
			List<String> fileNameList = PropertyUtils.getStringListValue(object, "Source_Text");
			for (String fileName : fileNameList) {
				if (fileName.endsWith(".h")) {
					_additionalHeaders.get(_gtypesHeaderCode).add(fileName);
					return;
				}
			}
			return;
		}
		String id = PropertyUtils.getStringValue(object, "Source_Name");
		if (id == null)
			id = GenerationUtilsC.getGenerationCIdentifier(baToCUnparserAction, object, _identifierMappingWithContext);

		TypeHolder dataTypeHolder = null;

		try {
			dataTypeHolder = AadlBaUtils.getTypeHolder(object);
		} catch (DimensionException e) {
			// This is an internal error, to be logged and displayed
			String errMsg = RamsesException.formatRethrowMessage("cannot fetch the type of \'" + object + '\'', e);
			_LOGGER.error(errMsg, e);
			// ServiceProvider.SYS_ERR_REP.error(errMsg, true);
		}

		// define types the current data type depends on
		EList<PropertyExpression> referencedBaseType = PropertyUtils.findPropertyExpression(dataTypeHolder.getKlass(),
				DataModelProperties.BASE_TYPE);

		for (PropertyExpression baseTypeProperty : referencedBaseType) {
			if (baseTypeProperty instanceof ListValue) {
				ListValue lv = (ListValue) baseTypeProperty;

				for (PropertyExpression v : lv.getOwnedListElements()) {
					if (v instanceof ClassifierValue) {
						ClassifierValue cv = (ClassifierValue) v;
						if (_processedTypes.contains(cv.getClassifier().getQualifiedName()) == false
								&& cv.getClassifier() instanceof DataSubcomponentType) {
							getCTypeDeclarator(cv.getClassifier());
							_processedTypes.add(cv.getClassifier().getQualifiedName());
						}
					}
				}
			}
		}

		EList<PropertyExpression> numberRepresentation = PropertyUtils.findPropertyExpression(dataTypeHolder.getKlass(),
				DataModelProperties.NUMBER_REPRESENTATION);
		String numberRepresentationValue = "";

		for (PropertyExpression n : numberRepresentation) {
			if (n instanceof NamedValue) {
				NamedValue el = (NamedValue) n;
				numberRepresentationValue = ((EnumerationLiteral) el.getNamedValue()).getName().toLowerCase();
				break;
			}
		}

		EList<PropertyExpression> precisnioPEList = PropertyUtils.findPropertyExpression(dataTypeHolder.getKlass(),
				"IEEE754_Precision");
		String precision = "";

		for (PropertyExpression n : precisnioPEList) {
			if (n instanceof NamedValue) {
				NamedValue el = (NamedValue) n;
				precision = ((EnumerationLiteral) el.getNamedValue()).getName().toLowerCase();
				break;
			}
		}

		double size = DataSizeHelper.getOrComputeDataSize(dataTypeHolder.getKlass());

		switch (dataTypeHolder.getDataRep()) {
		// Simple types
		case BOOLEAN:
			_gtypesHeaderCode.addOutputNewline("typedef char " + id + ";");
			break;
		case CHARACTER: {
			_gtypesHeaderCode.addOutput("typedef ");
			_gtypesHeaderCode.addOutputNewline(numberRepresentationValue + " char " + id + ";");
			break;
		}
		case FIXED:
			break;
		case FLOAT:
			_gtypesHeaderCode.addOutput("typedef ");
			if (precision.equalsIgnoreCase("double"))
				_gtypesHeaderCode.addOutput("double ");
			else
				_gtypesHeaderCode.addOutput("float ");
			_gtypesHeaderCode.addOutputNewline(id + ";");

			break;
		case INTEGER: {
			_gtypesHeaderCode.addOutput("typedef ");
			String base_type = "int ";
			if (numberRepresentationValue.equalsIgnoreCase("unsigned")) {
				if (size == 1.0)
					base_type = "uint8_t ";
				else if (size == 2.0)
					base_type = "uint16_t ";
				else if (size == 4.0)
					base_type = "uint32_t ";
				else if (size == 8.0)
					base_type = "uint64_t ";
				else
					_gtypesHeaderCode.addOutput("unsigned ");
			} else {
				if (size == 1.0)
					base_type = "int8_t ";
				else if (size == 2.0)
					base_type = "int16_t ";
				else if (size == 4.0)
					base_type = "int32_t ";
				else if (size == 8.0)
					base_type = "int64_t ";
			}

			_gtypesHeaderCode.addOutputNewline(base_type + id + ";");
			break;
		}
		case STRING:
			_gtypesHeaderCode.addOutputNewline("typedef char * " + id + ";");
			break;
		// Complex types
		case ENUM: {
			StringBuilder enumDeclaration = new StringBuilder("typedef enum e_" + id + " {\n");
			List<String> stringifiedRepresentation = new ArrayList<String>();
			EList<PropertyExpression> dataRepresentation = PropertyUtils.findPropertyExpression(dataTypeHolder.getKlass(),
					DataModelProperties.REPRESENTATION);

			for (PropertyExpression representationProperty : dataRepresentation) {
				if (representationProperty instanceof ListValue) {
					ListValue lv = (ListValue) representationProperty;

					for (PropertyExpression v : lv.getOwnedListElements()) {
						if (v instanceof StringLiteral) {
							StringLiteral enumString = (StringLiteral) v;
							stringifiedRepresentation.add(enumString.getValue());
						}
					}
				}
			}

			EList<PropertyExpression> dataEnumerators = PropertyUtils.findPropertyExpression(dataTypeHolder.getKlass(),
					DataModelProperties.ENUMERATORS);

			for (PropertyExpression enumeratorProperty : dataEnumerators) {
				if (enumeratorProperty instanceof ListValue) {
					ListValue lv = (ListValue) enumeratorProperty;
					Iterator<PropertyExpression> it = lv.getOwnedListElements().iterator();

					while (it.hasNext()) {
						PropertyExpression v = it.next();

						if (v instanceof StringLiteral) {
							StringLiteral enumString = (StringLiteral) v;
							String rep = "";

							if (stringifiedRepresentation.isEmpty() == false) {
								rep = " = " + stringifiedRepresentation.get(lv.getOwnedListElements().indexOf(v));
							}

							if (it.hasNext()) {
								rep += ",";
							}
							enumDeclaration.append("\t" + // id +
									enumString.getValue() + rep + "\n");
						}
					}
				}
			}

			_gtypesHeaderCode.addOutput(enumDeclaration.toString());
			_gtypesHeaderCode.addOutputNewline("} " + id + ";");
			break;
		}
		case STRUCT: {

			StringBuilder structDefinition = new StringBuilder("typedef struct " + id + " {\n");
			EList<PropertyExpression> elementNames = PropertyUtils.findPropertyExpression(dataTypeHolder.getKlass(),
					DataModelProperties.ELEMENT_NAMES);
			if (elementNames.isEmpty() == false) {
				List<String> stringifiedElementNames = new ArrayList<String>();

				for (PropertyExpression elementNameProperty : elementNames) {
					if (elementNameProperty instanceof ListValue) {
						ListValue lv = (ListValue) elementNameProperty;

						for (PropertyExpression v : lv.getOwnedListElements()) {
							if (v instanceof StringLiteral) {
								StringLiteral eltName = (StringLiteral) v;
								stringifiedElementNames.add(eltName.getValue());
							}
						}
					}
				}

				EList<PropertyExpression> elementTypes = PropertyUtils.findPropertyExpression(dataTypeHolder.getKlass(),
						DataModelProperties.BASE_TYPE);

				for (PropertyExpression elementTypeProperty : elementTypes) {
					if (elementTypeProperty instanceof ListValue) {
						ListValue lv = (ListValue) elementTypeProperty;

						for (PropertyExpression v : lv.getOwnedListElements()) {
							if (v instanceof ClassifierValue) {
								ClassifierValue cv = (ClassifierValue) v;
								String type = PropertyUtils.getStringValue(cv.getClassifier(), "Source_Name");
								List<String> fileNameList = PropertyUtils.getStringListValue(cv.getClassifier(),
										"Source_Text");
								boolean headerReferenced = false;
								if (fileNameList != null) {
									for (String fileName : fileNameList) {
										if (fileName.endsWith(".h")) {
											headerReferenced = true;
											_additionalHeaders.get(_gtypesHeaderCode).add(fileName);
										}
									}
								}
								if (type == null || headerReferenced == false)
									type = GenerationUtilsC.getGenerationCIdentifier(baToCUnparserAction,
											cv.getClassifier(), _identifierMappingWithContext);
								int idx = lv.getOwnedListElements().indexOf(v);
								structDefinition.append("\t" + type + " " + stringifiedElementNames.get(idx) + ";\n");
							}
						}
					}
				}
			} else {
				_owner = object;
				if (object instanceof DataImplementation) {
					boolean isAbstract = isAbstractType((DataClassifier) object);
					if (isAbstract)
						break; // do not consider data with prototypes as acceptable struct definitions.
					for (DataSubcomponent ds : getAllDataSubcomponents((DataImplementation) object)) {
						DataSubcomponentType dst = ds.getDataSubcomponentType();
						Set<String> l = _additionalHeaders.get(_gtypesHeaderCode);

						String sourceName = GenerationUtilsC.resolveExistingCodeDependencies(dst, l);
						if (sourceName == null) {
							unparserSwitch.process(dst);
							if (dst instanceof DataPrototype) {
								Classifier cl = (Classifier) _owner;
								for (PrototypeBinding pb : cl.getOwnedPrototypeBindings()) {
									if (pb.getFormal().equals(dst)) {
										for (ComponentPrototypeActual pa : ((ComponentPrototypeBinding) pb)
												.getActuals()) {
											if (pa.getSubcomponentType() instanceof DataSubcomponentType) {
												DataSubcomponentType dstActual = (DataSubcomponentType) pa
														.getSubcomponentType();
												unparserSwitch.process(dstActual);
												sourceName = GenerationUtilsC.resolveExistingCodeDependencies(dstActual,
														l);
												if (sourceName == null) {
													sourceName = PropertyUtils.getStringValue(dstActual, "Source_Name");
													if (sourceName == null)
														sourceName = GenerationUtilsC.getGenerationCIdentifier(
																baToCUnparserAction, dstActual,
																_identifierMappingWithContext);
													break;
												}
											}
										}
									}
								}
							} else {
								sourceName = PropertyUtils.getStringValue(dst, "Source_Name");
								if (sourceName == null)
									sourceName = GenerationUtilsC.getGenerationCIdentifier(baToCUnparserAction, dst,
											_identifierMappingWithContext);
							}
						}
						if (sourceName != null)
							structDefinition.append("\t" + sourceName);
						structDefinition.append(" " + ds.getName() + ";\n");
					}
				}
			}
			_gtypesHeaderCode.addOutput(structDefinition.toString());
			_gtypesHeaderCode.addOutputNewline("} " + id + ";");
			break;
		}
		case UNION: {
			StringBuilder unionDeclaration = new StringBuilder("typedef union " + id + " {\n");
			EList<PropertyExpression> elementNames = PropertyUtils.findPropertyExpression(dataTypeHolder.getKlass(),
					DataModelProperties.ELEMENT_NAMES);
			List<String> stringifiedElementNames = new ArrayList<String>();

			for (PropertyExpression elementNameProperty : elementNames) {
				if (elementNameProperty instanceof ListValue) {
					ListValue lv = (ListValue) elementNameProperty;

					for (PropertyExpression v : lv.getOwnedListElements()) {
						if (v instanceof StringLiteral) {
							StringLiteral eltName = (StringLiteral) v;
							stringifiedElementNames.add(eltName.getValue());
						}
					}
				}
			}

			EList<PropertyExpression> elementTypes = PropertyUtils.findPropertyExpression(dataTypeHolder.getKlass(),
					DataModelProperties.BASE_TYPE);

			for (PropertyExpression elementTypeProperty : elementTypes) {
				if (elementTypeProperty instanceof ListValue) {
					ListValue lv = (ListValue) elementTypeProperty;

					for (PropertyExpression v : lv.getOwnedListElements()) {
						if (v instanceof ClassifierValue) {
							ClassifierValue cv = (ClassifierValue) v;

							String type = PropertyUtils.getStringValue(cv.getClassifier(), "Source_Name");
							if (type == null)
								type = GenerationUtilsC.getGenerationCIdentifier(baToCUnparserAction,
										cv.getClassifier(), _identifierMappingWithContext);
							unionDeclaration.append("\t" + type + " "
									+ stringifiedElementNames.get(lv.getOwnedListElements().indexOf(v)) + ";\n");
						}
					}
				}
			}
			_gtypesHeaderCode.addOutput(unionDeclaration.toString());
			_gtypesHeaderCode.addOutputNewline("}" + id + ";");
			break;
		}
		case ARRAY: {
			StringBuilder arrayDef = new StringBuilder("typedef ");
			EList<PropertyExpression> baseType = PropertyUtils.findPropertyExpression(dataTypeHolder.getKlass(),
					DataModelProperties.BASE_TYPE);
			boolean found = false;
			boolean isAbstract = false;
			for (int i = baseType.size() - 1; i >= 0; i--) {
				PropertyExpression baseTypeProperty = baseType.get(i);
				if (baseTypeProperty instanceof ListValue) {
					ListValue lv = (ListValue) baseTypeProperty;

					for (PropertyExpression v : lv.getOwnedListElements()) {
						if (v instanceof ClassifierValue) {
							ClassifierValue cv = (ClassifierValue) v;
							isAbstract = isAbstractType((DataClassifier) cv.getClassifier());
							if (isAbstract)
								break;
							if (false == _processedTypes.contains(cv.getClassifier().getQualifiedName())) {
								getCTypeDeclarator(cv.getClassifier());
								_processedTypes.add(cv.getClassifier().getQualifiedName());
							}
							String type = PropertyUtils.getStringValue(cv.getClassifier(), "Source_Name");
							if (type == null)
								arrayDef.append(GenerationUtilsC.getGenerationCIdentifier(baToCUnparserAction,
										cv.getClassifier(), _identifierMappingWithContext));
							else
								arrayDef.append(type);
							found = true;
							break;
						}
					}
					if (found)
						break;
				}
			}
			if (isAbstract)
				break;
			_gtypesHeaderCode.addOutput(arrayDef.toString());
			_gtypesHeaderCode.addOutput(" ");
			_gtypesHeaderCode.addOutput(id);
			EList<PropertyExpression> arrayDimensions = PropertyUtils.findPropertyExpression(dataTypeHolder.getKlass(),
					DataModelProperties.DIMENSION);

			if (arrayDimensions.isEmpty()) {
				_gtypesHeaderCode.addOutput("[]");
			} else {
				for (PropertyExpression dimensionProperty : arrayDimensions) {
					if (dimensionProperty instanceof ListValue) {
						ListValue lv = (ListValue) dimensionProperty;

						for (PropertyExpression v : lv.getOwnedListElements()) {
							if (v instanceof IntegerLiteral) {
								_gtypesHeaderCode.addOutput("[");
								IntegerLiteral dimension = (IntegerLiteral) v;
								_gtypesHeaderCode.addOutput(Long.toString(dimension.getValue()));
								_gtypesHeaderCode.addOutput("]");
							}
						}
					}
				}
			}

			_gtypesHeaderCode.addOutputNewline(";");
			break;
		}
		case UNKNOWN: {
			try {
				_gtypesHeaderCode.addOutput("typedef ");
				resolveExistingCodeDependencies(object, _gtypesHeaderCode, _gtypesHeaderCode);
				_gtypesHeaderCode.addOutput(" ");
				_gtypesHeaderCode.addOutput(id);
				_gtypesHeaderCode.addOutputNewline(";");
			} catch (GenerationException e) {
				String errMsg = RamsesException
						.formatRethrowMessage("cannot resolve dependencies for \'" + object + '\'', e);
				_LOGGER.warn(errMsg, e);
				// ServiceProvider.SYS_ERR_REP.warning(errMsg, true); ;
			}

			break;
		}
		default:
			break;
		}
	}

	boolean isAbstractType(DataClassifier dc) {
		if (dc instanceof DataImplementation) {
			DataImplementation di = (DataImplementation) dc;
			for (DataSubcomponent ds : getAllDataSubcomponents(di)) {
				DataSubcomponentType dst = ds.getDataSubcomponentType();
				if (dst instanceof DataPrototype) {
					boolean bounded = false;
					for (PrototypeBinding pb : dc.getOwnedPrototypeBindings()) {
						if (pb.getFormal().equals(dst)) {
							bounded = true;
							break;
						}
					}
					if (bounded == false) {
						return true;
					}
				}
			}
		}
		return false;
	}

	List<DataSubcomponent> getAllDataSubcomponents(ComponentImplementation ci) {
		final List<DataSubcomponent> result = new ArrayList<DataSubcomponent>();
		for (Subcomponent s : ci.getAllSubcomponents()) {
			if (s instanceof DataSubcomponent)
				result.add((DataSubcomponent) s);
		}

//    Comparator<? super DataSubcomponent> c = new Comparator<DataSubcomponent>() {
//    	// To fix if there is a cycle
//    	private boolean dependOn(String dependences, String name)
//    	{
//    		dependences = dependences.replace("{", "");// FIXME this is really dirty
//    		dependences = dependences.replace("}", "");
//    		dependences = dependences.replace("&", "");
//    		dependences = dependences.replace(" ", "");
//    		dependences = dependences.replace("=", "");
//    		String[] dependences_arr = dependences.split(",");
//
//    		for (String s : dependences_arr)
//    		{
//    			if (name.equals(s))
//    				return true;
//    			else
//    			{
//    				DataSubcomponent tmp = null;
//    				for (DataSubcomponent ds : result)
//    				{
//    					if (ds.getName().equals(s))
//    					{
//    						tmp = ds;
//    						break;
//    					}
//    				}
//    				if (tmp != null && dependOn(GeneratorUtils.getInitialValue(tmp, "c"), name))
//    					return true;
//    			}
//    		}
//    		return false;
//    	}
//
//		@Override
//		public int compare(DataSubcomponent o1, DataSubcomponent o2) {
//			String o1_init = GeneratorUtils.getInitialValue(o1, "c");
//			String o2_init = GeneratorUtils.getInitialValue(o2, "c");
//
//			if (o1_init == null || o1_init.isEmpty())
//				return -1;
//			else if (o2_init == null || o2_init.isEmpty())
//				return 1;
//
//			if (dependOn(o1_init, o2.getName()))
//				return 1;
//			else if (dependOn(o2_init, o1.getName()))
//				return -1;
//			else
//				return 0;
//		}
//	};
//
//    Collections.sort(result,c);

		return result;
	}

	List<String> takenId = new ArrayList<String>();

	private void genTaskInitCode(ThreadSubcomponent ts, UnparseText _mainCCode) {
		String th_name = ts.getName();
		_mainCCode.addOutputNewline(
				ts.getName() + "_config.name = \"" + GeneratorUtils.limitCharNb(th_name, 30, takenId) + "\";");
		_mainCCode.addOutputNewline("test_threads |= Create_Thread(" + "&" + ts.getName() + "_config" + ");");
		_mainCCode.addOutputNewline("test_threads |= Start_Thread(" + "&" + ts.getName() + "_config" + ");");
	}

	public void genMainGlobalVariables(ProcessSubcomponent process, UnparseText _mainCCode) {
		ProcessImplementation pi = (ProcessImplementation) process.getComponentImplementation();

		String tasksInModesInit = "";
		String prefix = "bitarray_";
		for (Mode m : pi.getOwnedModes()) {
			_mainCCode.addOutputNewline("char " + prefix + m.getName() + "[BITNSLOTS(TASKS_NB)];");
			tasksInModesInit += prefix + m.getName();
			if (pi.getOwnedModes().get(pi.getOwnedModes().size() - 1) != m)
				tasksInModesInit += ",";
		}
		String tasksInModeTransitionInProgressInit = "";
		if (pi.getOwnedModes().isEmpty()) {
			tasksInModesInit += prefix + "_m";
			_mainCCode.addOutputNewline("char " + prefix + "_m" + "[BITNSLOTS(TASKS_NB)];");
		}
		else
		{
			_mainCCode.addOutputNewline("char mode_transition_in_progress_m" + "[BITNSLOTS(TASKS_NB)];");
			tasksInModeTransitionInProgressInit = ", mode_transition_in_progress_m";
		}
		_mainCCode.addOutputNewline("mode_id_t current_mode,source_mode,target_mode;");
		_mainCCode.addOutputNewline("char * tasks_in_mode[MODES_NB] = {" + tasksInModesInit + tasksInModeTransitionInProgressInit+"};");
		_mainCCode.addOutputNewline("unsigned long reached_init_barrier=0;");

		EList<ThreadSubcomponent> subcomponents = pi.getOwnedThreadSubcomponents();
		for (ThreadSubcomponent ts : subcomponents) {
			genTaskGlobalVariables(ts, _mainCCode);
		}

	}

	public void genTaskGlobalVariables(ThreadSubcomponent ts, UnparseText _mainCCode) {

		_mainCCode.addOutputNewline("extern thread_config_t " + ts.getName() + "_config;");
		_mainCCode.addOutputNewline("extern port_list_t " + ts.getName() + "_globalQueue;");

		//String threadName = getProgrammingLanguageIdentifier(ts.getComponentImplementation());

	}

	public String getProgrammingLanguageIdentifier(NamedElement ne) {
		return GenerationUtilsC.getGenerationCIdentifier(ne.getContainingClassifier(), ne,
				_identifierMappingWithContext);
	}

	public void genModeInit(ProcessSubcomponent process, UnparseText _mainCCode) {
		ProcessImplementation pi = (ProcessImplementation) process.getComponentImplementation();

		Mode initialMode = null;
		for (Mode m : pi.getOwnedModes()) {
			if (m.isInitial()) {
				initialMode = m;
				break;
			}
		}
		_mainCCode.addOutputNewline("void mode_init() {");
		_mainCCode.incrementIndent();
		
		List<DataSubcomponent> processorLinkList = new ArrayList<DataSubcomponent>();
		
		for (DataSubcomponent d : pi.getOwnedDataSubcomponents()) {
			if (d.getDataSubcomponentType().getName().equals("ProcessorLinkType")) {
				processorLinkList.add(d);
			}
		}
		
		processorLinkList.sort(new NamedElementSort());
		
		for (DataSubcomponent d : processorLinkList)
		{
			_mainCCode.addOutputNewline("processor_port_init(&" + d.getName() + ");");
		}
		
		Set<String> l;
		if (_additionalHeaders.containsKey(_currentHeaderUnparser) == false) {
			l = new HashSet<String>();
			_additionalHeaders.put(_currentHeaderUnparser, l);
		} else {
			l = _additionalHeaders.get(_currentHeaderUnparser);
		}
		
		if (processorLinkList.size()>0) {
			
			l.add("aadl_ports_network.h");
		}
		
		if (initialMode != null) {
			_mainCCode.addOutputNewline("mode_id_t initial_mode = " + initialMode.getName() + ";");
			_mainCCode.addOutputNewline("source_mode = initial_mode;");
			_mainCCode.addOutputNewline("Set_System_Mode(&initial_mode, EMERGENCY_MODE_SWITCH);");
		} else {
			_mainCCode.addOutputNewline("current_mode = 0;");
			_mainCCode.addOutputNewline("Set_System_Mode(&current_mode, EMERGENCY_MODE_SWITCH);");
		}
	}

	protected void genMainImplEnd(ProcessSubcomponent process) {

	}

	@Override
	public void generateSourceCode(Element element, URI generatedFilePath, IProgressMonitor monitor) throws GenerationException {
		AadlToCSwitchProcess.process(element);
		saveGeneratedFilesContent(generatedFilePath);
		// Reset all AadlToCSwitchProcess private attributes !
		init();
	}

}
