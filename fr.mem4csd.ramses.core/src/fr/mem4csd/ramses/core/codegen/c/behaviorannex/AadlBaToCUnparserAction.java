/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen.c.behaviorannex ;

import org.osate.aadl2.AnnexSubclause ;
import org.osate.aadl2.DefaultAnnexSubclause;
import org.osate.ba.AadlBaUnParserAction ;
import org.osate.ba.aadlba.BehaviorAnnex;

import fr.mem4csd.ramses.core.codegen.c.behaviorannex.AadlBaToCUnparser;

// ** AADL RESTRICTED BA HAS TO BE SET AS A STANDALONE ECLIPSE PLUGIN PROJECT ** 

public class AadlBaToCUnparserAction extends AadlBaUnParserAction
{

  public static final String ANNEX_NAME = "c_behavior_specification" ;
  protected AadlBaToCUnparser _unparser = null ;

  public AadlBaToCUnparserAction()
  {
    _unparser = new AadlBaToCUnparser() ;
  }

  @Override
  public String unparseAnnexSubclause(AnnexSubclause subclause, String indent)
  {
	BehaviorAnnex ba = null;
	if(subclause instanceof BehaviorAnnex)
	  ba = (BehaviorAnnex) subclause;
	else
	{
	  DefaultAnnexSubclause das = (DefaultAnnexSubclause)subclause;
	  ba = (BehaviorAnnex) das.getParsedAnnexSubclause();
	}
	return _unparser.process(ba) ;
  }

  public AadlBaToCUnparser getUnparser()
  {
    return _unparser ;
  }
//
//  @Override
//  public String getRegistryName()
//  {
//    return ANNEX_NAME ;
//  }

}
