package fr.mem4csd.ramses.core.codegen;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.ProcessImplementation;
import org.osate.aadl2.ProcessSubcomponent;
import org.osate.aadl2.ProcessorSubcomponent;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.SubcomponentType;
import org.osate.aadl2.SystemImplementation;
import org.osate.aadl2.SystemSubcomponent;
import org.osate.aadl2.modelsupport.UnparseText;
import org.osate.aadl2.modelsupport.modeltraversal.AadlProcessingSwitch;
import org.osate.aadl2.util.Aadl2Switch;

import fr.mem4csd.ramses.core.codegen.AbstractMakefileUnparser.IncludeDirIterator;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;
import fr.mem4csd.ramses.core.workflowramses.Codegen;

public class AadlToDefaultMakefileUnparser extends AbstractAadlToCMakefileUnparser {
	
	protected UnparseText unparserContent ;
	private List<ProcessSubcomponent> bindedProcess ;
	private String processorName;

	private List<Integer> processAffinity = new ArrayList<Integer>();
	
	
	public AadlToDefaultMakefileUnparser() {
		super();
	}
		
	protected String getAdditionalPreprocessingOptions(ProcessSubcomponent process) {
		return "";
	}
	
	protected String getAdditionalLinkingOptions(ProcessSubcomponent process) {
		return "";
	}
	
	protected String getProcessMQTTOptions() {
		return "";
	}
	
	public class DefaultMakefileUnparser extends AadlProcessingSwitch
	{
		public DefaultMakefileUnparser() {
			container = getCodeGenWorkflowComponent();
		}
			
		protected void initSwitches()
		{
			aadl2Switch = new Aadl2Switch<String>()
			{

				@Override
				public String caseProcessSubcomponent(ProcessSubcomponent object)
				{
					
					unparserContent.addOutputNewline("TARGET = " + object.getName()) ;
					Subcomponent processor = (Subcomponent)
							AadlHelperImpl.getDeloymentProcessorSubcomponent(object);
					String sched_protocol = GeneratorUtils.getSchedulingProtocol(processor);
					
					if ( sched_protocol!=null && sched_protocol.equalsIgnoreCase("static") ) {
						Codegen container = getCodeGenWorkflowComponent();
						final URI coreRuntimeDirURI = container.getCoreRuntimeDirectoryURI();
						
						if ( coreRuntimeDirURI != null ) {
							sourceFilesURISet.add(coreRuntimeDirURI.appendSegment( "aadl_time_triggered_sched.c" ) );
						}
					}

					
					process(object.getComponentImplementation()) ;
					
					setTargetIncludeDirectories(object);
					sourceFilesURISet.addAll(getTargetSourceFiles(object));
					
					List<String> alreadyIncludedDirs = new ArrayList<String>();
					Iterator<URI> it = new IncludeDirIterator() ;
					unparserContent.addOutput("INCLUDES=");

					if(it.hasNext())
					{
						URI include ;
						while(it.hasNext())
						{
							include = it.next() ;
							if(include == null || include.isEmpty())
								continue;
							String filePath = toMakefileString( include );
							if(alreadyIncludedDirs.contains(filePath))
								continue;
							alreadyIncludedDirs.add(filePath);
							unparserContent.addOutput("-I\"" + filePath + "\" ") ;
						}
					}
					unparserContent.addOutput("-I./ ") ;
					
					for(URI includeDirURI: includeDirURISet)
					{
						String filePath = toMakefileString( includeDirURI );
						if(alreadyIncludedDirs.contains(filePath))
							continue;
						alreadyIncludedDirs.add(filePath);
						unparserContent.addOutput("-I\"" + filePath + "\" ") ;
					}

					unparserContent.addOutputNewline("");        
					unparserContent.addOutput("OPTS=" + getAdditionalPreprocessingOptions(object) + " $(ADD_OPTS)");
					if(GeneratorUtils.processUsesMQTT((ProcessImplementation) object.getComponentImplementation()))
						unparserContent.addOutput(getProcessMQTTOptions());
					unparserContent.addOutputNewline("");
					unparserContent.addOutputNewline("");
					
					List<String> alreadyAddedObjects = new ArrayList<String>();
					
					unparserContent.addOutput("OBJECTS= ");
					boolean first = true;
					for(URI st : sourceFilesURISet)
					{
						if(!"c".equals(st.fileExtension()))
							continue;
						
						URI stLastSegment = URI.createURI(st.lastSegment());
						if(first)
						{
							String filePath = toMakefileString( stLastSegment.trimFileExtension().appendFileExtension( "o" ) );
							unparserContent.addOutputNewline( filePath + "\\");
							first = false;
						}
						else
						{
							List<URI> sourceFilesURISetList = new ArrayList<URI>(sourceFilesURISet);
							
							
							String filePath = toMakefileString( stLastSegment.trimFileExtension().appendFileExtension( "o" ) );
							if(alreadyAddedObjects.contains(filePath))
								continue;
							alreadyAddedObjects.add(filePath);
							
							if(sourceFilesURISetList.indexOf(st)==sourceFilesURISetList.size()-1)
								unparserContent.addOutputNewline("\t"+ filePath );
							else
								unparserContent.addOutputNewline("\t"+ filePath + "\\");
						}
					}
					
					unparserContent.addOutput("\n") ;
					
					unparserContent.addOutputNewline("all: $(OBJECTS)") ;
					unparserContent.addOutputNewline("\t$(CC) $(OBJECTS) -o $(TARGET) "+getAdditionalLinkingOptions(object));
					unparserContent.addOutput("\n") ;
					
					List<String> alreadyCompiledFiles = new ArrayList<String>();
					
					for(URI s : sourceFilesURISet) {
						URI objectFilePrefix = URI.createURI(s.lastSegment());
						String filePath = toMakefileString( objectFilePrefix.trimFileExtension().appendFileExtension( "o" ) );
						if(alreadyCompiledFiles.contains(filePath))
							continue;
						alreadyCompiledFiles.add(filePath);
						unparserContent.addOutputNewline( filePath + ": "+ toMakefileString( s.trimFileExtension().appendFileExtension( "c" ) ) );
						unparserContent.addOutputNewline("\t$(CC) $(INCLUDES) $(OPTS) -c "+toMakefileString(s.trimFileExtension())+".c");
						unparserContent.addOutput("\n") ;
					}

					for(URI s : sourceFilesURISet) {
						if(!"c".equals(s.fileExtension()))
							continue;
						URI objectFilePrefix = URI.createURI(s.lastSegment());
						String filePath = toMakefileString( objectFilePrefix.trimFileExtension().appendFileExtension( "o" ) );
						if(alreadyCompiledFiles.contains(filePath))
							continue;
						alreadyCompiledFiles.add(filePath);
						unparserContent.addOutputNewline( filePath + ": "+ toMakefileString( s.trimFileExtension().appendFileExtension( "c" ) ) );
						unparserContent.addOutputNewline("\tgcc $(CPPFLAGS) -c "+toMakefileString(s.trimFileExtension())+".c");
						unparserContent.addOutput("\n") ;
					}

					alreadyCompiledFiles.clear();
					unparserContent.addOutputNewline("");
					
					unparserContent.addOutputNewline("clean:") ;
					unparserContent.addOutputNewline("\trm -rf $(OBJECTS) $(TARGET)");
					
					unparserContent.addOutputNewline("run:") ;
					unparserContent.addOutput("\tsudo ");
					if(!processAffinity.isEmpty())
					{
						unparserContent.addOutput("taskset -c ");
						int idx = 0;
						for(Integer i: processAffinity)
						{
							unparserContent.addOutput(i.toString());
							idx++;
							if(idx != processAffinity.size())
								unparserContent.addOutput(",");
						}
						unparserContent.addOutput(" ");
					}
					processAffinity.clear();
					unparserContent.addOutputNewline("./$(TARGET)");
					return DONE ;
				}

				@Override
				public String caseProcessImplementation(ProcessImplementation object)
				{
					sourceFilesURISet.addAll(getGeneratedSourceFiles());
					sourceFilesURISet.addAll(getListOfReferencedObjects(object));
					return DONE ;
				}

				@Override
				public String caseProcessorSubcomponent(ProcessorSubcomponent object)
				{
					generateProcessorMakefilePart(object);
					return null ;
				}

				@Override
				public String caseSystemSubcomponent(SystemSubcomponent object)
				{
					if(!AadlHelperImpl.isProcessor(object))
						return DONE ;
					generateProcessorMakefilePart(object);
					return DONE ;
				}

				private void generateProcessorMakefilePart(Subcomponent object) {
					bindedProcess =
							AadlHelperImpl.getBindedProcesses(object) ;
					processorName = object.getName();
					process(object.getSubcomponentType()) ;

				}

				@Override
				public String caseSubcomponentType(SubcomponentType object)
				{
					unparserContent
					.addOutputNewline("all: ") ;

					for(ProcessSubcomponent part:bindedProcess)
					{
						unparserContent.addOutputNewline("\t$(MAKE) -C " + part.getName() +
								" all") ;
					}

					unparserContent.addOutputNewline("benchmark: ") ;
					unparserContent.addOutputNewline("\t@ADD_OPTS=-DBENCHMARK $(MAKE) all") ;

					unparserContent.addOutputNewline("runtime-services-debug: ") ;
					unparserContent.addOutputNewline("\t@ADD_OPTS='-DDEBUG -DRUNTIME_DEBUG' $(MAKE) all") ;


					unparserContent.addOutputNewline("clean:") ;

					for(ProcessSubcomponent part:bindedProcess)
					{
						unparserContent.addOutputNewline("\t$(MAKE) -C " + part.getName() +
								" clean") ;
					}

					unparserContent.addOutputNewline("run:") ;

					for(ProcessSubcomponent part:bindedProcess)
					{
						unparserContent.addOutputNewline("\t$(MAKE) -C " + part.getName() +
								" run") ;
					}

					unparserContent.addOutputNewline("") ;
					unparserContent.addOutputNewline("test:") ;

					for(ProcessSubcomponent part:bindedProcess)
					{
						unparserContent.addOutputNewline("\t$(MAKE) -C " + part.getName() +
								" run > " + processorName +".exec_trace") ;
					}

					return DONE ;
				}
			} ;
		}

	}
	protected void generateMakefile(NamedElement ne, URI outputDir)
	{
		unparserContent = new UnparseText() ;
		DefaultMakefileUnparser unparser = new DefaultMakefileUnparser();
		unparser.process(ne) ;
		super.saveMakefile(unparserContent, outputDir) ;
	}

	@Override
	public void generateProcessorBuild(Subcomponent processor,
			URI processorDir,
			IProgressMonitor monitor)
					throws GenerationException
	{		
		generateMakefile((NamedElement) processor, processorDir) ;
	}

	@Override
	public void generateProcessBuild(ProcessSubcomponent process,
			URI processDir,
			IProgressMonitor monitor)
					throws GenerationException
	{
		Subcomponent processor = (Subcomponent) AadlHelperImpl.getDeloymentProcessorSubcomponent(process);
		
		if(processor instanceof ProcessorSubcomponent)
		{
			SystemImplementation si = (SystemImplementation) processor.eContainer();
			List<ProcessorSubcomponent> coreList = si.getOwnedProcessorSubcomponents();
			
			ProcessorSubcomponent core = (ProcessorSubcomponent) processor;
			Integer coreId = getCoreId(core);
			if(coreId!=null)
				processAffinity.add(coreId);
			else
				processAffinity.add(coreList.indexOf(core));
		}
		else if(processor instanceof SystemSubcomponent)
		{
			SystemSubcomponent multiCore =  (SystemSubcomponent) processor;
			SubcomponentType systemClass = (SubcomponentType) multiCore.getSubcomponentType();
			if(systemClass instanceof SystemImplementation)
			{
				SystemImplementation si = (SystemImplementation) systemClass;
				List<ProcessorSubcomponent> coreList = si.getOwnedProcessorSubcomponents();
				for(ProcessorSubcomponent core:coreList)
				{
					Integer coreId = getCoreId(core);
					if(coreId!=null)
						processAffinity.add(coreId);
					else
						processAffinity.add(coreList.indexOf(core));
				}
			}	
		}	
		generateMakefile((NamedElement) process, processDir) ;
	}

	private Integer getCoreId(ProcessorSubcomponent core) {
		// TODO: set affinity from a dedicated property
		return null;
	}

	@Override
	public boolean validateTargetPath(URI runtimePath)
	{
		return true ;
	}

	@Override
	public void generateSystemBuild(EList<Subcomponent> processorList, URI outputDir, IProgressMonitor monitor) {
		unparserContent = new UnparseText();
		unparserContent.addOutputNewline("") ;
		unparserContent.addOutputNewline("all:") ;

		for(Subcomponent aProcessorSubcomponent : 
			processorList)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " all") ;
		}

		unparserContent.addOutputNewline("") ;
		unparserContent.addOutputNewline("benchmark:") ;

		for(Subcomponent aProcessorSubcomponent : 
			processorList)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " benchmark") ;
		}

		unparserContent.addOutputNewline("") ;
		unparserContent.addOutputNewline("runtime-services-debug:") ;

		for(Subcomponent aProcessorSubcomponent : 
			processorList)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " runtime-services-debug") ;
		}

		unparserContent.addOutputNewline("") ;
		unparserContent.addOutputNewline("clean:") ;

		for(Subcomponent aProcessorSubcomponent : 
			processorList)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " clean") ;
		}

		unparserContent.addOutputNewline("") ;
		unparserContent.addOutputNewline("run:") ;

		for(Subcomponent aProcessorSubcomponent : processorList)
		{
			Boolean isLastProcessor = (processorList.indexOf(aProcessorSubcomponent) == 
					processorList.size()-1);
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " run" + (!isLastProcessor? " &":"") ) ;

		}

		unparserContent.addOutputNewline("") ;
		unparserContent.addOutputNewline("test:") ;
		for(Subcomponent aProcessorSubcomponent : 
			processorList)
		{
			Boolean isLastProcessor = (processorList.indexOf(aProcessorSubcomponent) == 
					processorList.size()-1);
			unparserContent.addOutputNewline("\t$(MAKE) -C " + 
					aProcessorSubcomponent.getName() + " test " + (!isLastProcessor? " &":"")) ;
		}
		super.saveMakefile(unparserContent, outputDir) ;
	}

	@Override
	public String getTargetShortDescription() {
		return "";
	}
	
	protected Set<URI> getTargetSourceFiles(ProcessSubcomponent process)
	{
		Set<URI> result = new HashSet<URI>();
		return result;
	}
	
	protected void setTargetIncludeDirectories(ProcessSubcomponent process)
	{
		return;
	}

}
