/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.URI;

import fr.mem4csd.ramses.core.workflowramses.Codegen;

public abstract class AbstractAadlToCMakefileUnparser extends AbstractMakefileUnparser {
	
	public static final String C_BASIC_SUB_PATH = "c_src";
	protected Codegen container;
	protected Set<URI> sourceFilesURISet = new LinkedHashSet<URI>();
	protected Set<URI> includeDirURISet = new LinkedHashSet<URI>();
	
	@Override
	public void initializeTargetBuilder() {
		container = getCodeGenWorkflowComponent();
	}
	
	protected void addSourceFilesToCompile(URI sourceFileURI)
	{
		sourceFilesURISet.add(sourceFileURI);
	}
	
	protected List<URI> getGeneratedSourceFiles()
	{
		List<URI> result = new ArrayList<URI>();
		result.add(URI.createURI("main.c"));
		result.add(URI.createURI("activity.c"));
		result.add(URI.createURI("subprograms.c"));
		result.add(URI.createURI("gtypes.c"));
		result.add(URI.createURI("globals.c"));
		return result;
	}
	
	protected boolean isSourceOrObjectFile( final URI uri ) {
		final String ext = uri.fileExtension();
		
		return "c".equals( ext ) || "o".equals( ext );
	}
	
	protected URI getCygwinPath(URI path)	{
		URI uri = URI.createFileURI( "/cygdrive" );
		
		final int indexStart;
		
		if ( path.isPlatform() ) {
			indexStart = 1;
		}
		else {
			indexStart = path.segment( 0 ).charAt( 1 ) == ':' ? 1 : 0;
		}
		
		for ( int index = indexStart; index < path.segmentCount(); index++ ) {
			uri = uri.appendSegment( path.segment( index ) );
		}

		return uri;
	}
}