/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen;

import java.util.ArrayList ;
import java.util.List ;

import org.apache.log4j.Logger ;
import org.eclipse.core.runtime.IProgressMonitor ;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject ;
import org.eclipse.xtext.EcoreUtil2;
import org.osate.aadl2.AadlPackage ;
import org.osate.aadl2.Classifier ;
import org.osate.aadl2.ProcessSubcomponent ;
import org.osate.aadl2.Property;
import org.osate.aadl2.PublicPackageSection ;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.SystemImplementation ;
import org.osate.aadl2.SystemSubcomponent ;
import org.osate.aadl2.instance.ComponentInstance ;
import org.osate.aadl2.instance.SystemInstance ;
import org.osate.aadl2.modelsupport.UnparseText;
import org.osate.xtext.aadl2.properties.util.GetProperties;
import org.osate.xtext.aadl2.properties.util.PropertyUtils;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.codegen.utils.FileUtils;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;
import fr.mem4csd.ramses.core.workflowramses.AadlToSourceCodeGenerator;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator;
import fr.mem4csd.ramses.core.workflowramses.Codegen;
import fr.mem4csd.ramses.core.workflowramses.TargetProperties;
import fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair;

public class AadlTargetSpecificCodeGenerator
{
	public static final String GENERATED_DIR_NAME = "generated-code" ; 
	public static final String KERNEL_DIR_NAME = "kernel" ;

	protected AadlToSourceCodeGenerator _genericUnparser ;
	protected AadlToTargetConfigurationGenerator _targetUnparser ;
	protected AadlToTargetBuildGenerator _targetBuilderGen ;

	private static Logger _LOGGER = Logger.getLogger(AadlTargetSpecificCodeGenerator.class) ;

	public AadlTargetSpecificCodeGenerator(AadlToSourceCodeGenerator genericUnparser,
			AadlToTargetConfigurationGenerator targetUnparser,
			AadlToTargetBuildGenerator targetBuilderGen)
	{
		_genericUnparser = genericUnparser ;
		_targetUnparser = targetUnparser ;
		_targetBuilderGen = targetBuilderGen ;
		_targetBuilderGen.initializeTargetBuilder();
	}

	public void generate(IProgressMonitor monitor )
	throws GenerationException {
		EList<Subcomponent> processors = new BasicEList<Subcomponent>();
		EList<TransformationResourcesPair> transformationResourcesPairList = _targetBuilderGen.getCodeGenWorkflowComponent().getTransformationResourcesPairList(); 
		for(TransformationResourcesPair r: transformationResourcesPairList)
		{
			SystemImplementation si;
			EObject root = r.getOutputModelRoot();
			if(root instanceof SystemInstance)
			{
				si = (SystemImplementation) ((SystemInstance) root).getComponentImplementation();
			}
			else
			{
				List<SystemImplementation> siList = EcoreUtil2.getAllContentsOfType(root, SystemImplementation.class);
				si = siList.get(0);
			}
			
			List<Subcomponent> processorList = getAllProcessors(si);
			
			for(Subcomponent ps: processorList)
			{
				String specifiedTargetInModel = "";
				Property specifiedTargetProperty = GetProperties.lookupPropertyDefinition(ps, "RAMSES_Properties", "Target");
				if(specifiedTargetProperty == null) throw new GenerationException("RAMSES_Properties::Target is null");
;				specifiedTargetInModel = PropertyUtils.getStringValue(ps, specifiedTargetProperty);
				if(false==_targetBuilderGen.getTargetName().equalsIgnoreCase(specifiedTargetInModel))
					continue;
				processors.add(ps);
			}
		}
		
//		List<ArchTraceSpec> tracesList = new ArrayList<ArchTraceSpec>();
		EList<SystemImplementation> systemImplementationList = new BasicEList<SystemImplementation>();
		for(TransformationResourcesPair r: transformationResourcesPairList)
		{
//			tracesList.add((ArchTraceSpec) r.getModelTransformationTrace());

			EObject root = r.getOutputModelRoot();

			if ( root instanceof SystemInstance ) {
				SystemInstance systemInstance = (SystemInstance) root;

				if ( systemInstance.eContainer() == null ) {
					systemImplementationList.addAll(this.getListOfSystemImplementation(systemInstance));
				}
			}
			else if(root instanceof AadlPackage)
			{
				AadlPackage aadlPackage = (AadlPackage) root;
				PublicPackageSection pps = aadlPackage.getOwnedPublicSection();
				for(Classifier c : pps.getOwnedClassifiers() )
					if(c instanceof SystemImplementation && isRootSystemImplementation((SystemImplementation)c, pps))
						systemImplementationList.add((SystemImplementation)c);
			}
			else {
				String errMsg = "Try to generate from a resources that is into an AADL model" ;
				_LOGGER.fatal(errMsg) ;

				throw new GenerationException(errMsg);
			}
		}
		
		
		Codegen container = this._targetBuilderGen.getCodeGenWorkflowComponent();

		URI generatedFileDir = container.getOutputDirectoryURI().appendSegment( GENERATED_DIR_NAME );
		FileUtils.makeDir( generatedFileDir );
		
		addMakefileForSubDirectories(generatedFileDir);
		
		generatedFileDir = generatedFileDir.appendSegment(_targetBuilderGen.getTargetName().toLowerCase());
		FileUtils.makeDir( generatedFileDir );
		
		monitor.subTask("Code generation ..."); 

		if ( systemImplementationList.isEmpty() ) {
			String errMsg = "No root system in output model used for code generation";
			_LOGGER.fatal(errMsg);
			throw new GenerationException(errMsg);
		}

		if( monitor.isCanceled() ) {
			String msg = "generation has been canceled after transformation" ;
			_LOGGER.trace(msg);
			throw new OperationCanceledException(msg) ;
		}

		// XXX Have AadlToSourceCodeGenerator to unparse the SystemImplementation
		// object ?
		TargetProperties tarProp = null;

		if( _targetUnparser != null ) {
			tarProp = _targetUnparser.getSystemTargetConfiguration(systemImplementationList, monitor);
			_targetUnparser.getCodeGenWorkflowComponent().setTargetProperties(tarProp);
		}
		int idx = 0;
		for ( SystemImplementation sys: systemImplementationList ) {

			TransformationResourcesPair r = transformationResourcesPairList.get(idx);
			
			List<Subcomponent> psList = getAllProcessors(sys);

			for(Subcomponent ps : psList)
			{
				String specifiedTargetInModel = "";
				Property isProcessor = GetProperties.lookupPropertyDefinition(ps, "RAMSES_Properties", "Target");
				specifiedTargetInModel = PropertyUtils.getStringValue(ps, isProcessor);
				if(false==_targetBuilderGen.getTargetName().equalsIgnoreCase(specifiedTargetInModel))
					continue;
					
				// create directory with the processor subcomponent name
				final URI processorFileDir = generatedFileDir.appendSegment( ps.getName() );
				FileUtils.makeDir( processorFileDir );

				final URI kernelUri = processorFileDir.appendSegment( KERNEL_DIR_NAME );
				FileUtils.makeDir( kernelUri );
				//kernelFileDir.mkdir();
				if(_targetUnparser != null)
				{
					_targetUnparser.setCurrentModelTransformationTrace((ArchTraceSpec) r.getModelTransformationTrace());
					_targetUnparser.generateProcessorTargetConfiguration(ps, kernelUri, monitor);
				}
				List<ProcessSubcomponent> ownedProcess = 
						AadlHelperImpl.getBindedProcesses(ps) ;

				for(ProcessSubcomponent process : ownedProcess)
				{
					URI processDirectory = processorFileDir.appendSegment( process.getName() );
					FileUtils.makeDir( processDirectory );
					//File processDirectory = new File(generationTargetDirectory) ;
					//processDirectory.mkdir() ;

					_genericUnparser.generateSourceCode(process, processDirectory, monitor) ;
					if(_targetUnparser!=null)
					{
						_targetUnparser.setCurrentModelTransformationTrace((ArchTraceSpec) r.getModelTransformationTrace());
						_targetUnparser.generateProcessTargetConfiguration(process, processDirectory, monitor);
					}
					if(_targetBuilderGen!= null)
						_targetBuilderGen.generateProcessBuild(process, processDirectory, monitor) ;
				}

				if(_targetBuilderGen != null)
					_targetBuilderGen.generateProcessorBuild(ps, processorFileDir,
							monitor);

			}
			idx++;
		}
		// This line is at the end because it will launch the build of the generated code;
		// Thus it is better is the code has been generated...
		//File makeFileDir = new File( conf.getOutputDir() + GENERATED_DIR_NAME );
		if(_targetBuilderGen!= null)
		{
			_targetBuilderGen.generateSystemBuild(processors,generatedFileDir,monitor);
		}

	}

	private List<Subcomponent> getAllProcessors(SystemImplementation sys) {
		List<Subcomponent> psList = new ArrayList<Subcomponent>();

		for(SystemSubcomponent ss: sys.getOwnedSystemSubcomponents())
		{
			boolean isProcessorSystem = false;
			Property isProcessor = GetProperties.lookupPropertyDefinition(ss, "RAMSES_Properties", "is_processor");
			isProcessorSystem = PropertyUtils.getBooleanValue(ss, isProcessor);
			if(isProcessorSystem)
				psList.add(ss);
		}

		psList.addAll(sys.getOwnedProcessorSubcomponents());
		return psList;
	}

	private void addMakefileForSubDirectories(URI generatedFileDir) {
		
		UnparseText unparserContent = new UnparseText();
		unparserContent.addOutputNewline("TOPTARGETS := all clean run");
		unparserContent.addOutputNewline("");
		unparserContent.addOutputNewline("SUBDIRS := $(wildcard */.)");
		unparserContent.addOutputNewline("");
		
		unparserContent.addOutputNewline("$(TOPTARGETS): $(SUBDIRS)");
		unparserContent.addOutputNewline("$(SUBDIRS):");
		unparserContent.addOutputNewline("\t$(MAKE) -C $@ $(MAKECMDGOALS)");
		unparserContent.addOutputNewline("");
		unparserContent.addOutputNewline(".PHONY: $(TOPTARGETS) $(SUBDIRS)");
		unparserContent.addOutputNewline("");
		
		AbstractMakefileUnparser makefileUnparser = (AbstractMakefileUnparser) _targetBuilderGen;
		makefileUnparser.saveMakefile(unparserContent, generatedFileDir);
	}

	private boolean isRootSystemImplementation(SystemImplementation c,
			PublicPackageSection pps)
	{
		for(Classifier c2 : pps.getOwnedClassifiers() )
		{
			if(c2 instanceof SystemImplementation)
			{
				SystemImplementation si = (SystemImplementation) c2;
				for(SystemSubcomponent s: si.getOwnedSystemSubcomponents())
					if(s.getSubcomponentType().equals(c))
						return false;
			}
		}
		return true ;
	}

	private List<SystemImplementation> getListOfSystemImplementation(SystemInstance systemInstance)
	{

		List<SystemImplementation> systemInstanceList =
				new ArrayList<SystemImplementation>() ;
		if(systemInstance.getComponentImplementation() != null)
			systemInstanceList.add((SystemImplementation) systemInstance.
					getComponentImplementation()) ;
		for(ComponentInstance ci : systemInstance.getComponentInstances())
		{
			if(ci instanceof SystemInstance)
			{
				systemInstanceList
				.addAll(getListOfSystemImplementation((SystemInstance) ci)) ;
			}
		}
		return systemInstanceList ;
	}

}