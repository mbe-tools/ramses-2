/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen;

import java.util.LinkedHashSet;
import java.util.Set;

import fr.mem4csd.ramses.core.codegen.GenerationInfos.GlobalQueueInfo;
import fr.mem4csd.ramses.core.codegen.GenerationInfos.QueueInfo;
import fr.mem4csd.ramses.core.codegen.GenerationInfos.SampleInfo;


public class ProcessProperties
{
  public ProcessProperties(String prefix)
  {
	this.prefix=prefix;
  }
  
  public int tasksNb;
  
  public String prefix = "";
  
  public boolean hasQueue = false ;
  
  public boolean hasSample = false ;
  
  public boolean hasVirtual = false ;
  
  public Set<GlobalQueueInfo> globalQueueInfo = new LinkedHashSet<GlobalQueueInfo>() ;
  
  public Set<QueueInfo> queueInfo = new LinkedHashSet<QueueInfo>();
  
  public Set<SampleInfo> sampleInfo = new LinkedHashSet<SampleInfo>();

  public boolean hasWaitMode;
  
  public Set<String> waitModeNames = new LinkedHashSet<String>();

  public boolean hasWaitMessage;
  
  public Set<String> waitMessageNames = new LinkedHashSet<String>();
  
  public boolean hasLock;
  
  public Set<String> lockNames = new LinkedHashSet<String>();


  public Set<String> virtualNames = new LinkedHashSet<String>();

}