/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen;

import java.util.ArrayList ;
import java.util.HashMap ;
import java.util.LinkedHashSet ;
import java.util.List ;
import java.util.Map ;
import java.util.Set ;

import org.eclipse.xtext.EcoreUtil2 ;
import org.osate.aadl2.ComponentCategory ;
import org.osate.aadl2.DirectionType ;
import org.osate.aadl2.ListValue ;
import org.osate.aadl2.ModalPropertyValue ;
import org.osate.aadl2.PropertyAssociation ;
import org.osate.aadl2.PropertyExpression ;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.instance.ComponentInstance ;
import org.osate.aadl2.instance.ConnectionInstance ;
import org.osate.aadl2.instance.ConnectionReference ;
import org.osate.aadl2.instance.FeatureCategory ;
import org.osate.aadl2.instance.FeatureInstance ;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.InstanceReferenceValue ;
import org.osate.aadl2.instance.SystemInstance ;
import org.osate.aadl2.instance.impl.ComponentInstanceImpl;
import org.osate.aadl2.instance.impl.FeatureInstanceImpl;
import org.osate.aadl2.instance.impl.SystemInstanceImpl;
import org.osate.utils.internal.PropertyUtils;
import org.osate.xtext.aadl2.properties.util.AadlProject;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.arch_trace.util.ArchTraceSourceRetreival;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;
import fr.mem4csd.ramses.core.workflowramses.impl.TargetPropertiesImpl;

public class RoutingProperties extends TargetPropertiesImpl {
	
	public RoutingProperties() {
		super();
	}

	public void setTraces(ArchTraceSpec traces) {
		this.archTraces = traces;
	}
	
	protected ArchTraceSpec archTraces;
  
  public Set<FeatureInstance> globalPort = new LinkedHashSet<FeatureInstance>();

	public Set<ComponentInstance> processes = new LinkedHashSet<ComponentInstance>();

//	public Set<ComponentInstance> processors = new LinkedHashSet<ComponentInstance>();
	
	public Set<ComponentInstance> buses = new LinkedHashSet<ComponentInstance>();
	
	public Map<FeatureInstance, ComponentInstance> processorPort = 
	    new HashMap<FeatureInstance, ComponentInstance>();

	public Map<ComponentInstance, Set<ComponentInstance>> processPerProcessor =
	    new HashMap<ComponentInstance, Set<ComponentInstance>>();

	public Map<ComponentInstance, Set<VirtualPort>> virtualPortPerProcessor = 
	    new HashMap<ComponentInstance, Set<VirtualPort>>();

	public Set<VirtualPort> globalVirtualPort = new LinkedHashSet<VirtualPort>();

	public Map<VirtualPort, ComponentInstance> processorVirtualPort = 
      new HashMap<VirtualPort, ComponentInstance>();
	
	public Map<FeatureInstance, VirtualPort> virtualPortPerPort = 
		      new HashMap<FeatureInstance, VirtualPort>();
	
	public Map<VirtualPort, List<FeatureInstance>> portsPerVirtualPort = 
			new HashMap<VirtualPort, List<FeatureInstance>>();

//    public Map<FeatureInstance, ComponentInstance> processVirtualPort =
//      new HashMap<FeatureInstance, ComponentInstance>();
	
	public void setRoutingProperties(SystemInstance system)
	{
		
		for(ComponentInstance subComponent: system.getComponentInstances())
		{
			processComponentInstanceInTarget(subComponent);
		}
		
		ArchTraceSpec traces = ArchTraceSourceRetreival.getTracesFromNamedElement(system.getComponentImplementation());
		if(traces==null)
			return;
		SystemInstance si = (SystemInstance) ArchTraceSourceRetreival.
				findNamedElementByClassInTransformationTraces(traces, system.getComponentImplementation(), SystemInstanceImpl.class);
		for(ComponentInstance subComponent: si.getComponentInstances())
		{
			processComponentInstanceInSource(subComponent);
		}
	}

	private void processComponentInstanceInSource(ComponentInstance component)
	{

		if(component.getCategory().equals(ComponentCategory.BUS) || component.getCategory().equals(ComponentCategory.VIRTUAL_BUS))
		{
			buses.add(component);
		}
		else
		{
			for(ComponentInstance subComponent: component.getComponentInstances())
			{
				processComponentInstanceInSource(subComponent);
			}
		}
	}
	
	private void processComponentInstanceInTarget(ComponentInstance component)
	{

		if(component.getCategory().equals(ComponentCategory.PROCESS))
		{
			processes.add(component);
			getPartitionProperties(component);
		}
		else
		{
			for(ComponentInstance subComponent: component.getComponentInstances())
			{
				processComponentInstanceInTarget(subComponent);
			}
		}
	}

	private void getPartitionProperties(ComponentInstance process)
	{
		List<FeatureInstance> processPorts = getProcessPorts(process);
		ComponentInstance processor = getProcessorBinding(process);
		
		for(FeatureInstance f:process.getAllFeatureInstances())
		{
		  if(RoutingProperties.needsRoutage(f))
		  {		  
		    processPorts.add(f);
		    globalPort.add(f);
		    processorPort.put(f, processor);
		  }
		}
		
		List<ComponentInstance> subcomponents = 
		    EcoreUtil2.getAllContentsOfType(process, ComponentInstance.class);
		
		for(ComponentInstance c: subcomponents)
		  if(c.getCategory().equals(ComponentCategory.DATA))
		  {
			Subcomponent sub = c.getSubcomponent();
			ArchTraceSpec traces = ArchTraceSourceRetreival.getTracesFromNamedElement(sub);
			if(traces==null)
				continue;
			FeatureInstance f = (FeatureInstance) ArchTraceSourceRetreival.
					findNamedElementByClassInTransformationTraces(traces, sub, FeatureInstanceImpl.class);

			if(f!=null && RoutingProperties.needsRoutage(f))
			{
				registerVirtualPort(f);
			}
			else if(f!=null && f.getCategory().equals(FeatureCategory.BUS_ACCESS))
			{
				List<ConnectionInstance> busConnections = new ArrayList<ConnectionInstance>();
				busConnections.addAll(f.getDstConnectionInstances());
				busConnections.addAll(f.getSrcConnectionInstances());
				for(ConnectionInstance cnxInst: busConnections)
		    	{
					ComponentInstance bus = null;
					if(cnxInst.getSource() instanceof ComponentInstance)
						bus = (ComponentInstance) cnxInst.getSource();
					else if(cnxInst.getDestination() instanceof ComponentInstance)
						bus = (ComponentInstance) cnxInst.getDestination();
					if(bus == null)
						continue;
					ComponentInstance ci = (ComponentInstance) ArchTraceSourceRetreival.
							findNamedElementByClassInTransformationTraces(traces, process.getSubcomponent(), ComponentInstanceImpl.class);

					for(FeatureInstance portInstance: ci.getAllFeatureInstances())
					{
//						if(!portUsesBus(portInstance, bus))
//							continue;
						registerVirtualPort(portInstance);
					}
		    	}
			}

//		??????
//		for(FeatureInstance busAccess: processor.getAllFeatureInstances())
//	    {
//	    	if(busAccess.getCategory().equals(FeatureCategory.BUS_ACCESS))
//	    	{
	    		
//	    		???
//	    		processVirtualPorts.add(busAccess);
	    		
//	    		globalVirtualPort.add(busAccess);
	    		
//	    		???
//	    		processVirtualPort.put(busAccess, process);
//		        processorVirtualPort.put(busAccess, getProcessorBinding(process));
		        
//	    		???
//		        for(ConnectionInstance cnxInst: busAccess.getDstConnectionInstances())
//	    		{
//	    			ComponentInstance bus = (ComponentInstance) cnxInst.getSource();
//	    			for(ConnectionInstance busCnxInst: bus.getSrcConnectionInstances())
//	        		{
//	    				if(busCnxInst.getDestination() instanceof FeatureInstance)
//	    				{
//	    					FeatureInstance otherBusAccess = (FeatureInstance) busCnxInst.getDestination() ;
//	    					if(!globalVirtualPort.contains(otherBusAccess))
//	    					{
//	    						ComponentInstance otherProcessor = (ComponentInstance)otherBusAccess.eContainer();
//	    						if(!isProcessor(otherProcessor))
//	    							continue;
//	    						globalVirtualPort.add(otherBusAccess);
////	    						processVirtualPort.put(otherBusAccess, process);
//	    				        processorVirtualPort.put(otherBusAccess, otherProcessor);
//	    					}
//	    				}
//	        		}
//	    			for(ConnectionInstance busCnxInst: bus.getSrcConnectionInstances())
//	        		{
//	    				if(busCnxInst.getDestination() instanceof FeatureInstance)
//	    				{
//	    					FeatureInstance otherBusAccess = (FeatureInstance) busCnxInst.getDestination() ;
//	    					if(!globalVirtualPort.contains(otherBusAccess))
//	    					{
//	    						ComponentInstance otherProcessor = (ComponentInstance)otherBusAccess.eContainer();
//	    						if(!isProcessor(otherProcessor))
//	    							continue;
//
//	    						globalVirtualPort.add(otherBusAccess);
////	    						processVirtualPort.put(otherBusAccess, process);
//	    				        processorVirtualPort.put(otherBusAccess, otherProcessor);
//	    					}
//	    				}
//	        		}
//	    		}
		        
//	    		???
//	    		for(ConnectionInstance cnxInst: busAccess.getSrcConnectionInstances())
//	    		{
//	    			ComponentInstance bus = (ComponentInstance) cnxInst.getDestination();
//	    			for(ConnectionInstance busCnxInst: bus.getSrcConnectionInstances())
//	        		{
//	    				if(busCnxInst.getDestination() instanceof FeatureInstance)
//	    				{
//	    					FeatureInstance otherBusAccess = (FeatureInstance) busCnxInst.getDestination() ;
//	    					if(!globalVirtualPort.contains(otherBusAccess))
//	    					{
//	    						ComponentInstance otherProcessor = (ComponentInstance)otherBusAccess.eContainer();
//	    						if(!isProcessor(otherProcessor))
//	    							continue;
//	    						
//	    						globalVirtualPort.add(otherBusAccess);
////	    						processVirtualPort.put(otherBusAccess, process);
//	    				        processorVirtualPort.put(otherBusAccess, otherProcessor);
//	    					}
//	    				}
//	        		}
//	    			for(ConnectionInstance busCnxInst: bus.getDstConnectionInstances())
//	        		{
//	    				if(busCnxInst.getSource() instanceof FeatureInstance)
//	    				{
//	    					FeatureInstance otherBusAccess = (FeatureInstance) busCnxInst.getSource() ;
//	    					if(!globalVirtualPort.contains(otherBusAccess))
//	    					{
//	    						ComponentInstance otherProcessor = (ComponentInstance)otherBusAccess.eContainer();
//	    						if(!isProcessor(otherProcessor))
//	    							continue;
//	    						
//	    						globalVirtualPort.add(otherBusAccess);
////	    						processVirtualPort.put(otherBusAccess, process);
//	    				        processorVirtualPort.put(otherBusAccess, otherProcessor);
//	    					}
//	    				}
//	        		}
//	    		}
//	    	}
	    }
	}

//	private boolean portUsesBus(FeatureInstance portInstance, ComponentInstance bus) {
//		if(!isPort(portInstance))
//			return false;
//		List<ConnectionInstance> allCnxInstances = new ArrayList<ConnectionInstance>();
//		allCnxInstances.addAll(portInstance.getSrcConnectionInstances());
//		allCnxInstances.addAll(portInstance.getDstConnectionInstances());
//		for(ConnectionInstance cnxInst: allCnxInstances)
//			if(getActualConnectionBinding(cnxInst).equals(bus))
//				return true;
//		return false;
//	}

	private void registerVirtualPort(FeatureInstance f,
			ConnectionInstance cnxInst,
			ComponentInstance bus)
	{
		
		if(false == bus.getCategory().equals(ComponentCategory.VIRTUAL_BUS)
				&& false == bus.getCategory().equals(ComponentCategory.BUS))
			return;
		
		List<ConnectionInstance> busCnxInstList = new ArrayList<ConnectionInstance>();
		busCnxInstList.addAll(bus.getSrcConnectionInstances());
		busCnxInstList.addAll(bus.getDstConnectionInstances());
		
		if(!busCnxInstList.isEmpty()) {
			ComponentInstance process = f.getComponentInstance();
			ComponentInstance processor = getProcessorBinding(process);
			for(ConnectionInstance busCnxInst: busCnxInstList)
			{
				FeatureInstance busAccess = null;
				ComponentInstance processProcessor = getProcessorBinding(f.getComponentInstance());
				if(busCnxInst.getDestination() instanceof FeatureInstance)
				{
					busAccess = (FeatureInstance) busCnxInst.getDestination() ;
					if(getBusAccessProcessor(busAccess)==null || 
							!getBusAccessProcessor(busAccess).equals(processProcessor))
						continue;

				}
				else if(busAccess == null && busCnxInst.getSource() instanceof FeatureInstance)
				{
					busAccess = (FeatureInstance) busCnxInst.getSource() ;
					if(getBusAccessProcessor(busAccess)==null || 
							!getBusAccessProcessor(busAccess).equals(processProcessor))
						continue;

				}
				// EB: retreive bus access by name; did not find a better way
				for(FeatureInstance busAccessInTargetModel: processor.getAllFeatureInstances())
				{
					if(busAccessInTargetModel.getName().equalsIgnoreCase(busAccess.getName()))
					{
						ComponentInstance destinationProcess = null;
						if(AadlHelperImpl.isPort(f) && AadlHelperImpl.isOutputFeature(f))
						{
							destinationProcess =  getProcess(cnxInst.getDestination());
							if(getProcessorBinding(process).equals(getProcessorBinding(destinationProcess)))
								continue;
						}
						if(AadlHelperImpl.isPort(f) && AadlHelperImpl.isInputFeature(f))
						{
							ComponentInstance srcProcess =  getProcess(cnxInst.getSource());
							if(getProcessorBinding(process).equals(getProcessorBinding(srcProcess)))
								continue;
						}
						VirtualPort lvp = new VirtualPort(busAccess, process, destinationProcess);
						addVirtualPortToProcessor(lvp, processor);
						globalVirtualPort.add(lvp);
						processorVirtualPort.put(lvp, getProcessorBinding(process));
						virtualPortPerPort.put(f, lvp);
						if(portsPerVirtualPort.get(lvp)==null)
						{
							List<FeatureInstance> featList = new ArrayList<FeatureInstance>();
							portsPerVirtualPort.put(lvp,featList);
						}
						portsPerVirtualPort.get(lvp).add(f);
						break;

					}
				}
			}
		} else {
			List<ComponentInstance> network = getDeepestActualConnectionBinding(bus);
			if(network.size()>2)
			{
				registerVirtualPort(f, cnxInst, network.get(1));
				registerVirtualPort(f, cnxInst, network.get(network.size()-2));
			}
		}
	}
	
	private void registerVirtualPort(FeatureInstance f) {
		
		virtualPortPerPort.put(f, null);
		
		// get bus
		Set<ConnectionInstance> fConnections = getConnectionInstances(f); 
		for(ConnectionInstance cnxInst : fConnections)
		{
			ComponentInstance bus = getActualConnectionBinding(cnxInst);
			if(bus == null)
				continue; // cnxInst should be a local connection in this case
			registerVirtualPort(f, cnxInst, bus);
			
			//		      globalVirtualPort.add(f);
			FeatureInstance dest = (FeatureInstance) cnxInst.getDestination();
			if(!dest.getContainingComponentInstance().getCategory().equals(ComponentCategory.PROCESS))
			{
				Set<FeatureInstance> destSet = getConnectedProcessPort(dest);
				for(FeatureInstance processDest: destSet)
				{
					if(!virtualPortPerPort.containsKey(processDest))
						registerVirtualPort((FeatureInstance) processDest);
				}
			} else if(!virtualPortPerPort.containsKey(dest))
				registerVirtualPort((FeatureInstance) dest);

		}
		
	}

	public static ComponentInstance getProcess(InstanceObject iObject) {
		if(iObject instanceof ComponentInstance)
		{
			ComponentInstance res = (ComponentInstance) iObject;
			if(res.getCategory()==ComponentCategory.PROCESS)
				return res;
		}
		ComponentInstance inst = iObject.getContainingComponentInstance();
		if(inst==null)
			return null;
		return getProcess(inst);
	}

	private void addVirtualPortToProcessor(VirtualPort lvp, ComponentInstance processor) {
		if(virtualPortPerProcessor.get(processor) == null)
		{
			Set<VirtualPort> vpSet = new LinkedHashSet<RoutingProperties.VirtualPort>();
			virtualPortPerProcessor.put(processor, vpSet);
		}
		virtualPortPerProcessor.get(processor).add(lvp);
	}

	private ComponentInstance getBusAccessProcessor(FeatureInstance busAccess) {
		if(isProcessor(busAccess.getComponentInstance()))//.getCategory().equals(ComponentCategory.PROCESSOR))
			return busAccess.getComponentInstance();
		else
			return getProcessorContainer(busAccess.getComponentInstance());
			
	}

	private ComponentInstance getProcessorContainer(ComponentInstance componentInstance) {
		if(componentInstance == null)
			return null;
		else if(isProcessor(componentInstance))
			return componentInstance;
		else
			return getProcessorContainer((ComponentInstance) componentInstance.eContainer());
	}

	private Set<ConnectionInstance> getConnectionInstances(FeatureInstance f) {
		Set<ConnectionInstance> result = new LinkedHashSet<ConnectionInstance>();

		ComponentInstance containerComponent = f.getContainingComponentInstance();
		if(containerComponent.getCategory().equals(ComponentCategory.THREAD))
		{
			return getConnectionInstancesFromThreadFeature(f);
		}
		for(ComponentInstance thread: containerComponent.getComponentInstances())
		{
			if(!thread.getCategory().equals(ComponentCategory.THREAD))
				continue;
			for(FeatureInstance fi: thread.getAllFeatureInstances())
			{
				if(fi.getDirection() == DirectionType.OUT)
				{
					for(ConnectionInstance ci : fi.getSrcConnectionInstances())
					{
						for(ConnectionReference cnxRef: ci.getConnectionReferences())
						{
							if(cnxRef.getDestination() == f)
								result.add((ConnectionInstance)cnxRef.eContainer());
						}
					}
				}
				if(fi.getDirection() == DirectionType.IN)
				{
					for(ConnectionInstance ci: fi.getDstConnectionInstances())
					{
						for(ConnectionReference cnxRef: ci.getConnectionReferences())
						{
							if(cnxRef.getSource() == f)
								result.add((ConnectionInstance)cnxRef.eContainer());
						}
					}
				}
			}
		}
		return result;
	}

	private Set<ConnectionInstance> getConnectionInstancesFromThreadFeature(FeatureInstance f) {
		Set<ConnectionInstance> result = new LinkedHashSet<ConnectionInstance>();
		result.addAll(f.getSrcConnectionInstances());
		result.addAll(f.getDstConnectionInstances());
		return result;
	}

	private boolean isProcessor(ComponentInstance otherProcessor) {
		if(otherProcessor.getCategory() == ComponentCategory.PROCESSOR)
			return true;
		if(otherProcessor.getCategory() == ComponentCategory.SYSTEM)
		{
			try {
				return PropertyUtils.getBooleanValue(otherProcessor, "Is_Processor");
			} 
			catch (Exception e)
			{
				return false;
			}
		}
		return false;
	}

	public static ComponentInstance getActualConnectionBinding(ConnectionInstance connectionInstance) {
		if(false==connectionInstance.getKind().equals(org.osate.aadl2.instance.ConnectionKind.PORT_CONNECTION))
			return null;
		PropertyExpression pe = PropertyUtils.getPropertyValue("Actual_Connection_Binding", connectionInstance);
		if(pe == null)
			return null;
		ListValue lvBus = (ListValue) pe;
		InstanceReferenceValue irvBus = (InstanceReferenceValue) lvBus.getOwnedListElements().get(0);
		return (ComponentInstance) irvBus.getReferencedInstanceObject();
	}
	

	private static List<ComponentInstance> getDeepestActualConnectionBinding(ComponentInstance bus) {
		List<ComponentInstance> res = new ArrayList<ComponentInstance>();
		if(false == bus.getCategory().equals(ComponentCategory.VIRTUAL_BUS)
				&& false == bus.getCategory().equals(ComponentCategory.BUS))
			return res;
		
		PropertyExpression pe = PropertyUtils.getPropertyValue("Actual_Connection_Binding", bus);
		ListValue lvBus = (ListValue) pe;
		List<PropertyExpression> peList = lvBus.getOwnedListElements();
		for(PropertyExpression ref: peList)
			if(ref instanceof InstanceReferenceValue)
			{
				InstanceReferenceValue irv = (InstanceReferenceValue) ref;
				if(irv.getReferencedInstanceObject() instanceof ComponentInstance)
				{
					res.add((ComponentInstance)irv.getReferencedInstanceObject());
				}
			}
		return res;
	}
	
	
	private ComponentInstance getProcessorBinding(ComponentInstance process)
	{
		ComponentInstance processor = AadlHelperImpl.getDeloymentProcessorInstance(process);
		if(processor==null)
			return null;
		
		if(process.getCategory().equals(ComponentCategory.PROCESS))
		{
			if(processPerProcessor.get(processor)!=null)
				processPerProcessor.get(processor).add(process);
			else
			{
				Set<ComponentInstance> processes  =new LinkedHashSet<ComponentInstance>();
				processes.add(process);
				processPerProcessor.put(processor, processes);
			}
		}

		return processor ;
	}
	
  public static boolean needsRoutage(FeatureInstance fi)
  {
    boolean result = false;
    if(AadlHelperImpl.isPort(fi))
    {
      Set<FeatureInstance> dstList = getFeatureDestinations(fi);
      if(dstList!=null)
      {
        for(FeatureInstance dst : dstList)
          if(false == areInSameProcess(fi,dst))
            return true;
      }
      List<FeatureInstance> srcList = getFeatureSources(fi);
      if(srcList!=null)
      {
        for(FeatureInstance src : srcList)
          if(false == areInSameProcess(fi,src))
            return true;
      }
    }
    return result;
  }
  
  public static boolean areInSameProcess(FeatureInstance src, FeatureInstance dst)
  {
    ComponentInstance srcProcess=null, dstProcess=null;
    if(src.getContainingComponentInstance().getCategory()
        .equals(ComponentCategory.THREAD))
      srcProcess = src.getContainingComponentInstance()
    		  .getContainingComponentInstance();
    else if (src.getContainingComponentInstance().getCategory()
            .equals(ComponentCategory.PROCESS))
    	srcProcess = src.getContainingComponentInstance();
    if(dst.getContainingComponentInstance().getCategory()
        .equals(ComponentCategory.THREAD))
      dstProcess = dst.getContainingComponentInstance()
    		  .getContainingComponentInstance();
    else if(dst.getContainingComponentInstance().getCategory()
            .equals(ComponentCategory.PROCESS))
    	dstProcess = dst.getContainingComponentInstance();
    if(srcProcess==null || dstProcess==null)
      return false;
    return srcProcess.equals(dstProcess);
  }
  
  public static List<FeatureInstance> getFeatureSources(FeatureInstance port)
  {
    List<FeatureInstance> result = new ArrayList<FeatureInstance>();
	if(port.getContainingComponentInstance().getCategory()
        .equals(ComponentCategory.THREAD))
    {
      for(ConnectionInstance ci: port.getDstConnectionInstances())
      {
        FeatureInstance fi = (FeatureInstance)ci.getSource();
        if(fi.getContainingComponentInstance().getCategory()
            .equals(ComponentCategory.THREAD))
        {
          result.add((FeatureInstance)ci.getConnectionReferences().get(0).getDestination());
        }
      }
    }
    else if(port.getContainingComponentInstance().getCategory()
            .equals(ComponentCategory.PROCESS))
    {
      ComponentInstance process = port.getContainingComponentInstance();
      for(ComponentInstance thread: process.getComponentInstances())
      {
        if(!thread.getCategory().equals(ComponentCategory.THREAD))
          continue;
    	for(FeatureInstance fi: thread.getAllFeatureInstances())
    	{
      	  int last = fi.getDstConnectionInstances().size()-1;
      	  if(last>=0 && fi.getDirection() == DirectionType.IN)
      	  {
      		  for(ConnectionReference cnxRef: fi.getDstConnectionInstances().get(last).getConnectionReferences())
      		  {
      			  if(cnxRef.getSource() == port)
      				  result.addAll(getFeatureSources(fi));
      		  }
      	  }
    	}
      }
    }
    return result;
  }
  
  public static List<ConnectionInstance> getFeatureConnections(FeatureInstance port)
  {
	  List<ConnectionInstance> result = new ArrayList<ConnectionInstance>();
	  if(port.getContainingComponentInstance().getCategory()
			  .equals(ComponentCategory.THREAD))
	  {
		  return port.getSrcConnectionInstances();
	  }
	  else if(port.getContainingComponentInstance().getCategory()
			  .equals(ComponentCategory.PROCESS))
	  {
		  ComponentInstance process = port.getContainingComponentInstance();
		  for(ComponentInstance thread: process.getComponentInstances())
		  {
			  if(!thread.getCategory().equals(ComponentCategory.THREAD))
				  continue;
			  for(FeatureInstance fi: thread.getFeatureInstances())
			  {
				  if(fi.getDirection() == DirectionType.OUT)
				  {
					  for(ConnectionReference cnxRef: fi.getSrcConnectionInstances().get(0).getConnectionReferences())
					  {
						  if(cnxRef.getDestination() == port)
							  result.addAll(getFeatureConnections(fi));
					  }
				  }
			  }
		  }
	  }
	  return result;
  }
  
  public static Set<FeatureInstance> getConnectedProcessPort(FeatureInstance threadPort)
  {
	  Set<FeatureInstance> res = new LinkedHashSet<FeatureInstance>();
	  for(ConnectionInstance ci: threadPort.getSrcConnectionInstances())
      {
        FeatureInstance fi = (FeatureInstance)ci.getDestination();
        if(fi.getContainingComponentInstance().getCategory()
            .equals(ComponentCategory.THREAD))
        {
    	  int last = ci.getConnectionReferences().size()-1;
    	  FeatureInstance toAdd = (FeatureInstance) ci.getConnectionReferences().get(last).getSource();
    	  if(!toAdd.getComponentInstance().equals(threadPort.getComponentInstance()))
    		  res.add(toAdd);
        }
      }
	  for(ConnectionInstance ci: threadPort.getDstConnectionInstances())
      {
        FeatureInstance fi = (FeatureInstance)ci.getSource();
        if(fi.getContainingComponentInstance().getCategory()
            .equals(ComponentCategory.THREAD))
        {
    	  int last = ci.getConnectionReferences().size()-1;
    	  FeatureInstance toAdd = (FeatureInstance) ci.getConnectionReferences().get(last).getSource();
    	  if(!toAdd.getComponentInstance().equals(threadPort.getComponentInstance()))
    		  res.add(toAdd);
        }
      }
	  return res;
  }
  
  public static Set<FeatureInstance> getFeatureDestinations(FeatureInstance pokPort)
  {
	Set<FeatureInstance> result = new LinkedHashSet<FeatureInstance>();
    if(pokPort.getContainingComponentInstance().getCategory()
        .equals(ComponentCategory.THREAD))
    {
    	result.addAll(getConnectedProcessPort(pokPort));
    	return result;
    }
    else if(pokPort.getContainingComponentInstance().getCategory()
            .equals(ComponentCategory.PROCESS))
    {
      ComponentInstance process = pokPort.getContainingComponentInstance();
      for(ComponentInstance thread: process.getComponentInstances())
      {
    	if(!thread.getCategory().equals(ComponentCategory.THREAD))
    	  continue;
    	for(FeatureInstance fi: thread.getAllFeatureInstances())
    	{
    	  if(fi.getDirection() == DirectionType.OUT)
    	  {
    		for(ConnectionReference cnxRef: fi.getSrcConnectionInstances().get(0).getConnectionReferences())
    		{
    		  if(cnxRef.getDestination() == pokPort)
    	        result.addAll(getFeatureDestinations(fi));
    		}
    	  }
    	}
      }
    }
    else if(pokPort.getContainingComponentInstance().getCategory()
            .equals(ComponentCategory.PROCESSOR))
    {
    	if(!pokPort.getCategory().equals(FeatureCategory.BUS_ACCESS))
    		return result;
    	for(ConnectionInstance cnxInst: pokPort.getSrcConnectionInstances())
		{
			ComponentInstance bus = (ComponentInstance) cnxInst.getDestination();
			for(ConnectionInstance busCnxInst: bus.getSrcConnectionInstances())
    		{
				if(busCnxInst.getDestination() instanceof FeatureInstance)
				{
					FeatureInstance otherBusAccess = (FeatureInstance) busCnxInst.getDestination() ;
					if(otherBusAccess != pokPort)
						result.add(otherBusAccess);
				}
    		}
			for(ConnectionInstance busCnxInst: bus.getDstConnectionInstances())
    		{
				if(busCnxInst.getSource() instanceof FeatureInstance)
				{
					FeatureInstance otherBusAccess = (FeatureInstance) busCnxInst.getSource() ;
					if(otherBusAccess != pokPort)
						result.add(otherBusAccess);
				}
    		}
		}
    }
    return result;
  }

  public ComponentInstance getBoundProcessorFor(FeatureInstance dst) {
	  ComponentInstance process = dst.getComponentInstance();
	  return getProcessorBinding(process);
  }
  
  public class VirtualPort {
	  // Warning: busAccess is in the input model
	  FeatureInstance busAccess;
	  
	  // process is in the output model
	  ComponentInstance process;
	  
	  ComponentInstance destinationProcess = null;
	  
	  public VirtualPort(FeatureInstance virtualBus,
			  ComponentInstance process, 
			  ComponentInstance destinationProcess)
	  {
		  this.busAccess = virtualBus;
		  this.process = process;
		  this.destinationProcess = destinationProcess;
	  }
	  
	  @Override
	  public boolean equals(Object obj) {
		  VirtualPort lvp = (VirtualPort) obj;
		  return (this.process == lvp.process 
				  && this.busAccess == lvp.busAccess
				  && this.destinationProcess == lvp.destinationProcess);
	  }
	  
	  @Override
	  public int hashCode() {
		  return 0;
	  }
	  
	  public FeatureInstance getBusAccess()
	  {
		  return busAccess;
	  }

	  public ComponentInstance getProcess() {
		  return process;
	  }
	  
	  public ComponentInstance getDestinationProcess()
	  {
		  return destinationProcess;
	  }
  }
  
  public Set<VirtualPort> virtualPortLimitedToProcessor(ComponentInstance processor)
  {
	  // processor must be in the input model
	  Set<VirtualPort> res = virtualPortPerProcessor.get(processor);
	  Set<ComponentInstance> processes = processPerProcessor.get(processor);
	  if(res == null && processes!=null)
	  {
		  res = new LinkedHashSet<VirtualPort>();
		  // pass by holded process as processor is not necessarily copied in output model		  
		  for(ComponentInstance ci : processes)
		  {
			  Subcomponent subProcess = ci.getSubcomponent();
			  ArchTraceSpec traces = ArchTraceSourceRetreival.getTracesFromNamedElement(subProcess);
			  if(traces==null)
				  return res;
			  // retreive processor in input model
			  ComponentInstance processInSource = (ComponentInstance) ArchTraceSourceRetreival.
						findNamedElementByClassInTransformationTraces(traces, subProcess, ComponentInstanceImpl.class);
			  if(processInSource.getCategory() == ComponentCategory.PROCESS)
			  {
				  ComponentInstance processorInSource = getProcessorBinding(processInSource);
				  res.addAll(virtualPortLimitedToProcessor(processorInSource));
			  }
			  else if(processInSource.getCategory() == ComponentCategory.VIRTUAL_PROCESSOR)
			  {
				  ComponentInstance processorInSource = getProcessorBinding(processInSource);
				  if(processorInSource==null)
					  processorInSource = getProcessorContainer(processInSource);
				  res.addAll(virtualPortLimitedToProcessor(processorInSource));
			  }  
		  }
		  virtualPortPerProcessor.put(processor, res);
	  }
//	  if(res==null)
//		  return new HashSet<RoutingProperties.VirtualPort>();
	  return res;
  }
  
  public Set<VirtualPort> virtualPortLimitedToProcess(ComponentInstance process)
  {
	  Set<VirtualPort> res = new LinkedHashSet<RoutingProperties.VirtualPort>();
	  for(VirtualPort vp: globalVirtualPort)
	  {
		  if(vp.getProcess().equals(process))
		  {
			  res.add(vp);
		  }
	  }
	  return res;
  }
  
  
  public List<FeatureInstance> getProcessPorts(ComponentInstance process)
  {
	  List<FeatureInstance> res = new ArrayList<FeatureInstance>();
	  for(FeatureInstance fi: process.getAllFeatureInstances())
	  {
		  if(!globalPort.isEmpty() && !globalPort.contains(fi))
			  continue;
		  if(AadlHelperImpl.isPort(fi))
			  res.add(fi);
	  }
	  return res;
	  
  }

  public Set<VirtualPort> getFeatureDestinations(VirtualPort vp) {
	  Set<VirtualPort> res = new LinkedHashSet<RoutingProperties.VirtualPort>();
	  // get virtual port process
	  ComponentInstance process = vp.getProcess();
	  ComponentInstance destinationProcess = vp.getDestinationProcess();
	  
	  // ignore it virtual ports of same processor
	  ComponentInstance processor = processorVirtualPort.get(vp);
	  
	  // ports are only in the 
	  // get output ports
	  for(FeatureInstance fi: AadlHelperImpl.getAllProcessPorts(process))
	  {
		  if(fi.getDirection().equals(DirectionType.IN))
			  continue;
		  Set<FeatureInstance> dstList = getFeatureDestinations(fi);
		  // get dst virtual port
		  for(FeatureInstance dst: dstList)
		  {
			  if(virtualPortPerPort.get(dst)==null)
				  registerVirtualPort(dst);
			  if(virtualPortPerPort.get(dst)!=null)
			  {
				  ComponentInstance dstProcessor = processorVirtualPort.get(virtualPortPerPort.get(dst));
				  if(dstProcessor.equals(processor))
					  continue;
				  if(destinationProcess!=null && getProcess(dst)==destinationProcess)
					  res.add(virtualPortPerPort.get(dst));
				  else if(destinationProcess==null)
					  res.add(virtualPortPerPort.get(dst));
			  }
		  }
	  }
	  return res;
  }
  
  public static long getFeatureDataSize(FeatureInstance fi) {
	  try
	  {
		  return PropertyUtils.getIntValue(fi, "Data_Size", AadlProject.B_LITERAL);
	  }
	  catch (Exception e)
	  {
		  return 0;
	  }
  }
}