/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen.utils ;

public interface Names {
//  public final static String[] FILE_EXTENSIONS = new String[]
//  {"aadl2", "aadl"} ;

  String ANALYSIS_EXT_ID = "analyzer" ;

  String GENERATOR_EXT_ID = "generator" ;

  String LOOPMANAGER_EXT_ID = "loopManager" ;

  
  String ATT_ID = "id" ;
  String ATT_NAME = "name" ;
  String ATT_ANNEXNAME = "annexName" ;
  String ATT_CLASS = "class" ;
  
  String AADL_RESOURCE_DIRECTORY_NAME="aadl_resources" ;
  String RAMSES_RESOURCES_VAR = "RAMSES_DIR";
  
  String GENERATION_ROOT_OBJECT_CLASS = "org.osate.aadl2.SystemImplementation" ;
  
  String DEFAULT_RAMSES_RESOUCE_DIR = "." ;
  
  String AADL_PREDEFINED_PACKAGE_DIR_NAME = "package" ;
  
  String AADL_PREDEFINED_PROPERTIES_DIR_NAME = "propertyset" ;
  
  String LOG4J_CONSOLE_APPENDER_NAME = "ramses_console_log" ;
  
  String NEW_LINE = System.getProperty("line.separator");
  
  String LOGGING_FILENAME = ".ramses-log" ;

  String VALIDATOR_SUFFIX = "-validator";

  String RAMSES_PROPERTIES = "RAMSES_PROPERTIES" ;
  
  String TIMING_ANALYSIS_PLUGIN_NAME = "AADLInspector-SchedulingAnalysis";
  
  String WCET_ANALYSIS_PLUGIN_NAME = "WCETModelsExtractionAnalysis";
  
  String MEMORY_ANALYSIS_PLUGIN_NAME = "RAMSES-MemoryFootprintAnalysis";

  String RELIABILITY_ANALYSIS_PLUGIN_NAME = "RAMSES-ReliabilityAnalysis";

  String APOSTERIORI_ANALYSIS_PLUGIN_NAME = "RAMSES-aPosterioriChecking";
  
  String LATENCY_ANALYSIS_PLUGIN_NAME = "RAMSES-LatencyAnalysis";

  String SWITCHED_NETWORK_ANALYSIS_PLUGIN_NAME = "RAMSES-SwitchedNetworkLatencyAnalysis";
  
  String MAINTAINABILITY_ANALYSIS_PLUGIN_NAME = "RAMSES-MaintainabilityAnalysis";
  
  String MAINTENANCE_QA = "Maintenance";
  
  String PRISM_ANALYSIS_PLUGIN_NAME = "PRISM-AvailabilityAnalysis";
  
  String OSATE_LatencyFlow_ANALYSIS_PLUGIN_NAME = "OSATE-FlowLatencyAnalysis";

  String EXECUTION_TIME_ANALYSIS_PLUGIN_NAME = "RAMSES-ExecutionTimeAnalysis";

  String EXECTIME_ANALYSIS_PLUGIN_NAME = "RAMSES-ExectimeAnalysis";
  
  String RAMSES_CODGEN_PROBLEM_MARKER = "fr.mem4csd.ramses.core.codegenProblem";
}