package fr.mem4csd.ramses.core.codegen.utils;

import java.util.ArrayList ;
import java.util.List ;

import org.apache.log4j.Logger ;
import org.eclipse.emf.common.util.EList ;
import org.osate.aadl2.Classifier ;
import org.osate.aadl2.ClassifierValue ;
import org.osate.aadl2.ComponentClassifier ;
import org.osate.aadl2.ComponentImplementation ;
import org.osate.aadl2.ComponentPrototypeBinding ;
import org.osate.aadl2.ComponentType ;
import org.osate.aadl2.DataAccess;
import org.osate.aadl2.DataClassifier ;
import org.osate.aadl2.DataPort;
import org.osate.aadl2.DataPrototype ;
import org.osate.aadl2.DataSubcomponent ;
import org.osate.aadl2.DataSubcomponentType ;
import org.osate.aadl2.EventDataPort;
import org.osate.aadl2.EventPort;
import org.osate.aadl2.Feature;
import org.osate.aadl2.IntegerLiteral ;
import org.osate.aadl2.ListValue ;
import org.osate.aadl2.NamedElement ;
import org.osate.aadl2.PropertyExpression ;
import org.osate.aadl2.PrototypeBinding ;
import org.osate.aadl2.Subcomponent ;
import org.osate.aadl2.instance.ComponentInstance ;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.ba.analyzers.TypeHolder ;
import org.osate.ba.utils.AadlBaUtils ;
import org.osate.ba.utils.DimensionException ;
import org.osate.utils.internal.PropertyUtils ;
import org.osate.utils.internal.names.DataModelProperties ;
import org.osate.xtext.aadl2.properties.util.AadlProject;

import fr.mem4csd.ramses.core.helpers.impl.RamsesException;


public class DataSizeHelper
{
  
  private final static Logger _LOGGER = Logger.getLogger(DataSizeHelper.class) ;

  private DataSizeHelper(){}
  
  private static int getSourceDataSizeInOctetsImpl(NamedElement e)
  {
    double size = 0l;
    try 
    {
      size = PropertyUtils.getIntValue(e, "Data_Size", AadlProject.B_LITERAL);
    } 
    catch (Exception e1) 
    {
      _LOGGER.warn("Data_Size property missing for "+e.getFullName());
    }
    return (int) size;
  }
  

  public static double getSourceDataSizeInOctets(NamedElement e)
  {
    double size = 0;
    
    size = getSourceDataSizeInOctetsImpl(e);
    if(size!=0)
    	return size;
    if(e instanceof FeatureInstance)
    {
    	FeatureInstance fi = (FeatureInstance) e;
        Feature f = fi.getFeature();
        return getSourceDataSizeInOctets(f);
    }
    if(e instanceof DataPort)
    {
    	DataPort dp = (DataPort) e;
    	return getOrComputeDataSize(dp.getDataFeatureClassifier());
    }
    if(e instanceof EventDataPort)
    {
    	EventDataPort edp = (EventDataPort) e;
    	return getOrComputeDataSize(edp.getDataFeatureClassifier());
    }
    if(e instanceof EventPort)
    {
    	return 1;
    }
    if(e instanceof DataAccess)
    {
    	DataAccess da = (DataAccess) e;
    	return getOrComputeDataSize(da);
    }
    ComponentClassifier cc = null;
    if(e instanceof ComponentInstance)
    {
      ComponentInstance inst = (ComponentInstance) e;
      cc = inst.getComponentClassifier();
    }
    if (e instanceof ComponentClassifier)
    {
      cc = (ComponentClassifier) e;
    }
    if(cc!=null && cc != e)
      size = getSourceDataSizeInOctetsImpl(cc);
    if (size == 0)
    {
      if (cc instanceof ComponentImplementation)
      {
        ComponentImplementation ci = (ComponentImplementation) cc;
        ComponentImplementation extended = ci.getExtended();

        if (extended != null)
        {
          size = getSourceDataSizeInOctets (extended);
          if (size == 0)
          {
            if (ci.getType() != null)
            {
              cc = ci.getType();
            }
          }
        }
      }
      if (cc instanceof ComponentType)
      {
        ComponentType ct = (ComponentType) cc;
        if(ct.getExtended()!=null)
        	size = getSourceDataSizeInOctets (ct.getExtended());
      }
    }
    return size;
  }
  
  
  private static double computeDataSize(NamedElement e)
  {
    return computeDataSize(e, null);
  }
  
  private static double computeDataSize(NamedElement e, Classifier owner)
  {
    DataClassifier dc = null;
    if(e instanceof ComponentInstance)
    {
      ComponentInstance ci = (ComponentInstance) e;
      DataSubcomponent ds = (DataSubcomponent) ci.getSubcomponent();
      DataSubcomponentType dst = ds.getDataSubcomponentType();
      if(dst instanceof DataClassifier)
        dc = (DataClassifier) dst;
      else if(dst instanceof DataPrototype && owner != null)
      {
        for(PrototypeBinding pb: owner.getOwnedPrototypeBindings())
        {
          if(pb instanceof ComponentPrototypeBinding
              && pb.getFormal().getName().equalsIgnoreCase(dst.getName()))
          {
            ComponentPrototypeBinding cpb = (ComponentPrototypeBinding) pb;
            dc = (DataClassifier) cpb.getActuals().get(0).getSubcomponentType();
            break;
          }
        }
      }
    }
    else if(e instanceof DataClassifier)
      dc = (DataClassifier) e;
    else if (e instanceof DataSubcomponent)
    {
      DataSubcomponent ds = (DataSubcomponent) e;
      DataSubcomponentType dst = ds.getDataSubcomponentType();
      if(dst instanceof DataClassifier)
        dc = (DataClassifier) dst;
      else if(dst instanceof DataPrototype && owner != null)
      {
        for(PrototypeBinding pb: owner.getOwnedPrototypeBindings())
        {
          if(pb instanceof ComponentPrototypeBinding 
              && pb.getFormal().getName().equalsIgnoreCase(dst.getName()))
          {
            ComponentPrototypeBinding cpb = (ComponentPrototypeBinding) pb;
            dc = (DataClassifier) cpb.getActuals().get(0).getSubcomponentType();
            break;
          }
        }
      }
    }
    double res = 0;
    TypeHolder dataTypeHolder = null ;
    try
    {
      dataTypeHolder = AadlBaUtils.getTypeHolder(dc) ;
    }
    catch(DimensionException ex)
    {
      // This is an internal error, to be logged and displayed
      String errMsg =  RamsesException.formatRethrowMessage("cannot fetch the type of \'" +
          e.getQualifiedName() + '\'', ex) ;
      _LOGGER.error(errMsg, ex);
//      ServiceProvider.SYS_ERR_REP.error(errMsg, true);
    }

    // Array
    switch ( dataTypeHolder.getDataRep())
    {
      // Simple types
      case BOOLEAN:
        System.out.println("BOOLEAN ");
        res = 1;
        break;
      case CHARACTER:
        System.out.println("CHARACTER ");
        res = 1;
        break;
      case FLOAT :
        res = 4;
        break;
      case INTEGER :
        res = 2;
        break ;
      case ENUM :
        res = 1;
        break ;
      case STRUCT :
        if(dc instanceof ComponentImplementation)
        {
          ComponentImplementation ci = (ComponentImplementation) dc;
          for(Subcomponent s: ci.getAllSubcomponents())
          {
            if(s instanceof DataSubcomponent)
            {
              DataSubcomponent ds = (DataSubcomponent) s;
              res+=getOrComputeDataSize(ds, ci);
            }
          }
        }
        break ;
      case ARRAY :
        List<Classifier> classifierList = new ArrayList<Classifier>();
        EList<PropertyExpression> baseType =
            PropertyUtils
                  .findPropertyExpression(dataTypeHolder.getKlass(),
                                         DataModelProperties.BASE_TYPE) ;
        for(PropertyExpression baseTypeProperty : baseType)
        {
          if(baseTypeProperty instanceof ListValue)
            {
              ListValue lv = (ListValue) baseTypeProperty ;

              for(PropertyExpression v : lv.getOwnedListElements())
              {
                if(v instanceof ClassifierValue)
                {
                  ClassifierValue cv = (ClassifierValue) v ;
                  classifierList.add(cv.getClassifier());
                }
              }
            }
        }
        int i=0;
        EList<PropertyExpression> arrayDimensions =
                  PropertyUtils
                        .findPropertyExpression(dataTypeHolder.getKlass(),
                                               DataModelProperties.DIMENSION) ;
        for(PropertyExpression dimensionProperty : arrayDimensions)
          {
            if(dimensionProperty instanceof ListValue)
            {
              ListValue lv = (ListValue) dimensionProperty ;
 
              for(PropertyExpression v : lv.getOwnedListElements())
              {
                if(v instanceof IntegerLiteral)
                {
                  Classifier c = classifierList.get(i);
                  IntegerLiteral dimension = (IntegerLiteral) v ;
                  double unitDataSize = getOrComputeDataSize(c);
                  res+=dimension.getValue()*unitDataSize;
                  i++;
                }
              }
            }
          }
        break;
      case UNION :
        System.out.println("CHARACTER ");
        break ;
      default :
        break ;
    }
    return res;
    
//    String rep = getDataRepresentation(e);
//    if ((e == null) || (rep == null))
//    {
//      return 0;
//    }
//    else if ((rep.equalsIgnoreCase("Struct")) && (e instanceof DataImplementation))
//    {
//      DataImplementation di = (DataImplementation) e;
//      double size = 0;
//      for(Subcomponent sub : di.getAllSubcomponents())
//      {
//        if (sub instanceof DataSubcomponent)
//        {
//          DataSubcomponent dsub = (DataSubcomponent) sub;
//          DataSubcomponentType dst = dsub.getDataSubcomponentType();
//          DataClassifier subdc = resolveClassifier (dst, di);
//          size += getOrComputeDataSize (subdc); 
//        }
//      }
//      return size;
//    }
//    else if (rep.equalsIgnoreCase("Array"))
//    {
//      DataClassifier dc = getBaseType (e);
//      double elementSize = getOrComputeDataSize (dc);
//      double numberOfElements = getSumOfDimensions(e);
//      return numberOfElements*elementSize;
//    }
//    return 0;
  }
  
  public static double getOrComputeDataSize (NamedElement e)
  {
    double size = getSourceDataSizeInOctets (e);
    if (size == 0)
    {
      size = computeDataSize (e);
    }
    return size;
  }
  
  public static double getOrComputeDataSize (NamedElement e, Classifier owner)
  {
    double size = getSourceDataSizeInOctets (e);
    if (size == 0)
    {
      size = computeDataSize (e, owner);
    }
    return size;
  }
}
