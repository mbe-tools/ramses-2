/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator ;
import java.util.List ;
import java.util.Map ;
import java.util.Set;

import org.apache.log4j.Logger ;
import org.eclipse.emf.common.util.EList ;
import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.AccessCategory ;
import org.osate.aadl2.AccessConnection ;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.ComponentImplementation ;
import org.osate.aadl2.ConnectedElement ;
import org.osate.aadl2.Connection ;
import org.osate.aadl2.Data ;
import org.osate.aadl2.DataAccess ;
import org.osate.aadl2.DataClassifier ;
import org.osate.aadl2.DataSubcomponent ;
import org.osate.aadl2.EnumerationLiteral;
import org.osate.aadl2.IntegerLiteral;
import org.osate.aadl2.ListValue ;
import org.osate.aadl2.ModalPropertyValue ;
import org.osate.aadl2.NamedElement ;
import org.osate.aadl2.NamedValue;
import org.osate.aadl2.Parameter ;
import org.osate.aadl2.Port ;
import org.osate.aadl2.ProcessImplementation;
import org.osate.aadl2.ProcessSubcomponent ;
import org.osate.aadl2.Processor;
import org.osate.aadl2.ProcessorSubcomponent ;
import org.osate.aadl2.Property ;
import org.osate.aadl2.PropertyAssociation ;
import org.osate.aadl2.PropertyExpression ;
import org.osate.aadl2.RangeValue;
import org.osate.aadl2.ReferenceValue ;
import org.osate.aadl2.StringLiteral ;
import org.osate.aadl2.Subcomponent ;
import org.osate.aadl2.System;
import org.osate.aadl2.SystemImplementation ;
import org.osate.aadl2.SystemSubcomponent;
import org.osate.aadl2.SystemSubcomponentType;
import org.osate.aadl2.VirtualProcessorSubcomponent ;
import org.osate.aadl2.instance.ComponentInstance ;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.ba.aadlba.DataRepresentation ;
import org.osate.ba.analyzers.TypeHolder ;
import org.osate.ba.utils.AadlBaUtils ;
import org.osate.ba.utils.DimensionException ;
import org.osate.utils.internal.PropertyUtils ;
import org.osate.utils.internal.names.DataModelProperties ;
import org.osate.xtext.aadl2.properties.util.GetProperties;

public class GeneratorUtils
{
  private static Logger _LOGGER = Logger.getLogger(GeneratorUtils.class) ;
  
  public static String limitCharNb(String src, Integer limit, List<String> existingId)
  {
	// limit name to 30 chars as defined in POK
	if(src.length()>=limit)
		src = src.substring(src.length()-(limit-1), src.length());
	
	if(!existingId.contains(src)) {
		existingId.add(src);
		return src;
	}
	else
	{
		String res = src;
		int i=1;
		while(existingId.contains(res))
		{
			String suffix = Integer.toString(i);
			res = src+suffix;
			res = res.substring(suffix.length());
			i++;
		}
		existingId.add(res);
		return res;
	}
  }
  
  public static String getInitialValue(NamedElement e, String language)
  {
    StringBuilder initialization = new StringBuilder() ;

    if(e instanceof Data)
    {
      Data d = (Data) e ;
      if(d instanceof DataSubcomponent)
      {
        DataSubcomponent ds = (DataSubcomponent) d ;
        for(PropertyAssociation pa : ds.getOwnedPropertyAssociations())
        {
          Property p = pa.getProperty() ;

          // Sometime, properties don't have name.
          if(p.getName() != null &&
              p.getName()
              .equalsIgnoreCase(DataModelProperties.INITIAL_VALUE))
          {
            setInitialization(ds, initialization, PropertyUtils
                              .getPropertyExpression(pa), language) ;
            return initialization.toString() ;
          }
        }

        return getInitialValue(ds.getClassifier(), language) ;
      }
      else if(d instanceof DataClassifier)
      {
        DataClassifier dc = (DataClassifier) d ;
        EList<PropertyExpression> initialValueProperty =
            PropertyUtils
            .findPropertyExpression(dc,
                                   DataModelProperties.INITIAL_VALUE) ;
        setInitialization(dc, initialization, initialValueProperty, language) ;
        return initialization.toString() ;
      }
    }
    else if(e instanceof Port)
    {
    
    }
    else if(e instanceof Parameter)
    {
    
    }
    return initialization.toString() ;
  }

  private static void setInitialization(NamedElement obj,
		  StringBuilder res,
		  List<PropertyExpression> initialValues,
		  String language)
  {
	StringBuilder initialization = new StringBuilder();
	
    boolean isStringType = false;
    DataClassifier dc;
    if(obj instanceof DataSubcomponent)
    {
      DataSubcomponent ds = (DataSubcomponent) obj;
      dc = (DataClassifier) ds.getDataSubcomponentType();
    }
    else
      dc=(DataClassifier) obj;
    
    try
    {
      TypeHolder dataTypeHolder = AadlBaUtils.getTypeHolder(dc);
      isStringType=dataTypeHolder.getDataRep().equals(DataRepresentation.STRING);
      
    }
    catch(DimensionException ex)
    {
      String errMsg = "fail to fetch the initial value of " +  obj.getName();
      _LOGGER.error(errMsg, ex);
    //  Dialog.showError("Initialization: ", errMsg);
//      ServiceProvider.SYS_ERR_REP.error(errMsg, true);
    }

    for(PropertyExpression pe : initialValues)
    {
      if(pe instanceof ListValue)
      {
        ListValue lv = (ListValue) pe ;
        List<PropertyExpression> initValueList = lv.getOwnedListElements() ;

        if(initValueList.size() > 0)
        {
          if(language.equals("ada"))
            res.append(" := ") ;
          else
            res.append(" = ") ;
        }
        
        if(initValueList.size() > 1)
        {
          if(language.equals("ada"))
        	  initialization.append("(") ;
          else
        	  initialization.append("{") ;
        }

        Iterator<PropertyExpression> it = initValueList.iterator() ;

        while(it.hasNext())
        {
          PropertyExpression initValue = it.next() ;

          if(initValue instanceof StringLiteral)
          {
            StringLiteral sl = (StringLiteral) initValue ;
            if (language.equalsIgnoreCase("java"))
            {
            	if (obj instanceof DataClassifier)
            	{
		          	if (AadlBaUtils.getDataRepresentation((DataClassifier)obj) == DataRepresentation.ENUM)	
		          	{
		              initialization.append(obj.getQualifiedName() +
		                                    "INSERTDOTHERE");
		          	}
            	}
            }
            
            initialization.append(sl.getValue().replace("\\", "")) ;
            
            if(it.hasNext())
            {
              initialization.append(",") ;
            }
          }
        }

        if(initValueList.size() > 1)
        {
          if(language.equals("ada"))
            initialization.append(")") ;
          else
          	initialization.append("}") ;
        }
        
        isStringType = isStringType && !initialization.toString().equalsIgnoreCase("NULL");
        
        if(isStringType && false==initialization.toString().startsWith("\""))
            res.append("\"");
        
        res.append(initialization);
        
        if(isStringType && false==initialization.toString().endsWith("\""))
        	res.append("\"");

      }
    }
  }
  
  //Builds the data access mapping via the connections described in the
  // process implementation.
  public static void buildDataAccessMapping(ComponentImplementation cptImpl,
                                            Map<DataAccess, DataSubcomponent> _dataAccessMapping)
  {
    
    EList<Subcomponent> subcmpts = cptImpl.getAllSubcomponents() ;
    
    List<DataSubcomponent> dataSubcomponents = new ArrayList<DataSubcomponent>() ;
    
    // Fetches data subcomponent names.
    for(Subcomponent s : subcmpts)
    {
      if(s instanceof DataSubcomponent)
      {
    	  dataSubcomponents.add((DataSubcomponent) s) ;
      }
    }
    
    // Binds data subcomponent names with DataAcess objects
    // of threads.
    // See process implementation's connections.
    for(Connection connect : cptImpl.getAllConnections())
    {
      if (connect instanceof AccessConnection &&
         ((AccessConnection) connect).getAccessCategory() == AccessCategory.DATA)
      {

      if(connect.getAllDestination() instanceof DataSubcomponent)
      {
        DataSubcomponent destination =  (DataSubcomponent) connect.
                                                       getAllDestination() ;
        
          if(dataSubcomponents.contains(destination))
          {
            ConnectedElement source = (ConnectedElement) connect.getSource() ;
            DataAccess da = (DataAccess) source.getConnectionEnd() ;
            _dataAccessMapping.put(da, destination) ; 
          }
      }
        else if(connect.getAllSource() instanceof DataSubcomponent)
        {
          DataSubcomponent source =  (DataSubcomponent) connect.
              getAllSource() ;
          if(dataSubcomponents.contains(source))
          {
            ConnectedElement dest = (ConnectedElement) connect.getDestination() ;
             
            DataAccess da = (DataAccess) dest.getConnectionEnd() ;
            _dataAccessMapping.put(da, source) ;
          }
        }
        else if(connect.getAllDestination() instanceof DataAccess
            && connect.getAllSource() instanceof DataAccess)
        {
          if(!(connect.getAllDestination().eContainer() instanceof Thread)
            && !(connect.getAllSource().eContainer() instanceof Thread))
            continue;
          DataAccess destination = (DataAccess) connect.getAllDestination();
          DataAccess source = (DataAccess) connect.getAllSource();
          if(_dataAccessMapping.containsKey(destination) &&
              !_dataAccessMapping.containsKey(source))
            _dataAccessMapping.put(source, _dataAccessMapping.get(destination)) ;
          if(_dataAccessMapping.containsKey(source) &&
              !_dataAccessMapping.containsKey(destination))
            _dataAccessMapping.put(destination, _dataAccessMapping.get(source)) ;
          
        }
      }
    }
  }

  public static String getSchedulingProtocol(NamedElement ne) 
  {
	  Property prop = GetProperties.lookupPropertyDefinition(ne, "Deployment_Properties", "Scheduling_Protocol");
	  List<? extends PropertyExpression> props;
	  try {
		  props = ne.getPropertyValueList(prop);
	  } catch (Exception e) {
		  props = Collections.emptyList();
	  }

	  for(PropertyExpression pe: props)
	  {
		  NamedValue nv = (NamedValue) pe;
		  EnumerationLiteral el = (EnumerationLiteral) nv.getNamedValue();
		  return el.getName();
	  }
	  return null;
  }

  public static long getMaxPriority(NamedElement processor) {
	  
	  RangeValue rv = PropertyUtils.getRangeValue(processor, "Priority_Range");
	  if(rv!=null) {
		  IntegerLiteral il = (IntegerLiteral) rv.getMaximum();
		  return il.getValue()-5;
	  }
	  else
	  {
		  long defaultValue = 50;
		  String errMsg = "No Priority_Range property defined for component "+
				  processor.getName() +"; max prio set to default value: "+defaultValue;
	      _LOGGER.warn(errMsg);
	  //    Dialog.showWarning("Priority: ", errMsg);
//	      ServiceProvider.SYS_ERR_REP.warning(errMsg, true);
	      return defaultValue;
	  }
  }

  public static boolean processUsesMQTT(ProcessImplementation procImpl) {
    for(DataSubcomponent ds: procImpl.getOwnedDataSubcomponents())
    {
      if(ds.getName().contains("MQTT"))
      {
        return true;
      }
    }
    return false;
  }
  
  public static boolean processUsesSOCKET(ProcessImplementation procImpl) {
    for(DataSubcomponent ds: procImpl.getOwnedDataSubcomponents())
    {
      if(ds.getName().contains("_SocketConfig"))
      {
        return true;
      }
    }
    return false;
  }
  
  public static void addCFilesInFolder(final URI folderURI, Set<URI> sourceFilesURISet) {

	  File dir = new File(folderURI.toFileString());
	  for (final File fileEntry : dir.listFiles())
	  {
		  if(fileEntry.getName().endsWith(".c"))
			  sourceFilesURISet.add(folderURI.appendSegment(fileEntry.getName()));
	  }
  }
  
  public static Long toBitMask(List<Long> integerList)
  {
	 Long bitmask = 0L;
 	 
 	 for(Long i: integerList)
 	 {
 		 Double pow_2 = Math.pow(2, i-1);
 		 bitmask = bitmask | pow_2.longValue();
 	 }
 	 
 	 return bitmask;
  }

  public static String getEntryPoint(NamedElement ne, String classifierPropertyName, String textPropertyName, Collection<String> headerFiles) {
	  return getEntryPoint(ne, classifierPropertyName, textPropertyName, headerFiles, new ArrayList<String>());
  }
  
  public static String getEntryPoint(NamedElement ne, String classifierPropertyName, String textPropertyName, Collection<String> headerFiles, Collection<String> sourceFiles) {
	  String result = null;
	  Classifier c = PropertyUtils.getClassifierValue(ne, classifierPropertyName);
	  if(c!=null)
	  {
		  String sourceName = PropertyUtils.getStringValue(c, "Source_Name");
		  if(sourceName==null)
			  sourceName = c.getName();
		  result = sourceName;
		  List<String> sourceText = PropertyUtils.getStringListValue(c, "Source_Text");
		  if (sourceText != null) {
			  for (String fileName : sourceText) {
				  if (fileName.endsWith(".h")) {
					  headerFiles.add(fileName);
				  }
				  if (fileName.endsWith(".c")) {
					  sourceFiles.add(fileName);
				  }
			  }
		  }

	  }
	  else
	  {
		  String recoverSourceText = PropertyUtils.getStringValue(ne, textPropertyName);
		  if(recoverSourceText!=null)
			  result = recoverSourceText;
	  }

	  List<String> sourceText = PropertyUtils.getStringListValue(ne, "Source_Text");
	  if (sourceText != null) {
		  for (String fileName : sourceText) {
			  if (fileName.endsWith(".h")) {
				  headerFiles.add(fileName);
			  }
			  if (fileName.endsWith(".c")) {
				  sourceFiles.add(fileName);
			  }
		  }
	  }
	  return result; 
  }
  
}