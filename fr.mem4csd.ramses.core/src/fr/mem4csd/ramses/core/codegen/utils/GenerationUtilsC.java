/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen.utils ;

import java.util.Collection;
import java.util.HashMap;
import java.util.List ;
import java.util.Map;
import java.util.Set ;

import org.apache.log4j.Logger ;
import org.osate.aadl2.BehavioredImplementation;
import org.osate.aadl2.BooleanLiteral ;
import org.osate.aadl2.ComponentImplementation ;
import org.osate.aadl2.ComponentType ;
import org.osate.aadl2.DataClassifier;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.NamedElement ;
import org.osate.aadl2.Parameter ;
import org.osate.aadl2.Property ;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.Subprogram;
import org.osate.aadl2.SubprogramCall;
import org.osate.aadl2.SubprogramCallSequence;
import org.osate.aadl2.SubprogramImplementation;
import org.osate.aadl2.SubprogramType;
import org.osate.aadl2.modelsupport.util.AadlUtil;
import org.osate.utils.internal.PropertyUtils ;
import org.osate.xtext.aadl2.properties.util.GetProperties;

import fr.mem4csd.ramses.core.codegen.c.AadlToCSwitchProcess;

public class GenerationUtilsC
{
  public final static String THREAD_SUFFIX = "_Job" ;
  public final static String THREAD_INIT_SUFFIX = "_Init" ;
  
  private static Logger _LOGGER = Logger.getLogger(GenerationUtilsC.class) ;
  
  
  // Give file name, in upper case or not and with or without extension.
  public static String generateHeaderInclusionGuard(String fileName)
  {
    fileName = fileName.toUpperCase() ;
    fileName = fileName.replace('.', '_') ;
    
    StringBuilder result = new StringBuilder("#ifndef __GENERATED_") ;
    result.append(fileName);
    result.append("__\n#define __GENERATED_") ;
    result.append(fileName) ;
    result.append("__\n") ;
    
    return result.toString() ;
  }
  
  public static String getGenerationCIdentifier(Object owner, NamedElement elt,
		  Map<Object, Map<NamedElement, String>> identifierMappingWithContext) {
	  Object context;
	  if(owner == null)
		  context = elt;
	  else
		  context = owner;
	  
	  Map<NamedElement, String> neToIdMapWithContext = identifierMappingWithContext.get(context);
	  if(neToIdMapWithContext==null)
	  {
		  neToIdMapWithContext = new HashMap<NamedElement, String>();
		  identifierMappingWithContext.put(context, neToIdMapWithContext);
	  }
	  
	  return getGenerationCIdentifier(elt, neToIdMapWithContext);
  }
  
  private static String getGenerationCIdentifier(NamedElement ne, Map<NamedElement, String> neToIdMap)
  {
	if(ne instanceof DataClassifier)
		return getGenerationCIdentifier(ne.getQualifiedName());
	if(neToIdMap.containsKey(ne))
		return neToIdMap.get(ne);
	else
	{
		String id=getGenerationCIdentifier(ne.getName());
		if(neToIdMap.values().contains(id))
		{
			id = getNewUniqueId(id, ne, neToIdMap.values());
		}
		neToIdMap.put(ne, id);
		return id;
	}
  }
  
  private static String getNewUniqueId(String id, NamedElement ne, Collection<String> values) {
	if(ne.eContainer()!=null && ne.eContainer() instanceof NamedElement)
	{
		NamedElement parent = (NamedElement) ne.eContainer();
		String newId = getGenerationCIdentifier(parent.getName())+"_"+id;
		if(values.contains(newId))
			return getNewUniqueId(newId, parent, values);
		else
			return newId;
	}
	return null;
  }

  public static String getGenerationCIdentifier(String id) {
	return id.replaceAll("::", "__").replace('[', '_').replace(']', '_').replace('.', '_') ;
  }

  public static String generateSectionTitle(String object)
  {
    checkSectionObject(object) ;
    int maxChar = 80 ;
    char spacer = ' ' ;
    StringBuilder sb = new StringBuilder() ;
    int titleChar = maxChar - object.length() - 8 ;
    int nbStars = titleChar / 2 ;
    boolean symetric = (titleChar % 2) == 0 ;
    sb.append("/* ") ;

    for(int i = 0 ; i < nbStars ; i++)
    {
      sb.append(spacer) ;
    }

    sb.append(' ') ;
    sb.append(object) ;
    sb.append(' ') ;

    if(false == symetric)
    {
      nbStars++ ;
    }

    for(int i = 0 ; i < nbStars ; i++)
    {
      sb.append(spacer) ;
    }

    sb.append(" */\n") ;
    return sb.toString() ;
  }

  public static String generateSectionComment(String comment)
  {
    checkSectionObject(comment) ;
    int maxChar = 80 ;
    char spacer = ' ' ;
    StringBuilder sb = new StringBuilder() ;
    int titleChar = comment.length() + 4 ;
    sb.append("/* ") ;
    sb.append(comment) ;
    sb.append(' ') ;

    for(int i = titleChar ; i < maxChar - 3 ; i++)
    {
      sb.append(spacer) ;
    }

    sb.append(" */") ;
    return sb.toString() ;
  }

  private static void checkSectionObject(String object)
  {
    if(object.length() > 74) // 80 - 6 length of /*_ x 2
    {
      String errorMsg = "title more than 78 characters" ;
      _LOGGER.fatal(errorMsg);
      throw new UnsupportedOperationException(errorMsg) ;
    }
  }

  public static String generateSectionMark()
  {
    return "\n/******************************************************************************/" ;
  }
   
  public static boolean usesOperation(BehavioredImplementation bi, 
		  String subprogramName)
  {
	  for(SubprogramCallSequence scs: bi.getOwnedSubprogramCallSequences())
	  {
		  for(SubprogramCall sc: scs.getOwnedSubprogramCalls())
		  {
			  Subprogram s = (Subprogram) sc.getCalledSubprogram();
			  if(s.getName().equals(subprogramName))
			  {
				  return true;
			  }
			  else if(s instanceof SubprogramType)
			  {
				  SubprogramType st = (SubprogramType) s;
				  if(extendsSubprogramType(st,subprogramName))
					  return true;
			  }
			  else if(s instanceof SubprogramImplementation)
			  {
				  SubprogramImplementation si = (SubprogramImplementation) s;
				  if(extendsSubprogramImpl(si,subprogramName))
					  return true;
			  }
			  if(s instanceof BehavioredImplementation)
				  usesOperation((BehavioredImplementation) s, subprogramName);
		  }
	  }
	  return false;
  }

  private static boolean extendsSubprogramType(SubprogramType st, 
		  String subprogramName)
  {
	  if(st.getOwnedExtension()!=null)
	  {
		  SubprogramType parent = (SubprogramType) st.getOwnedExtension().getExtended();
		  if(parent.getName().equals(subprogramName))
		  {
			  return true;
		  }
		  else
			  return extendsSubprogramType(parent, subprogramName);
	  }
	  return false;
  }

  private static boolean extendsSubprogramImpl(SubprogramImplementation si, 
		  String subprogramName)
  {
	  if(si.getOwnedExtension()!=null)
	  {
		  SubprogramImplementation parent = (SubprogramImplementation) si.getOwnedExtension().getExtended();
		  if(parent.getName().equals(subprogramName))
		  {
			  return true;
		  }
		  else
			  return extendsSubprogramImpl(parent, subprogramName);
	  }
	  return false;
  }
  
  
  public static boolean isReturnParameter(Parameter p)
  {
    Boolean isReturnParam = PropertyUtils.getBooleanValue(p, "Return_Parameter") ;
    if(isReturnParam == null)
    {
      isReturnParam=false;
      
      Property prop = GetProperties.lookupPropertyDefinition(p, "Code_Generation_Properties", "Return_Parameter") ;
      BooleanLiteral bl = (BooleanLiteral) prop.getDefaultValue() ;
      isReturnParam = bl.getValue();
    }
    
	return isReturnParam;
  }
  
  public static String resolveExistingCodeDependencies(NamedElement object,
                                                       Set<String> additionalHeaders)
  {
    try
    {
      NamedElement ne = object ;
      List<String> sourceText =
                                PropertyUtils.getStringListValue(ne,
                                                                 "Source_Text") ;
      for(String s : sourceText)
      {
        if(s.endsWith(".h"))
        {
          additionalHeaders.add(s) ;
        }
      }
      
      String sourceName = PropertyUtils.getStringValue(ne, "Source_Name") ;
      
      _LOGGER.trace("sourceName : " + sourceName);
      
      return sourceName ;
    }
    catch(Exception e)
    {
      if(object instanceof ComponentType)
      {
        ComponentType c = (ComponentType) object ;
        if(c.getOwnedExtension() != null)
          return resolveExistingCodeDependencies(c.getOwnedExtension()
                                                  .getExtended(),
                                                 additionalHeaders) ;
      }
      /*else   FIXME: ComponentPrototype */
      else if(object instanceof ComponentImplementation)
      {
        ComponentImplementation ci = (ComponentImplementation) object ;
        if(ci.getOwnedExtension() != null)
          return resolveExistingCodeDependencies(ci.getOwnedExtension()
                                                   .getExtended(),
                                                 additionalHeaders) ;
      }
      return null ;
    }
  }

  public static boolean isProcessor(NamedElement entry) {
	  boolean isProcessorProperty = false;
	  PropertyAssociation pa=AadlUtil.findOwnedPropertyAssociation(entry, "Is_Processor");
	  if(pa != null)
	  {
		  ModalPropertyValue mpv = pa.getOwnedValues().get(0);
		  BooleanLiteral bl = (BooleanLiteral) mpv.getOwnedValue();
		  isProcessorProperty = bl.getValue();
	  }
	  return isProcessorProperty;
  }
  
  public static String getAdditionalHeader(AadlToCSwitchProcess fileUnparser, Map<AadlToCSwitchProcess, Set<String>> additionalHeaders) {
		
		if (additionalHeaders.containsKey(fileUnparser)) {
			Set<String> additionalTypeHeaders = additionalHeaders.get(fileUnparser);
			return getAdditionalHeader(additionalTypeHeaders);
		}
		return "";
		
	}

  public static String getAdditionalHeader(Collection<String> additionalHeaders) {
	  StringBuffer res = new StringBuffer("");
	  for (String s : additionalHeaders) {
			res.append("#include \"" + s + "\"\n");
		}
	  additionalHeaders.clear();
	  return res.toString();
  }
}