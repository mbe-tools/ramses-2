/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.codegen;

import java.util.List ;

import org.osate.aadl2.DirectionType ;
import org.osate.aadl2.instance.ComponentInstance ;
import org.osate.aadl2.instance.FeatureInstance ;

public class GenerationInfos
{

  public static class GlobalQueueInfo
  {
    public String id = null;
    
    public ComponentInstance component ;

  }

  public static class QueueInfo
  {    
    public GlobalQueueInfo gQueue = null;

    public List<FeatureInstance> portList ;

    public Long timeOut ;

    public String id = null ;

    public String uniqueId = null;

    public long size = -1 ;

    public String type = null ;

    public String dataType = null;

    public DirectionType direction = null ;

    public int threadPortId = -1 ;

  }

  public static class SampleInfo
  {
    public String id = null ;

    public String uniqueId = null;

    public long refresh = -1 ;

    public String dataType = null;

    public DirectionType direction = null ;
  }

}
