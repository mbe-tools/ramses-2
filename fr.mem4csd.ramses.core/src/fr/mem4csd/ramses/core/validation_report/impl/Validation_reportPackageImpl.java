/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.validation_report.impl;

import fr.mem4csd.ramses.core.validation_report.Validation_reportFactory;
import fr.mem4csd.ramses.core.validation_report.Validation_reportPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Validation_reportPackageImpl extends EPackageImpl implements Validation_reportPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass errorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.ramses.core.validation_report.Validation_reportPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Validation_reportPackageImpl() {
		super(eNS_URI, Validation_reportFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link Validation_reportPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Validation_reportPackage init() {
		if (isInited) return (Validation_reportPackage)EPackage.Registry.INSTANCE.getEPackage(Validation_reportPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredValidation_reportPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		Validation_reportPackageImpl theValidation_reportPackage = registeredValidation_reportPackage instanceof Validation_reportPackageImpl ? (Validation_reportPackageImpl)registeredValidation_reportPackage : new Validation_reportPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theValidation_reportPackage.createPackageContents();

		// Initialize created meta-data
		theValidation_reportPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theValidation_reportPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Validation_reportPackage.eNS_URI, theValidation_reportPackage);
		return theValidation_reportPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getError() {
		return errorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getError_Message() {
		return (EAttribute)errorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getError_Object() {
		return (EReference)errorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Validation_reportFactory getValidation_reportFactory() {
		return (Validation_reportFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		errorEClass = createEClass(ERROR);
		createEAttribute(errorEClass, ERROR__MESSAGE);
		createEReference(errorEClass, ERROR__OBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(errorEClass, fr.mem4csd.ramses.core.validation_report.Error.class, "Error", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getError_Message(), ecorePackage.getEString(), "message", null, 1, 1, fr.mem4csd.ramses.core.validation_report.Error.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getError_Object(), ecorePackage.getEObject(), null, "object", null, 1, 1, fr.mem4csd.ramses.core.validation_report.Error.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Validation_reportPackageImpl
