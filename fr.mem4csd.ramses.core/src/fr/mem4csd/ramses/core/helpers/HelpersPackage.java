/**
 */
package fr.mem4csd.ramses.core.helpers;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.core.helpers.HelpersFactory
 * @model kind="package"
 * @generated
 */
public interface HelpersPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "helpers";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/ramses/helpers";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "helpers";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	HelpersPackage eINSTANCE = fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.helpers.impl.AtlHelperImpl <em>Atl Helper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.helpers.impl.AtlHelperImpl
	 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getAtlHelper()
	 * @generated
	 */
	int ATL_HELPER = 0;

	/**
	 * The feature id for the '<em><b>Output Package Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER__OUTPUT_PACKAGE_NAME = 0;

	/**
	 * The number of structural features of the '<em>Atl Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Order Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___ORDER_FEATURES__COMPONENTTYPE = 0;

	/**
	 * The operation id for the '<em>Copy Location Reference</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___COPY_LOCATION_REFERENCE__ELEMENT_ELEMENT = 1;

	/**
	 * The operation id for the '<em>Get Current Perion Read Table</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_CURRENT_PERION_READ_TABLE__FEATUREINSTANCE = 2;

	/**
	 * The operation id for the '<em>Get Current Deadline Write Table</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_CURRENT_DEADLINE_WRITE_TABLE__FEATUREINSTANCE_FEATUREINSTANCE = 3;

	/**
	 * The operation id for the '<em>Get Buffer Size</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_BUFFER_SIZE__FEATUREINSTANCE = 4;

	/**
	 * The operation id for the '<em>Set Direction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___SET_DIRECTION__DIRECTEDFEATURE_STRING = 5;

	/**
	 * The operation id for the '<em>Is Used In Special Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___IS_USED_IN_SPECIAL_OPERATOR__BEHAVIORANNEX_PORT_STRING = 6;

	/**
	 * The operation id for the '<em>Get Hyperperiod</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_HYPERPERIOD__FEATUREINSTANCE = 7;

	/**
	 * The operation id for the '<em>Get Timing Precision</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_TIMING_PRECISION__NAMEDELEMENT = 8;

	/**
	 * The operation id for the '<em>Get List Of Path</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_LIST_OF_PATH__PROPERTYASSOCIATION = 9;

	/**
	 * The operation id for the '<em>All Port Count</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___ALL_PORT_COUNT__BEHAVIORELEMENT = 10;

	/**
	 * The operation id for the '<em>Get String Literal</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_STRING_LITERAL__PROPERTYASSOCIATION_STRING = 11;

	/**
	 * The operation id for the '<em>Get Enumerators</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_ENUMERATORS__CLASSIFIER = 12;

	/**
	 * The operation id for the '<em>Unique Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___UNIQUE_NAME__NAMEDELEMENT_NAMEDELEMENT = 13;

	/**
	 * The operation id for the '<em>Get Hyperperiod From Threads</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_HYPERPERIOD_FROM_THREADS__ELIST_STRING = 14;

	/**
	 * The operation id for the '<em>Get Sched Table Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_SCHED_TABLE_INIT__COMPONENTINSTANCE_MODEINSTANCE = 15;

	/**
	 * The operation id for the '<em>Deployed On Transformed Cpu</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___DEPLOYED_ON_TRANSFORMED_CPU__COMPONENTINSTANCE = 16;

	/**
	 * The operation id for the '<em>Get Cpu To Transform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_CPU_TO_TRANSFORM = 17;

	/**
	 * The operation id for the '<em>Reset Cpu To Transform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___RESET_CPU_TO_TRANSFORM__ELIST = 18;

	/**
	 * The operation id for the '<em>Get Output Package Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER___GET_OUTPUT_PACKAGE_NAME__STRING = 19;

	/**
	 * The number of operations of the '<em>Atl Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATL_HELPER_OPERATION_COUNT = 20;


	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.helpers.impl.MathHelperImpl <em>Math Helper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.helpers.impl.MathHelperImpl
	 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getMathHelper()
	 * @generated
	 */
	int MATH_HELPER = 1;

	/**
	 * The number of structural features of the '<em>Math Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATH_HELPER_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Math Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATH_HELPER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl <em>Aadl Helper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl
	 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getAadlHelper()
	 * @generated
	 */
	int AADL_HELPER = 2;

	/**
	 * The number of structural features of the '<em>Aadl Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_HELPER_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Aadl Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_HELPER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.helpers.impl.CodeGenHelperImpl <em>Code Gen Helper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.helpers.impl.CodeGenHelperImpl
	 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getCodeGenHelper()
	 * @generated
	 */
	int CODE_GEN_HELPER = 3;

	/**
	 * The number of structural features of the '<em>Code Gen Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_GEN_HELPER_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Code Gen Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_GEN_HELPER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl <em>Communication Dimensioning Helper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl
	 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getCommunicationDimensioningHelper()
	 * @generated
	 */
	int COMMUNICATION_DIMENSIONING_HELPER = 4;

	/**
	 * The feature id for the '<em><b>Reader Receiving Task Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER__READER_RECEIVING_TASK_INSTANCE = 0;

	/**
	 * The feature id for the '<em><b>Writer Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER__WRITER_INSTANCES = 1;

	/**
	 * The feature id for the '<em><b>Writer Feature Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER__WRITER_FEATURE_INSTANCES = 2;

	/**
	 * The feature id for the '<em><b>Cpr Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER__CPR_SIZE = 3;

	/**
	 * The feature id for the '<em><b>Cdw Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER__CDW_SIZE = 4;

	/**
	 * The feature id for the '<em><b>Current Period Read</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER__CURRENT_PERIOD_READ = 5;

	/**
	 * The feature id for the '<em><b>Current Deadline Write Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER__CURRENT_DEADLINE_WRITE_MAP = 6;

	/**
	 * The feature id for the '<em><b>Buffer Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER__BUFFER_SIZE = 7;

	/**
	 * The feature id for the '<em><b>Hyperperiod</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER__HYPERPERIOD = 8;

	/**
	 * The number of structural features of the '<em>Communication Dimensioning Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER_FEATURE_COUNT = 9;

	/**
	 * The operation id for the '<em>Get Cdw Size</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER___GET_CDW_SIZE__FEATUREINSTANCE = 0;

	/**
	 * The operation id for the '<em>Get Current Deadline Write Index</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER___GET_CURRENT_DEADLINE_WRITE_INDEX__FEATUREINSTANCE_INT = 1;

	/**
	 * The operation id for the '<em>Get Current Period Read Index</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER___GET_CURRENT_PERIOD_READ_INDEX__INT = 2;

	/**
	 * The number of operations of the '<em>Communication Dimensioning Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DIMENSIONING_HELPER_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.helpers.impl.EventDataPortComDimHelperImpl <em>Event Data Port Com Dim Helper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.helpers.impl.EventDataPortComDimHelperImpl
	 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getEventDataPortComDimHelper()
	 * @generated
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER = 5;

	/**
	 * The feature id for the '<em><b>Reader Receiving Task Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER__READER_RECEIVING_TASK_INSTANCE = COMMUNICATION_DIMENSIONING_HELPER__READER_RECEIVING_TASK_INSTANCE;

	/**
	 * The feature id for the '<em><b>Writer Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER__WRITER_INSTANCES = COMMUNICATION_DIMENSIONING_HELPER__WRITER_INSTANCES;

	/**
	 * The feature id for the '<em><b>Writer Feature Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER__WRITER_FEATURE_INSTANCES = COMMUNICATION_DIMENSIONING_HELPER__WRITER_FEATURE_INSTANCES;

	/**
	 * The feature id for the '<em><b>Cpr Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER__CPR_SIZE = COMMUNICATION_DIMENSIONING_HELPER__CPR_SIZE;

	/**
	 * The feature id for the '<em><b>Cdw Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER__CDW_SIZE = COMMUNICATION_DIMENSIONING_HELPER__CDW_SIZE;

	/**
	 * The feature id for the '<em><b>Current Period Read</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER__CURRENT_PERIOD_READ = COMMUNICATION_DIMENSIONING_HELPER__CURRENT_PERIOD_READ;

	/**
	 * The feature id for the '<em><b>Current Deadline Write Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER__CURRENT_DEADLINE_WRITE_MAP = COMMUNICATION_DIMENSIONING_HELPER__CURRENT_DEADLINE_WRITE_MAP;

	/**
	 * The feature id for the '<em><b>Buffer Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER__BUFFER_SIZE = COMMUNICATION_DIMENSIONING_HELPER__BUFFER_SIZE;

	/**
	 * The feature id for the '<em><b>Hyperperiod</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER__HYPERPERIOD = COMMUNICATION_DIMENSIONING_HELPER__HYPERPERIOD;

	/**
	 * The number of structural features of the '<em>Event Data Port Com Dim Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER_FEATURE_COUNT = COMMUNICATION_DIMENSIONING_HELPER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Cdw Size</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER___GET_CDW_SIZE__FEATUREINSTANCE = COMMUNICATION_DIMENSIONING_HELPER___GET_CDW_SIZE__FEATUREINSTANCE;

	/**
	 * The operation id for the '<em>Get Current Deadline Write Index</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER___GET_CURRENT_DEADLINE_WRITE_INDEX__FEATUREINSTANCE_INT = COMMUNICATION_DIMENSIONING_HELPER___GET_CURRENT_DEADLINE_WRITE_INDEX__FEATUREINSTANCE_INT;

	/**
	 * The operation id for the '<em>Get Current Period Read Index</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER___GET_CURRENT_PERIOD_READ_INDEX__INT = COMMUNICATION_DIMENSIONING_HELPER___GET_CURRENT_PERIOD_READ_INDEX__INT;

	/**
	 * The number of operations of the '<em>Event Data Port Com Dim Helper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DATA_PORT_COM_DIM_HELPER_OPERATION_COUNT = COMMUNICATION_DIMENSIONING_HELPER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '<em>Ramses Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.helpers.impl.RamsesException
	 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getRamsesException()
	 * @generated
	 */
	int RAMSES_EXCEPTION = 6;

	/**
	 * The meta object id for the '<em>Dimensioning Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.helpers.impl.DimensioningException
	 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getDimensioningException()
	 * @generated
	 */
	int DIMENSIONING_EXCEPTION = 7;

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.helpers.AtlHelper <em>Atl Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atl Helper</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper
	 * @generated
	 */
	EClass getAtlHelper();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getOutputPackageName <em>Output Package Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Package Name</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getOutputPackageName()
	 * @see #getAtlHelper()
	 * @generated
	 */
	EAttribute getAtlHelper_OutputPackageName();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#orderFeatures(org.osate.aadl2.ComponentType) <em>Order Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Order Features</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#orderFeatures(org.osate.aadl2.ComponentType)
	 * @generated
	 */
	EOperation getAtlHelper__OrderFeatures__ComponentType();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#copyLocationReference(org.osate.aadl2.Element, org.osate.aadl2.Element) <em>Copy Location Reference</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Copy Location Reference</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#copyLocationReference(org.osate.aadl2.Element, org.osate.aadl2.Element)
	 * @generated
	 */
	EOperation getAtlHelper__CopyLocationReference__Element_Element();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getCurrentPerionReadTable(org.osate.aadl2.instance.FeatureInstance) <em>Get Current Perion Read Table</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Current Perion Read Table</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getCurrentPerionReadTable(org.osate.aadl2.instance.FeatureInstance)
	 * @generated
	 */
	EOperation getAtlHelper__GetCurrentPerionReadTable__FeatureInstance();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getCurrentDeadlineWriteTable(org.osate.aadl2.instance.FeatureInstance, org.osate.aadl2.instance.FeatureInstance) <em>Get Current Deadline Write Table</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Current Deadline Write Table</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getCurrentDeadlineWriteTable(org.osate.aadl2.instance.FeatureInstance, org.osate.aadl2.instance.FeatureInstance)
	 * @generated
	 */
	EOperation getAtlHelper__GetCurrentDeadlineWriteTable__FeatureInstance_FeatureInstance();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getBufferSize(org.osate.aadl2.instance.FeatureInstance) <em>Get Buffer Size</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Buffer Size</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getBufferSize(org.osate.aadl2.instance.FeatureInstance)
	 * @generated
	 */
	EOperation getAtlHelper__GetBufferSize__FeatureInstance();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#setDirection(org.osate.aadl2.DirectedFeature, java.lang.String) <em>Set Direction</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Direction</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#setDirection(org.osate.aadl2.DirectedFeature, java.lang.String)
	 * @generated
	 */
	EOperation getAtlHelper__SetDirection__DirectedFeature_String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#isUsedInSpecialOperator(org.osate.ba.aadlba.BehaviorAnnex, org.osate.aadl2.Port, java.lang.String) <em>Is Used In Special Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Used In Special Operator</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#isUsedInSpecialOperator(org.osate.ba.aadlba.BehaviorAnnex, org.osate.aadl2.Port, java.lang.String)
	 * @generated
	 */
	EOperation getAtlHelper__IsUsedInSpecialOperator__BehaviorAnnex_Port_String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getHyperperiod(org.osate.aadl2.instance.FeatureInstance) <em>Get Hyperperiod</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Hyperperiod</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getHyperperiod(org.osate.aadl2.instance.FeatureInstance)
	 * @generated
	 */
	EOperation getAtlHelper__GetHyperperiod__FeatureInstance();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getTimingPrecision(org.osate.aadl2.NamedElement) <em>Get Timing Precision</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Timing Precision</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getTimingPrecision(org.osate.aadl2.NamedElement)
	 * @generated
	 */
	EOperation getAtlHelper__GetTimingPrecision__NamedElement();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getListOfPath(org.osate.aadl2.PropertyAssociation) <em>Get List Of Path</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get List Of Path</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getListOfPath(org.osate.aadl2.PropertyAssociation)
	 * @generated
	 */
	EOperation getAtlHelper__GetListOfPath__PropertyAssociation();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#allPortCount(org.osate.ba.aadlba.BehaviorElement) <em>All Port Count</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Port Count</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#allPortCount(org.osate.ba.aadlba.BehaviorElement)
	 * @generated
	 */
	EOperation getAtlHelper__AllPortCount__BehaviorElement();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getStringLiteral(org.osate.aadl2.PropertyAssociation, java.lang.String) <em>Get String Literal</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get String Literal</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getStringLiteral(org.osate.aadl2.PropertyAssociation, java.lang.String)
	 * @generated
	 */
	EOperation getAtlHelper__GetStringLiteral__PropertyAssociation_String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getEnumerators(org.osate.aadl2.Classifier) <em>Get Enumerators</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Enumerators</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getEnumerators(org.osate.aadl2.Classifier)
	 * @generated
	 */
	EOperation getAtlHelper__GetEnumerators__Classifier();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#uniqueName(org.osate.aadl2.NamedElement, org.osate.aadl2.NamedElement) <em>Unique Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unique Name</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#uniqueName(org.osate.aadl2.NamedElement, org.osate.aadl2.NamedElement)
	 * @generated
	 */
	EOperation getAtlHelper__UniqueName__NamedElement_NamedElement();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getHyperperiodFromThreads(org.eclipse.emf.common.util.EList, java.lang.String) <em>Get Hyperperiod From Threads</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Hyperperiod From Threads</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getHyperperiodFromThreads(org.eclipse.emf.common.util.EList, java.lang.String)
	 * @generated
	 */
	EOperation getAtlHelper__GetHyperperiodFromThreads__EList_String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getSchedTableInit(org.osate.aadl2.instance.ComponentInstance, org.osate.aadl2.instance.ModeInstance) <em>Get Sched Table Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Sched Table Init</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getSchedTableInit(org.osate.aadl2.instance.ComponentInstance, org.osate.aadl2.instance.ModeInstance)
	 * @generated
	 */
	EOperation getAtlHelper__GetSchedTableInit__ComponentInstance_ModeInstance();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#deployedOnTransformedCpu(org.osate.aadl2.instance.ComponentInstance) <em>Deployed On Transformed Cpu</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Deployed On Transformed Cpu</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#deployedOnTransformedCpu(org.osate.aadl2.instance.ComponentInstance)
	 * @generated
	 */
	EOperation getAtlHelper__DeployedOnTransformedCpu__ComponentInstance();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getCpuToTransform() <em>Get Cpu To Transform</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Cpu To Transform</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getCpuToTransform()
	 * @generated
	 */
	EOperation getAtlHelper__GetCpuToTransform();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#resetCpuToTransform(org.eclipse.emf.common.util.EList) <em>Reset Cpu To Transform</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset Cpu To Transform</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#resetCpuToTransform(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getAtlHelper__ResetCpuToTransform__EList();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getOutputPackageName(java.lang.String) <em>Get Output Package Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Output Package Name</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper#getOutputPackageName(java.lang.String)
	 * @generated
	 */
	EOperation getAtlHelper__GetOutputPackageName__String();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.helpers.MathHelper <em>Math Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Math Helper</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.MathHelper
	 * @generated
	 */
	EClass getMathHelper();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.helpers.AadlHelper <em>Aadl Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aadl Helper</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.AadlHelper
	 * @generated
	 */
	EClass getAadlHelper();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.helpers.CodeGenHelper <em>Code Gen Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code Gen Helper</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.CodeGenHelper
	 * @generated
	 */
	EClass getCodeGenHelper();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper <em>Communication Dimensioning Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Dimensioning Helper</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper
	 * @generated
	 */
	EClass getCommunicationDimensioningHelper();

	/**
	 * Returns the meta object for the reference '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getReaderReceivingTaskInstance <em>Reader Receiving Task Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reader Receiving Task Instance</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getReaderReceivingTaskInstance()
	 * @see #getCommunicationDimensioningHelper()
	 * @generated
	 */
	EReference getCommunicationDimensioningHelper_ReaderReceivingTaskInstance();

	/**
	 * Returns the meta object for the reference list '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getWriterInstances <em>Writer Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Writer Instances</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getWriterInstances()
	 * @see #getCommunicationDimensioningHelper()
	 * @generated
	 */
	EReference getCommunicationDimensioningHelper_WriterInstances();

	/**
	 * Returns the meta object for the reference list '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getWriterFeatureInstances <em>Writer Feature Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Writer Feature Instances</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getWriterFeatureInstances()
	 * @see #getCommunicationDimensioningHelper()
	 * @generated
	 */
	EReference getCommunicationDimensioningHelper_WriterFeatureInstances();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCprSize <em>Cpr Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cpr Size</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCprSize()
	 * @see #getCommunicationDimensioningHelper()
	 * @generated
	 */
	EAttribute getCommunicationDimensioningHelper_CprSize();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCdwSize <em>Cdw Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cdw Size</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCdwSize()
	 * @see #getCommunicationDimensioningHelper()
	 * @generated
	 */
	EAttribute getCommunicationDimensioningHelper_CdwSize();

	/**
	 * Returns the meta object for the attribute list '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCurrentPeriodRead <em>Current Period Read</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Current Period Read</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCurrentPeriodRead()
	 * @see #getCommunicationDimensioningHelper()
	 * @generated
	 */
	EAttribute getCommunicationDimensioningHelper_CurrentPeriodRead();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCurrentDeadlineWriteMap <em>Current Deadline Write Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Current Deadline Write Map</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCurrentDeadlineWriteMap()
	 * @see #getCommunicationDimensioningHelper()
	 * @generated
	 */
	EAttribute getCommunicationDimensioningHelper_CurrentDeadlineWriteMap();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getBufferSize <em>Buffer Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Buffer Size</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getBufferSize()
	 * @see #getCommunicationDimensioningHelper()
	 * @generated
	 */
	EAttribute getCommunicationDimensioningHelper_BufferSize();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getHyperperiod <em>Hyperperiod</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hyperperiod</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getHyperperiod()
	 * @see #getCommunicationDimensioningHelper()
	 * @generated
	 */
	EAttribute getCommunicationDimensioningHelper_Hyperperiod();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCdwSize(org.osate.aadl2.instance.FeatureInstance) <em>Get Cdw Size</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Cdw Size</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCdwSize(org.osate.aadl2.instance.FeatureInstance)
	 * @generated
	 */
	EOperation getCommunicationDimensioningHelper__GetCdwSize__FeatureInstance();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCurrentDeadlineWriteIndex(org.osate.aadl2.instance.FeatureInstance, int) <em>Get Current Deadline Write Index</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Current Deadline Write Index</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCurrentDeadlineWriteIndex(org.osate.aadl2.instance.FeatureInstance, int)
	 * @generated
	 */
	EOperation getCommunicationDimensioningHelper__GetCurrentDeadlineWriteIndex__FeatureInstance_int();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCurrentPeriodReadIndex(int) <em>Get Current Period Read Index</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Current Period Read Index</em>' operation.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCurrentPeriodReadIndex(int)
	 * @generated
	 */
	EOperation getCommunicationDimensioningHelper__GetCurrentPeriodReadIndex__int();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.helpers.EventDataPortComDimHelper <em>Event Data Port Com Dim Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Data Port Com Dim Helper</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.EventDataPortComDimHelper
	 * @generated
	 */
	EClass getEventDataPortComDimHelper();

	/**
	 * Returns the meta object for data type '{@link fr.mem4csd.ramses.core.helpers.impl.RamsesException <em>Ramses Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Ramses Exception</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.impl.RamsesException
	 * @model instanceClass="fr.mem4csd.ramses.core.helpers.impl.RamsesException"
	 * @generated
	 */
	EDataType getRamsesException();

	/**
	 * Returns the meta object for data type '{@link fr.mem4csd.ramses.core.helpers.impl.DimensioningException <em>Dimensioning Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Dimensioning Exception</em>'.
	 * @see fr.mem4csd.ramses.core.helpers.impl.DimensioningException
	 * @model instanceClass="fr.mem4csd.ramses.core.helpers.impl.DimensioningException"
	 * @generated
	 */
	EDataType getDimensioningException();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	HelpersFactory getHelpersFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.helpers.impl.AtlHelperImpl <em>Atl Helper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.helpers.impl.AtlHelperImpl
		 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getAtlHelper()
		 * @generated
		 */
		EClass ATL_HELPER = eINSTANCE.getAtlHelper();

		/**
		 * The meta object literal for the '<em><b>Output Package Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATL_HELPER__OUTPUT_PACKAGE_NAME = eINSTANCE.getAtlHelper_OutputPackageName();

		/**
		 * The meta object literal for the '<em><b>Order Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___ORDER_FEATURES__COMPONENTTYPE = eINSTANCE.getAtlHelper__OrderFeatures__ComponentType();

		/**
		 * The meta object literal for the '<em><b>Copy Location Reference</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___COPY_LOCATION_REFERENCE__ELEMENT_ELEMENT = eINSTANCE.getAtlHelper__CopyLocationReference__Element_Element();

		/**
		 * The meta object literal for the '<em><b>Get Current Perion Read Table</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_CURRENT_PERION_READ_TABLE__FEATUREINSTANCE = eINSTANCE.getAtlHelper__GetCurrentPerionReadTable__FeatureInstance();

		/**
		 * The meta object literal for the '<em><b>Get Current Deadline Write Table</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_CURRENT_DEADLINE_WRITE_TABLE__FEATUREINSTANCE_FEATUREINSTANCE = eINSTANCE.getAtlHelper__GetCurrentDeadlineWriteTable__FeatureInstance_FeatureInstance();

		/**
		 * The meta object literal for the '<em><b>Get Buffer Size</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_BUFFER_SIZE__FEATUREINSTANCE = eINSTANCE.getAtlHelper__GetBufferSize__FeatureInstance();

		/**
		 * The meta object literal for the '<em><b>Set Direction</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___SET_DIRECTION__DIRECTEDFEATURE_STRING = eINSTANCE.getAtlHelper__SetDirection__DirectedFeature_String();

		/**
		 * The meta object literal for the '<em><b>Is Used In Special Operator</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___IS_USED_IN_SPECIAL_OPERATOR__BEHAVIORANNEX_PORT_STRING = eINSTANCE.getAtlHelper__IsUsedInSpecialOperator__BehaviorAnnex_Port_String();

		/**
		 * The meta object literal for the '<em><b>Get Hyperperiod</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_HYPERPERIOD__FEATUREINSTANCE = eINSTANCE.getAtlHelper__GetHyperperiod__FeatureInstance();

		/**
		 * The meta object literal for the '<em><b>Get Timing Precision</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_TIMING_PRECISION__NAMEDELEMENT = eINSTANCE.getAtlHelper__GetTimingPrecision__NamedElement();

		/**
		 * The meta object literal for the '<em><b>Get List Of Path</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_LIST_OF_PATH__PROPERTYASSOCIATION = eINSTANCE.getAtlHelper__GetListOfPath__PropertyAssociation();

		/**
		 * The meta object literal for the '<em><b>All Port Count</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___ALL_PORT_COUNT__BEHAVIORELEMENT = eINSTANCE.getAtlHelper__AllPortCount__BehaviorElement();

		/**
		 * The meta object literal for the '<em><b>Get String Literal</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_STRING_LITERAL__PROPERTYASSOCIATION_STRING = eINSTANCE.getAtlHelper__GetStringLiteral__PropertyAssociation_String();

		/**
		 * The meta object literal for the '<em><b>Get Enumerators</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_ENUMERATORS__CLASSIFIER = eINSTANCE.getAtlHelper__GetEnumerators__Classifier();

		/**
		 * The meta object literal for the '<em><b>Unique Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___UNIQUE_NAME__NAMEDELEMENT_NAMEDELEMENT = eINSTANCE.getAtlHelper__UniqueName__NamedElement_NamedElement();

		/**
		 * The meta object literal for the '<em><b>Get Hyperperiod From Threads</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_HYPERPERIOD_FROM_THREADS__ELIST_STRING = eINSTANCE.getAtlHelper__GetHyperperiodFromThreads__EList_String();

		/**
		 * The meta object literal for the '<em><b>Get Sched Table Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_SCHED_TABLE_INIT__COMPONENTINSTANCE_MODEINSTANCE = eINSTANCE.getAtlHelper__GetSchedTableInit__ComponentInstance_ModeInstance();

		/**
		 * The meta object literal for the '<em><b>Deployed On Transformed Cpu</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___DEPLOYED_ON_TRANSFORMED_CPU__COMPONENTINSTANCE = eINSTANCE.getAtlHelper__DeployedOnTransformedCpu__ComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Get Cpu To Transform</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_CPU_TO_TRANSFORM = eINSTANCE.getAtlHelper__GetCpuToTransform();

		/**
		 * The meta object literal for the '<em><b>Reset Cpu To Transform</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___RESET_CPU_TO_TRANSFORM__ELIST = eINSTANCE.getAtlHelper__ResetCpuToTransform__EList();

		/**
		 * The meta object literal for the '<em><b>Get Output Package Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATL_HELPER___GET_OUTPUT_PACKAGE_NAME__STRING = eINSTANCE.getAtlHelper__GetOutputPackageName__String();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.helpers.impl.MathHelperImpl <em>Math Helper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.helpers.impl.MathHelperImpl
		 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getMathHelper()
		 * @generated
		 */
		EClass MATH_HELPER = eINSTANCE.getMathHelper();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl <em>Aadl Helper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl
		 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getAadlHelper()
		 * @generated
		 */
		EClass AADL_HELPER = eINSTANCE.getAadlHelper();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.helpers.impl.CodeGenHelperImpl <em>Code Gen Helper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.helpers.impl.CodeGenHelperImpl
		 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getCodeGenHelper()
		 * @generated
		 */
		EClass CODE_GEN_HELPER = eINSTANCE.getCodeGenHelper();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl <em>Communication Dimensioning Helper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl
		 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getCommunicationDimensioningHelper()
		 * @generated
		 */
		EClass COMMUNICATION_DIMENSIONING_HELPER = eINSTANCE.getCommunicationDimensioningHelper();

		/**
		 * The meta object literal for the '<em><b>Reader Receiving Task Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_DIMENSIONING_HELPER__READER_RECEIVING_TASK_INSTANCE = eINSTANCE.getCommunicationDimensioningHelper_ReaderReceivingTaskInstance();

		/**
		 * The meta object literal for the '<em><b>Writer Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_DIMENSIONING_HELPER__WRITER_INSTANCES = eINSTANCE.getCommunicationDimensioningHelper_WriterInstances();

		/**
		 * The meta object literal for the '<em><b>Writer Feature Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_DIMENSIONING_HELPER__WRITER_FEATURE_INSTANCES = eINSTANCE.getCommunicationDimensioningHelper_WriterFeatureInstances();

		/**
		 * The meta object literal for the '<em><b>Cpr Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_DIMENSIONING_HELPER__CPR_SIZE = eINSTANCE.getCommunicationDimensioningHelper_CprSize();

		/**
		 * The meta object literal for the '<em><b>Cdw Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_DIMENSIONING_HELPER__CDW_SIZE = eINSTANCE.getCommunicationDimensioningHelper_CdwSize();

		/**
		 * The meta object literal for the '<em><b>Current Period Read</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_DIMENSIONING_HELPER__CURRENT_PERIOD_READ = eINSTANCE.getCommunicationDimensioningHelper_CurrentPeriodRead();

		/**
		 * The meta object literal for the '<em><b>Current Deadline Write Map</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_DIMENSIONING_HELPER__CURRENT_DEADLINE_WRITE_MAP = eINSTANCE.getCommunicationDimensioningHelper_CurrentDeadlineWriteMap();

		/**
		 * The meta object literal for the '<em><b>Buffer Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_DIMENSIONING_HELPER__BUFFER_SIZE = eINSTANCE.getCommunicationDimensioningHelper_BufferSize();

		/**
		 * The meta object literal for the '<em><b>Hyperperiod</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_DIMENSIONING_HELPER__HYPERPERIOD = eINSTANCE.getCommunicationDimensioningHelper_Hyperperiod();

		/**
		 * The meta object literal for the '<em><b>Get Cdw Size</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMMUNICATION_DIMENSIONING_HELPER___GET_CDW_SIZE__FEATUREINSTANCE = eINSTANCE.getCommunicationDimensioningHelper__GetCdwSize__FeatureInstance();

		/**
		 * The meta object literal for the '<em><b>Get Current Deadline Write Index</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMMUNICATION_DIMENSIONING_HELPER___GET_CURRENT_DEADLINE_WRITE_INDEX__FEATUREINSTANCE_INT = eINSTANCE.getCommunicationDimensioningHelper__GetCurrentDeadlineWriteIndex__FeatureInstance_int();

		/**
		 * The meta object literal for the '<em><b>Get Current Period Read Index</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMMUNICATION_DIMENSIONING_HELPER___GET_CURRENT_PERIOD_READ_INDEX__INT = eINSTANCE.getCommunicationDimensioningHelper__GetCurrentPeriodReadIndex__int();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.helpers.impl.EventDataPortComDimHelperImpl <em>Event Data Port Com Dim Helper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.helpers.impl.EventDataPortComDimHelperImpl
		 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getEventDataPortComDimHelper()
		 * @generated
		 */
		EClass EVENT_DATA_PORT_COM_DIM_HELPER = eINSTANCE.getEventDataPortComDimHelper();

		/**
		 * The meta object literal for the '<em>Ramses Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.helpers.impl.RamsesException
		 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getRamsesException()
		 * @generated
		 */
		EDataType RAMSES_EXCEPTION = eINSTANCE.getRamsesException();

		/**
		 * The meta object literal for the '<em>Dimensioning Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.helpers.impl.DimensioningException
		 * @see fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl#getDimensioningException()
		 * @generated
		 */
		EDataType DIMENSIONING_EXCEPTION = eINSTANCE.getDimensioningException();

	}

} //HelpersPackage
