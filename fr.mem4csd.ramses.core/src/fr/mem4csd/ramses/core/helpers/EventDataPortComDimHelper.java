/**
 */
package fr.mem4csd.ramses.core.helpers;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Data Port Com Dim Helper</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getEventDataPortComDimHelper()
 * @model
 * @generated
 */
public interface EventDataPortComDimHelper extends CommunicationDimensioningHelper {

} // EventDataPortComDimHelper
