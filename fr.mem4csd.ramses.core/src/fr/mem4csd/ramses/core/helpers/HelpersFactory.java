/**
 */
package fr.mem4csd.ramses.core.helpers;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage
 * @generated
 */
public interface HelpersFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	HelpersFactory eINSTANCE = fr.mem4csd.ramses.core.helpers.impl.HelpersFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Atl Helper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Atl Helper</em>'.
	 * @generated
	 */
	AtlHelper createAtlHelper();

	/**
	 * Returns a new object of class '<em>Math Helper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Math Helper</em>'.
	 * @generated
	 */
	MathHelper createMathHelper();

	/**
	 * Returns a new object of class '<em>Aadl Helper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aadl Helper</em>'.
	 * @generated
	 */
	AadlHelper createAadlHelper();

	/**
	 * Returns a new object of class '<em>Code Gen Helper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Code Gen Helper</em>'.
	 * @generated
	 */
	CodeGenHelper createCodeGenHelper();

	/**
	 * Returns a new object of class '<em>Event Data Port Com Dim Helper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Data Port Com Dim Helper</em>'.
	 * @generated
	 */
	EventDataPortComDimHelper createEventDataPortComDimHelper();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	HelpersPackage getHelpersPackage();

} //HelpersFactory
