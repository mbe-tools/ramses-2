/**
 */
package fr.mem4csd.ramses.core.helpers;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.Classifier;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.DirectedFeature;
import org.osate.aadl2.Element;
import org.osate.aadl2.Feature;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.Port;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.StringLiteral;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.ModeInstance;
import org.osate.ba.aadlba.BehaviorAnnex;
import org.osate.ba.aadlba.BehaviorElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Atl Helper</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getOutputPackageName <em>Output Package Name</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getAtlHelper()
 * @model
 * @generated
 */
public interface AtlHelper extends EObject {
	/**
	 * Returns the value of the '<em><b>Output Package Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Package Name</em>' attribute.
	 * @see #setOutputPackageName(String)
	 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getAtlHelper_OutputPackageName()
	 * @model required="true"
	 * @generated
	 */
	String getOutputPackageName();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.helpers.AtlHelper#getOutputPackageName <em>Output Package Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Package Name</em>' attribute.
	 * @see #getOutputPackageName()
	 * @generated
	 */
	void setOutputPackageName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<Feature> orderFeatures(ComponentType cpt);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model targetRequired="true" sourceRequired="true"
	 * @generated
	 */
	void copyLocationReference(Element target, Element source);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model portRequired="true"
	 * @generated
	 */
	EList<Long> getCurrentPerionReadTable(FeatureInstance port);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model portRequired="true"
	 * @generated
	 */
	EList<Long> getCurrentDeadlineWriteTable(FeatureInstance port, FeatureInstance destinationPort);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" destinationFeatureInstanceRequired="true"
	 * @generated
	 */
	long getBufferSize(FeatureInstance destinationFeatureInstance);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model featureRequired="true" directionDataType="org.osate.aadl2.String"
	 * @generated
	 */
	void setDirection(DirectedFeature feature, String direction);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.osate.aadl2.Boolean" required="true" baRequired="true" operatorNameDataType="org.osate.aadl2.String" operatorNameRequired="true"
	 * @generated
	 */
	boolean isUsedInSpecialOperator(BehaviorAnnex ba, Port p, String operatorName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model portRequired="true"
	 * @generated
	 */
	long getHyperperiod(FeatureInstance port);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.osate.aadl2.String" required="true" namedElementRequired="true"
	 * @generated
	 */
	String getTimingPrecision(NamedElement namedElement);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.osate.aadl2.String"
	 * @generated
	 */
	EList<String> getListOfPath(PropertyAssociation pa);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model eRequired="true"
	 * @generated
	 */
	EList<Port> allPortCount(BehaviorElement e);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" paRequired="true" stringLiteralValueDataType="org.osate.aadl2.String" stringLiteralValueRequired="true"
	 * @generated
	 */
	StringLiteral getStringLiteral(PropertyAssociation pa, String stringLiteralValue);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" classifierRequired="true"
	 * @generated
	 */
	PropertyAssociation getEnumerators(Classifier classifier);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model objRequired="true" contextRequired="true"
	 * @generated
	 */
	String uniqueName(NamedElement obj, NamedElement context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model consideredTasksMany="true" scaleDataType="org.osate.ba.aadlba.String"
	 * @generated
	 */
	Long getHyperperiodFromThreads(EList<ComponentInstance> consideredTasks, String scale);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	String getSchedTableInit(ComponentInstance c, ModeInstance mi);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" cRequired="true"
	 * @generated
	 */
	boolean deployedOnTransformedCpu(ComponentInstance c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<ComponentInstance> getCpuToTransform();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cpuListMany="true"
	 * @generated
	 */
	void resetCpuToTransform(EList<ComponentInstance> cpuList);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" suffixRequired="true"
	 * @generated
	 */
	String getOutputPackageName(String suffix);

} // AtlHelper
