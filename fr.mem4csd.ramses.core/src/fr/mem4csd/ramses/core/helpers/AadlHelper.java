/**
 */
package fr.mem4csd.ramses.core.helpers;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aadl Helper</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getAadlHelper()
 * @model
 * @generated
 */
public interface AadlHelper extends EObject {
} // AadlHelper
