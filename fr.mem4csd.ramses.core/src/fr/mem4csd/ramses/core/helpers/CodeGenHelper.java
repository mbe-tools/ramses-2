/**
 */
package fr.mem4csd.ramses.core.helpers;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Code Gen Helper</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getCodeGenHelper()
 * @model
 * @generated
 */
public interface CodeGenHelper extends EObject {
} // CodeGenHelper
