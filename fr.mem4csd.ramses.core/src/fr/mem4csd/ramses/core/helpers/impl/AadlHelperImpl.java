/**
 */
package fr.mem4csd.ramses.core.helpers.impl;

import fr.mem4csd.ramses.core.helpers.AadlHelper;
import fr.mem4csd.ramses.core.helpers.HelpersPackage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.osate.aadl2.ArrayDimension;
import org.osate.aadl2.ArraySize;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.DataPort;
import org.osate.aadl2.DirectionType;
import org.osate.aadl2.Element;
import org.osate.aadl2.EventDataPort;
import org.osate.aadl2.IntegerLiteral;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.NumberValue;
import org.osate.aadl2.ProcessImplementation;
import org.osate.aadl2.ProcessSubcomponent;
import org.osate.aadl2.ProcessSubcomponentType;
import org.osate.aadl2.Processor;
import org.osate.aadl2.ProcessorSubcomponent;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.RangeValue;
import org.osate.aadl2.RealLiteral;
import org.osate.aadl2.RecordValue;
import org.osate.aadl2.ReferenceValue;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.Subprogram;
import org.osate.aadl2.SubprogramCall;
import org.osate.aadl2.SubprogramCallSequence;
import org.osate.aadl2.System;
import org.osate.aadl2.SystemImplementation;
import org.osate.aadl2.SystemSubcomponent;
import org.osate.aadl2.SystemSubcomponentType;
import org.osate.aadl2.ThreadSubcomponent;
import org.osate.aadl2.UnitLiteral;
import org.osate.aadl2.VirtualProcessorSubcomponent;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionKind;
import org.osate.aadl2.instance.FeatureCategory;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.utils.internal.PropertyUtils;
import org.osate.xtext.aadl2.properties.util.AadlProject;
import org.osate.xtext.aadl2.properties.util.GetProperties;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aadl Helper</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AadlHelperImpl extends MinimalEObjectImpl.Container implements AadlHelper {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AadlHelperImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HelpersPackage.Literals.AADL_HELPER;
	}

	private static Logger _LOGGER = Logger.getLogger(AadlHelperImpl.class) ;
	
	private final static String UTILIZATION = "utilization";
	private final static String RELIABILITY = "reliability";

	public enum DispatchProtocol {
		Periodic, Aperiodic, Sporadic, Timed, Hybrid, Background, Unknown
	};

	public static ComponentInstance getHighestPeriodTask(ComponentInstance process) {
		ComponentInstance result = null;
		long period = -1l;

		for (ComponentInstance c : process.getComponentInstances()) {
			if (c.getCategory() == ComponentCategory.THREAD) {
				long period2 = getInfoTaskPeriod(c);

				if (period2 > period) {
					period = period2;
					result = c;
				}
			}
		}
		return result;
	}

	public static ComponentInstance getInfoTaskCPU(ComponentInstance thread) {
		return getInfoProcessCPU((ComponentInstance) thread.getOwner());
	}

	public static ComponentInstance getInfoProcessCPU(ComponentInstance process) {
		PropertyExpression val = PropertyUtils.getPropertyValue("Actual_Processor_Binding", process);

		if (val != null) {
			if (val instanceof ComponentInstance) {
				return (ComponentInstance) val;
			} else if (val instanceof InstanceReferenceValue) {
				return (ComponentInstance) (((InstanceReferenceValue) val).getReferencedInstanceObject());
			}
		}

		return null;
	}

	public static ComponentInstance getInfoProcessGlobalCPU(ComponentInstance process) {
		ComponentInstance cpu = getInfoProcessCPU(process);

		while (cpu.getCategory() != ComponentCategory.PROCESSOR) {
			cpu = (ComponentInstance) cpu.getOwner();
		}

		return cpu;
	}

	public static double getInfoMaxDuration(NamedElement e, String unit) {
		NumberValue nv = PropertyUtils.getMaxRangeValue(e, "Compute_Execution_Time");
		if (nv != null) {
			return nv.getScaledValue(unit);
		} else {
			return 0f;
		}
	}

	public static double getInfoMinDuration(NamedElement e, String unit) {
		NumberValue nv = PropertyUtils.getMinRangeValue(e, "Compute_Execution_Time");
		if (nv != null) {
			return nv.getScaledValue(unit);
		} else {
			return getInfoMaxDuration(e, unit);
		}
	}

	public static Long getInfoTaskDeadline(ComponentInstance task) {
		Long result = PropertyUtils.getIntValue(task, "Deadline", AadlProject.MS_LITERAL);

		if (result == null) {
			result = getInfoTaskPeriod(task);
		}

		return result;
	}

	public static DispatchProtocol getInfoTaskDispatch(ComponentInstance task) {
		String value = PropertyUtils.getEnumValue(task, "Dispatch_Protocol");
		if (value != null) {
			return DispatchProtocol.valueOf(value);
		} else {
			return DispatchProtocol.Unknown;
		}
	}

	public static Long getInfoTaskJitter(ComponentInstance task) {
		Long result = PropertyUtils.getIntValue(task, "Jitter", AadlProject.MS_LITERAL);

		if (result == null) {
			result = 0L;
		}

		return result;
	}

	public static Long getInfoTaskPeriod(ComponentInstance task) {
		Long result = PropertyUtils.getIntValue(task, "Period", AadlProject.MS_LITERAL);

		if (result == null) {
			result = 0L;
		}

		return result;
	}

	/**
	 * get thread priority
	 * 
	 * @param thr the thread
	 * @return
	 */
	public static Long getTaskPriority(NamedElement task) {
		Long result = PropertyUtils.getIntValue(task, "Priority");

		if (result == null) {
			result = 0L;
		}

		return result;
	}

	public static Long getInfoTaskMemorySize(ComponentInstance task) {
		Long result = PropertyUtils.getIntValue(task, "Code_Size", AadlProject.B_LITERAL);

		if (result == null) {
			result = 0L;
		}

		return result;
	}

	public static Long getInfoTaskStackSize(ComponentInstance task) {
		Long result = PropertyUtils.getIntValue(task, "Stack_Size", AadlProject.B_LITERAL);

		if (result == null) {
			result = 0L;
		}

		return result;
	}

	public static long getInfoPortCriticality(FeatureInstance port) {
		Long result = PropertyUtils.getIntValue(port, "Criticality");

		if (result == null) {
			result = 0L;
		}

		return result;
	}

	public static double getThreadContextSwitchFor(ComponentInstance processor, String unit) {
		NumberValue nv = PropertyUtils.getMaxRangeValue(processor, "Thread_Swap_Execution_Time");
		if (nv != null) {
			return nv.getScaledValue(unit);
		} else {
			return 0f;
		}
	}

	public static double getProcessContextSwitchFor(ComponentInstance processor, String unit) {
		NumberValue nv = PropertyUtils.getMaxRangeValue(processor, "Process_Swap_Execution_Time");
		if (nv != null) {
			return nv.getScaledValue(unit);
		} else {
			return 0f;
		}
	}

	public static double getSchedulerQuantum(ComponentInstance processor, String unit) {
		NumberValue nv = PropertyUtils.getMaxRangeValue(processor, "Scheduler_Quantum");
		if (nv != null) {
			return nv.getScaledValue(unit);
		} else {
			return -1d;
		}
	}

	public static float getSequenceMaxDuration(List<Subprogram> subprograms, String unit) {
		BigDecimal d = new BigDecimal("0");

		for (Subprogram s : subprograms) {
			double max = getInfoMaxDuration(s, unit);
			d = d.add(new BigDecimal(max + ""));
		}

		return d.floatValue();
	}

	public static float getSequenceMinDuration(List<Subprogram> subprograms, String unit) {
		BigDecimal d = new BigDecimal("0");

		for (Subprogram s : subprograms) {
			double min = getInfoMinDuration(s, unit);
			d = d.add(new BigDecimal(min + ""));
		}

		return d.floatValue();
	}

	public static List<Subprogram> getAllCallSequenceAsSubprograms(ComponentInstance c) {
		List<SubprogramCall> calls = getAllCallSequence(c);
		List<Subprogram> spgs = new ArrayList<Subprogram>();

		for (SubprogramCall ca : calls) {
			spgs.add((Subprogram) ca.getCalledSubprogram());
		}

		return spgs;
	}

	public static List<SubprogramCall> getAllCallSequence(ComponentInstance c) {
		Classifier ti = c.getSubcomponent().getClassifier();

		for (Element e : ti.getOwnedElements()) {
			if (e instanceof SubprogramCallSequence) {
				return getCallSequence((SubprogramCallSequence) e);
			}
		}

		return Collections.emptyList();
	}

	private static List<SubprogramCall> getCallSequence(SubprogramCallSequence seq) {
		List<SubprogramCall> l = new ArrayList<SubprogramCall>();

		for (SubprogramCall call : seq.getOwnedSubprogramCalls()) {
			SubprogramCall sCall = (SubprogramCall) call;
			Subprogram s = (Subprogram) sCall.getCalledSubprogram();
			l.add(sCall);

			for (Element e : s.getOwnedElements()) {
				if (e instanceof SubprogramCallSequence) {
					l.addAll(getCallSequence((SubprogramCallSequence) e));
				}
			}
		}

		return l;
	}

	public static String getPrecision(NamedElement ne) {
		String precision = "";
		Property prop = null;
		try {
			prop = GetProperties.lookupPropertyDefinition(ne, "AADL_Project", "Timing_Precision");
		} catch (Exception e) {
			return "ms";
		}

		if (prop == null)
			return "ms";
		UnitLiteral sl = (UnitLiteral) prop.getDefaultValue();
		precision = sl.getName();
		return precision;
	}

	/**
	 * returns the processors list of the system
	 * 
	 * @param system
	 */

	public static List<ComponentInstance> getProcessors(SystemInstance system) {
		ArrayList<ComponentInstance> cpuList = new ArrayList<ComponentInstance>();

		for (ComponentInstance c : system.getAllComponentInstances()) {
			if (c.getCategory().equals(ComponentCategory.SYSTEM)) {
				boolean isProcessor = isProcessor(c);
				if (isProcessor == true) {
					cpuList.add(c);
				}
			}
			if (c.getCategory().equals(ComponentCategory.PROCESSOR)) {
				cpuList.add(c);
			}
		}
		return cpuList;
	}

	/**
	 * Get the maximal capacity utilization of an aadl Component (processors and
	 * buses)
	 * 
	 * @param obj
	 *            represents the component isntance
	 * @return the equivalent maximal capacity utilization
	 */

	public static double getObjectMaxUtil(NamedElement e) {
		return getFloatPropertyValue(e, UTILIZATION);
	}

	/**
	 * Get the reliability of an aadl Component (processors and buses)
	 * 
	 * @param obj
	 *            represents the component isntance
	 * @return the equivalent reliability
	 */
	public static double getObjectReliability(ComponentInstance obj) {
		double reliability = -1;
		EList<PropertyAssociation> pasList = obj.getOwnedPropertyAssociations();
		for (PropertyAssociation propertyAssociation : pasList) {
			if (propertyAssociation.getProperty().getName().equals(RELIABILITY)) {
				EList<ModalPropertyValue> ownedValuesList = propertyAssociation.getOwnedValues();
				for (ModalPropertyValue modalPropertyValue : ownedValuesList) {
					ModalPropertyValue modalPropertyValueImpl = (ModalPropertyValue) modalPropertyValue;
					RealLiteral ov = (RealLiteral) modalPropertyValueImpl.getOwnedValue();
					reliability = ov.getScaledValue();
				}
			}
		}
		return reliability;
	}

	/**
	 * 
	 * @param obj
	 *            represents the component isntance (a processor)
	 * @return
	 */
	public static List<ComponentInstance> getCoresList(ComponentInstance obj) {
		ArrayList<ComponentInstance> coresList = new ArrayList<ComponentInstance>();
		for (ComponentInstance core : obj.getComponentInstances()) {
			if (core.getCategory().equals(ComponentCategory.PROCESSOR)) {
				coresList.add(core);
			}
		}
		return coresList;
	}

	/**
	 * returns the buses list of the system
	 * 
	 * @param system
	 */

	public static List<ComponentInstance> getBuses(SystemInstance system) {
		List<ComponentInstance> busList = new ArrayList<ComponentInstance>();
		for (ComponentInstance b : system.getAllComponentInstances()) {
			if (b.getCategory().equals(ComponentCategory.BUS)) {
				busList.add(b);
			}
		}
		return busList;
	}

	/**
	 * returns the buses list connected to a processor
	 * 
	 * @param obj
	 *            represents the component isntance
	 */

	public static List<ComponentInstance> getBusesList(ComponentInstance obj) {
		ArrayList<ComponentInstance> busList = new ArrayList<ComponentInstance>();

		for (FeatureInstance fet : obj.getFeatureInstances()) {
			for (ConnectionInstance conn : fet.getSrcConnectionInstances()) {
				if (!busList.contains(conn.getDestination().getComponentInstance())) {
					busList.add(conn.getDestination().getComponentInstance());
				}
			}
			for (ConnectionInstance conn : fet.getDstConnectionInstances()) {
				if (!busList.contains(conn.getSource().getComponentInstance())) {
					busList.add(conn.getSource().getComponentInstance());
				}
			}
		}
		return busList;
	}

	/**
	 * returns the processes list of the system
	 * 
	 * @param system
	 * @return
	 */
	public static List<ComponentInstance> getProcesses(SystemInstance system) {

		ArrayList<ComponentInstance> processList = new ArrayList<ComponentInstance>();

		for (ComponentInstance p : system.getAllComponentInstances()) {
			if (p.getCategory().equals(ComponentCategory.PROCESS)) {
				processList.add(p);
			}
		}
		return processList;
	}

	/**
	 * returns the threads list of the process
	 *
	 * @param proc the process
	 * @return
	 */

	public static List<ComponentInstance> getThreads(ComponentInstance proc) {
		ArrayList<ComponentInstance> threadsList = new ArrayList<ComponentInstance>();
		for (ComponentInstance thread : proc.getComponentInstances()) {
			if (thread.getCategory().equals(ComponentCategory.THREAD)) {
				threadsList.add(thread);
			}
		}
		return threadsList;
	}

	/**
	 * get connection priority
	 * 
	 * @param conn
	 * @return
	 */

	public static int getMsgPriority(ConnectionInstance conn) {
		Long priority = PropertyUtils.getIntValue(conn, "Priority");
		return priority.intValue();
	}

	/**
	 * returns the list of threads that follow the thread thr
	 * 
	 * @param thr
	 * @param sys
	 * @return
	 */
	public static List<ComponentInstance> getSuccThrList(ComponentInstance thr, SystemInstance sys) {
		ArrayList<ComponentInstance> succTsksList = new ArrayList<ComponentInstance>();
		for (ConnectionInstance conn : sys.getAllConnectionInstances()) {
			if (conn.getSource().getComponentInstance().equals(thr)) {
				succTsksList.add(conn.getDestination().getComponentInstance());
			}
		}
		return succTsksList;
	}

	/**
	 * * returns true if the thread thr is the first thread activated within its
	 * end to end path
	 * 
	 * @param thr
	 * @param sys
	 * @return
	 */

	public static boolean IsSrcTsk(ComponentInstance thr, SystemInstance sys) {
		boolean isSrcTsk = true;
		for (ConnectionInstance conn : sys.getAllConnectionInstances()) {
			if (conn.getDestination().getComponentInstance().equals(thr)) {
				isSrcTsk = false;
				return isSrcTsk;
			}
		}
		return isSrcTsk;
	}

	/**
	 * returns the messages list of the system
	 *
	 * @param system
	 * @return
	 */

	public static List<ConnectionInstance> getMessagesList(SystemInstance sys) {

		ArrayList<ConnectionInstance> msgs = new ArrayList<ConnectionInstance>();

		for (ConnectionInstance cnx : sys.getConnectionInstances()) {
			if (cnx.getSource().getComponentInstance().getCategory().equals(ComponentCategory.THREAD)
					&& cnx.getDestination().getComponentInstance().getCategory().equals(ComponentCategory.THREAD)) {
				msgs.add(cnx);
			}
		}
		return msgs;
	}

	/**
	 * returns task deadline
	 * 
	 * @param threadComponent
	 * @return
	 */
	public static int getTaskDeadline(ComponentInstance threadComponent) {
		Long deadline = PropertyUtils.getIntValue(threadComponent, "Deadline", AadlProject.MS_LITERAL);
		if (deadline == null) {
			return 0;
		}
		return deadline.intValue();
	}

	/**
	 * returns the value of an aadl property
	 * 
	 * @param e
	 * @param property
	 * @return
	 */
	public static double getFloatPropertyValue(NamedElement e, String property) {
		try {
			return PropertyUtils.getFloatValue(e, property);
		} catch (Exception e1) {
			// _LOGGER.warn("Property " + property + " undefined for element " +
			// e.getQualifiedName());
			return 0;
		}
	}


	/**
	 * get task period
	 * 
	 * @param task
	 * @param scale (ms, us, ns, etc.)
	 * @return
	 */
	public static Long getTaskPeriod(ComponentInstance task, String scale) {
		Long result = PropertyUtils.getIntValue(task, "Period", scale);

		if (result == null) {
			result = 0L;
		}

		return result;
	}

	public static long getHyperperiod(List<ComponentInstance> consideredTasks)
	{
		Long[] periods = new Long[consideredTasks.size()];
		ArrayList<Long> consideredPeriods = new ArrayList<Long>();
		for(ComponentInstance ci : consideredTasks)
		{
			consideredPeriods.add(getInfoTaskPeriod(ci));
		}
		consideredPeriods.toArray(periods);
		return MathHelperImpl.lcm(periods);
	}

	public static long getHyperperiod(List<ComponentInstance> consideredTasks, String scale)
	{
		Set<Long> consideredPeriods = new HashSet<Long>();
		for(ComponentInstance ci : consideredTasks)
		{
			consideredPeriods.add(getTaskPeriod(ci, scale));
		}
		Long[] periods = new Long[consideredPeriods.size()];
		consideredPeriods.toArray(periods);
		if(consideredPeriods.size()==1)
			return periods[0];
		return MathHelperImpl.lcm(periods);
	}


	public static NumberValue getPropertyValue(PropertyAssociation pa) {
		NumberValue nbV = null;
		List<ModalPropertyValue> values = pa.getOwnedValues();
		if (values.size() == 1) {
			ModalPropertyValue v = values.get(0);
			PropertyExpression expr = v.getOwnedValue();
			if (expr instanceof RangeValue) {
				nbV = ((RangeValue) expr).getMaximumValue();

			} else if (expr instanceof NumberValue) {
				nbV = (NumberValue) expr;
			}
		}
		return nbV;
	}

	/**
	 * 
	 * @param e
	 * @return
	 */

	public static double getSourceDataSizeInBytes(NamedElement e) {
		double size = 0;

		ComponentClassifier cc = null;

		if (e instanceof ConnectionInstance) {
			ConnectionInstance inst = (ConnectionInstance) e;
			size = getSourceDataSizeInBytes(inst.getSource());
		}

		if (e instanceof DataPort) {
			DataPort inst = (DataPort) e;
			size = getSourceDataSizeInBytes(inst.getDataFeatureClassifier());
		}
		if (e instanceof EventDataPort) {
			EventDataPort inst = (EventDataPort) e;
			size = getSourceDataSizeInBytes(inst.getDataFeatureClassifier());

		}
		if (e instanceof FeatureInstance) {
			FeatureInstance inst = (FeatureInstance) e;
			size = getSourceDataSizeInOctetsImpl(inst);
			if(size == 0)
				size = getSourceDataSizeInBytes(inst.getFeature());
			else
				return size;
		}

		if (e instanceof ComponentInstance) {
			ComponentInstance inst = (ComponentInstance) e;
			size = getSourceDataSizeInOctetsImpl(inst);
			if(size == 0)
				cc = inst.getComponentClassifier();
			else
				return size;
		}
		if (e instanceof ComponentClassifier) {
			cc = (ComponentClassifier) e;
		}
		if (cc != null)
			size = getSourceDataSizeInOctetsImpl(cc);
		if (size == 0) {
			if (cc instanceof ComponentImplementation) {
				ComponentImplementation ci = (ComponentImplementation) cc;
				ComponentImplementation extended = ci.getExtended();

				if (extended != null) {
					size = getSourceDataSizeInBytes(extended);
					if (size == 0) {
						if (ci.getType() != null) {
							cc = ci.getType();
						}
					}
				}
			}
			if (cc instanceof ComponentType) {
				ComponentType ct = (ComponentType) cc;
				size = getSourceDataSizeInBytes(ct.getExtended());
			}
		}
		return size;
	}

	private static double getSourceDataSizeInOctetsImpl(NamedElement e) {
		Double size = 0.0;
		try {
			// size = PropertyUtils.getIntValue(e, "Data_Size");
			for (PropertyAssociation pa : e.getOwnedPropertyAssociations()) {
				if (pa.getProperty().getName().equalsIgnoreCase("Data_Size")) {
					NumberValue nbV = getPropertyValue(pa);
					size = nbV.getScaledValue();
					UnitLiteral target = nbV.getUnit();
					size = nbV.getScaledValue(target);
				}
			}
		} catch (Exception e1) {
			// _LOGGER.warn("Data_Size property missing for "+e.getFullName());
		}
		return size;
	}

	public static double getSystemObjectReliability(
			ComponentInstance obj, SystemInstance firstSysInstance) {
		double reliability = 0.0;
		for(ComponentInstance ci : firstSysInstance.getAllComponentInstances())
		{
			if(ci.getQualifiedName().equals(obj.getQualifiedName()))
			{
				EList<PropertyAssociation> pasList = ci.getOwnedPropertyAssociations();
				for (PropertyAssociation propertyAssociation : pasList) {
					if (propertyAssociation.getProperty().getName().equals("reliability")) {
						EList<ModalPropertyValue> ownedValuesList = propertyAssociation.getOwnedValues();
						for (ModalPropertyValue modalPropertyValue : ownedValuesList) {
							//ModalPropertyValue modalPropertyValue = (ModalPropertyValue) modalPropertyValue;
							PropertyExpression ownedValue = modalPropertyValue.getOwnedValue();
							if(ownedValue instanceof IntegerLiteral){
								IntegerLiteral ov = (IntegerLiteral)modalPropertyValue.getOwnedValue();
								reliability = ov.getScaledValue();
							}
							else if(ownedValue instanceof RealLiteral){
								RealLiteral ov = (RealLiteral)modalPropertyValue.getOwnedValue();
								reliability = ov.getScaledValue();
							}							
						}
					}
				}
				break;
			}
		}				
		return reliability;
	}

	public static double getSystemMaxUtil(ComponentInstance obj,
			SystemInstance firstSysInstance) {
		double utilization = 0.0;
		for(ComponentInstance ci : firstSysInstance.getAllComponentInstances())
		{
			if(ci.getQualifiedName().equals(obj.getQualifiedName()))
			{
				EList<PropertyAssociation> pasList = ci.getOwnedPropertyAssociations();
				for (PropertyAssociation propertyAssociation : pasList) {
					if (propertyAssociation.getProperty().getName().equals("utilization")) {
						EList<ModalPropertyValue> ownedValuesList = propertyAssociation.getOwnedValues();
						for (ModalPropertyValue modalPropertyValue : ownedValuesList) {
							ModalPropertyValue modalPropertyValueImpl = (ModalPropertyValue) modalPropertyValue;
							PropertyExpression ownedValue = modalPropertyValueImpl.getOwnedValue();
							if(ownedValue instanceof IntegerLiteral){
								IntegerLiteral ov = (IntegerLiteral)modalPropertyValueImpl.getOwnedValue();
								utilization = ov.getScaledValue();
							}
							else if(ownedValue instanceof RealLiteral){
								RealLiteral ov = (RealLiteral)modalPropertyValueImpl.getOwnedValue();
								utilization = ov.getScaledValue();
							}							
						}
					}
				}
				break;
			}
		}				
		return utilization;
	}

	public static double getWcetSharedMsg(ConnectionInstance obj,
			SystemInstance firstSysInstance) {
		double wcetSharedMsg = 0.0;
		ComponentInstance objProcInst = (ComponentInstance) obj.getSource().eContainer().eContainer();
		ComponentInstance objThreadInst = (ComponentInstance) obj.getSource().eContainer();
		for(ConnectionInstance ci : firstSysInstance.getConnectionInstances())
		{
			if(ci.getKind() == ConnectionKind.PORT_CONNECTION)
			{
				ComponentInstance ciProcInst = (ComponentInstance) ci.getSource().eContainer().eContainer();
				ComponentInstance ciThreadInst = (ComponentInstance) ci.getSource().eContainer();
				if(objProcInst.getQualifiedName().contains(ciProcInst.getQualifiedName())
						&&
						(objThreadInst.getQualifiedName().equals(ciThreadInst.getQualifiedName())))
				{
					EList<PropertyAssociation> pasList = ci.getOwnedPropertyAssociations();
					for (PropertyAssociation propertyAssociation : pasList) {
						if (propertyAssociation.getProperty().getName().equals("wcetSharedMsg")) {
							EList<ModalPropertyValue> ownedValuesList = propertyAssociation.getOwnedValues();
							for (ModalPropertyValue modalPropertyValue : ownedValuesList) {
								ModalPropertyValue modalPropertyValueImpl = (ModalPropertyValue) modalPropertyValue;
								PropertyExpression ownedValue = modalPropertyValueImpl.getOwnedValue();
								if(ownedValue instanceof IntegerLiteral){
									IntegerLiteral ov = (IntegerLiteral)modalPropertyValueImpl.getOwnedValue();
									wcetSharedMsg = ov.getScaledValue();
								}
								else if(ownedValue instanceof RealLiteral){
									RealLiteral ov = (RealLiteral)modalPropertyValueImpl.getOwnedValue();
									wcetSharedMsg = ov.getScaledValue();
								}							
							}
						}
					}
					break;
				}				
			}
		}
		return wcetSharedMsg;
	}

	public static int getBusPriority(ConnectionInstance obj, SystemInstance firstSys) {
		double priority = 0.0;
		ComponentInstance objProcInst = (ComponentInstance) obj.getSource().eContainer().eContainer();
		ComponentInstance objThreadInst = (ComponentInstance) obj.getSource().eContainer();
		for(ConnectionInstance ci : firstSys.getConnectionInstances())
		{
			if(ci.getKind() == ConnectionKind.PORT_CONNECTION)
			{
				ComponentInstance ciProcInst = (ComponentInstance) ci.getSource().eContainer().eContainer();
				ComponentInstance ciThreadInst = (ComponentInstance) ci.getSource().eContainer();
				if(objProcInst.getQualifiedName().contains(ciProcInst.getQualifiedName())
						&&
						(objThreadInst.getQualifiedName().equals(ciThreadInst.getQualifiedName())))
				{
					EList<PropertyAssociation> pasList = ci.getOwnedPropertyAssociations();
					for (PropertyAssociation propertyAssociation : pasList) {
						if (propertyAssociation.getProperty().getName().equals("Priority")) {
							EList<ModalPropertyValue> ownedValuesList = propertyAssociation.getOwnedValues();
							for (ModalPropertyValue modalPropertyValue : ownedValuesList) {
								ModalPropertyValue modalPropertyValueImpl = (ModalPropertyValue) modalPropertyValue;
								PropertyExpression ownedValue = modalPropertyValueImpl.getOwnedValue();
								if(ownedValue instanceof IntegerLiteral){
									IntegerLiteral ov = (IntegerLiteral)modalPropertyValueImpl.getOwnedValue();
									priority = ov.getScaledValue();
								}
								else if(ownedValue instanceof RealLiteral){
									RealLiteral ov = (RealLiteral)modalPropertyValueImpl.getOwnedValue();
									priority = ov.getScaledValue();
								}							
							}
						}
					}
					break;
				}				
			}
		}
		return (int) priority;
	}



	public static double getMaximumTransmissionTimePerByte(ComponentInstance obj) 
	{
		double transmission = 0;
		EList<PropertyAssociation> pasList = obj.getOwnedPropertyAssociations();
		for (PropertyAssociation propertyAssociation : pasList) 
		{
			if (propertyAssociation.getProperty().getName().equals("Transmission_Time")) 
			{
				EList<ModalPropertyValue> ownedValuesList = propertyAssociation.getOwnedValues();
				for (ModalPropertyValue modalPropertyValue : ownedValuesList) 
				{
					ModalPropertyValue modalPropertyValueImpl = (ModalPropertyValue) modalPropertyValue;

					RecordValue rv = (RecordValue) modalPropertyValueImpl.getOwnedValue();

					for (BasicPropertyAssociation fv : rv.getOwnedFieldValues()) 
					{
						if(fv.getProperty().getName().contains("PerByte"))
						{
							PropertyExpression expr = fv.getOwnedValue();

							if (expr instanceof RangeValue) 
							{
								NumberValue n = ((RangeValue) expr).getMaximumValue();
								transmission = n.getScaledValue("us");
							}
						}
					}
				}
			}
		}
		return transmission;
	}

	public static double getMaximumTransmissionTimeFixed(ComponentInstance obj) 
	{
		double transmission = 0;
		EList<PropertyAssociation> pasList = obj.getOwnedPropertyAssociations();
		for (PropertyAssociation propertyAssociation : pasList) 
		{
			if (propertyAssociation.getProperty().getName().equals("Transmission_Time")) 
			{
				EList<ModalPropertyValue> ownedValuesList = propertyAssociation.getOwnedValues();
				for (ModalPropertyValue modalPropertyValue : ownedValuesList) 
				{
					//ModalPropertyValue modalPropertyValue = (ModalPropertyValue) modalPropertyValue;

					RecordValue rv = (RecordValue) modalPropertyValue.getOwnedValue();

					for (BasicPropertyAssociation fv : rv.getOwnedFieldValues()) 
					{
						if(fv.getProperty().getName().contains("Fixed"))
						{
							PropertyExpression expr = fv.getOwnedValue();

							if (expr instanceof RangeValue) 
							{
								NumberValue n = ((RangeValue) expr).getMaximumValue();
								transmission = n.getScaledValue("ms");
							}
						}
					}
				}
			}
		}
		return transmission;
	}

	/**
	 * get connection wcet if the message is shared between tasks belonging to
	 * the same cpu but to different cores
	 * 
	 * @param conn
	 * @return
	 */

	public static double getWcetIfShared(ConnectionInstance conn) {
		double wcetShared = PropertyUtils.getFloatValue(conn, "wcetSharedMsg");
		return wcetShared;
	}

	private static ListValue getDeloymentListValue(NamedElement aProcess)
	{
		PropertyAssociation aPropertyAssociation =
				PropertyUtils.findPropertyAssociation("Actual_Processor_Binding",
						aProcess) ;

		if(aPropertyAssociation == null)
		{
			return null;
		}

		for(ModalPropertyValue aModalPropertyValue : aPropertyAssociation
				.getOwnedValues())
		{
			if(aModalPropertyValue.getOwnedValue() instanceof ListValue)
			{
				return (ListValue) aModalPropertyValue.getOwnedValue() ;
			}
		}
		return null;
	}

	public static ComponentInstance getActualProcessorBinding(ComponentInstance aProcess)
	{
		List<ComponentInstance> res = getActualProcessorBindingList(aProcess);
		if(res!=null && !res.isEmpty())
			return res.get(0);
		return null;
	}

	public static List<ComponentInstance> getActualProcessorBindingList(ComponentInstance aProcess)
	{
		List<ComponentInstance> res = new ArrayList<ComponentInstance>();
		ListValue list = getDeloymentListValue(aProcess);
		if(list == null)
			return res;
		for(PropertyExpression pe : list.getOwnedListElements())
		{
			if(pe instanceof InstanceReferenceValue)
			{
				InstanceReferenceValue irv = (InstanceReferenceValue) pe ;
				ComponentInstance ci = (ComponentInstance) irv.getReferencedInstanceObject();
				res.add(ci);
			}
		}
		return res;
	}
	
	public static ComponentInstance getDeloymentProcessorInstance(
			ComponentInstance process)
	{
		ComponentInstance ci = getActualProcessorBinding(process);
		if(ci==null)
			return null;
		ComponentInstance processor = ci ;
		if(ci.getCategory().equals(ComponentCategory.VIRTUAL_PROCESSOR))
		{
			processor = getDeloymentProcessorInstance(ci);
			if(processor == null)
				processor = getParentProcessor(ci);
		}
		else if(ci.getCategory().equals(ComponentCategory.PROCESSOR))
		{
			if(isInMultiCore(ci))
				processor = getParentProcessor(ci);
		}
		else if (ci.getCategory().equals(ComponentCategory.SYSTEM) && isProcessor(ci))
			processor = ci;
		return processor;
	}

	private static ComponentInstance getParentProcessor(ComponentInstance ci) {
		if(ci.getContainingComponentInstance() == null)
			return null;
		else if(isProcessor(ci.getContainingComponentInstance()) &&!isInMultiCore(ci.getContainingComponentInstance()))
			return ci.getContainingComponentInstance();
		else
			return getParentProcessor(ci.getContainingComponentInstance());
	}

	private static boolean isInMultiCore(ComponentInstance ci)
	{
		ComponentInstance parent = getParentProcessor(ci);
		return parent!=null;
	}
	
	//if multicore, returns the processor (not one of its core)
	public static List<ProcessorSubcomponent> getDeloymentProcessorSubcomponentList(
			Subcomponent processOrVirtualProcessor)
	{
		List<ProcessorSubcomponent> res = new ArrayList<ProcessorSubcomponent>();

		ListValue list = getDeloymentListValue(processOrVirtualProcessor);
		if(list==null)
			return res;
		for(PropertyExpression pe : list.getOwnedListElements())
		{
			if(pe instanceof ReferenceValue)
			{
				ReferenceValue rv = (ReferenceValue) pe ;
				int lastIdx = rv.getContainmentPathElements().size()-1;
				NamedElement anElement =
						rv.getContainmentPathElements().get(lastIdx).getNamedElement() ;

				if(anElement instanceof ProcessorSubcomponent)
				{
					ProcessorSubcomponent ps = (ProcessorSubcomponent) anElement ;
					res.add(ps);
				}
				else if(anElement instanceof VirtualProcessorSubcomponent)
				{
					VirtualProcessorSubcomponent vps =
							(VirtualProcessorSubcomponent) anElement ;
					List<ProcessorSubcomponent> vpsProcessors = getDeloymentProcessorSubcomponentList(vps);
					if(!vpsProcessors.isEmpty())
						res.addAll(vpsProcessors);
					else
					{
						if(lastIdx>0)
						{
							anElement = rv.getContainmentPathElements().get(lastIdx-1).getNamedElement() ;
							if(anElement instanceof ProcessorSubcomponent)
								res.add((ProcessorSubcomponent) anElement);
							else if (anElement instanceof Subcomponent)
								res.addAll(getDeloymentProcessorSubcomponentList((Subcomponent) anElement));
						}

					}
				}
				else if(anElement instanceof SystemSubcomponent)
				{
					SystemSubcomponent ss =
							(SystemSubcomponent) anElement ;
					SystemSubcomponentType sct = ss.getSystemSubcomponentType();
					if(sct instanceof SystemImplementation)
					{
						SystemImplementation si = (SystemImplementation) sct;
						for(VirtualProcessorSubcomponent vp: si.getOwnedVirtualProcessorSubcomponents())
							res.addAll(getDeloymentProcessorSubcomponentList(vp));
						for(ProcessorSubcomponent ps: si.getOwnedProcessorSubcomponents())
							res.add(ps);
						for(SystemSubcomponent ss2: si.getOwnedSystemSubcomponents())
							res.addAll(getDeloymentProcessorSubcomponentList(ss2));
					}
				}
			}
		}

		return res;
	}

	// if multicore, returns the processor (not one of its core)
	// may be a systemsubcomponent or a processorsubcomponent
	public static Subcomponent getDeloymentProcessorSubcomponent(
			Subcomponent processOrVirtualProcessor)
	{
		ListValue list = getDeloymentListValue(processOrVirtualProcessor);
		for(PropertyExpression pe : list.getOwnedListElements())
		{
			if(pe instanceof ReferenceValue)
			{
				ReferenceValue rv = (ReferenceValue) pe ;
				NamedElement anElement =
						rv.getContainmentPathElements().get(0).getNamedElement() ;

				if(anElement instanceof ProcessorSubcomponent)
				{
					ProcessorSubcomponent ps = (ProcessorSubcomponent) anElement ;
					return ps ;
				}
				else if(anElement instanceof VirtualProcessorSubcomponent)
				{

					// TODO: Should go for the processor!?!
					VirtualProcessorSubcomponent vps =
							(VirtualProcessorSubcomponent) anElement ;
					return getDeloymentProcessorSubcomponent(vps);
				}
				else if(anElement instanceof SystemSubcomponent)
				{
					SystemSubcomponent ss =
							(SystemSubcomponent) anElement ;
					return ss ;
				}
			}
		}
		return null ;
	}

	public static List<ProcessSubcomponent> getBindedProcesses(Subcomponent object)
	{
		List<ProcessSubcomponent> bindedProcess = new ArrayList<ProcessSubcomponent>() ;
		SystemImplementation si = (SystemImplementation) object.eContainer();
		for(ProcessSubcomponent ps : si.getOwnedProcessSubcomponents())
		{
			Subcomponent ne = getDeloymentProcessorSubcomponent(ps);
			if(ne instanceof VirtualProcessorSubcomponent)
				ne = getDeloymentProcessorSubcomponent(ne);
			if(ne!=null && ne.equals(object))
			{
				bindedProcess.add(ps) ;
			}
		}

		return bindedProcess ;
	}

	public static long getProcessorsNumber(SystemImplementation processor) {
		long res = 0;
		for(Subcomponent sub: processor.getAllSubcomponents())
		{
			if(sub instanceof ProcessorSubcomponent)
			{
				long prod=1;
				for(ArrayDimension ad: sub.getArrayDimensions())
				{
					ArraySize as = ad.getSize();
					prod = prod*as.getSize();
				}
				res+=prod;
			}
			else if(isProcessor(sub))
				res+=getProcessorsNumber(sub);
		}
		return res;
	}


	public static long getProcessorsNumber(SystemSubcomponent processor) {
		SystemSubcomponentType subCptType = processor.getSystemSubcomponentType();
		if(subCptType instanceof SystemImplementation)
			return getProcessorsNumber((SystemImplementation) subCptType);
		else if (isProcessor(processor))
			return 1; 
		return 0;
	}

	public static long getProcessorsNumber(Subcomponent processor) {
		if(processor instanceof SystemSubcomponent 
				&& isProcessor(processor))
		{
			SystemSubcomponent sys = (SystemSubcomponent) processor;
			return getProcessorsNumber(sys);
		}
		return 1;
	}

	public static List<Long> getProcessOrVirtualProcessorAffinities(ComponentInstance processOrVP) {
		List<Long> res = new ArrayList<Long>();
		List<ComponentInstance> processorOrVirtualProcessorList = getActualProcessorBindingList(processOrVP);
		if(processorOrVirtualProcessorList.isEmpty())
		{
			ComponentInstance deployed = getParentProcessor(processOrVP);
			if(deployed!=null)
				processorOrVirtualProcessorList.add(deployed);
		}
		for(ComponentInstance deployed: processorOrVirtualProcessorList)
		{
			if(deployed.getCategory().equals(ComponentCategory.VIRTUAL_PROCESSOR))
			{
				List<Long> vpAffinities = getProcessOrVirtualProcessorAffinities(deployed);
				res.addAll(vpAffinities);
			}
			else
			{
				Long id = getCoreId(deployed);
				if(id == null)
					res.add(1L);
				else
					res.add(id);
			}
		}		
		return res;
	}

	
	public static List<Long> getProcessOrVirtualProcessorAffinities(ProcessSubcomponent processOrVP) {
		List<Long> res = new ArrayList<Long>();

		List<ProcessorSubcomponent> processorList = getDeloymentProcessorSubcomponentList(processOrVP);
		for(ProcessorSubcomponent core: processorList)
		{
			Long id = getCoreId(core);
			if(id == null)
				res.add(1L);
			else
				res.add(id);
		}
		return res;
	}

	public static Long getCoreId(NamedElement processor)
	{
		return PropertyUtils.getIntValue(processor, "Processor_Core_ID");
	}

	public static Long getCoreId(VirtualProcessorSubcomponent virtualProcessor)
	{
		Long id = PropertyUtils.getIntValue(virtualProcessor, "Processor_Core_ID");
		if(id==null)
		{
			NamedElement processor = getDeloymentProcessorSubcomponent(virtualProcessor);
			id =  getCoreId(processor);
		}
		return id;
	}


	public static boolean isProcessor(NamedElement sub) {
		if(sub instanceof Processor)
			return true;
		
		if(sub instanceof ComponentInstance)
		{
			ComponentInstance ci = (ComponentInstance) sub;
			if(ci.getCategory().equals(ComponentCategory.PROCESSOR))
				return true;
			else if(!ci.getCategory().equals(ComponentCategory.SYSTEM))
				return false;
		}		
		if(sub instanceof System || sub instanceof ComponentInstance)
		{
			Boolean b = PropertyUtils.getBooleanValue(sub, "is_processor");
			if(b==null)
			{
				if(sub instanceof Subcomponent)
				{
					Subcomponent sc = (Subcomponent) sub;
					return isProcessor(sc.getSubcomponentType());
				}
				else if(sub instanceof ComponentImplementation)
				{
					ComponentImplementation ci = (ComponentImplementation) sub;
					return isProcessor(ci.getType());
				}
				return false;
			}
			else
				return b;
		}
		return false;
	}
	
	public static NamedElement getDeloymentMemorySubcomponent(NamedElement aProcessSubcomponent)
	  {
	    // aProcessSubcomponent could be a of class ComponentInstance as well
	    PropertyAssociation aPropertyAssociation =
	          PropertyUtils.findPropertyAssociation("Actual_Memory_Binding",
	                                     aProcessSubcomponent) ;

	    if(aPropertyAssociation != null)
	    {
	      for(ModalPropertyValue aModalPropertyValue : aPropertyAssociation.
	                                                               getOwnedValues())
	      {
	        if(aModalPropertyValue.getOwnedValue() instanceof ListValue)
	        {
	          ListValue list = (ListValue) aModalPropertyValue.getOwnedValue() ;

	          for(PropertyExpression pe : list.getOwnedListElements())
	          {
	            if(pe instanceof ReferenceValue)
	            {
	              ReferenceValue rv = (ReferenceValue) pe ;
	              NamedElement anElement =
	                                       rv.getContainmentPathElements()
	                                         .get(rv.getContainmentPathElements()
	                                                .size() - 1).getNamedElement() ;

	              return anElement;
	            }
	          }
	        }
	      }
	    }

	    return null ;
	  }
	
	public static boolean isPort(FeatureInstance fi)
	{
		return fi.getCategory().equals(FeatureCategory.DATA_PORT)
		        || fi.getCategory().equals(FeatureCategory.EVENT_PORT)
		        || fi.getCategory().equals(FeatureCategory.EVENT_DATA_PORT);
	}
	
	public static List<FeatureInstance> getAllProcessPorts(ComponentInstance process)
	{
		List<FeatureInstance> res = new ArrayList<FeatureInstance>();
		for(FeatureInstance fi: process.getAllFeatureInstances())
		{
			if(AadlHelperImpl.isPort(fi))
				res.add(fi);
		}
		return res;

	}

	public static boolean isInputFeature(FeatureInstance fi) {
		return fi.getDirection().equals(DirectionType.IN)
				|| fi.getDirection().equals(DirectionType.IN_OUT);
	}

	public static boolean isOutputFeature(FeatureInstance fi) {
		return fi.getDirection().equals(DirectionType.OUT)
				|| fi.getDirection().equals(DirectionType.IN_OUT);
	}

	
	private static Long getMaxPriorityRange(NamedElement ne)
	{
		RangeValue rv = PropertyUtils.getRangeValue(ne, "Priority_Range");
		if(rv!=null) {
			IntegerLiteral il = (IntegerLiteral) rv.getMaximum();
			return il.getValue()-5;
		}
		return null;
	}
	
	public static long getMaxPriority(NamedElement ne) {
		List<Long> priorityList = new ArrayList<Long>(); 
		if(isProcessor(ne))
		{
			Long maxPrioRange = getMaxPriorityRange(ne);
			if(maxPrioRange!=null)
				return maxPrioRange;
			else
			{
				if(ne instanceof ProcessorSubcomponent)
				{
					ProcessorSubcomponent procSubcpt = (ProcessorSubcomponent) ne;
					List<ProcessSubcomponent> appSubcptList = getBindedProcesses(procSubcpt);
					for(ProcessSubcomponent appSubcpt: appSubcptList)
						priorityList.add(getMaxPriority(appSubcpt));
				}
			}
		}
		if(ne instanceof ProcessSubcomponent)
		{
			ProcessSubcomponent ps = (ProcessSubcomponent) ne;
			Subcomponent procSubcpt = getDeloymentProcessorSubcomponent(ps);
			if(procSubcpt!=null)
			{
				Long maxPrioRange = getMaxPriorityRange(ne);
				if(maxPrioRange!=null)
					return maxPrioRange;
				else
				{
					List<ProcessSubcomponent> appSubcptList = getBindedProcesses(procSubcpt);
					for(ProcessSubcomponent appSubcpt: appSubcptList)
						priorityList.add(getMaxPriorityOfThreads(appSubcpt));
				}
			}
			else
			{
				priorityList.add(getMaxPriorityOfThreads(ps));
			}
		}
		if(!priorityList.isEmpty())
			return Collections.max(priorityList);
		long defaultValue = 50;
		String errMsg = "No Priority_Range property defined for component "+
				ne.getName() +"; max prio set to default value: "+defaultValue;
		_LOGGER.warn(errMsg);
		//  ServiceProvider.SYS_ERR_REP.warning(errMsg, true);
		return defaultValue;
	}

	private static Long getMaxPriorityOfThreads(ProcessSubcomponent appSubcpt) {
		List<Long> priorityList = new ArrayList<Long>(); 
		List<ThreadSubcomponent> taskList = new ArrayList<ThreadSubcomponent>();
		ProcessSubcomponentType pst = (ProcessSubcomponentType) appSubcpt.getSubcomponentType();
		if(pst instanceof ProcessImplementation)
		{
			ProcessImplementation pi = (ProcessImplementation) pst;
			taskList.addAll(pi.getOwnedThreadSubcomponents());
		}
		for(ThreadSubcomponent t: taskList)
		{
			priorityList.add(getTaskPriority(t));
		}
		if(!priorityList.isEmpty())
			return Collections.max(priorityList);
		return 0L;
	}

	public static List<FeatureInstance> getOutputPorts(ComponentInstance ci) {
		List<FeatureInstance> res = new ArrayList<FeatureInstance>();
		for(FeatureInstance fi: ci.getAllFeatureInstances())
		{
			if(fi.getDirection().equals(DirectionType.IN))
				continue;
			if(fi.getCategory().equals(FeatureCategory.DATA_PORT)
					|| fi.getCategory().equals(FeatureCategory.EVENT_DATA_PORT)
					|| fi.getCategory().equals(FeatureCategory.EVENT_PORT))
				res.add(fi);
		}
		return res;
	}

} //AadlHelperImpl
