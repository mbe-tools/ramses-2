package fr.mem4csd.ramses.core.helpers.impl;

public class DimensioningException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7533360783774269903L;
	
	
	public DimensioningException(String message)
	{
	      super(message) ;
	}
}
