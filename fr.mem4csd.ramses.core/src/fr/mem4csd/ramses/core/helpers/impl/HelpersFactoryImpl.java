/**
 */
package fr.mem4csd.ramses.core.helpers.impl;

import fr.mem4csd.ramses.core.helpers.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HelpersFactoryImpl extends EFactoryImpl implements HelpersFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static HelpersFactory init() {
		try {
			HelpersFactory theHelpersFactory = (HelpersFactory)EPackage.Registry.INSTANCE.getEFactory(HelpersPackage.eNS_URI);
			if (theHelpersFactory != null) {
				return theHelpersFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new HelpersFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HelpersFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case HelpersPackage.ATL_HELPER: return createAtlHelper();
			case HelpersPackage.MATH_HELPER: return createMathHelper();
			case HelpersPackage.AADL_HELPER: return createAadlHelper();
			case HelpersPackage.CODE_GEN_HELPER: return createCodeGenHelper();
			case HelpersPackage.EVENT_DATA_PORT_COM_DIM_HELPER: return createEventDataPortComDimHelper();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case HelpersPackage.RAMSES_EXCEPTION:
				return createRamsesExceptionFromString(eDataType, initialValue);
			case HelpersPackage.DIMENSIONING_EXCEPTION:
				return createDimensioningExceptionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case HelpersPackage.RAMSES_EXCEPTION:
				return convertRamsesExceptionToString(eDataType, instanceValue);
			case HelpersPackage.DIMENSIONING_EXCEPTION:
				return convertDimensioningExceptionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AtlHelper createAtlHelper() {
		AtlHelperImpl atlHelper = new AtlHelperImpl();
		return atlHelper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MathHelper createMathHelper() {
		MathHelperImpl mathHelper = new MathHelperImpl();
		return mathHelper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AadlHelper createAadlHelper() {
		AadlHelperImpl aadlHelper = new AadlHelperImpl();
		return aadlHelper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CodeGenHelper createCodeGenHelper() {
		CodeGenHelperImpl codeGenHelper = new CodeGenHelperImpl();
		return codeGenHelper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EventDataPortComDimHelper createEventDataPortComDimHelper() {
		EventDataPortComDimHelperImpl eventDataPortComDimHelper = new EventDataPortComDimHelperImpl();
		return eventDataPortComDimHelper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RamsesException createRamsesExceptionFromString(EDataType eDataType, String initialValue) {
		return (RamsesException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRamsesExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DimensioningException createDimensioningExceptionFromString(EDataType eDataType, String initialValue) {
		return (DimensioningException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDimensioningExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public HelpersPackage getHelpersPackage() {
		return (HelpersPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static HelpersPackage getPackage() {
		return HelpersPackage.eINSTANCE;
	}

} //HelpersFactoryImpl
