/**
 */
package fr.mem4csd.ramses.core.helpers.impl;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.osate.aadl2.BasicProperty;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.DirectedFeature;
import org.osate.aadl2.DirectionType;
import org.osate.aadl2.Element;
import org.osate.aadl2.Feature;
import org.osate.aadl2.IntegerLiteral;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.Mode;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.Port;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.RangeValue;
import org.osate.aadl2.RecordValue;
import org.osate.aadl2.StringLiteral;
import org.osate.aadl2.SubprogramClassifier;
import org.osate.aadl2.UnitLiteral;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.aadl2.instance.ModeInstance;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.instance.SystemOperationMode;
import org.osate.aadl2.parsesupport.LocationReference;
import org.osate.ba.aadlba.BehaviorAnnex;
import org.osate.ba.aadlba.BehaviorElement;
import org.osate.ba.aadlba.BehaviorTransition;
import org.osate.ba.aadlba.DispatchConjunction;
import org.osate.ba.aadlba.DispatchTrigger;
import org.osate.ba.aadlba.DispatchTriggerLogicalExpression;
import org.osate.ba.aadlba.ElementHolder;
import org.osate.ba.aadlba.EventDataPortHolder;
import org.osate.ba.aadlba.EventPortHolder;
import org.osate.ba.aadlba.PortCountValue;
import org.osate.ba.aadlba.PortFreshValue;
import org.osate.ba.utils.AadlBaLocationReference;
import org.osate.ba.utils.AadlBaVisitors;
import org.osate.utils.internal.Aadl2Utils;
import org.osate.utils.internal.PropertyUtils;

import fr.mem4csd.ramses.core.helpers.AtlHelper;
import fr.mem4csd.ramses.core.helpers.EventDataPortComDimHelper;
import fr.mem4csd.ramses.core.helpers.HelpersPackage;
import fr.mem4csd.ramses.core.codegen.utils.DataSizeHelper;
/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Atl
 * Helper</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.impl.AtlHelperImpl#getOutputPackageName <em>Output Package Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AtlHelperImpl extends MinimalEObjectImpl.Container implements AtlHelper {
	/**
	 * The default value of the '{@link #getOutputPackageName() <em>Output Package Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOutputPackageName()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_PACKAGE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutputPackageName() <em>Output Package Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOutputPackageName()
	 * @generated
	 * @ordered
	 */
	protected String outputPackageName = OUTPUT_PACKAGE_NAME_EDEFAULT;

	private static Logger _LOGGER = Logger.getLogger(AtlHelperImpl.class);

	private static final String _ENUMERATORS = "Enumerators";

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected AtlHelperImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HelpersPackage.Literals.ATL_HELPER;
	}
	
	private String getQualifiedName(NamedElement ne, NamedElement context) {
		if(ne.eContainer()!=null && ne.eContainer() instanceof NamedElement && !ne.eContainer().equals(context))
			return getQualifiedName((NamedElement)ne.eContainer(), context)+'_'+ne.getName();
		return ne.getName();
	}
	
	private String findstem(List<String> arr) 
    { 
        // Determine size of the array 
        int n = arr.size(); 
  
        // Take first word from array as reference 
        String s = arr.get(0); 
        int len = s.length(); 
  
        String res = ""; 
  
        for (int i = 0; i < len; i++) { 
            for (int j = i + 1; j <= len; j++) { 
  
                // generating all possible substrings 
                // of our reference string arr[0] i.e s 
                String stem = s.substring(i, j); 
                int k = 1; 
                for (k = 1; k < n; k++)  
  
                    // Check if the generated stem is 
                    // common to to all words 
                    if (!arr.get(k).contains(stem)) 
                        break; 
                  
                // If current substring is present in 
                // all strings and its length is greater   
                // than current result 
                if (k == n && res.length() < stem.length()) 
                    res = stem; 
            } 
        } 
  
        return res; 
    } 
	
//	public String uniqueName(NamedElement obj, NamedElement context)
//	{
//		List<String> sameName = new ArrayList<String>();
//		if(!EcoreUtil2.getAllContentsOfType(context, obj.getClass()).contains(obj))
//			return obj.getName();
//		for(EObject content: EcoreUtil2.getAllContentsOfType(context, obj.getClass()))
//		{
//			NamedElement ne = (NamedElement) content;
//			if(ne.getName().equals(obj.getName()) && obj!=ne)
//				sameName.add(getQualifiedName(ne, context));
//		}
//		String objQN = getQualifiedName(obj, context);
//		sameName.add(objQN);
//		
//		if(sameName.size()==1)
//			return obj.getName();
//		
//		String common = findstem(sameName);
//		
//		if(common.length()==0)
//			return objQN;
//		if(common.length()>=objQN.length())
//			return obj.getName();
//		else
//		{
//			String res = "CUT";
//			int index = objQN.indexOf(common);
//			if(index>0)
//				res = res+objQN.substring(0, index);
//			String suffix = objQN.substring(index+common.length());
//			if(suffix.length()>1)
//				res = res + suffix;
//			if(!res.endsWith(obj.getName()))
//				res = res+obj.getName();
//			return res.replaceAll("__", "_");
//		}
//	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOutputPackageName() {
		return outputPackageName;
	}

	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getOutputPackageName(String suffix) {
		if(suffix==null || suffix.isEmpty() || suffix.equalsIgnoreCase("NONE"))
			return outputPackageName;
		return outputPackageName+"_"+suffix;
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOutputPackageName(String newOutputPackageName) {
		String oldOutputPackageName = outputPackageName;
		outputPackageName = newOutputPackageName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.ATL_HELPER__OUTPUT_PACKAGE_NAME, oldOutputPackageName, outputPackageName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public EList<Feature> orderFeatures(ComponentType cpt) {
		EList<Feature> res = new BasicEList<Feature>();
		res.addAll(Aadl2Utils.orderFeatures(cpt));
		return res;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public void copyLocationReference(Element target, Element source) {
		INode node = NodeModelUtils.findActualNodeFor(source);
		int line = -1;

		if (node != null)
			line = node.getStartLine();
		else if (source.getLocationReference() != null) {
			line = source.getLocationReference().getLine();
		}

		String filename = source.eResource().getURI().lastSegment();

		LocationReference lr;

		if (source instanceof BehaviorElement && target instanceof BehaviorElement
				&& source.getLocationReference() != null) {
			AadlBaLocationReference src = (AadlBaLocationReference) source.getLocationReference();
			lr = src.clone();
			lr.setFilename(filename);
		} else {
			lr = new LocationReference(filename, line);
		}

		target.setLocationReference(lr);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public EList<Long> getCurrentPerionReadTable(FeatureInstance port) {
		EList<Long> CPRTable = new BasicEList<Long>();
		try {
			EventDataPortComDimHelper EDPCD = EventDataPortComDimHelperImpl.create(port);

			String CPRString = "";
			long CPRSize = EDPCD.getCprSize();
			_LOGGER.trace("CPRSize " + CPRSize);
			for (int iteration = 0; iteration < CPRSize; iteration++) {
				long CPR_iteration = EDPCD.getCurrentPeriodReadIndex(iteration);
				CPRString = CPRString + String.valueOf(CPR_iteration);
				if (iteration < CPRSize - 1)
					CPRString = CPRString + ", ";
				CPRTable.add(CPR_iteration);
			}
			_LOGGER.trace("CPR Table " + CPRString);
		} catch (DimensioningException e) {
			String errMsg = RamsesException
					.formatRethrowMessage("cannot get current period on read table for \'" + port + '\'', e);
			_LOGGER.error(errMsg, e);
			// ServiceProvider.SYS_ERR_REP.error(errMsg, true);
		}
		return CPRTable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public EList<Long> getCurrentDeadlineWriteTable(FeatureInstance port, FeatureInstance destinationPort) {
		EList<Long> CDWTable = new BasicEList<Long>();
		try {
			EventDataPortComDimHelper EDPCD = EventDataPortComDimHelperImpl.create(destinationPort);
			_LOGGER.trace(port.getInstanceObjectPath());
			long CDWSize = EDPCD.getCdwSize(port);
			_LOGGER.trace("CDWSize " + CDWSize);
			String CDWString = "";
			for (int iteration = 0; iteration < CDWSize; iteration++) {
				long CDW_iteration = EDPCD.getCurrentDeadlineWriteIndex(port, iteration);
				CDWString = CDWString + String.valueOf(CDW_iteration);
				if (iteration < CDWSize - 1)
					CDWString = CDWString + ", ";
				CDWTable.add(CDW_iteration);
			}
			_LOGGER.trace("CDW Table " + CDWString);
		} catch (DimensioningException e) {
			String errMsg = RamsesException.formatRethrowMessage("cannot get current dead line for \'" + port + '\'',
					e);
			_LOGGER.error(errMsg, e);
			// ServiceProvider.SYS_ERR_REP.error(errMsg, true);
		}
		return CDWTable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public long getBufferSize(FeatureInstance destinationFeatureInstance) {
		try {
			EventDataPortComDimHelper EDPCD = EventDataPortComDimHelperImpl.create(destinationFeatureInstance);
			long size = EDPCD.getBufferSize();
			_LOGGER.trace("Buffer size " + size);
			return size;
		} catch (DimensioningException e) {
			String errMsg = RamsesException
					.formatRethrowMessage("cannot get the buffer size for \'" + destinationFeatureInstance + '\'', e);
			_LOGGER.error(errMsg, e);
			// ServiceProvider.SYS_ERR_REP.error(errMsg, true);
		}
		return 0;
	}

	public DirectionType getDirection(String direction) {
		if (direction.equals("in")) {
			return DirectionType.IN;
		} else if (direction.equals("out")) {
			return DirectionType.OUT;
		} else {
			return DirectionType.IN_OUT;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public void setDirection(DirectedFeature feature, String direction) {
		if (direction.equals("in")) {
			feature.setIn(true);
			feature.setOut(false);
		} else if (direction.equals("out")) {
			feature.setIn(false);
			feature.setOut(true);
		} else {
			feature.setIn(true);
			feature.setOut(true);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	
	public boolean isReferencedIn(Element root, Element elt)
	{
		for (Element content : EcoreUtil2.getAllContentsOfType(root, Element.class)) {
			if(content.equals(elt))
				return true;
			else if(content instanceof ElementHolder)
			{
				ElementHolder eh = (ElementHolder) content;
				if(eh.getElement().equals(elt))
					return true;
			}
		}
		return false;
	}
	
	public boolean isUsedInSpecialOperator(BehaviorAnnex ba, Port p, String operatorName) {
		if (operatorName.equalsIgnoreCase("fresh"))
			return AadlBaVisitors.isFresh(ba, p);
		else if (operatorName.equalsIgnoreCase("count")) {
			for (PortCountValue pcv : EcoreUtil2.getAllContentsOfType(ba, PortCountValue.class)) {
				if (pcv.getElement().equals(p))
					return true;
			}
		}
		else if(operatorName.equalsIgnoreCase("dispatch"))
		{
			for (DispatchTriggerLogicalExpression dtle : EcoreUtil2.getAllContentsOfType(ba, DispatchTriggerLogicalExpression.class)) {
				for(DispatchConjunction dc: dtle.getDispatchConjunctions())
					for(DispatchTrigger dt: dc.getDispatchTriggers())
					{
						if(dt instanceof EventDataPortHolder)
						{
							EventDataPortHolder edph = (EventDataPortHolder) dt;
							if(edph.geteventDataPort().equals(p))
								return true;
						}
						else if(dt instanceof EventPortHolder)
						{
							EventPortHolder eph = (EventPortHolder) dt;
							if(eph.getEventPort().equals(p))
								return true;
						}
					}
			}
		}
		return false;
	}

	public List<FeatureInstance> getFeaturesOrderedByCriticality(ComponentInstance ci) {
		List<FeatureInstance> result = new ArrayList<FeatureInstance>();
		List<ComparablePortByCriticality> cpbcList = new ArrayList<ComparablePortByCriticality>();
		for (FeatureInstance fi : ci.getFeatureInstances()) {
			if (fi.getFeature() instanceof Port) {
				ComparablePortByCriticality cpbc = new ComparablePortByCriticality(fi);
				cpbcList.add(cpbc);
			}
		}
		Collections.sort(cpbcList);
		for (ComparablePortByCriticality iter : cpbcList) {
			result.add(iter.getFeatureInstance());
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public long getHyperperiod(FeatureInstance port) {
		Long hyperperiod = 0L;
		List<ComponentInstance> threads = new ArrayList<ComponentInstance>();
		for (ConnectionInstance fi : port.getDstConnectionInstances()) {
			threads.add((ComponentInstance) fi.getSource().eContainer());
		}
		threads.add((ComponentInstance) port.eContainer());
		hyperperiod = getHyperperiod(threads);
		return hyperperiod;
	}


	
	public long getHyperperiod(List<ComponentInstance> consideredTasks, String scale)
	{
		return AadlHelperImpl.getHyperperiod(consideredTasks, scale);
	}

	public long getHyperperiod(List<ComponentInstance> consideredTasks) {
		return AadlHelperImpl.getHyperperiod(consideredTasks);
	}

	public Long getBaseperiodFromThreads(Set<ComponentInstance> consideredTasks, UnitLiteral ul) {
		return getBaseperiod(consideredTasks, ul);
	}

	public long getBaseperiod(Set<ComponentInstance> consideredTasks, UnitLiteral ul) {
		Set<Long> consideredPeriods = new HashSet<Long>();
		for (ComponentInstance ci : consideredTasks) {
			Long result = PropertyUtils.getIntValue(ci, "Period", ul.getName());
			consideredPeriods.add(result);
		}
		Long[] periods = new Long[consideredPeriods.size()];
		consideredPeriods.toArray(periods);
		if (consideredPeriods.size() == 1)
			return periods[0];
		return MathHelperImpl.gcd(periods);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public String getTimingPrecision(NamedElement ne) {
		return AadlHelperImpl.getPrecision(ne);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public EList<String> getListOfPath(PropertyAssociation pa) {
		List<String> res = new BasicEList<String>();
		ListValue lv = (ListValue) pa.getOwnedValues().get(0).getOwnedValue();
		URI dirURI = pa.eResource().getURI();

		String path = "";
//	    if(RamsesConfiguration.USES_GUI)
		// DB: No more OSGI
		if (Platform.isRunning()) {
			IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			String projectName = dirURI.toPlatformString(true).substring(1);
			projectName = projectName.substring(0, projectName.indexOf("/"));

			IProject project = workspaceRoot.getProject(projectName);

			path = project.getLocation().toOSString();
			path = path.substring(0, path.lastIndexOf(File.separator));
			path = path + dirURI.toPlatformString(true);
		} else {
			if (dirURI.isFile())
				path = dirURI.toFileString();
			else
				path = dirURI.toString();
		}

		int index = path.lastIndexOf(File.separator);
		path = path.substring(0, index + 1);
		
		if(dirURI.lastSegment().endsWith("aaxl2"))
		{
			index = path.lastIndexOf(File.separator);
			path = path.substring(0, index);
			index = path.lastIndexOf(File.separator);
			path = path.substring(0, index +1 );
		}
		
//	    String path = "";
//	    if(dirURI.isFile())
//	      path = dirURI.toFileString();
//	    else
//	      path = dirURI.toString();
//	    int index = path.lastIndexOf(File.separator);
//	    path = path.substring(0, index+1);

		for (PropertyExpression pe : lv.getOwnedListElements()) {
			StringLiteral sl = (StringLiteral) pe;
			String fileName = sl.getValue();
			File f = new File(fileName);
			if (f.exists())
				res.add(fileName);
			else {
				File prefixedF = new File(path + fileName);
				if (prefixedF.exists())
					res.add(path + fileName);
			}
		}
		return (EList<String>) res;
	}

	void allPortCount(List<PortCountValue> result, EObject e) {
		if (e instanceof BehaviorTransition) {
			BehaviorTransition bt = (BehaviorTransition) e;
			if (bt.getActionBlock() != null)
				allPortCount(result, bt.getActionBlock());
		}
		for (EObject be : EcoreUtil2.getAllContentsOfType(e, EObject.class)) {
			if (be instanceof PortCountValue)
				result.add((PortCountValue) be);
			else
				allPortCount(result, be);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public EList<Port> allPortCount(BehaviorElement e) {
		List<PortCountValue> pcvList = new ArrayList<PortCountValue>();
		allPortCount(pcvList, e);

		Set<Port> pList = new HashSet<Port>();
		for (PortCountValue pcv : pcvList) {
			pList.add((Port) pcv.getElement());
		}
		EList<Port> res = new BasicEList<Port>(pList);
		return res;
	}
	
	void allPortFresh(List<PortFreshValue> result, EObject e) {
		if (e instanceof BehaviorTransition) {
			BehaviorTransition bt = (BehaviorTransition) e;
			if (bt.getActionBlock() != null)
				allPortFresh(result, bt.getActionBlock());
		}
		for (EObject be : EcoreUtil2.getAllContentsOfType(e, EObject.class)) {
			if (be instanceof PortFreshValue)
				result.add((PortFreshValue) be);
			else
				allPortFresh(result, be);
		}
	}
	
	public EList<Port> allPortFresh(BehaviorElement e) {
		List<PortFreshValue> pfvList = new ArrayList<PortFreshValue>();
		allPortFresh(pfvList, e);

		Set<Port> pList = new HashSet<Port>();
		for (PortFreshValue pfv : pfvList) {
			pList.add((Port) pfv.getElement());
		}
		EList<Port> res = new BasicEList<Port>(pList);
		return res;
	}

	public EList<Port> allPortDispatch(BehaviorElement e) {
		List<DispatchTriggerLogicalExpression> dtleList = new ArrayList<DispatchTriggerLogicalExpression>();
		allDispatchTrigger(dtleList, e);

		Set<Port> pList = new HashSet<Port>();
		for (DispatchTriggerLogicalExpression dtle : dtleList) {
			for(DispatchConjunction dc: dtle.getDispatchConjunctions())
				for(DispatchTrigger dt: dc.getDispatchTriggers())
					if(dt instanceof EventDataPortHolder)
					{
						EventDataPortHolder edph = (EventDataPortHolder) dt;
						pList.add(edph.geteventDataPort());
					}
					else if(dt instanceof EventPortHolder)
					{
						EventPortHolder eph = (EventPortHolder) dt;
						pList.add(eph.getEventPort());
					}
		}
		EList<Port> res = new BasicEList<Port>(pList);
		return res;
	}
	
	private void allDispatchTrigger(List<DispatchTriggerLogicalExpression> result, EObject e) {
		if (e instanceof BehaviorTransition) {
			BehaviorTransition bt = (BehaviorTransition) e;
			if (bt.getActionBlock() != null)
				allDispatchTrigger(result, bt.getActionBlock());
		}
		for (EObject be : EcoreUtil2.getAllContentsOfType(e, EObject.class)) {
			if (be instanceof DispatchTriggerLogicalExpression)
				result.add((DispatchTriggerLogicalExpression) be);
			else
				allDispatchTrigger(result, be);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public StringLiteral getStringLiteral(PropertyAssociation pa, String stringLiteralValue) {
		Element el = null;
		EList<PropertyExpression> pes = PropertyUtils.getPropertyExpression(pa);
		for (PropertyExpression pe : pes) {
			el = PropertyUtils.getValue(pe, stringLiteralValue);

			if (el != null) {
				return (StringLiteral) el;
			}
		}

		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public PropertyAssociation getEnumerators(Classifier classifier) {
		return PropertyUtils.findPropertyAssociation(_ENUMERATORS, classifier);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String uniqueName(NamedElement obj, NamedElement context) {
		List<String> sameName = new ArrayList<String>();
		if(!EcoreUtil2.getAllContentsOfType(context, obj.getClass()).contains(obj))
			return obj.getName();
		for(EObject content: EcoreUtil2.getAllContentsOfType(context, obj.getClass()))
		{
			NamedElement ne = (NamedElement) content;
			if(ne.getName().equals(obj.getName()) && obj!=ne)
				sameName.add(getQualifiedName(ne, context));
		}
		String objQN = getQualifiedName(obj, context);
		sameName.add(objQN);
		
		if(sameName.size()==1)
			return obj.getName();
		
		String common = findstem(sameName);
		
		if(common.length()==0)
			return objQN;
		if(common.length()>=objQN.length())
			return obj.getName();
		else
		{
			String res = "CUT";
			int index = objQN.indexOf(common);
			if(index>0)
				res = res+objQN.substring(0, index);
			String suffix = objQN.substring(index+common.length());
			if(suffix.length()>1)
				res = res + suffix;
			if(!res.endsWith(obj.getName()))
				res = res+obj.getName();
			return res.replaceAll("__", "_");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Long getHyperperiodFromThreads(EList<ComponentInstance> consideredTasks, String scale) {
		  return getHyperperiod(consideredTasks, scale);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getSchedTableInit(ComponentInstance c, ModeInstance mi) {
		String res = "{";
		
		if(this.schedTimes.isEmpty())
			getSchedulingEventTimeList(c);
		/**
		 * 
		 * 	--  in java code: get a representation of type
			--  LO --> cpu1 --> |0..1000 ; t1|1000..2000 ; t2|2000..3000 ; t1|
			--     --> cpu2 --> |0..1000 ; t3|1000..2000 ; t3|
			--
			
			This is in schedTablePerMode
			
			--  then browse list of time of interest
			--  etc.
			--
			--  get list of times of interest (0, 1000, 2000, 3000)

			This is in schedTimes
			
			--  given mi.mode
			--     for each time of interest
			--       produce:
			--          * the number of tasks to release
			--          * the table of tasks to release
			--          * the number of tasks having their deadlines at the time of interest
			--          * a table of tasks having their deadlines at the time of interest
			--          * the number of used core
			--          * a table of tasks using cores
			--          * a table of core used per task
			--          * the number of tasks to promote
			--          * a table of tasks promotion
			--          * the number of tasks to degrade
			--          * a table of tasks degradation
			
			This is done bellow
			
		 * 
		 * */
		
		EList<ComponentInstance> threadList = new BasicEList<ComponentInstance>();
		for(ComponentInstance subInstance: c.getComponentInstances())
		{
			if(subInstance.getCategory()==ComponentCategory.THREAD)
			{
				threadList.add(subInstance);
			}
		}
		
		// TODO: for performance reasons, extract hyperperiod from intermediate model (frame property associated to core)
		Double hyperperiod = Double.valueOf(this.getHyperperiodFromThreads(threadList, "ms"));
		
		ComponentInstance cpu = (ComponentInstance)
				AadlHelperImpl.getDeloymentProcessorInstance(c);
		
		List<ComponentInstance> coreList = new ArrayList<ComponentInstance>();
		for(ComponentInstance subInstance: cpu.getComponentInstances())
		{
			if(subInstance.getCategory()==ComponentCategory.PROCESSOR)
			{
				coreList.add(subInstance);
			}
		}
		
		Map<ComponentInstance, Double> executedTimePerTask = new HashMap<ComponentInstance, Double>();
		
		Map<Double, Set<Integer>> tasksToReleaseMap = new HashMap<Double, Set<Integer>>();
		Map<Double, Set<Integer>> tasksDeadlineMap = new HashMap<Double, Set<Integer>>();
		
		Map<Double, Set<Integer>> usedCoresMap = new HashMap<Double, Set<Integer>>();
		Map<Double, Set<Integer>> tasksPerCoreMap = new HashMap<Double, Set<Integer>>();
		
		Map<Double, Set<Integer>> tasksToPromoteMap = new HashMap<Double, Set<Integer>>();
		Map<Double, Set<Integer>> tasksToDegradeMap = new HashMap<Double, Set<Integer>>();
				
		Map<ComponentInstance, Set<SchedInterval>> SchedTablePerCore = 
				this.schedTablePerMode.get(mi.getMode());
		
		
		for(Double schedTime: schedTimes)
		{
			if(SchedTablePerCore == null)
				continue;
			for(ComponentInstance core : SchedTablePerCore.keySet())
			{
				Set<SchedInterval> schedList = SchedTablePerCore.get(core);
				for(SchedInterval schedInterval: schedList)
				{
					if(schedTime<schedInterval.startTime
							|| schedTime>schedInterval.endTime)
						continue;

					Set<Integer> tasksToRelease = tasksToReleaseMap.get(schedTime);
					if(tasksToRelease == null)
					{
						tasksToRelease = new LinkedHashSet<Integer>();
						tasksToReleaseMap.put(schedTime, tasksToRelease);
					}

					Set<Integer> tasksDeadline = tasksDeadlineMap.get(schedTime);
					if(tasksDeadline == null)
					{
						tasksDeadline = new LinkedHashSet<Integer>();
						tasksDeadlineMap.put(schedTime, tasksDeadline);
					}

					Set<Integer> tasksToPromote = tasksToPromoteMap.get(schedTime);
					if(tasksToPromote == null)
					{
						tasksToPromote = new LinkedHashSet<Integer>();
						tasksToPromoteMap.put(schedTime, tasksToPromote);
					}

					Set<Integer> tasksToDegrade = tasksToDegradeMap.get(schedTime);
					if(tasksToDegrade == null)
					{
						tasksToDegrade = new LinkedHashSet<Integer>();
						tasksToDegradeMap.put(schedTime, tasksToDegrade);
					}

					Set<Integer> usedCores = usedCoresMap.get(schedTime);
					if(usedCores == null)
					{
						usedCores = new LinkedHashSet<Integer>();
						usedCoresMap.put(schedTime, usedCores);
					}

					Set<Integer> tasksPerCore = tasksPerCoreMap.get(schedTime);
					if(tasksPerCore == null)
					{
						tasksPerCore = new LinkedHashSet<Integer>();
						tasksPerCoreMap.put(schedTime, tasksPerCore);
					}

					if(schedTime == schedInterval.startTime)
					{
						int taskId = threadList.indexOf(schedInterval.task);
						Double cumulatedExecTime = executedTimePerTask.get(schedInterval.task);
						if(cumulatedExecTime == null)
						{
							cumulatedExecTime = 0.0;
						}
						if(cumulatedExecTime.equals(0.0))
						{
							tasksToRelease.add(taskId);
						}
						else
							tasksToPromote.add(taskId);

						cumulatedExecTime = cumulatedExecTime + schedInterval.endTime - schedInterval.startTime;
						executedTimePerTask.put(schedInterval.task, cumulatedExecTime);


						usedCores.add(coreList.indexOf(core));
						tasksPerCore.add(taskId);
					}

					if(schedTime == schedInterval.endTime)
					{
						Double cumulatedExecTime = executedTimePerTask.get(schedInterval.task);
						
						Double taskWCET = getComputeExecutionTime(schedInterval.task, mi);
						if(cumulatedExecTime.equals(taskWCET))
						{
							cumulatedExecTime = 0.0;
							tasksDeadline.add(threadList.indexOf(schedInterval.task));
						}
						else
							tasksToDegrade.add(threadList.indexOf(schedInterval.task));

						executedTimePerTask.put(schedInterval.task, cumulatedExecTime);
					}

					if(schedTime>schedInterval.startTime
							&& schedTime<schedInterval.endTime)
					{
						// 
					}

				}
			}
			// sched_event
			
			/***
			 *   
			 *   unsigned int sched_time;
			 *   // tasks to release
			 *   unsigned int task_release_nb;
			 *   unsigned int * task_release_idx;
			 *   // tasks deadline
			 *   unsigned int task_deadline_nb;
			 *   unsigned int * task_deadline_idx;
			 *   // tasks placement
			 *   unsigned int task_placement_nb; // note: initialize to tasks_nb at time 0
			 *   unsigned int * task_placement_idx; // note: used to bind all threads at time 0
			 *   unsigned int * placement_core;
			 *   // tasks promotion
			 *   unsigned int task_promotion_nb;
			 *   unsigned int * task_promotion_idx;
			 *   // tasks degradation
			 *   unsigned int task_degradation_nb;
			 *   unsigned int * task_degradation_idx;
			 *   
			 ***/
			
			res+="{";
			res+=schedTime.intValue()+",";
			
			int s = 0;
			if(schedTime.equals(hyperperiod))
			{
				s=-1;
			}
			else
				s=tasksToReleaseMap.get(schedTime).size();
			
			res+= s;
			res+=",";
			
			res+="{";
			int k=0;
			Set<Integer> tasksToRelease = tasksToReleaseMap.get(schedTime);
			if(tasksToRelease == null)
				tasksToRelease = new HashSet<Integer>();
			for(Integer i : tasksToRelease)
			{
				res+=i;
				k++;
				if(k!=s)
					res+=",";
			}
			res+="},";
			
			Set<Integer> tasksDeadlines = tasksDeadlineMap.get(schedTime);
			if(tasksDeadlines == null)
				tasksDeadlines = new HashSet<Integer>();
			s = tasksDeadlines.size();
			res+=s;
			res+=",";
			
			res+="{";
			k=0;
			for(Integer i : tasksDeadlines)
			{
				res+=i;
				k++;
				if(k!=s)
					res+=",";
			}
			res+="},";
			
			Set<Integer> tasksPerCore = tasksPerCoreMap.get(schedTime);
			if(tasksPerCore == null)
				tasksPerCore = new HashSet<Integer>();
			s = tasksPerCore.size();
			res+=s;
			res+=",";
			
			res+="{";
			k=0;
			for(Integer i: tasksPerCore)
			{
				res+=i;
				k++;
				if(k!=s)
					res+=",";
			}
			res+="},";
			
			res+="{";
			k=0;
			
			Set<Integer> usedCores = usedCoresMap.get(schedTime);
			if(usedCores == null)
				usedCores = new HashSet<Integer>();
			for(Integer i: usedCores)
			{
				res+=i;
				k++;
				if(k!=s)
					res+=",";
			}
			res+="},";
			
			
			Set<Integer> tasksToPromote = tasksToPromoteMap.get(schedTime);
			if(tasksToPromote == null)
				tasksToPromote = new HashSet<Integer>();
			
			s = tasksToPromote.size();
			res+=s;
			res+=",";
			
			res+="{";
			k=0;
			for(Integer i: tasksToPromote)
			{
				res+=i;
				k++;
				if(k!=s)
					res+=",";
			}
			res+="},";
			
			Set<Integer> tasksToDegrade = tasksToDegradeMap.get(schedTime);
			if(tasksToDegrade == null)
				tasksToDegrade = new HashSet<Integer>();
			
			s = tasksToDegrade.size();
			res+=s;
			res+=",";
			
			res+="{";
			k=0;
			for(Integer i: tasksToDegrade)
			{
				res+=i;
				k++;
				if(k!=s)
					res+=",";
			}
			res+="}";
			
			// close sched_event init
			res+="}";
			if(schedTime!=schedTimes.get(schedTimes.size()-1))
				res+=",";
		
			

			
		}



		res+="}";
		
		// System.out.println("\n\n\t RES in "+mi.getMode().getName()+" =" + res);
		
		
		
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	
	private EList<ComponentInstance> cpuToTransform = new BasicEList<ComponentInstance>();
    
	@Override
	public boolean deployedOnTransformedCpu(ComponentInstance c) {
		if(cpuToTransform.isEmpty())
			return true;
	    if(c instanceof SystemInstance)
	      return false;
	    List<ComponentInstance> execUnit =
	    		PropertyUtils
	    		.getComponentInstanceList(c,
	    				"Actual_Processor_Binding") ;
	    if(execUnit==null)
	    {
	    	if(isContainedBy(cpuToTransform, c))
		    	return true;
	    	else
	    		return false;
	    }
	    if(execUnit.size()==1)
	    {
	      ComponentInstance exec = execUnit.get(0);
	      if(isContainedBy(cpuToTransform, exec))
	    	return true;
	      else
	    	return deployedOnTransformedCpu(exec);
	    }
	    return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<ComponentInstance> getCpuToTransform() {
		return this.cpuToTransform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void resetCpuToTransform(EList<ComponentInstance> cpuList) {
		this.cpuToTransform = cpuList;
	}

	public long getFloor(Double d) {
		return d.longValue();
	}

	public long getCeil(Double d) {
		Double res = Math.ceil(d);
		return res.longValue();
	}

	// Dirty workaround as ATL dos not support long data type...
	public Integer getInteger(Long l) {
		return l.intValue();
	}

	public Integer minus(Long lhs, Long rhs) {
		return (int) (lhs - rhs);
	}

	public Integer plus(Long lhs, Long rhs) {
		return (int) (lhs + rhs);
	}

	public Integer modulo(Integer nb, Integer mod) {
		Integer l = nb % mod;
		return l.intValue();
	}

	public boolean strictly_greater_than(Long lhs, Long rhs) {

		return lhs > rhs;
	}

	public Object log(String msg, Object o) {
		if (o instanceof NamedElement) {
			NamedElement ne = (NamedElement) o;
			_LOGGER.trace("\t" + ne.getFullName() + ": " + msg);
		} else if (o instanceof Boolean) {
			Boolean b = (Boolean) o;
			_LOGGER.trace("\t" + b.toString() + ": " + msg);
		} else if (o != null)
			_LOGGER.trace("\t" + o.toString() + ": " + msg);
		else
			_LOGGER.trace("\t NULL: " + msg);
		return o;
	}

	private List<ComponentInstance> cpuToIgnore;

	public List<ComponentInstance> getCpuToIgnore() {
		return cpuToIgnore;
	}

	public void resetCpuToIgnore(List<ComponentInstance> cpuList) {
		cpuToIgnore = cpuList;
	}

	public boolean deployedOnIgnoredCpu(ComponentInstance c) throws RamsesException {
		if (c instanceof SystemInstance)
			return false;
		if (c.getContainingComponentImpl() == null)
			return false; // workaround bug in osate
		List<ComponentInstance> execUnit = PropertyUtils.getComponentInstanceList(c, "Actual_Processor_Binding");
		if (execUnit.size() == 1) {
			ComponentInstance exec = execUnit.get(0);
			if (isContainedBy(cpuToIgnore, exec))
				return true;
		} else
			throw new RamsesException("Thread instance " + c.getName()
					+ " is deployed on several execution units (processor or " + "virtual processo components)");
		return false;
	}

	private boolean isContainedBy(List<ComponentInstance> cpuToIgnore, ComponentInstance execUnit) {
		if (cpuToIgnore.contains(execUnit))
			return true;
		else if (execUnit.eContainer() == null || execUnit.eContainer() instanceof SystemInstance)
			return false;
		else
			return isContainedBy(cpuToIgnore, (ComponentInstance) execUnit.eContainer());
	}

	public PropertyAssociation findPropertyAssociation(String propertyName, NamedElement ne) {
		return PropertyUtils.findPropertyAssociation(propertyName, ne);
	}
	
	public Property findProperty(List<PropertySet> propertySetList, String propertyName)
	{
		String propertySetName=null;
		if(propertyName.contains("::"))
		{
			int index = propertyName.indexOf("::");
			propertySetName = propertyName.substring(0, index);
			propertyName = propertyName.substring(index+2);
		}
		for(PropertySet ps:propertySetList)
		{
			if(propertySetName!=null && !ps.getName().equalsIgnoreCase(propertySetName))
				continue;
			for(Property p:ps.getOwnedProperties())
				if(p.getName().equalsIgnoreCase(propertyName))
					return p;
		}
		return null;
	}

	private static final String EXECUTION_SLOT_PROPERTY_NAME = "Execution_Slots";

	public Long getSchedTableSize(ComponentInstance c) {
		long res = schedTimes.size();
		if (res > 0)
			return res;

		res = Long.valueOf(getSchedulingEventTimeList(c).size());

		return res;
	}

	Map<Mode, Map<ComponentInstance, Set<SchedInterval>>> schedTablePerMode = new LinkedHashMap<Mode, Map<ComponentInstance, Set<SchedInterval>>>();

	class SchedInterval implements Comparable<SchedInterval> {
		ComponentInstance task;
		double startTime;
		double endTime;

		@Override
		public boolean equals(Object object) {
			if (object == null) {
				return false;
			}
			if (object instanceof SchedInterval) {
				SchedInterval theObj = (SchedInterval) object;
				boolean res = (theObj.task == this.task && theObj.startTime == this.startTime
						&& theObj.endTime == this.endTime);
				return res;
			}
			return false;
		}

		@Override
		public int hashCode() {
			return 0;
		}

		@Override
		public int compareTo(SchedInterval o) {
			Double res = this.startTime - o.startTime;
			return res.intValue();
		}
	}

	List<Double> schedTimes = new ArrayList<Double>();

	private List<Double> getSchedulingEventTimeList(ComponentInstance c)
	{
		Double hyperperiod = Double.valueOf(0);
		
		EList<ComponentInstance> threads = new BasicEList<ComponentInstance>();
		
		for(ComponentInstance subInstance: c.getComponentInstances())
		{
			if(subInstance.getCategory()==ComponentCategory.THREAD)
			{
				threads.add(subInstance);
				PropertyAssociation pa = 
						PropertyUtils.findPropertyAssociation(EXECUTION_SLOT_PROPERTY_NAME, subInstance);
				
				if(pa==null)
					continue;
				for(ModalPropertyValue mpv:pa.getOwnedValues())
				{
					PropertyExpression expr = mpv.getOwnedValue();
					if (expr instanceof ListValue) {
			            ListValue lv = (ListValue) expr;
			            for (PropertyExpression pe : lv.getOwnedListElements()) {
			              
			              ComponentInstance core=null;
						  Double startTimeMs=-1.0;
						  Double endTimeMs=-1.0;
			              if(pe instanceof RecordValue)
			              {
			                RecordValue rv = (RecordValue) pe;
			                for(BasicPropertyAssociation bpa: rv.getOwnedFieldValues())
			                {
			                  if(bpa.getProperty().getName().equalsIgnoreCase("Computation_Unit"))
			                  {
			                	  InstanceReferenceValue coreRef = (InstanceReferenceValue)bpa.getValue();
			                	  core = (ComponentInstance) coreRef.getReferencedInstanceObject();
			                  }
			                  else if(bpa.getProperty().getName().equalsIgnoreCase("Start_Time"))
			                  {
			                    IntegerLiteral startTime = (IntegerLiteral) bpa.getValue();
			                    startTimeMs = startTime.getScaledValue("ms");
			                    if(!schedTimes.contains(startTimeMs))
			                    	schedTimes.add(startTimeMs);
			                  }
			                  else if(bpa.getProperty().getName().equalsIgnoreCase("End_Time"))
			                  {
			                	  IntegerLiteral endTime = (IntegerLiteral) bpa.getValue();
				                  endTimeMs = endTime.getScaledValue("ms");
				                  if(!schedTimes.contains(endTimeMs))
				                	  schedTimes.add(endTimeMs);
			                  }
			                }
			              }
			              
			              List<Mode> mList = mpv.getInModes();
			              for(Mode theMode: mList)
			              {
			            	  Mode m = theMode;
			            	  if(m instanceof SystemOperationMode)
			            	  {
			            		  SystemOperationMode som = (SystemOperationMode) m;
			            		  for(ModeInstance mi_som: som.getCurrentModes())
			            			  if(c.getModeInstances().contains(mi_som))
			            			  {
			            				  m = mi_som.getMode();
			            				  break;
			            			  }

			            	  }
			            	  Map<ComponentInstance, Set<SchedInterval>> schedTablePerCore = schedTablePerMode.get(m);
			            	  if(schedTablePerCore==null)
			            	  {
			            		  schedTablePerCore = new LinkedHashMap<ComponentInstance, Set<AtlHelperImpl.SchedInterval>>();
			            		  schedTablePerMode.put(m, schedTablePerCore);
			            	  }
			            	  Set<SchedInterval> schedTable = schedTablePerCore.get(core);
			            	  if(schedTable==null)
			            	  {
			            		  schedTable = new TreeSet<AtlHelperImpl.SchedInterval>();
			            		  schedTablePerCore.put(core, schedTable);
			            	  }
			            	  AtlHelperImpl.SchedInterval schedInt = new AtlHelperImpl.SchedInterval();
			            	  schedInt.task = subInstance;
			            	  schedInt.startTime = startTimeMs;
			            	  schedInt.endTime = endTimeMs;
			            	  schedTable.add(schedInt);
			            	  
			            	  /*System.out.println("");
			            	  System.out.println("Mode: "+m.getName());
			            	  System.out.println("Core: "+core.getName());
			            	  System.out.println("Task: "+subInstance.getName()+
										" start: "+startTimeMs+
										" end: "+endTimeMs);
			            	  System.out.println("");*/
			              }

			            }
					}
				}
			}
		}
		
		// TODO: for performance reasons, extract hyperperiod from intermediate model (frame property associated to core)
		hyperperiod = Double.valueOf(this.getHyperperiodFromThreads(threads, "ms"));
		if(!schedTimes.contains(hyperperiod))
			schedTimes.add(hyperperiod);
		Collections.sort(schedTimes);
		
//		for(ModeInstance m: c.getModeInstances())
//		{
//			System.out.println("");
//			System.out.println("Mode: "+m.getMode().getName());
//			Map<ComponentInstance, Set<SchedInterval>> schedTablePerCore = schedTablePerMode.get(m.getMode());
//			for(ComponentInstance core : schedTablePerCore.keySet())
//			{
//				System.out.println("Core: "+core.getName());
//				Set<SchedInterval> schedList = schedTablePerCore.get(core);
//				for(SchedInterval schedInterval: schedList)
//				{
//					System.out.println("Task: "+schedInterval.task.getName()+
//							" start: "+schedInterval.startTime+
//							" end: "+schedInterval.endTime);
//				}
//			}
//			System.out.println("");
//		}
		return schedTimes;
	}
 
	private Double getComputeExecutionTime(ComponentInstance ci, ModeInstance mi) {
		PropertyAssociation pa = findPropertyAssociation("Compute_Execution_Time", ci);
		if (pa == null) {
			String errMsg = "Cannot get compute_execution_time for component " + ci.getName();
			_LOGGER.error(errMsg);
			// ServiceProvider.SYS_ERR_REP.error(errMsg, false);
		}
		List<ModalPropertyValue> values = pa.getOwnedValues();
		for (ModalPropertyValue mpv : values)
			if (mpv.getInModes().isEmpty()) {
				RangeValue rv = (RangeValue) mpv.getOwnedValue();
				IntegerLiteral il = (IntegerLiteral) rv.getMaximumValue();
				return il.getScaledValue("ms");
			} else {
				SystemOperationMode som = (SystemOperationMode) mpv.getInModes().get(0);
				if (som.getCurrentModes().contains(mi)) {
					RangeValue rv = (RangeValue) mpv.getOwnedValue();
					IntegerLiteral il = (IntegerLiteral) rv.getMaximumValue();
					return il.getScaledValue("ms");
				}
			}
		return null;
	}
	
	public int computeExecutionTime(List<NamedElement> neList, SubprogramClassifier sc)
	{
		Double res=0.0;
		for(int i=0;i<neList.size();i++)
		{
			res = res + computeExecutionTime(neList.get(i), sc);
		}
		return (int) Math.ceil(res);
	}
	
	public Double computeExecutionTime(NamedElement neData, NamedElement neBehavior)
	{
		Double size = DataSizeHelper.getOrComputeDataSize(neData);
		return computeExecutionTime(size, neBehavior);
	}
	
	public Double computeExecutionTime(Double bytes, NamedElement cc)
	{
		Double res=0.0;
		PropertyAssociation pa = PropertyUtils.findPropertyAssociation("Linear_Execution_Time", cc);
		if(pa==null)
			return res;
		ModalPropertyValue mpv = pa.getOwnedValues().get(0);
		RecordValue rv = (RecordValue) mpv.getOwnedValue();
		for(BasicPropertyAssociation bpa: rv.getOwnedFieldValues())
		{
			BasicProperty bp = bpa.getProperty();
			if(bp.getName().equalsIgnoreCase("Fixed"))
				res+=((IntegerLiteral)bpa.getOwnedValue()).getScaledValue("ms");
			else if(bp.getName().equalsIgnoreCase("SumOf"))
			{
				ListValue lv = (ListValue) bpa.getOwnedValue();
				for(PropertyExpression pe: lv.getOwnedListElements())
				{
					RecordValue rvInList = (RecordValue) pe;
					for(BasicPropertyAssociation bpaInList: rvInList.getOwnedFieldValues())
					{
						BasicProperty bpInList = bpaInList.getProperty();
						if(bpInList.getName().equalsIgnoreCase("PerByte"))
							res+=((IntegerLiteral)bpaInList.getOwnedValue()).getScaledValue("ms")*bytes;
					}
				}
			}
		}
		return res;
	}

	public Long getOSMaxPriority(NamedElement ne) {
		return AadlHelperImpl.getMaxPriority(ne);
	}

	private Map<ComponentInstance, List<String>> alreadyTakenNames = new HashMap<ComponentInstance, List<String>>();

	public String limitStringLength(ComponentInstance ci, int limit, String toCheck) {
		if (!alreadyTakenNames.containsKey(ci))
			alreadyTakenNames.put(ci, new ArrayList<String>());
		return CodeGenHelperImpl.limitCharNb(toCheck, limit, alreadyTakenNames.get(ci));
	}

	public String getIndex(InstanceObject inst) {
		if (inst instanceof ComponentInstance) {
			ComponentInstance ci = (ComponentInstance) inst;
			if (ci.getIndices().isEmpty() || ci.getIndices().get(0) == 0)
				return "";
			return "_" + ci.getIndices().get(0).toString();
		} else if (inst instanceof FeatureInstance) {
			FeatureInstance fi = (FeatureInstance) inst;
			if (fi.getIndex() == 0)
				return "";
			return "_" + fi.getIndex();
		} else
			return "";
	}

	public List<NamedElement> asSetByName(List<NamedElement> inputList) {
		List<NamedElement> res = new ArrayList<NamedElement>();
		Set<String> foundNames = new HashSet<String>();

		for (NamedElement ne : inputList) {
			if (foundNames.contains(ne.getName()))
				continue;
			else {
				res.add(ne);
				foundNames.add(ne.getName());
			}
		}
		return res;
	}
	
	public List<InstanceReferenceValue> referencedInstance(PropertyAssociation pa)
	{
		List<InstanceReferenceValue> res = EcoreUtil2.getAllContentsOfType(pa, InstanceReferenceValue.class);
		return res;
	}
	
	public class ParentRecorder {
		
		public ParentRecorder(FeatureInstance v) {
			feature = v;
			parent = null;
		}
		
		
		
		public ParentRecorder(ParentRecorder parent, FeatureInstance v) {
			feature = v;
			this.parent = parent;
		}



		public ParentRecorder parent;
		public FeatureInstance feature;
	}
	
	public List<FeatureInstance> getConnectionEndFeatureList(FeatureInstance processFeature, FeatureInstance leafFeature)
	{
		// kind of iterative dfs
		
		List<FeatureInstance> visited = new ArrayList<FeatureInstance>();
        
        Stack<FeatureInstance> stack = new Stack<FeatureInstance>(); 
        
        Stack<ParentRecorder> parentStack = new Stack<ParentRecorder>(); 
        stack.push(processFeature); 
        
        ParentRecorder res = new ParentRecorder(processFeature);
        parentStack.push(res);
        
        while(stack.empty() == false) 
        { 
            FeatureInstance v = stack.peek(); 
            stack.pop(); 
            
            ParentRecorder current = parentStack.peek();
            parentStack.pop();
            
            if(visited.contains(v) == false) 
                visited.add(v);
            
            if(v == leafFeature)
            {
            	res = current;
            	break;
            }
            Iterator<FeatureInstance> itr = v.getFeatureInstances().iterator(); 
              
            while (itr.hasNext())  
            {
            	FeatureInstance n = itr.next(); 
                if(!visited.contains(n)) 
                {
                    stack.push(n);
                    ParentRecorder pr = new ParentRecorder(current, n);
                    parentStack.push(pr);
                }
            }
        }
        
        // convert stack to list
        List<FeatureInstance> featureList = new ArrayList<FeatureInstance>();
        while(res.parent != null)
        {
        	featureList.add(0, res.feature);
        	res = res.parent;
        }
		return featureList;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HelpersPackage.ATL_HELPER__OUTPUT_PACKAGE_NAME:
				return getOutputPackageName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HelpersPackage.ATL_HELPER__OUTPUT_PACKAGE_NAME:
				setOutputPackageName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HelpersPackage.ATL_HELPER__OUTPUT_PACKAGE_NAME:
				setOutputPackageName(OUTPUT_PACKAGE_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HelpersPackage.ATL_HELPER__OUTPUT_PACKAGE_NAME:
				return OUTPUT_PACKAGE_NAME_EDEFAULT == null ? outputPackageName != null : !OUTPUT_PACKAGE_NAME_EDEFAULT.equals(outputPackageName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case HelpersPackage.ATL_HELPER___ORDER_FEATURES__COMPONENTTYPE:
				return orderFeatures((ComponentType)arguments.get(0));
			case HelpersPackage.ATL_HELPER___COPY_LOCATION_REFERENCE__ELEMENT_ELEMENT:
				copyLocationReference((Element)arguments.get(0), (Element)arguments.get(1));
				return null;
			case HelpersPackage.ATL_HELPER___GET_CURRENT_PERION_READ_TABLE__FEATUREINSTANCE:
				return getCurrentPerionReadTable((FeatureInstance)arguments.get(0));
			case HelpersPackage.ATL_HELPER___GET_CURRENT_DEADLINE_WRITE_TABLE__FEATUREINSTANCE_FEATUREINSTANCE:
				return getCurrentDeadlineWriteTable((FeatureInstance)arguments.get(0), (FeatureInstance)arguments.get(1));
			case HelpersPackage.ATL_HELPER___GET_BUFFER_SIZE__FEATUREINSTANCE:
				return getBufferSize((FeatureInstance)arguments.get(0));
			case HelpersPackage.ATL_HELPER___SET_DIRECTION__DIRECTEDFEATURE_STRING:
				setDirection((DirectedFeature)arguments.get(0), (String)arguments.get(1));
				return null;
			case HelpersPackage.ATL_HELPER___IS_USED_IN_SPECIAL_OPERATOR__BEHAVIORANNEX_PORT_STRING:
				return isUsedInSpecialOperator((BehaviorAnnex)arguments.get(0), (Port)arguments.get(1), (String)arguments.get(2));
			case HelpersPackage.ATL_HELPER___GET_HYPERPERIOD__FEATUREINSTANCE:
				return getHyperperiod((FeatureInstance)arguments.get(0));
			case HelpersPackage.ATL_HELPER___GET_TIMING_PRECISION__NAMEDELEMENT:
				return getTimingPrecision((NamedElement)arguments.get(0));
			case HelpersPackage.ATL_HELPER___GET_LIST_OF_PATH__PROPERTYASSOCIATION:
				return getListOfPath((PropertyAssociation)arguments.get(0));
			case HelpersPackage.ATL_HELPER___ALL_PORT_COUNT__BEHAVIORELEMENT:
				return allPortCount((BehaviorElement)arguments.get(0));
			case HelpersPackage.ATL_HELPER___GET_STRING_LITERAL__PROPERTYASSOCIATION_STRING:
				return getStringLiteral((PropertyAssociation)arguments.get(0), (String)arguments.get(1));
			case HelpersPackage.ATL_HELPER___GET_ENUMERATORS__CLASSIFIER:
				return getEnumerators((Classifier)arguments.get(0));
			case HelpersPackage.ATL_HELPER___UNIQUE_NAME__NAMEDELEMENT_NAMEDELEMENT:
				return uniqueName((NamedElement)arguments.get(0), (NamedElement)arguments.get(1));
			case HelpersPackage.ATL_HELPER___GET_HYPERPERIOD_FROM_THREADS__ELIST_STRING:
				return getHyperperiodFromThreads((EList<ComponentInstance>)arguments.get(0), (String)arguments.get(1));
			case HelpersPackage.ATL_HELPER___GET_SCHED_TABLE_INIT__COMPONENTINSTANCE_MODEINSTANCE:
				return getSchedTableInit((ComponentInstance)arguments.get(0), (ModeInstance)arguments.get(1));
			case HelpersPackage.ATL_HELPER___DEPLOYED_ON_TRANSFORMED_CPU__COMPONENTINSTANCE:
				return deployedOnTransformedCpu((ComponentInstance)arguments.get(0));
			case HelpersPackage.ATL_HELPER___GET_CPU_TO_TRANSFORM:
				return getCpuToTransform();
			case HelpersPackage.ATL_HELPER___RESET_CPU_TO_TRANSFORM__ELIST:
				resetCpuToTransform((EList<ComponentInstance>)arguments.get(0));
				return null;
			case HelpersPackage.ATL_HELPER___GET_OUTPUT_PACKAGE_NAME__STRING:
				return getOutputPackageName((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (outputPackageName: ");
		result.append(outputPackageName);
		result.append(')');
		return result.toString();
	}

	public List<EObject> allInstances(EObject ele, EClass type) {
		List<EObject> res = new ArrayList<EObject>();
		TreeIterator<Element> treeIter = EcoreUtil2.getAllContents(ele, false);
		while (treeIter.hasNext()) {
			Element el = treeIter.next();
			if (EcoreUtil2.isAssignableFrom(type, el.eClass()))
				res.add(el);
		}
		return res;
	}

	private class ComparablePortByCriticality implements Comparable<ComparablePortByCriticality> {

		private long _criticality = 1;
		private FeatureInstance _featureInstance = null;

		public ComparablePortByCriticality(FeatureInstance port) {
			_featureInstance = port;
			_criticality = AadlHelperImpl.getInfoPortCriticality(port);
		}

		@Override
		public int compareTo(ComparablePortByCriticality toCompare) {
			if (_criticality < toCompare._criticality)
				return -1;
			else if (_criticality == toCompare._criticality)
				return 0;
			else
				return 1;
		}

		public FeatureInstance getFeatureInstance() {
			return _featureInstance;
		}
	}
} // AtlHelperImpl
