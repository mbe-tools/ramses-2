package fr.mem4csd.ramses.core.helpers.impl;

import org.osate.aadl2.instance.ComponentInstance;

public class ComparableThreadByDeadline implements Comparable<ComparableThreadByDeadline> 
{
	private ComponentInstance thread;
	private long deadline;
	
	public ComponentInstance getThread()
	{
		return thread;
	}
	
	public ComparableThreadByDeadline(ComponentInstance thread)
	{
		this.thread=thread;
		this.deadline = AadlHelperImpl.getInfoTaskDeadline(thread);
	}

	@Override
	public int compareTo(ComparableThreadByDeadline toCompare)
	{
		if(deadline<toCompare.deadline)
			return -1;
		else if(deadline==toCompare.deadline)
			return 0; 
		else return 1;
	} 
}