/**
 */
package fr.mem4csd.ramses.core.helpers.impl;

import fr.mem4csd.ramses.core.helpers.EventDataPortComDimHelper;
import fr.mem4csd.ramses.core.helpers.HelpersPackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.DirectionType;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.FeatureCategory;
import org.osate.aadl2.instance.FeatureInstance;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Event
 * Data Port Com Dim Helper</b></em>'. <!-- end-user-doc -->
 *
 * @generated
 */
public class EventDataPortComDimHelperImpl extends CommunicationDimensioningHelperImpl	implements EventDataPortComDimHelper {

	private static final Logger _LOGGER = Logger.getLogger(EventDataPortComDimHelperImpl.class);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected EventDataPortComDimHelperImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HelpersPackage.Literals.EVENT_DATA_PORT_COM_DIM_HELPER;
	}

	protected EventDataPortComDimHelperImpl(FeatureInstance receiverPort) throws DimensioningException {
		boolean isInOrInOutFeature = receiverPort.getDirection().equals(DirectionType.IN)
				|| receiverPort.getDirection().equals(DirectionType.IN_OUT);
		if (!isInOrInOutFeature && !receiverPort.getCategory().equals(FeatureCategory.EVENT_DATA_PORT)) {
			String errMsg = "Buffer size can only be computed for an " + "in or inout even data port";
			_LOGGER.fatal(errMsg);
			throw new DimensioningException(errMsg);
		}

		readerReceivingTaskInstance = (ComponentInstance) receiverPort.eContainer();

		if (!readerReceivingTaskInstance.getCategory().equals(ComponentCategory.THREAD)) {
			{
				String errMsg = "Buffer size can only be computed for an "
						+ "in or inout even data port of a thread component instance";
				_LOGGER.fatal(errMsg);
				throw new DimensioningException(errMsg);
			}
		}

		writerInstances = new BasicEList<ComponentInstance>();

		List<ComparableThreadByDeadline> toSortWriters = new ArrayList<ComparableThreadByDeadline>();
		for (ConnectionInstance cnxInst : receiverPort.getDstConnectionInstances()) {
			FeatureInstance writerPort = (FeatureInstance) cnxInst.getSource();
			ComponentInstance writerTaskInstance = (ComponentInstance) writerPort.eContainer();
			ComparableThreadByDeadline comparableThread = new ComparableThreadByDeadline(writerTaskInstance);
			toSortWriters.add(comparableThread);
			getWriterFeatureInstances().add(writerPort);

		}
		Collections.sort(toSortWriters);
		for (ComparableThreadByDeadline sortedThread : toSortWriters) {
			getWriterInstances().add(sortedThread.getThread());
		}

		setBufferSize(computeBufferSize());
		this.setCPRSize();
		this.setCurrentPeriodReadIndex();
		for (FeatureInstance writer : getWriterFeatureInstances()) {
			this.setCDWSize(writer);
			EList<Long> writerIndexes = new BasicEList<Long>();
			getCurrentDeadlineWriteMap().put(writer, writerIndexes);
		}
		this.setCurrentDeadlineWriteIndex();
	}

	protected long computeBufferSize() throws DimensioningException {
		long result = 0;
		long maxDeadline = 0;
		for (ComponentInstance writerTaskInstance : getWriterInstances()) {
			if (getDeadline(writerTaskInstance) > maxDeadline) {
				maxDeadline = getDeadline(writerTaskInstance);
			}
		}
		long readerPeriod = getPeriod(getReaderReceivingTaskInstance());
		long writerPeriod = 0;

		for (ComponentInstance writerTaskInstance : getWriterInstances()) {
			writerPeriod = getPeriod(writerTaskInstance);

			result += ((2 * readerPeriod + maxDeadline) / writerPeriod) + 1;
		}
		return result;
	}

	protected void setCurrentPeriodReadIndex() throws DimensioningException {

		long readerTaskPeriod = getPeriod(getReaderReceivingTaskInstance());
		long readerTaskOffset = getOffset(getReaderReceivingTaskInstance());
		for (int iteration = 0; iteration < getCPRSize(); iteration++) {
			long SEJDprev = 0;
			long readingTime = iteration * readerTaskPeriod + readerTaskOffset;

			for (ComponentInstance otherWriter : getWriterInstances()) {
				long otherPeriod = getPeriod(otherWriter);
				long otherDeadline = getDeadline(otherWriter);
				SEJDprev += ((getHyperperiod() + readingTime - otherDeadline) / otherPeriod) + 1;
			}
			SEJDprev = SEJDprev % getBufferSize();
			getCurrentPeriodRead().add(SEJDprev);

		}
	}

	protected void setCurrentDeadlineWriteIndex() throws DimensioningException {
		boolean first = true;
		for (FeatureInstance writerFeatureInstance : getWriterFeatureInstances()) {
			if (!getWriterFeatureInstances().contains(writerFeatureInstance)) {
				String msg = writerFeatureInstance.getComponentInstancePath() + "is not connected to: "
						+ getReaderReceivingTaskInstance().getComponentInstancePath();
				_LOGGER.fatal(msg);
				throw new DimensioningException(msg);
			}

			ComponentInstance writerTaskInstance = (ComponentInstance) writerFeatureInstance.eContainer();
			for (int iteration = 0; iteration < getCdwSize().get(writerFeatureInstance); iteration++) {
				long CDW = 0, SEJD = 0;
				long writerTaskPeriod = getPeriod(writerTaskInstance);
				long writerTaskDeadline = getDeadline(writerTaskInstance);
				long writingTime = iteration * writerTaskPeriod + writerTaskDeadline;

				int simultaneousWriters = 0;

				boolean found = false;

				for (ComponentInstance otherWriter : getWriterInstances()) {
					long otherPeriod = getPeriod(otherWriter);
					long otherDeadline = getDeadline(otherWriter);

					// formule
					if (otherWriter.equals(writerTaskInstance)) {
						found = true;
					}

					SEJD += ((writingTime - otherDeadline) / otherPeriod) + 1;
					if ((((writingTime - otherDeadline) % otherPeriod) == 0) && (found)) {
						simultaneousWriters++;
					}
				}
				CDW = SEJD - simultaneousWriters;
				CDW = CDW % getBufferSize();
				if (first && (CDW <= 1)) {
					getCurrentDeadlineWriteMap().get(writerFeatureInstance).add((long) 0);
					first = false;
					continue;
				}

				getCurrentDeadlineWriteMap().get(writerFeatureInstance).add(CDW);
			}
		}
	}

	private static final Map<FeatureInstance, EventDataPortComDimHelper> _instances = new HashMap<FeatureInstance, EventDataPortComDimHelper>();

	public static EventDataPortComDimHelper create(FeatureInstance port) throws DimensioningException {
		if (_instances.get(port) != null) {
			return _instances.get(port);
		}

		EventDataPortComDimHelper instance = new EventDataPortComDimHelperImpl(port);
		_instances.put(port, instance);

		return instance;
	}
} // EventDataPortComDimHelperImpl
