/**
 */
package fr.mem4csd.ramses.core.helpers.impl;

import fr.mem4csd.ramses.core.helpers.CodeGenHelper;
import fr.mem4csd.ramses.core.helpers.HelpersPackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.osate.aadl2.AccessCategory;
import org.osate.aadl2.AccessConnection;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ConnectedElement;
import org.osate.aadl2.Connection;
import org.osate.aadl2.Data;
import org.osate.aadl2.DataAccess;
import org.osate.aadl2.DataClassifier;
import org.osate.aadl2.DataSubcomponent;
import org.osate.aadl2.EnumerationLiteral;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.NamedValue;
import org.osate.aadl2.Parameter;
import org.osate.aadl2.Port;
import org.osate.aadl2.ProcessImplementation;
import org.osate.aadl2.ProcessSubcomponent;
import org.osate.aadl2.Processor;
import org.osate.aadl2.ProcessorSubcomponent;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.ReferenceValue;
import org.osate.aadl2.StringLiteral;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.System;
import org.osate.aadl2.SystemImplementation;
import org.osate.aadl2.SystemSubcomponent;
import org.osate.aadl2.VirtualProcessorSubcomponent;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.ba.aadlba.DataRepresentation;
import org.osate.ba.analyzers.TypeHolder;
import org.osate.ba.utils.AadlBaUtils;
import org.osate.ba.utils.DimensionException;
import org.osate.utils.internal.Aadl2Utils;
import org.osate.utils.internal.PropertyUtils;
import org.osate.utils.internal.names.DataModelProperties;
import org.osate.xtext.aadl2.properties.util.GetProperties;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Code Gen Helper</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CodeGenHelperImpl extends MinimalEObjectImpl.Container implements CodeGenHelper {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodeGenHelperImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HelpersPackage.Literals.CODE_GEN_HELPER;
	}
	
	  private static Logger _LOGGER = Logger.getLogger(CodeGenHelperImpl.class) ;
	  
	  public static String limitCharNb(String src, Integer limit, List<String> existingId)
	  {
		// limit name to 30 chars as defined in POK
		if(src.length()>=limit)
			src = src.substring(src.length()-(limit-1), src.length());
		
		if(!existingId.contains(src)) {
			existingId.add(src);
			return src;
		}
		else
		{
			String res = src;
			int i=1;
			while(existingId.contains(res))
			{
				String suffix = Integer.toString(i);
				res = src+suffix;
				res = res.substring(suffix.length());
				i++;
			}
			existingId.add(res);
			return res;
		}
	  }
	  
	  public static String getInitialValue(NamedElement e, String language)
	  {
	    StringBuilder initialization = new StringBuilder() ;

	    if(e instanceof Data)
	    {
	      Data d = (Data) e ;
	      if(d instanceof DataSubcomponent)
	      {
	        DataSubcomponent ds = (DataSubcomponent) d ;
	        for(PropertyAssociation pa : ds.getOwnedPropertyAssociations())
	        {
	          Property p = pa.getProperty() ;

	          // Sometime, properties don't have name.
	          if(p.getName() != null &&
	              p.getName()
	              .equalsIgnoreCase(DataModelProperties.INITIAL_VALUE))
	          {
	            setInitialization(ds, initialization, PropertyUtils
	                              .getPropertyExpression(pa), language) ;
	            return initialization.toString() ;
	          }
	        }

	        return getInitialValue(ds.getClassifier(), language) ;
	      }
	      else if(d instanceof DataClassifier)
	      {
	        DataClassifier dc = (DataClassifier) d ;
	        EList<PropertyExpression> initialValueProperty =
	            PropertyUtils
	            .findPropertyExpression(dc,
	                                   DataModelProperties.INITIAL_VALUE) ;
	        setInitialization(dc, initialization, initialValueProperty, language) ;
	        return initialization.toString() ;
	      }
	    }
	    else if(e instanceof Port)
	    {
	    
	    }
	    else if(e instanceof Parameter)
	    {
	    
	    }
	    return initialization.toString() ;
	  }

	  public static NamedElement getDeloymentProcessorSubcomponent(
	                                       NamedElement aProcess)
	  {
	    PropertyAssociation aPropertyAssociation =
	          PropertyUtils.findPropertyAssociation("Actual_Processor_Binding",
	        		  aProcess) ;

	    if(aPropertyAssociation == null)
	    {
	      String errMsg = "Actual_Processor_Binding property not set for component instance "+
	    		  aProcess.getName() ;
	      _LOGGER.error(errMsg);
	    //  ServiceProvider.SYS_ERR_REP.error(errMsg, true);
	    	return null;
	    }
	    
	    for(ModalPropertyValue aModalPropertyValue : aPropertyAssociation
	          .getOwnedValues())
	    {
	      if(aModalPropertyValue.getOwnedValue() instanceof ListValue)
	      {
	        ListValue list = (ListValue) aModalPropertyValue.getOwnedValue() ;

	        for(PropertyExpression pe : list.getOwnedListElements())
	        {
	          if(pe instanceof ReferenceValue)
	          {
	            ReferenceValue rv = (ReferenceValue) pe ;
	            NamedElement anElement =
	                  rv.getContainmentPathElements().get(0).getNamedElement() ;

	            if(anElement instanceof ProcessorSubcomponent)
	            {
	              ProcessorSubcomponent ps = (ProcessorSubcomponent) anElement ;
	              return ps ;
	            }
	            else if(anElement instanceof VirtualProcessorSubcomponent)
	            {
	              VirtualProcessorSubcomponent vps =
	                    (VirtualProcessorSubcomponent) anElement ;
	              return vps ;
	            }
	            else if(anElement instanceof SystemSubcomponent)
	            {
	              SystemSubcomponent ss =
	                    (SystemSubcomponent) anElement ;
	              return ss ;
	            }
	          }
	          else if(pe instanceof InstanceReferenceValue)
	          {
	        	  InstanceReferenceValue irv = (InstanceReferenceValue) pe ;
	        	  ComponentInstance ci = (ComponentInstance) irv.getReferencedInstanceObject();
	        	  return ci;
	          }
	        }
	      }
	    }

	    return null ;
	  }

	  public static List<ProcessSubcomponent> getBindedProcesses(Subcomponent object)
	  {
	    List<ProcessSubcomponent> bindedProcess = new ArrayList<ProcessSubcomponent>() ;
	    SystemImplementation si = (SystemImplementation) object.eContainer();
	    for(ProcessSubcomponent ps : si.getOwnedProcessSubcomponents())
	    {
	      NamedElement ne = getDeloymentProcessorSubcomponent(ps);
	      if(ne instanceof VirtualProcessorSubcomponent)
	    	  ne = getDeloymentProcessorSubcomponent(ne);
	      if(ne!=null && ne.equals(object))
	      {
	        bindedProcess.add(ps) ;
	      }
	    }
	    
	    return bindedProcess ;
	  }

	  private static void setInitialization(NamedElement obj,
			  								                  StringBuilder initialization,
	                                        List<PropertyExpression> initialValues,
	                                        String language)
	  {
	    boolean isStringType = false;
	    DataClassifier dc;
	    if(obj instanceof DataSubcomponent)
	    {
	      DataSubcomponent ds = (DataSubcomponent) obj;
	      dc = (DataClassifier) ds.getDataSubcomponentType();
	    }
	    else
	      dc=(DataClassifier) obj;
	    
	    try
	    {
	      TypeHolder dataTypeHolder = AadlBaUtils.getTypeHolder(dc);
	      isStringType=dataTypeHolder.getDataRep().equals(DataRepresentation.STRING);
	    }
	    catch(DimensionException ex)
	    {
	      String errMsg = "fail to fetch the initial value of " +  obj.getName();
	      _LOGGER.error(errMsg, ex);
	     // ServiceProvider.SYS_ERR_REP.error(errMsg, true);
	    }

	    for(PropertyExpression pe : initialValues)
	    {
	      if(pe instanceof ListValue)
	      {
	        ListValue lv = (ListValue) pe ;
	        List<PropertyExpression> initValueList = lv.getOwnedListElements() ;

	        if(initValueList.size() > 0)
	        {
	          if(language.equals("ada"))
	            initialization.append(" := ") ;
	          else
	            initialization.append(" = ") ;
	        }

	        if(isStringType && false==initialization.toString().startsWith("\""))
	          initialization.append("\"");
	        
	        if(initValueList.size() > 1)
	        {
	          if(language.equals("ada"))
	        	  initialization.append("(") ;
	          else
	        	  initialization.append("{") ;
	        }

	        Iterator<PropertyExpression> it = initValueList.iterator() ;

	        while(it.hasNext())
	        {
	          PropertyExpression initValue = it.next() ;

	          if(initValue instanceof StringLiteral)
	          {
	            StringLiteral sl = (StringLiteral) initValue ;
	            if (language.equalsIgnoreCase("java"))
	            {
	            	if (obj instanceof DataClassifier)
	            	{
			          	if (AadlBaUtils.getDataRepresentation((DataClassifier)obj) == DataRepresentation.ENUM)	
			          	{
			              initialization.append(obj.getQualifiedName() +
			                                    "INSERTDOTHERE");
			          	}
	            	}
	            }
	            initialization.append(sl.getValue()) ;

	            if(it.hasNext())
	            {
	              initialization.append(",") ;
	            }
	          }
	        }

	        if(initValueList.size() > 1)
	        {
	          if(language.equals("ada"))
	            initialization.append(")") ;
	          else
	          	initialization.append("}") ;
	        }
	        if(isStringType && false==initialization.toString().endsWith("\""))
	          initialization.append("\"");

	      }
	    }
	  }
	  
	  public static NamedElement getDeloymentMemorySubcomponent(NamedElement aProcessSubcomponent)
	  {
	    // aProcessSubcomponent could be a of class ComponentInstance as well
	    PropertyAssociation aPropertyAssociation =
	          PropertyUtils.findPropertyAssociation("Actual_Memory_Binding",
	                                     aProcessSubcomponent) ;

	    if(aPropertyAssociation != null)
	    {
	      for(ModalPropertyValue aModalPropertyValue : aPropertyAssociation.
	                                                               getOwnedValues())
	      {
	        if(aModalPropertyValue.getOwnedValue() instanceof ListValue)
	        {
	          ListValue list = (ListValue) aModalPropertyValue.getOwnedValue() ;

	          for(PropertyExpression pe : list.getOwnedListElements())
	          {
	            if(pe instanceof ReferenceValue)
	            {
	              ReferenceValue rv = (ReferenceValue) pe ;
	              NamedElement anElement =
	                                       rv.getContainmentPathElements()
	                                         .get(rv.getContainmentPathElements()
	                                                .size() - 1).getNamedElement() ;

	              return anElement;
	            }
	          }
	        }
	      }
	    }

	    return null ;
	  }
	  
	  //Builds the data access mapping via the connections described in the
	  // process implementation.
	  public static void buildDataAccessMapping(ComponentImplementation cptImpl,
	                                            Map<DataAccess, String> _dataAccessMapping)
	  {
	    
	    EList<Subcomponent> subcmpts = cptImpl.getAllSubcomponents() ;
	    
	    List<String> dataSubcomponentNames = new ArrayList<String>() ;
	    
	    // Fetches data subcomponent names.
	    for(Subcomponent s : subcmpts)
	    {
	      if(s instanceof DataSubcomponent)
	      {
	        dataSubcomponentNames.add(s.getName()) ;
	      }
	    }
	    
	    // Binds data subcomponent names with DataAcess objects
	    // of threads.
	    // See process implementation's connections.
	    for(Connection connect : cptImpl.getAllConnections())
	    {
	      if (connect instanceof AccessConnection &&
	         ((AccessConnection) connect).getAccessCategory() == AccessCategory.DATA)
	      {

	      if(connect.getAllDestination() instanceof DataSubcomponent)
	      {
	        DataSubcomponent destination =  (DataSubcomponent) connect.
	                                                       getAllDestination() ;
	        
	          if(Aadl2Utils.contains(destination.getName(), dataSubcomponentNames))
	          {
	            ConnectedElement source = (ConnectedElement) connect.getSource() ;
	            DataAccess da = (DataAccess) source.getConnectionEnd() ;
	            _dataAccessMapping.put(da, destination.getName()) ; 
	          }
	      }
	        else if(connect.getAllSource() instanceof DataSubcomponent)
	        {
	          DataSubcomponent source =  (DataSubcomponent) connect.
	              getAllSource() ;
	          if(Aadl2Utils.contains(source.getName(), dataSubcomponentNames))
	          {
	            ConnectedElement dest = (ConnectedElement) connect.getDestination() ;
	             
	            DataAccess da = (DataAccess) dest.getConnectionEnd() ;
	            _dataAccessMapping.put(da, source.getName()) ;
	          }
	        }
	        else if(connect.getAllDestination() instanceof DataAccess
	            && connect.getAllSource() instanceof DataAccess)
	        {
	          if(!(connect.getAllDestination().eContainer() instanceof Thread)
	            && !(connect.getAllSource().eContainer() instanceof Thread))
	            continue;
	          DataAccess destination = (DataAccess) connect.getAllDestination();
	          DataAccess source = (DataAccess) connect.getAllSource();
	          if(_dataAccessMapping.containsKey(destination) &&
	              !_dataAccessMapping.containsKey(source))
	            _dataAccessMapping.put(source, _dataAccessMapping.get(destination)) ;
	          if(_dataAccessMapping.containsKey(source) &&
	              !_dataAccessMapping.containsKey(destination))
	            _dataAccessMapping.put(destination, _dataAccessMapping.get(source)) ;
	          
	        }
	      }
	    }
	  }
	  
	  public static long getHyperperiod(List<ComponentInstance> consideredTasks)
	  {
	    Long[] periods = new Long[consideredTasks.size()];
	    ArrayList<Long> consideredPeriods = new ArrayList<Long>();
	    for(ComponentInstance ci : consideredTasks)
	    {
	      consideredPeriods.add(AadlHelperImpl.getInfoTaskPeriod(ci));
	    }
	    consideredPeriods.toArray(periods);
	    return MathHelperImpl.lcm(periods);
	  }

	  public static boolean isProcessor(NamedElement sub) {
	    if(sub instanceof Processor)
	    	return true;
	    else if(sub instanceof System)
	    {
	      Boolean b = PropertyUtils.getBooleanValue(sub, "is_processor");
	      if(b==null)
	      {
	    	  if(sub instanceof Subcomponent)
	    	  {
	    		  Subcomponent sc = (Subcomponent) sub;
	    		  return isProcessor(sc.getSubcomponentType());
	    	  }
		      else if(sub instanceof ComponentImplementation)
		      {
		    	  ComponentImplementation ci = (ComponentImplementation) sub;
		    	  return isProcessor(ci.getType());
		      }
	    	    return false;
	      }
	      else
	    	return b;
	    }
	    return false;
	  }

	  public static String getSchedulingProtocol(NamedElement ne) 
	  {
	  	Property prop = GetProperties.lookupPropertyDefinition(ne, "Deployment_Properties", "Scheduling_Protocol");
	  	List<? extends PropertyExpression> props;
	  	try {
	  		props = ne.getPropertyValueList(prop);
	  	} catch (Exception e) {
	  		props = Collections.emptyList();
	  	}
	  	for(PropertyExpression pe: props)
	  	{
	      NamedValue nv = (NamedValue) pe;
		  EnumerationLiteral el = (EnumerationLiteral) nv.getNamedValue();
	      return el.getName();
	  	}
		return null;
	  }

	  public static boolean processUsesMQTT(ProcessImplementation procImpl) {
	    for(DataSubcomponent ds: procImpl.getOwnedDataSubcomponents())
	    {
	      if(ds.getName().contains("MQTT"))
	      {
	        return true;
	      }
	    }
	    return false;
	  }
	  
	  public static boolean processUsesSOCKET(ProcessImplementation procImpl) {
	    for(DataSubcomponent ds: procImpl.getOwnedDataSubcomponents())
	    {
	      if(ds.getName().contains("_SocketConfig"))
	      {
	        return true;
	      }
	    }
	    return false;
	  }

} //CodeGenHelperImpl
