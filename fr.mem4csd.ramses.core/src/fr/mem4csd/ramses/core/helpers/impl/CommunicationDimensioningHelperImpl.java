/**
 */
package fr.mem4csd.ramses.core.helpers.impl;

import fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper;
import fr.mem4csd.ramses.core.helpers.HelpersPackage;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.utils.internal.PropertyUtils;
import org.osate.xtext.aadl2.properties.util.AadlProject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Dimensioning Helper</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl#getReaderReceivingTaskInstance <em>Reader Receiving Task Instance</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl#getWriterInstances <em>Writer Instances</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl#getWriterFeatureInstances <em>Writer Feature Instances</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl#getCprSize <em>Cpr Size</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl#getCdwSize <em>Cdw Size</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl#getCurrentPeriodRead <em>Current Period Read</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl#getCurrentDeadlineWriteMap <em>Current Deadline Write Map</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl#getBufferSize <em>Buffer Size</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.impl.CommunicationDimensioningHelperImpl#getHyperperiod <em>Hyperperiod</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class CommunicationDimensioningHelperImpl extends MinimalEObjectImpl.Container implements CommunicationDimensioningHelper {
	/**
	 * The cached value of the '{@link #getReaderReceivingTaskInstance() <em>Reader Receiving Task Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReaderReceivingTaskInstance()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance readerReceivingTaskInstance;
	/**
	 * The cached value of the '{@link #getWriterInstances() <em>Writer Instances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWriterInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentInstance> writerInstances;
	/**
	 * The cached value of the '{@link #getWriterFeatureInstances() <em>Writer Feature Instances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWriterFeatureInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<FeatureInstance> writerFeatureInstances;
	/**
	 * The default value of the '{@link #getCprSize() <em>Cpr Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCprSize()
	 * @generated
	 * @ordered
	 */
	protected static final long CPR_SIZE_EDEFAULT = 0L;
	/**
	 * The cached value of the '{@link #getCprSize() <em>Cpr Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCprSize()
	 * @generated
	 * @ordered
	 */
	protected long cprSize = CPR_SIZE_EDEFAULT;
	/**
	 * The cached value of the '{@link #getCdwSize() <em>Cdw Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCdwSize()
	 * @generated
	 * @ordered
	 */
	protected Map<FeatureInstance, Long> cdwSize;
	/**
	 * The cached value of the '{@link #getCurrentPeriodRead() <em>Current Period Read</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentPeriodRead()
	 * @generated
	 * @ordered
	 */
	protected EList<Long> currentPeriodRead;
	/**
	 * The cached value of the '{@link #getCurrentDeadlineWriteMap() <em>Current Deadline Write Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentDeadlineWriteMap()
	 * @generated
	 * @ordered
	 */
	protected Map<FeatureInstance, EList<Long>> currentDeadlineWriteMap;
	/**
	 * The default value of the '{@link #getBufferSize() <em>Buffer Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBufferSize()
	 * @generated
	 * @ordered
	 */
	protected static final long BUFFER_SIZE_EDEFAULT = 0L;
	/**
	 * The cached value of the '{@link #getBufferSize() <em>Buffer Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBufferSize()
	 * @generated
	 * @ordered
	 */
	protected long bufferSize = BUFFER_SIZE_EDEFAULT;
	/**
	 * The default value of the '{@link #getHyperperiod() <em>Hyperperiod</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHyperperiod()
	 * @generated
	 * @ordered
	 */
	protected static final long HYPERPERIOD_EDEFAULT = 0L;
	/**
	 * The cached value of the '{@link #getHyperperiod() <em>Hyperperiod</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHyperperiod()
	 * @generated
	 * @ordered
	 */
	protected long hyperperiod = HYPERPERIOD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationDimensioningHelperImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HelpersPackage.Literals.COMMUNICATION_DIMENSIONING_HELPER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ComponentInstance getReaderReceivingTaskInstance() {
		if (readerReceivingTaskInstance != null && readerReceivingTaskInstance.eIsProxy()) {
			InternalEObject oldReaderReceivingTaskInstance = (InternalEObject)readerReceivingTaskInstance;
			readerReceivingTaskInstance = (ComponentInstance)eResolveProxy(oldReaderReceivingTaskInstance);
			if (readerReceivingTaskInstance != oldReaderReceivingTaskInstance) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__READER_RECEIVING_TASK_INSTANCE, oldReaderReceivingTaskInstance, readerReceivingTaskInstance));
			}
		}
		return readerReceivingTaskInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetReaderReceivingTaskInstance() {
		return readerReceivingTaskInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReaderReceivingTaskInstance(ComponentInstance newReaderReceivingTaskInstance) {
		ComponentInstance oldReaderReceivingTaskInstance = readerReceivingTaskInstance;
		readerReceivingTaskInstance = newReaderReceivingTaskInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__READER_RECEIVING_TASK_INSTANCE, oldReaderReceivingTaskInstance, readerReceivingTaskInstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ComponentInstance> getWriterInstances() {
		if (writerInstances == null) {
			writerInstances = new EObjectResolvingEList<ComponentInstance>(ComponentInstance.class, this, HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__WRITER_INSTANCES);
		}
		return writerInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<FeatureInstance> getWriterFeatureInstances() {
		if (writerFeatureInstances == null) {
			writerFeatureInstances = new EObjectResolvingEList<FeatureInstance>(FeatureInstance.class, this, HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__WRITER_FEATURE_INSTANCES);
		}
		return writerFeatureInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getCprSize() {
		return cprSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCprSize(long newCprSize) {
		long oldCprSize = cprSize;
		cprSize = newCprSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CPR_SIZE, oldCprSize, cprSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	@Override
	public Map<FeatureInstance, Long> getCdwSize() {
		if(cdwSize==null)
			cdwSize = new HashMap<FeatureInstance, Long>();
		return cdwSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCdwSize(Map<FeatureInstance, Long> newCdwSize) {
		Map<FeatureInstance, Long> oldCdwSize = cdwSize;
		cdwSize = newCdwSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CDW_SIZE, oldCdwSize, cdwSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Long> getCurrentPeriodRead() {
		if (currentPeriodRead == null) {
			currentPeriodRead = new EDataTypeEList<Long>(Long.class, this, HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_PERIOD_READ);
		}
		return currentPeriodRead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Map<FeatureInstance, EList<Long>> getCurrentDeadlineWriteMap() {
		if(currentDeadlineWriteMap==null)
			currentDeadlineWriteMap = new HashMap<FeatureInstance, EList<Long>>();
		return currentDeadlineWriteMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCurrentDeadlineWriteMap(Map<FeatureInstance, EList<Long>> newCurrentDeadlineWriteMap) {
		Map<FeatureInstance, EList<Long>> oldCurrentDeadlineWriteMap = currentDeadlineWriteMap;
		currentDeadlineWriteMap = newCurrentDeadlineWriteMap;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_DEADLINE_WRITE_MAP, oldCurrentDeadlineWriteMap, currentDeadlineWriteMap));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBufferSize() {
		return bufferSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBufferSize(long newBufferSize) {
		long oldBufferSize = bufferSize;
		bufferSize = newBufferSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__BUFFER_SIZE, oldBufferSize, bufferSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getHyperperiod() {
		return hyperperiod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHyperperiod(long newHyperperiod) {
		long oldHyperperiod = hyperperiod;
		hyperperiod = newHyperperiod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__HYPERPERIOD, oldHyperperiod, hyperperiod));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__READER_RECEIVING_TASK_INSTANCE:
				if (resolve) return getReaderReceivingTaskInstance();
				return basicGetReaderReceivingTaskInstance();
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__WRITER_INSTANCES:
				return getWriterInstances();
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__WRITER_FEATURE_INSTANCES:
				return getWriterFeatureInstances();
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CPR_SIZE:
				return getCprSize();
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CDW_SIZE:
				return getCdwSize();
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_PERIOD_READ:
				return getCurrentPeriodRead();
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_DEADLINE_WRITE_MAP:
				return getCurrentDeadlineWriteMap();
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__BUFFER_SIZE:
				return getBufferSize();
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__HYPERPERIOD:
				return getHyperperiod();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__READER_RECEIVING_TASK_INSTANCE:
				setReaderReceivingTaskInstance((ComponentInstance)newValue);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__WRITER_INSTANCES:
				getWriterInstances().clear();
				getWriterInstances().addAll((Collection<? extends ComponentInstance>)newValue);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__WRITER_FEATURE_INSTANCES:
				getWriterFeatureInstances().clear();
				getWriterFeatureInstances().addAll((Collection<? extends FeatureInstance>)newValue);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CPR_SIZE:
				setCprSize((Long)newValue);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CDW_SIZE:
				setCdwSize((Map<FeatureInstance, Long>)newValue);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_PERIOD_READ:
				getCurrentPeriodRead().clear();
				getCurrentPeriodRead().addAll((Collection<? extends Long>)newValue);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_DEADLINE_WRITE_MAP:
				setCurrentDeadlineWriteMap((Map<FeatureInstance, EList<Long>>)newValue);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__BUFFER_SIZE:
				setBufferSize((Long)newValue);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__HYPERPERIOD:
				setHyperperiod((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__READER_RECEIVING_TASK_INSTANCE:
				setReaderReceivingTaskInstance((ComponentInstance)null);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__WRITER_INSTANCES:
				getWriterInstances().clear();
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__WRITER_FEATURE_INSTANCES:
				getWriterFeatureInstances().clear();
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CPR_SIZE:
				setCprSize(CPR_SIZE_EDEFAULT);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CDW_SIZE:
				setCdwSize((Map<FeatureInstance, Long>)null);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_PERIOD_READ:
				getCurrentPeriodRead().clear();
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_DEADLINE_WRITE_MAP:
				setCurrentDeadlineWriteMap((Map<FeatureInstance, EList<Long>>)null);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__BUFFER_SIZE:
				setBufferSize(BUFFER_SIZE_EDEFAULT);
				return;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__HYPERPERIOD:
				setHyperperiod(HYPERPERIOD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__READER_RECEIVING_TASK_INSTANCE:
				return readerReceivingTaskInstance != null;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__WRITER_INSTANCES:
				return writerInstances != null && !writerInstances.isEmpty();
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__WRITER_FEATURE_INSTANCES:
				return writerFeatureInstances != null && !writerFeatureInstances.isEmpty();
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CPR_SIZE:
				return cprSize != CPR_SIZE_EDEFAULT;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CDW_SIZE:
				return cdwSize != null;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_PERIOD_READ:
				return currentPeriodRead != null && !currentPeriodRead.isEmpty();
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__CURRENT_DEADLINE_WRITE_MAP:
				return currentDeadlineWriteMap != null;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__BUFFER_SIZE:
				return bufferSize != BUFFER_SIZE_EDEFAULT;
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER__HYPERPERIOD:
				return hyperperiod != HYPERPERIOD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER___GET_CDW_SIZE__FEATUREINSTANCE:
				try {
					return getCdwSize((FeatureInstance)arguments.get(0));
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER___GET_CURRENT_DEADLINE_WRITE_INDEX__FEATUREINSTANCE_INT:
				try {
					return getCurrentDeadlineWriteIndex((FeatureInstance)arguments.get(0), (Integer)arguments.get(1));
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case HelpersPackage.COMMUNICATION_DIMENSIONING_HELPER___GET_CURRENT_PERIOD_READ_INDEX__INT:
				try {
					return getCurrentPeriodReadIndex((Integer)arguments.get(0));
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (cprSize: ");
		result.append(cprSize);
		result.append(", cdwSize: ");
		result.append(cdwSize);
		result.append(", currentPeriodRead: ");
		result.append(currentPeriodRead);
		result.append(", currentDeadlineWriteMap: ");
		result.append(currentDeadlineWriteMap);
		result.append(", bufferSize: ");
		result.append(bufferSize);
		result.append(", hyperperiod: ");
		result.append(hyperperiod);
		result.append(')');
		return result.toString();
	}
	
	private static Logger _LOGGER = Logger.getLogger(CommunicationDimensioningHelperImpl.class) ;

	public long getPreviousPeriodReadIndex(int iteration) throws DimensioningException
	{
		if(iteration > 0)
			return getCurrentPeriodRead().get(iteration-1);
		else
			return -1;
	}

	public long getCPRSize()
	{
		return this.getCprSize();
	}

	public long getCurrentPeriodReadIndex(int iteration) throws DimensioningException
	{
		return getCurrentPeriodRead().get(iteration);
	}

	public long getCdwSize(FeatureInstance writer) throws DimensioningException
	{
		return getCdwSize().get(writer);
	}

	public long getCurrentDeadlineWriteIndex(FeatureInstance writer, int iteration) throws DimensioningException
	{
		return getCurrentDeadlineWriteMap().get(writer).get(iteration);
	}
	
	protected long deliveredAt(long t)throws DimensioningException
	{
		long deliveredAtIteration = 0;
		for(ComponentInstance writerTaskInstance: this.writerInstances)
		{
			long writerTaskPeriod = getPeriod(writerTaskInstance);
			long writerTaskDeadline = getDeadline(writerTaskInstance);
			long Pi=getPeriod(readerReceivingTaskInstance);
			long readingTime = ((t/writerTaskPeriod) * writerTaskPeriod + writerTaskDeadline) ;
			if(readingTime%Pi<=getOffset(readerReceivingTaskInstance))
				readingTime=readingTime/Pi;
			else
				readingTime= readingTime/Pi +1;
			readingTime*=Pi;
			deliveredAtIteration = deliveredAtIteration+Math.max(0, readingTime/writerTaskPeriod + 1);
		}
		return deliveredAtIteration;

	}
		
	protected long getOffset(ComponentInstance ci) throws DimensioningException
	{
		Long res = PropertyUtils.getIntValue(ci, "Offset", AadlProject.MS_LITERAL);
		if(res==null)
			return 0;
		else
			return res;
	}
	
	protected long getPeriod(ComponentInstance ci) throws DimensioningException
	{
		long taskPeriod = AadlHelperImpl.getInfoTaskPeriod(ci);
		if(taskPeriod==0)
		{
		  String errMsg = "Buffer size can only be computed for a periodic " +
	          "task with a specified period; see thread instance: "
	          + ci.getComponentInstancePath() ;
	     _LOGGER.fatal(errMsg) ;
	     throw new DimensioningException(errMsg);
		}
			
		return taskPeriod;
	}

	protected long getDeadline(ComponentInstance ci) throws DimensioningException {
		long taskDeadline = AadlHelperImpl.getInfoTaskDeadline(ci);
		if(taskDeadline == 0)
			taskDeadline  = AadlHelperImpl.getInfoTaskPeriod(ci);
		if(taskDeadline==0)
		{
      String errMsg = "Buffer size can only be computed for a periodic " +
            "producer task with a specified deadline; see thread instance: "
            + ci.getComponentInstancePath() ;
       _LOGGER.fatal(errMsg) ;
       throw new DimensioningException(errMsg);
    }
		  
		return taskDeadline;
	}
	
	
	protected void setCDWSize(FeatureInstance writer) throws DimensioningException
	{
		long WritersHyperperiod = getHyperperiod(this.writerInstances);
		long MessageNumbers=0;
		for(ComponentInstance w: this.writerInstances)
		{
			long writerPeriod = getPeriod(w);
			MessageNumbers=MessageNumbers+WritersHyperperiod/writerPeriod;
		}
		
		long writerCDWSize = (MathHelperImpl.lcm(MessageNumbers, bufferSize)/MessageNumbers)
				*WritersHyperperiod/getPeriod((ComponentInstance) writer.eContainer());
		getCdwSize().put(writer, writerCDWSize); 
	}
	
	protected void setCPRSize() throws DimensioningException
	{
		List<ComponentInstance> accessingTasks = new ArrayList<ComponentInstance>();
		accessingTasks.addAll(writerInstances);
		accessingTasks.add(this.readerReceivingTaskInstance);
		long AccessingTasksHyperperiod = getHyperperiod(accessingTasks);
		long MessageNumbers=0;
		
		for(ComponentInstance w: this.writerInstances)
		{
			long writerPeriod = getPeriod(w);
			MessageNumbers=MessageNumbers+AccessingTasksHyperperiod/writerPeriod;
		}
		
		setCprSize( (MathHelperImpl.lcm(MessageNumbers, bufferSize)/MessageNumbers)
				*AccessingTasksHyperperiod/getPeriod(this.readerReceivingTaskInstance) );

		this.hyperperiod = AccessingTasksHyperperiod;

	}


	public long getHyperperiod(List<ComponentInstance> consideredTasks) throws DimensioningException
	{
		Long[] periods = new Long[consideredTasks.size()];
		List<Long> consideredPeriods = new ArrayList<Long>();
		for(ComponentInstance ci : consideredTasks)
		{
			consideredPeriods.add(getPeriod(ci));
		}
		consideredPeriods.toArray(periods);
		return MathHelperImpl.lcm(periods);
	}

} //CommunicationDimensioningHelperImpl
