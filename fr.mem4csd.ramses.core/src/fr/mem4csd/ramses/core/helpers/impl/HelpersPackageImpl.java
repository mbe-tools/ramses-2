/**
 */
package fr.mem4csd.ramses.core.helpers.impl;

import fr.mem4csd.ramses.core.helpers.AadlHelper;
import fr.mem4csd.ramses.core.helpers.AtlHelper;
import fr.mem4csd.ramses.core.helpers.CodeGenHelper;
import fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper;
import fr.mem4csd.ramses.core.helpers.EventDataPortComDimHelper;
import fr.mem4csd.ramses.core.helpers.HelpersFactory;
import fr.mem4csd.ramses.core.helpers.HelpersPackage;

import fr.mem4csd.ramses.core.helpers.MathHelper;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.osate.aadl2.Aadl2Package;

import org.osate.aadl2.instance.InstancePackage;
import org.osate.ba.aadlba.AadlBaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HelpersPackageImpl extends EPackageImpl implements HelpersPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass atlHelperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mathHelperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aadlHelperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeGenHelperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationDimensioningHelperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventDataPortComDimHelperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ramsesExceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType dimensioningExceptionEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private HelpersPackageImpl() {
		super(eNS_URI, HelpersFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link HelpersPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static HelpersPackage init() {
		if (isInited) return (HelpersPackage)EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredHelpersPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		HelpersPackageImpl theHelpersPackage = registeredHelpersPackage instanceof HelpersPackageImpl ? (HelpersPackageImpl)registeredHelpersPackage : new HelpersPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Aadl2Package.eINSTANCE.eClass();
		AadlBaPackage.eINSTANCE.eClass();
		InstancePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theHelpersPackage.createPackageContents();

		// Initialize created meta-data
		theHelpersPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theHelpersPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(HelpersPackage.eNS_URI, theHelpersPackage);
		return theHelpersPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAtlHelper() {
		return atlHelperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAtlHelper_OutputPackageName() {
		return (EAttribute)atlHelperEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__OrderFeatures__ComponentType() {
		return atlHelperEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__CopyLocationReference__Element_Element() {
		return atlHelperEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetCurrentPerionReadTable__FeatureInstance() {
		return atlHelperEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetCurrentDeadlineWriteTable__FeatureInstance_FeatureInstance() {
		return atlHelperEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetBufferSize__FeatureInstance() {
		return atlHelperEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__SetDirection__DirectedFeature_String() {
		return atlHelperEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__IsUsedInSpecialOperator__BehaviorAnnex_Port_String() {
		return atlHelperEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetHyperperiod__FeatureInstance() {
		return atlHelperEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetTimingPrecision__NamedElement() {
		return atlHelperEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetListOfPath__PropertyAssociation() {
		return atlHelperEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__AllPortCount__BehaviorElement() {
		return atlHelperEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetStringLiteral__PropertyAssociation_String() {
		return atlHelperEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetEnumerators__Classifier() {
		return atlHelperEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__UniqueName__NamedElement_NamedElement() {
		return atlHelperEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetHyperperiodFromThreads__EList_String() {
		return atlHelperEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetSchedTableInit__ComponentInstance_ModeInstance() {
		return atlHelperEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__DeployedOnTransformedCpu__ComponentInstance() {
		return atlHelperEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetCpuToTransform() {
		return atlHelperEClass.getEOperations().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__ResetCpuToTransform__EList() {
		return atlHelperEClass.getEOperations().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAtlHelper__GetOutputPackageName__String() {
		return atlHelperEClass.getEOperations().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMathHelper() {
		return mathHelperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAadlHelper() {
		return aadlHelperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCodeGenHelper() {
		return codeGenHelperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCommunicationDimensioningHelper() {
		return communicationDimensioningHelperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCommunicationDimensioningHelper_ReaderReceivingTaskInstance() {
		return (EReference)communicationDimensioningHelperEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCommunicationDimensioningHelper_WriterInstances() {
		return (EReference)communicationDimensioningHelperEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCommunicationDimensioningHelper_WriterFeatureInstances() {
		return (EReference)communicationDimensioningHelperEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationDimensioningHelper_CprSize() {
		return (EAttribute)communicationDimensioningHelperEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationDimensioningHelper_CdwSize() {
		return (EAttribute)communicationDimensioningHelperEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationDimensioningHelper_CurrentPeriodRead() {
		return (EAttribute)communicationDimensioningHelperEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationDimensioningHelper_CurrentDeadlineWriteMap() {
		return (EAttribute)communicationDimensioningHelperEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationDimensioningHelper_BufferSize() {
		return (EAttribute)communicationDimensioningHelperEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationDimensioningHelper_Hyperperiod() {
		return (EAttribute)communicationDimensioningHelperEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCommunicationDimensioningHelper__GetCdwSize__FeatureInstance() {
		return communicationDimensioningHelperEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCommunicationDimensioningHelper__GetCurrentDeadlineWriteIndex__FeatureInstance_int() {
		return communicationDimensioningHelperEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCommunicationDimensioningHelper__GetCurrentPeriodReadIndex__int() {
		return communicationDimensioningHelperEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEventDataPortComDimHelper() {
		return eventDataPortComDimHelperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getRamsesException() {
		return ramsesExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getDimensioningException() {
		return dimensioningExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public HelpersFactory getHelpersFactory() {
		return (HelpersFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		atlHelperEClass = createEClass(ATL_HELPER);
		createEAttribute(atlHelperEClass, ATL_HELPER__OUTPUT_PACKAGE_NAME);
		createEOperation(atlHelperEClass, ATL_HELPER___ORDER_FEATURES__COMPONENTTYPE);
		createEOperation(atlHelperEClass, ATL_HELPER___COPY_LOCATION_REFERENCE__ELEMENT_ELEMENT);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_CURRENT_PERION_READ_TABLE__FEATUREINSTANCE);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_CURRENT_DEADLINE_WRITE_TABLE__FEATUREINSTANCE_FEATUREINSTANCE);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_BUFFER_SIZE__FEATUREINSTANCE);
		createEOperation(atlHelperEClass, ATL_HELPER___SET_DIRECTION__DIRECTEDFEATURE_STRING);
		createEOperation(atlHelperEClass, ATL_HELPER___IS_USED_IN_SPECIAL_OPERATOR__BEHAVIORANNEX_PORT_STRING);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_HYPERPERIOD__FEATUREINSTANCE);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_TIMING_PRECISION__NAMEDELEMENT);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_LIST_OF_PATH__PROPERTYASSOCIATION);
		createEOperation(atlHelperEClass, ATL_HELPER___ALL_PORT_COUNT__BEHAVIORELEMENT);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_STRING_LITERAL__PROPERTYASSOCIATION_STRING);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_ENUMERATORS__CLASSIFIER);
		createEOperation(atlHelperEClass, ATL_HELPER___UNIQUE_NAME__NAMEDELEMENT_NAMEDELEMENT);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_HYPERPERIOD_FROM_THREADS__ELIST_STRING);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_SCHED_TABLE_INIT__COMPONENTINSTANCE_MODEINSTANCE);
		createEOperation(atlHelperEClass, ATL_HELPER___DEPLOYED_ON_TRANSFORMED_CPU__COMPONENTINSTANCE);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_CPU_TO_TRANSFORM);
		createEOperation(atlHelperEClass, ATL_HELPER___RESET_CPU_TO_TRANSFORM__ELIST);
		createEOperation(atlHelperEClass, ATL_HELPER___GET_OUTPUT_PACKAGE_NAME__STRING);

		mathHelperEClass = createEClass(MATH_HELPER);

		aadlHelperEClass = createEClass(AADL_HELPER);

		codeGenHelperEClass = createEClass(CODE_GEN_HELPER);

		communicationDimensioningHelperEClass = createEClass(COMMUNICATION_DIMENSIONING_HELPER);
		createEReference(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER__READER_RECEIVING_TASK_INSTANCE);
		createEReference(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER__WRITER_INSTANCES);
		createEReference(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER__WRITER_FEATURE_INSTANCES);
		createEAttribute(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER__CPR_SIZE);
		createEAttribute(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER__CDW_SIZE);
		createEAttribute(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER__CURRENT_PERIOD_READ);
		createEAttribute(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER__CURRENT_DEADLINE_WRITE_MAP);
		createEAttribute(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER__BUFFER_SIZE);
		createEAttribute(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER__HYPERPERIOD);
		createEOperation(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER___GET_CDW_SIZE__FEATUREINSTANCE);
		createEOperation(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER___GET_CURRENT_DEADLINE_WRITE_INDEX__FEATUREINSTANCE_INT);
		createEOperation(communicationDimensioningHelperEClass, COMMUNICATION_DIMENSIONING_HELPER___GET_CURRENT_PERIOD_READ_INDEX__INT);

		eventDataPortComDimHelperEClass = createEClass(EVENT_DATA_PORT_COM_DIM_HELPER);

		// Create data types
		ramsesExceptionEDataType = createEDataType(RAMSES_EXCEPTION);
		dimensioningExceptionEDataType = createEDataType(DIMENSIONING_EXCEPTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Aadl2Package theAadl2Package = (Aadl2Package)EPackage.Registry.INSTANCE.getEPackage(Aadl2Package.eNS_URI);
		InstancePackage theInstancePackage = (InstancePackage)EPackage.Registry.INSTANCE.getEPackage(InstancePackage.eNS_URI);
		AadlBaPackage theAadlBaPackage = (AadlBaPackage)EPackage.Registry.INSTANCE.getEPackage(AadlBaPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		eventDataPortComDimHelperEClass.getESuperTypes().add(this.getCommunicationDimensioningHelper());

		// Initialize classes, features, and operations; add parameters
		initEClass(atlHelperEClass, AtlHelper.class, "AtlHelper", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAtlHelper_OutputPackageName(), ecorePackage.getEString(), "outputPackageName", null, 1, 1, AtlHelper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getAtlHelper__OrderFeatures__ComponentType(), theAadl2Package.getFeature(), "orderFeatures", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getComponentType(), "cpt", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__CopyLocationReference__Element_Element(), null, "copyLocationReference", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getElement(), "target", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getElement(), "source", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__GetCurrentPerionReadTable__FeatureInstance(), ecorePackage.getELong(), "getCurrentPerionReadTable", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getFeatureInstance(), "port", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__GetCurrentDeadlineWriteTable__FeatureInstance_FeatureInstance(), ecorePackage.getELong(), "getCurrentDeadlineWriteTable", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getFeatureInstance(), "port", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getFeatureInstance(), "destinationPort", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__GetBufferSize__FeatureInstance(), ecorePackage.getELong(), "getBufferSize", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getFeatureInstance(), "destinationFeatureInstance", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__SetDirection__DirectedFeature_String(), null, "setDirection", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getDirectedFeature(), "feature", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getString(), "direction", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__IsUsedInSpecialOperator__BehaviorAnnex_Port_String(), theAadl2Package.getBoolean(), "isUsedInSpecialOperator", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadlBaPackage.getBehaviorAnnex(), "ba", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getPort(), "p", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getString(), "operatorName", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__GetHyperperiod__FeatureInstance(), ecorePackage.getELong(), "getHyperperiod", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getFeatureInstance(), "port", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__GetTimingPrecision__NamedElement(), theAadl2Package.getString(), "getTimingPrecision", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getNamedElement(), "namedElement", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__GetListOfPath__PropertyAssociation(), theAadl2Package.getString(), "getListOfPath", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getPropertyAssociation(), "pa", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__AllPortCount__BehaviorElement(), theAadl2Package.getPort(), "allPortCount", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadlBaPackage.getBehaviorElement(), "e", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__GetStringLiteral__PropertyAssociation_String(), theAadl2Package.getStringLiteral(), "getStringLiteral", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getPropertyAssociation(), "pa", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getString(), "stringLiteralValue", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__GetEnumerators__Classifier(), theAadl2Package.getPropertyAssociation(), "getEnumerators", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getClassifier(), "classifier", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__UniqueName__NamedElement_NamedElement(), ecorePackage.getEString(), "uniqueName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getNamedElement(), "obj", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getNamedElement(), "context", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__GetHyperperiodFromThreads__EList_String(), ecorePackage.getELongObject(), "getHyperperiodFromThreads", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getComponentInstance(), "consideredTasks", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadlBaPackage.getString(), "scale", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__GetSchedTableInit__ComponentInstance_ModeInstance(), ecorePackage.getEString(), "getSchedTableInit", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getComponentInstance(), "c", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getModeInstance(), "mi", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__DeployedOnTransformedCpu__ComponentInstance(), ecorePackage.getEBoolean(), "deployedOnTransformedCpu", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getComponentInstance(), "c", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAtlHelper__GetCpuToTransform(), theInstancePackage.getComponentInstance(), "getCpuToTransform", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__ResetCpuToTransform__EList(), null, "resetCpuToTransform", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getComponentInstance(), "cpuList", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAtlHelper__GetOutputPackageName__String(), ecorePackage.getEString(), "getOutputPackageName", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "suffix", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mathHelperEClass, MathHelper.class, "MathHelper", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aadlHelperEClass, AadlHelper.class, "AadlHelper", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(codeGenHelperEClass, CodeGenHelper.class, "CodeGenHelper", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(communicationDimensioningHelperEClass, CommunicationDimensioningHelper.class, "CommunicationDimensioningHelper", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommunicationDimensioningHelper_ReaderReceivingTaskInstance(), theInstancePackage.getComponentInstance(), null, "readerReceivingTaskInstance", null, 1, 1, CommunicationDimensioningHelper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationDimensioningHelper_WriterInstances(), theInstancePackage.getComponentInstance(), null, "writerInstances", null, 0, -1, CommunicationDimensioningHelper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationDimensioningHelper_WriterFeatureInstances(), theInstancePackage.getFeatureInstance(), null, "writerFeatureInstances", null, 0, -1, CommunicationDimensioningHelper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCommunicationDimensioningHelper_CprSize(), ecorePackage.getELong(), "cprSize", null, 0, 1, CommunicationDimensioningHelper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(theInstancePackage.getFeatureInstance());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getELongObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getCommunicationDimensioningHelper_CdwSize(), g1, "cdwSize", null, 0, 1, CommunicationDimensioningHelper.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCommunicationDimensioningHelper_CurrentPeriodRead(), ecorePackage.getELongObject(), "currentPeriodRead", null, 0, -1, CommunicationDimensioningHelper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(theInstancePackage.getFeatureInstance());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEEList());
		g1.getETypeArguments().add(g2);
		EGenericType g3 = createEGenericType(ecorePackage.getELongObject());
		g2.getETypeArguments().add(g3);
		initEAttribute(getCommunicationDimensioningHelper_CurrentDeadlineWriteMap(), g1, "currentDeadlineWriteMap", null, 0, 1, CommunicationDimensioningHelper.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCommunicationDimensioningHelper_BufferSize(), ecorePackage.getELong(), "bufferSize", null, 0, 1, CommunicationDimensioningHelper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCommunicationDimensioningHelper_Hyperperiod(), ecorePackage.getELong(), "hyperperiod", null, 0, 1, CommunicationDimensioningHelper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getCommunicationDimensioningHelper__GetCdwSize__FeatureInstance(), ecorePackage.getELong(), "getCdwSize", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getFeatureInstance(), "writer", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getDimensioningException());

		op = initEOperation(getCommunicationDimensioningHelper__GetCurrentDeadlineWriteIndex__FeatureInstance_int(), ecorePackage.getELong(), "getCurrentDeadlineWriteIndex", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstancePackage.getFeatureInstance(), "writer", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "iteration", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getDimensioningException());

		op = initEOperation(getCommunicationDimensioningHelper__GetCurrentPeriodReadIndex__int(), ecorePackage.getELong(), "getCurrentPeriodReadIndex", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "iteration", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getDimensioningException());

		initEClass(eventDataPortComDimHelperEClass, EventDataPortComDimHelper.class, "EventDataPortComDimHelper", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize data types
		initEDataType(ramsesExceptionEDataType, RamsesException.class, "RamsesException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(dimensioningExceptionEDataType, DimensioningException.class, "DimensioningException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //HelpersPackageImpl
