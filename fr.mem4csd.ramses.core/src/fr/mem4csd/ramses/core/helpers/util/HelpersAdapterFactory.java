/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.helpers.util;

import fr.mem4csd.ramses.core.helpers.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage
 * @generated
 */
public class HelpersAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static HelpersPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HelpersAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = HelpersPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HelpersSwitch<Adapter> modelSwitch =
		new HelpersSwitch<Adapter>() {
			@Override
			public Adapter caseAtlHelper(AtlHelper object) {
				return createAtlHelperAdapter();
			}
			@Override
			public Adapter caseMathHelper(MathHelper object) {
				return createMathHelperAdapter();
			}
			@Override
			public Adapter caseAadlHelper(AadlHelper object) {
				return createAadlHelperAdapter();
			}
			@Override
			public Adapter caseCodeGenHelper(CodeGenHelper object) {
				return createCodeGenHelperAdapter();
			}
			@Override
			public Adapter caseCommunicationDimensioningHelper(CommunicationDimensioningHelper object) {
				return createCommunicationDimensioningHelperAdapter();
			}
			@Override
			public Adapter caseEventDataPortComDimHelper(EventDataPortComDimHelper object) {
				return createEventDataPortComDimHelperAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.helpers.AtlHelper <em>Atl Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.helpers.AtlHelper
	 * @generated
	 */
	public Adapter createAtlHelperAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.helpers.MathHelper <em>Math Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.helpers.MathHelper
	 * @generated
	 */
	public Adapter createMathHelperAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.helpers.AadlHelper <em>Aadl Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.helpers.AadlHelper
	 * @generated
	 */
	public Adapter createAadlHelperAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.helpers.CodeGenHelper <em>Code Gen Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.helpers.CodeGenHelper
	 * @generated
	 */
	public Adapter createCodeGenHelperAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper <em>Communication Dimensioning Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper
	 * @generated
	 */
	public Adapter createCommunicationDimensioningHelperAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.helpers.EventDataPortComDimHelper <em>Event Data Port Com Dim Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.helpers.EventDataPortComDimHelper
	 * @generated
	 */
	public Adapter createEventDataPortComDimHelperAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //HelpersAdapterFactory
