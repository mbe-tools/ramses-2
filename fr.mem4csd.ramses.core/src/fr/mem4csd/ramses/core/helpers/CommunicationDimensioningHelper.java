/**
 */
package fr.mem4csd.ramses.core.helpers;

import fr.mem4csd.ramses.core.helpers.impl.DimensioningException;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Dimensioning Helper</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getReaderReceivingTaskInstance <em>Reader Receiving Task Instance</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getWriterInstances <em>Writer Instances</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getWriterFeatureInstances <em>Writer Feature Instances</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCprSize <em>Cpr Size</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCdwSize <em>Cdw Size</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCurrentPeriodRead <em>Current Period Read</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCurrentDeadlineWriteMap <em>Current Deadline Write Map</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getBufferSize <em>Buffer Size</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getHyperperiod <em>Hyperperiod</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getCommunicationDimensioningHelper()
 * @model abstract="true"
 * @generated
 */
public interface CommunicationDimensioningHelper extends EObject {

	/**
	 * Returns the value of the '<em><b>Reader Receiving Task Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reader Receiving Task Instance</em>' reference.
	 * @see #setReaderReceivingTaskInstance(ComponentInstance)
	 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getCommunicationDimensioningHelper_ReaderReceivingTaskInstance()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getReaderReceivingTaskInstance();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getReaderReceivingTaskInstance <em>Reader Receiving Task Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reader Receiving Task Instance</em>' reference.
	 * @see #getReaderReceivingTaskInstance()
	 * @generated
	 */
	void setReaderReceivingTaskInstance(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>Writer Instances</b></em>' reference list.
	 * The list contents are of type {@link org.osate.aadl2.instance.ComponentInstance}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Writer Instances</em>' reference list.
	 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getCommunicationDimensioningHelper_WriterInstances()
	 * @model
	 * @generated
	 */
	EList<ComponentInstance> getWriterInstances();

	/**
	 * Returns the value of the '<em><b>Writer Feature Instances</b></em>' reference list.
	 * The list contents are of type {@link org.osate.aadl2.instance.FeatureInstance}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Writer Feature Instances</em>' reference list.
	 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getCommunicationDimensioningHelper_WriterFeatureInstances()
	 * @model
	 * @generated
	 */
	EList<FeatureInstance> getWriterFeatureInstances();

	/**
	 * Returns the value of the '<em><b>Cpr Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpr Size</em>' attribute.
	 * @see #setCprSize(long)
	 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getCommunicationDimensioningHelper_CprSize()
	 * @model
	 * @generated
	 */
	long getCprSize();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCprSize <em>Cpr Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cpr Size</em>' attribute.
	 * @see #getCprSize()
	 * @generated
	 */
	void setCprSize(long value);

	/**
	 * Returns the value of the '<em><b>Cdw Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cdw Size</em>' attribute.
	 * @see #setCdwSize(Map)
	 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getCommunicationDimensioningHelper_CdwSize()
	 * @model transient="true"
	 * @generated
	 */
	Map<FeatureInstance, Long> getCdwSize();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCdwSize <em>Cdw Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cdw Size</em>' attribute.
	 * @see #getCdwSize()
	 * @generated
	 */
	void setCdwSize(Map<FeatureInstance, Long> value);

	/**
	 * Returns the value of the '<em><b>Current Period Read</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Long}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Period Read</em>' attribute list.
	 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getCommunicationDimensioningHelper_CurrentPeriodRead()
	 * @model unique="false"
	 * @generated
	 */
	EList<Long> getCurrentPeriodRead();

	/**
	 * Returns the value of the '<em><b>Current Deadline Write Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Deadline Write Map</em>' attribute.
	 * @see #setCurrentDeadlineWriteMap(Map)
	 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getCommunicationDimensioningHelper_CurrentDeadlineWriteMap()
	 * @model transient="true"
	 * @generated
	 */
	Map<FeatureInstance, EList<Long>> getCurrentDeadlineWriteMap();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getCurrentDeadlineWriteMap <em>Current Deadline Write Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Deadline Write Map</em>' attribute.
	 * @see #getCurrentDeadlineWriteMap()
	 * @generated
	 */
	void setCurrentDeadlineWriteMap(Map<FeatureInstance, EList<Long>> value);

	/**
	 * Returns the value of the '<em><b>Buffer Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Buffer Size</em>' attribute.
	 * @see #setBufferSize(long)
	 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getCommunicationDimensioningHelper_BufferSize()
	 * @model
	 * @generated
	 */
	long getBufferSize();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getBufferSize <em>Buffer Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Buffer Size</em>' attribute.
	 * @see #getBufferSize()
	 * @generated
	 */
	void setBufferSize(long value);

	/**
	 * Returns the value of the '<em><b>Hyperperiod</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hyperperiod</em>' attribute.
	 * @see #setHyperperiod(long)
	 * @see fr.mem4csd.ramses.core.helpers.HelpersPackage#getCommunicationDimensioningHelper_Hyperperiod()
	 * @model
	 * @generated
	 */
	long getHyperperiod();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.helpers.CommunicationDimensioningHelper#getHyperperiod <em>Hyperperiod</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hyperperiod</em>' attribute.
	 * @see #getHyperperiod()
	 * @generated
	 */
	void setHyperperiod(long value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="fr.mem4csd.ramses.core.helpers.DimensioningException"
	 * @generated
	 */
	long getCdwSize(FeatureInstance writer) throws DimensioningException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="fr.mem4csd.ramses.core.helpers.DimensioningException"
	 * @generated
	 */
	long getCurrentDeadlineWriteIndex(FeatureInstance writer, int iteration) throws DimensioningException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="fr.mem4csd.ramses.core.helpers.DimensioningException"
	 * @generated
	 */
	long getCurrentPeriodReadIndex(int iteration) throws DimensioningException;

} // CommunicationDimensioningHelper
