/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel;

import java.io.File;

import java.util.Map;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ramses Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getOutputDirectory <em>Output Directory</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getRuntimeDirectory <em>Runtime Directory</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getTargetId <em>Target Id</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#isExposeRuntimeSharedResources <em>Expose Runtime Shared Resources</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getPropertyMap <em>Property Map</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesConfiguration()
 * @model
 * @generated
 */
public interface RamsesConfiguration extends EObject {
	boolean USES_GUI = false;
	String PREFIX = "fr.mem4csd.ramses.";
	String OUTPUT_DIR = "output.directory";
	String TARGET_ID = "target.id";
	String RUNTIME_PATH = "runtime.path";
	String EXPOSE_RUNTIME_SHARED_RESOURCES = "expose.runtime.shared.resources";

	/**
	 * Returns the value of the '<em><b>Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Directory</em>' attribute.
	 * @see #setOutputDirectory(File)
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesConfiguration_OutputDirectory()
	 * @model dataType="fr.mem4csd.ramses.core.ramsesviewmodel.EFile"
	 * @generated
	 */
	File getOutputDirectory();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getOutputDirectory <em>Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Directory</em>' attribute.
	 * @see #getOutputDirectory()
	 * @generated
	 */
	void setOutputDirectory(File value);

	/**
	 * Returns the value of the '<em><b>Runtime Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Directory</em>' attribute.
	 * @see #setRuntimeDirectory(File)
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesConfiguration_RuntimeDirectory()
	 * @model dataType="fr.mem4csd.ramses.core.ramsesviewmodel.EFile"
	 * @generated
	 */
	File getRuntimeDirectory();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getRuntimeDirectory <em>Runtime Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runtime Directory</em>' attribute.
	 * @see #getRuntimeDirectory()
	 * @generated
	 */
	void setRuntimeDirectory(File value);

	/**
	 * Returns the value of the '<em><b>Target Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Id</em>' attribute.
	 * @see #setTargetId(String)
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesConfiguration_TargetId()
	 * @model
	 * @generated
	 */
	String getTargetId();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getTargetId <em>Target Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Id</em>' attribute.
	 * @see #getTargetId()
	 * @generated
	 */
	void setTargetId(String value);

	/**
	 * Returns the value of the '<em><b>Expose Runtime Shared Resources</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expose Runtime Shared Resources</em>' attribute.
	 * @see #setExposeRuntimeSharedResources(boolean)
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesConfiguration_ExposeRuntimeSharedResources()
	 * @model
	 * @generated
	 */
	boolean isExposeRuntimeSharedResources();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#isExposeRuntimeSharedResources <em>Expose Runtime Shared Resources</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expose Runtime Shared Resources</em>' attribute.
	 * @see #isExposeRuntimeSharedResources()
	 * @generated
	 */
	void setExposeRuntimeSharedResources(boolean value);

	/**
	 * Returns the value of the '<em><b>Property Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Map</em>' attribute.
	 * @see #setPropertyMap(Map)
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesConfiguration_PropertyMap()
	 * @model transient="true"
	 * @generated
	 */
	Map<String, String> getPropertyMap();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getPropertyMap <em>Property Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Map</em>' attribute.
	 * @see #getPropertyMap()
	 * @generated
	 */
	void setPropertyMap(Map<String, String> value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.mem4csd.ramses.core.ramsesviewmodel.EConfigStatus"
	 * @generated
	 */
	ConfigStatus setRamsesOutputDir(String path);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="fr.mem4csd.ramses.core.ramsesviewmodel.ConfigurationException"
	 * @generated
	 */
	void setRuntimePath(String path) throws ConfigurationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="fr.mem4csd.ramses.core.ramsesviewmodel.EConfigStatus"
	 * @generated
	 */
	ConfigStatus setGenerationTargetId(String path);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="fr.mem4csd.ramses.core.ramsesviewmodel.EFile"
	 * @generated
	 */
	File getRamsesOutputDir();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getRuntimePath();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void log();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="fr.mem4csd.ramses.core.ramsesviewmodel.CoreException fr.mem4csd.ramses.core.ramsesviewmodel.ConfigurationException" projectDataType="fr.mem4csd.ramses.core.ramsesviewmodel.IProject"
	 * @generated
	 */
	void fetchProperties(IProject project) throws CoreException, ConfigurationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="fr.mem4csd.ramses.core.ramsesviewmodel.CoreException" projectDataType="fr.mem4csd.ramses.core.ramsesviewmodel.IProject"
	 * @generated
	 */
	void saveConfig(IProject project, Map<?, ?> properties) throws CoreException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated NOT
	 */
	void updateTargetPath() throws ConfigurationException;

} // RamsesConfiguration
