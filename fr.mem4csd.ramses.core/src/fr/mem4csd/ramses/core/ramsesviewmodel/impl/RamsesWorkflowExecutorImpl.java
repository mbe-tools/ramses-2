/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel.impl;

import de.mdelab.workflow.Workflow;
import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.mdelab.workflow.util.WorkflowLauncher;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowExecutor;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Map;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ramses Workflow Executor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RamsesWorkflowExecutorImpl extends MinimalEObjectImpl.Container implements RamsesWorkflowExecutor {
	
	//protected ResourceSet resourceSet;
	//protected WorkflowLauncher launcher;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RamsesWorkflowExecutorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamsesviewmodelPackage.Literals.RAMSES_WORKFLOW_EXECUTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public WorkflowExecutionContext execute(RamsesWorkflowConfiguration config, ResourceSet resourceSet, IProgressMonitor monitor) throws WorkflowExecutionException, IOException {
		final URI workflowUri =  config.getWorkflowURI();
		
		final Resource workflowResource = resourceSet.getResource(workflowUri, true);

		if ( workflowResource.getContents().isEmpty() ) {
			throw new WorkflowExecutionException( "Resource '" + workflowResource.getURI() + "' is empty." );
		}
		
		final Workflow workflow = (Workflow) workflowResource.getContents().get(0);
		
		final Map<String, String> propertyValues = config.getProperties().map();
		RamsesWorkflowKeys.setDefaultPropertyValues( propertyValues );
		
		// final WorkflowExecutionContext context;
//		try {
			// context = 
		final WorkflowLauncher launcher = new WorkflowLauncher(Collections.emptyList(), resourceSet, System.out);
		launcher.execute( workflow, propertyValues, monitor );
//		} catch (WorkflowExecutionException e) {
//			Dialog.showError("Workflow Execution", "Error: " + e.getMessage());
//			throw e;
//		} catch (IOException e) {
//			Dialog.showError("IO Exception", "Error: " + e.getMessage());
//			throw e;
//		}
//		
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_EXECUTOR___EXECUTE__RAMSESWORKFLOWCONFIGURATION_RESOURCESET_IPROGRESSMONITOR:
				try {
					return execute((RamsesWorkflowConfiguration)arguments.get(0), (ResourceSet)arguments.get(1), (IProgressMonitor)arguments.get(2));
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
		}
		return super.eInvoke(operationID, arguments);
	}


} //RamsesWorkflowExecutorImpl
