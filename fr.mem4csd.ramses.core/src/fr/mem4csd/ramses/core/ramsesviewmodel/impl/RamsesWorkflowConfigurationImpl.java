/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel.impl;

import de.mdelab.workflow.Workflow;
import de.mdelab.workflow.WorkflowProperty;
import de.mdelab.workflow.helpers.HelpersPackage;

import de.mdelab.workflow.helpers.impl.MapEntryImpl;

import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map.Entry;
import java.util.Properties;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ramses Workflow Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowConfigurationImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowConfigurationImpl#getWorkflowURI <em>Workflow URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowConfigurationImpl#getProperties <em>Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RamsesWorkflowConfigurationImpl extends MinimalEObjectImpl.Container implements RamsesWorkflowConfiguration {
	
	public final static String NULL_STRING_PLACEHOLDER = "<CONFIG_VALUE_NULL>";
	public final static String CONFIG_NAME_KEY = "WORKFLOW_CONFIG_NAME";
	public final static String CONFIG_URI_KEY = "WORKFLOW_URI";
	
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorkflowURI() <em>Workflow URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowURI()
	 * @generated
	 * @ordered
	 */
	protected static final URI WORKFLOW_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWorkflowURI() <em>Workflow URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowURI()
	 * @generated
	 * @ordered
	 */
	protected URI workflowURI = WORKFLOW_URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> properties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RamsesWorkflowConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamsesviewmodelPackage.Literals.RAMSES_WORKFLOW_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public URI getWorkflowURI() {
		return workflowURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorkflowURI(URI newWorkflowURI) {
		URI oldWorkflowURI = workflowURI;
		workflowURI = newWorkflowURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__WORKFLOW_URI, oldWorkflowURI, workflowURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getProperties() {
		if (properties == null) {
			properties = new EcoreEMap<String,String>(HelpersPackage.Literals.MAP_ENTRY, MapEntryImpl.class, this, RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void store(URI uri) throws IOException {
		Properties props = new Properties();
		props.put(CONFIG_NAME_KEY, getName());
		props.put(CONFIG_URI_KEY, getWorkflowURI().toString());
		
		for ( final Entry<String, String> entry : properties ) {
			props.put( entry.getKey(), encodeNull(entry.getValue()) );
		}
		
		final ExtensibleURIConverterImpl ec = new ExtensibleURIConverterImpl();
		props.store(ec.createOutputStream(uri), null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void load(URI uri) throws IOException {
		final ExtensibleURIConverterImpl ec = new ExtensibleURIConverterImpl();
		Properties props = new Properties();
		props.load(ec.createInputStream(uri));
		
		setName(props.getProperty(CONFIG_NAME_KEY));
		setWorkflowURI(URI.createURI(props.getProperty(CONFIG_URI_KEY)));
		
		getProperties();
		for( final Entry<Object, Object> entry : props.entrySet() ) {
			properties.put( (String)entry.getKey(), decodeNull( (String)entry.getValue()) );
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void create(String name, Workflow workflow) {
		setName(name);
		setWorkflowURI(workflow.eResource().getURI());
		
		getProperties();
		for( final WorkflowProperty property : workflow.getProperties() ) {
			properties.put(property.getName(), property.getDefaultValue());
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__NAME:
				return getName();
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__WORKFLOW_URI:
				return getWorkflowURI();
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__PROPERTIES:
				if (coreType) return getProperties();
				else return getProperties().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__NAME:
				setName((String)newValue);
				return;
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__WORKFLOW_URI:
				setWorkflowURI((URI)newValue);
				return;
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__PROPERTIES:
				((EStructuralFeature.Setting)getProperties()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__WORKFLOW_URI:
				setWorkflowURI(WORKFLOW_URI_EDEFAULT);
				return;
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__PROPERTIES:
				getProperties().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__WORKFLOW_URI:
				return WORKFLOW_URI_EDEFAULT == null ? workflowURI != null : !WORKFLOW_URI_EDEFAULT.equals(workflowURI);
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION__PROPERTIES:
				return properties != null && !properties.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION___STORE__URI:
				try {
					store((URI)arguments.get(0));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION___LOAD__URI:
				try {
					load((URI)arguments.get(0));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION___CREATE__STRING_WORKFLOW:
				create((String)arguments.get(0), (Workflow)arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", workflowURI: ");
		result.append(workflowURI);
		result.append(')');
		return result.toString();
	}
	
	protected String encodeNull( final String input ) {
		return input == null ? NULL_STRING_PLACEHOLDER : input;
	}
	
	protected String decodeNull( final String input ) {
		return input.equals(NULL_STRING_PLACEHOLDER) ? null : input;
	}

} //RamsesWorkflowConfigurationImpl
