/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel.impl;

import fr.mem4csd.ramses.core.ramsesviewmodel.ConfigStatus;
import fr.mem4csd.ramses.core.ramsesviewmodel.ConfigurationException;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.osate.utils.internal.FileUtils;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

import com.google.common.base.Splitter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ramses Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesConfigurationImpl#getOutputDirectory <em>Output Directory</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesConfigurationImpl#getRuntimeDirectory <em>Runtime Directory</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesConfigurationImpl#getTargetId <em>Target Id</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesConfigurationImpl#isExposeRuntimeSharedResources <em>Expose Runtime Shared Resources</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesConfigurationImpl#getPropertyMap <em>Property Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RamsesConfigurationImpl extends MinimalEObjectImpl.Container implements RamsesConfiguration {
	
	/**
	 * The default value of the '{@link #getOutputDirectory() <em>Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDirectory()
	 * @generated
	 * @ordered
	 */
	protected static final File OUTPUT_DIRECTORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutputDirectory() <em>Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDirectory()
	 * @generated
	 * @ordered
	 */
	protected File outputDirectory = OUTPUT_DIRECTORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getRuntimeDirectory() <em>Runtime Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeDirectory()
	 * @generated
	 * @ordered
	 */
	protected static final File RUNTIME_DIRECTORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRuntimeDirectory() <em>Runtime Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeDirectory()
	 * @generated
	 * @ordered
	 */
	protected File runtimeDirectory = RUNTIME_DIRECTORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetId() <em>Target Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetId()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetId() <em>Target Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetId()
	 * @generated
	 * @ordered
	 */
	protected String targetId = TARGET_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isExposeRuntimeSharedResources() <em>Expose Runtime Shared Resources</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExposeRuntimeSharedResources()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXPOSE_RUNTIME_SHARED_RESOURCES_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExposeRuntimeSharedResources() <em>Expose Runtime Shared Resources</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExposeRuntimeSharedResources()
	 * @generated
	 * @ordered
	 */
	protected boolean exposeRuntimeSharedResources = EXPOSE_RUNTIME_SHARED_RESOURCES_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPropertyMap() <em>Property Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyMap()
	 * @generated NOT
	 * @ordered
	 */
	protected Map<String, String> propertyMap = new HashMap<String, String>();

	private static Logger _LOGGER = Logger.getLogger(RamsesConfiguration.class) ;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RamsesConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamsesviewmodelPackage.Literals.RAMSES_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public File getOutputDirectory() {
		String value = propertyMap.get(RamsesConfiguration.OUTPUT_DIR);
		File outputDirectory;

		if (value != null)
			outputDirectory = new File(value);
		else
			outputDirectory = OUTPUT_DIRECTORY_EDEFAULT;

		return outputDirectory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setOutputDirectory(File newOutputDirectory) {
		String oldOutputDirectory = propertyMap.get(OUTPUT_DIR);
		propertyMap.put(OUTPUT_DIR, newOutputDirectory.getAbsolutePath());
		outputDirectory = newOutputDirectory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamsesviewmodelPackage.RAMSES_CONFIGURATION__OUTPUT_DIRECTORY, oldOutputDirectory, outputDirectory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public File getRuntimeDirectory() {
		return null;	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setRuntimeDirectory(File newRuntimeDirectory) {
		String oldRuntimeDirectory = propertyMap.get(RUNTIME_PATH);
		propertyMap.put(RUNTIME_PATH, newRuntimeDirectory.getAbsolutePath());
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamsesviewmodelPackage.RAMSES_CONFIGURATION__RUNTIME_DIRECTORY, oldRuntimeDirectory, runtimeDirectory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTargetId() {
		return targetId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetId(String newTargetId) {
		String oldTargetId = targetId;
		targetId = newTargetId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamsesviewmodelPackage.RAMSES_CONFIGURATION__TARGET_ID, oldTargetId, targetId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExposeRuntimeSharedResources() {
		return exposeRuntimeSharedResources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExposeRuntimeSharedResources(boolean newExposeRuntimeSharedResources) {
		boolean oldExposeRuntimeSharedResources = exposeRuntimeSharedResources;
		exposeRuntimeSharedResources = newExposeRuntimeSharedResources;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamsesviewmodelPackage.RAMSES_CONFIGURATION__EXPOSE_RUNTIME_SHARED_RESOURCES, oldExposeRuntimeSharedResources, exposeRuntimeSharedResources));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<String, String> getPropertyMap() {
		return propertyMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyMap(Map<String, String> newPropertyMap) {
		Map<String, String> oldPropertyMap = propertyMap;
		propertyMap = newPropertyMap;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamsesviewmodelPackage.RAMSES_CONFIGURATION__PROPERTY_MAP, oldPropertyMap, propertyMap));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ConfigStatus setRamsesOutputDir(String path) {
		if(! (path == null || path.isEmpty()))
		{
			File outputDir = new File(path) ;

			setOutputDirectory(outputDir);

			ConfigStatus.SET.msg = "set output directory to \'" + path + "\'" ;
			_LOGGER.info(ConfigStatus.SET.msg);
			return ConfigStatus.SET ;
		}
		else
		{
			ConfigStatus.NOT_VALID.msg = "output directory is not configured" ;
			return ConfigStatus.NOT_VALID ;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @throws ConfigurationException 
	 * @generated NOT
	 */
	public void setRuntimePath(String path) throws ConfigurationException {
		
		AadlToTargetBuildGenerator gen = RamsesWorkflowViewModel.INSTANCE.getGeneratorFromTargetName( getTargetId() ) ;
		
		if (gen == null) return;
		Boolean isValidURI = false;
		URI runtimePath = null;
		if(path != null && !path.isEmpty())
		{
			if(path.contains("="))
			{
				if(path.endsWith(","))
					path = path.substring(0, path.lastIndexOf(","));
				
				Boolean isValidMultipleURI = true;
				Map<String, String> installPaths = Splitter.on(",").withKeyValueSeparator("=").split(path);
				for(String singlePath: installPaths.values())
				{
					
					if(singlePath==null || singlePath.isEmpty() || singlePath.equals("null"))
						continue;
					URI runtimePathForCheck = fileCheckerUri(singlePath);
					isValidMultipleURI &= (runtimePathForCheck!=null);
				}
				isValidURI = isValidMultipleURI;
			}
			else
			{
				runtimePath = fileCheckerUri(path);
				isValidURI = runtimePath!=null;
			}
		}
		else
		{
			_LOGGER.info("runtime path has not been set.");
		}

		
		
		// The runtime path can be null.
		if(gen.validateTargetPath(runtimePath))
		{
			if(isValidURI)
			{
				propertyMap.put(RamsesConfiguration.RUNTIME_PATH, path);
				ConfigStatus.SET.msg = "set runtime path to \'" + path + "\'" ;
				_LOGGER.info(ConfigStatus.SET.msg);
				// Issue #37
				//return ConfigStatus.SET ;
			}
		}
		else
		{
			
			String msg = "";

			if(path == null || path.isEmpty())
			{
				msg = "missing runtime path" ;
			}
			else
			{
				msg = "\'" + path + "\' is not a valid runtime path" ;
			}

			ConfigStatus.NOT_VALID.msg = msg;

			// Issue #37
			throw new ConfigurationException( ConfigStatus.NOT_VALID );
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ConfigStatus setGenerationTargetId(String id) {
		setTargetId(id);
		
		if (getTargetId() != null && getTargetId().equals( id ) ) {
			return ConfigStatus.SET;
		} else {
			return ConfigStatus.NOT_VALID;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public File getRamsesOutputDir() {
		return getOutputDirectory();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getRuntimePath() {
		Map<String, AadlToTargetBuildGenerator> Generators = RamsesWorkflowViewModelImpl.INSTANCE.getAvailableGenerators();
		String targetId = getTargetId();
		if(targetId == null)
			return null;
		AadlToTargetBuildGenerator buildGen = Generators.get(targetId);
		String value = propertyMap.get(buildGen.getTargetName().toLowerCase());
		
		if (value != null)
			return value;
		else
		{
			value = propertyMap.get(RUNTIME_PATH);
			if(value==null)
				runtimeDirectory = RUNTIME_DIRECTORY_EDEFAULT;
			else
				return value;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void log() {
//		_LOGGER.info("RAMSES resource directory is \'" + _RAMSES_RESOURCE_DIR + '\'') ;
//		_LOGGER.info("ATL resource directory is \'" + _ATL_RESOURCE_DIR + '\'') ;
//		_LOGGER.info("AADL resource directory is \'" + _AADL_PACKAGE_DIR + '\'') ;
		_LOGGER.info("output directory is \'" + getRamsesOutputDir() + '\'') ;
		_LOGGER.info("generation target is \'" + getTargetId() + '\'') ;
		_LOGGER.info("runtime path is \'" + getRuntimeDirectory() + '\'') ;
	}

	public static String fetchPropertiesValue(IProject project,
			String property) throws
	CoreException
	{
		String value = project.getPersistentProperty(new QualifiedName(
				PREFIX,
				property));
		if(value==null)
			value = project.getLocation().toString();
		return value ;
	}

	public static String fetchPropertiesValue(IProject project,
			String property, String defaultValue) throws
	CoreException
	{
		String value = project.getPersistentProperty(new QualifiedName(
				PREFIX,
				property));
		if(value==null)
			value = defaultValue;
		return value ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @throws CoreException 
	 * @throws ConfigurationException 
	 * @generated NOT
	 */
	public void fetchProperties(IProject project) throws CoreException, ConfigurationException {
		ConfigStatus status ;
		Map<QualifiedName, String> persisentProperties = project.getPersistentProperties();
		
		for (Map.Entry<QualifiedName, String> property : persisentProperties.entrySet()) {
			if (property.getKey().getLocalName() == RamsesConfiguration.RUNTIME_PATH)
				continue;

			propertyMap.put(property.getKey().getLocalName(), property.getValue());
		}
		
		status = setGenerationTargetId(fetchPropertiesValue(project, TARGET_ID, null)) ;
		if(status != ConfigStatus.SET)
		{
			throw new ConfigurationException(status) ;
		}

		updateTargetPath();
		
		status = setRamsesOutputDir(fetchPropertiesValue(project, OUTPUT_DIR, project.getLocation().append("output").toString()));
		
		if(status != ConfigStatus.SET)
		{
			throw new ConfigurationException(status) ;
		}

		// Issue #37
		/*status =*///setRuntimePath(propertyMap.get(TARGET_ID));
//		if(status != ConfigStatus.SET)
//		{
//		throw new ConfigurationException(status) ;
//		}

		boolean exposeRuntimeSharedResources = 
				Boolean.valueOf(fetchPropertiesValue(project, EXPOSE_RUNTIME_SHARED_RESOURCES, null));
		setExposeRuntimeSharedResources(exposeRuntimeSharedResources) ;
	}

	public void updateTargetPath() throws ConfigurationException {
		Map<String, AadlToTargetBuildGenerator> Generators = RamsesWorkflowViewModelImpl.INSTANCE.getAvailableGeneratorsFromTargetId();

		AadlToTargetBuildGenerator gen = RamsesWorkflowViewModel.INSTANCE.getGeneratorFromTargetName( getTargetId() ) ;

		String targetInstallDir ="";
		for(String key: Generators.keySet())
		{
			String value = Platform.getPreferencesService().
				    getString("fr.mem4csd.ramses.core.RamsesTargetPreferencePage", key, null, null);
			if(value == null)
			{
				Preferences preferences = InstanceScope.INSTANCE
		                .getNode("fr.mem4csd.ramses.core.RamsesTargetPreferencePage");
				value = preferences.get(key, null);
			}
			if(value!=null)
				propertyMap.put(key, value);
			
			
			if(gen.getTargetName().equals("All"))
				targetInstallDir += key+"="+value+",";
		}
		
		if(!targetInstallDir.isEmpty())
		{
			propertyMap.put(gen.getTargetName().toLowerCase(), targetInstallDir);
			setRuntimePath(targetInstallDir.substring(0,targetInstallDir.length()-1));
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void saveConfig(IProject project, Map<?, ?> properties) throws CoreException {
		Set<String> Targets = RamsesWorkflowViewModelImpl.INSTANCE.getAvailableGeneratorNames();

		for (String element : (Set<String>) properties.keySet()) {
			
			if (Targets.contains(element)) {
				continue;
			}

			String value = (String) properties.get(element);
			project.setPersistentProperty(new QualifiedName(RamsesConfigurationImpl.PREFIX, element), value);
		}

		Map<String, AadlToTargetBuildGenerator> generators = RamsesWorkflowViewModelImpl.INSTANCE.getAvailableGenerators();
		
		for (String name : generators.keySet()) {
			String value = (String) properties.get(name);
			if (value != null) {
				Preferences preferences = InstanceScope.INSTANCE
		                .getNode("fr.mem4csd.ramses.core.RamsesTargetPreferencePage");
				preferences.put(generators.get(name).getTargetName().toLowerCase(), value);
				try {
                    // forces the application to save the preferences
                    preferences.flush();
                } catch (BackingStoreException e2) {
                    e2.printStackTrace();
                }
			}
		}

		setRamsesOutputDir((String) properties.get(RamsesConfiguration.OUTPUT_DIR));
		project.setPersistentProperty(new QualifiedName(RamsesConfiguration.PREFIX, RamsesConfiguration.OUTPUT_DIR),
				getOutputDirectory().getAbsolutePath());

		project.setPersistentProperty(new QualifiedName(RamsesConfiguration.PREFIX, RamsesConfiguration.TARGET_ID),
				getTargetId());
		

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__OUTPUT_DIRECTORY:
				return getOutputDirectory();
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__RUNTIME_DIRECTORY:
				return getRuntimeDirectory();
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__TARGET_ID:
				return getTargetId();
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__EXPOSE_RUNTIME_SHARED_RESOURCES:
				return isExposeRuntimeSharedResources();
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__PROPERTY_MAP:
				return getPropertyMap();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__OUTPUT_DIRECTORY:
				setOutputDirectory((File)newValue);
				return;
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__RUNTIME_DIRECTORY:
				setRuntimeDirectory((File)newValue);
				return;
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__TARGET_ID:
				setTargetId((String)newValue);
				return;
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__EXPOSE_RUNTIME_SHARED_RESOURCES:
				setExposeRuntimeSharedResources((Boolean)newValue);
				return;
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__PROPERTY_MAP:
				setPropertyMap((Map<String, String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__OUTPUT_DIRECTORY:
				setOutputDirectory(OUTPUT_DIRECTORY_EDEFAULT);
				return;
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__RUNTIME_DIRECTORY:
				setRuntimeDirectory(RUNTIME_DIRECTORY_EDEFAULT);
				return;
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__TARGET_ID:
				setTargetId(TARGET_ID_EDEFAULT);
				return;
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__EXPOSE_RUNTIME_SHARED_RESOURCES:
				setExposeRuntimeSharedResources(EXPOSE_RUNTIME_SHARED_RESOURCES_EDEFAULT);
				return;
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__PROPERTY_MAP:
				setPropertyMap((Map<String, String>)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__OUTPUT_DIRECTORY:
				return OUTPUT_DIRECTORY_EDEFAULT == null ? outputDirectory != null : !OUTPUT_DIRECTORY_EDEFAULT.equals(outputDirectory);
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__RUNTIME_DIRECTORY:
				return RUNTIME_DIRECTORY_EDEFAULT == null ? runtimeDirectory != null : !RUNTIME_DIRECTORY_EDEFAULT.equals(runtimeDirectory);
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__TARGET_ID:
				return TARGET_ID_EDEFAULT == null ? targetId != null : !TARGET_ID_EDEFAULT.equals(targetId);
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__EXPOSE_RUNTIME_SHARED_RESOURCES:
				return exposeRuntimeSharedResources != EXPOSE_RUNTIME_SHARED_RESOURCES_EDEFAULT;
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION__PROPERTY_MAP:
				return propertyMap != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION___SET_RAMSES_OUTPUT_DIR__STRING:
				return setRamsesOutputDir((String)arguments.get(0));
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION___SET_RUNTIME_PATH__STRING:
				try {
					setRuntimePath((String)arguments.get(0));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION___SET_GENERATION_TARGET_ID__STRING:
				return setGenerationTargetId((String)arguments.get(0));
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION___GET_RAMSES_OUTPUT_DIR:
				return getRamsesOutputDir();
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION___GET_RUNTIME_PATH:
				return getRuntimePath();
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION___LOG:
				log();
				return null;
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION___FETCH_PROPERTIES__IPROJECT:
				try {
					fetchProperties((IProject)arguments.get(0));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION___SAVE_CONFIG__IPROJECT_MAP:
				try {
					saveConfig((IProject)arguments.get(0), (Map<?, ?>)arguments.get(1));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (outputDirectory: ");
		result.append(outputDirectory);
		result.append(", runtimeDirectory: ");
		result.append(runtimeDirectory);
		result.append(", targetId: ");
		result.append(targetId);
		result.append(", exposeRuntimeSharedResources: ");
		result.append(exposeRuntimeSharedResources);
		result.append(", propertyMap: ");
		result.append(propertyMap);
		result.append(')');
		return result.toString();
	}

	private static URI fileCheckerUri(String path)
	throws ConfigurationException {
		return URI.createURI( fileChecker( path ).getAbsolutePath() );
	}
	
	private static File fileChecker(String path) throws ConfigurationException
	{
		try
		{
			return FileUtils.stringToFile(path) ;
		}
		catch(FileNotFoundException e)
		{
			ConfigStatus.NOT_FOUND.msg = "\'" + path + "\' is not found";

			throw new ConfigurationException(ConfigStatus.NOT_FOUND) ;
		}
	}

} //RamsesConfigurationImpl
