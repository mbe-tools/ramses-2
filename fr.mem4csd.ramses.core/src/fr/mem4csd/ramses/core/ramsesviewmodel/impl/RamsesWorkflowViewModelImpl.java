/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel.impl;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.InvalidRegistryObjectException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.osate.aadl2.modelsupport.FileNameConstants;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;

import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowExecutor;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelFactory;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage;
import fr.mem4csd.ramses.core.util.RamsesWorkflowKeys;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ramses Workflow View Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowViewModelImpl#getResourceSet <em>Resource Set</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RamsesWorkflowViewModelImpl extends MinimalEObjectImpl.Container implements RamsesWorkflowViewModel {
	
	/**
	 * The default value of the '{@link #getResourceSet() <em>Resource Set</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceSet()
	 * @generated NOT
	 * @ordered
	 */
	protected static final ResourceSet RESOURCE_SET_EDEFAULT = new ResourceSetImpl();
	/**
	 * The cached value of the '{@link #getResourceSet() <em>Resource Set</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceSet()
	 * @generated
	 * @ordered
	 */
	protected ResourceSet resourceSet = RESOURCE_SET_EDEFAULT;
	private static final String WORKFLOW_EXT_POINT = "fr.mem4csd.ramses.core.workflowextensionpoint";
	private static final String WORKFLOW_EXT_NAME_ATTR_ID = "name";
	private static final String WORKFLOW_EXT_WORKFLOW_ATTR_ID = "workflow";	
	
	private static final String CODEGEN_EXT_POINT = "fr.mem4csd.ramses.core.codegenextensionpoint";	
	private static final String CODEGEN_EXT_NAME_ATTR_ID = "name";	
	private static final String CODEGEN_EXT_GENERATOR_ATTR_ID = "generator";
	private static final String CODEGEN_EXT_WORKFLOW_ATTR_ID = "workflow_file";
	private static final String CODEGEN_EXT_INSTANCE_WORKFLOW_ATTR_ID = "instance_workflow_file";	
		
	private static final String PLATFORM_PLUGIN_SCHEME = "platform:/plugin";
	private static final String CODEGEN_EXT_TARGET_ATTR_ID = "target";
	
	// Strangely when using URI.createPlatformPluginURI an error occurs when Xtext replaces the plugin URI prefix with the platform resource one
	private static URI WORKFLOW_URI_BASE = URI.createURI( PLATFORM_PLUGIN_SCHEME );
	
	private Set<String> _registeredPlugins = null;
	private Map<String, Map<String, URI>> _workflowsPerPlugin = null;
	private Map<String, URI> _availableWorkflows = null;
	private Map<String, AadlToTargetBuildGenerator> _availableGenerators = null;
	private Map<String, AadlToTargetBuildGenerator> _availableGeneratorsId = null;
	private Map<String, URI> _generatorToWorkflow = null;
	private Map<String, URI> _generatorToInstanceWorkflow = null;
	private Set<String> _availableGeneratorNames = null;
	
	private final RamsesWorkflowExecutor _executor = RamsesviewmodelFactory.eINSTANCE.createRamsesWorkflowExecutor();

	private final IExtensionRegistry _extensionRegistry;
	
	private final IResourceChangeListener resourceChangeListener;
	
	private class ResourceDeltaVisitor implements IResourceDeltaVisitor {

		@Override
		public boolean visit( final IResourceDelta delta ) {
			if ( delta.getResource().getType() == IResource.FILE /*&& delta.getFlags() != IResourceDelta.MARKERS*/ ) {
				if ( delta.getKind() == IResourceDelta.REMOVED || delta.getKind() == IResourceDelta.CHANGED ) {
					final Resource resource = getResourceSet().getResource( URI.createPlatformResourceURI( delta.getFullPath().toString(), true ), false );
					
					if ( resource != null ) {
						if ( resource.isLoaded() ) {
							resource.unload();
						}

						getResourceSet().getResources().remove( resource );
					}
				}
				
				return false;
			}

			return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RamsesWorkflowViewModelImpl() {
		super();
		
		_extensionRegistry = Platform.getExtensionRegistry();
		
		resourceChangeListener = new IResourceChangeListener() {
			
			@Override
			public void resourceChanged( final IResourceChangeEvent event ) {
				final IResourceDelta delta = event.getDelta();
				
				try {
					final ResourceDeltaVisitor visitor = new ResourceDeltaVisitor();
					delta.accept(visitor);
				}
				catch ( final CoreException exception ) {
					throw new RuntimeException( exception );
				}
			}
		};
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RamsesviewmodelPackage.Literals.RAMSES_WORKFLOW_VIEW_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceSet getResourceSet() {
		return resourceSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceSet(ResourceSet newResourceSet) {
		ResourceSet oldResourceSet = resourceSet;
		resourceSet = newResourceSet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL__RESOURCE_SET, oldResourceSet, resourceSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public Map<String, URI> getAvailableWorkflows() {
		
		if( _availableWorkflows == null ) {
		
			_availableWorkflows = new LinkedHashMap<String, URI>();
			
			IExtension[] extensions = _extensionRegistry.getExtensionPoint(WORKFLOW_EXT_POINT).getExtensions();
			
			for( final IExtension ext : extensions ) {
				IConfigurationElement[] elements = ext.getConfigurationElements();
				for( final IConfigurationElement element : elements ) {
					_availableWorkflows.put(
							element.getAttribute(WORKFLOW_EXT_NAME_ATTR_ID), 
							WORKFLOW_URI_BASE.appendSegment( element.getNamespaceIdentifier() ).appendSegments( element.getAttribute( WORKFLOW_EXT_WORKFLOW_ATTR_ID ).split("/")) );
				}
			}
		}
		
		return _availableWorkflows;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Map<String, AadlToTargetBuildGenerator> getAvailableGenerators() {
		
		if ( _availableGenerators == null ) {

			_availableGenerators = new LinkedHashMap<String, AadlToTargetBuildGenerator>();
			
			IExtension[] extensions = _extensionRegistry.getExtensionPoint( CODEGEN_EXT_POINT ).getExtensions();
			
			for( final IExtension ext : extensions ) {
				IConfigurationElement[] elements = ext.getConfigurationElements();
				for( final IConfigurationElement element : elements ) {
					try {
						_availableGenerators.put( element.getAttribute( CODEGEN_EXT_NAME_ATTR_ID ), (AadlToTargetBuildGenerator)element.createExecutableExtension(CODEGEN_EXT_GENERATOR_ATTR_ID) );
					} catch (InvalidRegistryObjectException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (CoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
		return _availableGenerators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Set<String> getAvailableGeneratorNames() {
		if ( _availableGeneratorNames == null || _availableGeneratorNames.isEmpty()) {
			_availableGeneratorNames = getAvailableGenerators().keySet();
		}
		return _availableGeneratorNames;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public AadlToTargetBuildGenerator getGeneratorFromTargetId(String targetId) {
		return getAvailableGeneratorsFromTargetId().get( targetId );
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public AadlToTargetBuildGenerator getGeneratorFromTargetName(String targetName) {
		return getAvailableGenerators().get( targetName );
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Set<String> getRegisteredPlugins() {
		if( _registeredPlugins == null ) {
			_registeredPlugins = new HashSet<String>();
			
			IExtension[] extensions = _extensionRegistry.getExtensionPoint(WORKFLOW_EXT_POINT).getExtensions();
			
			for( final IExtension ext : extensions ) {
				_registeredPlugins.add( ext.getLabel() );
			}
		}
		return _registeredPlugins;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Map<String, URI> getWorkflows(String plugin) {
		if( _workflowsPerPlugin == null ) {
		
			_workflowsPerPlugin = new LinkedHashMap<String, Map<String, URI>>();
			
			IExtension[] extensions = _extensionRegistry.getExtensionPoint(WORKFLOW_EXT_POINT).getExtensions();
			
			for( final IExtension ext : extensions ) {
				final Map<String, URI> workflowMap = new LinkedHashMap<String, URI>();
				IConfigurationElement[] elements = ext.getConfigurationElements();
				for( final IConfigurationElement element : elements ) {
					workflowMap.put(
							element.getAttribute(WORKFLOW_EXT_NAME_ATTR_ID), 
							WORKFLOW_URI_BASE.appendSegment( element.getNamespaceIdentifier() ).appendSegments( element.getAttribute( WORKFLOW_EXT_WORKFLOW_ATTR_ID ).split("/")) );
				}
				_workflowsPerPlugin.put( ext.getLabel(), workflowMap );
			}
		}
		return _workflowsPerPlugin.get(plugin);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void executeWorkflow(RamsesConfiguration ramsesConfig, Resource sourceFile, String systemImplName, IProgressMonitor monitor) throws WorkflowExecutionException, IOException {
		final URI sourceFileUri = sourceFile.getURI();
		boolean isInstance = FileNameConstants.INSTANCE_FILE_EXT.equals( sourceFileUri.fileExtension() );
		
		final String targetId = ramsesConfig.getTargetId();		
		final URI srcUri;
		final URI workflowURI;
		
		if ( isInstance )	{
			srcUri = computeSourceFileUri(sourceFileUri, systemImplName );
			workflowURI = RamsesWorkflowViewModel.INSTANCE.getTargetInstanceWorkflow(targetId);
		} 
		else {
			srcUri = sourceFileUri; 
			workflowURI = RamsesWorkflowViewModel.INSTANCE.getTargetWorkflow(targetId);
		}
		
		final File source = new File( srcUri.toString() );
		final String sourceDir = source.getParentFile().getAbsolutePath();
		final String filename = source.getName().split("\\.")[0];
				
		String outputDir = URI.createFileURI( ramsesConfig.getOutputDirectory().getAbsolutePath()).toString();
		
		//if ( Platform.isRunning() ) {
		final IResource inpIRes = OsateResourceUtil.toIFile( srcUri );
		final IProject project = inpIRes.getProject();
		final URI uriProject = URI.createFileURI( project.getLocation().toString() );
		final URI uriRel = URI.createFileURI(outputDir).deresolve( uriProject );
		String outputResURIName = uriRel.path();
//		outputResURIName = outputResURIName.substring(outputResURIName.lastIndexOf(project.getLocation().toString())+project.getLocation().toString().length());
//		outputResURIName = uriRel.lastSegment();
		outputResURIName = project.getName()+"/"+ uriRel.lastSegment();
		outputDir = URI.createPlatformResourceURI( outputResURIName, true ).toString();
		//}
		
		boolean exposeRuntimeSharedResources = ramsesConfig.isExposeRuntimeSharedResources();
		
		URI instanceFile;
		
		if (isInstance) {
			instanceFile = sourceFileUri;
		}
		else {
			instanceFile = computeInstanceModelUri(sourceFileUri, systemImplName);
		}
		
		final String runtimePath = ramsesConfig.getRuntimePath() == null ? null : ramsesConfig.getRuntimePath();
		
		final RamsesWorkflowConfiguration workflowConfig = RamsesviewmodelFactory.eINSTANCE.createRamsesWorkflowConfiguration();
		workflowConfig.setName("DEFAULT");
		workflowConfig.setWorkflowURI(workflowURI);
		
		final EMap<String, String> properties = workflowConfig.getProperties();

		properties.put(RamsesWorkflowKeys.SOURCE_FILE_NAME, filename);
		properties.put(RamsesWorkflowKeys.SOURCE_AADL_FILE, srcUri.toString() );
		properties.put(RamsesWorkflowKeys.INSTANCE_MODEL_FILE, instanceFile.toString());
		properties.put(RamsesWorkflowKeys.INCLUDE_DIR, sourceDir);
		properties.put(RamsesWorkflowKeys.OUTPUT_DIR, outputDir);
		properties.put(RamsesWorkflowKeys.SYSTEM_IMPLEMENTATION_NAME, systemImplName);
		properties.put(RamsesWorkflowKeys.EXPOSE_RUNTIME_SHARED_RESOURCES, String.valueOf(exposeRuntimeSharedResources));
		
		if ( runtimePath != null ) {
			properties.put(RamsesWorkflowKeys.TARGET_INSTALL_DIR, runtimePath);
		}
		else {
			properties.put(RamsesWorkflowKeys.TARGET_INSTALL_DIR, "");
		}
		
		ResourcesPlugin.getWorkspace().removeResourceChangeListener( resourceChangeListener );

		try {
			_executor.execute( workflowConfig, getResourceSet(), monitor );
			//_executor.execute( workflowConfig, sourceFile.getResourceSet(), monitor );
		}
		finally {
			IPath resultPath = new Path(outputResURIName);
			IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			final IFolder outFolder = workspaceRoot.getFolder(resultPath);
			
			
			
//			final IFolder outFolder = ResourcesPlugin.getWorkspace().getRoot().getFolder(  new Path( outputResURIName ) );

			try {
				if ( outFolder.exists() ) {
					outFolder.refreshLocal( IResource.DEPTH_INFINITE, monitor );
				}
			} 
			catch ( final CoreException ex ) {
				throw new IOException( ex );
			}
			finally {
				ResourcesPlugin.getWorkspace().addResourceChangeListener( resourceChangeListener, IResourceChangeEvent.POST_CHANGE );
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public URI getTargetWorkflow(String target) {
		if (_generatorToWorkflow == null) {
			_generatorToWorkflow = new LinkedHashMap<String, URI>();
			
			IExtension[] extensions = _extensionRegistry.getExtensionPoint( CODEGEN_EXT_POINT ).getExtensions();
			
			for( final IExtension ext : extensions ) {
				IConfigurationElement[] elements = ext.getConfigurationElements();
				for( final IConfigurationElement element : elements ) {
					try {

						// Strangely when using URI.createPlatformPluginURI an error occurs when Xtext replaces the plugin URI prefix with the platform resource one
						final URI workUri = URI.createURI( PLATFORM_PLUGIN_SCHEME + "/" + element.getContributor().getName() + "/" + element.getAttribute( CODEGEN_EXT_WORKFLOW_ATTR_ID ) );
						_generatorToWorkflow.put( element.getAttribute( CODEGEN_EXT_NAME_ATTR_ID ), workUri );
//						_generatorToWorkflow.put( element.getAttribute( CODEGEN_EXT_NAME_ATTR_ID ), URI.createPlatformPluginURI(element.getContributor().getName()+"/"+element.getAttribute( CODEGEN_EXT_WORKFLOW_ATTR_ID ), true));
					} catch (InvalidRegistryObjectException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return _generatorToWorkflow.get(target);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public URI getTargetInstanceWorkflow(String target) {
		if (_generatorToInstanceWorkflow == null) {
			_generatorToInstanceWorkflow = new LinkedHashMap<String, URI>();
			
			IExtension[] extensions = _extensionRegistry.getExtensionPoint( CODEGEN_EXT_POINT ).getExtensions();
			
			for( final IExtension ext : extensions ) {
				IConfigurationElement[] elements = ext.getConfigurationElements();
				for( final IConfigurationElement element : elements ) {
					try {

						// Strangely when using URI.createPlatformPluginURI an error occurs when Xtext replaces the plugin URI prefix with the platform resource one
						final URI workUri = URI.createURI( PLATFORM_PLUGIN_SCHEME + "/" + element.getContributor().getName() + "/" + element.getAttribute( CODEGEN_EXT_INSTANCE_WORKFLOW_ATTR_ID ) );
						_generatorToInstanceWorkflow.put( element.getAttribute( CODEGEN_EXT_NAME_ATTR_ID ), workUri );
						//_generatorToInstanceWorkflow.put( element.getAttribute( CODEGEN_EXT_NAME_ATTR_ID ), URI.createPlatformPluginURI(element.getContributor().getName()+"/"+element.getAttribute( CODEGEN_EXT_INSTANCE_WORKFLOW_ATTR_ID ), true));
					} catch (InvalidRegistryObjectException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return _generatorToInstanceWorkflow.get(target);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Map<String, AadlToTargetBuildGenerator> getAvailableGeneratorsFromTargetId() {
		
		if ( _availableGeneratorsId == null ) {

			_availableGeneratorsId = new LinkedHashMap<String, AadlToTargetBuildGenerator>();
			
			IExtension[] extensions = _extensionRegistry.getExtensionPoint( CODEGEN_EXT_POINT ).getExtensions();
			
			for( final IExtension ext : extensions ) {
				IConfigurationElement[] elements = ext.getConfigurationElements();
				for( final IConfigurationElement element : elements ) {
					try {
						_availableGeneratorsId.put( element.getAttribute( CODEGEN_EXT_TARGET_ATTR_ID ), (AadlToTargetBuildGenerator)element.createExecutableExtension(CODEGEN_EXT_GENERATOR_ATTR_ID) );
					} catch (InvalidRegistryObjectException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (CoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
		return _availableGeneratorsId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL__RESOURCE_SET:
				return getResourceSet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL__RESOURCE_SET:
				setResourceSet((ResourceSet)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL__RESOURCE_SET:
				setResourceSet(RESOURCE_SET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL__RESOURCE_SET:
				return RESOURCE_SET_EDEFAULT == null ? resourceSet != null : !RESOURCE_SET_EDEFAULT.equals(resourceSet);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_WORKFLOWS:
				return getAvailableWorkflows();
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATORS:
				return getAvailableGenerators();
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATOR_NAMES:
				return getAvailableGeneratorNames();
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL___GET_GENERATOR_FROM_TARGET_ID__STRING:
				return getGeneratorFromTargetId((String)arguments.get(0));
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL___GET_GENERATOR_FROM_TARGET_NAME__STRING:
				return getGeneratorFromTargetName((String)arguments.get(0));
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL___GET_REGISTERED_PLUGINS:
				return getRegisteredPlugins();
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL___GET_WORKFLOWS__STRING:
				return getWorkflows((String)arguments.get(0));
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL___EXECUTE_WORKFLOW__RAMSESCONFIGURATION_RESOURCE_STRING_IPROGRESSMONITOR:
				try {
					executeWorkflow((RamsesConfiguration)arguments.get(0), (Resource)arguments.get(1), (String)arguments.get(2), (IProgressMonitor)arguments.get(3));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL___GET_TARGET_WORKFLOW__STRING:
				return getTargetWorkflow((String)arguments.get(0));
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL___GET_TARGET_INSTANCE_WORKFLOW__STRING:
				return getTargetInstanceWorkflow((String)arguments.get(0));
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATORS_FROM_TARGET_ID:
				return getAvailableGeneratorsFromTargetId();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (resourceSet: ");
		result.append(resourceSet);
		result.append(')');
		return result.toString();
	}

	protected URI computeInstanceModelUri( 	final URI aadlFileUri,
											final String systImplName ) {
		final URI dir = aadlFileUri.trimFileExtension().trimSegments( 1 );
		final URI instanceURI = dir.appendSegment( FileNameConstants.AADL_INSTANCES_DIR).appendSegment(aadlFileUri.trimFileExtension().lastSegment() + "_"
				+ systImplName.substring( 0, systImplName.indexOf( '.' ) ) + "_" + systImplName.substring(systImplName.indexOf( '.' ) + 1 ) + FileNameConstants.INSTANCE_MODEL_POSTFIX);
		
		return instanceURI.appendFileExtension( FileNameConstants.INSTANCE_FILE_EXT);
	}

	protected URI computeSourceFileUri( 	final URI instanceUri,
											final String systImplName ) {
		final URI dir = instanceUri.trimFileExtension().trimSegments( 2 );
		final String filename = instanceUri.trimFileExtension().lastSegment();
		return dir.appendSegment(filename.substring(0, filename.lastIndexOf(systImplName)-1)).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT );
	}

} //RamsesWorkflowViewModelImpl
