/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel.impl;

import fr.mem4csd.ramses.core.ramsesviewmodel.*;

import java.io.File;

import java.util.List;
import java.util.Set;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RamsesviewmodelFactoryImpl extends EFactoryImpl implements RamsesviewmodelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RamsesviewmodelFactory init() {
		try {
			RamsesviewmodelFactory theRamsesviewmodelFactory = (RamsesviewmodelFactory)EPackage.Registry.INSTANCE.getEFactory(RamsesviewmodelPackage.eNS_URI);
			if (theRamsesviewmodelFactory != null) {
				return theRamsesviewmodelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RamsesviewmodelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RamsesviewmodelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_CONFIGURATION: return createRamsesWorkflowConfiguration();
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_VIEW_MODEL: return createRamsesWorkflowViewModel();
			case RamsesviewmodelPackage.RAMSES_WORKFLOW_EXECUTOR: return createRamsesWorkflowExecutor();
			case RamsesviewmodelPackage.RAMSES_CONFIGURATION: return createRamsesConfiguration();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case RamsesviewmodelPackage.EFILE:
				return createEFileFromString(eDataType, initialValue);
			case RamsesviewmodelPackage.ECONFIG_STATUS:
				return createEConfigStatusFromString(eDataType, initialValue);
			case RamsesviewmodelPackage.ELIST:
				return createEListFromString(eDataType, initialValue);
			case RamsesviewmodelPackage.ESET:
				return createESetFromString(eDataType, initialValue);
			case RamsesviewmodelPackage.CONFIGURATION_EXCEPTION:
				return createConfigurationExceptionFromString(eDataType, initialValue);
			case RamsesviewmodelPackage.IPROJECT:
				return createIProjectFromString(eDataType, initialValue);
			case RamsesviewmodelPackage.CORE_EXCEPTION:
				return createCoreExceptionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case RamsesviewmodelPackage.EFILE:
				return convertEFileToString(eDataType, instanceValue);
			case RamsesviewmodelPackage.ECONFIG_STATUS:
				return convertEConfigStatusToString(eDataType, instanceValue);
			case RamsesviewmodelPackage.ELIST:
				return convertEListToString(eDataType, instanceValue);
			case RamsesviewmodelPackage.ESET:
				return convertESetToString(eDataType, instanceValue);
			case RamsesviewmodelPackage.CONFIGURATION_EXCEPTION:
				return convertConfigurationExceptionToString(eDataType, instanceValue);
			case RamsesviewmodelPackage.IPROJECT:
				return convertIProjectToString(eDataType, instanceValue);
			case RamsesviewmodelPackage.CORE_EXCEPTION:
				return convertCoreExceptionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RamsesWorkflowConfiguration createRamsesWorkflowConfiguration() {
		RamsesWorkflowConfigurationImpl ramsesWorkflowConfiguration = new RamsesWorkflowConfigurationImpl();
		return ramsesWorkflowConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RamsesWorkflowViewModel createRamsesWorkflowViewModel() {
		RamsesWorkflowViewModelImpl ramsesWorkflowViewModel = new RamsesWorkflowViewModelImpl();
		return ramsesWorkflowViewModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RamsesWorkflowExecutor createRamsesWorkflowExecutor() {
		RamsesWorkflowExecutorImpl ramsesWorkflowExecutor = new RamsesWorkflowExecutorImpl();
		return ramsesWorkflowExecutor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RamsesConfiguration createRamsesConfiguration() {
		RamsesConfigurationImpl ramsesConfiguration = new RamsesConfigurationImpl();
		return ramsesConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public File createEFileFromString(EDataType eDataType, String initialValue) {
		return (File)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEFileToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigStatus createEConfigStatusFromString(EDataType eDataType, String initialValue) {
		return (ConfigStatus)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEConfigStatusToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<?> createEListFromString(EDataType eDataType, String initialValue) {
		return (List<?>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Set<?> createESetFromString(EDataType eDataType, String initialValue) {
		return (Set<?>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertESetToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationException createConfigurationExceptionFromString(EDataType eDataType, String initialValue) {
		return (ConfigurationException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConfigurationExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IProject createIProjectFromString(EDataType eDataType, String initialValue) {
		return (IProject)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIProjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoreException createCoreExceptionFromString(EDataType eDataType, String initialValue) {
		return (CoreException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCoreExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RamsesviewmodelPackage getRamsesviewmodelPackage() {
		return (RamsesviewmodelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RamsesviewmodelPackage getPackage() {
		return RamsesviewmodelPackage.eINSTANCE;
	}

} //RamsesviewmodelFactoryImpl
