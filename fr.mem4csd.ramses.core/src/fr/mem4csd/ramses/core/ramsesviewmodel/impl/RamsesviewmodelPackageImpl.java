/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel.impl;

import de.mdelab.workflow.WorkflowPackage;

import de.mdelab.workflow.components.ComponentsPackage;
import de.mdelab.workflow.helpers.HelpersPackage;
import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;
import fr.mem4csd.ramses.core.ramsesviewmodel.ConfigStatus;
import fr.mem4csd.ramses.core.ramsesviewmodel.ConfigurationException;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowExecutor;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelFactory;
import fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage;

import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;
import java.io.File;

import java.util.List;
import java.util.Set;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RamsesviewmodelPackageImpl extends EPackageImpl implements RamsesviewmodelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ramsesWorkflowConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ramsesWorkflowViewModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ramsesWorkflowExecutorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ramsesConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eFileEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eConfigStatusEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eSetEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType configurationExceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iProjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType coreExceptionEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RamsesviewmodelPackageImpl() {
		super(eNS_URI, RamsesviewmodelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link RamsesviewmodelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RamsesviewmodelPackage init() {
		if (isInited) return (RamsesviewmodelPackage)EPackage.Registry.INSTANCE.getEPackage(RamsesviewmodelPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredRamsesviewmodelPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		RamsesviewmodelPackageImpl theRamsesviewmodelPackage = registeredRamsesviewmodelPackage instanceof RamsesviewmodelPackageImpl ? (RamsesviewmodelPackageImpl)registeredRamsesviewmodelPackage : new RamsesviewmodelPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Aadl2Package.eINSTANCE.eClass();
		Arch_tracePackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();
		HelpersPackage.eINSTANCE.eClass();
		ComponentsPackage.eINSTANCE.eClass();
		WorkflowramsesPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRamsesviewmodelPackage.createPackageContents();

		// Initialize created meta-data
		theRamsesviewmodelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRamsesviewmodelPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RamsesviewmodelPackage.eNS_URI, theRamsesviewmodelPackage);
		return theRamsesviewmodelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRamsesWorkflowConfiguration() {
		return ramsesWorkflowConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRamsesWorkflowConfiguration_Name() {
		return (EAttribute)ramsesWorkflowConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRamsesWorkflowConfiguration_WorkflowURI() {
		return (EAttribute)ramsesWorkflowConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRamsesWorkflowConfiguration_Properties() {
		return (EReference)ramsesWorkflowConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowConfiguration__Store__URI() {
		return ramsesWorkflowConfigurationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowConfiguration__Load__URI() {
		return ramsesWorkflowConfigurationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowConfiguration__Create__String_Workflow() {
		return ramsesWorkflowConfigurationEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRamsesWorkflowViewModel() {
		return ramsesWorkflowViewModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRamsesWorkflowViewModel_ResourceSet() {
		return (EAttribute)ramsesWorkflowViewModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowViewModel__GetAvailableWorkflows() {
		return ramsesWorkflowViewModelEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowViewModel__GetAvailableGenerators() {
		return ramsesWorkflowViewModelEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowViewModel__GetAvailableGeneratorNames() {
		return ramsesWorkflowViewModelEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowViewModel__GetGeneratorFromTargetId__String() {
		return ramsesWorkflowViewModelEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowViewModel__GetGeneratorFromTargetName__String() {
		return ramsesWorkflowViewModelEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowViewModel__GetRegisteredPlugins() {
		return ramsesWorkflowViewModelEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowViewModel__GetWorkflows__String() {
		return ramsesWorkflowViewModelEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowViewModel__ExecuteWorkflow__RamsesConfiguration_Resource_String_IProgressMonitor() {
		return ramsesWorkflowViewModelEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowViewModel__GetTargetWorkflow__String() {
		return ramsesWorkflowViewModelEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowViewModel__GetTargetInstanceWorkflow__String() {
		return ramsesWorkflowViewModelEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowViewModel__GetAvailableGeneratorsFromTargetId() {
		return ramsesWorkflowViewModelEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRamsesWorkflowExecutor() {
		return ramsesWorkflowExecutorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesWorkflowExecutor__Execute__RamsesWorkflowConfiguration_ResourceSet_IProgressMonitor() {
		return ramsesWorkflowExecutorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRamsesConfiguration() {
		return ramsesConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRamsesConfiguration_OutputDirectory() {
		return (EAttribute)ramsesConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRamsesConfiguration_RuntimeDirectory() {
		return (EAttribute)ramsesConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRamsesConfiguration_TargetId() {
		return (EAttribute)ramsesConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRamsesConfiguration_ExposeRuntimeSharedResources() {
		return (EAttribute)ramsesConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRamsesConfiguration_PropertyMap() {
		return (EAttribute)ramsesConfigurationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesConfiguration__SetRamsesOutputDir__String() {
		return ramsesConfigurationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesConfiguration__SetRuntimePath__String() {
		return ramsesConfigurationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesConfiguration__SetGenerationTargetId__String() {
		return ramsesConfigurationEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesConfiguration__GetRamsesOutputDir() {
		return ramsesConfigurationEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesConfiguration__GetRuntimePath() {
		return ramsesConfigurationEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesConfiguration__Log() {
		return ramsesConfigurationEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesConfiguration__FetchProperties__IProject() {
		return ramsesConfigurationEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRamsesConfiguration__SaveConfig__IProject_Map() {
		return ramsesConfigurationEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEFile() {
		return eFileEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEConfigStatus() {
		return eConfigStatusEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEList() {
		return eListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getESet() {
		return eSetEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getConfigurationException() {
		return configurationExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIProject() {
		return iProjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getCoreException() {
		return coreExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RamsesviewmodelFactory getRamsesviewmodelFactory() {
		return (RamsesviewmodelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		ramsesWorkflowConfigurationEClass = createEClass(RAMSES_WORKFLOW_CONFIGURATION);
		createEAttribute(ramsesWorkflowConfigurationEClass, RAMSES_WORKFLOW_CONFIGURATION__NAME);
		createEAttribute(ramsesWorkflowConfigurationEClass, RAMSES_WORKFLOW_CONFIGURATION__WORKFLOW_URI);
		createEReference(ramsesWorkflowConfigurationEClass, RAMSES_WORKFLOW_CONFIGURATION__PROPERTIES);
		createEOperation(ramsesWorkflowConfigurationEClass, RAMSES_WORKFLOW_CONFIGURATION___STORE__URI);
		createEOperation(ramsesWorkflowConfigurationEClass, RAMSES_WORKFLOW_CONFIGURATION___LOAD__URI);
		createEOperation(ramsesWorkflowConfigurationEClass, RAMSES_WORKFLOW_CONFIGURATION___CREATE__STRING_WORKFLOW);

		ramsesWorkflowViewModelEClass = createEClass(RAMSES_WORKFLOW_VIEW_MODEL);
		createEAttribute(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL__RESOURCE_SET);
		createEOperation(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_WORKFLOWS);
		createEOperation(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATORS);
		createEOperation(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATOR_NAMES);
		createEOperation(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL___GET_GENERATOR_FROM_TARGET_ID__STRING);
		createEOperation(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL___GET_GENERATOR_FROM_TARGET_NAME__STRING);
		createEOperation(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL___GET_REGISTERED_PLUGINS);
		createEOperation(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL___GET_WORKFLOWS__STRING);
		createEOperation(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL___EXECUTE_WORKFLOW__RAMSESCONFIGURATION_RESOURCE_STRING_IPROGRESSMONITOR);
		createEOperation(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL___GET_TARGET_WORKFLOW__STRING);
		createEOperation(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL___GET_TARGET_INSTANCE_WORKFLOW__STRING);
		createEOperation(ramsesWorkflowViewModelEClass, RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATORS_FROM_TARGET_ID);

		ramsesWorkflowExecutorEClass = createEClass(RAMSES_WORKFLOW_EXECUTOR);
		createEOperation(ramsesWorkflowExecutorEClass, RAMSES_WORKFLOW_EXECUTOR___EXECUTE__RAMSESWORKFLOWCONFIGURATION_RESOURCESET_IPROGRESSMONITOR);

		ramsesConfigurationEClass = createEClass(RAMSES_CONFIGURATION);
		createEAttribute(ramsesConfigurationEClass, RAMSES_CONFIGURATION__OUTPUT_DIRECTORY);
		createEAttribute(ramsesConfigurationEClass, RAMSES_CONFIGURATION__RUNTIME_DIRECTORY);
		createEAttribute(ramsesConfigurationEClass, RAMSES_CONFIGURATION__TARGET_ID);
		createEAttribute(ramsesConfigurationEClass, RAMSES_CONFIGURATION__EXPOSE_RUNTIME_SHARED_RESOURCES);
		createEAttribute(ramsesConfigurationEClass, RAMSES_CONFIGURATION__PROPERTY_MAP);
		createEOperation(ramsesConfigurationEClass, RAMSES_CONFIGURATION___SET_RAMSES_OUTPUT_DIR__STRING);
		createEOperation(ramsesConfigurationEClass, RAMSES_CONFIGURATION___SET_RUNTIME_PATH__STRING);
		createEOperation(ramsesConfigurationEClass, RAMSES_CONFIGURATION___SET_GENERATION_TARGET_ID__STRING);
		createEOperation(ramsesConfigurationEClass, RAMSES_CONFIGURATION___GET_RAMSES_OUTPUT_DIR);
		createEOperation(ramsesConfigurationEClass, RAMSES_CONFIGURATION___GET_RUNTIME_PATH);
		createEOperation(ramsesConfigurationEClass, RAMSES_CONFIGURATION___LOG);
		createEOperation(ramsesConfigurationEClass, RAMSES_CONFIGURATION___FETCH_PROPERTIES__IPROJECT);
		createEOperation(ramsesConfigurationEClass, RAMSES_CONFIGURATION___SAVE_CONFIG__IPROJECT_MAP);

		// Create data types
		eFileEDataType = createEDataType(EFILE);
		eConfigStatusEDataType = createEDataType(ECONFIG_STATUS);
		eListEDataType = createEDataType(ELIST);
		eSetEDataType = createEDataType(ESET);
		configurationExceptionEDataType = createEDataType(CONFIGURATION_EXCEPTION);
		iProjectEDataType = createEDataType(IPROJECT);
		coreExceptionEDataType = createEDataType(CORE_EXCEPTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		HelpersPackage theHelpersPackage = (HelpersPackage)EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI);
		WorkflowPackage theWorkflowPackage = (WorkflowPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowPackage.eNS_URI);
		WorkflowramsesPackage theWorkflowramsesPackage = (WorkflowramsesPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowramsesPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters
		addETypeParameter(eListEDataType, "T");
		addETypeParameter(eSetEDataType, "T");

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(ramsesWorkflowConfigurationEClass, RamsesWorkflowConfiguration.class, "RamsesWorkflowConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRamsesWorkflowConfiguration_Name(), ecorePackage.getEString(), "name", null, 0, 1, RamsesWorkflowConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRamsesWorkflowConfiguration_WorkflowURI(), theHelpersPackage.getURI(), "workflowURI", null, 0, 1, RamsesWorkflowConfiguration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(theHelpersPackage.getMapEntry());
		EGenericType g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEReference(getRamsesWorkflowConfiguration_Properties(), g1, null, "properties", null, 0, -1, RamsesWorkflowConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getRamsesWorkflowConfiguration__Store__URI(), null, "store", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getURI(), "uri", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getIOException());

		op = initEOperation(getRamsesWorkflowConfiguration__Load__URI(), null, "load", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getURI(), "uri", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getIOException());

		op = initEOperation(getRamsesWorkflowConfiguration__Create__String_Workflow(), null, "create", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theWorkflowPackage.getWorkflow(), "workflow", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ramsesWorkflowViewModelEClass, RamsesWorkflowViewModel.class, "RamsesWorkflowViewModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRamsesWorkflowViewModel_ResourceSet(), ecorePackage.getEResourceSet(), "resourceSet", null, 0, 1, RamsesWorkflowViewModel.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getRamsesWorkflowViewModel__GetAvailableWorkflows(), null, "getAvailableWorkflows", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theHelpersPackage.getURI());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getRamsesWorkflowViewModel__GetAvailableGenerators(), null, "getAvailableGenerators", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theWorkflowramsesPackage.getAadlToTargetBuildGenerator());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getRamsesWorkflowViewModel__GetAvailableGeneratorNames(), null, "getAvailableGeneratorNames", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getESet());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getRamsesWorkflowViewModel__GetGeneratorFromTargetId__String(), theWorkflowramsesPackage.getAadlToTargetBuildGenerator(), "getGeneratorFromTargetId", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "targetId", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRamsesWorkflowViewModel__GetGeneratorFromTargetName__String(), theWorkflowramsesPackage.getAadlToTargetBuildGenerator(), "getGeneratorFromTargetName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "targetId", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRamsesWorkflowViewModel__GetRegisteredPlugins(), null, "getRegisteredPlugins", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getESet());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getRamsesWorkflowViewModel__GetWorkflows__String(), null, "getWorkflows", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "plugin", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theHelpersPackage.getURI());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getRamsesWorkflowViewModel__ExecuteWorkflow__RamsesConfiguration_Resource_String_IProgressMonitor(), null, "executeWorkflow", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRamsesConfiguration(), "ramsesConfig", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResource(), "sourceFile", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "systemImplName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getWorkflowExecutionException());
		addEException(op, theHelpersPackage.getIOException());

		op = initEOperation(getRamsesWorkflowViewModel__GetTargetWorkflow__String(), theHelpersPackage.getURI(), "getTargetWorkflow", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "target", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRamsesWorkflowViewModel__GetTargetInstanceWorkflow__String(), theHelpersPackage.getURI(), "getTargetInstanceWorkflow", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "target", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRamsesWorkflowViewModel__GetAvailableGeneratorsFromTargetId(), null, "getAvailableGeneratorsFromTargetId", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theWorkflowramsesPackage.getAadlToTargetBuildGenerator());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		initEClass(ramsesWorkflowExecutorEClass, RamsesWorkflowExecutor.class, "RamsesWorkflowExecutor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getRamsesWorkflowExecutor__Execute__RamsesWorkflowConfiguration_ResourceSet_IProgressMonitor(), theWorkflowPackage.getWorkflowExecutionContext(), "execute", 0, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getRamsesWorkflowConfiguration(), "config", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEResourceSet(), "resourceSet", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getWorkflowExecutionException());
		addEException(op, theHelpersPackage.getIOException());

		initEClass(ramsesConfigurationEClass, RamsesConfiguration.class, "RamsesConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRamsesConfiguration_OutputDirectory(), this.getEFile(), "outputDirectory", null, 0, 1, RamsesConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRamsesConfiguration_RuntimeDirectory(), this.getEFile(), "runtimeDirectory", null, 0, 1, RamsesConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRamsesConfiguration_TargetId(), ecorePackage.getEString(), "targetId", null, 0, 1, RamsesConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRamsesConfiguration_ExposeRuntimeSharedResources(), ecorePackage.getEBoolean(), "exposeRuntimeSharedResources", null, 0, 1, RamsesConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEAttribute(getRamsesConfiguration_PropertyMap(), g1, "propertyMap", null, 0, 1, RamsesConfiguration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getRamsesConfiguration__SetRamsesOutputDir__String(), this.getEConfigStatus(), "setRamsesOutputDir", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "path", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRamsesConfiguration__SetRuntimePath__String(), null, "setRuntimePath", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "path", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getConfigurationException());

		op = initEOperation(getRamsesConfiguration__SetGenerationTargetId__String(), this.getEConfigStatus(), "setGenerationTargetId", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "path", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getRamsesConfiguration__GetRamsesOutputDir(), this.getEFile(), "getRamsesOutputDir", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getRamsesConfiguration__GetRuntimePath(), theEcorePackage.getEString(), "getRuntimePath", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getRamsesConfiguration__Log(), null, "log", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRamsesConfiguration__FetchProperties__IProject(), null, "fetchProperties", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIProject(), "project", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getCoreException());
		addEException(op, this.getConfigurationException());

		op = initEOperation(getRamsesConfiguration__SaveConfig__IProject_Map(), null, "saveConfig", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIProject(), "project", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "properties", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getCoreException());

		// Initialize data types
		initEDataType(eFileEDataType, File.class, "EFile", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eConfigStatusEDataType, ConfigStatus.class, "EConfigStatus", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eListEDataType, List.class, "EList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eSetEDataType, Set.class, "ESet", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(configurationExceptionEDataType, ConfigurationException.class, "ConfigurationException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iProjectEDataType, IProject.class, "IProject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(coreExceptionEDataType, CoreException.class, "CoreException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //RamsesviewmodelPackageImpl
