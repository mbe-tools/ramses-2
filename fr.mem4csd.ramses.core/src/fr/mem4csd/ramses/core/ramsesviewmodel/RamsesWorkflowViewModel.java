/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel;

import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ramses Workflow View Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getResourceSet <em>Resource Set</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesWorkflowViewModel()
 * @model
 * @generated
 */
public interface RamsesWorkflowViewModel extends EObject {

	/**
	 * Returns the value of the '<em><b>Resource Set</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Set</em>' attribute.
	 * @see #setResourceSet(ResourceSet)
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesWorkflowViewModel_ResourceSet()
	 * @model transient="true"
	 * @generated
	 */
	ResourceSet getResourceSet();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getResourceSet <em>Resource Set</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Set</em>' attribute.
	 * @see #getResourceSet()
	 * @generated
	 */
	void setResourceSet(ResourceSet value);

	RamsesWorkflowViewModel INSTANCE = RamsesviewmodelFactory.eINSTANCE.createRamsesWorkflowViewModel();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	Map<String, URI> getAvailableWorkflows();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	Map<String, AadlToTargetBuildGenerator> getAvailableGenerators();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="fr.mem4csd.ramses.core.ramsesviewmodel.ESet&lt;org.eclipse.emf.ecore.EString&gt;"
	 * @generated
	 */
	Set<String> getAvailableGeneratorNames();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	AadlToTargetBuildGenerator getGeneratorFromTargetId(String targetId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	AadlToTargetBuildGenerator getGeneratorFromTargetName(String targetId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="fr.mem4csd.ramses.core.ramsesviewmodel.ESet&lt;org.eclipse.emf.ecore.EString&gt;"
	 * @generated
	 */
	Set<String> getRegisteredPlugins();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Map<String, URI> getWorkflows(String plugin);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.mdelab.workflow.helpers.WorkflowExecutionException de.mdelab.workflow.helpers.IOException" monitorDataType="de.mdelab.workflow.helpers.IProgressMonitor"
	 * @generated
	 */
	void executeWorkflow(RamsesConfiguration ramsesConfig, Resource sourceFile, String systemImplName, IProgressMonitor monitor) throws WorkflowExecutionException, IOException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="de.mdelab.workflow.helpers.URI"
	 * @generated
	 */
	URI getTargetWorkflow(String target);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="de.mdelab.workflow.helpers.URI"
	 * @generated
	 */
	URI getTargetInstanceWorkflow(String target);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	Map<String, AadlToTargetBuildGenerator> getAvailableGeneratorsFromTargetId();
} // RamsesWorkflowViewModel
