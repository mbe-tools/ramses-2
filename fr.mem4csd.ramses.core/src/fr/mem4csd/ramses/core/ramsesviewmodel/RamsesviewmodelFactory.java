/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage
 * @generated
 */
public interface RamsesviewmodelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RamsesviewmodelFactory eINSTANCE = fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Ramses Workflow Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ramses Workflow Configuration</em>'.
	 * @generated
	 */
	RamsesWorkflowConfiguration createRamsesWorkflowConfiguration();

	/**
	 * Returns a new object of class '<em>Ramses Workflow View Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ramses Workflow View Model</em>'.
	 * @generated
	 */
	RamsesWorkflowViewModel createRamsesWorkflowViewModel();

	/**
	 * Returns a new object of class '<em>Ramses Workflow Executor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ramses Workflow Executor</em>'.
	 * @generated
	 */
	RamsesWorkflowExecutor createRamsesWorkflowExecutor();

	/**
	 * Returns a new object of class '<em>Ramses Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ramses Configuration</em>'.
	 * @generated
	 */
	RamsesConfiguration createRamsesConfiguration();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	RamsesviewmodelPackage getRamsesviewmodelPackage();

} //RamsesviewmodelFactory
