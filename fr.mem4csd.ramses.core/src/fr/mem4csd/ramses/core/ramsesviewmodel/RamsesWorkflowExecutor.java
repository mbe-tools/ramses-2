/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;

import java.io.IOException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ramses Workflow Executor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesWorkflowExecutor()
 * @model
 * @generated
 */
public interface RamsesWorkflowExecutor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" exceptions="de.mdelab.workflow.helpers.WorkflowExecutionException de.mdelab.workflow.helpers.IOException" monitorDataType="de.mdelab.workflow.helpers.IProgressMonitor"
	 * @generated
	 */
	WorkflowExecutionContext execute(RamsesWorkflowConfiguration config, ResourceSet resourceSet, IProgressMonitor monitor) throws WorkflowExecutionException, IOException;

} // RamsesWorkflowExecutor
