/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel;

import de.mdelab.workflow.Workflow;

import java.io.IOException;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ramses Workflow Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#getName <em>Name</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#getWorkflowURI <em>Workflow URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#getProperties <em>Properties</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesWorkflowConfiguration()
 * @model
 * @generated
 */
public interface RamsesWorkflowConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesWorkflowConfiguration_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Workflow URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Workflow URI</em>' attribute.
	 * @see #setWorkflowURI(URI)
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesWorkflowConfiguration_WorkflowURI()
	 * @model dataType="de.mdelab.workflow.helpers.URI" transient="true"
	 * @generated
	 */
	URI getWorkflowURI();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#getWorkflowURI <em>Workflow URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Workflow URI</em>' attribute.
	 * @see #getWorkflowURI()
	 * @generated
	 */
	void setWorkflowURI(URI value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' map.
	 * The key is of type {@link KeyType},
	 * and the value is of type {@link ValueType},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' map.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage#getRamsesWorkflowConfiguration_Properties()
	 * @model mapType="de.mdelab.workflow.helpers.MapEntry&lt;KeyType, ValueType&gt;"
	 * @generated
	 */
	EMap<String, String> getProperties();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.mdelab.workflow.helpers.IOException" uriDataType="de.mdelab.workflow.helpers.URI"
	 * @generated
	 */
	void store(URI uri) throws IOException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.mdelab.workflow.helpers.IOException" uriDataType="de.mdelab.workflow.helpers.URI"
	 * @generated
	 */
	void load(URI uri) throws IOException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void create(String name, Workflow workflow);

} // RamsesWorkflowConfiguration
