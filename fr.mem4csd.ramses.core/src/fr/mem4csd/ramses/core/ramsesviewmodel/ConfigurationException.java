/**
 * RAMSES 2.0
 * 
 * Copyright © 2014-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel;

public class ConfigurationException extends Exception
{
  /**
   * 
   */
  private static final long serialVersionUID = 1L ;
  public ConfigStatus status ;
  
  public ConfigurationException (ConfigStatus status)
  {
    this.status = status ; 
  }
  
  @Override
  public String getMessage()
  {
    String msg = "configuration has failed" ;
    
    if(false == (status.msg == null || status.msg.isEmpty()))
    {
      msg += ": " + status.msg ;
    }
    else
    {
      msg += status.cardinal ;
    }
    
    return msg ; 
  }
}