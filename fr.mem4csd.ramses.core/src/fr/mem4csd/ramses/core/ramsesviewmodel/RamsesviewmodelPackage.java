/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelFactory
 * @model kind="package"
 * @generated
 */
public interface RamsesviewmodelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ramsesviewmodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/ramses/ramsesviewmodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ramsesviewmodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RamsesviewmodelPackage eINSTANCE = fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowConfigurationImpl <em>Ramses Workflow Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowConfigurationImpl
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getRamsesWorkflowConfiguration()
	 * @generated
	 */
	int RAMSES_WORKFLOW_CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_CONFIGURATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Workflow URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_CONFIGURATION__WORKFLOW_URI = 1;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_CONFIGURATION__PROPERTIES = 2;

	/**
	 * The number of structural features of the '<em>Ramses Workflow Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_CONFIGURATION_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Store</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_CONFIGURATION___STORE__URI = 0;

	/**
	 * The operation id for the '<em>Load</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_CONFIGURATION___LOAD__URI = 1;

	/**
	 * The operation id for the '<em>Create</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_CONFIGURATION___CREATE__STRING_WORKFLOW = 2;

	/**
	 * The number of operations of the '<em>Ramses Workflow Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_CONFIGURATION_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowViewModelImpl <em>Ramses Workflow View Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowViewModelImpl
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getRamsesWorkflowViewModel()
	 * @generated
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Resource Set</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL__RESOURCE_SET = 0;

	/**
	 * The number of structural features of the '<em>Ramses Workflow View Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Get Available Workflows</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_WORKFLOWS = 0;

	/**
	 * The operation id for the '<em>Get Available Generators</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATORS = 1;

	/**
	 * The operation id for the '<em>Get Available Generator Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATOR_NAMES = 2;

	/**
	 * The operation id for the '<em>Get Generator From Target Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL___GET_GENERATOR_FROM_TARGET_ID__STRING = 3;

	/**
	 * The operation id for the '<em>Get Generator From Target Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL___GET_GENERATOR_FROM_TARGET_NAME__STRING = 4;

	/**
	 * The operation id for the '<em>Get Registered Plugins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL___GET_REGISTERED_PLUGINS = 5;

	/**
	 * The operation id for the '<em>Get Workflows</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL___GET_WORKFLOWS__STRING = 6;

	/**
	 * The operation id for the '<em>Execute Workflow</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL___EXECUTE_WORKFLOW__RAMSESCONFIGURATION_RESOURCE_STRING_IPROGRESSMONITOR = 7;

	/**
	 * The operation id for the '<em>Get Target Workflow</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL___GET_TARGET_WORKFLOW__STRING = 8;

	/**
	 * The operation id for the '<em>Get Target Instance Workflow</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL___GET_TARGET_INSTANCE_WORKFLOW__STRING = 9;

	/**
	 * The operation id for the '<em>Get Available Generators From Target Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATORS_FROM_TARGET_ID = 10;

	/**
	 * The number of operations of the '<em>Ramses Workflow View Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_VIEW_MODEL_OPERATION_COUNT = 11;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowExecutorImpl <em>Ramses Workflow Executor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowExecutorImpl
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getRamsesWorkflowExecutor()
	 * @generated
	 */
	int RAMSES_WORKFLOW_EXECUTOR = 2;

	/**
	 * The number of structural features of the '<em>Ramses Workflow Executor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_EXECUTOR_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_EXECUTOR___EXECUTE__RAMSESWORKFLOWCONFIGURATION_RESOURCESET_IPROGRESSMONITOR = 0;

	/**
	 * The number of operations of the '<em>Ramses Workflow Executor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_WORKFLOW_EXECUTOR_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesConfigurationImpl <em>Ramses Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesConfigurationImpl
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getRamsesConfiguration()
	 * @generated
	 */
	int RAMSES_CONFIGURATION = 3;

	/**
	 * The feature id for the '<em><b>Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION__OUTPUT_DIRECTORY = 0;

	/**
	 * The feature id for the '<em><b>Runtime Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION__RUNTIME_DIRECTORY = 1;

	/**
	 * The feature id for the '<em><b>Target Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION__TARGET_ID = 2;

	/**
	 * The feature id for the '<em><b>Expose Runtime Shared Resources</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION__EXPOSE_RUNTIME_SHARED_RESOURCES = 3;

	/**
	 * The feature id for the '<em><b>Property Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION__PROPERTY_MAP = 4;

	/**
	 * The number of structural features of the '<em>Ramses Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Set Ramses Output Dir</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION___SET_RAMSES_OUTPUT_DIR__STRING = 0;

	/**
	 * The operation id for the '<em>Set Runtime Path</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION___SET_RUNTIME_PATH__STRING = 1;

	/**
	 * The operation id for the '<em>Set Generation Target Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION___SET_GENERATION_TARGET_ID__STRING = 2;

	/**
	 * The operation id for the '<em>Get Ramses Output Dir</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION___GET_RAMSES_OUTPUT_DIR = 3;

	/**
	 * The operation id for the '<em>Get Runtime Path</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION___GET_RUNTIME_PATH = 4;

	/**
	 * The operation id for the '<em>Log</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION___LOG = 5;

	/**
	 * The operation id for the '<em>Fetch Properties</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION___FETCH_PROPERTIES__IPROJECT = 6;

	/**
	 * The operation id for the '<em>Save Config</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION___SAVE_CONFIG__IPROJECT_MAP = 7;

	/**
	 * The number of operations of the '<em>Ramses Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMSES_CONFIGURATION_OPERATION_COUNT = 8;

	/**
	 * The meta object id for the '<em>EFile</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.io.File
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getEFile()
	 * @generated
	 */
	int EFILE = 4;

	/**
	 * The meta object id for the '<em>EConfig Status</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.ConfigStatus
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getEConfigStatus()
	 * @generated
	 */
	int ECONFIG_STATUS = 5;


	/**
	 * The meta object id for the '<em>EList</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getEList()
	 * @generated
	 */
	int ELIST = 6;


	/**
	 * The meta object id for the '<em>ESet</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Set
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getESet()
	 * @generated
	 */
	int ESET = 7;


	/**
	 * The meta object id for the '<em>Configuration Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.ConfigurationException
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getConfigurationException()
	 * @generated
	 */
	int CONFIGURATION_EXCEPTION = 8;


	/**
	 * The meta object id for the '<em>IProject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.core.resources.IProject
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getIProject()
	 * @generated
	 */
	int IPROJECT = 9;


	/**
	 * The meta object id for the '<em>Core Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.core.runtime.CoreException
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getCoreException()
	 * @generated
	 */
	int CORE_EXCEPTION = 10;


	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration <em>Ramses Workflow Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ramses Workflow Configuration</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration
	 * @generated
	 */
	EClass getRamsesWorkflowConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#getName()
	 * @see #getRamsesWorkflowConfiguration()
	 * @generated
	 */
	EAttribute getRamsesWorkflowConfiguration_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#getWorkflowURI <em>Workflow URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Workflow URI</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#getWorkflowURI()
	 * @see #getRamsesWorkflowConfiguration()
	 * @generated
	 */
	EAttribute getRamsesWorkflowConfiguration_WorkflowURI();

	/**
	 * Returns the meta object for the map '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Properties</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#getProperties()
	 * @see #getRamsesWorkflowConfiguration()
	 * @generated
	 */
	EReference getRamsesWorkflowConfiguration_Properties();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#store(org.eclipse.emf.common.util.URI) <em>Store</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Store</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#store(org.eclipse.emf.common.util.URI)
	 * @generated
	 */
	EOperation getRamsesWorkflowConfiguration__Store__URI();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#load(org.eclipse.emf.common.util.URI) <em>Load</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Load</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#load(org.eclipse.emf.common.util.URI)
	 * @generated
	 */
	EOperation getRamsesWorkflowConfiguration__Load__URI();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#create(java.lang.String, de.mdelab.workflow.Workflow) <em>Create</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration#create(java.lang.String, de.mdelab.workflow.Workflow)
	 * @generated
	 */
	EOperation getRamsesWorkflowConfiguration__Create__String_Workflow();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel <em>Ramses Workflow View Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ramses Workflow View Model</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel
	 * @generated
	 */
	EClass getRamsesWorkflowViewModel();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getResourceSet <em>Resource Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resource Set</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getResourceSet()
	 * @see #getRamsesWorkflowViewModel()
	 * @generated
	 */
	EAttribute getRamsesWorkflowViewModel_ResourceSet();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getAvailableWorkflows() <em>Get Available Workflows</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Available Workflows</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getAvailableWorkflows()
	 * @generated
	 */
	EOperation getRamsesWorkflowViewModel__GetAvailableWorkflows();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getAvailableGenerators() <em>Get Available Generators</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Available Generators</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getAvailableGenerators()
	 * @generated
	 */
	EOperation getRamsesWorkflowViewModel__GetAvailableGenerators();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getAvailableGeneratorNames() <em>Get Available Generator Names</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Available Generator Names</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getAvailableGeneratorNames()
	 * @generated
	 */
	EOperation getRamsesWorkflowViewModel__GetAvailableGeneratorNames();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getGeneratorFromTargetId(java.lang.String) <em>Get Generator From Target Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Generator From Target Id</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getGeneratorFromTargetId(java.lang.String)
	 * @generated
	 */
	EOperation getRamsesWorkflowViewModel__GetGeneratorFromTargetId__String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getGeneratorFromTargetName(java.lang.String) <em>Get Generator From Target Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Generator From Target Name</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getGeneratorFromTargetName(java.lang.String)
	 * @generated
	 */
	EOperation getRamsesWorkflowViewModel__GetGeneratorFromTargetName__String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getRegisteredPlugins() <em>Get Registered Plugins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Registered Plugins</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getRegisteredPlugins()
	 * @generated
	 */
	EOperation getRamsesWorkflowViewModel__GetRegisteredPlugins();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getWorkflows(java.lang.String) <em>Get Workflows</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Workflows</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getWorkflows(java.lang.String)
	 * @generated
	 */
	EOperation getRamsesWorkflowViewModel__GetWorkflows__String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#executeWorkflow(fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration, org.eclipse.emf.ecore.resource.Resource, java.lang.String, org.eclipse.core.runtime.IProgressMonitor) <em>Execute Workflow</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Execute Workflow</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#executeWorkflow(fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration, org.eclipse.emf.ecore.resource.Resource, java.lang.String, org.eclipse.core.runtime.IProgressMonitor)
	 * @generated
	 */
	EOperation getRamsesWorkflowViewModel__ExecuteWorkflow__RamsesConfiguration_Resource_String_IProgressMonitor();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getTargetWorkflow(java.lang.String) <em>Get Target Workflow</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Target Workflow</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getTargetWorkflow(java.lang.String)
	 * @generated
	 */
	EOperation getRamsesWorkflowViewModel__GetTargetWorkflow__String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getTargetInstanceWorkflow(java.lang.String) <em>Get Target Instance Workflow</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Target Instance Workflow</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getTargetInstanceWorkflow(java.lang.String)
	 * @generated
	 */
	EOperation getRamsesWorkflowViewModel__GetTargetInstanceWorkflow__String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getAvailableGeneratorsFromTargetId() <em>Get Available Generators From Target Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Available Generators From Target Id</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel#getAvailableGeneratorsFromTargetId()
	 * @generated
	 */
	EOperation getRamsesWorkflowViewModel__GetAvailableGeneratorsFromTargetId();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowExecutor <em>Ramses Workflow Executor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ramses Workflow Executor</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowExecutor
	 * @generated
	 */
	EClass getRamsesWorkflowExecutor();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowExecutor#execute(fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration, org.eclipse.emf.ecore.resource.ResourceSet, org.eclipse.core.runtime.IProgressMonitor) <em>Execute</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Execute</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowExecutor#execute(fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration, org.eclipse.emf.ecore.resource.ResourceSet, org.eclipse.core.runtime.IProgressMonitor)
	 * @generated
	 */
	EOperation getRamsesWorkflowExecutor__Execute__RamsesWorkflowConfiguration_ResourceSet_IProgressMonitor();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration <em>Ramses Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ramses Configuration</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration
	 * @generated
	 */
	EClass getRamsesConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getOutputDirectory <em>Output Directory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Directory</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getOutputDirectory()
	 * @see #getRamsesConfiguration()
	 * @generated
	 */
	EAttribute getRamsesConfiguration_OutputDirectory();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getRuntimeDirectory <em>Runtime Directory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Runtime Directory</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getRuntimeDirectory()
	 * @see #getRamsesConfiguration()
	 * @generated
	 */
	EAttribute getRamsesConfiguration_RuntimeDirectory();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getTargetId <em>Target Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Id</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getTargetId()
	 * @see #getRamsesConfiguration()
	 * @generated
	 */
	EAttribute getRamsesConfiguration_TargetId();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#isExposeRuntimeSharedResources <em>Expose Runtime Shared Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expose Runtime Shared Resources</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#isExposeRuntimeSharedResources()
	 * @see #getRamsesConfiguration()
	 * @generated
	 */
	EAttribute getRamsesConfiguration_ExposeRuntimeSharedResources();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getPropertyMap <em>Property Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Map</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getPropertyMap()
	 * @see #getRamsesConfiguration()
	 * @generated
	 */
	EAttribute getRamsesConfiguration_PropertyMap();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#setRamsesOutputDir(java.lang.String) <em>Set Ramses Output Dir</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Ramses Output Dir</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#setRamsesOutputDir(java.lang.String)
	 * @generated
	 */
	EOperation getRamsesConfiguration__SetRamsesOutputDir__String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#setRuntimePath(java.lang.String) <em>Set Runtime Path</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Runtime Path</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#setRuntimePath(java.lang.String)
	 * @generated
	 */
	EOperation getRamsesConfiguration__SetRuntimePath__String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#setGenerationTargetId(java.lang.String) <em>Set Generation Target Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Generation Target Id</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#setGenerationTargetId(java.lang.String)
	 * @generated
	 */
	EOperation getRamsesConfiguration__SetGenerationTargetId__String();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getRamsesOutputDir() <em>Get Ramses Output Dir</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Ramses Output Dir</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getRamsesOutputDir()
	 * @generated
	 */
	EOperation getRamsesConfiguration__GetRamsesOutputDir();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getRuntimePath() <em>Get Runtime Path</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Runtime Path</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#getRuntimePath()
	 * @generated
	 */
	EOperation getRamsesConfiguration__GetRuntimePath();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#log() <em>Log</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Log</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#log()
	 * @generated
	 */
	EOperation getRamsesConfiguration__Log();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#fetchProperties(org.eclipse.core.resources.IProject) <em>Fetch Properties</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Fetch Properties</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#fetchProperties(org.eclipse.core.resources.IProject)
	 * @generated
	 */
	EOperation getRamsesConfiguration__FetchProperties__IProject();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#saveConfig(org.eclipse.core.resources.IProject, java.util.Map) <em>Save Config</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Save Config</em>' operation.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration#saveConfig(org.eclipse.core.resources.IProject, java.util.Map)
	 * @generated
	 */
	EOperation getRamsesConfiguration__SaveConfig__IProject_Map();

	/**
	 * Returns the meta object for data type '{@link java.io.File <em>EFile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EFile</em>'.
	 * @see java.io.File
	 * @model instanceClass="java.io.File"
	 * @generated
	 */
	EDataType getEFile();

	/**
	 * Returns the meta object for data type '{@link fr.mem4csd.ramses.core.ramsesviewmodel.ConfigStatus <em>EConfig Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EConfig Status</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.ConfigStatus
	 * @model instanceClass="fr.mem4csd.ramses.core.ramsesviewmodel.ConfigStatus"
	 * @generated
	 */
	EDataType getEConfigStatus();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>EList</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EList</em>'.
	 * @see java.util.List
	 * @model instanceClass="java.util.List" typeParameters="T"
	 * @generated
	 */
	EDataType getEList();

	/**
	 * Returns the meta object for data type '{@link java.util.Set <em>ESet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ESet</em>'.
	 * @see java.util.Set
	 * @model instanceClass="java.util.Set" typeParameters="T"
	 * @generated
	 */
	EDataType getESet();

	/**
	 * Returns the meta object for data type '{@link fr.mem4csd.ramses.core.ramsesviewmodel.ConfigurationException <em>Configuration Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Configuration Exception</em>'.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.ConfigurationException
	 * @model instanceClass="fr.mem4csd.ramses.core.ramsesviewmodel.ConfigurationException"
	 * @generated
	 */
	EDataType getConfigurationException();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.core.resources.IProject <em>IProject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>IProject</em>'.
	 * @see org.eclipse.core.resources.IProject
	 * @model instanceClass="org.eclipse.core.resources.IProject"
	 * @generated
	 */
	EDataType getIProject();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.core.runtime.CoreException <em>Core Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Core Exception</em>'.
	 * @see org.eclipse.core.runtime.CoreException
	 * @model instanceClass="org.eclipse.core.runtime.CoreException"
	 * @generated
	 */
	EDataType getCoreException();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RamsesviewmodelFactory getRamsesviewmodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowConfigurationImpl <em>Ramses Workflow Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowConfigurationImpl
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getRamsesWorkflowConfiguration()
		 * @generated
		 */
		EClass RAMSES_WORKFLOW_CONFIGURATION = eINSTANCE.getRamsesWorkflowConfiguration();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RAMSES_WORKFLOW_CONFIGURATION__NAME = eINSTANCE.getRamsesWorkflowConfiguration_Name();

		/**
		 * The meta object literal for the '<em><b>Workflow URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RAMSES_WORKFLOW_CONFIGURATION__WORKFLOW_URI = eINSTANCE.getRamsesWorkflowConfiguration_WorkflowURI();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RAMSES_WORKFLOW_CONFIGURATION__PROPERTIES = eINSTANCE.getRamsesWorkflowConfiguration_Properties();

		/**
		 * The meta object literal for the '<em><b>Store</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_CONFIGURATION___STORE__URI = eINSTANCE.getRamsesWorkflowConfiguration__Store__URI();

		/**
		 * The meta object literal for the '<em><b>Load</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_CONFIGURATION___LOAD__URI = eINSTANCE.getRamsesWorkflowConfiguration__Load__URI();

		/**
		 * The meta object literal for the '<em><b>Create</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_CONFIGURATION___CREATE__STRING_WORKFLOW = eINSTANCE.getRamsesWorkflowConfiguration__Create__String_Workflow();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowViewModelImpl <em>Ramses Workflow View Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowViewModelImpl
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getRamsesWorkflowViewModel()
		 * @generated
		 */
		EClass RAMSES_WORKFLOW_VIEW_MODEL = eINSTANCE.getRamsesWorkflowViewModel();

		/**
		 * The meta object literal for the '<em><b>Resource Set</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RAMSES_WORKFLOW_VIEW_MODEL__RESOURCE_SET = eINSTANCE.getRamsesWorkflowViewModel_ResourceSet();

		/**
		 * The meta object literal for the '<em><b>Get Available Workflows</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_WORKFLOWS = eINSTANCE.getRamsesWorkflowViewModel__GetAvailableWorkflows();

		/**
		 * The meta object literal for the '<em><b>Get Available Generators</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATORS = eINSTANCE.getRamsesWorkflowViewModel__GetAvailableGenerators();

		/**
		 * The meta object literal for the '<em><b>Get Available Generator Names</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATOR_NAMES = eINSTANCE.getRamsesWorkflowViewModel__GetAvailableGeneratorNames();

		/**
		 * The meta object literal for the '<em><b>Get Generator From Target Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_VIEW_MODEL___GET_GENERATOR_FROM_TARGET_ID__STRING = eINSTANCE.getRamsesWorkflowViewModel__GetGeneratorFromTargetId__String();

		/**
		 * The meta object literal for the '<em><b>Get Generator From Target Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_VIEW_MODEL___GET_GENERATOR_FROM_TARGET_NAME__STRING = eINSTANCE.getRamsesWorkflowViewModel__GetGeneratorFromTargetName__String();

		/**
		 * The meta object literal for the '<em><b>Get Registered Plugins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_VIEW_MODEL___GET_REGISTERED_PLUGINS = eINSTANCE.getRamsesWorkflowViewModel__GetRegisteredPlugins();

		/**
		 * The meta object literal for the '<em><b>Get Workflows</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_VIEW_MODEL___GET_WORKFLOWS__STRING = eINSTANCE.getRamsesWorkflowViewModel__GetWorkflows__String();

		/**
		 * The meta object literal for the '<em><b>Execute Workflow</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_VIEW_MODEL___EXECUTE_WORKFLOW__RAMSESCONFIGURATION_RESOURCE_STRING_IPROGRESSMONITOR = eINSTANCE.getRamsesWorkflowViewModel__ExecuteWorkflow__RamsesConfiguration_Resource_String_IProgressMonitor();

		/**
		 * The meta object literal for the '<em><b>Get Target Workflow</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_VIEW_MODEL___GET_TARGET_WORKFLOW__STRING = eINSTANCE.getRamsesWorkflowViewModel__GetTargetWorkflow__String();

		/**
		 * The meta object literal for the '<em><b>Get Target Instance Workflow</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_VIEW_MODEL___GET_TARGET_INSTANCE_WORKFLOW__STRING = eINSTANCE.getRamsesWorkflowViewModel__GetTargetInstanceWorkflow__String();

		/**
		 * The meta object literal for the '<em><b>Get Available Generators From Target Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_VIEW_MODEL___GET_AVAILABLE_GENERATORS_FROM_TARGET_ID = eINSTANCE.getRamsesWorkflowViewModel__GetAvailableGeneratorsFromTargetId();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowExecutorImpl <em>Ramses Workflow Executor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesWorkflowExecutorImpl
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getRamsesWorkflowExecutor()
		 * @generated
		 */
		EClass RAMSES_WORKFLOW_EXECUTOR = eINSTANCE.getRamsesWorkflowExecutor();

		/**
		 * The meta object literal for the '<em><b>Execute</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_WORKFLOW_EXECUTOR___EXECUTE__RAMSESWORKFLOWCONFIGURATION_RESOURCESET_IPROGRESSMONITOR = eINSTANCE.getRamsesWorkflowExecutor__Execute__RamsesWorkflowConfiguration_ResourceSet_IProgressMonitor();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesConfigurationImpl <em>Ramses Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesConfigurationImpl
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getRamsesConfiguration()
		 * @generated
		 */
		EClass RAMSES_CONFIGURATION = eINSTANCE.getRamsesConfiguration();

		/**
		 * The meta object literal for the '<em><b>Output Directory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RAMSES_CONFIGURATION__OUTPUT_DIRECTORY = eINSTANCE.getRamsesConfiguration_OutputDirectory();

		/**
		 * The meta object literal for the '<em><b>Runtime Directory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RAMSES_CONFIGURATION__RUNTIME_DIRECTORY = eINSTANCE.getRamsesConfiguration_RuntimeDirectory();

		/**
		 * The meta object literal for the '<em><b>Target Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RAMSES_CONFIGURATION__TARGET_ID = eINSTANCE.getRamsesConfiguration_TargetId();

		/**
		 * The meta object literal for the '<em><b>Expose Runtime Shared Resources</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RAMSES_CONFIGURATION__EXPOSE_RUNTIME_SHARED_RESOURCES = eINSTANCE.getRamsesConfiguration_ExposeRuntimeSharedResources();

		/**
		 * The meta object literal for the '<em><b>Property Map</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RAMSES_CONFIGURATION__PROPERTY_MAP = eINSTANCE.getRamsesConfiguration_PropertyMap();

		/**
		 * The meta object literal for the '<em><b>Set Ramses Output Dir</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_CONFIGURATION___SET_RAMSES_OUTPUT_DIR__STRING = eINSTANCE.getRamsesConfiguration__SetRamsesOutputDir__String();

		/**
		 * The meta object literal for the '<em><b>Set Runtime Path</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_CONFIGURATION___SET_RUNTIME_PATH__STRING = eINSTANCE.getRamsesConfiguration__SetRuntimePath__String();

		/**
		 * The meta object literal for the '<em><b>Set Generation Target Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_CONFIGURATION___SET_GENERATION_TARGET_ID__STRING = eINSTANCE.getRamsesConfiguration__SetGenerationTargetId__String();

		/**
		 * The meta object literal for the '<em><b>Get Ramses Output Dir</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_CONFIGURATION___GET_RAMSES_OUTPUT_DIR = eINSTANCE.getRamsesConfiguration__GetRamsesOutputDir();

		/**
		 * The meta object literal for the '<em><b>Get Runtime Path</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_CONFIGURATION___GET_RUNTIME_PATH = eINSTANCE.getRamsesConfiguration__GetRuntimePath();

		/**
		 * The meta object literal for the '<em><b>Log</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_CONFIGURATION___LOG = eINSTANCE.getRamsesConfiguration__Log();

		/**
		 * The meta object literal for the '<em><b>Fetch Properties</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_CONFIGURATION___FETCH_PROPERTIES__IPROJECT = eINSTANCE.getRamsesConfiguration__FetchProperties__IProject();

		/**
		 * The meta object literal for the '<em><b>Save Config</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RAMSES_CONFIGURATION___SAVE_CONFIG__IPROJECT_MAP = eINSTANCE.getRamsesConfiguration__SaveConfig__IProject_Map();

		/**
		 * The meta object literal for the '<em>EFile</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.io.File
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getEFile()
		 * @generated
		 */
		EDataType EFILE = eINSTANCE.getEFile();

		/**
		 * The meta object literal for the '<em>EConfig Status</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.ConfigStatus
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getEConfigStatus()
		 * @generated
		 */
		EDataType ECONFIG_STATUS = eINSTANCE.getEConfigStatus();

		/**
		 * The meta object literal for the '<em>EList</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getEList()
		 * @generated
		 */
		EDataType ELIST = eINSTANCE.getEList();

		/**
		 * The meta object literal for the '<em>ESet</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Set
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getESet()
		 * @generated
		 */
		EDataType ESET = eINSTANCE.getESet();

		/**
		 * The meta object literal for the '<em>Configuration Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.ConfigurationException
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getConfigurationException()
		 * @generated
		 */
		EDataType CONFIGURATION_EXCEPTION = eINSTANCE.getConfigurationException();

		/**
		 * The meta object literal for the '<em>IProject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.core.resources.IProject
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getIProject()
		 * @generated
		 */
		EDataType IPROJECT = eINSTANCE.getIProject();

		/**
		 * The meta object literal for the '<em>Core Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.core.runtime.CoreException
		 * @see fr.mem4csd.ramses.core.ramsesviewmodel.impl.RamsesviewmodelPackageImpl#getCoreException()
		 * @generated
		 */
		EDataType CORE_EXCEPTION = eINSTANCE.getCoreException();

	}

} //RamsesviewmodelPackage
