/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.ramsesviewmodel.util;

import fr.mem4csd.ramses.core.ramsesviewmodel.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesviewmodelPackage
 * @generated
 */
public class RamsesviewmodelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RamsesviewmodelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RamsesviewmodelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = RamsesviewmodelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RamsesviewmodelSwitch<Adapter> modelSwitch =
		new RamsesviewmodelSwitch<Adapter>() {
			@Override
			public Adapter caseRamsesWorkflowConfiguration(RamsesWorkflowConfiguration object) {
				return createRamsesWorkflowConfigurationAdapter();
			}
			@Override
			public Adapter caseRamsesWorkflowViewModel(RamsesWorkflowViewModel object) {
				return createRamsesWorkflowViewModelAdapter();
			}
			@Override
			public Adapter caseRamsesWorkflowExecutor(RamsesWorkflowExecutor object) {
				return createRamsesWorkflowExecutorAdapter();
			}
			@Override
			public Adapter caseRamsesConfiguration(RamsesConfiguration object) {
				return createRamsesConfigurationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration <em>Ramses Workflow Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowConfiguration
	 * @generated
	 */
	public Adapter createRamsesWorkflowConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel <em>Ramses Workflow View Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowViewModel
	 * @generated
	 */
	public Adapter createRamsesWorkflowViewModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowExecutor <em>Ramses Workflow Executor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesWorkflowExecutor
	 * @generated
	 */
	public Adapter createRamsesWorkflowExecutorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration <em>Ramses Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.ramsesviewmodel.RamsesConfiguration
	 * @generated
	 */
	public Adapter createRamsesConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //RamsesviewmodelAdapterFactory
