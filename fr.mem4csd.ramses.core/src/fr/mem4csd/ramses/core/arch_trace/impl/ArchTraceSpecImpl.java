/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.arch_trace.impl;

import fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace;
import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Arch Trace Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.arch_trace.impl.ArchTraceSpecImpl#getArchRefinementTrace <em>Arch Refinement Trace</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArchTraceSpecImpl extends IdentifiedElementImpl implements ArchTraceSpec {
	/**
	 * The cached value of the '{@link #getArchRefinementTrace() <em>Arch Refinement Trace</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArchRefinementTrace()
	 * @generated
	 * @ordered
	 */
	protected EList<ArchRefinementTrace> archRefinementTrace;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArchTraceSpecImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Arch_tracePackage.Literals.ARCH_TRACE_SPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ArchRefinementTrace> getArchRefinementTrace() {
		if (archRefinementTrace == null) {
			archRefinementTrace = new EObjectContainmentEList<ArchRefinementTrace>(ArchRefinementTrace.class, this, Arch_tracePackage.ARCH_TRACE_SPEC__ARCH_REFINEMENT_TRACE);
		}
		return archRefinementTrace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public NamedElement getTransformationTrace(NamedElement targetDeclarative) {
		for(ArchRefinementTrace ne: archRefinementTrace)
		{
			String qualifiedName = ne.getTargetElement().getQualifiedName();
			if(qualifiedName != null && qualifiedName.equals(targetDeclarative.getQualifiedName()))
				return ne.getSourceElement();
		}

		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void cleanupTransformationTrace() {
		archRefinementTrace.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Arch_tracePackage.ARCH_TRACE_SPEC__ARCH_REFINEMENT_TRACE:
				return ((InternalEList<?>)getArchRefinementTrace()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Arch_tracePackage.ARCH_TRACE_SPEC__ARCH_REFINEMENT_TRACE:
				return getArchRefinementTrace();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Arch_tracePackage.ARCH_TRACE_SPEC__ARCH_REFINEMENT_TRACE:
				getArchRefinementTrace().clear();
				getArchRefinementTrace().addAll((Collection<? extends ArchRefinementTrace>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Arch_tracePackage.ARCH_TRACE_SPEC__ARCH_REFINEMENT_TRACE:
				getArchRefinementTrace().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Arch_tracePackage.ARCH_TRACE_SPEC__ARCH_REFINEMENT_TRACE:
				return archRefinementTrace != null && !archRefinementTrace.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Arch_tracePackage.ARCH_TRACE_SPEC___GET_TRANSFORMATION_TRACE__NAMEDELEMENT:
				return getTransformationTrace((NamedElement)arguments.get(0));
			case Arch_tracePackage.ARCH_TRACE_SPEC___CLEANUP_TRANSFORMATION_TRACE:
				cleanupTransformationTrace();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //ArchTraceSpecImpl
