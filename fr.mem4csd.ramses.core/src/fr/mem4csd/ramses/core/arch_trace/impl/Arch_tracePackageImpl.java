/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.arch_trace.impl;

import fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace;
import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.arch_trace.Arch_traceFactory;
import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;
import fr.mem4csd.ramses.core.arch_trace.IdentifiedElement;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Arch_tracePackageImpl extends EPackageImpl implements Arch_tracePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass archTraceSpecEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass archRefinementTraceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identifiedElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Arch_tracePackageImpl() {
		super(eNS_URI, Arch_traceFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link Arch_tracePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Arch_tracePackage init() {
		if (isInited) return (Arch_tracePackage)EPackage.Registry.INSTANCE.getEPackage(Arch_tracePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredArch_tracePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		Arch_tracePackageImpl theArch_tracePackage = registeredArch_tracePackage instanceof Arch_tracePackageImpl ? (Arch_tracePackageImpl)registeredArch_tracePackage : new Arch_tracePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Aadl2Package.eINSTANCE.eClass();

		// Create package meta-data objects
		theArch_tracePackage.createPackageContents();

		// Initialize created meta-data
		theArch_tracePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theArch_tracePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Arch_tracePackage.eNS_URI, theArch_tracePackage);
		return theArch_tracePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getArchTraceSpec() {
		return archTraceSpecEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArchTraceSpec_ArchRefinementTrace() {
		return (EReference)archTraceSpecEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getArchTraceSpec__GetTransformationTrace__NamedElement() {
		return archTraceSpecEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getArchTraceSpec__CleanupTransformationTrace() {
		return archTraceSpecEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getArchRefinementTrace() {
		return archRefinementTraceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArchRefinementTrace_SourceElement() {
		return (EReference)archRefinementTraceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArchRefinementTrace_TargetElement() {
		return (EReference)archRefinementTraceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArchRefinementTrace_TransformationRule() {
		return (EAttribute)archRefinementTraceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIdentifiedElement() {
		return identifiedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIdentifiedElement_Id() {
		return (EAttribute)identifiedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIdentifiedElement_Name() {
		return (EAttribute)identifiedElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Arch_traceFactory getArch_traceFactory() {
		return (Arch_traceFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		archTraceSpecEClass = createEClass(ARCH_TRACE_SPEC);
		createEReference(archTraceSpecEClass, ARCH_TRACE_SPEC__ARCH_REFINEMENT_TRACE);
		createEOperation(archTraceSpecEClass, ARCH_TRACE_SPEC___GET_TRANSFORMATION_TRACE__NAMEDELEMENT);
		createEOperation(archTraceSpecEClass, ARCH_TRACE_SPEC___CLEANUP_TRANSFORMATION_TRACE);

		archRefinementTraceEClass = createEClass(ARCH_REFINEMENT_TRACE);
		createEReference(archRefinementTraceEClass, ARCH_REFINEMENT_TRACE__SOURCE_ELEMENT);
		createEReference(archRefinementTraceEClass, ARCH_REFINEMENT_TRACE__TARGET_ELEMENT);
		createEAttribute(archRefinementTraceEClass, ARCH_REFINEMENT_TRACE__TRANSFORMATION_RULE);

		identifiedElementEClass = createEClass(IDENTIFIED_ELEMENT);
		createEAttribute(identifiedElementEClass, IDENTIFIED_ELEMENT__ID);
		createEAttribute(identifiedElementEClass, IDENTIFIED_ELEMENT__NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Aadl2Package theAadl2Package = (Aadl2Package)EPackage.Registry.INSTANCE.getEPackage(Aadl2Package.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		archTraceSpecEClass.getESuperTypes().add(this.getIdentifiedElement());
		archRefinementTraceEClass.getESuperTypes().add(this.getIdentifiedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(archTraceSpecEClass, ArchTraceSpec.class, "ArchTraceSpec", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArchTraceSpec_ArchRefinementTrace(), this.getArchRefinementTrace(), null, "archRefinementTrace", null, 1, -1, ArchTraceSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getArchTraceSpec__GetTransformationTrace__NamedElement(), theAadl2Package.getNamedElement(), "getTransformationTrace", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getNamedElement(), "targetDeclarative", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getArchTraceSpec__CleanupTransformationTrace(), null, "cleanupTransformationTrace", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(archRefinementTraceEClass, ArchRefinementTrace.class, "ArchRefinementTrace", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArchRefinementTrace_SourceElement(), theAadl2Package.getNamedElement(), null, "sourceElement", null, 0, 1, ArchRefinementTrace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArchRefinementTrace_TargetElement(), theAadl2Package.getNamedElement(), null, "targetElement", null, 0, 1, ArchRefinementTrace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArchRefinementTrace_TransformationRule(), ecorePackage.getEString(), "transformationRule", null, 1, 1, ArchRefinementTrace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(identifiedElementEClass, IdentifiedElement.class, "IdentifiedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIdentifiedElement_Id(), ecorePackage.getEString(), "id", null, 1, 1, IdentifiedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdentifiedElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, IdentifiedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Arch_tracePackageImpl
