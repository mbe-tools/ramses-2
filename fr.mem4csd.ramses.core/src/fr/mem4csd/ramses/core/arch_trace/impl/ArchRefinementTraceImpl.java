/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.arch_trace.impl;

import fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace;
import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Arch Refinement Trace</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.arch_trace.impl.ArchRefinementTraceImpl#getSourceElement <em>Source Element</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.arch_trace.impl.ArchRefinementTraceImpl#getTargetElement <em>Target Element</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.arch_trace.impl.ArchRefinementTraceImpl#getTransformationRule <em>Transformation Rule</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArchRefinementTraceImpl extends IdentifiedElementImpl implements ArchRefinementTrace {
	/**
	 * The cached value of the '{@link #getSourceElement() <em>Source Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceElement()
	 * @generated
	 * @ordered
	 */
	protected NamedElement sourceElement;

	/**
	 * The cached value of the '{@link #getTargetElement() <em>Target Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetElement()
	 * @generated
	 * @ordered
	 */
	protected NamedElement targetElement;

	/**
	 * The default value of the '{@link #getTransformationRule() <em>Transformation Rule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransformationRule()
	 * @generated
	 * @ordered
	 */
	protected static final String TRANSFORMATION_RULE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTransformationRule() <em>Transformation Rule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransformationRule()
	 * @generated
	 * @ordered
	 */
	protected String transformationRule = TRANSFORMATION_RULE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArchRefinementTraceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Arch_tracePackage.Literals.ARCH_REFINEMENT_TRACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NamedElement getSourceElement() {
		if (sourceElement != null && ((EObject)sourceElement).eIsProxy()) {
			InternalEObject oldSourceElement = (InternalEObject)sourceElement;
			sourceElement = (NamedElement)eResolveProxy(oldSourceElement);
			if (sourceElement != oldSourceElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Arch_tracePackage.ARCH_REFINEMENT_TRACE__SOURCE_ELEMENT, oldSourceElement, sourceElement));
			}
		}
		return sourceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedElement basicGetSourceElement() {
		return sourceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceElement(NamedElement newSourceElement) {
		NamedElement oldSourceElement = sourceElement;
		sourceElement = newSourceElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Arch_tracePackage.ARCH_REFINEMENT_TRACE__SOURCE_ELEMENT, oldSourceElement, sourceElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NamedElement getTargetElement() {
		if (targetElement != null && ((EObject)targetElement).eIsProxy()) {
			InternalEObject oldTargetElement = (InternalEObject)targetElement;
			targetElement = (NamedElement)eResolveProxy(oldTargetElement);
			if (targetElement != oldTargetElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Arch_tracePackage.ARCH_REFINEMENT_TRACE__TARGET_ELEMENT, oldTargetElement, targetElement));
			}
		}
		return targetElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedElement basicGetTargetElement() {
		return targetElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetElement(NamedElement newTargetElement) {
		NamedElement oldTargetElement = targetElement;
		targetElement = newTargetElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Arch_tracePackage.ARCH_REFINEMENT_TRACE__TARGET_ELEMENT, oldTargetElement, targetElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTransformationRule() {
		return transformationRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTransformationRule(String newTransformationRule) {
		String oldTransformationRule = transformationRule;
		transformationRule = newTransformationRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Arch_tracePackage.ARCH_REFINEMENT_TRACE__TRANSFORMATION_RULE, oldTransformationRule, transformationRule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__SOURCE_ELEMENT:
				if (resolve) return getSourceElement();
				return basicGetSourceElement();
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__TARGET_ELEMENT:
				if (resolve) return getTargetElement();
				return basicGetTargetElement();
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__TRANSFORMATION_RULE:
				return getTransformationRule();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__SOURCE_ELEMENT:
				setSourceElement((NamedElement)newValue);
				return;
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__TARGET_ELEMENT:
				setTargetElement((NamedElement)newValue);
				return;
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__TRANSFORMATION_RULE:
				setTransformationRule((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__SOURCE_ELEMENT:
				setSourceElement((NamedElement)null);
				return;
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__TARGET_ELEMENT:
				setTargetElement((NamedElement)null);
				return;
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__TRANSFORMATION_RULE:
				setTransformationRule(TRANSFORMATION_RULE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__SOURCE_ELEMENT:
				return sourceElement != null;
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__TARGET_ELEMENT:
				return targetElement != null;
			case Arch_tracePackage.ARCH_REFINEMENT_TRACE__TRANSFORMATION_RULE:
				return TRANSFORMATION_RULE_EDEFAULT == null ? transformationRule != null : !TRANSFORMATION_RULE_EDEFAULT.equals(transformationRule);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (transformationRule: ");
		result.append(transformationRule);
		result.append(')');
		return result.toString();
	}

} //ArchRefinementTraceImpl
