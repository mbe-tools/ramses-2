/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.arch_trace.util;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.instance.FeatureInstance;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;


public class ArchTraceSourceRetreival {

	
	private static Logger _LOGGER = Logger.getLogger(ArchTraceSourceRetreival.class) ;
	  
	public static NamedElement findNamedElementByClassInTransformationTraces(NamedElement ne,
			Class<? extends NamedElement> type)
	{
		ArchTraceSpec traces = getTracesFromNamedElement(ne);
		NamedElement res = traces.getTransformationTrace(ne);
		if(res!=null)
		{
			if(res.getClass()==type)
			{
				return (FeatureInstance) res;
			}
			else
				return findNamedElementByClassInTransformationTraces(res, type);
		}
	    return null;
	}

	public static NamedElement findNamedElementByClassInTransformationTraces(
			ArchTraceSpec traces ,
			NamedElement ne,
			Class<? extends NamedElement> type)
	{
		EcoreUtil.resolveAll(traces.eResource());
		NamedElement res = traces.getTransformationTrace(ne);
		if(res!=null)
		{
			if(res.getClass()==type)
			{
				return res;
			}
			else
				return findNamedElementByClassInTransformationTraces(traces, res, type);
		}
		return res;
	}
	
	public static ArchTraceSpec getTracesFromNamedElement(NamedElement ne) {
		try {
			String neResourceURIString = ne.eResource().getURI().toString();
			int extensionIndex = neResourceURIString.lastIndexOf(".aadl");
			String tracesURIString = neResourceURIString.substring(0, extensionIndex);
			tracesURIString = tracesURIString+".arch_trace";
			ResourceSet rs = ne.eResource().getResourceSet();
			Resource tracesRes = rs.getResource(URI.createURI(tracesURIString), true);
			ArchTraceSpec traces = (ArchTraceSpec) tracesRes.getContents().get(0);
			return traces;
		}
		catch (Exception e)
		{
			String errMsg = "Could not find transformation trace for \'" +
                    ne.getQualifiedName() + '\'' ;
			_LOGGER.error(errMsg);
			return null;
			
		}
	}
}
