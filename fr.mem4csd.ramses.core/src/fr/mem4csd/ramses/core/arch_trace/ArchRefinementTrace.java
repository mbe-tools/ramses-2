/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.arch_trace;

import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arch Refinement Trace</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getSourceElement <em>Source Element</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getTargetElement <em>Target Element</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getTransformationRule <em>Transformation Rule</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage#getArchRefinementTrace()
 * @model
 * @generated
 */
public interface ArchRefinementTrace extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Element</em>' reference.
	 * @see #setSourceElement(NamedElement)
	 * @see fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage#getArchRefinementTrace_SourceElement()
	 * @model
	 * @generated
	 */
	NamedElement getSourceElement();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getSourceElement <em>Source Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Element</em>' reference.
	 * @see #getSourceElement()
	 * @generated
	 */
	void setSourceElement(NamedElement value);

	/**
	 * Returns the value of the '<em><b>Target Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Element</em>' reference.
	 * @see #setTargetElement(NamedElement)
	 * @see fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage#getArchRefinementTrace_TargetElement()
	 * @model
	 * @generated
	 */
	NamedElement getTargetElement();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getTargetElement <em>Target Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Element</em>' reference.
	 * @see #getTargetElement()
	 * @generated
	 */
	void setTargetElement(NamedElement value);

	/**
	 * Returns the value of the '<em><b>Transformation Rule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformation Rule</em>' attribute.
	 * @see #setTransformationRule(String)
	 * @see fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage#getArchRefinementTrace_TransformationRule()
	 * @model required="true"
	 * @generated
	 */
	String getTransformationRule();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getTransformationRule <em>Transformation Rule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transformation Rule</em>' attribute.
	 * @see #getTransformationRule()
	 * @generated
	 */
	void setTransformationRule(String value);

} // ArchRefinementTrace
