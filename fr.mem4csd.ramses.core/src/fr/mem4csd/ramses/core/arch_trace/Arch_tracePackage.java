/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.arch_trace;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.core.arch_trace.Arch_traceFactory
 * @model kind="package"
 * @generated
 */
public interface Arch_tracePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "arch_trace";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/ramses/arch_trace";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "arch_trace";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Arch_tracePackage eINSTANCE = fr.mem4csd.ramses.core.arch_trace.impl.Arch_tracePackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.arch_trace.impl.IdentifiedElementImpl <em>Identified Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.arch_trace.impl.IdentifiedElementImpl
	 * @see fr.mem4csd.ramses.core.arch_trace.impl.Arch_tracePackageImpl#getIdentifiedElement()
	 * @generated
	 */
	int IDENTIFIED_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIED_ELEMENT__ID = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIED_ELEMENT__NAME = 1;

	/**
	 * The number of structural features of the '<em>Identified Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIED_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Identified Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.arch_trace.impl.ArchTraceSpecImpl <em>Arch Trace Spec</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.arch_trace.impl.ArchTraceSpecImpl
	 * @see fr.mem4csd.ramses.core.arch_trace.impl.Arch_tracePackageImpl#getArchTraceSpec()
	 * @generated
	 */
	int ARCH_TRACE_SPEC = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_TRACE_SPEC__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_TRACE_SPEC__NAME = IDENTIFIED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Arch Refinement Trace</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_TRACE_SPEC__ARCH_REFINEMENT_TRACE = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Arch Trace Spec</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_TRACE_SPEC_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Transformation Trace</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_TRACE_SPEC___GET_TRANSFORMATION_TRACE__NAMEDELEMENT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Cleanup Transformation Trace</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_TRACE_SPEC___CLEANUP_TRANSFORMATION_TRACE = IDENTIFIED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Arch Trace Spec</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_TRACE_SPEC_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.arch_trace.impl.ArchRefinementTraceImpl <em>Arch Refinement Trace</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.arch_trace.impl.ArchRefinementTraceImpl
	 * @see fr.mem4csd.ramses.core.arch_trace.impl.Arch_tracePackageImpl#getArchRefinementTrace()
	 * @generated
	 */
	int ARCH_REFINEMENT_TRACE = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_REFINEMENT_TRACE__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_REFINEMENT_TRACE__NAME = IDENTIFIED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_REFINEMENT_TRACE__SOURCE_ELEMENT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_REFINEMENT_TRACE__TARGET_ELEMENT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Transformation Rule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_REFINEMENT_TRACE__TRANSFORMATION_RULE = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Arch Refinement Trace</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_REFINEMENT_TRACE_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Arch Refinement Trace</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCH_REFINEMENT_TRACE_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec <em>Arch Trace Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Arch Trace Spec</em>'.
	 * @see fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec
	 * @generated
	 */
	EClass getArchTraceSpec();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec#getArchRefinementTrace <em>Arch Refinement Trace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Arch Refinement Trace</em>'.
	 * @see fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec#getArchRefinementTrace()
	 * @see #getArchTraceSpec()
	 * @generated
	 */
	EReference getArchTraceSpec_ArchRefinementTrace();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec#getTransformationTrace(org.osate.aadl2.NamedElement) <em>Get Transformation Trace</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Transformation Trace</em>' operation.
	 * @see fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec#getTransformationTrace(org.osate.aadl2.NamedElement)
	 * @generated
	 */
	EOperation getArchTraceSpec__GetTransformationTrace__NamedElement();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec#cleanupTransformationTrace() <em>Cleanup Transformation Trace</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cleanup Transformation Trace</em>' operation.
	 * @see fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec#cleanupTransformationTrace()
	 * @generated
	 */
	EOperation getArchTraceSpec__CleanupTransformationTrace();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace <em>Arch Refinement Trace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Arch Refinement Trace</em>'.
	 * @see fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace
	 * @generated
	 */
	EClass getArchRefinementTrace();

	/**
	 * Returns the meta object for the reference '{@link fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getSourceElement <em>Source Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source Element</em>'.
	 * @see fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getSourceElement()
	 * @see #getArchRefinementTrace()
	 * @generated
	 */
	EReference getArchRefinementTrace_SourceElement();

	/**
	 * Returns the meta object for the reference '{@link fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getTargetElement <em>Target Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Element</em>'.
	 * @see fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getTargetElement()
	 * @see #getArchRefinementTrace()
	 * @generated
	 */
	EReference getArchRefinementTrace_TargetElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getTransformationRule <em>Transformation Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transformation Rule</em>'.
	 * @see fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace#getTransformationRule()
	 * @see #getArchRefinementTrace()
	 * @generated
	 */
	EAttribute getArchRefinementTrace_TransformationRule();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.arch_trace.IdentifiedElement <em>Identified Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Identified Element</em>'.
	 * @see fr.mem4csd.ramses.core.arch_trace.IdentifiedElement
	 * @generated
	 */
	EClass getIdentifiedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.arch_trace.IdentifiedElement#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see fr.mem4csd.ramses.core.arch_trace.IdentifiedElement#getId()
	 * @see #getIdentifiedElement()
	 * @generated
	 */
	EAttribute getIdentifiedElement_Id();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.arch_trace.IdentifiedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.mem4csd.ramses.core.arch_trace.IdentifiedElement#getName()
	 * @see #getIdentifiedElement()
	 * @generated
	 */
	EAttribute getIdentifiedElement_Name();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Arch_traceFactory getArch_traceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.arch_trace.impl.ArchTraceSpecImpl <em>Arch Trace Spec</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.arch_trace.impl.ArchTraceSpecImpl
		 * @see fr.mem4csd.ramses.core.arch_trace.impl.Arch_tracePackageImpl#getArchTraceSpec()
		 * @generated
		 */
		EClass ARCH_TRACE_SPEC = eINSTANCE.getArchTraceSpec();

		/**
		 * The meta object literal for the '<em><b>Arch Refinement Trace</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARCH_TRACE_SPEC__ARCH_REFINEMENT_TRACE = eINSTANCE.getArchTraceSpec_ArchRefinementTrace();

		/**
		 * The meta object literal for the '<em><b>Get Transformation Trace</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ARCH_TRACE_SPEC___GET_TRANSFORMATION_TRACE__NAMEDELEMENT = eINSTANCE.getArchTraceSpec__GetTransformationTrace__NamedElement();

		/**
		 * The meta object literal for the '<em><b>Cleanup Transformation Trace</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ARCH_TRACE_SPEC___CLEANUP_TRANSFORMATION_TRACE = eINSTANCE.getArchTraceSpec__CleanupTransformationTrace();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.arch_trace.impl.ArchRefinementTraceImpl <em>Arch Refinement Trace</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.arch_trace.impl.ArchRefinementTraceImpl
		 * @see fr.mem4csd.ramses.core.arch_trace.impl.Arch_tracePackageImpl#getArchRefinementTrace()
		 * @generated
		 */
		EClass ARCH_REFINEMENT_TRACE = eINSTANCE.getArchRefinementTrace();

		/**
		 * The meta object literal for the '<em><b>Source Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARCH_REFINEMENT_TRACE__SOURCE_ELEMENT = eINSTANCE.getArchRefinementTrace_SourceElement();

		/**
		 * The meta object literal for the '<em><b>Target Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARCH_REFINEMENT_TRACE__TARGET_ELEMENT = eINSTANCE.getArchRefinementTrace_TargetElement();

		/**
		 * The meta object literal for the '<em><b>Transformation Rule</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARCH_REFINEMENT_TRACE__TRANSFORMATION_RULE = eINSTANCE.getArchRefinementTrace_TransformationRule();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.arch_trace.impl.IdentifiedElementImpl <em>Identified Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.arch_trace.impl.IdentifiedElementImpl
		 * @see fr.mem4csd.ramses.core.arch_trace.impl.Arch_tracePackageImpl#getIdentifiedElement()
		 * @generated
		 */
		EClass IDENTIFIED_ELEMENT = eINSTANCE.getIdentifiedElement();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIED_ELEMENT__ID = eINSTANCE.getIdentifiedElement_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIED_ELEMENT__NAME = eINSTANCE.getIdentifiedElement_Name();

	}

} //Arch_tracePackage
