/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.arch_trace;

import org.eclipse.emf.common.util.EList;

import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arch Trace Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec#getArchRefinementTrace <em>Arch Refinement Trace</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage#getArchTraceSpec()
 * @model
 * @generated
 */
public interface ArchTraceSpec extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Arch Refinement Trace</b></em>' containment reference list.
	 * The list contents are of type {@link fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arch Refinement Trace</em>' containment reference list.
	 * @see fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage#getArchTraceSpec_ArchRefinementTrace()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ArchRefinementTrace> getArchRefinementTrace();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" targetDeclarativeRequired="true"
	 * @generated
	 */
	NamedElement getTransformationTrace(NamedElement targetDeclarative);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void cleanupTransformationTrace();

} // ArchTraceSpec
