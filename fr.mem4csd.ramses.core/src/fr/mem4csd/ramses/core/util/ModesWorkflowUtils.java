/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.util;

import java.util.List;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.EcoreUtil2;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.instance.ComponentInstance;

public class ModesWorkflowUtils {

	
	public static boolean hasProcessModes(Resource instanceModel)
	{
		List<ComponentInstance> cList = EcoreUtil2.getAllContentsOfType(instanceModel.getContents().get(0), ComponentInstance.class);
		for(ComponentInstance c:cList)
		{
			if(c.getCategory()!=ComponentCategory.PROCESS)
				continue;
			if(c.getModeInstances().isEmpty())
				continue;
			
			return true;
		}
		return false;
	}
}
