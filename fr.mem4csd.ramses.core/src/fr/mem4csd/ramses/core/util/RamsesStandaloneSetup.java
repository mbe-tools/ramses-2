/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.util;

import java.util.Collection;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.m2m.atl.emftvm.EmftvmPackage;
import org.eclipse.m2m.atl.emftvm.impl.resource.EMFTVMResourceFactoryImpl;

import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;
import fr.mem4csd.ramses.core.helpers.impl.HelpersPackageImpl;
import fr.mem4csd.ramses.core.validation_report.Validation_reportPackage;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;
import fr.mem4csd.ramses.modes.workflowramsesmodes.WorkflowramsesmodesPackage;
import fr.tpt.mem4csd.utils.osate.standalone.OsateStandaloneSetup;
import fr.tpt.mem4csd.utils.osate.standalone.StandaloneAnnexRegistry.AnnexExtensionRegistration;
import fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage;
import fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage;

public class RamsesStandaloneSetup {
	
	private OsateStandaloneSetup osateSetup;
	
	public RamsesStandaloneSetup() {
		osateSetup = new OsateStandaloneSetup();
		initialize();
	}
	
	public RamsesStandaloneSetup( final Collection<AnnexExtensionRegistration> annexExts ) {
		osateSetup = new OsateStandaloneSetup( annexExts );
		initialize();
	}
	
	public RamsesStandaloneSetup( final URI osateBaseUri ) {
		osateSetup = new OsateStandaloneSetup( osateBaseUri );
		initialize();
	}
	
	public RamsesStandaloneSetup( final URI osateBaseUri,
								  final Collection<AnnexExtensionRegistration> annexExts ) {
		osateSetup = new OsateStandaloneSetup( osateBaseUri, annexExts );
		initialize();
	}

	public ResourceSet createResourceSet() {
		return osateSetup.createResourceSet();
	}
	
	public ResourceSet createResourceSet( final Set<String> predefinedAadlResNames ) {
		return osateSetup.createResourceSet( predefinedAadlResNames );
	}

	public ResourceSet createResourceSet( final URI predefinedAadlResDir ) {
		return osateSetup.createResourceSet( predefinedAadlResDir );
	}

	public ResourceSet createResourceSet( final URI predefinedAadlResDir,
										  final Set<String> predefinedAadlResNames ) {
		return osateSetup.createResourceSet( predefinedAadlResDir, predefinedAadlResNames );
	}
	
	public static void initialize() {
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("emftvm", new EMFTVMResourceFactoryImpl()) ;
		EmftvmPackage.eINSTANCE.eClass();
		WorkflowosatePackage.eINSTANCE.eClass();
		WorkflowemftvmPackage.eINSTANCE.eClass();
		HelpersPackageImpl.eINSTANCE.eClass();// Strangely only works with impl class
		Validation_reportPackage.eINSTANCE.eClass();
		Arch_tracePackage.eINSTANCE.eClass();
		WorkflowramsesPackage.eINSTANCE.eClass();
		WorkflowramsesmodesPackage.eINSTANCE.eClass();
	}
}
