/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.util;

import java.util.Map;

import org.eclipse.core.runtime.Platform;

public class RamsesWorkflowKeys {
	public static final String PROPERTIES_FILE = "properties_file";
	public static final String SOURCE_AADL_FILE = "source_aadl_file";
	public static final String REFINED_AADL_FILE = "refined_aadl_file";
	public static final String REFINED_TRACE_FILE = "refined_trace_file";
	public static final String INSTANCE_MODEL_FILE = "instance_model_file";
	public static final String INPUT_DIR = "input_dir";
	public static final String OUTPUT_DIR = "output_dir";
	public static final String INCLUDE_DIR = "include_dir";
	public static final String TARGET_INSTALL_DIR = "target_install_dir";
	public static final String SYSTEM_IMPLEMENTATION_NAME = "system_implementation_name";
	public static final String WORKING_DIR = "working_dir";
	public static final String SOURCE_FILE_NAME = "source_file_name";
	public static final String VALIDATION_REPORT_FILE = "validation_report_file";
	public static final String EXPOSE_RUNTIME_SHARED_RESOURCES = "expose_runtime_shared_resources";
	
	public static void setDefaultPropertyValues(Map<String, String> propertyValues)
	{
		if(!propertyValues.containsKey(RamsesWorkflowKeys.TARGET_INSTALL_DIR))
			propertyValues.put( RamsesWorkflowKeys.TARGET_INSTALL_DIR, "" );
		
		if ( Platform.isRunning() ) {
			if(!propertyValues.containsKey("runtime_scheme"))
					propertyValues.put( "runtime_scheme", "platform:/plugin/" );
		}
		else {
			
			// Change the scheme for standalone so that contributions packaged into jars 
			// are read using the classpath scheme without the plugin name 
			if(!propertyValues.containsKey("runtime_scheme"))
				propertyValues.put( "runtime_scheme", "../../../../../plugins/" );
		}
	}
}
