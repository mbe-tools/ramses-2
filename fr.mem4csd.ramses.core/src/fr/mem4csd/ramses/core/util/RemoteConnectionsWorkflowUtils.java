/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.util;

import java.util.List;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.EcoreUtil2;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.utils.internal.PropertyUtils;

public class RemoteConnectionsWorkflowUtils {

	private static final String comPropertyName = "Communication_Protocol";
	
	public static boolean hasRemoteConnectionOfProtocol(Resource instanceModel,
			String protocol)
	{
		List<ConnectionInstance> cList = EcoreUtil2.getAllContentsOfType(instanceModel.getContents().get(0), ConnectionInstance.class);
		for(ConnectionInstance c:cList)
		{
			String specifiedProtocol;
			if(c.getSource() instanceof FeatureInstance &&
					c.getDestination() instanceof FeatureInstance)
			{
				FeatureInstance srcFeatureInstance = (FeatureInstance) c.getSource();
				FeatureInstance dstFeatureInstance = (FeatureInstance) c.getDestination();
				// thread to thread
				if(srcFeatureInstance.getComponentInstance().getCategory().equals(ComponentCategory.THREAD)
						&& dstFeatureInstance.getComponentInstance().getCategory().equals(ComponentCategory.THREAD))
				{
					PropertyExpression pe = PropertyUtils.getPropertyValue("Actual_Connection_Binding", c);
					if(pe == null)
						continue;
					ListValue lvBus = (ListValue) pe;
					InstanceReferenceValue irvBus = (InstanceReferenceValue) lvBus.getOwnedListElements().get(0);
					ComponentInstance bus = (ComponentInstance) irvBus.getReferencedInstanceObject();
					if(!bus.getCategory().equals(ComponentCategory.BUS) && 
							!bus.getCategory().equals(ComponentCategory.VIRTUAL_BUS))
						continue;
					specifiedProtocol = PropertyUtils.getStringValue(bus, comPropertyName);
					if(protocol.equalsIgnoreCase(specifiedProtocol))
						return true;
					for(ConnectionInstance busCnxEnd: bus.getSrcConnectionInstances())
					{
						specifiedProtocol = PropertyUtils.getStringValue(busCnxEnd, comPropertyName);
						if(protocol.equalsIgnoreCase(specifiedProtocol))
							return true;
					}
					for(ConnectionInstance busCnxEnd: bus.getDstConnectionInstances())
					{
						specifiedProtocol = PropertyUtils.getStringValue(busCnxEnd, comPropertyName);
						if(protocol.equalsIgnoreCase(specifiedProtocol))
							return true;
					}
				}
				else
				{
					ComponentInstance vbInstance=null;
					// virtual bus to thread
					if(isBusOrVirtualBus(srcFeatureInstance.getComponentInstance())
							&& dstFeatureInstance.getComponentInstance().getCategory().equals(ComponentCategory.THREAD))
					{
						vbInstance = srcFeatureInstance.getComponentInstance();
					}
					// thread to virtual bus
					if(srcFeatureInstance.getComponentInstance().getCategory().equals(ComponentCategory.THREAD)
							&& isBusOrVirtualBus(dstFeatureInstance.getComponentInstance()))
					{
						vbInstance = dstFeatureInstance.getComponentInstance();
					}
					if(vbInstance==null)
						continue;
					specifiedProtocol = PropertyUtils.getStringValue(vbInstance, comPropertyName);
					if(protocol.equalsIgnoreCase(specifiedProtocol))
						return true;
					
				}
				
			}
			

		}
		return false;

	}

	private static boolean isBusOrVirtualBus(ComponentInstance componentInstance) {
		return componentInstance.getCategory().equals(ComponentCategory.VIRTUAL_BUS) || componentInstance.getCategory().equals(ComponentCategory.BUS);
	}
}
