/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transformation Resources Pair</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair#getModelTransformationTrace <em>Model Transformation Trace</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair#getOutputModelRoot <em>Output Model Root</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getTransformationResourcesPair()
 * @model
 * @generated
 */
public interface TransformationResourcesPair extends EObject {
	/**
	 * Returns the value of the '<em><b>Model Transformation Trace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Transformation Trace</em>' reference.
	 * @see #setModelTransformationTrace(ArchTraceSpec)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getTransformationResourcesPair_ModelTransformationTrace()
	 * @model
	 * @generated
	 */
	ArchTraceSpec getModelTransformationTrace();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair#getModelTransformationTrace <em>Model Transformation Trace</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Transformation Trace</em>' reference.
	 * @see #getModelTransformationTrace()
	 * @generated
	 */
	void setModelTransformationTrace(ArchTraceSpec value);

	/**
	 * Returns the value of the '<em><b>Output Model Root</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Model Root</em>' reference.
	 * @see #setOutputModelRoot(EObject)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getTransformationResourcesPair_OutputModelRoot()
	 * @model
	 * @generated
	 */
	EObject getOutputModelRoot();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair#getOutputModelRoot <em>Output Model Root</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Model Root</em>' reference.
	 * @see #getOutputModelRoot()
	 * @generated
	 */
	void setOutputModelRoot(EObject value);

} // TransformationResourcesPair
