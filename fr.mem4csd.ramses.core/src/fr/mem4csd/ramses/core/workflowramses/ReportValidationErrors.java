/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Report Validation Errors</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors#getValidationReportModelSlot <em>Validation Report Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors#getHasErrorModelSlot <em>Has Error Model Slot</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getReportValidationErrors()
 * @model
 * @generated
 */
public interface ReportValidationErrors extends WorkflowComponent {
	/**
	 * Returns the value of the '<em><b>Validation Report Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validation Report Model Slot</em>' attribute.
	 * @see #setValidationReportModelSlot(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getReportValidationErrors_ValidationReportModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getValidationReportModelSlot();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors#getValidationReportModelSlot <em>Validation Report Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Validation Report Model Slot</em>' attribute.
	 * @see #getValidationReportModelSlot()
	 * @generated
	 */
	void setValidationReportModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Has Error Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Error Model Slot</em>' attribute.
	 * @see #setHasErrorModelSlot(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getReportValidationErrors_HasErrorModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getHasErrorModelSlot();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors#getHasErrorModelSlot <em>Has Error Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Error Model Slot</em>' attribute.
	 * @see #getHasErrorModelSlot()
	 * @generated
	 */
	void setHasErrorModelSlot(String value);

} // ReportValidationErrors
