/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Clear Validation Errors</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.ClearValidationErrors#getValidationReportModelURI <em>Validation Report Model URI</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getClearValidationErrors()
 * @model
 * @generated
 */
public interface ClearValidationErrors extends WorkflowComponent {
	/**
	 * Returns the value of the '<em><b>Validation Report Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validation Report Model URI</em>' attribute.
	 * @see #setValidationReportModelURI(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getClearValidationErrors_ValidationReportModelURI()
	 * @model required="true"
	 * @generated
	 */
	String getValidationReportModelURI();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.ClearValidationErrors#getValidationReportModelURI <em>Validation Report Model URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Validation Report Model URI</em>' attribute.
	 * @see #getValidationReportModelURI()
	 * @generated
	 */
	void setValidationReportModelURI(String value);

} // ClearValidationErrors
