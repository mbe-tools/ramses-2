/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition Evaluation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation#getInstanceModelSlot <em>Instance Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation#getResultModelSlot <em>Result Model Slot</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getConditionEvaluation()
 * @model abstract="true"
 * @generated
 */
public interface ConditionEvaluation extends WorkflowComponent {
	/**
	 * Returns the value of the '<em><b>Instance Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance Model Slot</em>' attribute.
	 * @see #setInstanceModelSlot(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getConditionEvaluation_InstanceModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getInstanceModelSlot();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation#getInstanceModelSlot <em>Instance Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance Model Slot</em>' attribute.
	 * @see #getInstanceModelSlot()
	 * @generated
	 */
	void setInstanceModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Result Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result Model Slot</em>' attribute.
	 * @see #setResultModelSlot(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getConditionEvaluation_ResultModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getResultModelSlot();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation#getResultModelSlot <em>Result Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Model Slot</em>' attribute.
	 * @see #getResultModelSlot()
	 * @generated
	 */
	void setResultModelSlot(String value);

} // ConditionEvaluation
