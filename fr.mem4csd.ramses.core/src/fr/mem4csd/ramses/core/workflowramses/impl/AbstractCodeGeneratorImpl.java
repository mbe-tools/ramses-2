/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;

import fr.mem4csd.ramses.core.workflowramses.AbstractCodeGenerator;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Code Generator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.AbstractCodeGeneratorImpl#getCurrentModelTransformationTrace <em>Current Model Transformation Trace</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractCodeGeneratorImpl extends MinimalEObjectImpl.Container implements AbstractCodeGenerator {
	/**
	 * The cached value of the '{@link #getCurrentModelTransformationTrace() <em>Current Model Transformation Trace</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentModelTransformationTrace()
	 * @generated
	 * @ordered
	 */
	protected ArchTraceSpec currentModelTransformationTrace;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractCodeGeneratorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesPackage.Literals.ABSTRACT_CODE_GENERATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ArchTraceSpec getCurrentModelTransformationTrace() {
		if (currentModelTransformationTrace != null && currentModelTransformationTrace.eIsProxy()) {
			InternalEObject oldCurrentModelTransformationTrace = (InternalEObject)currentModelTransformationTrace;
			currentModelTransformationTrace = (ArchTraceSpec)eResolveProxy(oldCurrentModelTransformationTrace);
			if (currentModelTransformationTrace != oldCurrentModelTransformationTrace) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WorkflowramsesPackage.ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE, oldCurrentModelTransformationTrace, currentModelTransformationTrace));
			}
		}
		return currentModelTransformationTrace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArchTraceSpec basicGetCurrentModelTransformationTrace() {
		return currentModelTransformationTrace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCurrentModelTransformationTrace(ArchTraceSpec newCurrentModelTransformationTrace) {
		ArchTraceSpec oldCurrentModelTransformationTrace = currentModelTransformationTrace;
		currentModelTransformationTrace = newCurrentModelTransformationTrace;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE, oldCurrentModelTransformationTrace, currentModelTransformationTrace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowramsesPackage.ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE:
				if (resolve) return getCurrentModelTransformationTrace();
				return basicGetCurrentModelTransformationTrace();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowramsesPackage.ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE:
				setCurrentModelTransformationTrace((ArchTraceSpec)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE:
				setCurrentModelTransformationTrace((ArchTraceSpec)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE:
				return currentModelTransformationTrace != null;
		}
		return super.eIsSet(featureID);
	}

} //AbstractCodeGeneratorImpl
