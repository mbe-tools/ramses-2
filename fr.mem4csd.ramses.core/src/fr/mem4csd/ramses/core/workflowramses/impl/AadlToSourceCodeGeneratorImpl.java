/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import fr.mem4csd.ramses.core.codegen.GenerationException;

import fr.mem4csd.ramses.core.workflowramses.AadlToSourceCodeGenerator;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EClass;

import org.osate.aadl2.Element;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aadl To Source Code Generator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AadlToSourceCodeGeneratorImpl extends AbstractCodeGeneratorImpl implements AadlToSourceCodeGenerator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AadlToSourceCodeGeneratorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesPackage.Literals.AADL_TO_SOURCE_CODE_GENERATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void generateSourceCode(Element element, URI systemOutputDir, IProgressMonitor monitor) throws GenerationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorkflowramsesPackage.AADL_TO_SOURCE_CODE_GENERATOR___GENERATE_SOURCE_CODE__ELEMENT_URI_IPROGRESSMONITOR:
				try {
					generateSourceCode((Element)arguments.get(0), (URI)arguments.get(1), (IProgressMonitor)arguments.get(2));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
		}
		return super.eInvoke(operationID, arguments);
	}

} //AadlToSourceCodeGeneratorImpl
