/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import fr.mem4csd.ramses.core.codegen.GenerationException;

import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;
import fr.mem4csd.ramses.core.workflowramses.Codegen;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.osate.aadl2.ProcessSubcomponent;
import org.osate.aadl2.Subcomponent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aadl To Target Build Generator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.AadlToTargetBuildGeneratorImpl#getCodeGenWorkflowComponent <em>Code Gen Workflow Component</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AadlToTargetBuildGeneratorImpl extends AbstractCodeGeneratorImpl implements AadlToTargetBuildGenerator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AadlToTargetBuildGeneratorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesPackage.Literals.AADL_TO_TARGET_BUILD_GENERATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Codegen getCodeGenWorkflowComponent() {
		if (eContainerFeatureID() != WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT) return null;
		return (Codegen)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCodeGenWorkflowComponent(Codegen newCodeGenWorkflowComponent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCodeGenWorkflowComponent, WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCodeGenWorkflowComponent(Codegen newCodeGenWorkflowComponent) {
		if (newCodeGenWorkflowComponent != eInternalContainer() || (eContainerFeatureID() != WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT && newCodeGenWorkflowComponent != null)) {
			if (EcoreUtil.isAncestor(this, newCodeGenWorkflowComponent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCodeGenWorkflowComponent != null)
				msgs = ((InternalEObject)newCodeGenWorkflowComponent).eInverseAdd(this, WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD, Codegen.class, msgs);
			msgs = basicSetCodeGenWorkflowComponent(newCodeGenWorkflowComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT, newCodeGenWorkflowComponent, newCodeGenWorkflowComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void generateSystemBuild(EList<Subcomponent> processor, URI systemOutputDir, IProgressMonitor monitor) throws GenerationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void generateProcessorBuild(Subcomponent processor, URI processorOutputDir, IProgressMonitor monitor) throws GenerationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void generateProcessBuild(ProcessSubcomponent process, URI processOutputDir, IProgressMonitor monitor) throws GenerationException {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean validateTargetPath(URI targetPath) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTargetShortDescription() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getTargetName() {
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void initializeTargetBuilder() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCodeGenWorkflowComponent((Codegen)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT:
				return basicSetCodeGenWorkflowComponent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT:
				return eInternalContainer().eInverseRemove(this, WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD, Codegen.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT:
				return getCodeGenWorkflowComponent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT:
				setCodeGenWorkflowComponent((Codegen)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT:
				setCodeGenWorkflowComponent((Codegen)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT:
				return getCodeGenWorkflowComponent() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_SYSTEM_BUILD__ELIST_URI_IPROGRESSMONITOR:
				try {
					generateSystemBuild((EList<Subcomponent>)arguments.get(0), (URI)arguments.get(1), (IProgressMonitor)arguments.get(2));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_PROCESSOR_BUILD__SUBCOMPONENT_URI_IPROGRESSMONITOR:
				try {
					generateProcessorBuild((Subcomponent)arguments.get(0), (URI)arguments.get(1), (IProgressMonitor)arguments.get(2));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_PROCESS_BUILD__PROCESSSUBCOMPONENT_URI_IPROGRESSMONITOR:
				try {
					generateProcessBuild((ProcessSubcomponent)arguments.get(0), (URI)arguments.get(1), (IProgressMonitor)arguments.get(2));
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR___VALIDATE_TARGET_PATH__URI:
				return validateTargetPath((URI)arguments.get(0));
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR___GET_TARGET_SHORT_DESCRIPTION:
				return getTargetShortDescription();
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR___GET_TARGET_NAME:
				return getTargetName();
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR___INITIALIZE_TARGET_BUILDER:
				initializeTargetBuilder();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //AadlToTargetBuildGeneratorImpl
