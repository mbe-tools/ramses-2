/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import fr.mem4csd.ramses.core.util.RemoteConnectionsWorkflowUtils;
import fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationProtocol;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Condition Evaluation Protocol</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationProtocolImpl#getProtocols <em>Protocols</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConditionEvaluationProtocolImpl extends ConditionEvaluationImpl implements ConditionEvaluationProtocol {
	/**
	 * The default value of the '{@link #getProtocols() <em>Protocols</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocols()
	 * @generated
	 * @ordered
	 */
	protected static final String PROTOCOLS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProtocols() <em>Protocols</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocols()
	 * @generated
	 * @ordered
	 */
	protected String protocols = PROTOCOLS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionEvaluationProtocolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesPackage.Literals.CONDITION_EVALUATION_PROTOCOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProtocols() {
		return protocols;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProtocols(String newProtocols) {
		String oldProtocols = protocols;
		protocols = newProtocols;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CONDITION_EVALUATION_PROTOCOL__PROTOCOLS, oldProtocols, protocols));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowramsesPackage.CONDITION_EVALUATION_PROTOCOL__PROTOCOLS:
				return getProtocols();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowramsesPackage.CONDITION_EVALUATION_PROTOCOL__PROTOCOLS:
				setProtocols((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.CONDITION_EVALUATION_PROTOCOL__PROTOCOLS:
				setProtocols(PROTOCOLS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.CONDITION_EVALUATION_PROTOCOL__PROTOCOLS:
				return PROTOCOLS_EDEFAULT == null ? protocols != null : !PROTOCOLS_EDEFAULT.equals(protocols);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (protocols: ");
		result.append(protocols);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void execute (final WorkflowExecutionContext context,
			final IProgressMonitor monitor) throws WorkflowExecutionException, IOException {
		
		EMap<String, Object> modelSlots = context.getModelSlots();
		
		
		Resource inputModel = ((EObject) modelSlots.get(instanceModelSlot)).eResource();
		Boolean hasRemoteConnections = (Boolean) modelSlots.get(this.resultModelSlot);
		hasRemoteConnections = false;
		
		String[] protocolsArray = protocols.split(",");
		for(String protocol: protocolsArray)
		{
			hasRemoteConnections |= 
				RemoteConnectionsWorkflowUtils.hasRemoteConnectionOfProtocol(inputModel, protocol);
			if(hasRemoteConnections)
				break;
		}
		
		modelSlots.put(resultModelSlot, hasRemoteConnections);
		
		System.out.println("Model has remote connections: "+hasRemoteConnections);
		
	}
	
} //ConditionEvaluationProtocolImpl
