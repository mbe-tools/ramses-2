/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;

import fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transformation Resources Pair</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.TransformationResourcesPairImpl#getModelTransformationTrace <em>Model Transformation Trace</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.TransformationResourcesPairImpl#getOutputModelRoot <em>Output Model Root</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransformationResourcesPairImpl extends MinimalEObjectImpl.Container implements TransformationResourcesPair {
	/**
	 * The cached value of the '{@link #getModelTransformationTrace() <em>Model Transformation Trace</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelTransformationTrace()
	 * @generated
	 * @ordered
	 */
	protected ArchTraceSpec modelTransformationTrace;

	/**
	 * The cached value of the '{@link #getOutputModelRoot() <em>Output Model Root</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputModelRoot()
	 * @generated
	 * @ordered
	 */
	protected EObject outputModelRoot;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransformationResourcesPairImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesPackage.Literals.TRANSFORMATION_RESOURCES_PAIR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ArchTraceSpec getModelTransformationTrace() {
		if (modelTransformationTrace != null && modelTransformationTrace.eIsProxy()) {
			InternalEObject oldModelTransformationTrace = (InternalEObject)modelTransformationTrace;
			modelTransformationTrace = (ArchTraceSpec)eResolveProxy(oldModelTransformationTrace);
			if (modelTransformationTrace != oldModelTransformationTrace) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__MODEL_TRANSFORMATION_TRACE, oldModelTransformationTrace, modelTransformationTrace));
			}
		}
		return modelTransformationTrace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArchTraceSpec basicGetModelTransformationTrace() {
		return modelTransformationTrace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setModelTransformationTrace(ArchTraceSpec newModelTransformationTrace) {
		ArchTraceSpec oldModelTransformationTrace = modelTransformationTrace;
		modelTransformationTrace = newModelTransformationTrace;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__MODEL_TRANSFORMATION_TRACE, oldModelTransformationTrace, modelTransformationTrace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject getOutputModelRoot() {
		if (outputModelRoot != null && outputModelRoot.eIsProxy()) {
			InternalEObject oldOutputModelRoot = (InternalEObject)outputModelRoot;
			outputModelRoot = eResolveProxy(oldOutputModelRoot);
			if (outputModelRoot != oldOutputModelRoot) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__OUTPUT_MODEL_ROOT, oldOutputModelRoot, outputModelRoot));
			}
		}
		return outputModelRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetOutputModelRoot() {
		return outputModelRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOutputModelRoot(EObject newOutputModelRoot) {
		EObject oldOutputModelRoot = outputModelRoot;
		outputModelRoot = newOutputModelRoot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__OUTPUT_MODEL_ROOT, oldOutputModelRoot, outputModelRoot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__MODEL_TRANSFORMATION_TRACE:
				if (resolve) return getModelTransformationTrace();
				return basicGetModelTransformationTrace();
			case WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__OUTPUT_MODEL_ROOT:
				if (resolve) return getOutputModelRoot();
				return basicGetOutputModelRoot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__MODEL_TRANSFORMATION_TRACE:
				setModelTransformationTrace((ArchTraceSpec)newValue);
				return;
			case WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__OUTPUT_MODEL_ROOT:
				setOutputModelRoot((EObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__MODEL_TRANSFORMATION_TRACE:
				setModelTransformationTrace((ArchTraceSpec)null);
				return;
			case WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__OUTPUT_MODEL_ROOT:
				setOutputModelRoot((EObject)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__MODEL_TRANSFORMATION_TRACE:
				return modelTransformationTrace != null;
			case WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR__OUTPUT_MODEL_ROOT:
				return outputModelRoot != null;
		}
		return super.eIsSet(featureID);
	}

} //TransformationResourcesPairImpl
