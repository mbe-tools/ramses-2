/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import java.io.IOException;
import java.util.Collection;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.codegen.utils.Names;
import fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Report Validation Errors</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.ReportValidationErrorsImpl#getValidationReportModelSlot <em>Validation Report Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.ReportValidationErrorsImpl#getHasErrorModelSlot <em>Has Error Model Slot</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReportValidationErrorsImpl extends WorkflowComponentImpl implements ReportValidationErrors {
	/**
	 * The default value of the '{@link #getValidationReportModelSlot() <em>Validation Report Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidationReportModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String VALIDATION_REPORT_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValidationReportModelSlot() <em>Validation Report Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidationReportModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String validationReportModelSlot = VALIDATION_REPORT_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getHasErrorModelSlot() <em>Has Error Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasErrorModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String HAS_ERROR_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHasErrorModelSlot() <em>Has Error Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasErrorModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String hasErrorModelSlot = HAS_ERROR_MODEL_SLOT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReportValidationErrorsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesPackage.Literals.REPORT_VALIDATION_ERRORS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValidationReportModelSlot() {
		return validationReportModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValidationReportModelSlot(String newValidationReportModelSlot) {
		String oldValidationReportModelSlot = validationReportModelSlot;
		validationReportModelSlot = newValidationReportModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.REPORT_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_SLOT, oldValidationReportModelSlot, validationReportModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getHasErrorModelSlot() {
		return hasErrorModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHasErrorModelSlot(String newHasErrorModelSlot) {
		String oldHasErrorModelSlot = hasErrorModelSlot;
		hasErrorModelSlot = newHasErrorModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.REPORT_VALIDATION_ERRORS__HAS_ERROR_MODEL_SLOT, oldHasErrorModelSlot, hasErrorModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowramsesPackage.REPORT_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_SLOT:
				return getValidationReportModelSlot();
			case WorkflowramsesPackage.REPORT_VALIDATION_ERRORS__HAS_ERROR_MODEL_SLOT:
				return getHasErrorModelSlot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowramsesPackage.REPORT_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_SLOT:
				setValidationReportModelSlot((String)newValue);
				return;
			case WorkflowramsesPackage.REPORT_VALIDATION_ERRORS__HAS_ERROR_MODEL_SLOT:
				setHasErrorModelSlot((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.REPORT_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_SLOT:
				setValidationReportModelSlot(VALIDATION_REPORT_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowramsesPackage.REPORT_VALIDATION_ERRORS__HAS_ERROR_MODEL_SLOT:
				setHasErrorModelSlot(HAS_ERROR_MODEL_SLOT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.REPORT_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_SLOT:
				return VALIDATION_REPORT_MODEL_SLOT_EDEFAULT == null ? validationReportModelSlot != null : !VALIDATION_REPORT_MODEL_SLOT_EDEFAULT.equals(validationReportModelSlot);
			case WorkflowramsesPackage.REPORT_VALIDATION_ERRORS__HAS_ERROR_MODEL_SLOT:
				return HAS_ERROR_MODEL_SLOT_EDEFAULT == null ? hasErrorModelSlot != null : !HAS_ERROR_MODEL_SLOT_EDEFAULT.equals(hasErrorModelSlot);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (validationReportModelSlot: ");
		result.append(validationReportModelSlot);
		result.append(", hasErrorModelSlot: ");
		result.append(hasErrorModelSlot);
		result.append(')');
		return result.toString();
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void execute(	final WorkflowExecutionContext context,
							final IProgressMonitor monitor )
	throws WorkflowExecutionException, IOException {
		final EMap<String, Object> modelSlots = context.getModelSlots();
		final Object obj = modelSlots.get(getValidationReportModelSlot());
		final Collection<EObject> errors;
		
		if ( obj instanceof Collection<?> ) {
			errors = (Collection<EObject>) obj;
		}
		else {
			final Resource errTransRes = ( (EObject) obj ).eResource();
			errors = errTransRes.getContents();
		}
		
		boolean hasErrors = false;

		for ( final EObject err : errors ) {
			if ( err instanceof fr.mem4csd.ramses.core.validation_report.Error ) {
				hasErrors = true;
				
				if ( Platform.isRunning() ) {
					final fr.mem4csd.ramses.core.validation_report.Error validationError = (fr.mem4csd.ramses.core.validation_report.Error) err;
					
					try {
						createMarker( validationError );
					}
					catch ( final CoreException ex ) {
						throw new IOException( ex );
					}
				}
			}
		}
		
		modelSlots.put( getHasErrorModelSlot(), hasErrors );
		
		if ( hasErrors ) {
			throw new WorkflowExecutionException( "The input AADL model is not valid for RAMSES code generation. Please see errors in the problems view." );
		}
	}
	
	private void createMarker( final fr.mem4csd.ramses.core.validation_report.Error validationError ) 
	throws CoreException {
		final EObject element = validationError.getObject();
		final Resource res = element.eResource();
		final IFile file = OsateResourceUtil.toIFile( res.getURI() );

		if ( file.exists() ) {
			final IMarker marker = file.createMarker( Names.RAMSES_CODGEN_PROBLEM_MARKER );
			marker.setAttribute( IMarker.MESSAGE, validationError.getMessage() );
			marker.setAttribute( IMarker.SEVERITY, IMarker.SEVERITY_ERROR );
			marker.setAttribute( EValidator.URI_ATTRIBUTE, EcoreUtil.getURI( element ).toString() );
		    final ICompositeNode node = NodeModelUtils.findActualNodeFor( element );
		    
		    if ( node != null ) {
		    	final int line = node.getStartLine();
		    	
		    	if ( line > -1 ) {
		    		marker.setAttribute(IMarker.LINE_NUMBER, line );
		    	}
			}
		}
	}
} //ReportValidationErrorsImpl
