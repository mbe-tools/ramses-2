/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import com.google.common.base.Splitter;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.mdelab.workflow.util.WorkflowUtil;
import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.codegen.AadlTargetSpecificCodeGenerator;
import fr.mem4csd.ramses.core.codegen.AbstractAadlToCMakefileUnparser;
import fr.mem4csd.ramses.core.codegen.GenerationException;
import fr.mem4csd.ramses.core.codegen.c.AadlToCUnparser;
import fr.mem4csd.ramses.core.codegen.utils.FileUtils;
import fr.mem4csd.ramses.core.workflowramses.AadlToSourceCodeGenerator;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator;
import fr.mem4csd.ramses.core.workflowramses.Codegen;
import fr.mem4csd.ramses.core.workflowramses.TargetProperties;
import fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Codegen</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#isDebugOutput <em>Debug Output</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getAadlModelSlot <em>Aadl Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getTraceModelSlot <em>Trace Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getOutputDirectory <em>Output Directory</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getTargetInstallDir <em>Target Install Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getIncludeDir <em>Include Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getCoreRuntimeDir <em>Core Runtime Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getTargetRuntimeDir <em>Target Runtime Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getAadlToSourceCode <em>Aadl To Source Code</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getAadlToTargetConfiguration <em>Aadl To Target Configuration</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getAadlToTargetBuild <em>Aadl To Target Build</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getTransformationResourcesPairList <em>Transformation Resources Pair List</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getTargetProperties <em>Target Properties</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getProgressMonitor <em>Progress Monitor</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getUriConverter <em>Uri Converter</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getTargetInstallDirectoryURI <em>Target Install Directory URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getOutputDirectoryURI <em>Output Directory URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getCoreRuntimeDirectoryURI <em>Core Runtime Directory URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getTargetRuntimeDirectoryURI <em>Target Runtime Directory URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl#getIncludeDirectoryURIList <em>Include Directory URI List</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class CodegenImpl extends WorkflowComponentImpl implements Codegen {
	/**
	 * The default value of the '{@link #isDebugOutput() <em>Debug Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDebugOutput()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEBUG_OUTPUT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDebugOutput() <em>Debug Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDebugOutput()
	 * @generated
	 * @ordered
	 */
	protected boolean debugOutput = DEBUG_OUTPUT_EDEFAULT;

	/**
	 * The default value of the '{@link #getAadlModelSlot() <em>Aadl Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAadlModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String AADL_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAadlModelSlot() <em>Aadl Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAadlModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String aadlModelSlot = AADL_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getTraceModelSlot() <em>Trace Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTraceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String TRACE_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTraceModelSlot() <em>Trace Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTraceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String traceModelSlot = TRACE_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutputDirectory() <em>Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDirectory()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_DIRECTORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutputDirectory() <em>Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDirectory()
	 * @generated
	 * @ordered
	 */
	protected String outputDirectory = OUTPUT_DIRECTORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetInstallDir() <em>Target Install Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetInstallDir()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_INSTALL_DIR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetInstallDir() <em>Target Install Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetInstallDir()
	 * @generated
	 * @ordered
	 */
	protected String targetInstallDir = TARGET_INSTALL_DIR_EDEFAULT;

	/**
	 * The default value of the '{@link #getIncludeDir() <em>Include Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludeDir()
	 * @generated
	 * @ordered
	 */
	protected static final String INCLUDE_DIR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIncludeDir() <em>Include Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludeDir()
	 * @generated
	 * @ordered
	 */
	protected String includeDir = INCLUDE_DIR_EDEFAULT;

	/**
	 * The default value of the '{@link #getCoreRuntimeDir() <em>Core Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoreRuntimeDir()
	 * @generated
	 * @ordered
	 */
	protected static final String CORE_RUNTIME_DIR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCoreRuntimeDir() <em>Core Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoreRuntimeDir()
	 * @generated
	 * @ordered
	 */
	protected String coreRuntimeDir = CORE_RUNTIME_DIR_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetRuntimeDir() <em>Target Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetRuntimeDir()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_RUNTIME_DIR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetRuntimeDir() <em>Target Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetRuntimeDir()
	 * @generated
	 * @ordered
	 */
	protected String targetRuntimeDir = TARGET_RUNTIME_DIR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAadlToSourceCode() <em>Aadl To Source Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAadlToSourceCode()
	 * @generated
	 * @ordered
	 */
	protected AadlToSourceCodeGenerator aadlToSourceCode;

	/**
	 * The cached value of the '{@link #getAadlToTargetConfiguration() <em>Aadl To Target Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAadlToTargetConfiguration()
	 * @generated
	 * @ordered
	 */
	protected AadlToTargetConfigurationGenerator aadlToTargetConfiguration;

	/**
	 * The cached value of the '{@link #getAadlToTargetBuild() <em>Aadl To Target Build</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAadlToTargetBuild()
	 * @generated
	 * @ordered
	 */
	protected AadlToTargetBuildGenerator aadlToTargetBuild;

	/**
	 * The cached value of the '{@link #getTransformationResourcesPairList() <em>Transformation Resources Pair List</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransformationResourcesPairList()
	 * @generated
	 * @ordered
	 */
	protected EList<TransformationResourcesPair> transformationResourcesPairList;

	/**
	 * The cached value of the '{@link #getTargetProperties() <em>Target Properties</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetProperties()
	 * @generated
	 * @ordered
	 */
	protected TargetProperties targetProperties;

	/**
	 * The default value of the '{@link #getProgressMonitor() <em>Progress Monitor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgressMonitor()
	 * @generated
	 * @ordered
	 */
	protected static final IProgressMonitor PROGRESS_MONITOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProgressMonitor() <em>Progress Monitor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgressMonitor()
	 * @generated
	 * @ordered
	 */
	protected IProgressMonitor progressMonitor = PROGRESS_MONITOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getUriConverter() <em>Uri Converter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUriConverter()
	 * @generated
	 * @ordered
	 */
	protected static final URIConverter URI_CONVERTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUriConverter() <em>Uri Converter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUriConverter()
	 * @generated
	 * @ordered
	 */
	protected URIConverter uriConverter = URI_CONVERTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetInstallDirectoryURI() <em>Target Install Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetInstallDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected static final URI TARGET_INSTALL_DIRECTORY_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetInstallDirectoryURI() <em>Target Install Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetInstallDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected URI targetInstallDirectoryURI = TARGET_INSTALL_DIRECTORY_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutputDirectoryURI() <em>Output Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected static final URI OUTPUT_DIRECTORY_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutputDirectoryURI() <em>Output Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected URI outputDirectoryURI = OUTPUT_DIRECTORY_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getCoreRuntimeDirectoryURI() <em>Core Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoreRuntimeDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected static final URI CORE_RUNTIME_DIRECTORY_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCoreRuntimeDirectoryURI() <em>Core Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoreRuntimeDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected URI coreRuntimeDirectoryURI = CORE_RUNTIME_DIRECTORY_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetRuntimeDirectoryURI() <em>Target Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetRuntimeDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected static final URI TARGET_RUNTIME_DIRECTORY_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetRuntimeDirectoryURI() <em>Target Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetRuntimeDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected URI targetRuntimeDirectoryURI = TARGET_RUNTIME_DIRECTORY_URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIncludeDirectoryURIList() <em>Include Directory URI List</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludeDirectoryURIList()
	 * @generated
	 * @ordered
	 */
	protected EList<URI> includeDirectoryURIList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodegenImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesPackage.Literals.CODEGEN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isDebugOutput() {
		return debugOutput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDebugOutput(boolean newDebugOutput) {
		boolean oldDebugOutput = debugOutput;
		debugOutput = newDebugOutput;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__DEBUG_OUTPUT, oldDebugOutput, debugOutput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getAadlModelSlot() {
		return aadlModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAadlModelSlot(String newAadlModelSlot) {
		String oldAadlModelSlot = aadlModelSlot;
		aadlModelSlot = newAadlModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__AADL_MODEL_SLOT, oldAadlModelSlot, aadlModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTraceModelSlot() {
		return traceModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTraceModelSlot(String newTraceModelSlot) {
		String oldTraceModelSlot = traceModelSlot;
		traceModelSlot = newTraceModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__TRACE_MODEL_SLOT, oldTraceModelSlot, traceModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOutputDirectory() {
		return outputDirectory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOutputDirectory(String newOutputDirectory) {
		String oldOutputDirectory = outputDirectory;
		outputDirectory = newOutputDirectory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY, oldOutputDirectory, outputDirectory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTargetInstallDir() {
		return targetInstallDir;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetInstallDir(String newTargetInstallDir) {
		String oldTargetInstallDir = targetInstallDir;
		targetInstallDir = newTargetInstallDir;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIR, oldTargetInstallDir, targetInstallDir));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIncludeDir() {
		return includeDir;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIncludeDir(String newIncludeDir) {
		String oldIncludeDir = includeDir;
		includeDir = newIncludeDir;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__INCLUDE_DIR, oldIncludeDir, includeDir));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCoreRuntimeDir() {
		return coreRuntimeDir;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCoreRuntimeDir(String newCoreRuntimeDir) {
		String oldCoreRuntimeDir = coreRuntimeDir;
		coreRuntimeDir = newCoreRuntimeDir;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIR, oldCoreRuntimeDir, coreRuntimeDir));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTargetRuntimeDir() {
		return targetRuntimeDir;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetRuntimeDir(String newTargetRuntimeDir) {
		String oldTargetRuntimeDir = targetRuntimeDir;
		targetRuntimeDir = newTargetRuntimeDir;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIR, oldTargetRuntimeDir, targetRuntimeDir));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getTargetName() {
		if(aadlToTargetBuild!=null)
			return aadlToTargetBuild.getTargetName();
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public AadlToSourceCodeGenerator getAadlToSourceCode() {
		if(aadlToSourceCode == null)
			setAadlToSourceCode(AadlToCUnparser.getAadlToCUnparser());
		return aadlToSourceCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAadlToSourceCode(AadlToSourceCodeGenerator newAadlToSourceCode, NotificationChain msgs) {
		AadlToSourceCodeGenerator oldAadlToSourceCode = aadlToSourceCode;
		aadlToSourceCode = newAadlToSourceCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__AADL_TO_SOURCE_CODE, oldAadlToSourceCode, newAadlToSourceCode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAadlToSourceCode(AadlToSourceCodeGenerator newAadlToSourceCode) {
		if (newAadlToSourceCode != aadlToSourceCode) {
			NotificationChain msgs = null;
			if (aadlToSourceCode != null)
				msgs = ((InternalEObject)aadlToSourceCode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WorkflowramsesPackage.CODEGEN__AADL_TO_SOURCE_CODE, null, msgs);
			if (newAadlToSourceCode != null)
				msgs = ((InternalEObject)newAadlToSourceCode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WorkflowramsesPackage.CODEGEN__AADL_TO_SOURCE_CODE, null, msgs);
			msgs = basicSetAadlToSourceCode(newAadlToSourceCode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__AADL_TO_SOURCE_CODE, newAadlToSourceCode, newAadlToSourceCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AadlToTargetConfigurationGenerator getAadlToTargetConfiguration() {
		return aadlToTargetConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAadlToTargetConfiguration(AadlToTargetConfigurationGenerator newAadlToTargetConfiguration, NotificationChain msgs) {
		AadlToTargetConfigurationGenerator oldAadlToTargetConfiguration = aadlToTargetConfiguration;
		aadlToTargetConfiguration = newAadlToTargetConfiguration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_CONFIGURATION, oldAadlToTargetConfiguration, newAadlToTargetConfiguration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAadlToTargetConfiguration(AadlToTargetConfigurationGenerator newAadlToTargetConfiguration) {
		if (newAadlToTargetConfiguration != aadlToTargetConfiguration) {
			NotificationChain msgs = null;
			if (aadlToTargetConfiguration != null)
				msgs = ((InternalEObject)aadlToTargetConfiguration).eInverseRemove(this, WorkflowramsesPackage.AADL_TO_TARGET_CONFIGURATION_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT, AadlToTargetConfigurationGenerator.class, msgs);
			if (newAadlToTargetConfiguration != null)
				msgs = ((InternalEObject)newAadlToTargetConfiguration).eInverseAdd(this, WorkflowramsesPackage.AADL_TO_TARGET_CONFIGURATION_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT, AadlToTargetConfigurationGenerator.class, msgs);
			msgs = basicSetAadlToTargetConfiguration(newAadlToTargetConfiguration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_CONFIGURATION, newAadlToTargetConfiguration, newAadlToTargetConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AadlToTargetBuildGenerator getAadlToTargetBuild() {
		return aadlToTargetBuild;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAadlToTargetBuild(AadlToTargetBuildGenerator newAadlToTargetBuild, NotificationChain msgs) {
		AadlToTargetBuildGenerator oldAadlToTargetBuild = aadlToTargetBuild;
		aadlToTargetBuild = newAadlToTargetBuild;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD, oldAadlToTargetBuild, newAadlToTargetBuild);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAadlToTargetBuild(AadlToTargetBuildGenerator newAadlToTargetBuild) {
		if (newAadlToTargetBuild != aadlToTargetBuild) {
			NotificationChain msgs = null;
			if (aadlToTargetBuild != null)
				msgs = ((InternalEObject)aadlToTargetBuild).eInverseRemove(this, WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT, AadlToTargetBuildGenerator.class, msgs);
			if (newAadlToTargetBuild != null)
				msgs = ((InternalEObject)newAadlToTargetBuild).eInverseAdd(this, WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT, AadlToTargetBuildGenerator.class, msgs);
			msgs = basicSetAadlToTargetBuild(newAadlToTargetBuild, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD, newAadlToTargetBuild, newAadlToTargetBuild));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TransformationResourcesPair> getTransformationResourcesPairList() {
		if (transformationResourcesPairList == null) {
			transformationResourcesPairList = new EObjectResolvingEList<TransformationResourcesPair>(TransformationResourcesPair.class, this, WorkflowramsesPackage.CODEGEN__TRANSFORMATION_RESOURCES_PAIR_LIST);
		}
		return transformationResourcesPairList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetProperties getTargetProperties() {
		if (targetProperties != null && targetProperties.eIsProxy()) {
			InternalEObject oldTargetProperties = (InternalEObject)targetProperties;
			targetProperties = (TargetProperties)eResolveProxy(oldTargetProperties);
			if (targetProperties != oldTargetProperties) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WorkflowramsesPackage.CODEGEN__TARGET_PROPERTIES, oldTargetProperties, targetProperties));
			}
		}
		return targetProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TargetProperties basicGetTargetProperties() {
		return targetProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetProperties(TargetProperties newTargetProperties) {
		TargetProperties oldTargetProperties = targetProperties;
		targetProperties = newTargetProperties;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__TARGET_PROPERTIES, oldTargetProperties, targetProperties));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IProgressMonitor getProgressMonitor() {
		return progressMonitor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProgressMonitor(IProgressMonitor newProgressMonitor) {
		IProgressMonitor oldProgressMonitor = progressMonitor;
		progressMonitor = newProgressMonitor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__PROGRESS_MONITOR, oldProgressMonitor, progressMonitor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public URIConverter getUriConverter() {
		return uriConverter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUriConverter(URIConverter newUriConverter) {
		URIConverter oldUriConverter = uriConverter;
		uriConverter = newUriConverter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__URI_CONVERTER, oldUriConverter, uriConverter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public URI getTargetInstallDirectoryURI() {
		return targetInstallDirectoryURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetInstallDirectoryURI(URI newTargetInstallDirectoryURI) {
		URI oldTargetInstallDirectoryURI = targetInstallDirectoryURI;
		targetInstallDirectoryURI = newTargetInstallDirectoryURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIRECTORY_URI, oldTargetInstallDirectoryURI, targetInstallDirectoryURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public URI getOutputDirectoryURI() {
		return outputDirectoryURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOutputDirectoryURI(URI newOutputDirectoryURI) {
		URI oldOutputDirectoryURI = outputDirectoryURI;
		outputDirectoryURI = newOutputDirectoryURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY_URI, oldOutputDirectoryURI, outputDirectoryURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public URI getCoreRuntimeDirectoryURI() {
		return coreRuntimeDirectoryURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCoreRuntimeDirectoryURI(URI newCoreRuntimeDirectoryURI) {
		URI oldCoreRuntimeDirectoryURI = coreRuntimeDirectoryURI;
		coreRuntimeDirectoryURI = newCoreRuntimeDirectoryURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIRECTORY_URI, oldCoreRuntimeDirectoryURI, coreRuntimeDirectoryURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public URI getTargetRuntimeDirectoryURI() {
		return targetRuntimeDirectoryURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetRuntimeDirectoryURI(URI newTargetRuntimeDirectoryURI) {
		URI oldTargetRuntimeDirectoryURI = targetRuntimeDirectoryURI;
		targetRuntimeDirectoryURI = newTargetRuntimeDirectoryURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIRECTORY_URI, oldTargetRuntimeDirectoryURI, targetRuntimeDirectoryURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<URI> getIncludeDirectoryURIList() {
		if (includeDirectoryURIList == null) {
			includeDirectoryURIList = new EDataTypeUniqueEList<URI>(URI.class, this, WorkflowramsesPackage.CODEGEN__INCLUDE_DIRECTORY_URI_LIST);
		}
		return includeDirectoryURIList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<ArchTraceSpec> getModelTransformationTracesList() {
		final EList<ArchTraceSpec> result = new BasicEList<ArchTraceSpec>();
		for(TransformationResourcesPair pair : this.transformationResourcesPairList)
		{
			result.add(pair.getModelTransformationTrace());
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_CONFIGURATION:
				if (aadlToTargetConfiguration != null)
					msgs = ((InternalEObject)aadlToTargetConfiguration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_CONFIGURATION, null, msgs);
				return basicSetAadlToTargetConfiguration((AadlToTargetConfigurationGenerator)otherEnd, msgs);
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD:
				if (aadlToTargetBuild != null)
					msgs = ((InternalEObject)aadlToTargetBuild).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD, null, msgs);
				return basicSetAadlToTargetBuild((AadlToTargetBuildGenerator)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorkflowramsesPackage.CODEGEN__AADL_TO_SOURCE_CODE:
				return basicSetAadlToSourceCode(null, msgs);
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_CONFIGURATION:
				return basicSetAadlToTargetConfiguration(null, msgs);
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD:
				return basicSetAadlToTargetBuild(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowramsesPackage.CODEGEN__DEBUG_OUTPUT:
				return isDebugOutput();
			case WorkflowramsesPackage.CODEGEN__AADL_MODEL_SLOT:
				return getAadlModelSlot();
			case WorkflowramsesPackage.CODEGEN__TRACE_MODEL_SLOT:
				return getTraceModelSlot();
			case WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY:
				return getOutputDirectory();
			case WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIR:
				return getTargetInstallDir();
			case WorkflowramsesPackage.CODEGEN__INCLUDE_DIR:
				return getIncludeDir();
			case WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIR:
				return getCoreRuntimeDir();
			case WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIR:
				return getTargetRuntimeDir();
			case WorkflowramsesPackage.CODEGEN__AADL_TO_SOURCE_CODE:
				return getAadlToSourceCode();
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_CONFIGURATION:
				return getAadlToTargetConfiguration();
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD:
				return getAadlToTargetBuild();
			case WorkflowramsesPackage.CODEGEN__TRANSFORMATION_RESOURCES_PAIR_LIST:
				return getTransformationResourcesPairList();
			case WorkflowramsesPackage.CODEGEN__TARGET_PROPERTIES:
				if (resolve) return getTargetProperties();
				return basicGetTargetProperties();
			case WorkflowramsesPackage.CODEGEN__PROGRESS_MONITOR:
				return getProgressMonitor();
			case WorkflowramsesPackage.CODEGEN__URI_CONVERTER:
				return getUriConverter();
			case WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIRECTORY_URI:
				return getTargetInstallDirectoryURI();
			case WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY_URI:
				return getOutputDirectoryURI();
			case WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIRECTORY_URI:
				return getCoreRuntimeDirectoryURI();
			case WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIRECTORY_URI:
				return getTargetRuntimeDirectoryURI();
			case WorkflowramsesPackage.CODEGEN__INCLUDE_DIRECTORY_URI_LIST:
				return getIncludeDirectoryURIList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowramsesPackage.CODEGEN__DEBUG_OUTPUT:
				setDebugOutput((Boolean)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__AADL_MODEL_SLOT:
				setAadlModelSlot((String)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__TRACE_MODEL_SLOT:
				setTraceModelSlot((String)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY:
				setOutputDirectory((String)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIR:
				setTargetInstallDir((String)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__INCLUDE_DIR:
				setIncludeDir((String)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIR:
				setCoreRuntimeDir((String)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIR:
				setTargetRuntimeDir((String)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__AADL_TO_SOURCE_CODE:
				setAadlToSourceCode((AadlToSourceCodeGenerator)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_CONFIGURATION:
				setAadlToTargetConfiguration((AadlToTargetConfigurationGenerator)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD:
				setAadlToTargetBuild((AadlToTargetBuildGenerator)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__TRANSFORMATION_RESOURCES_PAIR_LIST:
				getTransformationResourcesPairList().clear();
				getTransformationResourcesPairList().addAll((Collection<? extends TransformationResourcesPair>)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__TARGET_PROPERTIES:
				setTargetProperties((TargetProperties)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__PROGRESS_MONITOR:
				setProgressMonitor((IProgressMonitor)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__URI_CONVERTER:
				setUriConverter((URIConverter)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIRECTORY_URI:
				setTargetInstallDirectoryURI((URI)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY_URI:
				setOutputDirectoryURI((URI)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIRECTORY_URI:
				setCoreRuntimeDirectoryURI((URI)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIRECTORY_URI:
				setTargetRuntimeDirectoryURI((URI)newValue);
				return;
			case WorkflowramsesPackage.CODEGEN__INCLUDE_DIRECTORY_URI_LIST:
				getIncludeDirectoryURIList().clear();
				getIncludeDirectoryURIList().addAll((Collection<? extends URI>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.CODEGEN__DEBUG_OUTPUT:
				setDebugOutput(DEBUG_OUTPUT_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__AADL_MODEL_SLOT:
				setAadlModelSlot(AADL_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__TRACE_MODEL_SLOT:
				setTraceModelSlot(TRACE_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY:
				setOutputDirectory(OUTPUT_DIRECTORY_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIR:
				setTargetInstallDir(TARGET_INSTALL_DIR_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__INCLUDE_DIR:
				setIncludeDir(INCLUDE_DIR_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIR:
				setCoreRuntimeDir(CORE_RUNTIME_DIR_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIR:
				setTargetRuntimeDir(TARGET_RUNTIME_DIR_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__AADL_TO_SOURCE_CODE:
				setAadlToSourceCode((AadlToSourceCodeGenerator)null);
				return;
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_CONFIGURATION:
				setAadlToTargetConfiguration((AadlToTargetConfigurationGenerator)null);
				return;
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD:
				setAadlToTargetBuild((AadlToTargetBuildGenerator)null);
				return;
			case WorkflowramsesPackage.CODEGEN__TRANSFORMATION_RESOURCES_PAIR_LIST:
				getTransformationResourcesPairList().clear();
				return;
			case WorkflowramsesPackage.CODEGEN__TARGET_PROPERTIES:
				setTargetProperties((TargetProperties)null);
				return;
			case WorkflowramsesPackage.CODEGEN__PROGRESS_MONITOR:
				setProgressMonitor(PROGRESS_MONITOR_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__URI_CONVERTER:
				setUriConverter(URI_CONVERTER_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIRECTORY_URI:
				setTargetInstallDirectoryURI(TARGET_INSTALL_DIRECTORY_URI_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY_URI:
				setOutputDirectoryURI(OUTPUT_DIRECTORY_URI_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIRECTORY_URI:
				setCoreRuntimeDirectoryURI(CORE_RUNTIME_DIRECTORY_URI_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIRECTORY_URI:
				setTargetRuntimeDirectoryURI(TARGET_RUNTIME_DIRECTORY_URI_EDEFAULT);
				return;
			case WorkflowramsesPackage.CODEGEN__INCLUDE_DIRECTORY_URI_LIST:
				getIncludeDirectoryURIList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.CODEGEN__DEBUG_OUTPUT:
				return debugOutput != DEBUG_OUTPUT_EDEFAULT;
			case WorkflowramsesPackage.CODEGEN__AADL_MODEL_SLOT:
				return AADL_MODEL_SLOT_EDEFAULT == null ? aadlModelSlot != null : !AADL_MODEL_SLOT_EDEFAULT.equals(aadlModelSlot);
			case WorkflowramsesPackage.CODEGEN__TRACE_MODEL_SLOT:
				return TRACE_MODEL_SLOT_EDEFAULT == null ? traceModelSlot != null : !TRACE_MODEL_SLOT_EDEFAULT.equals(traceModelSlot);
			case WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY:
				return OUTPUT_DIRECTORY_EDEFAULT == null ? outputDirectory != null : !OUTPUT_DIRECTORY_EDEFAULT.equals(outputDirectory);
			case WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIR:
				return TARGET_INSTALL_DIR_EDEFAULT == null ? targetInstallDir != null : !TARGET_INSTALL_DIR_EDEFAULT.equals(targetInstallDir);
			case WorkflowramsesPackage.CODEGEN__INCLUDE_DIR:
				return INCLUDE_DIR_EDEFAULT == null ? includeDir != null : !INCLUDE_DIR_EDEFAULT.equals(includeDir);
			case WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIR:
				return CORE_RUNTIME_DIR_EDEFAULT == null ? coreRuntimeDir != null : !CORE_RUNTIME_DIR_EDEFAULT.equals(coreRuntimeDir);
			case WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIR:
				return TARGET_RUNTIME_DIR_EDEFAULT == null ? targetRuntimeDir != null : !TARGET_RUNTIME_DIR_EDEFAULT.equals(targetRuntimeDir);
			case WorkflowramsesPackage.CODEGEN__AADL_TO_SOURCE_CODE:
				return aadlToSourceCode != null;
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_CONFIGURATION:
				return aadlToTargetConfiguration != null;
			case WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD:
				return aadlToTargetBuild != null;
			case WorkflowramsesPackage.CODEGEN__TRANSFORMATION_RESOURCES_PAIR_LIST:
				return transformationResourcesPairList != null && !transformationResourcesPairList.isEmpty();
			case WorkflowramsesPackage.CODEGEN__TARGET_PROPERTIES:
				return targetProperties != null;
			case WorkflowramsesPackage.CODEGEN__PROGRESS_MONITOR:
				return PROGRESS_MONITOR_EDEFAULT == null ? progressMonitor != null : !PROGRESS_MONITOR_EDEFAULT.equals(progressMonitor);
			case WorkflowramsesPackage.CODEGEN__URI_CONVERTER:
				return URI_CONVERTER_EDEFAULT == null ? uriConverter != null : !URI_CONVERTER_EDEFAULT.equals(uriConverter);
			case WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIRECTORY_URI:
				return TARGET_INSTALL_DIRECTORY_URI_EDEFAULT == null ? targetInstallDirectoryURI != null : !TARGET_INSTALL_DIRECTORY_URI_EDEFAULT.equals(targetInstallDirectoryURI);
			case WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY_URI:
				return OUTPUT_DIRECTORY_URI_EDEFAULT == null ? outputDirectoryURI != null : !OUTPUT_DIRECTORY_URI_EDEFAULT.equals(outputDirectoryURI);
			case WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIRECTORY_URI:
				return CORE_RUNTIME_DIRECTORY_URI_EDEFAULT == null ? coreRuntimeDirectoryURI != null : !CORE_RUNTIME_DIRECTORY_URI_EDEFAULT.equals(coreRuntimeDirectoryURI);
			case WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIRECTORY_URI:
				return TARGET_RUNTIME_DIRECTORY_URI_EDEFAULT == null ? targetRuntimeDirectoryURI != null : !TARGET_RUNTIME_DIRECTORY_URI_EDEFAULT.equals(targetRuntimeDirectoryURI);
			case WorkflowramsesPackage.CODEGEN__INCLUDE_DIRECTORY_URI_LIST:
				return includeDirectoryURIList != null && !includeDirectoryURIList.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorkflowramsesPackage.CODEGEN___GET_MODEL_TRANSFORMATION_TRACES_LIST:
				return getModelTransformationTracesList();
			case WorkflowramsesPackage.CODEGEN___GET_TARGET_NAME:
				return getTargetName();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (debugOutput: ");
		result.append(debugOutput);
		result.append(", aadlModelSlot: ");
		result.append(aadlModelSlot);
		result.append(", traceModelSlot: ");
		result.append(traceModelSlot);
		result.append(", outputDirectory: ");
		result.append(outputDirectory);
		result.append(", targetInstallDir: ");
		result.append(targetInstallDir);
		result.append(", includeDir: ");
		result.append(includeDir);
		result.append(", coreRuntimeDir: ");
		result.append(coreRuntimeDir);
		result.append(", targetRuntimeDir: ");
		result.append(targetRuntimeDir);
		result.append(", progressMonitor: ");
		result.append(progressMonitor);
		result.append(", uriConverter: ");
		result.append(uriConverter);
		result.append(", targetInstallDirectoryURI: ");
		result.append(targetInstallDirectoryURI);
		result.append(", outputDirectoryURI: ");
		result.append(outputDirectoryURI);
		result.append(", coreRuntimeDirectoryURI: ");
		result.append(coreRuntimeDirectoryURI);
		result.append(", targetRuntimeDirectoryURI: ");
		result.append(targetRuntimeDirectoryURI);
		result.append(", includeDirectoryURIList: ");
		result.append(includeDirectoryURIList);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public void execute (final WorkflowExecutionContext context,
			final IProgressMonitor monitor) throws WorkflowExecutionException, IOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 2);
		
		AadlToTargetBuildGenerator buildGenerator = getAadlToTargetBuild();
		AadlToSourceCodeGenerator sourceCodeGenerator = getAadlToSourceCode();
		AadlToTargetConfigurationGenerator confGenerator = getAadlToTargetConfiguration();
		
		initConfiguration( context );
		
		if(!buildGenerator.validateTargetPath(getTargetInstallDirectoryURI()))
			throw new WorkflowExecutionException("Install directory provided for "+ getTargetName() + " is invalid");
		
		try
		{
			AadlTargetSpecificCodeGenerator codegen = new AadlTargetSpecificCodeGenerator(sourceCodeGenerator, 
					confGenerator, 
					buildGenerator);

			codegen.generate(monitor);
		}
		catch (GenerationException e) {
			throw new WorkflowExecutionException("Code generation failed :"+ e.getLocalizedMessage(), e);
		}
		
		subMonitor.worked(1);
	}

	@Override
	public void execute(final WorkflowExecutionContext context)
	throws WorkflowExecutionException, IOException {
		execute( context, new NullProgressMonitor() );
	}
	
	protected EList<URI> getIncludeDirs( final String path,
										final URIConverter uriConverter) {
		final EList<URI> result = new BasicEList<URI>();
		
		if ( path != null && !path.isEmpty() ) {
			final URI projectPathUri = URI.createURI( path );
	//		File projectPath = new File(path);
	
			if( FileUtils.exists( projectPathUri, uriConverter ) ) {
	//			No reason to add all subdirectories of the project as include dirs...
	//			result = FileUtils.getSubDirectories(projectPath) ;
	//			Is it really necessary to register the project dir as an include dir?
				result.add( projectPathUri );
			}
		}
		
		return result;
	}
	
	private void initConfiguration( final WorkflowExecutionContext context ) {
		URI targetDir = null;
		String installDir = getTargetInstallDir();
		if(installDir.endsWith(","))
			installDir = installDir.substring(0, installDir.lastIndexOf(","));
		if(getTargetInstallDir().contains("="))
		{
			Map<String, String> installPaths = Splitter.on(",").withKeyValueSeparator("=").split(installDir);
			for(String targetName: installPaths.keySet())
			{
				if(targetName.equalsIgnoreCase(getTargetName()))
				{
					String installPath = installPaths.get(targetName);
					targetDir = URI.createURI( installPath );
					break;
				}
			}
		}
		else
		{
			String installPath = getTargetInstallDir();
			if(installPath!=null)
				targetDir = URI.createURI( installPath );
		}
		setTargetInstallDirectoryURI(targetDir);
		
		setUriConverter(context.getGlobalResourceSet().getURIConverter());
		

		final URI coreRuntimeDirURI;
		final URI targetRuntimeDirURI;
		final URI outputURI;
		
		if (Platform.isRunning()) {
			coreRuntimeDirURI = CommonPlugin.resolve( URI.createURI( getCoreRuntimeDir() ) );
			targetRuntimeDirURI = CommonPlugin.resolve( URI.createURI( getTargetRuntimeDir() ) );
			outputURI = CommonPlugin.resolve( URI.createURI(getOutputDirectory()) );
		}
		else {
			// DB: We now work with absolute paths in standalone otherwise relative URIs resolution will not work
			// EB : predefined resources uri is specified relative to workflow uri; needs resolution
			URI unresolvedTargetPredefResDirURI = URI.createURI( getTargetRuntimeDir() );
			URI unresolvedPredefResDirURI = URI.createURI( getCoreRuntimeDir() );
			URI normalizedWorkflowUri = context.getGlobalResourceSet().getURIConverter().normalize(context.getWorkflowFileURI());
			
			if(unresolvedPredefResDirURI.hasAbsolutePath())
				coreRuntimeDirURI = unresolvedPredefResDirURI;
			else
				coreRuntimeDirURI = WorkflowUtil.getResolvedURI( unresolvedPredefResDirURI, normalizedWorkflowUri );
			
			if(unresolvedTargetPredefResDirURI.hasAbsolutePath())
				targetRuntimeDirURI = unresolvedTargetPredefResDirURI;
			else
				targetRuntimeDirURI = WorkflowUtil.getResolvedURI( unresolvedTargetPredefResDirURI, normalizedWorkflowUri );
			
			outputURI = URI.createURI(getOutputDirectory());
		}

		setOutputDirectoryURI(outputURI);
		setCoreRuntimeDirectoryURI(coreRuntimeDirURI.appendSegment( AbstractAadlToCMakefileUnparser.C_BASIC_SUB_PATH ));
		setTargetRuntimeDirectoryURI(targetRuntimeDirURI.appendSegment( AbstractAadlToCMakefileUnparser.C_BASIC_SUB_PATH ));
		
		FileUtils.makeDir( outputURI );

		final EMap<String, Object> modelSlots = context.getModelSlots();
		TransformationResourcesPair transfoResourcePair = new TransformationResourcesPairImpl(); 
		transfoResourcePair.setModelTransformationTrace((ArchTraceSpec) modelSlots.get(getTraceModelSlot() ));
		transfoResourcePair.setOutputModelRoot((EObject) modelSlots.get(getAadlModelSlot() ) );
		this.getTransformationResourcesPairList().add(transfoResourcePair);
		
		includeDirectoryURIList = getIncludeDirs( getIncludeDir(), context.getGlobalResourceSet().getURIConverter() );
		
	}
} //CodegenImpl
