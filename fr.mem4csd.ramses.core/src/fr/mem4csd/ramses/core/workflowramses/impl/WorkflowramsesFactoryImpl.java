/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import fr.mem4csd.ramses.core.codegen.GenerationException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.resource.URIConverter;
import fr.mem4csd.ramses.core.workflowramses.*;
import java.io.File;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowramsesFactoryImpl extends EFactoryImpl implements WorkflowramsesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WorkflowramsesFactory init() {
		try {
			WorkflowramsesFactory theWorkflowramsesFactory = (WorkflowramsesFactory)EPackage.Registry.INSTANCE.getEFactory(WorkflowramsesPackage.eNS_URI);
			if (theWorkflowramsesFactory != null) {
				return theWorkflowramsesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WorkflowramsesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramsesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WorkflowramsesPackage.TRACE_WRITER: return createTraceWriter();
			case WorkflowramsesPackage.CLEAR_VALIDATION_ERRORS: return createClearValidationErrors();
			case WorkflowramsesPackage.REPORT_VALIDATION_ERRORS: return createReportValidationErrors();
			case WorkflowramsesPackage.CONDITION_EVALUATION_TARGET: return createConditionEvaluationTarget();
			case WorkflowramsesPackage.CONDITION_EVALUATION_PROTOCOL: return createConditionEvaluationProtocol();
			case WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR: return createTransformationResourcesPair();
			case WorkflowramsesPackage.AADL_WRITER: return createAadlWriter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case WorkflowramsesPackage.EFILE:
				return createEFileFromString(eDataType, initialValue);
			case WorkflowramsesPackage.GENERATION_EXCEPTION:
				return createGenerationExceptionFromString(eDataType, initialValue);
			case WorkflowramsesPackage.URI_CONVERTER:
				return createURIConverterFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case WorkflowramsesPackage.EFILE:
				return convertEFileToString(eDataType, instanceValue);
			case WorkflowramsesPackage.GENERATION_EXCEPTION:
				return convertGenerationExceptionToString(eDataType, instanceValue);
			case WorkflowramsesPackage.URI_CONVERTER:
				return convertURIConverterToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TraceWriter createTraceWriter() {
		TraceWriterImpl traceWriter = new TraceWriterImpl();
		return traceWriter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ClearValidationErrors createClearValidationErrors() {
		ClearValidationErrorsImpl clearValidationErrors = new ClearValidationErrorsImpl();
		return clearValidationErrors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ReportValidationErrors createReportValidationErrors() {
		ReportValidationErrorsImpl reportValidationErrors = new ReportValidationErrorsImpl();
		return reportValidationErrors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConditionEvaluationTarget createConditionEvaluationTarget() {
		ConditionEvaluationTargetImpl conditionEvaluationTarget = new ConditionEvaluationTargetImpl();
		return conditionEvaluationTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConditionEvaluationProtocol createConditionEvaluationProtocol() {
		ConditionEvaluationProtocolImpl conditionEvaluationProtocol = new ConditionEvaluationProtocolImpl();
		return conditionEvaluationProtocol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TransformationResourcesPair createTransformationResourcesPair() {
		TransformationResourcesPairImpl transformationResourcesPair = new TransformationResourcesPairImpl();
		return transformationResourcesPair;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AadlWriter createAadlWriter() {
		AadlWriterImpl aadlWriter = new AadlWriterImpl();
		return aadlWriter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public File createEFileFromString(EDataType eDataType, String initialValue) {
		return (File)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEFileToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenerationException createGenerationExceptionFromString(EDataType eDataType, String initialValue) {
		return (GenerationException)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertGenerationExceptionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public URIConverter createURIConverterFromString(EDataType eDataType, String initialValue) {
		return (URIConverter)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertURIConverterToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WorkflowramsesPackage getWorkflowramsesPackage() {
		return (WorkflowramsesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WorkflowramsesPackage getPackage() {
		return WorkflowramsesPackage.eINSTANCE;
	}

} //WorkflowramsesFactoryImpl
