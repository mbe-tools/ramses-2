/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.osate.aadl2.Element;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import de.mdelab.workflow.util.WorkflowUtil;
import fr.mem4csd.ramses.core.codegen.utils.Names;
import fr.mem4csd.ramses.core.workflowramses.ClearValidationErrors;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;
import fr.mem4csd.utils.emf.EMFUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Clear Validation Errors</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.ClearValidationErrorsImpl#getValidationReportModelURI <em>Validation Report Model URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClearValidationErrorsImpl extends WorkflowComponentImpl implements ClearValidationErrors {
	/**
	 * The default value of the '{@link #getValidationReportModelURI() <em>Validation Report Model URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidationReportModelURI()
	 * @generated
	 * @ordered
	 */
	protected static final String VALIDATION_REPORT_MODEL_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValidationReportModelURI() <em>Validation Report Model URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidationReportModelURI()
	 * @generated
	 * @ordered
	 */
	protected String validationReportModelURI = VALIDATION_REPORT_MODEL_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClearValidationErrorsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesPackage.Literals.CLEAR_VALIDATION_ERRORS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValidationReportModelURI() {
		return validationReportModelURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValidationReportModelURI(String newValidationReportModelURI) {
		String oldValidationReportModelURI = validationReportModelURI;
		validationReportModelURI = newValidationReportModelURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CLEAR_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_URI, oldValidationReportModelURI, validationReportModelURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowramsesPackage.CLEAR_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_URI:
				return getValidationReportModelURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowramsesPackage.CLEAR_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_URI:
				setValidationReportModelURI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.CLEAR_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_URI:
				setValidationReportModelURI(VALIDATION_REPORT_MODEL_URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.CLEAR_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_URI:
				return VALIDATION_REPORT_MODEL_URI_EDEFAULT == null ? validationReportModelURI != null : !VALIDATION_REPORT_MODEL_URI_EDEFAULT.equals(validationReportModelURI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (validationReportModelURI: ");
		result.append(validationReportModelURI);
		result.append(')');
		return result.toString();
	}


	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void execute(	final WorkflowExecutionContext context,
							final IProgressMonitor monitor )
	throws WorkflowExecutionException, IOException {
		if ( Platform.isRunning() ) {
			final URI errResURI = WorkflowUtil.getResolvedURI( URI.createURI( getValidationReportModelURI() ), context.getWorkflowFileURI() );
			final ResourceSet resSet = context.getGlobalResourceSet();		
			if ( resSet.getURIConverter().exists( errResURI, null ) ) {
				final Resource errRes = resSet.getResource( errResURI, true );
				final Set<URI> cleared = new HashSet<URI>();

				for ( final EObject err : errRes.getContents() ) {
					if ( err instanceof fr.mem4csd.ramses.core.validation_report.Error ) {
						final fr.mem4csd.ramses.core.validation_report.Error constraintError = (fr.mem4csd.ramses.core.validation_report.Error) err;
						
						if ( constraintError.getObject() instanceof Element ) {
							final Element elt = (Element) constraintError.getObject();
							final Resource resWithError = elt.eResource();
						
							if ( resWithError != null ) {
								try {
									deleteMarkers( resWithError, cleared );
								} 
								catch ( final CoreException ex ) {
									throw new IOException( ex );
								}
							}
						}
					}
				}

				errRes.delete( null );
			}
		}
	}
				
	private void deleteMarkers( final Resource res,
								final Set<URI> traversedRes ) 
	throws CoreException {
		final URI resUri = res.getURI();
		
		if ( resUri != null && resUri.isPlatformResource() && !traversedRes.contains( resUri ) ) {
			final IFile file = OsateResourceUtil.toIFile( resUri );
			file.deleteMarkers( Names.RAMSES_CODGEN_PROBLEM_MARKER, false, IResource.DEPTH_INFINITE );
			traversedRes.add( resUri );
			
			for ( final Resource depRes : EMFUtil.allReferencedExtents( res ) ) {
				deleteMarkers( depRes, traversedRes );
			}
		}
	}
} //ClearValidationErrorsImpl
