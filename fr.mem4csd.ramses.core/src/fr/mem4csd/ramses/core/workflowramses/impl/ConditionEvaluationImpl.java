/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import de.mdelab.workflow.components.impl.WorkflowComponentImpl;

import fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Condition Evaluation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationImpl#getInstanceModelSlot <em>Instance Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationImpl#getResultModelSlot <em>Result Model Slot</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ConditionEvaluationImpl extends WorkflowComponentImpl implements ConditionEvaluation {
	/**
	 * The default value of the '{@link #getInstanceModelSlot() <em>Instance Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String INSTANCE_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInstanceModelSlot() <em>Instance Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String instanceModelSlot = INSTANCE_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getResultModelSlot() <em>Result Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String RESULT_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResultModelSlot() <em>Result Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String resultModelSlot = RESULT_MODEL_SLOT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionEvaluationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesPackage.Literals.CONDITION_EVALUATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getInstanceModelSlot() {
		return instanceModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInstanceModelSlot(String newInstanceModelSlot) {
		String oldInstanceModelSlot = instanceModelSlot;
		instanceModelSlot = newInstanceModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CONDITION_EVALUATION__INSTANCE_MODEL_SLOT, oldInstanceModelSlot, instanceModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getResultModelSlot() {
		return resultModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setResultModelSlot(String newResultModelSlot) {
		String oldResultModelSlot = resultModelSlot;
		resultModelSlot = newResultModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowramsesPackage.CONDITION_EVALUATION__RESULT_MODEL_SLOT, oldResultModelSlot, resultModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowramsesPackage.CONDITION_EVALUATION__INSTANCE_MODEL_SLOT:
				return getInstanceModelSlot();
			case WorkflowramsesPackage.CONDITION_EVALUATION__RESULT_MODEL_SLOT:
				return getResultModelSlot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowramsesPackage.CONDITION_EVALUATION__INSTANCE_MODEL_SLOT:
				setInstanceModelSlot((String)newValue);
				return;
			case WorkflowramsesPackage.CONDITION_EVALUATION__RESULT_MODEL_SLOT:
				setResultModelSlot((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.CONDITION_EVALUATION__INSTANCE_MODEL_SLOT:
				setInstanceModelSlot(INSTANCE_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowramsesPackage.CONDITION_EVALUATION__RESULT_MODEL_SLOT:
				setResultModelSlot(RESULT_MODEL_SLOT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowramsesPackage.CONDITION_EVALUATION__INSTANCE_MODEL_SLOT:
				return INSTANCE_MODEL_SLOT_EDEFAULT == null ? instanceModelSlot != null : !INSTANCE_MODEL_SLOT_EDEFAULT.equals(instanceModelSlot);
			case WorkflowramsesPackage.CONDITION_EVALUATION__RESULT_MODEL_SLOT:
				return RESULT_MODEL_SLOT_EDEFAULT == null ? resultModelSlot != null : !RESULT_MODEL_SLOT_EDEFAULT.equals(resultModelSlot);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (instanceModelSlot: ");
		result.append(instanceModelSlot);
		result.append(", resultModelSlot: ");
		result.append(resultModelSlot);
		result.append(')');
		return result.toString();
	}

} //ConditionEvaluationImpl
