/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import de.mdelab.workflow.WorkflowPackage;
import de.mdelab.workflow.components.ComponentsPackage;
import de.mdelab.workflow.helpers.HelpersPackage;
import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;
import fr.mem4csd.ramses.core.codegen.GenerationException;
import fr.mem4csd.ramses.core.workflowramses.AadlToSourceCodeGenerator;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator;
import fr.mem4csd.ramses.core.workflowramses.AadlWriter;
import fr.mem4csd.ramses.core.workflowramses.AbstractCodeGenerator;
import fr.mem4csd.ramses.core.workflowramses.ClearValidationErrors;
import fr.mem4csd.ramses.core.workflowramses.Codegen;
import fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation;
import fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationProtocol;
import fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationTarget;
import fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors;
import fr.mem4csd.ramses.core.workflowramses.TargetProperties;
import fr.mem4csd.ramses.core.workflowramses.TraceWriter;
import fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesFactory;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;
import java.io.File;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowramsesPackageImpl extends EPackageImpl implements WorkflowramsesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codegenEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass traceWriterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionEvaluationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clearValidationErrorsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reportValidationErrorsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionEvaluationTargetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionEvaluationProtocolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aadlToSourceCodeGeneratorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aadlToTargetConfigurationGeneratorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aadlToTargetBuildGeneratorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass targetPropertiesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transformationResourcesPairEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractCodeGeneratorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aadlWriterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eFileEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType generationExceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType uriConverterEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkflowramsesPackageImpl() {
		super(eNS_URI, WorkflowramsesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WorkflowramsesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkflowramsesPackage init() {
		if (isInited) return (WorkflowramsesPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowramsesPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorkflowramsesPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WorkflowramsesPackageImpl theWorkflowramsesPackage = registeredWorkflowramsesPackage instanceof WorkflowramsesPackageImpl ? (WorkflowramsesPackageImpl)registeredWorkflowramsesPackage : new WorkflowramsesPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Aadl2Package.eINSTANCE.eClass();
		Arch_tracePackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();
		HelpersPackage.eINSTANCE.eClass();
		ComponentsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkflowramsesPackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowramsesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorkflowramsesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkflowramsesPackage.eNS_URI, theWorkflowramsesPackage);
		return theWorkflowramsesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCodegen() {
		return codegenEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_DebugOutput() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_AadlModelSlot() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_TraceModelSlot() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_OutputDirectory() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_TargetInstallDir() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_IncludeDir() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_CoreRuntimeDir() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_TargetRuntimeDir() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCodegen_AadlToSourceCode() {
		return (EReference)codegenEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCodegen_AadlToTargetConfiguration() {
		return (EReference)codegenEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCodegen_AadlToTargetBuild() {
		return (EReference)codegenEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCodegen_TransformationResourcesPairList() {
		return (EReference)codegenEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCodegen_TargetProperties() {
		return (EReference)codegenEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_ProgressMonitor() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_UriConverter() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_TargetInstallDirectoryURI() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_OutputDirectoryURI() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_CoreRuntimeDirectoryURI() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_TargetRuntimeDirectoryURI() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCodegen_IncludeDirectoryURIList() {
		return (EAttribute)codegenEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCodegen__GetModelTransformationTracesList() {
		return codegenEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCodegen__GetTargetName() {
		return codegenEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTraceWriter() {
		return traceWriterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConditionEvaluation() {
		return conditionEvaluationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionEvaluation_InstanceModelSlot() {
		return (EAttribute)conditionEvaluationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionEvaluation_ResultModelSlot() {
		return (EAttribute)conditionEvaluationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getClearValidationErrors() {
		return clearValidationErrorsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getClearValidationErrors_ValidationReportModelURI() {
		return (EAttribute)clearValidationErrorsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReportValidationErrors() {
		return reportValidationErrorsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getReportValidationErrors_ValidationReportModelSlot() {
		return (EAttribute)reportValidationErrorsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getReportValidationErrors_HasErrorModelSlot() {
		return (EAttribute)reportValidationErrorsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConditionEvaluationTarget() {
		return conditionEvaluationTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionEvaluationTarget_Target() {
		return (EAttribute)conditionEvaluationTargetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConditionEvaluationProtocol() {
		return conditionEvaluationProtocolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionEvaluationProtocol_Protocols() {
		return (EAttribute)conditionEvaluationProtocolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAadlToSourceCodeGenerator() {
		return aadlToSourceCodeGeneratorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAadlToSourceCodeGenerator__GenerateSourceCode__Element_URI_IProgressMonitor() {
		return aadlToSourceCodeGeneratorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAadlToTargetConfigurationGenerator() {
		return aadlToTargetConfigurationGeneratorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAadlToTargetConfigurationGenerator_CodeGenWorkflowComponent() {
		return (EReference)aadlToTargetConfigurationGeneratorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAadlToTargetConfigurationGenerator__GetSystemTargetConfiguration__EList_IProgressMonitor() {
		return aadlToTargetConfigurationGeneratorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAadlToTargetConfigurationGenerator__GenerateProcessorTargetConfiguration__Subcomponent_URI_IProgressMonitor() {
		return aadlToTargetConfigurationGeneratorEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAadlToTargetConfigurationGenerator__GenerateProcessTargetConfiguration__ProcessSubcomponent_URI_IProgressMonitor() {
		return aadlToTargetConfigurationGeneratorEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAadlToTargetBuildGenerator() {
		return aadlToTargetBuildGeneratorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAadlToTargetBuildGenerator_CodeGenWorkflowComponent() {
		return (EReference)aadlToTargetBuildGeneratorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAadlToTargetBuildGenerator__GenerateSystemBuild__EList_URI_IProgressMonitor() {
		return aadlToTargetBuildGeneratorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAadlToTargetBuildGenerator__GenerateProcessorBuild__Subcomponent_URI_IProgressMonitor() {
		return aadlToTargetBuildGeneratorEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAadlToTargetBuildGenerator__GenerateProcessBuild__ProcessSubcomponent_URI_IProgressMonitor() {
		return aadlToTargetBuildGeneratorEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAadlToTargetBuildGenerator__ValidateTargetPath__URI() {
		return aadlToTargetBuildGeneratorEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAadlToTargetBuildGenerator__GetTargetShortDescription() {
		return aadlToTargetBuildGeneratorEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAadlToTargetBuildGenerator__GetTargetName() {
		return aadlToTargetBuildGeneratorEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAadlToTargetBuildGenerator__InitializeTargetBuilder() {
		return aadlToTargetBuildGeneratorEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTargetProperties() {
		return targetPropertiesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTransformationResourcesPair() {
		return transformationResourcesPairEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTransformationResourcesPair_ModelTransformationTrace() {
		return (EReference)transformationResourcesPairEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTransformationResourcesPair_OutputModelRoot() {
		return (EReference)transformationResourcesPairEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAbstractCodeGenerator() {
		return abstractCodeGeneratorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAbstractCodeGenerator_CurrentModelTransformationTrace() {
		return (EReference)abstractCodeGeneratorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAadlWriter() {
		return aadlWriterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getEFile() {
		return eFileEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getGenerationException() {
		return generationExceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getURIConverter() {
		return uriConverterEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WorkflowramsesFactory getWorkflowramsesFactory() {
		return (WorkflowramsesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		codegenEClass = createEClass(CODEGEN);
		createEAttribute(codegenEClass, CODEGEN__DEBUG_OUTPUT);
		createEAttribute(codegenEClass, CODEGEN__AADL_MODEL_SLOT);
		createEAttribute(codegenEClass, CODEGEN__TRACE_MODEL_SLOT);
		createEAttribute(codegenEClass, CODEGEN__OUTPUT_DIRECTORY);
		createEAttribute(codegenEClass, CODEGEN__TARGET_INSTALL_DIR);
		createEAttribute(codegenEClass, CODEGEN__INCLUDE_DIR);
		createEAttribute(codegenEClass, CODEGEN__CORE_RUNTIME_DIR);
		createEAttribute(codegenEClass, CODEGEN__TARGET_RUNTIME_DIR);
		createEReference(codegenEClass, CODEGEN__AADL_TO_SOURCE_CODE);
		createEReference(codegenEClass, CODEGEN__AADL_TO_TARGET_CONFIGURATION);
		createEReference(codegenEClass, CODEGEN__AADL_TO_TARGET_BUILD);
		createEReference(codegenEClass, CODEGEN__TRANSFORMATION_RESOURCES_PAIR_LIST);
		createEReference(codegenEClass, CODEGEN__TARGET_PROPERTIES);
		createEAttribute(codegenEClass, CODEGEN__PROGRESS_MONITOR);
		createEAttribute(codegenEClass, CODEGEN__URI_CONVERTER);
		createEAttribute(codegenEClass, CODEGEN__TARGET_INSTALL_DIRECTORY_URI);
		createEAttribute(codegenEClass, CODEGEN__OUTPUT_DIRECTORY_URI);
		createEAttribute(codegenEClass, CODEGEN__CORE_RUNTIME_DIRECTORY_URI);
		createEAttribute(codegenEClass, CODEGEN__TARGET_RUNTIME_DIRECTORY_URI);
		createEAttribute(codegenEClass, CODEGEN__INCLUDE_DIRECTORY_URI_LIST);
		createEOperation(codegenEClass, CODEGEN___GET_MODEL_TRANSFORMATION_TRACES_LIST);
		createEOperation(codegenEClass, CODEGEN___GET_TARGET_NAME);

		traceWriterEClass = createEClass(TRACE_WRITER);

		conditionEvaluationEClass = createEClass(CONDITION_EVALUATION);
		createEAttribute(conditionEvaluationEClass, CONDITION_EVALUATION__INSTANCE_MODEL_SLOT);
		createEAttribute(conditionEvaluationEClass, CONDITION_EVALUATION__RESULT_MODEL_SLOT);

		clearValidationErrorsEClass = createEClass(CLEAR_VALIDATION_ERRORS);
		createEAttribute(clearValidationErrorsEClass, CLEAR_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_URI);

		reportValidationErrorsEClass = createEClass(REPORT_VALIDATION_ERRORS);
		createEAttribute(reportValidationErrorsEClass, REPORT_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_SLOT);
		createEAttribute(reportValidationErrorsEClass, REPORT_VALIDATION_ERRORS__HAS_ERROR_MODEL_SLOT);

		conditionEvaluationTargetEClass = createEClass(CONDITION_EVALUATION_TARGET);
		createEAttribute(conditionEvaluationTargetEClass, CONDITION_EVALUATION_TARGET__TARGET);

		conditionEvaluationProtocolEClass = createEClass(CONDITION_EVALUATION_PROTOCOL);
		createEAttribute(conditionEvaluationProtocolEClass, CONDITION_EVALUATION_PROTOCOL__PROTOCOLS);

		aadlToSourceCodeGeneratorEClass = createEClass(AADL_TO_SOURCE_CODE_GENERATOR);
		createEOperation(aadlToSourceCodeGeneratorEClass, AADL_TO_SOURCE_CODE_GENERATOR___GENERATE_SOURCE_CODE__ELEMENT_URI_IPROGRESSMONITOR);

		aadlToTargetConfigurationGeneratorEClass = createEClass(AADL_TO_TARGET_CONFIGURATION_GENERATOR);
		createEReference(aadlToTargetConfigurationGeneratorEClass, AADL_TO_TARGET_CONFIGURATION_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT);
		createEOperation(aadlToTargetConfigurationGeneratorEClass, AADL_TO_TARGET_CONFIGURATION_GENERATOR___GET_SYSTEM_TARGET_CONFIGURATION__ELIST_IPROGRESSMONITOR);
		createEOperation(aadlToTargetConfigurationGeneratorEClass, AADL_TO_TARGET_CONFIGURATION_GENERATOR___GENERATE_PROCESSOR_TARGET_CONFIGURATION__SUBCOMPONENT_URI_IPROGRESSMONITOR);
		createEOperation(aadlToTargetConfigurationGeneratorEClass, AADL_TO_TARGET_CONFIGURATION_GENERATOR___GENERATE_PROCESS_TARGET_CONFIGURATION__PROCESSSUBCOMPONENT_URI_IPROGRESSMONITOR);

		aadlToTargetBuildGeneratorEClass = createEClass(AADL_TO_TARGET_BUILD_GENERATOR);
		createEReference(aadlToTargetBuildGeneratorEClass, AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT);
		createEOperation(aadlToTargetBuildGeneratorEClass, AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_SYSTEM_BUILD__ELIST_URI_IPROGRESSMONITOR);
		createEOperation(aadlToTargetBuildGeneratorEClass, AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_PROCESSOR_BUILD__SUBCOMPONENT_URI_IPROGRESSMONITOR);
		createEOperation(aadlToTargetBuildGeneratorEClass, AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_PROCESS_BUILD__PROCESSSUBCOMPONENT_URI_IPROGRESSMONITOR);
		createEOperation(aadlToTargetBuildGeneratorEClass, AADL_TO_TARGET_BUILD_GENERATOR___VALIDATE_TARGET_PATH__URI);
		createEOperation(aadlToTargetBuildGeneratorEClass, AADL_TO_TARGET_BUILD_GENERATOR___GET_TARGET_SHORT_DESCRIPTION);
		createEOperation(aadlToTargetBuildGeneratorEClass, AADL_TO_TARGET_BUILD_GENERATOR___GET_TARGET_NAME);
		createEOperation(aadlToTargetBuildGeneratorEClass, AADL_TO_TARGET_BUILD_GENERATOR___INITIALIZE_TARGET_BUILDER);

		targetPropertiesEClass = createEClass(TARGET_PROPERTIES);

		transformationResourcesPairEClass = createEClass(TRANSFORMATION_RESOURCES_PAIR);
		createEReference(transformationResourcesPairEClass, TRANSFORMATION_RESOURCES_PAIR__MODEL_TRANSFORMATION_TRACE);
		createEReference(transformationResourcesPairEClass, TRANSFORMATION_RESOURCES_PAIR__OUTPUT_MODEL_ROOT);

		abstractCodeGeneratorEClass = createEClass(ABSTRACT_CODE_GENERATOR);
		createEReference(abstractCodeGeneratorEClass, ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE);

		aadlWriterEClass = createEClass(AADL_WRITER);

		// Create data types
		eFileEDataType = createEDataType(EFILE);
		generationExceptionEDataType = createEDataType(GENERATION_EXCEPTION);
		uriConverterEDataType = createEDataType(URI_CONVERTER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ComponentsPackage theComponentsPackage = (ComponentsPackage)EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);
		HelpersPackage theHelpersPackage = (HelpersPackage)EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI);
		Arch_tracePackage theArch_tracePackage = (Arch_tracePackage)EPackage.Registry.INSTANCE.getEPackage(Arch_tracePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		Aadl2Package theAadl2Package = (Aadl2Package)EPackage.Registry.INSTANCE.getEPackage(Aadl2Package.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		codegenEClass.getESuperTypes().add(theComponentsPackage.getWorkflowComponent());
		traceWriterEClass.getESuperTypes().add(theComponentsPackage.getModelWriter());
		conditionEvaluationEClass.getESuperTypes().add(theComponentsPackage.getWorkflowComponent());
		clearValidationErrorsEClass.getESuperTypes().add(theComponentsPackage.getWorkflowComponent());
		reportValidationErrorsEClass.getESuperTypes().add(theComponentsPackage.getWorkflowComponent());
		conditionEvaluationTargetEClass.getESuperTypes().add(this.getConditionEvaluation());
		conditionEvaluationProtocolEClass.getESuperTypes().add(this.getConditionEvaluation());
		aadlToSourceCodeGeneratorEClass.getESuperTypes().add(this.getAbstractCodeGenerator());
		aadlToTargetConfigurationGeneratorEClass.getESuperTypes().add(this.getAbstractCodeGenerator());
		aadlToTargetBuildGeneratorEClass.getESuperTypes().add(this.getAbstractCodeGenerator());
		aadlWriterEClass.getESuperTypes().add(theComponentsPackage.getModelWriter());

		// Initialize classes, features, and operations; add parameters
		initEClass(codegenEClass, Codegen.class, "Codegen", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCodegen_DebugOutput(), ecorePackage.getEBoolean(), "debugOutput", null, 1, 1, Codegen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_AadlModelSlot(), ecorePackage.getEString(), "aadlModelSlot", null, 1, 1, Codegen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_TraceModelSlot(), ecorePackage.getEString(), "traceModelSlot", null, 1, 1, Codegen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_OutputDirectory(), ecorePackage.getEString(), "outputDirectory", null, 0, 1, Codegen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_TargetInstallDir(), ecorePackage.getEString(), "targetInstallDir", null, 1, 1, Codegen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_IncludeDir(), ecorePackage.getEString(), "includeDir", null, 0, 1, Codegen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_CoreRuntimeDir(), ecorePackage.getEString(), "coreRuntimeDir", null, 0, 1, Codegen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_TargetRuntimeDir(), ecorePackage.getEString(), "targetRuntimeDir", null, 0, 1, Codegen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodegen_AadlToSourceCode(), this.getAadlToSourceCodeGenerator(), null, "aadlToSourceCode", null, 0, 1, Codegen.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodegen_AadlToTargetConfiguration(), this.getAadlToTargetConfigurationGenerator(), this.getAadlToTargetConfigurationGenerator_CodeGenWorkflowComponent(), "aadlToTargetConfiguration", null, 0, 1, Codegen.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodegen_AadlToTargetBuild(), this.getAadlToTargetBuildGenerator(), this.getAadlToTargetBuildGenerator_CodeGenWorkflowComponent(), "aadlToTargetBuild", null, 0, 1, Codegen.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodegen_TransformationResourcesPairList(), this.getTransformationResourcesPair(), null, "transformationResourcesPairList", null, 0, -1, Codegen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodegen_TargetProperties(), this.getTargetProperties(), null, "targetProperties", null, 0, 1, Codegen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_ProgressMonitor(), theHelpersPackage.getIProgressMonitor(), "progressMonitor", null, 0, 1, Codegen.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_UriConverter(), this.getURIConverter(), "uriConverter", null, 0, 1, Codegen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_TargetInstallDirectoryURI(), theHelpersPackage.getURI(), "targetInstallDirectoryURI", null, 0, 1, Codegen.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_OutputDirectoryURI(), theHelpersPackage.getURI(), "outputDirectoryURI", null, 0, 1, Codegen.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_CoreRuntimeDirectoryURI(), theHelpersPackage.getURI(), "coreRuntimeDirectoryURI", null, 0, 1, Codegen.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_TargetRuntimeDirectoryURI(), theHelpersPackage.getURI(), "targetRuntimeDirectoryURI", null, 0, 1, Codegen.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegen_IncludeDirectoryURIList(), theHelpersPackage.getURI(), "includeDirectoryURIList", null, 0, -1, Codegen.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getCodegen__GetModelTransformationTracesList(), theArch_tracePackage.getArchTraceSpec(), "getModelTransformationTracesList", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getCodegen__GetTargetName(), theEcorePackage.getEString(), "getTargetName", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(traceWriterEClass, TraceWriter.class, "TraceWriter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(conditionEvaluationEClass, ConditionEvaluation.class, "ConditionEvaluation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConditionEvaluation_InstanceModelSlot(), ecorePackage.getEString(), "instanceModelSlot", null, 1, 1, ConditionEvaluation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConditionEvaluation_ResultModelSlot(), ecorePackage.getEString(), "resultModelSlot", null, 1, 1, ConditionEvaluation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(clearValidationErrorsEClass, ClearValidationErrors.class, "ClearValidationErrors", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClearValidationErrors_ValidationReportModelURI(), ecorePackage.getEString(), "validationReportModelURI", null, 1, 1, ClearValidationErrors.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(reportValidationErrorsEClass, ReportValidationErrors.class, "ReportValidationErrors", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getReportValidationErrors_ValidationReportModelSlot(), ecorePackage.getEString(), "validationReportModelSlot", null, 1, 1, ReportValidationErrors.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReportValidationErrors_HasErrorModelSlot(), ecorePackage.getEString(), "hasErrorModelSlot", null, 1, 1, ReportValidationErrors.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conditionEvaluationTargetEClass, ConditionEvaluationTarget.class, "ConditionEvaluationTarget", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConditionEvaluationTarget_Target(), ecorePackage.getEString(), "target", null, 1, 1, ConditionEvaluationTarget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conditionEvaluationProtocolEClass, ConditionEvaluationProtocol.class, "ConditionEvaluationProtocol", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConditionEvaluationProtocol_Protocols(), ecorePackage.getEString(), "protocols", null, 0, 1, ConditionEvaluationProtocol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aadlToSourceCodeGeneratorEClass, AadlToSourceCodeGenerator.class, "AadlToSourceCodeGenerator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getAadlToSourceCodeGenerator__GenerateSourceCode__Element_URI_IProgressMonitor(), null, "generateSourceCode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getElement(), "element", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getURI(), "systemOutputDir", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getGenerationException());

		initEClass(aadlToTargetConfigurationGeneratorEClass, AadlToTargetConfigurationGenerator.class, "AadlToTargetConfigurationGenerator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAadlToTargetConfigurationGenerator_CodeGenWorkflowComponent(), this.getCodegen(), this.getCodegen_AadlToTargetConfiguration(), "codeGenWorkflowComponent", null, 1, 1, AadlToTargetConfigurationGenerator.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getAadlToTargetConfigurationGenerator__GetSystemTargetConfiguration__EList_IProgressMonitor(), this.getTargetProperties(), "getSystemTargetConfiguration", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getSystemImplementation(), "systemImplementationLit", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getGenerationException());

		op = initEOperation(getAadlToTargetConfigurationGenerator__GenerateProcessorTargetConfiguration__Subcomponent_URI_IProgressMonitor(), null, "generateProcessorTargetConfiguration", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getSubcomponent(), "processor", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getURI(), "processorOutputDir", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getGenerationException());

		op = initEOperation(getAadlToTargetConfigurationGenerator__GenerateProcessTargetConfiguration__ProcessSubcomponent_URI_IProgressMonitor(), null, "generateProcessTargetConfiguration", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getProcessSubcomponent(), "process", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getURI(), "processOutputDir", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getGenerationException());

		initEClass(aadlToTargetBuildGeneratorEClass, AadlToTargetBuildGenerator.class, "AadlToTargetBuildGenerator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAadlToTargetBuildGenerator_CodeGenWorkflowComponent(), this.getCodegen(), this.getCodegen_AadlToTargetBuild(), "codeGenWorkflowComponent", null, 1, 1, AadlToTargetBuildGenerator.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getAadlToTargetBuildGenerator__GenerateSystemBuild__EList_URI_IProgressMonitor(), null, "generateSystemBuild", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getSubcomponent(), "processor", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getURI(), "systemOutputDir", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getGenerationException());

		op = initEOperation(getAadlToTargetBuildGenerator__GenerateProcessorBuild__Subcomponent_URI_IProgressMonitor(), null, "generateProcessorBuild", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getSubcomponent(), "processor", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getURI(), "processorOutputDir", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getGenerationException());

		op = initEOperation(getAadlToTargetBuildGenerator__GenerateProcessBuild__ProcessSubcomponent_URI_IProgressMonitor(), null, "generateProcessBuild", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAadl2Package.getProcessSubcomponent(), "process", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getURI(), "processOutputDir", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getIProgressMonitor(), "monitor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, this.getGenerationException());

		op = initEOperation(getAadlToTargetBuildGenerator__ValidateTargetPath__URI(), ecorePackage.getEBoolean(), "validateTargetPath", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theHelpersPackage.getURI(), "targetPath", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAadlToTargetBuildGenerator__GetTargetShortDescription(), ecorePackage.getEString(), "getTargetShortDescription", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAadlToTargetBuildGenerator__GetTargetName(), ecorePackage.getEString(), "getTargetName", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAadlToTargetBuildGenerator__InitializeTargetBuilder(), null, "initializeTargetBuilder", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(targetPropertiesEClass, TargetProperties.class, "TargetProperties", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(transformationResourcesPairEClass, TransformationResourcesPair.class, "TransformationResourcesPair", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransformationResourcesPair_ModelTransformationTrace(), theArch_tracePackage.getArchTraceSpec(), null, "modelTransformationTrace", null, 0, 1, TransformationResourcesPair.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransformationResourcesPair_OutputModelRoot(), theEcorePackage.getEObject(), null, "outputModelRoot", null, 0, 1, TransformationResourcesPair.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractCodeGeneratorEClass, AbstractCodeGenerator.class, "AbstractCodeGenerator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractCodeGenerator_CurrentModelTransformationTrace(), theArch_tracePackage.getArchTraceSpec(), null, "currentModelTransformationTrace", null, 0, 1, AbstractCodeGenerator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aadlWriterEClass, AadlWriter.class, "AadlWriter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize data types
		initEDataType(eFileEDataType, File.class, "EFile", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(generationExceptionEDataType, GenerationException.class, "GenerationException", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(uriConverterEDataType, URIConverter.class, "URIConverter", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //WorkflowramsesPackageImpl
