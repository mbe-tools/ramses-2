/**
 */
package fr.mem4csd.ramses.core.workflowramses.impl;

import java.io.IOException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EClass;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.ModelWriterImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.workflowramses.AadlWriter;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aadl Writer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AadlWriterImpl extends ModelWriterImpl implements AadlWriter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AadlWriterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesPackage.Literals.AADL_WRITER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void execute(WorkflowExecutionContext context, IProgressMonitor monitor)
			throws WorkflowExecutionException, IOException {
		super.execute(context, monitor);
		
		if(!Platform.isRunning())
			return;
		String projectName = modelURI.substring("platform:/resource/".length());
		projectName = projectName.substring(0, projectName.indexOf("/"));
		IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = workspaceRoot.getProject(projectName);
        try {
			project.build(IncrementalProjectBuilder.INCREMENTAL_BUILD, monitor);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
} //AadlWriterImpl
