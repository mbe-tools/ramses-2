/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.impl;

import java.io.IOException;
import java.util.Iterator;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.AadlPackage;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.ModelWriterImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.ramses.core.arch_trace.ArchRefinementTrace;
import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.workflowramses.TraceWriter;
import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trace Writer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TraceWriterImpl extends ModelWriterImpl implements TraceWriter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TraceWriterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesPackage.Literals.TRACE_WRITER;
	}
	
	@Override
	public void execute(	final WorkflowExecutionContext context,
							final IProgressMonitor monitor )
	throws WorkflowExecutionException, IOException {
		final Object modelSlotObject = context.getModelSlots().get(this.modelSlot);
		
		if (modelSlotObject instanceof ArchTraceSpec) {
			final ArchTraceSpec traces = (ArchTraceSpec) modelSlotObject;
			final Iterator<ArchRefinementTrace> iterator = traces.getArchRefinementTrace().iterator();
//			for (int i = 0; i < traces.getArchRefinementTrace().size(); i++) {
			while ( iterator.hasNext() ) {
				final ArchRefinementTrace trace = iterator.next();
				final EObject target = trace.getTargetElement();
				
				// Also remove traces that trace elements that are root elements not contained in an AADL package
				if ( target.eResource() == null || (target.eContainer() == null && !( target instanceof AadlPackage ) ) ) {
					iterator.remove();
				}
			}
		}
		
		super.execute(context, monitor);
	}

} //TraceWriterImpl
