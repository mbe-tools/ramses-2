/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Code Generator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.AbstractCodeGenerator#getCurrentModelTransformationTrace <em>Current Model Transformation Trace</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getAbstractCodeGenerator()
 * @model abstract="true"
 * @generated
 */
public interface AbstractCodeGenerator extends EObject {
	/**
	 * Returns the value of the '<em><b>Current Model Transformation Trace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Model Transformation Trace</em>' reference.
	 * @see #setCurrentModelTransformationTrace(ArchTraceSpec)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getAbstractCodeGenerator_CurrentModelTransformationTrace()
	 * @model
	 * @generated
	 */
	ArchTraceSpec getCurrentModelTransformationTrace();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.AbstractCodeGenerator#getCurrentModelTransformationTrace <em>Current Model Transformation Trace</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Model Transformation Trace</em>' reference.
	 * @see #getCurrentModelTransformationTrace()
	 * @generated
	 */
	void setCurrentModelTransformationTrace(ArchTraceSpec value);

} // AbstractCodeGenerator
