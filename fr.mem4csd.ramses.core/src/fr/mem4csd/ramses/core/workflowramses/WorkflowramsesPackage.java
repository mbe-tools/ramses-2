/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses;

import de.mdelab.workflow.components.ComponentsPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesFactory
 * @model kind="package"
 * @generated
 */
public interface WorkflowramsesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowramses";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/ramses/workflowramses";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workflowramses";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowramsesPackage eINSTANCE = fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl <em>Codegen</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getCodegen()
	 * @generated
	 */
	int CODEGEN = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__ENABLED = ComponentsPackage.WORKFLOW_COMPONENT__ENABLED;

	/**
	 * The feature id for the '<em><b>Debug Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__DEBUG_OUTPUT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Aadl Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__AADL_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Trace Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__TRACE_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__OUTPUT_DIRECTORY = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Target Install Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__TARGET_INSTALL_DIR = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Include Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__INCLUDE_DIR = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Core Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__CORE_RUNTIME_DIR = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Target Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__TARGET_RUNTIME_DIR = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Aadl To Source Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__AADL_TO_SOURCE_CODE = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Aadl To Target Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__AADL_TO_TARGET_CONFIGURATION = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Aadl To Target Build</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__AADL_TO_TARGET_BUILD = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Transformation Resources Pair List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__TRANSFORMATION_RESOURCES_PAIR_LIST = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Target Properties</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__TARGET_PROPERTIES = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Progress Monitor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__PROGRESS_MONITOR = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Uri Converter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__URI_CONVERTER = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Target Install Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__TARGET_INSTALL_DIRECTORY_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Output Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__OUTPUT_DIRECTORY_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Core Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__CORE_RUNTIME_DIRECTORY_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Target Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__TARGET_RUNTIME_DIRECTORY_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Include Directory URI List</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN__INCLUDE_DIRECTORY_URI_LIST = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 19;

	/**
	 * The number of structural features of the '<em>Codegen</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 20;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int CODEGEN___EXECUTE__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN___CHECK_CANCELED__IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Get Model Transformation Traces List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN___GET_MODEL_TRANSFORMATION_TRACES_LIST = ComponentsPackage.WORKFLOW_COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Target Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN___GET_TARGET_NAME = ComponentsPackage.WORKFLOW_COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Codegen</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_OPERATION_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.TraceWriterImpl <em>Trace Writer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.TraceWriterImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getTraceWriter()
	 * @generated
	 */
	int TRACE_WRITER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__NAME = ComponentsPackage.MODEL_WRITER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__DESCRIPTION = ComponentsPackage.MODEL_WRITER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__ENABLED = ComponentsPackage.MODEL_WRITER__ENABLED;

	/**
	 * The feature id for the '<em><b>Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__MODEL_SLOT = ComponentsPackage.MODEL_WRITER__MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__MODEL_URI = ComponentsPackage.MODEL_WRITER__MODEL_URI;

	/**
	 * The feature id for the '<em><b>Clone Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__CLONE_MODEL = ComponentsPackage.MODEL_WRITER__CLONE_MODEL;

	/**
	 * The feature id for the '<em><b>Save Schema Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__SAVE_SCHEMA_LOCATION = ComponentsPackage.MODEL_WRITER__SAVE_SCHEMA_LOCATION;

	/**
	 * The feature id for the '<em><b>Overwrite</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__OVERWRITE = ComponentsPackage.MODEL_WRITER__OVERWRITE;

	/**
	 * The feature id for the '<em><b>Uri Resolvers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__URI_RESOLVERS = ComponentsPackage.MODEL_WRITER__URI_RESOLVERS;

	/**
	 * The feature id for the '<em><b>Deresolve Plugin UR Is</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__DERESOLVE_PLUGIN_UR_IS = ComponentsPackage.MODEL_WRITER__DERESOLVE_PLUGIN_UR_IS;

	/**
	 * The feature id for the '<em><b>Unload After</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__UNLOAD_AFTER = ComponentsPackage.MODEL_WRITER__UNLOAD_AFTER;

	/**
	 * The feature id for the '<em><b>Resolve URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER__RESOLVE_URI = ComponentsPackage.MODEL_WRITER__RESOLVE_URI;

	/**
	 * The number of structural features of the '<em>Trace Writer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER_FEATURE_COUNT = ComponentsPackage.MODEL_WRITER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.MODEL_WRITER___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int TRACE_WRITER___EXECUTE__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.MODEL_WRITER___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = ComponentsPackage.MODEL_WRITER___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER___CHECK_CANCELED__IPROGRESSMONITOR = ComponentsPackage.MODEL_WRITER___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Trace Writer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_WRITER_OPERATION_COUNT = ComponentsPackage.MODEL_WRITER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationImpl <em>Condition Evaluation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getConditionEvaluation()
	 * @generated
	 */
	int CONDITION_EVALUATION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION__ENABLED = ComponentsPackage.WORKFLOW_COMPONENT__ENABLED;

	/**
	 * The feature id for the '<em><b>Instance Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION__INSTANCE_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Result Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION__RESULT_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Condition Evaluation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int CONDITION_EVALUATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION___CHECK_CANCELED__IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Condition Evaluation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_OPERATION_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.ClearValidationErrorsImpl <em>Clear Validation Errors</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.ClearValidationErrorsImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getClearValidationErrors()
	 * @generated
	 */
	int CLEAR_VALIDATION_ERRORS = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_VALIDATION_ERRORS__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_VALIDATION_ERRORS__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_VALIDATION_ERRORS__ENABLED = ComponentsPackage.WORKFLOW_COMPONENT__ENABLED;

	/**
	 * The feature id for the '<em><b>Validation Report Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_URI = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Clear Validation Errors</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_VALIDATION_ERRORS_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_VALIDATION_ERRORS___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int CLEAR_VALIDATION_ERRORS___EXECUTE__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_VALIDATION_ERRORS___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_VALIDATION_ERRORS___CHECK_CANCELED__IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Clear Validation Errors</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_VALIDATION_ERRORS_OPERATION_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.ReportValidationErrorsImpl <em>Report Validation Errors</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.ReportValidationErrorsImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getReportValidationErrors()
	 * @generated
	 */
	int REPORT_VALIDATION_ERRORS = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_VALIDATION_ERRORS__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_VALIDATION_ERRORS__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_VALIDATION_ERRORS__ENABLED = ComponentsPackage.WORKFLOW_COMPONENT__ENABLED;

	/**
	 * The feature id for the '<em><b>Validation Report Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has Error Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_VALIDATION_ERRORS__HAS_ERROR_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Report Validation Errors</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_VALIDATION_ERRORS_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_VALIDATION_ERRORS___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int REPORT_VALIDATION_ERRORS___EXECUTE__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_VALIDATION_ERRORS___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_VALIDATION_ERRORS___CHECK_CANCELED__IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Report Validation Errors</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_VALIDATION_ERRORS_OPERATION_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationTargetImpl <em>Condition Evaluation Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationTargetImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getConditionEvaluationTarget()
	 * @generated
	 */
	int CONDITION_EVALUATION_TARGET = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_TARGET__NAME = CONDITION_EVALUATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_TARGET__DESCRIPTION = CONDITION_EVALUATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_TARGET__ENABLED = CONDITION_EVALUATION__ENABLED;

	/**
	 * The feature id for the '<em><b>Instance Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_TARGET__INSTANCE_MODEL_SLOT = CONDITION_EVALUATION__INSTANCE_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Result Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_TARGET__RESULT_MODEL_SLOT = CONDITION_EVALUATION__RESULT_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_TARGET__TARGET = CONDITION_EVALUATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Condition Evaluation Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_TARGET_FEATURE_COUNT = CONDITION_EVALUATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_TARGET___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = CONDITION_EVALUATION___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int CONDITION_EVALUATION_TARGET___EXECUTE__WORKFLOWEXECUTIONCONTEXT = CONDITION_EVALUATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_TARGET___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = CONDITION_EVALUATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_TARGET___CHECK_CANCELED__IPROGRESSMONITOR = CONDITION_EVALUATION___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Condition Evaluation Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_TARGET_OPERATION_COUNT = CONDITION_EVALUATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationProtocolImpl <em>Condition Evaluation Protocol</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationProtocolImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getConditionEvaluationProtocol()
	 * @generated
	 */
	int CONDITION_EVALUATION_PROTOCOL = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_PROTOCOL__NAME = CONDITION_EVALUATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_PROTOCOL__DESCRIPTION = CONDITION_EVALUATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_PROTOCOL__ENABLED = CONDITION_EVALUATION__ENABLED;

	/**
	 * The feature id for the '<em><b>Instance Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_PROTOCOL__INSTANCE_MODEL_SLOT = CONDITION_EVALUATION__INSTANCE_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Result Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_PROTOCOL__RESULT_MODEL_SLOT = CONDITION_EVALUATION__RESULT_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Protocols</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_PROTOCOL__PROTOCOLS = CONDITION_EVALUATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Condition Evaluation Protocol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_PROTOCOL_FEATURE_COUNT = CONDITION_EVALUATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_PROTOCOL___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = CONDITION_EVALUATION___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int CONDITION_EVALUATION_PROTOCOL___EXECUTE__WORKFLOWEXECUTIONCONTEXT = CONDITION_EVALUATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_PROTOCOL___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = CONDITION_EVALUATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_PROTOCOL___CHECK_CANCELED__IPROGRESSMONITOR = CONDITION_EVALUATION___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Condition Evaluation Protocol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_PROTOCOL_OPERATION_COUNT = CONDITION_EVALUATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.AbstractCodeGeneratorImpl <em>Abstract Code Generator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.AbstractCodeGeneratorImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getAbstractCodeGenerator()
	 * @generated
	 */
	int ABSTRACT_CODE_GENERATOR = 12;

	/**
	 * The feature id for the '<em><b>Current Model Transformation Trace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE = 0;

	/**
	 * The number of structural features of the '<em>Abstract Code Generator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CODE_GENERATOR_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Abstract Code Generator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CODE_GENERATOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.AadlToSourceCodeGeneratorImpl <em>Aadl To Source Code Generator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.AadlToSourceCodeGeneratorImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getAadlToSourceCodeGenerator()
	 * @generated
	 */
	int AADL_TO_SOURCE_CODE_GENERATOR = 7;

	/**
	 * The feature id for the '<em><b>Current Model Transformation Trace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_SOURCE_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE = ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE;

	/**
	 * The number of structural features of the '<em>Aadl To Source Code Generator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_SOURCE_CODE_GENERATOR_FEATURE_COUNT = ABSTRACT_CODE_GENERATOR_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Generate Source Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_SOURCE_CODE_GENERATOR___GENERATE_SOURCE_CODE__ELEMENT_URI_IPROGRESSMONITOR = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aadl To Source Code Generator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_SOURCE_CODE_GENERATOR_OPERATION_COUNT = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.AadlToTargetConfigurationGeneratorImpl <em>Aadl To Target Configuration Generator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.AadlToTargetConfigurationGeneratorImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getAadlToTargetConfigurationGenerator()
	 * @generated
	 */
	int AADL_TO_TARGET_CONFIGURATION_GENERATOR = 8;

	/**
	 * The feature id for the '<em><b>Current Model Transformation Trace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_CONFIGURATION_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE = ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE;

	/**
	 * The feature id for the '<em><b>Code Gen Workflow Component</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_CONFIGURATION_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT = ABSTRACT_CODE_GENERATOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Aadl To Target Configuration Generator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_CONFIGURATION_GENERATOR_FEATURE_COUNT = ABSTRACT_CODE_GENERATOR_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get System Target Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_CONFIGURATION_GENERATOR___GET_SYSTEM_TARGET_CONFIGURATION__ELIST_IPROGRESSMONITOR = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Generate Processor Target Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_CONFIGURATION_GENERATOR___GENERATE_PROCESSOR_TARGET_CONFIGURATION__SUBCOMPONENT_URI_IPROGRESSMONITOR = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Generate Process Target Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_CONFIGURATION_GENERATOR___GENERATE_PROCESS_TARGET_CONFIGURATION__PROCESSSUBCOMPONENT_URI_IPROGRESSMONITOR = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Aadl To Target Configuration Generator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_CONFIGURATION_GENERATOR_OPERATION_COUNT = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.AadlToTargetBuildGeneratorImpl <em>Aadl To Target Build Generator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.AadlToTargetBuildGeneratorImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getAadlToTargetBuildGenerator()
	 * @generated
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR = 9;

	/**
	 * The feature id for the '<em><b>Current Model Transformation Trace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE = ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE;

	/**
	 * The feature id for the '<em><b>Code Gen Workflow Component</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT = ABSTRACT_CODE_GENERATOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Aadl To Target Build Generator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR_FEATURE_COUNT = ABSTRACT_CODE_GENERATOR_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Generate System Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_SYSTEM_BUILD__ELIST_URI_IPROGRESSMONITOR = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Generate Processor Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_PROCESSOR_BUILD__SUBCOMPONENT_URI_IPROGRESSMONITOR = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Generate Process Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_PROCESS_BUILD__PROCESSSUBCOMPONENT_URI_IPROGRESSMONITOR = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Validate Target Path</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR___VALIDATE_TARGET_PATH__URI = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Target Short Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR___GET_TARGET_SHORT_DESCRIPTION = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Target Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR___GET_TARGET_NAME = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Initialize Target Builder</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR___INITIALIZE_TARGET_BUILDER = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 6;

	/**
	 * The number of operations of the '<em>Aadl To Target Build Generator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_TO_TARGET_BUILD_GENERATOR_OPERATION_COUNT = ABSTRACT_CODE_GENERATOR_OPERATION_COUNT + 7;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.TargetPropertiesImpl <em>Target Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.TargetPropertiesImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getTargetProperties()
	 * @generated
	 */
	int TARGET_PROPERTIES = 10;

	/**
	 * The number of structural features of the '<em>Target Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_PROPERTIES_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Target Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_PROPERTIES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.TransformationResourcesPairImpl <em>Transformation Resources Pair</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.TransformationResourcesPairImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getTransformationResourcesPair()
	 * @generated
	 */
	int TRANSFORMATION_RESOURCES_PAIR = 11;

	/**
	 * The feature id for the '<em><b>Model Transformation Trace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_RESOURCES_PAIR__MODEL_TRANSFORMATION_TRACE = 0;

	/**
	 * The feature id for the '<em><b>Output Model Root</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_RESOURCES_PAIR__OUTPUT_MODEL_ROOT = 1;

	/**
	 * The number of structural features of the '<em>Transformation Resources Pair</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_RESOURCES_PAIR_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Transformation Resources Pair</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_RESOURCES_PAIR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.AadlWriterImpl <em>Aadl Writer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.AadlWriterImpl
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getAadlWriter()
	 * @generated
	 */
	int AADL_WRITER = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__NAME = ComponentsPackage.MODEL_WRITER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__DESCRIPTION = ComponentsPackage.MODEL_WRITER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__ENABLED = ComponentsPackage.MODEL_WRITER__ENABLED;

	/**
	 * The feature id for the '<em><b>Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__MODEL_SLOT = ComponentsPackage.MODEL_WRITER__MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__MODEL_URI = ComponentsPackage.MODEL_WRITER__MODEL_URI;

	/**
	 * The feature id for the '<em><b>Clone Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__CLONE_MODEL = ComponentsPackage.MODEL_WRITER__CLONE_MODEL;

	/**
	 * The feature id for the '<em><b>Save Schema Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__SAVE_SCHEMA_LOCATION = ComponentsPackage.MODEL_WRITER__SAVE_SCHEMA_LOCATION;

	/**
	 * The feature id for the '<em><b>Overwrite</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__OVERWRITE = ComponentsPackage.MODEL_WRITER__OVERWRITE;

	/**
	 * The feature id for the '<em><b>Uri Resolvers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__URI_RESOLVERS = ComponentsPackage.MODEL_WRITER__URI_RESOLVERS;

	/**
	 * The feature id for the '<em><b>Deresolve Plugin UR Is</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__DERESOLVE_PLUGIN_UR_IS = ComponentsPackage.MODEL_WRITER__DERESOLVE_PLUGIN_UR_IS;

	/**
	 * The feature id for the '<em><b>Unload After</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__UNLOAD_AFTER = ComponentsPackage.MODEL_WRITER__UNLOAD_AFTER;

	/**
	 * The feature id for the '<em><b>Resolve URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER__RESOLVE_URI = ComponentsPackage.MODEL_WRITER__RESOLVE_URI;

	/**
	 * The number of structural features of the '<em>Aadl Writer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER_FEATURE_COUNT = ComponentsPackage.MODEL_WRITER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.MODEL_WRITER___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int AADL_WRITER___EXECUTE__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.MODEL_WRITER___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = ComponentsPackage.MODEL_WRITER___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER___CHECK_CANCELED__IPROGRESSMONITOR = ComponentsPackage.MODEL_WRITER___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Aadl Writer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_WRITER_OPERATION_COUNT = ComponentsPackage.MODEL_WRITER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '<em>EFile</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.io.File
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getEFile()
	 * @generated
	 */
	int EFILE = 14;

	/**
	 * The meta object id for the '<em>Generation Exception</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.core.codegen.GenerationException
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getGenerationException()
	 * @generated
	 */
	int GENERATION_EXCEPTION = 15;

	/**
	 * The meta object id for the '<em>URI Converter</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.resource.URIConverter
	 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getURIConverter()
	 * @generated
	 */
	int URI_CONVERTER = 16;

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.Codegen <em>Codegen</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Codegen</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen
	 * @generated
	 */
	EClass getCodegen();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#isDebugOutput <em>Debug Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Debug Output</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#isDebugOutput()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_DebugOutput();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlModelSlot <em>Aadl Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Aadl Model Slot</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlModelSlot()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_AadlModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTraceModelSlot <em>Trace Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trace Model Slot</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getTraceModelSlot()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_TraceModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getOutputDirectory <em>Output Directory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Directory</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getOutputDirectory()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_OutputDirectory();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetInstallDir <em>Target Install Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Install Dir</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetInstallDir()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_TargetInstallDir();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getIncludeDir <em>Include Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Include Dir</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getIncludeDir()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_IncludeDir();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getCoreRuntimeDir <em>Core Runtime Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Core Runtime Dir</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getCoreRuntimeDir()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_CoreRuntimeDir();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetRuntimeDir <em>Target Runtime Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Runtime Dir</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetRuntimeDir()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_TargetRuntimeDir();

	/**
	 * Returns the meta object for the containment reference '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToSourceCode <em>Aadl To Source Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Aadl To Source Code</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToSourceCode()
	 * @see #getCodegen()
	 * @generated
	 */
	EReference getCodegen_AadlToSourceCode();

	/**
	 * Returns the meta object for the containment reference '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToTargetConfiguration <em>Aadl To Target Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Aadl To Target Configuration</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToTargetConfiguration()
	 * @see #getCodegen()
	 * @generated
	 */
	EReference getCodegen_AadlToTargetConfiguration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToTargetBuild <em>Aadl To Target Build</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Aadl To Target Build</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToTargetBuild()
	 * @see #getCodegen()
	 * @generated
	 */
	EReference getCodegen_AadlToTargetBuild();

	/**
	 * Returns the meta object for the reference list '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTransformationResourcesPairList <em>Transformation Resources Pair List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Transformation Resources Pair List</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getTransformationResourcesPairList()
	 * @see #getCodegen()
	 * @generated
	 */
	EReference getCodegen_TransformationResourcesPairList();

	/**
	 * Returns the meta object for the reference '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetProperties <em>Target Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Properties</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetProperties()
	 * @see #getCodegen()
	 * @generated
	 */
	EReference getCodegen_TargetProperties();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getProgressMonitor <em>Progress Monitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Progress Monitor</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getProgressMonitor()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_ProgressMonitor();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getUriConverter <em>Uri Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uri Converter</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getUriConverter()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_UriConverter();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetInstallDirectoryURI <em>Target Install Directory URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Install Directory URI</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetInstallDirectoryURI()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_TargetInstallDirectoryURI();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getOutputDirectoryURI <em>Output Directory URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Directory URI</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getOutputDirectoryURI()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_OutputDirectoryURI();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getCoreRuntimeDirectoryURI <em>Core Runtime Directory URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Core Runtime Directory URI</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getCoreRuntimeDirectoryURI()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_CoreRuntimeDirectoryURI();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetRuntimeDirectoryURI <em>Target Runtime Directory URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Runtime Directory URI</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetRuntimeDirectoryURI()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_TargetRuntimeDirectoryURI();

	/**
	 * Returns the meta object for the attribute list '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getIncludeDirectoryURIList <em>Include Directory URI List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Include Directory URI List</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getIncludeDirectoryURIList()
	 * @see #getCodegen()
	 * @generated
	 */
	EAttribute getCodegen_IncludeDirectoryURIList();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getModelTransformationTracesList() <em>Get Model Transformation Traces List</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Model Transformation Traces List</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getModelTransformationTracesList()
	 * @generated
	 */
	EOperation getCodegen__GetModelTransformationTracesList();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetName() <em>Get Target Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Target Name</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetName()
	 * @generated
	 */
	EOperation getCodegen__GetTargetName();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.TraceWriter <em>Trace Writer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Trace Writer</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.TraceWriter
	 * @generated
	 */
	EClass getTraceWriter();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation <em>Condition Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition Evaluation</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation
	 * @generated
	 */
	EClass getConditionEvaluation();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation#getInstanceModelSlot <em>Instance Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Instance Model Slot</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation#getInstanceModelSlot()
	 * @see #getConditionEvaluation()
	 * @generated
	 */
	EAttribute getConditionEvaluation_InstanceModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation#getResultModelSlot <em>Result Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result Model Slot</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation#getResultModelSlot()
	 * @see #getConditionEvaluation()
	 * @generated
	 */
	EAttribute getConditionEvaluation_ResultModelSlot();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.ClearValidationErrors <em>Clear Validation Errors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Clear Validation Errors</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ClearValidationErrors
	 * @generated
	 */
	EClass getClearValidationErrors();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.ClearValidationErrors#getValidationReportModelURI <em>Validation Report Model URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Validation Report Model URI</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ClearValidationErrors#getValidationReportModelURI()
	 * @see #getClearValidationErrors()
	 * @generated
	 */
	EAttribute getClearValidationErrors_ValidationReportModelURI();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors <em>Report Validation Errors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Report Validation Errors</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors
	 * @generated
	 */
	EClass getReportValidationErrors();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors#getValidationReportModelSlot <em>Validation Report Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Validation Report Model Slot</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors#getValidationReportModelSlot()
	 * @see #getReportValidationErrors()
	 * @generated
	 */
	EAttribute getReportValidationErrors_ValidationReportModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors#getHasErrorModelSlot <em>Has Error Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Error Model Slot</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors#getHasErrorModelSlot()
	 * @see #getReportValidationErrors()
	 * @generated
	 */
	EAttribute getReportValidationErrors_HasErrorModelSlot();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationTarget <em>Condition Evaluation Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition Evaluation Target</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationTarget
	 * @generated
	 */
	EClass getConditionEvaluationTarget();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationTarget#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationTarget#getTarget()
	 * @see #getConditionEvaluationTarget()
	 * @generated
	 */
	EAttribute getConditionEvaluationTarget_Target();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationProtocol <em>Condition Evaluation Protocol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition Evaluation Protocol</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationProtocol
	 * @generated
	 */
	EClass getConditionEvaluationProtocol();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationProtocol#getProtocols <em>Protocols</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protocols</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationProtocol#getProtocols()
	 * @see #getConditionEvaluationProtocol()
	 * @generated
	 */
	EAttribute getConditionEvaluationProtocol_Protocols();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.AadlToSourceCodeGenerator <em>Aadl To Source Code Generator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aadl To Source Code Generator</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToSourceCodeGenerator
	 * @generated
	 */
	EClass getAadlToSourceCodeGenerator();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToSourceCodeGenerator#generateSourceCode(org.osate.aadl2.Element, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor) <em>Generate Source Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Source Code</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToSourceCodeGenerator#generateSourceCode(org.osate.aadl2.Element, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor)
	 * @generated
	 */
	EOperation getAadlToSourceCodeGenerator__GenerateSourceCode__Element_URI_IProgressMonitor();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator <em>Aadl To Target Configuration Generator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aadl To Target Configuration Generator</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator
	 * @generated
	 */
	EClass getAadlToTargetConfigurationGenerator();

	/**
	 * Returns the meta object for the container reference '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator#getCodeGenWorkflowComponent <em>Code Gen Workflow Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Code Gen Workflow Component</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator#getCodeGenWorkflowComponent()
	 * @see #getAadlToTargetConfigurationGenerator()
	 * @generated
	 */
	EReference getAadlToTargetConfigurationGenerator_CodeGenWorkflowComponent();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator#getSystemTargetConfiguration(org.eclipse.emf.common.util.EList, org.eclipse.core.runtime.IProgressMonitor) <em>Get System Target Configuration</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get System Target Configuration</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator#getSystemTargetConfiguration(org.eclipse.emf.common.util.EList, org.eclipse.core.runtime.IProgressMonitor)
	 * @generated
	 */
	EOperation getAadlToTargetConfigurationGenerator__GetSystemTargetConfiguration__EList_IProgressMonitor();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator#generateProcessorTargetConfiguration(org.osate.aadl2.Subcomponent, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor) <em>Generate Processor Target Configuration</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Processor Target Configuration</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator#generateProcessorTargetConfiguration(org.osate.aadl2.Subcomponent, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor)
	 * @generated
	 */
	EOperation getAadlToTargetConfigurationGenerator__GenerateProcessorTargetConfiguration__Subcomponent_URI_IProgressMonitor();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator#generateProcessTargetConfiguration(org.osate.aadl2.ProcessSubcomponent, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor) <em>Generate Process Target Configuration</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Process Target Configuration</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator#generateProcessTargetConfiguration(org.osate.aadl2.ProcessSubcomponent, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor)
	 * @generated
	 */
	EOperation getAadlToTargetConfigurationGenerator__GenerateProcessTargetConfiguration__ProcessSubcomponent_URI_IProgressMonitor();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator <em>Aadl To Target Build Generator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aadl To Target Build Generator</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator
	 * @generated
	 */
	EClass getAadlToTargetBuildGenerator();

	/**
	 * Returns the meta object for the container reference '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#getCodeGenWorkflowComponent <em>Code Gen Workflow Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Code Gen Workflow Component</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#getCodeGenWorkflowComponent()
	 * @see #getAadlToTargetBuildGenerator()
	 * @generated
	 */
	EReference getAadlToTargetBuildGenerator_CodeGenWorkflowComponent();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#generateSystemBuild(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor) <em>Generate System Build</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate System Build</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#generateSystemBuild(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor)
	 * @generated
	 */
	EOperation getAadlToTargetBuildGenerator__GenerateSystemBuild__EList_URI_IProgressMonitor();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#generateProcessorBuild(org.osate.aadl2.Subcomponent, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor) <em>Generate Processor Build</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Processor Build</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#generateProcessorBuild(org.osate.aadl2.Subcomponent, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor)
	 * @generated
	 */
	EOperation getAadlToTargetBuildGenerator__GenerateProcessorBuild__Subcomponent_URI_IProgressMonitor();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#generateProcessBuild(org.osate.aadl2.ProcessSubcomponent, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor) <em>Generate Process Build</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Process Build</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#generateProcessBuild(org.osate.aadl2.ProcessSubcomponent, org.eclipse.emf.common.util.URI, org.eclipse.core.runtime.IProgressMonitor)
	 * @generated
	 */
	EOperation getAadlToTargetBuildGenerator__GenerateProcessBuild__ProcessSubcomponent_URI_IProgressMonitor();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#validateTargetPath(org.eclipse.emf.common.util.URI) <em>Validate Target Path</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Validate Target Path</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#validateTargetPath(org.eclipse.emf.common.util.URI)
	 * @generated
	 */
	EOperation getAadlToTargetBuildGenerator__ValidateTargetPath__URI();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#getTargetShortDescription() <em>Get Target Short Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Target Short Description</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#getTargetShortDescription()
	 * @generated
	 */
	EOperation getAadlToTargetBuildGenerator__GetTargetShortDescription();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#getTargetName() <em>Get Target Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Target Name</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#getTargetName()
	 * @generated
	 */
	EOperation getAadlToTargetBuildGenerator__GetTargetName();

	/**
	 * Returns the meta object for the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#initializeTargetBuilder() <em>Initialize Target Builder</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initialize Target Builder</em>' operation.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#initializeTargetBuilder()
	 * @generated
	 */
	EOperation getAadlToTargetBuildGenerator__InitializeTargetBuilder();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.TargetProperties <em>Target Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Target Properties</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.TargetProperties
	 * @generated
	 */
	EClass getTargetProperties();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair <em>Transformation Resources Pair</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transformation Resources Pair</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair
	 * @generated
	 */
	EClass getTransformationResourcesPair();

	/**
	 * Returns the meta object for the reference '{@link fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair#getModelTransformationTrace <em>Model Transformation Trace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Model Transformation Trace</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair#getModelTransformationTrace()
	 * @see #getTransformationResourcesPair()
	 * @generated
	 */
	EReference getTransformationResourcesPair_ModelTransformationTrace();

	/**
	 * Returns the meta object for the reference '{@link fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair#getOutputModelRoot <em>Output Model Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Output Model Root</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair#getOutputModelRoot()
	 * @see #getTransformationResourcesPair()
	 * @generated
	 */
	EReference getTransformationResourcesPair_OutputModelRoot();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.AbstractCodeGenerator <em>Abstract Code Generator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Code Generator</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.AbstractCodeGenerator
	 * @generated
	 */
	EClass getAbstractCodeGenerator();

	/**
	 * Returns the meta object for the reference '{@link fr.mem4csd.ramses.core.workflowramses.AbstractCodeGenerator#getCurrentModelTransformationTrace <em>Current Model Transformation Trace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Model Transformation Trace</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.AbstractCodeGenerator#getCurrentModelTransformationTrace()
	 * @see #getAbstractCodeGenerator()
	 * @generated
	 */
	EReference getAbstractCodeGenerator_CurrentModelTransformationTrace();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.core.workflowramses.AadlWriter <em>Aadl Writer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aadl Writer</em>'.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlWriter
	 * @generated
	 */
	EClass getAadlWriter();

	/**
	 * Returns the meta object for data type '{@link java.io.File <em>EFile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EFile</em>'.
	 * @see java.io.File
	 * @model instanceClass="java.io.File"
	 * @generated
	 */
	EDataType getEFile();

	/**
	 * Returns the meta object for data type '{@link fr.mem4csd.ramses.core.codegen.GenerationException <em>Generation Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Generation Exception</em>'.
	 * @see fr.mem4csd.ramses.core.codegen.GenerationException
	 * @model instanceClass="fr.mem4csd.ramses.core.codegen.GenerationException"
	 * @generated
	 */
	EDataType getGenerationException();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.resource.URIConverter <em>URI Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>URI Converter</em>'.
	 * @see org.eclipse.emf.ecore.resource.URIConverter
	 * @model instanceClass="org.eclipse.emf.ecore.resource.URIConverter"
	 * @generated
	 */
	EDataType getURIConverter();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkflowramsesFactory getWorkflowramsesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl <em>Codegen</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getCodegen()
		 * @generated
		 */
		EClass CODEGEN = eINSTANCE.getCodegen();
		/**
		 * The meta object literal for the '<em><b>Debug Output</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__DEBUG_OUTPUT = eINSTANCE.getCodegen_DebugOutput();
		/**
		 * The meta object literal for the '<em><b>Aadl Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__AADL_MODEL_SLOT = eINSTANCE.getCodegen_AadlModelSlot();
		/**
		 * The meta object literal for the '<em><b>Trace Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__TRACE_MODEL_SLOT = eINSTANCE.getCodegen_TraceModelSlot();
		/**
		 * The meta object literal for the '<em><b>Output Directory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__OUTPUT_DIRECTORY = eINSTANCE.getCodegen_OutputDirectory();
		/**
		 * The meta object literal for the '<em><b>Target Install Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__TARGET_INSTALL_DIR = eINSTANCE.getCodegen_TargetInstallDir();
		/**
		 * The meta object literal for the '<em><b>Include Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__INCLUDE_DIR = eINSTANCE.getCodegen_IncludeDir();
		/**
		 * The meta object literal for the '<em><b>Core Runtime Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__CORE_RUNTIME_DIR = eINSTANCE.getCodegen_CoreRuntimeDir();
		/**
		 * The meta object literal for the '<em><b>Target Runtime Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__TARGET_RUNTIME_DIR = eINSTANCE.getCodegen_TargetRuntimeDir();
		/**
		 * The meta object literal for the '<em><b>Aadl To Source Code</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODEGEN__AADL_TO_SOURCE_CODE = eINSTANCE.getCodegen_AadlToSourceCode();
		/**
		 * The meta object literal for the '<em><b>Aadl To Target Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODEGEN__AADL_TO_TARGET_CONFIGURATION = eINSTANCE.getCodegen_AadlToTargetConfiguration();
		/**
		 * The meta object literal for the '<em><b>Aadl To Target Build</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODEGEN__AADL_TO_TARGET_BUILD = eINSTANCE.getCodegen_AadlToTargetBuild();
		/**
		 * The meta object literal for the '<em><b>Transformation Resources Pair List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODEGEN__TRANSFORMATION_RESOURCES_PAIR_LIST = eINSTANCE.getCodegen_TransformationResourcesPairList();
		/**
		 * The meta object literal for the '<em><b>Target Properties</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODEGEN__TARGET_PROPERTIES = eINSTANCE.getCodegen_TargetProperties();
		/**
		 * The meta object literal for the '<em><b>Progress Monitor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__PROGRESS_MONITOR = eINSTANCE.getCodegen_ProgressMonitor();
		/**
		 * The meta object literal for the '<em><b>Uri Converter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__URI_CONVERTER = eINSTANCE.getCodegen_UriConverter();
		/**
		 * The meta object literal for the '<em><b>Target Install Directory URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__TARGET_INSTALL_DIRECTORY_URI = eINSTANCE.getCodegen_TargetInstallDirectoryURI();
		/**
		 * The meta object literal for the '<em><b>Output Directory URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__OUTPUT_DIRECTORY_URI = eINSTANCE.getCodegen_OutputDirectoryURI();
		/**
		 * The meta object literal for the '<em><b>Core Runtime Directory URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__CORE_RUNTIME_DIRECTORY_URI = eINSTANCE.getCodegen_CoreRuntimeDirectoryURI();
		/**
		 * The meta object literal for the '<em><b>Target Runtime Directory URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__TARGET_RUNTIME_DIRECTORY_URI = eINSTANCE.getCodegen_TargetRuntimeDirectoryURI();
		/**
		 * The meta object literal for the '<em><b>Include Directory URI List</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN__INCLUDE_DIRECTORY_URI_LIST = eINSTANCE.getCodegen_IncludeDirectoryURIList();
		/**
		 * The meta object literal for the '<em><b>Get Model Transformation Traces List</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CODEGEN___GET_MODEL_TRANSFORMATION_TRACES_LIST = eINSTANCE.getCodegen__GetModelTransformationTracesList();
		/**
		 * The meta object literal for the '<em><b>Get Target Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CODEGEN___GET_TARGET_NAME = eINSTANCE.getCodegen__GetTargetName();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.TraceWriterImpl <em>Trace Writer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.TraceWriterImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getTraceWriter()
		 * @generated
		 */
		EClass TRACE_WRITER = eINSTANCE.getTraceWriter();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationImpl <em>Condition Evaluation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getConditionEvaluation()
		 * @generated
		 */
		EClass CONDITION_EVALUATION = eINSTANCE.getConditionEvaluation();
		/**
		 * The meta object literal for the '<em><b>Instance Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION_EVALUATION__INSTANCE_MODEL_SLOT = eINSTANCE.getConditionEvaluation_InstanceModelSlot();
		/**
		 * The meta object literal for the '<em><b>Result Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION_EVALUATION__RESULT_MODEL_SLOT = eINSTANCE.getConditionEvaluation_ResultModelSlot();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.ClearValidationErrorsImpl <em>Clear Validation Errors</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.ClearValidationErrorsImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getClearValidationErrors()
		 * @generated
		 */
		EClass CLEAR_VALIDATION_ERRORS = eINSTANCE.getClearValidationErrors();
		/**
		 * The meta object literal for the '<em><b>Validation Report Model URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLEAR_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_URI = eINSTANCE.getClearValidationErrors_ValidationReportModelURI();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.ReportValidationErrorsImpl <em>Report Validation Errors</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.ReportValidationErrorsImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getReportValidationErrors()
		 * @generated
		 */
		EClass REPORT_VALIDATION_ERRORS = eINSTANCE.getReportValidationErrors();
		/**
		 * The meta object literal for the '<em><b>Validation Report Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPORT_VALIDATION_ERRORS__VALIDATION_REPORT_MODEL_SLOT = eINSTANCE.getReportValidationErrors_ValidationReportModelSlot();
		/**
		 * The meta object literal for the '<em><b>Has Error Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPORT_VALIDATION_ERRORS__HAS_ERROR_MODEL_SLOT = eINSTANCE.getReportValidationErrors_HasErrorModelSlot();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationTargetImpl <em>Condition Evaluation Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationTargetImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getConditionEvaluationTarget()
		 * @generated
		 */
		EClass CONDITION_EVALUATION_TARGET = eINSTANCE.getConditionEvaluationTarget();
		/**
		 * The meta object literal for the '<em><b>Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION_EVALUATION_TARGET__TARGET = eINSTANCE.getConditionEvaluationTarget_Target();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationProtocolImpl <em>Condition Evaluation Protocol</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationProtocolImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getConditionEvaluationProtocol()
		 * @generated
		 */
		EClass CONDITION_EVALUATION_PROTOCOL = eINSTANCE.getConditionEvaluationProtocol();
		/**
		 * The meta object literal for the '<em><b>Protocols</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION_EVALUATION_PROTOCOL__PROTOCOLS = eINSTANCE.getConditionEvaluationProtocol_Protocols();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.AadlToSourceCodeGeneratorImpl <em>Aadl To Source Code Generator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.AadlToSourceCodeGeneratorImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getAadlToSourceCodeGenerator()
		 * @generated
		 */
		EClass AADL_TO_SOURCE_CODE_GENERATOR = eINSTANCE.getAadlToSourceCodeGenerator();
		/**
		 * The meta object literal for the '<em><b>Generate Source Code</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AADL_TO_SOURCE_CODE_GENERATOR___GENERATE_SOURCE_CODE__ELEMENT_URI_IPROGRESSMONITOR = eINSTANCE.getAadlToSourceCodeGenerator__GenerateSourceCode__Element_URI_IProgressMonitor();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.AadlToTargetConfigurationGeneratorImpl <em>Aadl To Target Configuration Generator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.AadlToTargetConfigurationGeneratorImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getAadlToTargetConfigurationGenerator()
		 * @generated
		 */
		EClass AADL_TO_TARGET_CONFIGURATION_GENERATOR = eINSTANCE.getAadlToTargetConfigurationGenerator();
		/**
		 * The meta object literal for the '<em><b>Code Gen Workflow Component</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AADL_TO_TARGET_CONFIGURATION_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT = eINSTANCE.getAadlToTargetConfigurationGenerator_CodeGenWorkflowComponent();
		/**
		 * The meta object literal for the '<em><b>Get System Target Configuration</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AADL_TO_TARGET_CONFIGURATION_GENERATOR___GET_SYSTEM_TARGET_CONFIGURATION__ELIST_IPROGRESSMONITOR = eINSTANCE.getAadlToTargetConfigurationGenerator__GetSystemTargetConfiguration__EList_IProgressMonitor();
		/**
		 * The meta object literal for the '<em><b>Generate Processor Target Configuration</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AADL_TO_TARGET_CONFIGURATION_GENERATOR___GENERATE_PROCESSOR_TARGET_CONFIGURATION__SUBCOMPONENT_URI_IPROGRESSMONITOR = eINSTANCE.getAadlToTargetConfigurationGenerator__GenerateProcessorTargetConfiguration__Subcomponent_URI_IProgressMonitor();
		/**
		 * The meta object literal for the '<em><b>Generate Process Target Configuration</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AADL_TO_TARGET_CONFIGURATION_GENERATOR___GENERATE_PROCESS_TARGET_CONFIGURATION__PROCESSSUBCOMPONENT_URI_IPROGRESSMONITOR = eINSTANCE.getAadlToTargetConfigurationGenerator__GenerateProcessTargetConfiguration__ProcessSubcomponent_URI_IProgressMonitor();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.AadlToTargetBuildGeneratorImpl <em>Aadl To Target Build Generator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.AadlToTargetBuildGeneratorImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getAadlToTargetBuildGenerator()
		 * @generated
		 */
		EClass AADL_TO_TARGET_BUILD_GENERATOR = eINSTANCE.getAadlToTargetBuildGenerator();
		/**
		 * The meta object literal for the '<em><b>Code Gen Workflow Component</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AADL_TO_TARGET_BUILD_GENERATOR__CODE_GEN_WORKFLOW_COMPONENT = eINSTANCE.getAadlToTargetBuildGenerator_CodeGenWorkflowComponent();
		/**
		 * The meta object literal for the '<em><b>Generate System Build</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_SYSTEM_BUILD__ELIST_URI_IPROGRESSMONITOR = eINSTANCE.getAadlToTargetBuildGenerator__GenerateSystemBuild__EList_URI_IProgressMonitor();
		/**
		 * The meta object literal for the '<em><b>Generate Processor Build</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_PROCESSOR_BUILD__SUBCOMPONENT_URI_IPROGRESSMONITOR = eINSTANCE.getAadlToTargetBuildGenerator__GenerateProcessorBuild__Subcomponent_URI_IProgressMonitor();
		/**
		 * The meta object literal for the '<em><b>Generate Process Build</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AADL_TO_TARGET_BUILD_GENERATOR___GENERATE_PROCESS_BUILD__PROCESSSUBCOMPONENT_URI_IPROGRESSMONITOR = eINSTANCE.getAadlToTargetBuildGenerator__GenerateProcessBuild__ProcessSubcomponent_URI_IProgressMonitor();
		/**
		 * The meta object literal for the '<em><b>Validate Target Path</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AADL_TO_TARGET_BUILD_GENERATOR___VALIDATE_TARGET_PATH__URI = eINSTANCE.getAadlToTargetBuildGenerator__ValidateTargetPath__URI();
		/**
		 * The meta object literal for the '<em><b>Get Target Short Description</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AADL_TO_TARGET_BUILD_GENERATOR___GET_TARGET_SHORT_DESCRIPTION = eINSTANCE.getAadlToTargetBuildGenerator__GetTargetShortDescription();
		/**
		 * The meta object literal for the '<em><b>Get Target Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AADL_TO_TARGET_BUILD_GENERATOR___GET_TARGET_NAME = eINSTANCE.getAadlToTargetBuildGenerator__GetTargetName();
		/**
		 * The meta object literal for the '<em><b>Initialize Target Builder</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AADL_TO_TARGET_BUILD_GENERATOR___INITIALIZE_TARGET_BUILDER = eINSTANCE.getAadlToTargetBuildGenerator__InitializeTargetBuilder();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.TargetPropertiesImpl <em>Target Properties</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.TargetPropertiesImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getTargetProperties()
		 * @generated
		 */
		EClass TARGET_PROPERTIES = eINSTANCE.getTargetProperties();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.TransformationResourcesPairImpl <em>Transformation Resources Pair</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.TransformationResourcesPairImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getTransformationResourcesPair()
		 * @generated
		 */
		EClass TRANSFORMATION_RESOURCES_PAIR = eINSTANCE.getTransformationResourcesPair();
		/**
		 * The meta object literal for the '<em><b>Model Transformation Trace</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_RESOURCES_PAIR__MODEL_TRANSFORMATION_TRACE = eINSTANCE.getTransformationResourcesPair_ModelTransformationTrace();
		/**
		 * The meta object literal for the '<em><b>Output Model Root</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_RESOURCES_PAIR__OUTPUT_MODEL_ROOT = eINSTANCE.getTransformationResourcesPair_OutputModelRoot();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.AbstractCodeGeneratorImpl <em>Abstract Code Generator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.AbstractCodeGeneratorImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getAbstractCodeGenerator()
		 * @generated
		 */
		EClass ABSTRACT_CODE_GENERATOR = eINSTANCE.getAbstractCodeGenerator();
		/**
		 * The meta object literal for the '<em><b>Current Model Transformation Trace</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_CODE_GENERATOR__CURRENT_MODEL_TRANSFORMATION_TRACE = eINSTANCE.getAbstractCodeGenerator_CurrentModelTransformationTrace();
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.core.workflowramses.impl.AadlWriterImpl <em>Aadl Writer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.AadlWriterImpl
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getAadlWriter()
		 * @generated
		 */
		EClass AADL_WRITER = eINSTANCE.getAadlWriter();
		/**
		 * The meta object literal for the '<em>EFile</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.io.File
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getEFile()
		 * @generated
		 */
		EDataType EFILE = eINSTANCE.getEFile();
		/**
		 * The meta object literal for the '<em>Generation Exception</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.core.codegen.GenerationException
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getGenerationException()
		 * @generated
		 */
		EDataType GENERATION_EXCEPTION = eINSTANCE.getGenerationException();
		/**
		 * The meta object literal for the '<em>URI Converter</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.resource.URIConverter
		 * @see fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesPackageImpl#getURIConverter()
		 * @generated
		 */
		EDataType URI_CONVERTER = eINSTANCE.getURIConverter();

	}

} //WorkflowramsesPackage
