/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses;

import fr.mem4csd.ramses.core.codegen.GenerationException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;

import org.osate.aadl2.Element;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aadl To Source Code Generator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getAadlToSourceCodeGenerator()
 * @model abstract="true"
 * @generated
 */
public interface AadlToSourceCodeGenerator extends AbstractCodeGenerator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="fr.mem4csd.ramses.core.workflowramses.GenerationException" elementRequired="true" systemOutputDirDataType="de.mdelab.workflow.helpers.URI" systemOutputDirRequired="true" monitorDataType="de.mdelab.workflow.helpers.IProgressMonitor"
	 * @generated
	 */
	void generateSourceCode(Element element, URI systemOutputDir, IProgressMonitor monitor) throws GenerationException;

} // AadlToSourceCodeGenerator
