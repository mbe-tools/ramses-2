/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.util;

import de.mdelab.workflow.NamedComponent;
import de.mdelab.workflow.components.ModelWriter;
import de.mdelab.workflow.components.WorkflowComponent;
import fr.mem4csd.ramses.core.workflowramses.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage
 * @generated
 */
public class WorkflowramsesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static WorkflowramsesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramsesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = WorkflowramsesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WorkflowramsesSwitch<Adapter> modelSwitch =
		new WorkflowramsesSwitch<Adapter>() {
			@Override
			public Adapter caseCodegen(Codegen object) {
				return createCodegenAdapter();
			}
			@Override
			public Adapter caseTraceWriter(TraceWriter object) {
				return createTraceWriterAdapter();
			}
			@Override
			public Adapter caseConditionEvaluation(ConditionEvaluation object) {
				return createConditionEvaluationAdapter();
			}
			@Override
			public Adapter caseClearValidationErrors(ClearValidationErrors object) {
				return createClearValidationErrorsAdapter();
			}
			@Override
			public Adapter caseReportValidationErrors(ReportValidationErrors object) {
				return createReportValidationErrorsAdapter();
			}
			@Override
			public Adapter caseConditionEvaluationTarget(ConditionEvaluationTarget object) {
				return createConditionEvaluationTargetAdapter();
			}
			@Override
			public Adapter caseConditionEvaluationProtocol(ConditionEvaluationProtocol object) {
				return createConditionEvaluationProtocolAdapter();
			}
			@Override
			public Adapter caseAadlToSourceCodeGenerator(AadlToSourceCodeGenerator object) {
				return createAadlToSourceCodeGeneratorAdapter();
			}
			@Override
			public Adapter caseAadlToTargetConfigurationGenerator(AadlToTargetConfigurationGenerator object) {
				return createAadlToTargetConfigurationGeneratorAdapter();
			}
			@Override
			public Adapter caseAadlToTargetBuildGenerator(AadlToTargetBuildGenerator object) {
				return createAadlToTargetBuildGeneratorAdapter();
			}
			@Override
			public Adapter caseTargetProperties(TargetProperties object) {
				return createTargetPropertiesAdapter();
			}
			@Override
			public Adapter caseTransformationResourcesPair(TransformationResourcesPair object) {
				return createTransformationResourcesPairAdapter();
			}
			@Override
			public Adapter caseAbstractCodeGenerator(AbstractCodeGenerator object) {
				return createAbstractCodeGeneratorAdapter();
			}
			@Override
			public Adapter caseAadlWriter(AadlWriter object) {
				return createAadlWriterAdapter();
			}
			@Override
			public Adapter caseNamedComponent(NamedComponent object) {
				return createNamedComponentAdapter();
			}
			@Override
			public Adapter caseWorkflowComponent(WorkflowComponent object) {
				return createWorkflowComponentAdapter();
			}
			@Override
			public Adapter caseModelWriter(ModelWriter object) {
				return createModelWriterAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.Codegen <em>Codegen</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen
	 * @generated
	 */
	public Adapter createCodegenAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.TraceWriter <em>Trace Writer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.TraceWriter
	 * @generated
	 */
	public Adapter createTraceWriterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation <em>Condition Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.ConditionEvaluation
	 * @generated
	 */
	public Adapter createConditionEvaluationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.ClearValidationErrors <em>Clear Validation Errors</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.ClearValidationErrors
	 * @generated
	 */
	public Adapter createClearValidationErrorsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors <em>Report Validation Errors</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.ReportValidationErrors
	 * @generated
	 */
	public Adapter createReportValidationErrorsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationTarget <em>Condition Evaluation Target</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationTarget
	 * @generated
	 */
	public Adapter createConditionEvaluationTargetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationProtocol <em>Condition Evaluation Protocol</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.ConditionEvaluationProtocol
	 * @generated
	 */
	public Adapter createConditionEvaluationProtocolAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.AadlToSourceCodeGenerator <em>Aadl To Source Code Generator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToSourceCodeGenerator
	 * @generated
	 */
	public Adapter createAadlToSourceCodeGeneratorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator <em>Aadl To Target Configuration Generator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator
	 * @generated
	 */
	public Adapter createAadlToTargetConfigurationGeneratorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator <em>Aadl To Target Build Generator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator
	 * @generated
	 */
	public Adapter createAadlToTargetBuildGeneratorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.TargetProperties <em>Target Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.TargetProperties
	 * @generated
	 */
	public Adapter createTargetPropertiesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair <em>Transformation Resources Pair</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair
	 * @generated
	 */
	public Adapter createTransformationResourcesPairAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.AbstractCodeGenerator <em>Abstract Code Generator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.AbstractCodeGenerator
	 * @generated
	 */
	public Adapter createAbstractCodeGeneratorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.mem4csd.ramses.core.workflowramses.AadlWriter <em>Aadl Writer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlWriter
	 * @generated
	 */
	public Adapter createAadlWriterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.workflow.NamedComponent <em>Named Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.workflow.NamedComponent
	 * @generated
	 */
	public Adapter createNamedComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.workflow.components.WorkflowComponent <em>Workflow Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.workflow.components.WorkflowComponent
	 * @generated
	 */
	public Adapter createWorkflowComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.workflow.components.ModelWriter <em>Model Writer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.workflow.components.ModelWriter
	 * @generated
	 */
	public Adapter createModelWriterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //WorkflowramsesAdapterFactory
