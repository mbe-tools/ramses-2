/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses.util;

import de.mdelab.workflow.NamedComponent;
import de.mdelab.workflow.components.ModelWriter;
import de.mdelab.workflow.components.WorkflowComponent;
import fr.mem4csd.ramses.core.workflowramses.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage
 * @generated
 */
public class WorkflowramsesSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static WorkflowramsesPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowramsesSwitch() {
		if (modelPackage == null) {
			modelPackage = WorkflowramsesPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case WorkflowramsesPackage.CODEGEN: {
				Codegen codegen = (Codegen)theEObject;
				T result = caseCodegen(codegen);
				if (result == null) result = caseWorkflowComponent(codegen);
				if (result == null) result = caseNamedComponent(codegen);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.TRACE_WRITER: {
				TraceWriter traceWriter = (TraceWriter)theEObject;
				T result = caseTraceWriter(traceWriter);
				if (result == null) result = caseModelWriter(traceWriter);
				if (result == null) result = caseWorkflowComponent(traceWriter);
				if (result == null) result = caseNamedComponent(traceWriter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.CONDITION_EVALUATION: {
				ConditionEvaluation conditionEvaluation = (ConditionEvaluation)theEObject;
				T result = caseConditionEvaluation(conditionEvaluation);
				if (result == null) result = caseWorkflowComponent(conditionEvaluation);
				if (result == null) result = caseNamedComponent(conditionEvaluation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.CLEAR_VALIDATION_ERRORS: {
				ClearValidationErrors clearValidationErrors = (ClearValidationErrors)theEObject;
				T result = caseClearValidationErrors(clearValidationErrors);
				if (result == null) result = caseWorkflowComponent(clearValidationErrors);
				if (result == null) result = caseNamedComponent(clearValidationErrors);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.REPORT_VALIDATION_ERRORS: {
				ReportValidationErrors reportValidationErrors = (ReportValidationErrors)theEObject;
				T result = caseReportValidationErrors(reportValidationErrors);
				if (result == null) result = caseWorkflowComponent(reportValidationErrors);
				if (result == null) result = caseNamedComponent(reportValidationErrors);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.CONDITION_EVALUATION_TARGET: {
				ConditionEvaluationTarget conditionEvaluationTarget = (ConditionEvaluationTarget)theEObject;
				T result = caseConditionEvaluationTarget(conditionEvaluationTarget);
				if (result == null) result = caseConditionEvaluation(conditionEvaluationTarget);
				if (result == null) result = caseWorkflowComponent(conditionEvaluationTarget);
				if (result == null) result = caseNamedComponent(conditionEvaluationTarget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.CONDITION_EVALUATION_PROTOCOL: {
				ConditionEvaluationProtocol conditionEvaluationProtocol = (ConditionEvaluationProtocol)theEObject;
				T result = caseConditionEvaluationProtocol(conditionEvaluationProtocol);
				if (result == null) result = caseConditionEvaluation(conditionEvaluationProtocol);
				if (result == null) result = caseWorkflowComponent(conditionEvaluationProtocol);
				if (result == null) result = caseNamedComponent(conditionEvaluationProtocol);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.AADL_TO_SOURCE_CODE_GENERATOR: {
				AadlToSourceCodeGenerator aadlToSourceCodeGenerator = (AadlToSourceCodeGenerator)theEObject;
				T result = caseAadlToSourceCodeGenerator(aadlToSourceCodeGenerator);
				if (result == null) result = caseAbstractCodeGenerator(aadlToSourceCodeGenerator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.AADL_TO_TARGET_CONFIGURATION_GENERATOR: {
				AadlToTargetConfigurationGenerator aadlToTargetConfigurationGenerator = (AadlToTargetConfigurationGenerator)theEObject;
				T result = caseAadlToTargetConfigurationGenerator(aadlToTargetConfigurationGenerator);
				if (result == null) result = caseAbstractCodeGenerator(aadlToTargetConfigurationGenerator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.AADL_TO_TARGET_BUILD_GENERATOR: {
				AadlToTargetBuildGenerator aadlToTargetBuildGenerator = (AadlToTargetBuildGenerator)theEObject;
				T result = caseAadlToTargetBuildGenerator(aadlToTargetBuildGenerator);
				if (result == null) result = caseAbstractCodeGenerator(aadlToTargetBuildGenerator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.TARGET_PROPERTIES: {
				TargetProperties targetProperties = (TargetProperties)theEObject;
				T result = caseTargetProperties(targetProperties);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.TRANSFORMATION_RESOURCES_PAIR: {
				TransformationResourcesPair transformationResourcesPair = (TransformationResourcesPair)theEObject;
				T result = caseTransformationResourcesPair(transformationResourcesPair);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.ABSTRACT_CODE_GENERATOR: {
				AbstractCodeGenerator abstractCodeGenerator = (AbstractCodeGenerator)theEObject;
				T result = caseAbstractCodeGenerator(abstractCodeGenerator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowramsesPackage.AADL_WRITER: {
				AadlWriter aadlWriter = (AadlWriter)theEObject;
				T result = caseAadlWriter(aadlWriter);
				if (result == null) result = caseModelWriter(aadlWriter);
				if (result == null) result = caseWorkflowComponent(aadlWriter);
				if (result == null) result = caseNamedComponent(aadlWriter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Codegen</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Codegen</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodegen(Codegen object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trace Writer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trace Writer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTraceWriter(TraceWriter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Condition Evaluation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Condition Evaluation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionEvaluation(ConditionEvaluation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Clear Validation Errors</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Clear Validation Errors</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClearValidationErrors(ClearValidationErrors object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Report Validation Errors</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Report Validation Errors</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReportValidationErrors(ReportValidationErrors object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Condition Evaluation Target</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Condition Evaluation Target</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionEvaluationTarget(ConditionEvaluationTarget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Condition Evaluation Protocol</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Condition Evaluation Protocol</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionEvaluationProtocol(ConditionEvaluationProtocol object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aadl To Source Code Generator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aadl To Source Code Generator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAadlToSourceCodeGenerator(AadlToSourceCodeGenerator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aadl To Target Configuration Generator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aadl To Target Configuration Generator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAadlToTargetConfigurationGenerator(AadlToTargetConfigurationGenerator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aadl To Target Build Generator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aadl To Target Build Generator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAadlToTargetBuildGenerator(AadlToTargetBuildGenerator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Target Properties</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Target Properties</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTargetProperties(TargetProperties object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transformation Resources Pair</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transformation Resources Pair</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransformationResourcesPair(TransformationResourcesPair object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Code Generator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Code Generator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractCodeGenerator(AbstractCodeGenerator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aadl Writer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aadl Writer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAadlWriter(AadlWriter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedComponent(NamedComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Workflow Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Workflow Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorkflowComponent(WorkflowComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Writer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Writer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelWriter(ModelWriter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //WorkflowramsesSwitch
