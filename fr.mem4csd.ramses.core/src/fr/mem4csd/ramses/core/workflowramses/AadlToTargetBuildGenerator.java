/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses;

import fr.mem4csd.ramses.core.codegen.GenerationException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;

import org.osate.aadl2.ProcessSubcomponent;
import org.osate.aadl2.Subcomponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aadl To Target Build Generator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#getCodeGenWorkflowComponent <em>Code Gen Workflow Component</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getAadlToTargetBuildGenerator()
 * @model abstract="true"
 * @generated
 */
public interface AadlToTargetBuildGenerator extends AbstractCodeGenerator {
	/**
	 * Returns the value of the '<em><b>Code Gen Workflow Component</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToTargetBuild <em>Aadl To Target Build</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code Gen Workflow Component</em>' container reference.
	 * @see #setCodeGenWorkflowComponent(Codegen)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getAadlToTargetBuildGenerator_CodeGenWorkflowComponent()
	 * @see fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToTargetBuild
	 * @model opposite="aadlToTargetBuild" required="true"
	 * @generated
	 */
	Codegen getCodeGenWorkflowComponent();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#getCodeGenWorkflowComponent <em>Code Gen Workflow Component</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code Gen Workflow Component</em>' container reference.
	 * @see #getCodeGenWorkflowComponent()
	 * @generated
	 */
	void setCodeGenWorkflowComponent(Codegen value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="fr.mem4csd.ramses.core.workflowramses.GenerationException" processorRequired="true" processorMany="true" systemOutputDirDataType="de.mdelab.workflow.helpers.URI" systemOutputDirRequired="true" monitorDataType="de.mdelab.workflow.helpers.IProgressMonitor"
	 * @generated
	 */
	void generateSystemBuild(EList<Subcomponent> processor, URI systemOutputDir, IProgressMonitor monitor) throws GenerationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="fr.mem4csd.ramses.core.workflowramses.GenerationException" processorRequired="true" processorOutputDirDataType="de.mdelab.workflow.helpers.URI" processorOutputDirRequired="true" monitorDataType="de.mdelab.workflow.helpers.IProgressMonitor"
	 * @generated
	 */
	void generateProcessorBuild(Subcomponent processor, URI processorOutputDir, IProgressMonitor monitor) throws GenerationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="fr.mem4csd.ramses.core.workflowramses.GenerationException" processRequired="true" processOutputDirDataType="de.mdelab.workflow.helpers.URI" processOutputDirRequired="true" monitorDataType="de.mdelab.workflow.helpers.IProgressMonitor"
	 * @generated
	 */
	void generateProcessBuild(ProcessSubcomponent process, URI processOutputDir, IProgressMonitor monitor) throws GenerationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" targetPathDataType="de.mdelab.workflow.helpers.URI" targetPathRequired="true"
	 * @generated
	 */
	boolean validateTargetPath(URI targetPath);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 * @generated
	 */
	String getTargetShortDescription();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 * @generated
	 */
	String getTargetName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void initializeTargetBuilder();

} // AadlToTargetBuildGenerator
