/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses;

import de.mdelab.workflow.components.WorkflowComponent;
import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Codegen</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#isDebugOutput <em>Debug Output</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlModelSlot <em>Aadl Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTraceModelSlot <em>Trace Model Slot</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getOutputDirectory <em>Output Directory</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetInstallDir <em>Target Install Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getIncludeDir <em>Include Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getCoreRuntimeDir <em>Core Runtime Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetRuntimeDir <em>Target Runtime Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToSourceCode <em>Aadl To Source Code</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToTargetConfiguration <em>Aadl To Target Configuration</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToTargetBuild <em>Aadl To Target Build</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTransformationResourcesPairList <em>Transformation Resources Pair List</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetProperties <em>Target Properties</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getProgressMonitor <em>Progress Monitor</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getUriConverter <em>Uri Converter</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetInstallDirectoryURI <em>Target Install Directory URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getOutputDirectoryURI <em>Output Directory URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getCoreRuntimeDirectoryURI <em>Core Runtime Directory URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetRuntimeDirectoryURI <em>Target Runtime Directory URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getIncludeDirectoryURIList <em>Include Directory URI List</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen()
 * @model abstract="true"
 * @generated
 */
public interface Codegen extends WorkflowComponent {
	/**
	 * Returns the value of the '<em><b>Debug Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Debug Output</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Debug Output</em>' attribute.
	 * @see #setDebugOutput(boolean)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_DebugOutput()
	 * @model required="true"
	 * @generated
	 */
	boolean isDebugOutput();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#isDebugOutput <em>Debug Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Debug Output</em>' attribute.
	 * @see #isDebugOutput()
	 * @generated
	 */
	void setDebugOutput(boolean value);

	/**
	 * Returns the value of the '<em><b>Aadl Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aadl Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aadl Model Slot</em>' attribute.
	 * @see #setAadlModelSlot(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_AadlModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getAadlModelSlot();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlModelSlot <em>Aadl Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aadl Model Slot</em>' attribute.
	 * @see #getAadlModelSlot()
	 * @generated
	 */
	void setAadlModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Trace Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trace Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trace Model Slot</em>' attribute.
	 * @see #setTraceModelSlot(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_TraceModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getTraceModelSlot();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTraceModelSlot <em>Trace Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trace Model Slot</em>' attribute.
	 * @see #getTraceModelSlot()
	 * @generated
	 */
	void setTraceModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Directory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Directory</em>' attribute.
	 * @see #setOutputDirectory(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_OutputDirectory()
	 * @model
	 * @generated
	 */
	String getOutputDirectory();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getOutputDirectory <em>Output Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Directory</em>' attribute.
	 * @see #getOutputDirectory()
	 * @generated
	 */
	void setOutputDirectory(String value);

	/**
	 * Returns the value of the '<em><b>Target Install Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Install Dir</em>' attribute.
	 * @see #setTargetInstallDir(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_TargetInstallDir()
	 * @model required="true"
	 * @generated
	 */
	String getTargetInstallDir();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetInstallDir <em>Target Install Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Install Dir</em>' attribute.
	 * @see #getTargetInstallDir()
	 * @generated
	 */
	void setTargetInstallDir(String value);

	/**
	 * Returns the value of the '<em><b>Include Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Include Dir</em>' attribute.
	 * @see #setIncludeDir(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_IncludeDir()
	 * @model
	 * @generated
	 */
	String getIncludeDir();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getIncludeDir <em>Include Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Include Dir</em>' attribute.
	 * @see #getIncludeDir()
	 * @generated
	 */
	void setIncludeDir(String value);

	/**
	 * Returns the value of the '<em><b>Core Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Core Runtime Dir</em>' attribute.
	 * @see #setCoreRuntimeDir(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_CoreRuntimeDir()
	 * @model
	 * @generated
	 */
	String getCoreRuntimeDir();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getCoreRuntimeDir <em>Core Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Core Runtime Dir</em>' attribute.
	 * @see #getCoreRuntimeDir()
	 * @generated
	 */
	void setCoreRuntimeDir(String value);

	/**
	 * Returns the value of the '<em><b>Target Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Runtime Dir</em>' attribute.
	 * @see #setTargetRuntimeDir(String)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_TargetRuntimeDir()
	 * @model
	 * @generated
	 */
	String getTargetRuntimeDir();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetRuntimeDir <em>Target Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Runtime Dir</em>' attribute.
	 * @see #getTargetRuntimeDir()
	 * @generated
	 */
	void setTargetRuntimeDir(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getTargetName();

	/**
	 * Returns the value of the '<em><b>Aadl To Source Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aadl To Source Code</em>' containment reference.
	 * @see #setAadlToSourceCode(AadlToSourceCodeGenerator)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_AadlToSourceCode()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	AadlToSourceCodeGenerator getAadlToSourceCode();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToSourceCode <em>Aadl To Source Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aadl To Source Code</em>' containment reference.
	 * @see #getAadlToSourceCode()
	 * @generated
	 */
	void setAadlToSourceCode(AadlToSourceCodeGenerator value);

	/**
	 * Returns the value of the '<em><b>Aadl To Target Configuration</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator#getCodeGenWorkflowComponent <em>Code Gen Workflow Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aadl To Target Configuration</em>' containment reference.
	 * @see #setAadlToTargetConfiguration(AadlToTargetConfigurationGenerator)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_AadlToTargetConfiguration()
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator#getCodeGenWorkflowComponent
	 * @model opposite="codeGenWorkflowComponent" containment="true" transient="true"
	 * @generated
	 */
	AadlToTargetConfigurationGenerator getAadlToTargetConfiguration();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToTargetConfiguration <em>Aadl To Target Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aadl To Target Configuration</em>' containment reference.
	 * @see #getAadlToTargetConfiguration()
	 * @generated
	 */
	void setAadlToTargetConfiguration(AadlToTargetConfigurationGenerator value);

	/**
	 * Returns the value of the '<em><b>Aadl To Target Build</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#getCodeGenWorkflowComponent <em>Code Gen Workflow Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aadl To Target Build</em>' containment reference.
	 * @see #setAadlToTargetBuild(AadlToTargetBuildGenerator)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_AadlToTargetBuild()
	 * @see fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator#getCodeGenWorkflowComponent
	 * @model opposite="codeGenWorkflowComponent" containment="true" transient="true"
	 * @generated
	 */
	AadlToTargetBuildGenerator getAadlToTargetBuild();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getAadlToTargetBuild <em>Aadl To Target Build</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aadl To Target Build</em>' containment reference.
	 * @see #getAadlToTargetBuild()
	 * @generated
	 */
	void setAadlToTargetBuild(AadlToTargetBuildGenerator value);

	/**
	 * Returns the value of the '<em><b>Transformation Resources Pair List</b></em>' reference list.
	 * The list contents are of type {@link fr.mem4csd.ramses.core.workflowramses.TransformationResourcesPair}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformation Resources Pair List</em>' reference list.
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_TransformationResourcesPairList()
	 * @model
	 * @generated
	 */
	EList<TransformationResourcesPair> getTransformationResourcesPairList();

	/**
	 * Returns the value of the '<em><b>Target Properties</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Properties</em>' reference.
	 * @see #setTargetProperties(TargetProperties)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_TargetProperties()
	 * @model
	 * @generated
	 */
	TargetProperties getTargetProperties();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetProperties <em>Target Properties</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Properties</em>' reference.
	 * @see #getTargetProperties()
	 * @generated
	 */
	void setTargetProperties(TargetProperties value);

	/**
	 * Returns the value of the '<em><b>Progress Monitor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Progress Monitor</em>' attribute.
	 * @see #setProgressMonitor(IProgressMonitor)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_ProgressMonitor()
	 * @model dataType="de.mdelab.workflow.helpers.IProgressMonitor" transient="true"
	 * @generated
	 */
	IProgressMonitor getProgressMonitor();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getProgressMonitor <em>Progress Monitor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Progress Monitor</em>' attribute.
	 * @see #getProgressMonitor()
	 * @generated
	 */
	void setProgressMonitor(IProgressMonitor value);

	/**
	 * Returns the value of the '<em><b>Uri Converter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uri Converter</em>' attribute.
	 * @see #setUriConverter(URIConverter)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_UriConverter()
	 * @model dataType="fr.mem4csd.ramses.core.workflowramses.URIConverter"
	 * @generated
	 */
	URIConverter getUriConverter();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getUriConverter <em>Uri Converter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uri Converter</em>' attribute.
	 * @see #getUriConverter()
	 * @generated
	 */
	void setUriConverter(URIConverter value);

	/**
	 * Returns the value of the '<em><b>Target Install Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Install Directory URI</em>' attribute.
	 * @see #setTargetInstallDirectoryURI(URI)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_TargetInstallDirectoryURI()
	 * @model dataType="de.mdelab.workflow.helpers.URI" transient="true" derived="true"
	 * @generated
	 */
	URI getTargetInstallDirectoryURI();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetInstallDirectoryURI <em>Target Install Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Install Directory URI</em>' attribute.
	 * @see #getTargetInstallDirectoryURI()
	 * @generated
	 */
	void setTargetInstallDirectoryURI(URI value);

	/**
	 * Returns the value of the '<em><b>Output Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Directory URI</em>' attribute.
	 * @see #setOutputDirectoryURI(URI)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_OutputDirectoryURI()
	 * @model dataType="de.mdelab.workflow.helpers.URI" transient="true" derived="true"
	 * @generated
	 */
	URI getOutputDirectoryURI();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getOutputDirectoryURI <em>Output Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Directory URI</em>' attribute.
	 * @see #getOutputDirectoryURI()
	 * @generated
	 */
	void setOutputDirectoryURI(URI value);

	/**
	 * Returns the value of the '<em><b>Core Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Core Runtime Directory URI</em>' attribute.
	 * @see #setCoreRuntimeDirectoryURI(URI)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_CoreRuntimeDirectoryURI()
	 * @model dataType="de.mdelab.workflow.helpers.URI" transient="true" derived="true"
	 * @generated
	 */
	URI getCoreRuntimeDirectoryURI();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getCoreRuntimeDirectoryURI <em>Core Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Core Runtime Directory URI</em>' attribute.
	 * @see #getCoreRuntimeDirectoryURI()
	 * @generated
	 */
	void setCoreRuntimeDirectoryURI(URI value);

	/**
	 * Returns the value of the '<em><b>Target Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Runtime Directory URI</em>' attribute.
	 * @see #setTargetRuntimeDirectoryURI(URI)
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_TargetRuntimeDirectoryURI()
	 * @model dataType="de.mdelab.workflow.helpers.URI" transient="true" derived="true"
	 * @generated
	 */
	URI getTargetRuntimeDirectoryURI();

	/**
	 * Sets the value of the '{@link fr.mem4csd.ramses.core.workflowramses.Codegen#getTargetRuntimeDirectoryURI <em>Target Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Runtime Directory URI</em>' attribute.
	 * @see #getTargetRuntimeDirectoryURI()
	 * @generated
	 */
	void setTargetRuntimeDirectoryURI(URI value);

	/**
	 * Returns the value of the '<em><b>Include Directory URI List</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.common.util.URI}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Include Directory URI List</em>' attribute list.
	 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getCodegen_IncludeDirectoryURIList()
	 * @model dataType="de.mdelab.workflow.helpers.URI" transient="true" derived="true"
	 * @generated
	 */
	EList<URI> getIncludeDirectoryURIList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<ArchTraceSpec> getModelTransformationTracesList();

} // Codegen
