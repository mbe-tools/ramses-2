/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Target Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getTargetProperties()
 * @model abstract="true"
 * @generated
 */
public interface TargetProperties extends EObject {
} // TargetProperties
