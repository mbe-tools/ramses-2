/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.core.workflowramses;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage
 * @generated
 */
public interface WorkflowramsesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowramsesFactory eINSTANCE = fr.mem4csd.ramses.core.workflowramses.impl.WorkflowramsesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Trace Writer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trace Writer</em>'.
	 * @generated
	 */
	TraceWriter createTraceWriter();

	/**
	 * Returns a new object of class '<em>Clear Validation Errors</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Clear Validation Errors</em>'.
	 * @generated
	 */
	ClearValidationErrors createClearValidationErrors();

	/**
	 * Returns a new object of class '<em>Report Validation Errors</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Report Validation Errors</em>'.
	 * @generated
	 */
	ReportValidationErrors createReportValidationErrors();

	/**
	 * Returns a new object of class '<em>Condition Evaluation Target</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Condition Evaluation Target</em>'.
	 * @generated
	 */
	ConditionEvaluationTarget createConditionEvaluationTarget();

	/**
	 * Returns a new object of class '<em>Condition Evaluation Protocol</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Condition Evaluation Protocol</em>'.
	 * @generated
	 */
	ConditionEvaluationProtocol createConditionEvaluationProtocol();

	/**
	 * Returns a new object of class '<em>Transformation Resources Pair</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transformation Resources Pair</em>'.
	 * @generated
	 */
	TransformationResourcesPair createTransformationResourcesPair();

	/**
	 * Returns a new object of class '<em>Aadl Writer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aadl Writer</em>'.
	 * @generated
	 */
	AadlWriter createAadlWriter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorkflowramsesPackage getWorkflowramsesPackage();

} //WorkflowramsesFactory
