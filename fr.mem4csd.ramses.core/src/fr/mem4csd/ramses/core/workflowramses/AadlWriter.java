/**
 */
package fr.mem4csd.ramses.core.workflowramses;

import de.mdelab.workflow.components.ModelWriter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aadl Writer</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage#getAadlWriter()
 * @model
 * @generated
 */
public interface AadlWriter extends ModelWriter {
} // AadlWriter
