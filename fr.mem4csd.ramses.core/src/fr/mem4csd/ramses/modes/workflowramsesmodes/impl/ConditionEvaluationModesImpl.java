/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.modes.workflowramsesmodes.impl;

import fr.mem4csd.ramses.core.util.ModesWorkflowUtils;
import fr.mem4csd.ramses.core.workflowramses.impl.ConditionEvaluationImpl;

import fr.mem4csd.ramses.modes.workflowramsesmodes.ConditionEvaluationModes;
import fr.mem4csd.ramses.modes.workflowramsesmodes.WorkflowramsesmodesPackage;

import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Condition Evaluation Modes</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ConditionEvaluationModesImpl extends ConditionEvaluationImpl implements ConditionEvaluationModes {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionEvaluationModesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesmodesPackage.Literals.CONDITION_EVALUATION_MODES;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void execute (final WorkflowExecutionContext context,
			final IProgressMonitor monitor) throws WorkflowExecutionException, IOException {
		
		EMap<String, Object> modelSlots = context.getModelSlots();
		
		
		Resource inputModel = ((EObject) modelSlots.get(instanceModelSlot)).eResource();
		Boolean hasProcessModes = (Boolean) modelSlots.get(this.resultModelSlot);
		
		
		hasProcessModes = 
				ModesWorkflowUtils.hasProcessModes(inputModel);
		
		modelSlots.put(resultModelSlot, hasProcessModes);
		
		System.out.println("Model has process modes: "+hasProcessModes);
		
	}

} //ConditionEvaluationModesImpl
