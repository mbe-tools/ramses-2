/**
 * RAMSES 2.0
 * 
 * Copyright © 2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.modes.workflowramsesmodes;

import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.modes.workflowramsesmodes.WorkflowramsesmodesFactory
 * @model kind="package"
 * @generated
 */
public interface WorkflowramsesmodesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowramsesmodes";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/ramses/workflowramsesmodes";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workflowramsesmodes";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowramsesmodesPackage eINSTANCE = fr.mem4csd.ramses.modes.workflowramsesmodes.impl.WorkflowramsesmodesPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.modes.workflowramsesmodes.impl.ConditionEvaluationModesImpl <em>Condition Evaluation Modes</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.modes.workflowramsesmodes.impl.ConditionEvaluationModesImpl
	 * @see fr.mem4csd.ramses.modes.workflowramsesmodes.impl.WorkflowramsesmodesPackageImpl#getConditionEvaluationModes()
	 * @generated
	 */
	int CONDITION_EVALUATION_MODES = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_MODES__NAME = WorkflowramsesPackage.CONDITION_EVALUATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_MODES__DESCRIPTION = WorkflowramsesPackage.CONDITION_EVALUATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_MODES__ENABLED = WorkflowramsesPackage.CONDITION_EVALUATION__ENABLED;

	/**
	 * The feature id for the '<em><b>Instance Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_MODES__INSTANCE_MODEL_SLOT = WorkflowramsesPackage.CONDITION_EVALUATION__INSTANCE_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Result Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_MODES__RESULT_MODEL_SLOT = WorkflowramsesPackage.CONDITION_EVALUATION__RESULT_MODEL_SLOT;

	/**
	 * The number of structural features of the '<em>Condition Evaluation Modes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_MODES_FEATURE_COUNT = WorkflowramsesPackage.CONDITION_EVALUATION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_MODES___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = WorkflowramsesPackage.CONDITION_EVALUATION___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int CONDITION_EVALUATION_MODES___EXECUTE__WORKFLOWEXECUTIONCONTEXT = WorkflowramsesPackage.CONDITION_EVALUATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_MODES___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = WorkflowramsesPackage.CONDITION_EVALUATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_MODES___CHECK_CANCELED__IPROGRESSMONITOR = WorkflowramsesPackage.CONDITION_EVALUATION___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Condition Evaluation Modes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_EVALUATION_MODES_OPERATION_COUNT = WorkflowramsesPackage.CONDITION_EVALUATION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.modes.workflowramsesmodes.ConditionEvaluationModes <em>Condition Evaluation Modes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition Evaluation Modes</em>'.
	 * @see fr.mem4csd.ramses.modes.workflowramsesmodes.ConditionEvaluationModes
	 * @generated
	 */
	EClass getConditionEvaluationModes();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkflowramsesmodesFactory getWorkflowramsesmodesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.modes.workflowramsesmodes.impl.ConditionEvaluationModesImpl <em>Condition Evaluation Modes</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.modes.workflowramsesmodes.impl.ConditionEvaluationModesImpl
		 * @see fr.mem4csd.ramses.modes.workflowramsesmodes.impl.WorkflowramsesmodesPackageImpl#getConditionEvaluationModes()
		 * @generated
		 */
		EClass CONDITION_EVALUATION_MODES = eINSTANCE.getConditionEvaluationModes();

	}

} //WorkflowramsesmodesPackage
