<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_nlxhYNj9EeiIc5Eb0h0CDw" name="workflow" description="Operations of validation for every architecture">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_1GojAORGEem2b9tZ1uGgew" name="load_validation_core" description="" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/load_transformation_resources_core.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_cYRDsPTMEem6xNJJeqmUXg" name="${NameLoadRefinementTransformationModules}" modelSlot="RemoteConnectionsSocket" modelURI="${ramses_socket_transformation_path}RemoteConnectionsSocket.emftvm" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_GBbcIAcOEeuDOaqjzi9xMA" name="${NameLoadRefinementTransformationModules}" modelSlot="socketsAtlHelper" modelURI="${scheme}${ramses_sockets_plugin}ramses/sockets/workflows/refined_model_for_socket_communications.helpers" modelElementIndex="0"/>
  <properties xmi:id="_71Fv0EmjEeqn2-IY0znzPQ" name="ramses_socket_transformation_path" defaultValue="${scheme}${ramses_sockets_plugin}ramses/sockets/transformations/atl/refinement/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_DFG9QKE6Eeq-4tsqJgsrBg" name="ramses_sockets_plugin" defaultValue="${ramses_sockets_plugin}">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_xzruwOh0Eeqy-t86Uy9Gdw" fileURI="default_sockets.properties"/>
  <propertiesFiles xmi:id="_q7LcIEmSEeqn2-IY0znzPQ" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
