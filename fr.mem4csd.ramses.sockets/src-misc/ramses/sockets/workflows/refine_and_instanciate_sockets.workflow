<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_OmMiQAb9EeuDOaqjzi9xMA" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_R9i4kAb9EeuDOaqjzi9xMA" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/templates/load_instanciate_refine_and_instanciate_core.workflow">
    <propertyValues xmi:id="_hx47EAb9EeuDOaqjzi9xMA" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_nG3bsAb9EeuDOaqjzi9xMA" name="system_implementation_name" defaultValue="${system_implementation_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
</workflow:Workflow>
