package fr.mem4csd.ramses.sockets;

import java.util.Set;

import org.eclipse.emf.common.util.URI;

public class SocketsGeneratorUtils {

	public static void addSocketsRuntimeFiles(URI socketsDirURI,
			Set<URI> srcFileSet,
			Set<URI> indludeDirSet,
			String targetName)
	{
		URI socketsValueInterface = socketsDirURI.appendSegment("aadl_ports_socket"+"_"+targetName).appendFileExtension("c");
		srcFileSet.add(socketsValueInterface) ;
		indludeDirSet.add(socketsDirURI);
	}
}
