<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowramses="https://mem4csd.telecom-paris.fr/ramses/workflowramses" xmlns:workflowramsesmodes="https://mem4csd.telecom-paris.fr/ramses/workflowramsesmodes" xmi:id="_9UTd8F4qEeqYp8ezKVdS_w" name="workflow">
  <components xsi:type="workflowramses:ConditionEvaluationTarget" xmi:id="_q7DTYPHSEeqqFvtavUcehg" name="conditionEvaluationTarget" instanceModelSlot="sourceAadlInstance" resultModelSlot="needsPOSIXRefinement" target="${target}"/>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_23cM4PHSEeqqFvtavUcehg" name="needs POSIX refinement" conditionSlot="needsPOSIXRefinement">
    <onTrue xmi:id="_-jEb0PHSEeqqFvtavUcehg" name="onTrue">
      <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_Jq1SUF7GEeqTbYoc6DiMAg" name="workflowDelegation" workflowURI="${scheme}${ramses_posix_plugin}posix/workflows/validate_posix_instance.workflow">
        <propertyValues xmi:id="_QB0pkF7GEeqTbYoc6DiMAg" name="source_aadl_file" defaultValue="${source_aadl_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_SLS3gF7GEeqTbYoc6DiMAg" name="system_implementation_name" defaultValue="${system_implementation_name}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_bZN_oF7GEeqTbYoc6DiMAg" name="validation_report_file" defaultValue="${validation_report_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_nPvDMF7dEeqTbYoc6DiMAg" name="scheme" defaultValue="${scheme}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_a8x3oIPbEeqBH7GvAFgbag" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_ixDsQIPbEeqBH7GvAFgbag" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_98ID8I-jEeqLFb1r3L2n8g" name="instance_model_file" defaultValue="${instance_model_file}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_YOXtQLb3EeqkNY6l67PJRQ" name="atl_transformation_utilities_plugin" defaultValue="${atl_transformation_utilities_plugin}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_MFohANj9Eeq5OLQxMCLtYw" name="ramses_posix_plugin" defaultValue="${ramses_posix_plugin}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_b-EZUNj9Eeq5OLQxMCLtYw" name="ramses_core_plugin" defaultValue="${ramses_core_plugin}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
        <propertyValues xmi:id="_eZFxIPKiEeqsDOGYuOmxLA" name="output_dir" defaultValue="${output_dir}">
          <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
        </propertyValues>
      </components>
      <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_WM_pENDaEeqYCPJqBYsmOQ" name="hasErrors" conditionSlot="violatesTargetConstraints">
        <onTrue xmi:id="_CATQ0NDbEeqYCPJqBYsmOQ" name="onTrue, exit"/>
        <onFalse xmi:id="_dPbKANDaEeqYCPJqBYsmOQ" name="onFalse">
          <components xsi:type="workflowramsesmodes:ConditionEvaluationModes" xmi:id="_sYfrYKfcEeqjWv6l-exT6w" name="conditionEvaluationModes" instanceModelSlot="sourceAadlInstance" resultModelSlot="hasModes"/>
          <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_fEm60KfZEeqkUpYD5uVZcw" name="hasModes" conditionSlot="hasModes">
            <onTrue xmi:id="_6bgTIKfcEeqjWv6l-exT6w" name="onTrue">
              <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_GlMW4KffEeqjWv6l-exT6w" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/refinement_modes_core_instance.workflow">
                <propertyValues xmi:id="_NbncIaffEeqjWv6l-exT6w" name="system_implementation_name" defaultValue="${system_implementation_name}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_NbncIqffEeqjWv6l-exT6w" name="source_aadl_file" defaultValue="${source_aadl_file}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_NbncI6ffEeqjWv6l-exT6w" name="refined_aadl_file" defaultValue="${modes_file}.aadl">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_NbncJKffEeqjWv6l-exT6w" name="refined_trace_file" defaultValue="${modes_file}.arch_tarce">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_NbncJaffEeqjWv6l-exT6w" name="instance_model_file" defaultValue="${instance_model_file}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_NbncJ6ffEeqjWv6l-exT6w" name="scheme" defaultValue="${scheme}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_NbncKKffEeqjWv6l-exT6w" name="core_runtime_dir" defaultValue="${core_runtime_dir}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_NbncKaffEeqjWv6l-exT6w" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_NbncKqffEeqjWv6l-exT6w" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_g4ZtgKf2EeqjWv6l-exT6w" name="output_dir" defaultValue="${output_dir}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_1aYXoLb7EeqkNY6l67PJRQ" name="atl_transformation_utilities_plugin" defaultValue="${atl_transformation_utilities_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_sVfVQNlhEeq5OLQxMCLtYw" name="ramses_core_plugin" defaultValue="${ramses_core_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
              </components>
              <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_LcCxEKffEeqjWv6l-exT6w" name="workflowDelegation" workflowURI="${scheme}${ramses_posix_plugin}posix/workflows/refinement_posix.workflow">
                <propertyValues xmi:id="_LcCxE6ffEeqjWv6l-exT6w" name="source_aadl_file" defaultValue="${modes_file}.aadl">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_Z-E7EKh3Eeqi7qcgTPMvMQ" name="system_implementation_name" defaultValue="refined_model_for_modes.impl">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_LcCxFKffEeqjWv6l-exT6w" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_LcCxFaffEeqjWv6l-exT6w" name="refined_trace_file" defaultValue="${refined_trace_file}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_LcCxFqffEeqjWv6l-exT6w" name="instance_model_file" defaultValue="${modes_file}.aaxl2">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_LcCxGKffEeqjWv6l-exT6w" name="scheme" defaultValue="${scheme}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_LcCxGaffEeqjWv6l-exT6w" name="core_runtime_dir" defaultValue="${core_runtime_dir}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_LcCxGqffEeqjWv6l-exT6w" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_LcCxG6ffEeqjWv6l-exT6w" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_YvY_AKf2EeqjWv6l-exT6w" name="output_dir" defaultValue="${output_dir}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_Nhe6gLb8EeqkNY6l67PJRQ" name="atl_transformation_utilities_plugin" defaultValue="${atl_transformation_utilities_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_qS858NlaEeq5OLQxMCLtYw" name="ramses_core_plugin" defaultValue="${ramses_core_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="__LLksNloEeq5OLQxMCLtYw" name="ramses_posix_plugin" defaultValue="${ramses_posix_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_t4u5QP0GEeq3Gs5l9Sfxsw" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
              </components>
            </onTrue>
            <onFalse xmi:id="_7J0KoKfcEeqjWv6l-exT6w" name="onFalse">
              <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_Blx6MF4rEeqYp8ezKVdS_w" name="workflowDelegation" workflowURI="${scheme}${ramses_posix_plugin}posix/workflows/refinement_posix_instance.workflow">
                <propertyValues xmi:id="_IOr18F4tEeqYp8ezKVdS_w" name="system_implementation_name" defaultValue="${system_implementation_name}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_D6fu0F4rEeqYp8ezKVdS_w" name="source_aadl_file" defaultValue="${source_aadl_file}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_U35OoF4sEeqYp8ezKVdS_w" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_a8OIkF4sEeqYp8ezKVdS_w" name="refined_trace_file" defaultValue="${refined_trace_file}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_uK3kMF4rEeqYp8ezKVdS_w" name="instance_model_file" defaultValue="${instance_model_file}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_Os3xYF7dEeqTbYoc6DiMAg" name="scheme" defaultValue="${scheme}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_dZ8HoH_rEeqTUOoTKckx6g" name="core_runtime_dir" defaultValue="${core_runtime_dir}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_gpRgYIPbEeqBH7GvAFgbag" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_m911wIPbEeqBH7GvAFgbag" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_GLkBAKgCEeqjWv6l-exT6w" name="output_dir" defaultValue="${output_dir}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_497W4Lb7EeqkNY6l67PJRQ" name="atl_transformation_utilities_plugin" defaultValue="${atl_transformation_utilities_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_E0mDMNo4Eeq9fI4NRnUBRA" name="ramses_core_plugin" defaultValue="${ramses_core_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_C9_RcNo5Eeq9fI4NRnUBRA" name="ramses_posix_plugin" defaultValue="${ramses_posix_plugin}">
                  <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
                </propertyValues>
                <propertyValues xmi:id="_yJ8PwP0GEeq3Gs5l9Sfxsw" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file}"/>
              </components>
            </onFalse>
          </components>
          <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_iXtpwF4sEeqYp8ezKVdS_w" name="workflowDelegation" workflowURI="${code_generation_workflow}">
            <propertyValues xmi:id="_fUvBQIPZEeqW_5lHgufVZA" name="scheme" defaultValue="${scheme}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_g5yBkIPbEeqBH7GvAFgbag" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_nKYc8IPbEeqBH7GvAFgbag" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_KFk4Udj-Eeq5OLQxMCLtYw" name="ramses_core_plugin" defaultValue="${ramses_core_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_KFk4UNj-Eeq5OLQxMCLtYw" name="ramses_posix_plugin" defaultValue="${ramses_posix_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_nhN6MF4sEeqYp8ezKVdS_w" name="source_aadl_file" defaultValue="${source_aadl_file}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_tSi_oF4sEeqYp8ezKVdS_w" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_2ok64F4sEeqYp8ezKVdS_w" name="refined_trace_file" defaultValue="${refined_trace_file}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_qV4aMF4sEeqYp8ezKVdS_w" name="instance_model_file" defaultValue="${instance_model_file}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_QNdvsOe8Eeqy-t86Uy9Gdw" name="runtime_scheme" defaultValue="${runtime_scheme}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_-EMX4F4sEeqYp8ezKVdS_w" name="include_dir" defaultValue="${include_dir}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_7D_9UF4sEeqYp8ezKVdS_w" name="output_dir" defaultValue="${output_dir}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_jpTz8H_rEeqTUOoTKckx6g" name="core_runtime_dir" defaultValue="${core_runtime_dir}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
          </components>
          <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_NhciEF7HEeqTbYoc6DiMAg" name="workflowDelegation" workflowURI="${scheme}${ramses_posix_plugin}posix/workflows/compile_codegen_posix.workflow">
            <propertyValues xmi:id="_QyifYF7HEeqTbYoc6DiMAg" name="working_dir" defaultValue="${output_dir}/generated-code/">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_OQTMUNj-Eeq5OLQxMCLtYw" name="ramses_core_plugin" defaultValue="${ramses_core_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dPZy8No9Eeq9fI4NRnUBRA" name="ramses_posix_plugin" defaultValue="${ramses_posix_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_G14UwNpUEeq9fI4NRnUBRA" name="scheme" defaultValue="${scheme}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
          </components>
        </onFalse>
      </components>
    </onTrue>
    <onFalse xmi:id="_7KzfgPHSEeqqFvtavUcehg" name="onFalse: exit, nothing to do"/>
  </components>
  <properties xmi:id="_RvqH0F4rEeqYp8ezKVdS_w" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Wro5oF4rEeqYp8ezKVdS_w" name="include_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_auww0F4rEeqYp8ezKVdS_w" name="source_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_4lCdAF4rEeqYp8ezKVdS_w" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_OYkawF4sEeqYp8ezKVdS_w" name="instance_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_XbVbwF4sEeqYp8ezKVdS_w" name="refined_aadl_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_douyIF4sEeqYp8ezKVdS_w" name="refined_trace_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_fp81AF7GEeqTbYoc6DiMAg" name="validation_report_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_WgXrsP0GEeq3Gs5l9Sfxsw" name="refinement_target_emftvm_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_P1YMcF4rEeqYp8ezKVdS_w" fileURI="default_posix.properties"/>
  <propertiesFiles xmi:id="_y9mXMNpdEeq9fI4NRnUBRA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
