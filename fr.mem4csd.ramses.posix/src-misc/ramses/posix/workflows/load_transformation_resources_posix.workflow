<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_nlxhYNj9EeiIc5Eb0h0CDw" name="workflow" description="Operations of validation for every architecture">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_1GojAORGEem2b9tZ1uGgew" name="load_validation_core" description="" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/load_transformation_resources_core.workflow"/>
  <properties xmi:id="_71Fv0EmjEeqn2-IY0znzPQ" name="ramses_posix_transformation_path" defaultValue="${scheme}${ramses_posix_plugin}posix/transformations/atl/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_ykRWoEmhEeqn2-IY0znzPQ" name="ramses_posix_transformation_refinement_path" defaultValue="${ramses_posix_transformation_path}refinement/">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_q7LcIEmSEeqn2-IY0znzPQ" fileURI="default_posix.properties"/>
  <propertiesFiles xmi:id="_vSpFMNpZEeq9fI4NRnUBRA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
