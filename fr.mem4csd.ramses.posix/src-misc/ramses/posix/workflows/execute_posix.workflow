<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowosate="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowosate" xmi:id="_9UTd8F4qEeqYp8ezKVdS_w" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_qBN98PTSEem6xNJJeqmUXg" name="load_contributions_posix" description="" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/load_ramses_aadl_resources_core.workflow">
    <propertyValues xmi:id="_d55bUUv8EeqyhuEvlF_W_g" name="ramses_core_plugin" defaultValue="${ramses_core_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_d55bUkv8EeqyhuEvlF_W_g" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_d55bU0v8EeqyhuEvlF_W_g" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_d55bVEv8EeqyhuEvlF_W_g" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_EmUF8_GhEemHmY6XjiEv8w" name="${NameInputResourcesReader}" description="" modelSlot="srcAadlModel" modelURI="${source_aadl_file}" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflowosate:AadlInstanceModelCreator" xmi:id="_Q_KMoKflEeqjWv6l-exT6w" name="aadlInstanceModelCreator" systemImplementationName="${system_implementation_name}" packageModelSlot="srcAadlModel" systemInstanceModelSlot="sourceAadlInstance"/>
  <components xsi:type="workflow.components:ModelWriter" xmi:id="_PCyn4KGVEeq7gbrbATioGg" name="modelWriter" modelSlot="sourceAadlInstance" modelURI="${instance_model_file}" resolveURI="false"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_Jq1SUF7GEeqTbYoc6DiMAg" name="workflowDelegation" workflowURI="${scheme}${ramses_posix_plugin}posix/workflows/execute_posix_instance.workflow">
    <propertyValues xmi:id="_nPvDMF7dEeqTbYoc6DiMAg" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_lpAMMKfqEeqjWv6l-exT6w" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_piYRgKfqEeqjWv6l-exT6w" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_GOY80Lb7EeqkNY6l67PJRQ" name="atl_transformation_utilities_plugin" defaultValue="${atl_transformation_utilities_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_XNcYYNj8Eeq5OLQxMCLtYw" name="ramses_core_plugin" defaultValue="${ramses_core_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="__I6L4Nj8Eeq5OLQxMCLtYw" name="ramses_posix_plugin" defaultValue="${ramses_posix_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_QRmzsKH-EeqLeZPcACIvIQ" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_QB0pkF7GEeqTbYoc6DiMAg" name="source_aadl_file" defaultValue="${source_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_SLS3gF7GEeqTbYoc6DiMAg" name="system_implementation_name" defaultValue="${system_implementation_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_bZN_oF7GEeqTbYoc6DiMAg" name="validation_report_file" defaultValue="${validation_report_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_DLJYQKfnEeqjWv6l-exT6w" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_v2meEKfpEeqjWv6l-exT6w" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_zIMxsKfpEeqjWv6l-exT6w" name="refined_trace_file" defaultValue="${refined_trace_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_EsyCwKfqEeqjWv6l-exT6w" name="instance_model_file" defaultValue="${instance_model_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_BFrRMOe8Eeqy-t86Uy9Gdw" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_wiDtgP1qEeq3Gs5l9Sfxsw" name="code_generation_workflow" defaultValue="${code_generation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_FRtMYP1rEeq3Gs5l9Sfxsw" name="refinement_target_emftvm_file" defaultValue="${refinement_target_emftvm_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_Vav6QP1wEeq3Gs5l9Sfxsw" name="target" defaultValue="${target}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_UZ5ZoKFMEeqN5O1Mj1GQIw" name="refined_models_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Wro5oF4rEeqYp8ezKVdS_w" name="include_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_auww0F4rEeqYp8ezKVdS_w" name="source_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_4lCdAF4rEeqYp8ezKVdS_w" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_OYkawF4sEeqYp8ezKVdS_w" name="instance_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_RvqH0F4rEeqYp8ezKVdS_w" name="output_dir" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_Vg0I8KFNEeqN5O1Mj1GQIw" name="local_communications_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_douyIF4sEeqYp8ezKVdS_w" name="refined_trace_file" defaultValue="${local_communications_file}.arh_trace">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_XbVbwF4sEeqYp8ezKVdS_w" name="refined_aadl_file" defaultValue="${local_communications_file}.aadl">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_fp81AF7GEeqTbYoc6DiMAg" name="validation_report_file" defaultValue="${local_communications_file}.validation_report">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_P1YMcF4rEeqYp8ezKVdS_w" fileURI="default_posix.properties"/>
  <propertiesFiles xmi:id="_8CzoMNpdEeq9fI4NRnUBRA" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
