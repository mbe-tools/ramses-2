/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.posix.codegen.c;

import java.io.IOException ;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger ;
import org.eclipse.core.runtime.IProgressMonitor ;
import org.eclipse.emf.common.util.EList ;
import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.DataSubcomponent;
import org.osate.aadl2.ProcessImplementation ;
import org.osate.aadl2.ProcessSubcomponent ;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.SystemImplementation ;
import org.osate.aadl2.ThreadSubcomponent ;
import org.osate.aadl2.instance.ComponentInstance ;
import org.osate.aadl2.instance.FeatureInstance ;
import org.osate.aadl2.modelsupport.UnparseText ;

import fr.mem4csd.ramses.core.arch_trace.ArchTraceSpec;
import fr.mem4csd.ramses.core.codegen.AadlToXUnparser;
import fr.mem4csd.ramses.core.codegen.GenerationException;
import fr.mem4csd.ramses.core.codegen.GenerationInfos.QueueInfo;
import fr.mem4csd.ramses.core.codegen.ProcessProperties;
import fr.mem4csd.ramses.core.codegen.RoutingProperties;
import fr.mem4csd.ramses.core.codegen.c.AadlToCUnparser;
import fr.mem4csd.ramses.core.codegen.utils.FileUtils;
import fr.mem4csd.ramses.core.codegen.utils.GenerationUtilsC;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;
import fr.mem4csd.ramses.core.workflowramses.TargetProperties;

public class AadlToPOSIXCUnparser extends AadlToXUnparser
{
	
	private static Logger _LOGGER = Logger.getLogger(AadlToPOSIXCUnparser.class) ;

	//XXX: Just implemented to respect the interface definition, but they are
	// not used in case of POSIX
	@Override
	public void generateProcessorTargetConfiguration(Subcomponent processor, 
			URI processorDir,
			IProgressMonitor monitor)
					throws GenerationException
	{
		// nothing to do
		return;
	}

  @Override
  public void generateProcessTargetConfiguration(ProcessSubcomponent process,
		  URI outputDir,
		  IProgressMonitor monitor)
				  throws GenerationException
  {
    ArchTraceSpec traces = getCurrentModelTransformationTrace();
	// Generate main.c and main.h
	_mainCCode = new UnparseText();
	_mainHCode = new UnparseText();
	genMainImpl(process, traces) ;
    
    
    try
    {
      
      FileUtils.saveFile(outputDir, "main.h",
               _mainHCode.getParseOutput()) ;
      
      FileUtils.saveFile(outputDir, "main.c",
               _mainCCode.getParseOutput()) ;
      
    }
    catch(IOException e)
    {
      String errMsg = "cannot save the generated files" ;
      _LOGGER.fatal(errMsg, e) ;
      throw new RuntimeException(errMsg, e) ;
    }
  }
  
  private void genMainImpl(ProcessSubcomponent process, ArchTraceSpec traces)
  {
	  
	String guard = GenerationUtilsC.generateHeaderInclusionGuard("main.h");
	_mainHCode.addOutputNewline(guard) ;
	
	_mainHCode.addOutputNewline("#include \"gtypes.h\"");
	_mainHCode.addOutputNewline("#include \"globals.h\"");
	_mainHCode.addOutputNewline("#include \"subprograms.h\"");
	_mainHCode.addOutputNewline("#include \"aadl_dispatch.h\"");
	_mainHCode.addOutputNewline("#include \"bitset.h\"");
	_mainHCode.addOutputNewline("#include <pthread.h>");
	_mainHCode.addOutputNewline("#include <limits.h>");
	_mainHCode.addOutputNewline("#include <stdio.h>");
	
	ProcessImplementation procImpl = (ProcessImplementation) process.getSubcomponentType();
	
	if(GeneratorUtils.processUsesMQTT(procImpl))
		_mainHCode.addOutputNewline("#define USE_MQTT");
	if(GeneratorUtils.processUsesSOCKET(procImpl))
		_mainHCode.addOutputNewline("#define USE_SOCKET");
		
	
	_mainCCode.addOutputNewline("#include \"aadl_ports_network.h\"") ;
	if(GeneratorUtils.processUsesSOCKET(procImpl))
    	_mainCCode.addOutputNewline("#include <signal.h>") ;
	
	if(process.getProcessSubcomponentType() instanceof ProcessImplementation
			&& GeneratorUtils.processUsesMQTT((ProcessImplementation) process.getProcessSubcomponentType()))
		_mainCCode.addOutputNewline("#include \"aadl_ports_mqtt.h\"") ;
	
	super.genFileIncludedMainImpl();
	
    genMainGlobalVariables(process);
    
    AadlToCUnparser.getAadlToCUnparser().genStopProcessCode(process, _mainCCode);
    _mainHCode.addOutputNewline("void stop_process();");
    
    
    AadlToCUnparser.getAadlToCUnparser().genModeInit(process, _mainCCode);
    
    _mainCCode.decrementIndent();
    _mainCCode.addOutputNewline("}");
    _mainCCode.addOutputNewline("");
    
    genMQTTMessageHandlers(process);
    
    _mainCCode.addOutputNewline("int main(int argc, char* argv[])");
    _mainCCode.addOutputNewline("{");
    _mainCCode.incrementIndent();
    
    if(GeneratorUtils.processUsesSOCKET(procImpl))
    {
    	_mainCCode.addOutputNewline("signal(SIGINT, stop_process);");
    }
    List<String> additionalHeaders = new ArrayList<String>();
     
    genMainCCode(process, traces, additionalHeaders);
    _mainHCode.addOutput(GenerationUtilsC.getAdditionalHeader(additionalHeaders));
    
    _mainCCode.decrementIndent();
    _mainCCode.addOutputNewline("}");
    
    _mainCCode.addOutputNewline("");

    StringBuilder sb = new StringBuilder(process.getQualifiedName());
    ProcessProperties pp = new ProcessProperties(sb.substring(0, sb.lastIndexOf("::")+2)) ;
     
    ProcessImplementation processImpl = (ProcessImplementation) 
            process.getComponentImplementation() ;
    this.findCommunicationMechanism(processImpl, pp, traces);
    
    _mainHCode.addOutputNewline("#endif");
    
  }

  protected void genMQTTMessageHandlers(ProcessSubcomponent process) {
	  ProcessImplementation pi = (ProcessImplementation) process.getComponentImplementation();
	  for(DataSubcomponent ds: pi.getOwnedDataSubcomponents())
	  {
		  if(ds.getDataSubcomponentType().getName().equalsIgnoreCase("MQTTConfigType"))
		  {
			  _mainCCode.addOutputNewline("void on_message_arrived_"+ds.getName()+"(MessageData* md) {");
			  _mainCCode.incrementIndent();
			  
			  _mainCCode.addOutputNewline("on_mqtt_message_arrived(&"+ds.getName()+", md);");
			  
			  _mainCCode.decrementIndent();
			  _mainCCode.addOutputNewline("}");
		  }
	  }
  }

  protected void genMainCCode(ProcessSubcomponent process, ArchTraceSpec traces, List<String> additionalHeaders)
  {
	
	_mainCCode.addOutputNewline("int max_prio = sched_get_priority_max(SCHED_FIFO);");
	_mainCCode.addOutputNewline("int min_prio = sched_get_priority_min(SCHED_FIFO);");

	
	_mainCCode.addOutputNewline("pthread_t thId = pthread_self();");
	_mainCCode.addOutputNewline("struct sched_param params;");
	_mainCCode.addOutputNewline("params.sched_priority = max_prio;");
	_mainCCode.addOutputNewline("pthread_setschedparam(thId, SCHED_FIFO, &params);");
	
	_mainCCode.addOutputNewline("pthread_mutex_init(&init_barrier_mutex, NULL);");
	_mainCCode.addOutputNewline("pthread_cond_init(&init_barrier_cond, NULL);");
	
    ProcessImplementation pi = (ProcessImplementation) process.getComponentImplementation();    
    
    for(DataSubcomponent ds: pi.getOwnedDataSubcomponents())
    {
      if(ds.getDataSubcomponentType().getName().equalsIgnoreCase("MQTTConfigType"))
      {
    	  _mainCCode.addOutputNewline("mqtt_init(&"+ds.getName()+
    			  ",\"mqtt_ramses_model\","
    			  + "\""+ process.getFullName().replaceAll(":", "_")+"\","+
    			  "on_message_arrived_"+ds.getName()+");");
      }
    }
    	
    
    EList<ThreadSubcomponent> subcomponents = pi.getOwnedThreadSubcomponents();
    AadlToCUnparser.getAadlToCUnparser().genMainCCode(process, _mainCCode, traces, additionalHeaders);
    
    _mainCCode.addOutputNewline("mode_init();");

    Subcomponent target = (Subcomponent)
    		AadlHelperImpl.getDeloymentProcessorSubcomponent(process);
    String schedProtocol = GeneratorUtils.getSchedulingProtocol(target);
    if(schedProtocol!=null && schedProtocol.equalsIgnoreCase("static"))
    {
    	long maxPrio = GeneratorUtils.getMaxPriority(target)-2;
    	_mainCCode.addOutputNewline("void time_triggered_sched();");
        _mainCCode.addOutputNewline("pthread_t TT_sched_tid;");
        _mainCCode.addOutputNewline("create_thread ("+maxPrio+", 0, (void*) time_triggered_sched, &TT_sched_tid, SCHED_FIFO, NULL);");
        _mainCCode.addOutputNewline("pthread_join(TT_sched_tid, NULL);");
    }
    
    for(ThreadSubcomponent ts: subcomponents)
    {
      genTaskJoinCode(ts);
    }
        
  }

  private void genTaskJoinCode(ThreadSubcomponent ts)
  {
	_mainCCode.addOutputNewline("pthread_t * "+ts.getName()+"_tid_local = &" + ts.getName()+"_config.target_thread->thread_id;");
    _mainCCode.addOutputNewline("if("+ts.getName()+"_tid_local!=NULL)");
    _mainCCode.addOutputNewline("{");
    _mainCCode.incrementIndent();
    _mainCCode.addOutputNewline("pthread_join(*"+ts.getName()+"_tid_local, NULL);");
    _mainCCode.decrementIndent();
    _mainCCode.addOutputNewline("}");
  }

  protected void genMainGlobalVariables(ProcessSubcomponent process)
  {
	
	AadlToCUnparser.getAadlToCUnparser().genMainGlobalVariables(process, _mainCCode);
	_mainCCode.addOutputNewline("pthread_mutex_t init_barrier_mutex;");
	_mainCCode.addOutputNewline("pthread_cond_t init_barrier_cond;");
	
	ProcessImplementation pi = (ProcessImplementation) process.getComponentImplementation();
    EList<ThreadSubcomponent> subcomponents = pi.getOwnedThreadSubcomponents();
    for(ThreadSubcomponent ts: subcomponents)
    {
      AadlToCUnparser.getAadlToCUnparser().genTaskGlobalVariables(ts, _mainCCode);
    }
    

  }

  @Override
  public TargetProperties
      getSystemTargetConfiguration(EList<SystemImplementation> si,
              IProgressMonitor monitor) throws GenerationException
  {
    return new RoutingProperties();
  }
  
}
