/**
 */
package fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl;

import fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Workflowramsesev3devintegrationFactoryImpl extends EFactoryImpl implements Workflowramsesev3devintegrationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Workflowramsesev3devintegrationFactory init() {
		try {
			Workflowramsesev3devintegrationFactory theWorkflowramsesev3devintegrationFactory = (Workflowramsesev3devintegrationFactory)EPackage.Registry.INSTANCE.getEFactory(Workflowramsesev3devintegrationPackage.eNS_URI);
			if (theWorkflowramsesev3devintegrationFactory != null) {
				return theWorkflowramsesev3devintegrationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Workflowramsesev3devintegrationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Workflowramsesev3devintegrationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION: return createCodegenEv3devIntegration();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodegenEv3devIntegration createCodegenEv3devIntegration() {
		CodegenEv3devIntegrationImpl codegenEv3devIntegration = new CodegenEv3devIntegrationImpl();
		return codegenEv3devIntegration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Workflowramsesev3devintegrationPackage getWorkflowramsesev3devintegrationPackage() {
		return (Workflowramsesev3devintegrationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Workflowramsesev3devintegrationPackage getPackage() {
		return Workflowramsesev3devintegrationPackage.eINSTANCE;
	}

} //Workflowramsesev3devintegrationFactoryImpl
