/**
 */
package fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl;

import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;
import fr.mem4csd.ramses.ev3dev.integration.AadlToEv3devIntegrationMakefileUnparser;
import fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration;
import fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.Workflowramsesev3devintegrationPackage;

import fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.impl.CodegenEv3devImpl;

import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.impl.WorkflowExecutionException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Codegen Ev3dev Integration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.CodegenEv3devIntegrationImpl#getMqttRuntimeDir <em>Mqtt Runtime Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.CodegenEv3devIntegrationImpl#getSocketsRuntimeDir <em>Sockets Runtime Dir</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.CodegenEv3devIntegrationImpl#getMqttRuntimeDirectoryURI <em>Mqtt Runtime Directory URI</em>}</li>
 *   <li>{@link fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.CodegenEv3devIntegrationImpl#getSocketsRuntimeDirectoryURI <em>Sockets Runtime Directory URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CodegenEv3devIntegrationImpl extends CodegenEv3devImpl implements CodegenEv3devIntegration {
	/**
	 * The default value of the '{@link #getMqttRuntimeDir() <em>Mqtt Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMqttRuntimeDir()
	 * @generated
	 * @ordered
	 */
	protected static final String MQTT_RUNTIME_DIR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMqttRuntimeDir() <em>Mqtt Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMqttRuntimeDir()
	 * @generated
	 * @ordered
	 */
	protected String mqttRuntimeDir = MQTT_RUNTIME_DIR_EDEFAULT;

	/**
	 * The default value of the '{@link #getSocketsRuntimeDir() <em>Sockets Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSocketsRuntimeDir()
	 * @generated
	 * @ordered
	 */
	protected static final String SOCKETS_RUNTIME_DIR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSocketsRuntimeDir() <em>Sockets Runtime Dir</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSocketsRuntimeDir()
	 * @generated
	 * @ordered
	 */
	protected String socketsRuntimeDir = SOCKETS_RUNTIME_DIR_EDEFAULT;

	/**
	 * The default value of the '{@link #getMqttRuntimeDirectoryURI() <em>Mqtt Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMqttRuntimeDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected static final URI MQTT_RUNTIME_DIRECTORY_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMqttRuntimeDirectoryURI() <em>Mqtt Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMqttRuntimeDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected URI mqttRuntimeDirectoryURI = MQTT_RUNTIME_DIRECTORY_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getSocketsRuntimeDirectoryURI() <em>Sockets Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSocketsRuntimeDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected static final URI SOCKETS_RUNTIME_DIRECTORY_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSocketsRuntimeDirectoryURI() <em>Sockets Runtime Directory URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSocketsRuntimeDirectoryURI()
	 * @generated
	 * @ordered
	 */
	protected URI socketsRuntimeDirectoryURI = SOCKETS_RUNTIME_DIRECTORY_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodegenEv3devIntegrationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Workflowramsesev3devintegrationPackage.Literals.CODEGEN_EV3DEV_INTEGRATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMqttRuntimeDir() {
		return mqttRuntimeDir;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMqttRuntimeDir(String newMqttRuntimeDir) {
		String oldMqttRuntimeDir = mqttRuntimeDir;
		mqttRuntimeDir = newMqttRuntimeDir;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIR, oldMqttRuntimeDir, mqttRuntimeDir));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSocketsRuntimeDir() {
		return socketsRuntimeDir;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSocketsRuntimeDir(String newSocketsRuntimeDir) {
		String oldSocketsRuntimeDir = socketsRuntimeDir;
		socketsRuntimeDir = newSocketsRuntimeDir;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIR, oldSocketsRuntimeDir, socketsRuntimeDir));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public URI getMqttRuntimeDirectoryURI() {
		return mqttRuntimeDirectoryURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMqttRuntimeDirectoryURI(URI newMqttRuntimeDirectoryURI) {
		URI oldMqttRuntimeDirectoryURI = mqttRuntimeDirectoryURI;
		mqttRuntimeDirectoryURI = newMqttRuntimeDirectoryURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI, oldMqttRuntimeDirectoryURI, mqttRuntimeDirectoryURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public URI getSocketsRuntimeDirectoryURI() {
		return socketsRuntimeDirectoryURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSocketsRuntimeDirectoryURI(URI newSocketsRuntimeDirectoryURI) {
		URI oldSocketsRuntimeDirectoryURI = socketsRuntimeDirectoryURI;
		socketsRuntimeDirectoryURI = newSocketsRuntimeDirectoryURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI, oldSocketsRuntimeDirectoryURI, socketsRuntimeDirectoryURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIR:
				return getMqttRuntimeDir();
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIR:
				return getSocketsRuntimeDir();
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI:
				return getMqttRuntimeDirectoryURI();
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI:
				return getSocketsRuntimeDirectoryURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIR:
				setMqttRuntimeDir((String)newValue);
				return;
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIR:
				setSocketsRuntimeDir((String)newValue);
				return;
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI:
				setMqttRuntimeDirectoryURI((URI)newValue);
				return;
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI:
				setSocketsRuntimeDirectoryURI((URI)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIR:
				setMqttRuntimeDir(MQTT_RUNTIME_DIR_EDEFAULT);
				return;
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIR:
				setSocketsRuntimeDir(SOCKETS_RUNTIME_DIR_EDEFAULT);
				return;
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI:
				setMqttRuntimeDirectoryURI(MQTT_RUNTIME_DIRECTORY_URI_EDEFAULT);
				return;
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI:
				setSocketsRuntimeDirectoryURI(SOCKETS_RUNTIME_DIRECTORY_URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIR:
				return MQTT_RUNTIME_DIR_EDEFAULT == null ? mqttRuntimeDir != null : !MQTT_RUNTIME_DIR_EDEFAULT.equals(mqttRuntimeDir);
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIR:
				return SOCKETS_RUNTIME_DIR_EDEFAULT == null ? socketsRuntimeDir != null : !SOCKETS_RUNTIME_DIR_EDEFAULT.equals(socketsRuntimeDir);
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI:
				return MQTT_RUNTIME_DIRECTORY_URI_EDEFAULT == null ? mqttRuntimeDirectoryURI != null : !MQTT_RUNTIME_DIRECTORY_URI_EDEFAULT.equals(mqttRuntimeDirectoryURI);
			case Workflowramsesev3devintegrationPackage.CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI:
				return SOCKETS_RUNTIME_DIRECTORY_URI_EDEFAULT == null ? socketsRuntimeDirectoryURI != null : !SOCKETS_RUNTIME_DIRECTORY_URI_EDEFAULT.equals(socketsRuntimeDirectoryURI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mqttRuntimeDir: ");
		result.append(mqttRuntimeDir);
		result.append(", socketsRuntimeDir: ");
		result.append(socketsRuntimeDir);
		result.append(", mqttRuntimeDirectoryURI: ");
		result.append(mqttRuntimeDirectoryURI);
		result.append(", socketsRuntimeDirectoryURI: ");
		result.append(socketsRuntimeDirectoryURI);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * 
	 * @generated not
	 */
	@Override
	public AadlToTargetBuildGenerator getAadlToTargetBuild() {
		if(aadlToTargetBuild==null)
			setAadlToTargetBuild(new AadlToEv3devIntegrationMakefileUnparser());
		return aadlToTargetBuild;
	}
	
	/**
	 * 
	 * @generated not
	 */	
	@Override
	public void execute(WorkflowExecutionContext context,
			final IProgressMonitor monitor) throws WorkflowExecutionException, IOException {
		mqttRuntimeDirectoryURI = URI.createURI(mqttRuntimeDir);
		socketsRuntimeDirectoryURI = URI.createURI(socketsRuntimeDir);
		if(!Platform.isRunning())
		{
			URI uri = context.getWorkflowFileURI();
			URI normalized = context.getGlobalResourceSet().getURIConverter().normalize(uri);
			
			mqttRuntimeDirectoryURI = mqttRuntimeDirectoryURI.resolve(normalized);
			socketsRuntimeDirectoryURI = socketsRuntimeDirectoryURI.resolve(normalized);
		} else {
			mqttRuntimeDirectoryURI = CommonPlugin.resolve(mqttRuntimeDirectoryURI);
			socketsRuntimeDirectoryURI = CommonPlugin.resolve(socketsRuntimeDirectoryURI);
		}
				
		super.execute(context, monitor);
	}


} //CodegenEv3devIntegrationImpl
