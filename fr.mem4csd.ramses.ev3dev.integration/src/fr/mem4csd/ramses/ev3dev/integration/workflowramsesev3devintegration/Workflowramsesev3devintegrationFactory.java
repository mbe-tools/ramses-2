/**
 */
package fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.Workflowramsesev3devintegrationPackage
 * @generated
 */
public interface Workflowramsesev3devintegrationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Workflowramsesev3devintegrationFactory eINSTANCE = fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.Workflowramsesev3devintegrationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Codegen Ev3dev Integration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Codegen Ev3dev Integration</em>'.
	 * @generated
	 */
	CodegenEv3devIntegration createCodegenEv3devIntegration();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Workflowramsesev3devintegrationPackage getWorkflowramsesev3devintegrationPackage();

} //Workflowramsesev3devintegrationFactory
