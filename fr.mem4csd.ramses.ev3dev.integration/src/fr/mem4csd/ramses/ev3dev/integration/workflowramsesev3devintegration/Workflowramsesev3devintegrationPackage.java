/**
 */
package fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration;

import fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.Workflowramsesev3devPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.Workflowramsesev3devintegrationFactory
 * @model kind="package"
 * @generated
 */
public interface Workflowramsesev3devintegrationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowramsesev3devintegration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/ramses/workflowramsesev3devintegration";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workflowramsesev3devintegration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Workflowramsesev3devintegrationPackage eINSTANCE = fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.Workflowramsesev3devintegrationPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.CodegenEv3devIntegrationImpl <em>Codegen Ev3dev Integration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.CodegenEv3devIntegrationImpl
	 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.Workflowramsesev3devintegrationPackageImpl#getCodegenEv3devIntegration()
	 * @generated
	 */
	int CODEGEN_EV3DEV_INTEGRATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__NAME = Workflowramsesev3devPackage.CODEGEN_EV3DEV__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__DESCRIPTION = Workflowramsesev3devPackage.CODEGEN_EV3DEV__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__ENABLED = Workflowramsesev3devPackage.CODEGEN_EV3DEV__ENABLED;

	/**
	 * The feature id for the '<em><b>Debug Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__DEBUG_OUTPUT = Workflowramsesev3devPackage.CODEGEN_EV3DEV__DEBUG_OUTPUT;

	/**
	 * The feature id for the '<em><b>Aadl Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__AADL_MODEL_SLOT = Workflowramsesev3devPackage.CODEGEN_EV3DEV__AADL_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Trace Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__TRACE_MODEL_SLOT = Workflowramsesev3devPackage.CODEGEN_EV3DEV__TRACE_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__OUTPUT_DIRECTORY = Workflowramsesev3devPackage.CODEGEN_EV3DEV__OUTPUT_DIRECTORY;

	/**
	 * The feature id for the '<em><b>Target Install Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__TARGET_INSTALL_DIR = Workflowramsesev3devPackage.CODEGEN_EV3DEV__TARGET_INSTALL_DIR;

	/**
	 * The feature id for the '<em><b>Include Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__INCLUDE_DIR = Workflowramsesev3devPackage.CODEGEN_EV3DEV__INCLUDE_DIR;

	/**
	 * The feature id for the '<em><b>Core Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__CORE_RUNTIME_DIR = Workflowramsesev3devPackage.CODEGEN_EV3DEV__CORE_RUNTIME_DIR;

	/**
	 * The feature id for the '<em><b>Target Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__TARGET_RUNTIME_DIR = Workflowramsesev3devPackage.CODEGEN_EV3DEV__TARGET_RUNTIME_DIR;

	/**
	 * The feature id for the '<em><b>Aadl To Source Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__AADL_TO_SOURCE_CODE = Workflowramsesev3devPackage.CODEGEN_EV3DEV__AADL_TO_SOURCE_CODE;

	/**
	 * The feature id for the '<em><b>Aadl To Target Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__AADL_TO_TARGET_CONFIGURATION = Workflowramsesev3devPackage.CODEGEN_EV3DEV__AADL_TO_TARGET_CONFIGURATION;

	/**
	 * The feature id for the '<em><b>Aadl To Target Build</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__AADL_TO_TARGET_BUILD = Workflowramsesev3devPackage.CODEGEN_EV3DEV__AADL_TO_TARGET_BUILD;

	/**
	 * The feature id for the '<em><b>Transformation Resources Pair List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__TRANSFORMATION_RESOURCES_PAIR_LIST = Workflowramsesev3devPackage.CODEGEN_EV3DEV__TRANSFORMATION_RESOURCES_PAIR_LIST;

	/**
	 * The feature id for the '<em><b>Target Properties</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__TARGET_PROPERTIES = Workflowramsesev3devPackage.CODEGEN_EV3DEV__TARGET_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Progress Monitor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__PROGRESS_MONITOR = Workflowramsesev3devPackage.CODEGEN_EV3DEV__PROGRESS_MONITOR;

	/**
	 * The feature id for the '<em><b>Uri Converter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__URI_CONVERTER = Workflowramsesev3devPackage.CODEGEN_EV3DEV__URI_CONVERTER;

	/**
	 * The feature id for the '<em><b>Target Install Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__TARGET_INSTALL_DIRECTORY_URI = Workflowramsesev3devPackage.CODEGEN_EV3DEV__TARGET_INSTALL_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Output Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__OUTPUT_DIRECTORY_URI = Workflowramsesev3devPackage.CODEGEN_EV3DEV__OUTPUT_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Core Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__CORE_RUNTIME_DIRECTORY_URI = Workflowramsesev3devPackage.CODEGEN_EV3DEV__CORE_RUNTIME_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Target Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__TARGET_RUNTIME_DIRECTORY_URI = Workflowramsesev3devPackage.CODEGEN_EV3DEV__TARGET_RUNTIME_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Include Directory URI List</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__INCLUDE_DIRECTORY_URI_LIST = Workflowramsesev3devPackage.CODEGEN_EV3DEV__INCLUDE_DIRECTORY_URI_LIST;

	/**
	 * The feature id for the '<em><b>Mqtt Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIR = Workflowramsesev3devPackage.CODEGEN_EV3DEV_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sockets Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIR = Workflowramsesev3devPackage.CODEGEN_EV3DEV_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Mqtt Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI = Workflowramsesev3devPackage.CODEGEN_EV3DEV_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sockets Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI = Workflowramsesev3devPackage.CODEGEN_EV3DEV_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Codegen Ev3dev Integration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION_FEATURE_COUNT = Workflowramsesev3devPackage.CODEGEN_EV3DEV_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = Workflowramsesev3devPackage.CODEGEN_EV3DEV___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int CODEGEN_EV3DEV_INTEGRATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT = Workflowramsesev3devPackage.CODEGEN_EV3DEV___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = Workflowramsesev3devPackage.CODEGEN_EV3DEV___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION___CHECK_CANCELED__IPROGRESSMONITOR = Workflowramsesev3devPackage.CODEGEN_EV3DEV___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Get Model Transformation Traces List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION___GET_MODEL_TRANSFORMATION_TRACES_LIST = Workflowramsesev3devPackage.CODEGEN_EV3DEV___GET_MODEL_TRANSFORMATION_TRACES_LIST;

	/**
	 * The operation id for the '<em>Get Target Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION___GET_TARGET_NAME = Workflowramsesev3devPackage.CODEGEN_EV3DEV___GET_TARGET_NAME;

	/**
	 * The number of operations of the '<em>Codegen Ev3dev Integration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_EV3DEV_INTEGRATION_OPERATION_COUNT = Workflowramsesev3devPackage.CODEGEN_EV3DEV_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration <em>Codegen Ev3dev Integration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Codegen Ev3dev Integration</em>'.
	 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration
	 * @generated
	 */
	EClass getCodegenEv3devIntegration();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration#getMqttRuntimeDir <em>Mqtt Runtime Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mqtt Runtime Dir</em>'.
	 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration#getMqttRuntimeDir()
	 * @see #getCodegenEv3devIntegration()
	 * @generated
	 */
	EAttribute getCodegenEv3devIntegration_MqttRuntimeDir();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration#getSocketsRuntimeDir <em>Sockets Runtime Dir</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sockets Runtime Dir</em>'.
	 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration#getSocketsRuntimeDir()
	 * @see #getCodegenEv3devIntegration()
	 * @generated
	 */
	EAttribute getCodegenEv3devIntegration_SocketsRuntimeDir();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration#getMqttRuntimeDirectoryURI <em>Mqtt Runtime Directory URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mqtt Runtime Directory URI</em>'.
	 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration#getMqttRuntimeDirectoryURI()
	 * @see #getCodegenEv3devIntegration()
	 * @generated
	 */
	EAttribute getCodegenEv3devIntegration_MqttRuntimeDirectoryURI();

	/**
	 * Returns the meta object for the attribute '{@link fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration#getSocketsRuntimeDirectoryURI <em>Sockets Runtime Directory URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sockets Runtime Directory URI</em>'.
	 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration#getSocketsRuntimeDirectoryURI()
	 * @see #getCodegenEv3devIntegration()
	 * @generated
	 */
	EAttribute getCodegenEv3devIntegration_SocketsRuntimeDirectoryURI();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Workflowramsesev3devintegrationFactory getWorkflowramsesev3devintegrationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.CodegenEv3devIntegrationImpl <em>Codegen Ev3dev Integration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.CodegenEv3devIntegrationImpl
		 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl.Workflowramsesev3devintegrationPackageImpl#getCodegenEv3devIntegration()
		 * @generated
		 */
		EClass CODEGEN_EV3DEV_INTEGRATION = eINSTANCE.getCodegenEv3devIntegration();

		/**
		 * The meta object literal for the '<em><b>Mqtt Runtime Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIR = eINSTANCE.getCodegenEv3devIntegration_MqttRuntimeDir();

		/**
		 * The meta object literal for the '<em><b>Sockets Runtime Dir</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIR = eINSTANCE.getCodegenEv3devIntegration_SocketsRuntimeDir();

		/**
		 * The meta object literal for the '<em><b>Mqtt Runtime Directory URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI = eINSTANCE.getCodegenEv3devIntegration_MqttRuntimeDirectoryURI();

		/**
		 * The meta object literal for the '<em><b>Sockets Runtime Directory URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI = eINSTANCE.getCodegenEv3devIntegration_SocketsRuntimeDirectoryURI();

	}

} //Workflowramsesev3devintegrationPackage
