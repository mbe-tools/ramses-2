/**
 */
package fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.impl;

import de.mdelab.workflow.WorkflowPackage;

import de.mdelab.workflow.components.ComponentsPackage;

import de.mdelab.workflow.helpers.HelpersPackage;

import fr.mem4csd.ramses.core.arch_trace.Arch_tracePackage;

import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.CodegenEv3devIntegration;
import fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.Workflowramsesev3devintegrationFactory;
import fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.Workflowramsesev3devintegrationPackage;

import fr.mem4csd.ramses.ev3dev.workflowramsesev3dev.Workflowramsesev3devPackage;

import fr.mem4csd.ramses.linux.workflowramseslinux.WorkflowramseslinuxPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Workflowramsesev3devintegrationPackageImpl extends EPackageImpl implements Workflowramsesev3devintegrationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codegenEv3devIntegrationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.ramses.ev3dev.integration.workflowramsesev3devintegration.Workflowramsesev3devintegrationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Workflowramsesev3devintegrationPackageImpl() {
		super(eNS_URI, Workflowramsesev3devintegrationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link Workflowramsesev3devintegrationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Workflowramsesev3devintegrationPackage init() {
		if (isInited) return (Workflowramsesev3devintegrationPackage)EPackage.Registry.INSTANCE.getEPackage(Workflowramsesev3devintegrationPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorkflowramsesev3devintegrationPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		Workflowramsesev3devintegrationPackageImpl theWorkflowramsesev3devintegrationPackage = registeredWorkflowramsesev3devintegrationPackage instanceof Workflowramsesev3devintegrationPackageImpl ? (Workflowramsesev3devintegrationPackageImpl)registeredWorkflowramsesev3devintegrationPackage : new Workflowramsesev3devintegrationPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Aadl2Package.eINSTANCE.eClass();
		Arch_tracePackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		Workflowramsesev3devPackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();
		HelpersPackage.eINSTANCE.eClass();
		ComponentsPackage.eINSTANCE.eClass();
		WorkflowramsesPackage.eINSTANCE.eClass();
		WorkflowramseslinuxPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkflowramsesev3devintegrationPackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowramsesev3devintegrationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorkflowramsesev3devintegrationPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Workflowramsesev3devintegrationPackage.eNS_URI, theWorkflowramsesev3devintegrationPackage);
		return theWorkflowramsesev3devintegrationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodegenEv3devIntegration() {
		return codegenEv3devIntegrationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenEv3devIntegration_MqttRuntimeDir() {
		return (EAttribute)codegenEv3devIntegrationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenEv3devIntegration_SocketsRuntimeDir() {
		return (EAttribute)codegenEv3devIntegrationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenEv3devIntegration_MqttRuntimeDirectoryURI() {
		return (EAttribute)codegenEv3devIntegrationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodegenEv3devIntegration_SocketsRuntimeDirectoryURI() {
		return (EAttribute)codegenEv3devIntegrationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Workflowramsesev3devintegrationFactory getWorkflowramsesev3devintegrationFactory() {
		return (Workflowramsesev3devintegrationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		codegenEv3devIntegrationEClass = createEClass(CODEGEN_EV3DEV_INTEGRATION);
		createEAttribute(codegenEv3devIntegrationEClass, CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIR);
		createEAttribute(codegenEv3devIntegrationEClass, CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIR);
		createEAttribute(codegenEv3devIntegrationEClass, CODEGEN_EV3DEV_INTEGRATION__MQTT_RUNTIME_DIRECTORY_URI);
		createEAttribute(codegenEv3devIntegrationEClass, CODEGEN_EV3DEV_INTEGRATION__SOCKETS_RUNTIME_DIRECTORY_URI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Workflowramsesev3devPackage theWorkflowramsesev3devPackage = (Workflowramsesev3devPackage)EPackage.Registry.INSTANCE.getEPackage(Workflowramsesev3devPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		HelpersPackage theHelpersPackage = (HelpersPackage)EPackage.Registry.INSTANCE.getEPackage(HelpersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		codegenEv3devIntegrationEClass.getESuperTypes().add(theWorkflowramsesev3devPackage.getCodegenEv3dev());

		// Initialize classes, features, and operations; add parameters
		initEClass(codegenEv3devIntegrationEClass, CodegenEv3devIntegration.class, "CodegenEv3devIntegration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCodegenEv3devIntegration_MqttRuntimeDir(), theEcorePackage.getEString(), "mqttRuntimeDir", null, 1, 1, CodegenEv3devIntegration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegenEv3devIntegration_SocketsRuntimeDir(), theEcorePackage.getEString(), "socketsRuntimeDir", null, 1, 1, CodegenEv3devIntegration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegenEv3devIntegration_MqttRuntimeDirectoryURI(), theHelpersPackage.getURI(), "mqttRuntimeDirectoryURI", null, 0, 1, CodegenEv3devIntegration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodegenEv3devIntegration_SocketsRuntimeDirectoryURI(), theHelpersPackage.getURI(), "socketsRuntimeDirectoryURI", null, 0, 1, CodegenEv3devIntegration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Workflowramsesev3devintegrationPackageImpl
