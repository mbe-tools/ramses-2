<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_PAkpAMixEei-D6bRtwoJ1Q" name="workflow" description="EMFTVM operations needed by posix">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_CZ8WMAgNEeuaPYhX2kwqog" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/templates/refine_remotes.workflow">
    <propertyValues xmi:id="_sncBQAgOEeuaPYhX2kwqog" name="has_remote_connections_cond" defaultValue="hasMqttCom"/>
    <propertyValues xmi:id="_1APfQAgOEeuaPYhX2kwqog" name="remotes_refinement_workflow" defaultValue="${scheme}${ramses_mqtt_plugin}ramses/mqtt/workflows/refine_mqtt.workflow">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_djkBsAh3EeuaPYhX2kwqog" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_2EVBwAh3EeuaPYhX2kwqog" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_4lQtcAh3EeuaPYhX2kwqog" name="include_dir" defaultValue="${include_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_7oMS8Ah3EeuaPYhX2kwqog" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_xCOyUAiiEeulUut1LEEszg" name="protocols" defaultValue="MQTT">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_c7GMoGZzEeuMzacQL6_RpQ" name="protocol" defaultValue="mqtt">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_DOoO0AxoEeus9savO-lBQQ" name="workflowDelegation" workflowURI="${scheme}${ramses_core_plugin}ramses/core/workflows/templates/refine_remotes.workflow">
    <propertyValues xmi:id="_Yt-sUAxoEeus9savO-lBQQ" name="has_remote_connections_cond" defaultValue="hasSocketCom"/>
    <propertyValues xmi:id="_hkjEYAxoEeus9savO-lBQQ" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_hkjEYQxoEeus9savO-lBQQ" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_hkjEYgxoEeus9savO-lBQQ" name="include_dir" defaultValue="${include_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_MAkLQAx4Eeugheonmvj5-g" name="protocols" defaultValue="SOCKETS_TCP,SOCKETS_UDP">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_ZZ91UAx4Eeugheonmvj5-g" name="remotes_refinement_workflow" defaultValue="${scheme}${ramses_sockets_plugin}ramses/sockets/workflows/refine_sockets.workflow">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_gUntMGZzEeuMzacQL6_RpQ" name="protocol" defaultValue="sockets">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_TSNnMAxoEeus9savO-lBQQ" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/templates/check_refine_and_generate.workflow">
    <propertyValues xmi:id="_X1EHUAxoEeus9savO-lBQQ" name="target" defaultValue="${target}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_fwbbYAxoEeus9savO-lBQQ" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_fwbbYQxoEeus9savO-lBQQ" name="source_file_name" defaultValue="${source_file_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_fwbbYgxoEeus9savO-lBQQ" name="include_dir" defaultValue="${include_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_fwbbYwxoEeus9savO-lBQQ" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_fwbbZQxoEeus9savO-lBQQ" name="code_generation_workflow" defaultValue="${code_generation_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_fwbbZgxoEeus9savO-lBQQ" name="target_install_dir" defaultValue="${target_install_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_MRO-QAh3EeuaPYhX2kwqog" name="locals_refinement_workflow" defaultValue="${locals_refinement_workflow}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <properties xmi:id="_YZkB4KEEEeqR3ds1UXwt-g" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_QAWnUAemEeu1QfSkbofkZQ" name="has_remote_connections_cond">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_3E2yAAh0EeuaPYhX2kwqog" fileURI="default_integration_ev3dev.properties"/>
  <propertiesFiles xmi:id="_DSP-wOhyEeqy-t86Uy9Gdw" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
