<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_BXDDIAKZEeuXP8YXOvMHxQ" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_rcx-EAMCEeuXP8YXOvMHxQ" name="workflowDelegation" workflowURI="${load_sockets_transformation_resources_workflow}"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_FY7-EAMDEeuXP8YXOvMHxQ" name="workflowDelegation" workflowURI="${load_mqtt_transformation_resources_workflow}"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_exoJoAyQEeugheonmvj5-g" name="workflowDelegation" workflowURI="${load_transformation_resources_target}"/>
  <propertiesFiles xmi:id="_sB-V0AMCEeuXP8YXOvMHxQ" fileURI="default_integration_ev3dev.properties"/>
</workflow:Workflow>
