package fr.mem4csd.ramses.mqtt;

import java.util.Set;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;

import fr.mem4csd.ramses.core.codegen.AbstractAadlToCMakefileUnparser;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;

public class MQTTGeneratorUtils {

	public static void addMQTTRuntimeFiles(URI mqttDirURI,
			Set<URI> srcFileSet,
			Set<URI> indludeDirSet,
			String targetName)
	{
		
		URI mqttValueInterface = mqttDirURI.appendSegment("aadl_ports_mqtt").appendFileExtension("c");
		srcFileSet.add(mqttValueInterface) ;
		URI mqttValueClient = mqttDirURI.appendSegment("mqtt_client_c").appendFileExtension("c");
		srcFileSet.add(mqttValueClient) ;

		indludeDirSet.add(mqttDirURI);
		URI libMqttDirURI = mqttDirURI.appendSegment("libmqtt");
		indludeDirSet.add(libMqttDirURI);
		URI libMqttLinuxDirURI = libMqttDirURI.appendSegment(targetName);
		indludeDirSet.add(libMqttLinuxDirURI);

		GeneratorUtils.addCFilesInFolder(libMqttDirURI, srcFileSet);
		GeneratorUtils.addCFilesInFolder(libMqttLinuxDirURI, srcFileSet);
	}
}
