<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowosate="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowosate" xmlns:workflowramses="https://mem4csd.telecom-paris.fr/ramses/workflowramses" xmlns:workflowramsesmqtt="https://mem4csd.telecom-paris.fr/ramses/workflowramsesmqtt" xmi:id="_PAkpAMixEei-D6bRtwoJ1Q" name="workflow" description="EMFTVM operations needed by posix">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_WBMNIKZmEeqzUc24dshnew" name="workflowDelegation" workflowURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/load_ramses_aadl_resources_core.workflow">
    <propertyValues xmi:id="_rbW9wKZoEeqzUc24dshnew" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_luZs5KZmEeqzUc24dshnew" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_luZs5aZmEeqzUc24dshnew" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_luZs56ZmEeqzUc24dshnew" name="system_implementation_name">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_luZs6KZmEeqzUc24dshnew" name="instance_model_file">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_luZs6aZmEeqzUc24dshnew" name="refined_trace_file" defaultValue="${remote_communications_file}.arch_trace">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_luZs6qZmEeqzUc24dshnew" name="validation_report_file" defaultValue="${remote_communications_file}.validation_report">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_luZs66ZmEeqzUc24dshnew" name="refined_aadl_file" defaultValue="${remote_communications_file}.aadl">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_luZs7KZmEeqzUc24dshnew" name="output_dir">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_gZDzYKW3EeqjYPyoOgr21g" name="${NameInputResourcesReader}" modelSlot="srcAadlModel" modelURI="${source_aadl_file}" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflowosate:AadlInstanceModelCreator" xmi:id="_HqdLoKW3EeqjYPyoOgr21g" name="aadlInstanceModelCreator" systemImplementationName="${system_implementation_name}" packageModelSlot="srcAadlModel" systemInstanceModelSlot="sourceAadlInstance"/>
  <components xsi:type="workflow.components:ModelWriter" xmi:id="_I5JzYKW3EeqjYPyoOgr21g" name="modelWriter" modelSlot="sourceAadlInstance" modelURI="${instance_model_file}" resolveURI="false"/>
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_tKtt8JtkEeqJ3cxvaDjAOg" name="posix_validation" workflowURI="${scheme}${ramses_linux_plugin}linux/workflows/validate_linux_instance.workflow">
    <propertyValues xmi:id="_TzkgcKE3Eeq8QfqryBdbVw" name="scheme" defaultValue="${scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_WvZJNRDnEeqR3ds1UXwt_g" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_WvZFQKeZqeqR3ds1UXwt_g" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_mTdfALb_EeqkNY6l67PJRQ" name="atl_transformation_utilities_plugin" defaultValue="${atl_transformation_utilities_plugin}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_G214MKDnEeqR3ds1UXwt-g" name="system_implementation_name" defaultValue="${system_implementation_name}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_L7cagKDnEeqR3ds1UXwt-g" name="instance_model_file" defaultValue="${instance_model_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_R7ppcKDnEeqR3ds1UXwt-g" name="validation_report_file" defaultValue="${validation_report_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_WvZFQKDnEeqR3ds1UXwt-g" name="source_aadl_file" defaultValue="${source_aadl_file}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_u1wDMP5bEeqjUK5cPU4ZFw" name="output_dir" defaultValue="${output_dir}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
    <propertyValues xmi:id="_zjdIcP5bEeqjUK5cPU4ZFw" name="runtime_scheme" defaultValue="${runtime_scheme}">
      <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    </propertyValues>
  </components>
  <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_238xcNDcEeqtbeq_dJTm6w" name="hasErrors" conditionSlot="violates${target}Constraints">
    <onTrue xmi:id="_554RYNDcEeqtbeq_dJTm6w" name="onTrue, exit"/>
    <onFalse xmi:id="_7xkLwNDcEeqtbeq_dJTm6w" name="onFalse">
      <components xsi:type="workflowramsesmqtt:ConditionEvaluationMqtt" xmi:id="_HTT-ALDbEeqLE8HsirCK_w" name="conditionEvaluationMqtt" instanceModelSlot="sourceAadlInstance" resultModelSlot="hasRemoteMqtt"/>
      <components xsi:type="workflow.components:ConditionalExecution" xmi:id="_Oc_WcLDbEeqLE8HsirCK_w" name="conditionalExecution" conditionSlot="hasRemoteMqtt">
        <onTrue xmi:id="_Rp_d8LDbEeqLE8HsirCK_w" name="onTrue">
          <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_YowQUJtlEeqJ3cxvaDjAOg" name="workflowDelegation" workflowURI="${scheme}${ramses_mqtt_plugin}mqtt/workflows/refinement_mqtt_instance.workflow">
            <propertyValues xmi:id="_vNtzIKW6EeqjYPyoOgr21g" name="scheme" defaultValue="${scheme}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_PeBRwKW7EeqjYPyoOgr21g" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_PeB40KW7EeqjYPyoOgr21g" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_o02dsLb_EeqkNY6l67PJRQ" name="atl_transformation_utilities_plugin" defaultValue="${atl_transformation_utilities_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_GyjXoKDzEeqeJ-TKsbzeiQ" name="source_aadl_file" defaultValue="${source_aadl_file}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_rcRREKD0EeqdR8mG0m0ifg" name="system_implementation_name" defaultValue="${system_implementation_name}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_81gNwKD0Eeq3L_Z9HNXREA" name="instance_model_file" defaultValue="${instance_model_file}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_VCf9QKD1EeqDXb0OjUP0yw" name="refined_trace_file" defaultValue="${refined_trace_file}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_eDYKEKEEEeqR3ds1UXwt-g" name="output_dir" defaultValue="${output_dir}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_NE2cgKE9EeqxdbEz6rHidg" name="refined_aadl_file" defaultValue="${refined_aadl_file}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
          </components>
          <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_kWs2AKDHEeqcwfXgm86T9A" name="workflowDelegation" workflowURI="${scheme}${ramses_linux_plugin}linux/workflows/execute_linux.workflow">
            <propertyValues xmi:id="_yCUowKW6EeqjYPyoOgr21g" name="scheme" defaultValue="${scheme}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_q4Y00KW-EeqjYPyoOgr21g" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_q4Y00aW-EeqjYPyoOgr21g" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_r62lULb_EeqkNY6l67PJRQ" name="atl_transformation_utilities_plugin" defaultValue="${atl_transformation_utilities_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_KA3eAaEEEeqR3ds1UXwt-g" name="source_aadl_file" defaultValue="${remote_communications_file}.aadl">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_KA3eBKEEEeqR3ds1UXwt-g" name="system_implementation_name" defaultValue="refined_model_for_mqtt_communications.impl">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_KA3eBaEEEeqR3ds1UXwt-g" name="instance_model_file" defaultValue="${remote_communications_file}.aaxl2">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_KA3eB6EEEeqR3ds1UXwt-g" name="refined_trace_file" defaultValue="${local_communications_file}.arch_trace">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_lKCqwKEEEeqR3ds1UXwt-g" name="validation_report_file" defaultValue="${local_communications_file}.validation_report">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_kAhh4OhpEeqy-t86Uy9Gdw" name="runtime_scheme" defaultValue="${runtime_scheme}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_oaDC8KEEEeqR3ds1UXwt-g" name="include_dir" defaultValue="${include_dir}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_lc924KE9EeqLct1UpVQ7Rg" name="refined_aadl_file" defaultValue="${local_communications_file}.aadl">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_8cvn4KGWEeq8nqBB_6ScqA" name="output_dir" defaultValue="${output_dir}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_fAjdILDgEeqLE8HsirCK_w" name="source_file_name" defaultValue="${source_file_name}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
          </components>
        </onTrue>
        <onFalse xmi:id="_TMSXILDbEeqLE8HsirCK_w" name="onFalse">
          <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_dZS9sLDbEeqLE8HsirCK_w" name="workflowDelegation" workflowURI="${scheme}${ramses_linux_plugin}linux/workflows/execute_linux_instance.workflow">
            <propertyValues xmi:id="_dZS9s7DbEeqLE8HsirCK_w" name="scheme" defaultValue="${scheme}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dZS9vrDbEeqLE8HsirCK_w" name="predeclared_property_sets_plugin" defaultValue="${predeclared_property_sets_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dZS9v7DbEeqLE8HsirCK_w" name="sei_predeclared_property_sets_plugin" defaultValue="${sei_predeclared_property_sets_plugin}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dZS9srDbEeqLE8HsirCK_w" name="source_aadl_file" defaultValue="${remote_communications_file}.aadl">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dZS9t7DbEeqLE8HsirCK_w" name="system_implementation_name" defaultValue="refined_model_for_mqtt_communications.impl">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dZS9uLDbEeqLE8HsirCK_w" name="instance_model_file" defaultValue="${instance_model_file}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dZS9ubDbEeqLE8HsirCK_w" name="refined_trace_file" defaultValue="${local_communications_file}.arch_trace">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dZS9urDbEeqLE8HsirCK_w" name="validation_report_file" defaultValue="${local_communications_file}.validation_report">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_AyvwEOhqEeqy-t86Uy9Gdw" name="runtime_scheme" defaultValue="${runtime_scheme}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dZS9u7DbEeqLE8HsirCK_w" name="include_dir" defaultValue="${include_dir}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dZS9vLDbEeqLE8HsirCK_w" name="refined_aadl_file" defaultValue="${local_communications_file}.aadl">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dZS9vbDbEeqLE8HsirCK_w" name="output_dir" defaultValue="${output_dir}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
            <propertyValues xmi:id="_dAlwgLDgEeqLE8HsirCK_w" name="source_file_name" defaultValue="${source_file_name}">
              <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
            </propertyValues>
          </components>
        </onFalse>
      </components>
    </onFalse>
  </components>
  <properties xmi:id="_RAT4AKDmEeqR3ds1UXwt-g" name="system_implementation_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_WpUu8KDmEeqR3ds1UXwt-g" name="instance_model_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_jAEpMKDmEeqR3ds1UXwt-g" name="refined_trace_file" defaultValue="${remote_communications_file}.arch_trace">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_mcjXIKDmEeqR3ds1UXwt-g" name="validation_report_file" defaultValue="${remote_communications_file}.validation_report">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_2FwDgKE8EeqgtNEZpZXkFQ" name="refined_aadl_file" defaultValue="${remote_communications_file}.aadl">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_YZkB4KEEEeqR3ds1UXwt-g" name="output_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_a1l4kLDgEeqLE8HsirCK_w" name="source_file_name">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_mjudQOhPEeqy-t86Uy9Gdw" fileURI="default_mqtt.properties"/>
  <propertiesFiles xmi:id="_5AsSoKDkEeqR3ds1UXwt-g" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
