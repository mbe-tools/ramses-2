<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowemftvm="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowemftvm" xmlns:workflowosate="https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowosate" xmlns:workflowramses="https://mem4csd.telecom-paris.fr/ramses/workflowramses" xmi:id="_PAkpAMixEei-D6bRtwoJ1Q" name="remote_communications_refinement" description="EMFTVM operations to refine MQTT">
  <components xsi:type="workflowemftvm:EmftVmTransformer" xmi:id="_nwSKwKA0EeqcwfXgm86T9A" name="${NameExecuteModelRefinement} for MQTT-based communications" registerDependencyModels="true" discardExtraRootElements="true">
    <rulesModelSlot>IOHelpers</rulesModelSlot>
    <rulesModelSlot>AADLCopyHelpers</rulesModelSlot>
    <rulesModelSlot>AADLICopyHelpers</rulesModelSlot>
    <rulesModelSlot>BehaviorAnnexServices</rulesModelSlot>
    <rulesModelSlot>PropertiesTools</rulesModelSlot>
    <rulesModelSlot>PackagesTools</rulesModelSlot>
    <rulesModelSlot>FeaturesTools</rulesModelSlot>
    <rulesModelSlot>Uninstanciate</rulesModelSlot>
    <rulesModelSlot>Features</rulesModelSlot>
    <rulesModelSlot>Implementations</rulesModelSlot>
    <rulesModelSlot>Properties</rulesModelSlot>
    <rulesModelSlot>Modes</rulesModelSlot>
    <rulesModelSlot>Flows</rulesModelSlot>
    <rulesModelSlot>Types</rulesModelSlot>
    <rulesModelSlot>Connections</rulesModelSlot>
    <rulesModelSlot>Services</rulesModelSlot>
    <rulesModelSlot>BehaviorAnnexTools</rulesModelSlot>
    <rulesModelSlot>ACGServices</rulesModelSlot>
    <rulesModelSlot>UninstanciateOverrideForRemoteConnections</rulesModelSlot>
    <rulesModelSlot>RemoteConnectionsCommonRefinementSteps</rulesModelSlot>
    <rulesModelSlot>RemoteConnectionsMqtt</rulesModelSlot>
    <rulesModelSlot>LanguageSpecificitiesC</rulesModelSlot>
    <inputModels xmi:id="_EmUF9vGhEemHmY6XjiEv8w" modelName="IN" modelSlot="sourceAadlInstance" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUF-fGhEemHmY6XjiEv8w" modelName="TOOLS" modelSlot="mqttAtlHelper" metamodelName="ATLHELPER" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/helpers"/>
    <inputModels xmi:id="_EmUF-vGhEemHmY6XjiEv8w" modelName="BASE_TYPES" modelSlot="Base_Types" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba"/>
    <inputModels xmi:id="_EmUGAPGhEemHmY6XjiEv8w" modelName="AADL_RUNTIME" modelSlot="aadl_runtime" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGBPGhEemHmY6XjiEv8w" modelName="RAMSES_PROCESSORS" modelSlot="RAMSES_processors" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGBfGhEemHmY6XjiEv8w" modelName="RAMSES_BUSES" modelSlot="RAMSES_buses" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGBvGhEemHmY6XjiEv8w" modelName="AADL_PROJECT" modelSlot="AADL_Project" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGB_GhEemHmY6XjiEv8w" modelName="COMMUNICATION_PROPERTIES" modelSlot="Communication_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGCPGhEemHmY6XjiEv8w" modelName="DATA_MODEL" modelSlot="Data_Model" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGCfGhEemHmY6XjiEv8w" modelName="DEPLOYMENT_PROPERTIES" modelSlot="Deployment_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGCvGhEemHmY6XjiEv8w" modelName="MEMORY_PROPERTIES" modelSlot="Memory_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGC_GhEemHmY6XjiEv8w" modelName="MODELING_PROPERTIES" modelSlot="Modeling_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGDPGhEemHmY6XjiEv8w" modelName="PROGRAMMING_PROPERTIES" modelSlot="Programming_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGDfGhEemHmY6XjiEv8w" modelName="THREAD_PROPERTIES" modelSlot="Thread_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGDvGhEemHmY6XjiEv8w" modelName="TIMING_PROPERTIES" modelSlot="Timing_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGD_GhEemHmY6XjiEv8w" modelName="ARINC653" modelSlot="ARINC653" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGEPGhEemHmY6XjiEv8w" modelName="CODE_GENERATION_PROPERTIES" modelSlot="Code_Generation_Properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGE_GhEemHmY6XjiEv8w" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <inputModels xmi:id="_EmUGFvGhEemHmY6XjiEv8w" modelName="RAMSES_PROPERTIES" modelSlot="RAMSES_properties" metamodelName="AADLI" metamodelURI="http://aadl.info/AADL/2.0/instance"/>
    <outputModels xmi:id="_VmBpwPmAEemTadmi341e_w" modelName="OUT_TRACE" modelSlot="mqttCommunicationsRefinedTraceModel" metamodelName="ARCH_TRACE" metamodelURI="https://mem4csd.telecom-paris.fr/ramses/arch_trace"/>
    <inputOutputModels xmi:id="_e6V98PGiEemHmY6XjiEv8w" modelName="OUT" modelSlot="mqttCommunicationsRefinedAadlModel" metamodelName="AADLBA" metamodelURI="https://github.com/osate/osate2-ba.git/aadlba" extension="aadl"/>
  </components>
  <components xsi:type="workflow.components:ModelWriter" xmi:id="_-XVboKDuEeqkxYunMcwp9w" name="${NameSaveRefinedAADLModel}" modelSlot="mqttCommunicationsRefinedAadlModel" modelURI="${refined_aadl_file}" resolveURI="false"/>
  <components xsi:type="workflowramses:TraceWriter" xmi:id="_AVM2gKDvEeqkxYunMcwp9w" name="${NameSaveTransformationTraceModel}" modelSlot="mqttCommunicationsRefinedTraceModel" modelURI="${refined_trace_file}" resolveURI="false"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_5ayacAkrEeuixIGZFNyO-g" name="${NameRefinedResourceReader}" modelSlot="mqttCommunicationsRefinedAadlModel" modelURI="${refined_aadl_file}" resolveURI="false" modelElementIndex="0"/>
  <components xsi:type="workflowosate:AadlInstanceModelCreator" xmi:id="_AYKyAAjZEeuixIGZFNyO-g" name="aadlInstanceModelCreator" systemImplementationName="refined_model_for_mqtt_communications.impl" packageModelSlot="mqttCommunicationsRefinedAadlModel" systemInstanceModelSlot="sourceAadlInstance"/>
  <properties xmi:id="_jfIusKExEeqOFPbPfJ1RVQ" name="refined_aadl_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_rSjPcKBnEeqcwfXgm86T9A" name="refined_trace_file" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_Op9bwKW0EeqkJezO-na_Nw" fileURI="default_mqtt.properties"/>
  <propertiesFiles xmi:id="_aJIZQOhZEeqy-t86Uy9Gdw" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
