/**
 * RAMSES 2.0
 * 
 * Copyright © 2012-2015 TELECOM Paris and CNRS
 * Copyright © 2016-2020 TELECOM Paris
 * 
 * TELECOM Paris
 * 
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program. 
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.nxtosek.codegen.makefile;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator ;
import java.util.List ;
import java.util.Set ;

import org.eclipse.core.runtime.IProgressMonitor ;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.NamedElement ;
import org.osate.aadl2.ProcessImplementation ;
import org.osate.aadl2.ProcessSubcomponent ;
import org.osate.aadl2.Processor;
import org.osate.aadl2.ProcessorImplementation ;
import org.osate.aadl2.ProcessorSubcomponent ;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.modelsupport.UnparseText ;
import org.osate.aadl2.modelsupport.modeltraversal.AadlProcessingSwitch;
import org.osate.aadl2.util.Aadl2Switch ;

import fr.mem4csd.ramses.core.codegen.AbstractAadlToCMakefileUnparser;
import fr.mem4csd.ramses.core.codegen.GenerationException;
import fr.mem4csd.ramses.core.codegen.utils.GeneratorUtils;
import fr.mem4csd.ramses.core.helpers.impl.AadlHelperImpl;
import fr.mem4csd.ramses.core.workflowramses.Codegen;
import fr.mem4csd.utils.SystemUtil;

public class AadlToNxtOSEKMakefileUnparser extends AbstractAadlToCMakefileUnparser {

	public static final String NXTOSEK_TARGET_ID = "nxtOSEK";
	
	private final static String NXTOSEK_RUNTIME_PATH = "/ecrobot/c/ecrobot.c";

	private static final String OSEK_MACRO = "USE_OSEK USE_NXTOSEK $(ADD_OPTS)";

	private UnparseText unparserContent;

	private List<ProcessSubcomponent> bindedProcess ;
	
	@Override
	public String getTargetName() {
		return NXTOSEK_TARGET_ID;
	}

	@Override
	public void generateProcessBuild(ProcessSubcomponent process,
			URI outputDir,
			IProgressMonitor monitor) throws GenerationException
	{
		generateMakefile((NamedElement) process, outputDir) ;
	}

	private void generateMakefile(NamedElement ne,
			URI makeFile)
	{
		unparserContent = new UnparseText() ;
		NxtOSEKMakefileUnparser unparser = new NxtOSEKMakefileUnparser();
		unparser.process(ne) ;
		super.saveMakefile(unparserContent, makeFile) ;
	}

	class NxtOSEKMakefileUnparser extends AadlProcessingSwitch
	{
		public NxtOSEKMakefileUnparser() {
			container = getCodeGenWorkflowComponent();
		}
		
		@Override
		protected void initSwitches()
		{
			aadl2Switch = new Aadl2Switch<String>()
			{

				@Override
				public String caseProcessSubcomponent(ProcessSubcomponent object)
				{
					unparserContent.addOutputNewline("TARGET = " + object.getName() +
							"_OSEK") ;

					unparserContent.addOutputNewline("TOPPERS_OSEK_OIL_SOURCE = ./"+object.getName()+".oil");

					process(object.getComponentImplementation()) ;
					unparserContent.addOutputNewline("O_PATH ?= build");
					URI path = container.getTargetInstallDirectoryURI();
					
					if ( SystemUtil.isWindowsLike()/*Platform.isRunning() && Platform.getOS().equalsIgnoreCase(Platform.OS_WIN32)*/)
					{
						path = getCygwinPath(path);
					}

					unparserContent.addOutputNewline("include "+toMakefileString(path)+"/ecrobot/ecrobot.mak");
					return DONE ;
				}

				@Override
				public String caseProcessImplementation(ProcessImplementation object)
				{
					
					unparserContent
					.addOutput("TARGET_SOURCES = \\\n\t");
					
					sourceFilesURISet.addAll(getGeneratedSourceFiles());
					
					Set<URI> sourceFileList = getListOfReferencedObjects(object);
					sourceFilesURISet.addAll(sourceFileList);
					
					Set<URI> targetSourceFiles = getTargetSourceFiles();
					sourceFilesURISet.addAll(targetSourceFiles);
					
					for( URI sourceFile : sourceFilesURISet ) {
						if ( isSourceOrObjectFile( sourceFile ) ) {
							sourceFile = sourceFile.trimFileExtension().appendFileExtension( "c" );
						}
						else
							continue;

						if ( SystemUtil.isWindowsLike() ) {
							sourceFile = getCygwinPath( sourceFile );
						}
						unparserContent.addOutput("\\\n\t");
						unparserContent.addOutput( toMakefileString( sourceFile ) );
					}

					unparserContent.addOutput("\n") ;
					unparserContent.addOutputNewline("USER_DEF = " + OSEK_MACRO);

					Iterator<URI> it = new IncludeDirIterator();

					if ( it.hasNext() ) {
						unparserContent.addOutput("export USER_INCLUDES=\\\n\t");
						
						while(it.hasNext())	{
							URI include = it.next();
							
							if ( SystemUtil.isWindowsLike() ) {
								include = getCygwinPath( include );
							}

							unparserContent.addOutput(include.path());
							
							if(it.hasNext())
								unparserContent.addOutput("\\\n\t");
						}
						
						unparserContent.addOutput("\n") ;
					}
					
					return DONE ;
				}

				@Override
				public String caseProcessorSubcomponent(ProcessorSubcomponent object)
				{

					bindedProcess =
							AadlHelperImpl.getBindedProcesses(object) ;

					process(object.getSubcomponentType()) ;
					return DONE ;
				}

				@Override
				public String caseProcessor(Processor object)
				{

					unparserContent.addOutput("\n") ;
					unparserContent
					.addOutputNewline("all: partitions") ;
					unparserContent.addOutputNewline("partitions:") ;

					for(ProcessSubcomponent part:bindedProcess)
					{
						unparserContent.addOutputNewline("\t$(MAKE) -C " + part.getName() +
								" all") ;
					}

					unparserContent.addOutputNewline("") ;
					unparserContent.addOutputNewline("benchmark:") ;
					unparserContent.addOutputNewline("\t@ADD_OPTS=BENCHMARK $(MAKE) all") ;

					unparserContent.addOutputNewline("") ;
					unparserContent.addOutputNewline("clean:") ;

					for(ProcessSubcomponent part:bindedProcess)
					{
						unparserContent.addOutputNewline("\t$(MAKE) -C " + part.getName() +
								" clean") ;
					}

					return DONE ;
				}
			} ;
		}
	}

	@Override
	public void generateProcessorBuild(Subcomponent processor,
			URI outputDir,
			IProgressMonitor monitor) throws GenerationException {
		generateMakefile((NamedElement) processor, outputDir) ;
	}

	@Override
	public boolean validateTargetPath(URI runtimePath)
	{
		if(runtimePath==null)
			return false;
		final String path;
		if(runtimePath.isFile())
			path = runtimePath.toFileString();
		else
			path = runtimePath.toString();
		File result = new File(path +File.separator+ NXTOSEK_RUNTIME_PATH) ;
	    
	    try 
	    {
	      FileReader fr = new FileReader(result);
	      fr.close();
	    }
	    catch (IOException e)
	    {
	      return false;
	    }

	    return true;
	}

	@Override
	public void generateSystemBuild(EList<Subcomponent> processors, URI outputDir, IProgressMonitor monitor) {
		unparserContent = new UnparseText();
		unparserContent.addOutputNewline("all:") ;

		for(Subcomponent aProcessorSubcomponent : processors)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " all\n") ;
		}

		unparserContent.addOutputNewline("benchmark:") ;

		for(Subcomponent aProcessorSubcomponent : processors)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " benchmark\n") ;
		}

		unparserContent.addOutputNewline("clean:") ;

		for(Subcomponent aProcessorSubcomponent : processors)
		{
			unparserContent.addOutputNewline("\t$(MAKE) -C " +
					aProcessorSubcomponent.getName() + " clean\n") ;
		}
		super.saveMakefile(unparserContent, outputDir) ;
	}
	
	@Override
	public String getTargetShortDescription() {
		return "(http://lejos-osek.sourceforge.net/)";
	}

	protected Set<URI> getTargetSourceFiles() {
		Set<URI> result = new HashSet<URI>();
		for( URI sourceFile : sourceFilesURISet ) {
			
			if ( "aadl_dispatch.c".equals( sourceFile.lastSegment() ) ) // FIXME better way to do this ??
			{
				sourceFile = container.getTargetRuntimeDirectoryURI().appendSegment( "aadl_dispatch_osek.c" );
				result.add(sourceFile);
			}
			else if ( "aadl_ports_standard.c".equals( sourceFile.lastSegment() ) )
			{
				sourceFile = container.getTargetRuntimeDirectoryURI().appendSegment( "aadl_ports_standard_osek.c" );
				result.add(sourceFile);
			}
		}
		return result;
	}
}