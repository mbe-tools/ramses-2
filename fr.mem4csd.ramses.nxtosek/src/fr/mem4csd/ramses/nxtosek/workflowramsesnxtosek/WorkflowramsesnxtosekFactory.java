/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.WorkflowramsesnxtosekPackage
 * @generated
 */
public interface WorkflowramsesnxtosekFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowramsesnxtosekFactory eINSTANCE = fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.impl.WorkflowramsesnxtosekFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Codegen Nxt Osek</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Codegen Nxt Osek</em>'.
	 * @generated
	 */
	CodegenNxtOsek createCodegenNxtOsek();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorkflowramsesnxtosekPackage getWorkflowramsesnxtosekPackage();

} //WorkflowramsesnxtosekFactory
