/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek;

import fr.mem4csd.ramses.core.workflowramses.WorkflowramsesPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.WorkflowramsesnxtosekFactory
 * @model kind="package"
 * @generated
 */
public interface WorkflowramsesnxtosekPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowramsesnxtosek";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/ramses/workflowramsesnxtosek";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workflowramsesnxtosek";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowramsesnxtosekPackage eINSTANCE = fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.impl.WorkflowramsesnxtosekPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.impl.CodegenNxtOsekImpl <em>Codegen Nxt Osek</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.impl.CodegenNxtOsekImpl
	 * @see fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.impl.WorkflowramsesnxtosekPackageImpl#getCodegenNxtOsek()
	 * @generated
	 */
	int CODEGEN_NXT_OSEK = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__NAME = WorkflowramsesPackage.CODEGEN__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__DESCRIPTION = WorkflowramsesPackage.CODEGEN__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__ENABLED = WorkflowramsesPackage.CODEGEN__ENABLED;

	/**
	 * The feature id for the '<em><b>Debug Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__DEBUG_OUTPUT = WorkflowramsesPackage.CODEGEN__DEBUG_OUTPUT;

	/**
	 * The feature id for the '<em><b>Aadl Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__AADL_MODEL_SLOT = WorkflowramsesPackage.CODEGEN__AADL_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Trace Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__TRACE_MODEL_SLOT = WorkflowramsesPackage.CODEGEN__TRACE_MODEL_SLOT;

	/**
	 * The feature id for the '<em><b>Output Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__OUTPUT_DIRECTORY = WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY;

	/**
	 * The feature id for the '<em><b>Target Install Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__TARGET_INSTALL_DIR = WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIR;

	/**
	 * The feature id for the '<em><b>Include Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__INCLUDE_DIR = WorkflowramsesPackage.CODEGEN__INCLUDE_DIR;

	/**
	 * The feature id for the '<em><b>Core Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__CORE_RUNTIME_DIR = WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIR;

	/**
	 * The feature id for the '<em><b>Target Runtime Dir</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__TARGET_RUNTIME_DIR = WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIR;

	/**
	 * The feature id for the '<em><b>Aadl To Source Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__AADL_TO_SOURCE_CODE = WorkflowramsesPackage.CODEGEN__AADL_TO_SOURCE_CODE;

	/**
	 * The feature id for the '<em><b>Aadl To Target Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__AADL_TO_TARGET_CONFIGURATION = WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_CONFIGURATION;

	/**
	 * The feature id for the '<em><b>Aadl To Target Build</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__AADL_TO_TARGET_BUILD = WorkflowramsesPackage.CODEGEN__AADL_TO_TARGET_BUILD;

	/**
	 * The feature id for the '<em><b>Transformation Resources Pair List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__TRANSFORMATION_RESOURCES_PAIR_LIST = WorkflowramsesPackage.CODEGEN__TRANSFORMATION_RESOURCES_PAIR_LIST;

	/**
	 * The feature id for the '<em><b>Target Properties</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__TARGET_PROPERTIES = WorkflowramsesPackage.CODEGEN__TARGET_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Progress Monitor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__PROGRESS_MONITOR = WorkflowramsesPackage.CODEGEN__PROGRESS_MONITOR;

	/**
	 * The feature id for the '<em><b>Uri Converter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__URI_CONVERTER = WorkflowramsesPackage.CODEGEN__URI_CONVERTER;

	/**
	 * The feature id for the '<em><b>Target Install Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__TARGET_INSTALL_DIRECTORY_URI = WorkflowramsesPackage.CODEGEN__TARGET_INSTALL_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Output Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__OUTPUT_DIRECTORY_URI = WorkflowramsesPackage.CODEGEN__OUTPUT_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Core Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__CORE_RUNTIME_DIRECTORY_URI = WorkflowramsesPackage.CODEGEN__CORE_RUNTIME_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Target Runtime Directory URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__TARGET_RUNTIME_DIRECTORY_URI = WorkflowramsesPackage.CODEGEN__TARGET_RUNTIME_DIRECTORY_URI;

	/**
	 * The feature id for the '<em><b>Include Directory URI List</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK__INCLUDE_DIRECTORY_URI_LIST = WorkflowramsesPackage.CODEGEN__INCLUDE_DIRECTORY_URI_LIST;

	/**
	 * The number of structural features of the '<em>Codegen Nxt Osek</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK_FEATURE_COUNT = WorkflowramsesPackage.CODEGEN_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = WorkflowramsesPackage.CODEGEN___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int CODEGEN_NXT_OSEK___EXECUTE__WORKFLOWEXECUTIONCONTEXT = WorkflowramsesPackage.CODEGEN___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = WorkflowramsesPackage.CODEGEN___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK___CHECK_CANCELED__IPROGRESSMONITOR = WorkflowramsesPackage.CODEGEN___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Get Model Transformation Traces List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK___GET_MODEL_TRANSFORMATION_TRACES_LIST = WorkflowramsesPackage.CODEGEN___GET_MODEL_TRANSFORMATION_TRACES_LIST;

	/**
	 * The operation id for the '<em>Get Target Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK___GET_TARGET_NAME = WorkflowramsesPackage.CODEGEN___GET_TARGET_NAME;

	/**
	 * The number of operations of the '<em>Codegen Nxt Osek</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODEGEN_NXT_OSEK_OPERATION_COUNT = WorkflowramsesPackage.CODEGEN_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.CodegenNxtOsek <em>Codegen Nxt Osek</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Codegen Nxt Osek</em>'.
	 * @see fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.CodegenNxtOsek
	 * @generated
	 */
	EClass getCodegenNxtOsek();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkflowramsesnxtosekFactory getWorkflowramsesnxtosekFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.impl.CodegenNxtOsekImpl <em>Codegen Nxt Osek</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.impl.CodegenNxtOsekImpl
		 * @see fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.impl.WorkflowramsesnxtosekPackageImpl#getCodegenNxtOsek()
		 * @generated
		 */
		EClass CODEGEN_NXT_OSEK = eINSTANCE.getCodegenNxtOsek();

	}

} //WorkflowramsesnxtosekPackage
