/**
 * RAMSES 2.0
 *
 * Copyright © 2020 TELECOM Paris
 *
 * TELECOM Paris
 *
 * Authors: see AUTHORS
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License as published by Eclipse,
 * either version 1.0 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 * You should have received a copy of the Eclipse Public License
 * along with this program.
 * If not, see https://www.eclipse.org/legal/epl-2.0/
 */

package fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.impl;

import org.eclipse.emf.ecore.EClass;

import fr.mem4csd.ramses.core.workflowramses.AadlToTargetBuildGenerator;
import fr.mem4csd.ramses.core.workflowramses.AadlToTargetConfigurationGenerator;
import fr.mem4csd.ramses.core.workflowramses.impl.CodegenImpl;
import fr.mem4csd.ramses.nxtosek.codegen.makefile.AadlToNxtOSEKMakefileUnparser;
import fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.CodegenNxtOsek;
import fr.mem4csd.ramses.nxtosek.workflowramsesnxtosek.WorkflowramsesnxtosekPackage;
import fr.mem4csd.ramses.osek.codegen.c.AadlToOSEKCUnparser;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Codegen Nxt Osek</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CodegenNxtOsekImpl extends CodegenImpl implements CodegenNxtOsek {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodegenNxtOsekImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowramsesnxtosekPackage.Literals.CODEGEN_NXT_OSEK;
	}

	
	/**
	 * 
	 * @generated not
	 */
	@Override
	public AadlToTargetConfigurationGenerator getAadlToTargetConfiguration() {
		if(aadlToTargetConfiguration == null)
			setAadlToTargetConfiguration(new AadlToOSEKCUnparser());
		return aadlToTargetConfiguration;
	}
	
	/**
	 * 
	 * @generated not
	 */
	@Override
	public AadlToTargetBuildGenerator getAadlToTargetBuild() {
		if(aadlToTargetBuild==null)
			setAadlToTargetBuild(new AadlToNxtOSEKMakefileUnparser());
		return aadlToTargetBuild;
	}
	
} //CodegenNxtOsekImpl
