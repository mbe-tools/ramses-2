<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmlns:workflowramsesnxtosek="https://mem4csd.telecom-paris.fr/ramses/workflowramsesnxtosek" xmi:id="_YKYe0D6KEeq5xtZ5_imDzA" name="workflow">
  <components xsi:type="workflow.components:ModelReader" xmi:id="_CanvUD6hEeqUz4Y2Tk3Zrg" name="aadlReader" modelSlot="refinedAadlModelOsek" modelURI="${refined_aadl_file}" modelElementIndex="0"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_l-mlMD6hEeqUz4Y2Tk3Zrg" name="traceReader" modelSlot="refinedTraceModelOsek" modelURI="${refined_trace_file}" modelElementIndex="0"/>
  <components xsi:type="workflowramsesnxtosek:CodegenNxtOsek" xmi:id="_pKKV0E5FEeqIOpzmJvnc-w" name="${NameCodeGeneration}" aadlModelSlot="refinedAadlModelOsek" traceModelSlot="refinedTraceModelOsek" outputDirectory="${output_dir}" targetInstallDir="${target_install_dir}" includeDir="${include_dir}" coreRuntimeDir="${core_runtime_dir}" targetRuntimeDir="${target_runtime_dir}"/>
  <properties xmi:id="_PK8IQEGjEeqBM5afCympPg" name="include_dir" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_mzRWYEGhEeqLOKt47Neqmg" name="output_dir" defaultValue="">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_sIE-0AVoEeuDOaqjzi9xMA" name="refined_aadl_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="_hoh6oD6SEeq5xtZ5_imDzA" name="refined_trace_file">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <properties xmi:id="__Gx9UL7DEeqbPJAkQiktyw" name="target_install_dir">
    <type href="http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </properties>
  <propertiesFiles xmi:id="_c9L24E4qEeqIOpzmJvnc-w" fileURI="default_nxtosek.properties"/>
  <propertiesFiles xmi:id="_0UuQIAVoEeuDOaqjzi9xMA" fileURI="platform:/plugin/fr.mem4csd.ramses.osek/ramses/osek/workflows/default_osek.properties"/>
  <propertiesFiles xmi:id="_AhRHQNxxEeq6KstHiQyO6A" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
