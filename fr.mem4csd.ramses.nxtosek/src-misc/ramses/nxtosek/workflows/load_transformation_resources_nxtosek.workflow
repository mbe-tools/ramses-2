<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_1bNJoEgDEeqsT5NnXFzohg" name="workflow">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="__gTxYEgDEeqsT5NnXFzohg" name="load_refinement_osek" workflowURI="${scheme}${ramses_osek_plugin}ramses/osek/workflows/load_transformation_resources_osek.workflow"/>
  <components xsi:type="workflow.components:ModelReader" xmi:id="_qGKgwEjDEeqafNRi83mxbA" name="${NameLoadRefinementTransformationModules}" modelSlot="Refinement${target}" modelURI="${ramses_nxtosek_transformation_refinement_path}nxtOSEKTarget.emftvm" modelElementIndex="0"/>
  <propertiesFiles xmi:id="_C7pdEE4pEeqIOpzmJvnc-w" fileURI="default_nxtosek.properties"/>
  <propertiesFiles xmi:id="_SGHcMNr_Eeq05enNFbiy6w" fileURI="platform:/plugin/fr.mem4csd.ramses.core/ramses/core/workflows/default.properties" resolveURI="false"/>
</workflow:Workflow>
